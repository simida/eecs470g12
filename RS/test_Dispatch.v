module testbench;

	       reg        				reset;
	       reg        				clock;
	       reg        				branch_mis;
	       reg [`PRF_SIZE-1:0] 			valid_list; 

	       reg [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_regA;         
	       reg [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_regB;        
	       reg [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_dest_reg;     
	       
	       reg [1:0] [63:0] 			ID_NPC;
	       reg [1:0] [31:0] 			ID_IR;
	       reg [1:0] [1:0] 				ID_opa_select;   
	       reg [1:0] [1:0] 				ID_opb_select;   
   	                                                                 
	       reg [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	ID_alu_func;      
	       reg [1:0]        			ID_rd_mem;       
	       reg [1:0]        			ID_wr_mem;        
	       reg [1:0]  			        ID_cond_branch;   
	       reg [1:0]        			ID_uncond_branch; 
	                                                                  
	       reg [1:0]       				ID_halt;         
	       reg [1:0]       				ID_illegal;      
	       reg [1:0]       				ID_valid_inst;    
	       reg [1:0] [`ROB_SIZE_LOG-1:0]          	ID_ROB;
	       reg [7:0]                              	IS_ID_Not_Erase;
	        
	       wire [1:0]		   		ID_IS_rs_valid;
	       wire [1:0] [63:0] 	   		ID_IS_NPC;
	       wire [1:0] [31:0] 	   		ID_IS_IR;
	       wire [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	ID_IS_alu_func;
	       wire [1:0] [`PRF_SIZE_LOG-1:0] 		ID_IS_regA;
	       wire [1:0] [`PRF_SIZE_LOG-1:0]        	ID_IS_regB;
	       wire [1:0] [1:0]	       			ID_IS_opa_sel;
	       wire [1:0] [1:0]     	   		ID_IS_opb_sel;
	       wire [1:0] [`PRF_SIZE_LOG-1:0]        	ID_IS_dest;
	       wire [1:0] [`ROB_SIZE_LOG-1:0]        	ID_IS_ROB;
	       
	       wire [1:0]		  	RS_reject;


	integer i;
	always
		#5 clock = ~clock;

	Dispatch_Stage Dispatch(
	       reset,
	       clock,
	       branch_mis,
	       valid_list, 
	       FE_ID_regA,         // reg A value
	       FE_ID_regB,         // reg B value
	       FE_ID_dest_reg,     // destination (writeback) register index
	       ID_NPC,
	       ID_IR,
	       ID_opa_select,    // ALU opa mux select (ALU_OPA_xxx *)
	       ID_opb_select,    // ALU opb mux select (ALU_OPB_xxx *)
	       ID_alu_func,      // ALU function select (ALU_xxx *)
	       ID_rd_mem,        // does inst read memory?
	       ID_wr_mem,        // does inst write memory?
	       ID_cond_branch,   // is inst a conditional branch?
	       ID_uncond_branch, // is inst an unconditional branch 
	       ID_halt,          // is inst a halt 00 01 10 11
	       ID_illegal,       // is inst an illegal inst 00 01 10 11
	       ID_valid_inst,    // is inst a valid instruction to be  00 01 10 11
	       ID_ROB,
	       IS_ID_Not_Erase,
	       ID_IS_rs_valid,
	       ID_IS_NPC,
	       ID_IS_IR,
	       ID_IS_alu_func,
	       ID_IS_regA,
	       ID_IS_regB,
	       ID_IS_opa_sel,
	       ID_IS_opb_sel,
	       ID_IS_dest,
	       ID_IS_ROB,
	       RS_reject	       
	       );


	initial begin
		reset = 1;
		clock = 0;	
		branch_mis = 0;
		valid_list = 64'b0;
		IS_ID_N_of_Issue = 0;
		ID_valid_inst[0] = 1'b0;
		ID_NPC[0] = 0;
		ID_IR[0] = `NOOP_INST;
		ID_alu_func[0] = $random;
		FE_ID_regA[0] = {$random, $random};
		FE_ID_regB[0] = {$random, $random};
		FE_ID_dest_reg[0] = $random;
		ID_ROB[0] = $random;
		ID_opa_select[0] = 0;
		ID_opb_select[0] = 0;
		ID_rd_mem[0] = 0;
		ID_wr_mem[0] = 0;
		ID_cond_branch[0] = 0;
		ID_uncond_branch[0] = 0;
		ID_halt[0] = 0;
		ID_illegal[0] = 0;
		ID_valid_inst[1] = 1'b0;
		ID_NPC[1] = 0;
		ID_IR[1] = `NOOP_INST;
		ID_alu_func[1] = $random;
		FE_ID_regA[1] = {$random, $random};
		FE_ID_regB[1] = {$random, $random};
		FE_ID_dest_reg[1] = $random;
		ID_ROB[1] = $random;
		ID_opa_select[1] = 0;
		ID_opb_select[1] = 0;
		ID_rd_mem[1] = 0;
		ID_wr_mem[1] = 0;
		ID_cond_branch[1] = 0;
		ID_uncond_branch[1] = 0;
		ID_halt[1] = 0;
		ID_illegal[1] = 0;
		i = 0;
		@(negedge clock) 
		IS_ID_Not_Erase = 8'b00000000;
		@(negedge clock) 
		for (i=0; i<8; i=i+2) begin
			@(negedge clock) 
			IS_ID_N_of_Issue = 2'b11;
			reset = 0;
			ID_valid_inst[0] = 1'b1;
			ID_NPC[0] = i;
			ID_IR[0] = i;
			ID_alu_func[0] = i;
			FE_ID_regA[0] = i;
			FE_ID_regB[0] = i;
			FE_ID_dest_reg[0] = 32+i;
			ID_ROB[0] = i;
			ID_opa_select[0] = 0;
			ID_opb_select[0] = 0;
			ID_valid_inst[1] = 1'b1;
			ID_NPC[1] = i+1;
			ID_IR[1] = i+1;
			ID_alu_func[1] = i+1;
			FE_ID_regA[1] = i+1;
			FE_ID_regB[1] = i+1;
			FE_ID_dest_reg[1] = 33+i;
			ID_ROB[1] = i+1;
			ID_opa_select[1] = 0;
			ID_opb_select[1] = 0;				
		end
		for (i=8; i<20; i=i+2) begin
			@(negedge clock) 
			IS_ID_N_of_Issue = 2'b11;
			valid_list=64'hffff_ffff_f0ff_ffff;
			ID_NPC[0] = i;
			ID_IR[0] = i;
			ID_alu_func[0] = i;
			FE_ID_regA[0] = i;
			FE_ID_regB[0] = i;
			FE_ID_dest_reg[0] = 32+i;
			ID_ROB[0] = i;
			ID_NPC[1] = i+1;
			ID_IR[1] = i+1;
			ID_alu_func[1] = i+1;
			FE_ID_regA[1] = i+1;
			FE_ID_regB[1] = i+1;
			FE_ID_dest_reg[1] = 33+i;
			ID_ROB[1] = i+1;
		end
		for (i=20; i<26; i=i+1) begin
			@(negedge clock) 
			IS_ID_N_of_Issue = 2'b01;
			ID_NPC[0] = i;
			ID_IR[0] = i;
			ID_alu_func[0] = i;
			FE_ID_regA[0] = i;
			FE_ID_regB[0] = i;
			FE_ID_dest_reg[0] = 32+i;
			ID_ROB[0] = i;
			ID_opa_select[0] = (i==25);
			ID_opb_select[0] = (i==25);
			ID_valid_inst[1] = 1'b0;
			ID_NPC[1] = $random;
			ID_IR[1] = $random;
			ID_alu_func[1] = $random;
			FE_ID_regA[1] = $random;
			FE_ID_regB[1] = $random;
			FE_ID_dest_reg[1] = $random;
			ID_ROB[1] = $random;
			ID_opa_select[1] = $random;
			ID_opb_select[1] = $random;
		end
		for (i=26; i<32; i=i+1) begin
			@(negedge clock) 
			IS_ID_N_of_Issue = 2'b10;
			ID_NPC[0] = $random;
			ID_IR[0] = $random;
			ID_alu_func[0] = $random;
			FE_ID_regA[0] = $random;
			FE_ID_regB[0] = $random;
			FE_ID_dest_reg[0] = $random;
			ID_ROB[0] = $random;
			ID_opa_select[0] = $random;
			ID_opb_select[0] = $random;
			ID_valid_inst[0] = 1'b0;
			ID_valid_inst[1] = 1'b1;
			ID_NPC[1] = i;
			ID_IR[1] = i;
			ID_alu_func[1] = i;
			FE_ID_regA[1] = i;
			FE_ID_regB[1] = i;
			FE_ID_dest_reg[1] = 32+i;
			ID_ROB[1] = i;
			ID_opa_select[1] = (i==26);
			ID_opb_select[1] = (i==26);
		end
		@(negedge clock) 
		ID_valid_inst = 0;
		for (i=33; i<40; i=i+1) begin
			IS_ID_N_of_Issue = 3'b11;
			ID_NPC[0] = $random;
			ID_IR[0] = $random;
			ID_alu_func[0] = $random;
			FE_ID_regA[0] = $random;
			FE_ID_regB[0] = $random;
			FE_ID_dest_reg[0] = $random;
			ID_ROB[0] = $random;
			ID_opa_select[0] = $random;
			ID_opb_select[0] = $random;
			ID_NPC[1] = $random;
			ID_IR[1] = $random;
			ID_alu_func[1] = $random;
			FE_ID_regA[1] = $random;
			FE_ID_regB[1] = $random;
			FE_ID_dest_reg[1] = $random;
			ID_ROB[1] = $random;
			ID_opa_select[1] = $random;
			ID_opb_select[1] = $random;
			@(negedge clock); 	
		end
		valid_list=64'hffff_ffff_ffff_ffff;		
		branch_mis=1'b1;
		for (i=40; i<48; i=i+1) begin
			IS_ID_N_of_Issue = 3'b11;
			@(negedge clock);
		end
		@(negedge clock) 
		$display("ENDING TESTBENCH : SUCCESS!");
		$display("@@@Passed");		
		$finish;	
	end


endmodule
