module testbench;

   reg 				reset;
   reg 				clock;
   reg 				branch_mis;
   reg [`PRF_SIZE-1:0]		valid_list; 
   reg 				RS_in_valid;
   reg [63:0] 			RS_in_NPC;
   reg [31:0] 			RS_in_IR;
   
   // `ALU_FUNC_NUM Undefined!!
   reg [`ALU_FUNC_NUM_LOG-1:0] 	RS_in_alu_func;
   // RS_in_regA/B contains reg#/intermediate. 
   reg [`PRF_SIZE_LOG - 1:0]	 	RS_in_regA;
   reg [`PRF_SIZE_LOG - 1:0] 		RS_in_regB;
   // Notification bit for RS_in_opa/b. 
   reg [1:0]		   		RS_in_opa_sel;
   reg [1:0] 		   		RS_in_opb_sel;
   // `PRF_SIZE undefined!!
   reg [`PRF_SIZE_LOG-1:0] 		RS_in_dest;
   // `ROB_SIZE undefined!!
   reg [`ROB_SIZE_LOG-1:0] 		RS_in_ROB;
   //////////
   //Outputs
   //////////
   //Outputs for FU
   wire 		   		RS_out_valid;
   wire [63:0] 	   		RS_out_NPC;
   wire [31:0] 	   		RS_out_IR;
   wire [`ALU_FUNC_NUM_LOG-1:0] 	RS_out_alu_func;
   wire [`PRF_SIZE_LOG-1:0] 	   	RS_out_regA;
   wire [`PRF_SIZE_LOG-1:0] 	   	RS_out_regB;
   wire [1:0]		   		RS_out_opa_sel;
   wire [1:0]		   		RS_out_opb_sel;
   wire [`PRF_SIZE_LOG-1:0] 	   	RS_out_dest;
   wire [`ROB_SIZE_LOG-1:0] 	   	RS_out_ROB;
   // Outputs for front End
   wire 		   RS_full;

	always
	  #5 clock=~clock;

	reservation_station RS( 
			    reset, 
			    clock, 
			    branch_mis,
			    valid_list,
			    RS_in_valid,
			    RS_in_NPC,
			    RS_in_IR,
			    RS_in_alu_func,
			    RS_in_regA,
			    RS_in_regB,
			    RS_in_opa_sel,
			    RS_in_opb_sel,
			    RS_in_dest,
			    RS_in_ROB,
			    RS_out_valid,
			    RS_out_NPC,
			    RS_out_IR,
			    RS_out_alu_func,
			    RS_out_regA,
			    RS_out_regB,
			    RS_out_opa_sel,
			    RS_out_opb_sel,
			    RS_out_dest,
			    RS_out_ROB, 
			    RS_full
   			    );
	
	integer i;
	integer rob;

	always @(posedge clock) begin
		if (reset)
			valid_list <= #1 64'b0;
		else begin
			if (i<7)
				valid_list <= #1 64'b0;
			else if (i<32)
				valid_list <= #1 64'h0000_0003_ff3f_0fff;
			else 
				valid_list <= #1 64'hffff_ffff_ffff_ffff;
		end
	end
	initial 
	begin
		reset = 1;
		clock = 0;
		branch_mis = 0;
		valid_list = 64'b0;
		RS_in_valid = 0;
		RS_in_NPC = 0;
		RS_in_IR = `NOOP_INST;
		RS_in_alu_func = 0;
		RS_in_regA = 0;
		RS_in_regB = 0;
		RS_in_dest = 0;
		RS_in_ROB = 0;
		RS_in_opa_sel = 0;
		RS_in_opb_sel = 0;
		i = 0;
		rob = 0;
		@(negedge clock) 
		for (i=0; i<64; i=i+1) begin
			@(negedge clock) 
			reset = 0;
			RS_in_valid = 1;
			RS_in_NPC = i;
			RS_in_IR = $random;
			RS_in_alu_func = $random;
			RS_in_regA = i;
			RS_in_regB = i+1;
			RS_in_dest = i*2 + 8;
			RS_in_ROB = rob;
			RS_in_opa_sel = 0;
			RS_in_opb_sel = i;	
			rob = (branch_mis)? 0 : rob + 1;	
			branch_mis = (i==40) ? 1 : 0;			
		end
		@(negedge clock)  $finish;
 	end // initial
endmodule
