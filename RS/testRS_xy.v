

module testbench;

	       reg        				reset;
	       reg        				clock;
	       reg        				branch_mis;
	       reg [`PRF_SIZE-1:0] 			valid_list; 

	       reg [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_regA;         
	       reg [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_regB;        
	       reg [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_dest_reg;     
	       
	       reg [1:0] [63:0] 			ID_NPC;
	       reg [1:0] [31:0] 			ID_IR;
	       reg [1:0] [1:0] 				ID_opa_select;   
	       reg [1:0] [1:0] 				ID_opb_select;   
   	                                                                 
	       reg [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	ID_alu_func;      
	       reg [1:0]        			ID_rd_mem;       
	       reg [1:0]        			ID_wr_mem;        
	       reg [1:0]  			        ID_cond_branch;   
	       reg [1:0]        			ID_uncond_branch; 
	                                                                  
	       reg [1:0]       				ID_halt;         
	       reg [1:0]       				ID_illegal;      
	       reg [1:0]       				ID_valid_inst;    
	       reg [1:0] [`ROB_SIZE_LOG-1:0]          	ID_ROB;
				 
	       reg [7:0]                              	IS_ID_Not_Erase;
	       
				 wire [3:0] [1:0]		   		ID_IS_rs_valid;
	       wire [3:0] [1:0] [63:0] 	   		ID_IS_NPC;
	       wire [3:0] [1:0] [31:0] 	   		ID_IS_IR;
	       wire [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	ID_IS_alu_func;
	       wire [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 		ID_IS_regA;
	       wire [3:0] [1:0] [`PRF_SIZE_LOG-1:0]        	ID_IS_regB;
	       wire [3:0] [1:0] [1:0]	       			ID_IS_opa_sel;
	       wire [3:0] [1:0] [1:0]     	   		ID_IS_opb_sel;
	       wire [3:0] [1:0] [`PRF_SIZE_LOG-1:0]        	ID_IS_dest;
	       wire [3:0] [1:0] [`ROB_SIZE_LOG-1:0]        	ID_IS_ROB;
	       
	        
	      
	       wire [1:0]		  	RS_reject;

               int clknum;
	       int handle;

	Dispatch_Stage Dispatch(
	       reset,
	       clock,
	       branch_mis,
	       valid_list, 

	       FE_ID_regA,         
	       FE_ID_regB,        
	       FE_ID_dest_reg,     
	       
	       ID_NPC,
	       ID_IR,
	       ID_opa_select,   
	       ID_opb_select,   
   	                                              
	       ID_alu_func,      
	       ID_rd_mem,       
	       ID_wr_mem,        
	          ID_cond_branch,   
	       ID_uncond_branch, 
	                                            
	       ID_halt,         
	       ID_illegal,      
	       ID_valid_inst,    
	       ID_ROB,
				 
	       IS_ID_Not_Erase,
	   
	       ID_IS_rs_valid,
	       ID_IS_NPC,
	       ID_IS_IR,
	       ID_IS_alu_func,
	       ID_IS_regA,
	       ID_IS_regB,
	       ID_IS_opa_sel,
	       ID_IS_opb_sel,
	       ID_IS_dest,
	       ID_IS_ROB,
	       RS_reject

	       );

	always
		#5 clock = ~clock;
	initial
	begin
		       handle=$fopen("testbench.out", "w"); 
		       clknum = 0;
		       reset=1;
		       clock=0;
		       branch_mis=0;
		       valid_list=0; 

		       FE_ID_regA=0;         
		       FE_ID_regB=0;        
		       FE_ID_dest_reg=0;     
		       
		       ID_NPC=0;
		       ID_IR=0;
		       ID_opa_select=0;   
		       ID_opb_select=0;   
	   	                                              
		       ID_alu_func=0;      
		       ID_rd_mem=0;       
		       ID_wr_mem=0;        
			  ID_cond_branch=0;   
		       ID_uncond_branch=0; 
			                                    
		       ID_halt=0;         
		       ID_illegal=0;      
		       ID_valid_inst=0;    
		       ID_ROB=0;
					 
		       IS_ID_Not_Erase=0;
			 @ (negedge clock);
		       reset=0;
		       testDispatch;
		       testISSUE;
		       BRANCH;
		       DIS_ISS;
			
		   		 @ (negedge clock);
			$finish;   

	end


	//Dispatch (ID_valid_inst = 01,10,11, test: RS_reject)
	task testDispatch;
		for(int i = 0;i<2;i++)begin
		FE_ID_regA[0] = 0;
		FE_ID_regB[0] = 1;
		FE_ID_regA[1] = 32 * i;
		FE_ID_regB[1] = 32*i +1;
		//ID_valid_inst = 2'00
			@ (negedge clock);
			ID_valid_inst = 2'b00;
		//ID_valid_inst = 2'01
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b01;
			ID_uncond_branch = 0;
			ID_cond_branch = 1;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b01;
			ID_cond_branch = 0;
			ID_uncond_branch =1;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b01;
			ID_cond_branch  = 0;
			ID_uncond_branch = 0;
			ID_wr_mem = 0;
			ID_rd_mem = 1;
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b01;
			ID_cond_branch  = 0;
			ID_uncond_branch = 0;
			ID_rd_mem = 0;
			ID_wr_mem = 1;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b01;
			ID_cond_branch  = 0;
			ID_uncond_branch = 0;
			ID_rd_mem = 0;
			ID_wr_mem = 0;
			ID_alu_func = `ALU_MULQ;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b01;
			ID_cond_branch  = 0;
			ID_uncond_branch = 0;
			ID_rd_mem = 0;
			ID_wr_mem = 0;
			ID_alu_func = `ALU_ADDQ;
	//ID_valid_inst = 10
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b10;
			ID_uncond_branch[1] = 0;
			ID_cond_branch[1] = 1;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b10;
			ID_cond_branch[1] = 0;
			ID_uncond_branch[1] =1;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b10;
			ID_cond_branch[1]  = 0;
			ID_uncond_branch[1] = 0;
			ID_wr_mem[1] = 0;
			ID_rd_mem[1] = 1;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b10;
			ID_cond_branch[1]  = 0;
			ID_uncond_branch[1] = 0;
			ID_rd_mem[1] = 0;
			ID_wr_mem[1] = 1;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b10;
			ID_cond_branch[1]  = 0;
			ID_uncond_branch[1] = 0;
			ID_rd_mem[1] = 0;
			ID_wr_mem[1] = 0;
			ID_alu_func[1] = `ALU_MULQ;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b10;
			ID_cond_branch[1]  = 0;
			ID_uncond_branch[1] = 0;
			ID_rd_mem[1] = 0;
			ID_wr_mem[1] = 0;
			ID_alu_func[1] = `ALU_ADDQ;
	//ID_valid_inst = 2'11
			@ (negedge clock);
	 		FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b11;
			ID_uncond_branch = 2'b00;
			ID_cond_branch = 2'b11;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b11;
			ID_cond_branch = 2'b00;
			ID_uncond_branch = 2'b11;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b11;
			ID_cond_branch  = 2'b00;
			ID_uncond_branch = 2'b00;
			ID_wr_mem = 2'b00;
			ID_rd_mem = 2'b11;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b11;
			ID_cond_branch  = 2'b00;
			ID_uncond_branch = 2'b00;
			ID_rd_mem = 2'b00;
			ID_wr_mem = 2'b11;
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b11;
			ID_cond_branch  = 2'b00;
			ID_uncond_branch = 2'b00;
			ID_rd_mem = 2'b00;
			ID_wr_mem = 2'b00;
			ID_alu_func = {`ALU_MULQ,`ALU_MULQ};
			@ (negedge clock);
			FE_ID_regA[0]++;//yq
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;//yq
			FE_ID_regB[1]++;
			ID_valid_inst = 2'b11;
			ID_cond_branch  = 2'b00;
			ID_uncond_branch = 2'b00;
			ID_rd_mem = 2'b00;
			ID_wr_mem = 2'b00;
			ID_alu_func = {2{`ALU_ADDQ}};
		end
	//	////$display("Test Dispatch Ends");
	endtask

	//Issue
	//re-write according to regA/B, RS, nuke prev RS
	task testISSUE; //	IS_ID_Not_Erase = 01,10,11, valid_list = 	test: RS_reject

		int j = 0;
		valid_list = 64'h0000_0000_0000_0000;
		ID_valid_inst = 0;	
	
		for(int m = 1; m < 8; m=m*2) begin
		@ (negedge clock);
			IS_ID_Not_Erase = m;//RS0, free 1 CDB
			valid_list[0] = 1;
			valid_list[1] = 1;
		@ (negedge clock);
			valid_list[0] = 0;	
			valid_list[1] = 1;
			valid_list[2] = 1;	 
			valid_list[3] = 1;	 
			IS_ID_Not_Erase = m;//RS0, free 2 CDB
		@ (negedge clock);
			valid_list[31:0] = 32'hffff_ffff; 	 
			IS_ID_Not_Erase = m;//RS1
		@ (negedge clock);
			IS_ID_Not_Erase = m;//RS1
		@ (negedge clock);
			IS_ID_Not_Erase = m;//RS2
		@ (negedge clock);
			IS_ID_Not_Erase = m;//RS2

		end
	endtask

	//Dispatch2 + Issue1
	task DIS_ISS;// ID_valid_inst = 11, IS_ID_N_of_Issue=01,valid_list=  test:RS_reject
			int j=0;

			FE_ID_regA[0]=0;
			FE_ID_regB[0]=1;
			FE_ID_regA[1]=32;
			FE_ID_regB[1]=33;
			ID_valid_inst = 2'b00;
			IS_ID_Not_Erase = 8'b11;//clear issue
			@ (negedge clock);
			FE_ID_regA[0]++;
			FE_ID_regB[0]++;
			FE_ID_regA[1]++;
			FE_ID_regB[1]++;
			ID_valid_inst = 8'b11;//Dispatch 2/cyc
			ID_uncond_branch = 0;
			ID_cond_branch = 2'b1;
			ID_rd_mem = 0;
			ID_wr_mem = 0;
			ID_alu_func = {2{`ALU_ADDQ}};
		@ (negedge clock);//DUPLICATE THIS BLOCK
			ID_uncond_branch = 0;//Disp
			ID_cond_branch = 2'b11;
			ID_rd_mem = 0;
			ID_wr_mem = 0;
			ID_alu_func = {2{`ALU_ADDQ}};
			IS_ID_Not_Erase[2] = 8'b00;//issue RS2
			valid_list[j] =1; 
		@ (negedge clock);
			ID_uncond_branch = 2'b0;//DispIssue: %2b
			ID_cond_branch = 2'b0;
			ID_rd_mem = 0;
			ID_wr_mem = 0;
			ID_alu_func = {2{`ALU_ADDQ}};
			IS_ID_Not_Erase[1] = 8'b00;//issue RS1

	endtask

	//Branch
	task BRANCH;// task1, then branch_mis=1, test:RS_reject

		@ (negedge clock);
			branch_mis=1;
		@ (negedge clock);
			branch_mis=0;

	endtask

	always@(negedge clock)
	begin
		clknum <= clknum + 1;
		#2 $fdisplay(handle,"Clock: %5d", clknum);
		$fdisplay(handle,"           --------------------------------             -------------------------------            -------------------------------             --------------------------------   ");
		$fdisplay(handle,"@@ALU_RS@@ | Valid: %2b RegA: %3d RegB: %3d | @@MULT_RS@@ | Valid: %2b RegA: %3d RegB: %3d | @@BR_RS@@ | Valid: %2b RegA: %3d RegB: %3d | @@MEM_RS@@ | Valid: %2b RegA: %3d RegB: %3d |", Dispatch.ALU.in_use[0], Dispatch.ALU.opa[0], Dispatch.ALU.opb[0], Dispatch.MULT.in_use[0], Dispatch.MULT.opa[0], Dispatch.MULT.opb[0], Dispatch.BR.in_use[0], Dispatch.BR.opa[0], Dispatch.BR.opb[0],  Dispatch.MEM.in_use[0], Dispatch.MEM.opa[0], Dispatch.MEM.opb[0]); 
		$fdisplay(handle,"           | Valid: %2b RegA: %3d RegB: %3d |             | Valid: %2b RegA: %3d RegB: %3d |           | Valid: %2b RegA: %3d RegB: %3d |            | Valid: %2b RegA: %3d RegB: %3d |", Dispatch.ALU.in_use[1], Dispatch.ALU.opa[1], Dispatch.ALU.opb[1], Dispatch.MULT.in_use[1], Dispatch.MULT.opa[1], Dispatch.MULT.opb[1], Dispatch.BR.in_use[1], Dispatch.BR.opa[1], Dispatch.BR.opb[1],  Dispatch.MEM.in_use[1], Dispatch.MEM.opa[1], Dispatch.MEM.opb[1]);
		$fdisplay(handle,"           | Valid: %2b RegA: %3d RegB: %3d |             | Valid: %2b RegA: %3d RegB: %3d |           | Valid: %2b RegA: %3d RegB: %3d |            | Valid: %2b RegA: %3d RegB: %3d |", Dispatch.ALU.in_use[2], Dispatch.ALU.opa[2], Dispatch.ALU.opb[2], Dispatch.MULT.in_use[2], Dispatch.MULT.opa[2], Dispatch.MULT.opb[2], Dispatch.BR.in_use[2], Dispatch.BR.opa[2], Dispatch.BR.opb[2],  Dispatch.MEM.in_use[2], Dispatch.MEM.opa[2], Dispatch.MEM.opb[2]);
		$fdisplay(handle,"           | Valid: %2b RegA: %3d RegB: %3d |             | Valid: %2b RegA: %3d RegB: %3d |           | Valid: %2b RegA: %3d RegB: %3d |            | Valid: %2b RegA: %3d RegB: %3d |", Dispatch.ALU.in_use[3], Dispatch.ALU.opa[3], Dispatch.ALU.opb[3], Dispatch.MULT.in_use[3], Dispatch.MULT.opa[3], Dispatch.MULT.opb[3], Dispatch.BR.in_use[3], Dispatch.BR.opa[3], Dispatch.BR.opb[3],  Dispatch.MEM.in_use[3], Dispatch.MEM.opa[3], Dispatch.MEM.opb[3]);
		$fdisplay(handle,"           | Valid: %2b RegA: %3d RegB: %3d |             --------------------------------           --------------------------------            --------------------------------", Dispatch.ALU.in_use[4], Dispatch.ALU.opa[4], Dispatch.ALU.opb[4]);
		$fdisplay(handle,"           | Valid: %2b RegA: %3d RegB: %3d |             ", Dispatch.ALU.in_use[5], Dispatch.ALU.opa[5], Dispatch.ALU.opb[5]);
		$fdisplay(handle,"           | Valid: %2b RegA: %3d RegB: %3d |             ", Dispatch.ALU.in_use[6], Dispatch.ALU.opa[6], Dispatch.ALU.opb[6]);
		$fdisplay(handle,"           | Valid: %2b RegA: %3d RegB: %3d |             ", Dispatch.ALU.in_use[7], Dispatch.ALU.opa[7], Dispatch.ALU.opb[7]);
		$fdisplay(handle,"           --------------------------------             -------------------------------            -------------------------------             --------------------------------   ");
		$fdisplay(handle,"@@Output@@ | Issue: %2b RegA: %3d RegB: %3d |             | Issue: %2b RegA: %3d RegB: %3d |           | Issue: %2b RegA: %3d RegB: %3d |            | Issue: %2b RegA: %3d RegB: %3d |", (ID_IS_rs_valid[0][0] &  ~IS_ID_Not_Erase[0]), ID_IS_regA[0][0], ID_IS_regB[0][0], (ID_IS_rs_valid[2][0] &  ~IS_ID_Not_Erase[4]), ID_IS_regA[2][0], ID_IS_regB[2][0], (ID_IS_rs_valid[1][0] &  ~IS_ID_Not_Erase[2]), ID_IS_regA[1][0], ID_IS_regB[1][0], (ID_IS_rs_valid[3][0] &  ~IS_ID_Not_Erase[6]), ID_IS_regA[3][0], ID_IS_regB[3][0]);
		$fdisplay(handle,"           | Issue: %2b RegA: %3d RegB: %3d |             | Issue: %2b RegA: %3d RegB: %3d |           | Issue: %2b RegA: %3d RegB: %3d |            | Issue: %2b RegA: %3d RegB: %3d |", (ID_IS_rs_valid[0][1] &  ~IS_ID_Not_Erase[1]), ID_IS_regA[0][1], ID_IS_regB[0][1], (ID_IS_rs_valid[2][1] &  ~IS_ID_Not_Erase[5]), ID_IS_regA[2][1], ID_IS_regB[2][1], (ID_IS_rs_valid[1][1] &  ~IS_ID_Not_Erase[3]), ID_IS_regA[1][1], ID_IS_regB[1][1], (ID_IS_rs_valid[3][1] &  ~IS_ID_Not_Erase[7]), ID_IS_regA[3][1], ID_IS_regB[3][1]);	
		$fdisplay(handle,"           --------------------------------             -------------------------------            -------------------------------             --------------------------------   ");
		$fdisplay(handle,"@@Inputs@@ | Valid: %2b Reject: %2b RegA: %3d RegB: %3d  MULT: %2b  BR: %2b  MEM: %2b            |     Valid list: 64%b", ID_valid_inst[0] & (~ID_illegal[0]) & (~ID_halt[0]), RS_reject[0], FE_ID_regA[0], FE_ID_regB[0], ID_alu_func[0]==`ALU_MULQ, ID_cond_branch[0] | ID_uncond_branch[0], ID_rd_mem[0] | ID_wr_mem[0], valid_list);
		$fdisplay(handle,"           | Valid: %2b Reject: %2b RegA: %3d RegB: %3d  MULT: %2b  BR: %2b  MEM: %2b            |                     ", ID_valid_inst[1] & (~ID_illegal[1]) & (~ID_halt[1]), RS_reject[1], FE_ID_regA[1], FE_ID_regB[1], ID_alu_func[1]==`ALU_MULQ, ID_cond_branch[1] | ID_uncond_branch[1], ID_rd_mem[1] | ID_wr_mem[1]);
		$fdisplay(handle,"           --------------------------------             -------------------------------            -------------------------------             --------------------------------   ");
	$fdisplay(handle,"");
	end



endmodule
