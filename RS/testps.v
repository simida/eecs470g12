module testbench;

	reg [15:0] req;
	reg [3:0] count;
	reg [1:0] en;
	reg clock;
	wire [1:0][3:0] gnt_4;
	wire [1:0][7:0] gnt_8;
	wire [1:0][15:0] gnt_16;


	rps_16 rps16(req, en, count, gnt_16);
	rps_8  rps8(req[7:0], en, count[2:0], gnt_8);
	rps_4  rps4(req[3:0], en, count[1:0], gnt_4);	

	always
	  #5 clock=~clock;

	always @(posedge clock) begin
		count <= count+1;
		en <= $random;
		req <= $random;
	end

	initial 
	begin
		req = 0;
		en = 0;
		count = 0;
		clock = 0;
		@(negedge clock) 
		en = 1;			
 		#2000 $finish;
 	end // initial
endmodule
