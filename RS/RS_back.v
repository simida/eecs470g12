module ps2(req, en, gnt, req_up);
	input [1:0] req;
	input en;
	output [1:0] gnt;
	output req_up;

	assign gnt[0] = (en & req[0]) ? 1'b1 : 1'b0;
	assign gnt[1] = (en & ~req[0] & req[1])? 1'b1 : 1'b0;
	assign req_up = | req[1:0];	

endmodule

module ps4(req, en, gnt, req_up);
	input [3:0] req;
	input en;
	output [3:0] gnt;
	output req_up;

	wire req_left, req_right;
	wire gnt_left, gnt_right;
	wire [3:0] temp;

	ps2 left(req[3:2], en, temp[3:2], req_left);
	ps2 right(req[1:0], en, temp[1:0], req_right);
	ps2 top({req_left, req_right}, en, {gnt_left, gnt_right}, req_up);

	assign gnt[3:2] = temp[3:2] & {gnt_left, gnt_left};
	assign gnt[1:0] = temp[1:0] & {gnt_right, gnt_right};

endmodule

module ps8(req, en, gnt, req_up);
	input [7:0] req;
	input en;
	output [7:0] gnt;
	output req_up;

	wire req_left, req_right;
	wire gnt_left, gnt_right;
	wire [7:0] temp;

	ps4 left(req[7:4], en, temp[7:4], req_left);
	ps4 right(req[3:0], en, temp[3:0], req_right);
	ps2 top({req_left, req_right}, en, {gnt_left, gnt_right}, req_up);

	assign gnt[7:4] = temp[7:4] & {gnt_left, gnt_left, gnt_left, gnt_left};
	assign gnt[3:0] = temp[3:0] & {gnt_right, gnt_right, gnt_right, gnt_right};

endmodule



module rps4(req, en, count, gnt, req_up);
	input [3:0] req;
	input [1:0] count;
	input en;
	output req_up;
	output [3:0] gnt;

	reg [3:0] req_tmp;
	reg [3:0] gnt;
	wire [3:0] gnt_tmp;
	
	always @* begin
		if (count == 2'b00) req_tmp = req;
		else if (count == 2'b01) req_tmp = {req[0], req[3:1]};
		else if (count == 2'b10) req_tmp = {req[1:0], req[3:2]};
		else req_tmp = {req[2:0], req[3]};
	end
	
	always @* begin
		if (count == 2'b00) gnt = gnt_tmp;
		else if (count == 2'b01) gnt = {gnt_tmp[2:0], gnt_tmp[3]};
		else if (count == 2'b10) gnt = {gnt_tmp[1:0], gnt_tmp[3:2]};
		else gnt = {gnt_tmp[0], gnt_tmp[3:1]};
	end
	
	ps4 ps4 (req_tmp, en, gnt_tmp, req_up);

endmodule

module rps8(req, en, count, gnt, req_up);
	input [7:0] req;
	input [2:0] count;
	input en;
	output req_up;
	output [7:0] gnt;

	reg [7:0] req_tmp;
	reg [7:0] gnt;
	wire [7:0] gnt_tmp;
	
	always @* begin
		if (count == 3'b000) req_tmp = req;
		else if (count == 3'b001) req_tmp = {req[0], req[7:1]};
		else if (count == 3'b010) req_tmp = {req[1:0], req[7:2]};
		else if (count == 3'b011) req_tmp = {req[2:0], req[7:3]};
		else if (count == 3'b100) req_tmp = {req[3:0], req[7:4]};
		else if (count == 3'b101) req_tmp = {req[4:0], req[7:5]};
		else if (count == 3'b110) req_tmp = {req[5:0], req[7:6]};
		else req_tmp = {req[6:0], req[7]};
	end
	
	always @* begin
		if (count == 3'b000) gnt = gnt_tmp;
		else if (count == 3'b001) gnt = {gnt_tmp[6:0], gnt_tmp[7]};
		else if (count == 3'b010) gnt = {gnt_tmp[5:0], gnt_tmp[7:6]};
		else if (count == 3'b011) gnt = {gnt_tmp[4:0], gnt_tmp[7:5]};
		else if (count == 3'b100) gnt = {gnt_tmp[3:0], gnt_tmp[7:4]};
		else if (count == 3'b101) gnt = {gnt_tmp[2:0], gnt_tmp[7:3]};
		else if (count == 3'b110) gnt = {gnt_tmp[1:0], gnt_tmp[7:2]};
		else gnt = {gnt_tmp[0], gnt_tmp[7:1]};		
	end
	
	ps8 ps8 (req_tmp, en, gnt_tmp, req_up);

endmodule



module reservation_station( 
	reset, 
	clock, 
	branch_mis,
	valid_list,
	RS_in_valid,
	RS_in_alu_func,
	RS_in_opa,
	RS_in_opb,
	RS_in_opa_immi,
	RS_in_opb_immi,
	RS_in_dest,
	RS_in_ROB,
	RS_out_valid,
	RS_out_alu_func,
	RS_out_opa,
	RS_out_opb,
	RS_out_opa_immi,
	RS_out_opb_immi,
	RS_out_dest,
	RS_out_ROB, 
	RS_full
);

input reset;
input clock;
input branch_mis;
input [63:0] valid_list; 
input RS_in_valid;
input [5:0] RS_in_alu_func;
input [63:0] RS_in_opa;
input [63:0] RS_in_opb;
input RS_in_opa_immi;
input RS_in_opb_immi;
input [5:0] RS_in_dest;
input [4:0] RS_in_ROB;
output RS_out_valid;
output [5:0] RS_out_alu_func;
output [63:0] RS_out_opa;
output [63:0] RS_out_opb;
output RS_out_opa_immi;
output RS_out_opb_immi;
output [5:0] RS_out_dest;
output [4:0] RS_out_ROB; 
output RS_full;


parameter RS_entry_num = 8;

reg [2:0] head, tail;
reg [5:0] alu_func [RS_entry_num-1:0];
reg [63:0] opa [RS_entry_num-1:0];        
reg [63:0] opb [RS_entry_num-1:0];         
reg [5:0] dest [RS_entry_num-1:0];        
reg [RS_entry_num-1:0] opa_valid;         
reg [RS_entry_num-1:0] opb_valid;         
reg [RS_entry_num-1:0] opa_immi;
reg [RS_entry_num-1:0] opb_immi;
reg [4:0] ROB [RS_entry_num-1:0];
reg [RS_entry_num-1:0] in_use;            

wire [RS_entry_num-1:0] rs_load_in;
wire [RS_entry_num-1:0] rs_free_in;
wire [RS_entry_num-1:0] rs_avail_out;
wire [RS_entry_num-1:0] rs_ready_out;

wire [5:0] alu_func_out [RS_entry_num-1:0];
wire [63:0] opa_out [RS_entry_num-1:0];        
wire [63:0] opb_out [RS_entry_num-1:0];         
wire [5:0] dest_out [RS_entry_num-1:0];           
wire [RS_entry_num-1:0] opa_immi_out;
wire [RS_entry_num-1:0] opb_immi_out;
wire [4:0] ROB_out [RS_entry_num-1:0];

genvar j;
generate 
	for (j=0; j<RS_entry_num; j=j+1) begin
		assign rs_avail_out[j] = ~in_use[j]; 
		assign rs_ready_out[j] = in_use[j] & opa_valid[j] & opb_valid[j]; 
	end
endgenerate

assign RS_full = !rs_ready_out & !rs_avail_out;
assign RS_out_valid = |rs_ready_out;

rps8 rps_dispatch (.req(rs_avail_out|rs_free_in), .en(RS_in_valid), .gnt(rs_load_in), .count(tail));
rps8 rps_issue (.req(rs_ready_out), .en(1'b1), .gnt(rs_free_in), .count(head));


always @(posedge clock) begin
	if (reset) begin
		head <= `SD 3'b000;
		tail <= `SD 3'b000;
	end
	else begin
		if (rs_free_in[head])
			head <= `SD head + 1;
		if (rs_load_in[tail])
			tail <= `SD tail + 1; 
	end
end


generate
	for (j=0; j<RS_entry_num; j=j+1) begin : dispatch
		always @(posedge clock) begin
			if (reset | branch_mis) begin
				alu_func[j] <= `SD 6'b0;
				opa[j] <= `SD 64'b0;
				opb[j] <= `SD 64'b0;
				dest[j] <= `SD 6'b0;
				opa_valid[j] <= `SD 1'b0;
				opb_valid[j] <= `SD 1'b0;
				opa_immi[j] <= `SD 1'b0;
				opb_immi[j] <= `SD 1'b0;
				ROB[j] <= `SD 1'b0;
				in_use[j] <= `SD 1'b0;	
			end
			else if (rs_load_in[j]) begin
				alu_func[j] <= `SD RS_in_alu_func;
				opa[j] <= `SD RS_in_opa;
				opb[j] <= `SD RS_in_opb;
				dest[j] <= `SD RS_in_dest;
				opa_valid[j] <= `SD valid_list[RS_in_opa[5:0]]|RS_in_opa_immi;
				opb_valid[j] <= `SD valid_list[RS_in_opb[5:0]]|RS_in_opb_immi;
				opa_immi[j] <= `SD RS_in_opa_immi;
				opb_immi[j] <= `SD RS_in_opb_immi;
				ROB[j] <= `SD RS_in_ROB;
				in_use[j] <= `SD 1'b1;
			end
			else if (rs_free_in[j]) begin
				in_use[j] <= `SD 1'b0;
			end
			else begin
				if (in_use[j] & !opa_immi[j] & !opa_valid[j] & valid_list[opa[j][5:0]])
					opa_valid[j] <= `SD 1'b1;
				if (in_use[j] & !opb_immi[j] & !opb_valid[j] & valid_list[opb[j][5:0]])
					opb_valid[j] <= `SD 1'b1;		
			end
		end
	end
endgenerate
	
generate 
	for (j=0; j<RS_entry_num; j=j+1) begin : issue
		assign alu_func_out[j] = {6{rs_free_in[j]}} & alu_func[j];
		assign opa_out[j] = {64{rs_free_in[j]}} & opa[j];        
		assign opb_out[j] = {64{rs_free_in[j]}} & opb[j];    
		assign dest_out[j] = {6{rs_free_in[j]}} & dest[j];       
		assign opa_immi_out[j] = rs_free_in[j] & opa_immi[j];
		assign opb_immi_out[j] = rs_free_in[j] & opb_immi[j];
		assign ROB_out[j] = {5{rs_free_in[j]}} & ROB[j];
	end
endgenerate

assign RS_out_alu_func = alu_func_out[0] | alu_func_out[1] | alu_func_out[2] | alu_func_out[3] | alu_func_out[4] | alu_func_out[5] | alu_func_out[6] | alu_func_out[7];
assign RS_out_opa = opa_out[0] | opa_out[1] | opa_out[2] | opa_out[3] | opa_out[4] | opa_out[5] | opa_out[6] | opa_out[7]; 
assign RS_out_opb = opb_out[0] | opb_out[1] | opb_out[2] | opb_out[3] | opb_out[4] | opb_out[5] | opb_out[6] | opb_out[7]; 
assign RS_out_dest = dest_out[0] | dest_out[1] | dest_out[2] | dest_out[3] | dest_out[4] | dest_out[5] | dest_out[6] | dest_out[7]; 
assign RS_out_opa_immi = opa_immi_out[0] | opa_immi_out[1] | opa_immi_out[2] | opa_immi_out[3] | opa_immi_out[4] | opa_immi_out[5] | opa_immi_out[6] | opa_immi_out[7]; 
assign RS_out_opb_immi = opb_immi_out[0] | opb_immi_out[1] | opb_immi_out[2] | opb_immi_out[3] | opb_immi_out[4] | opb_immi_out[5] | opb_immi_out[6] | opb_immi_out[7];
assign RS_out_ROB = ROB_out[0] | ROB_out[1] | ROB_out[2] | ROB_out[3] | ROB_out[4] | ROB_out[5] | ROB_out[6] | ROB_out[7]; 

endmodule

