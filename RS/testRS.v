module testbench;
      
      reg 				        reset;            // Syetem Reset
      reg 				        clock;            // System Clock
      reg     			        	branch_mis;       // Branch Misprediction from ROB. 00 if no; 01 if first mis; 10 if second mis; 11 if all miss
      reg [`PRF_SIZE-1:0]		        valid_list;       // Read Valid List to see who can be executed
      reg [1:0]			        	RS_in_valid;      // IF there is one/two Instructions dispatched
      reg [1:0]					RS_in_not_erase;
      reg [1:0] [63:0] 				RS_in_NPC;        // NPC for Dispatched INS
      reg [1:0] [31:0] 				RS_in_IR;         // IR for Dispatched INS  
      reg [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	RS_in_alu_func;   // ALU FUNCTION for Dispatched INS   
      reg [1:0] [`PRF_SIZE_LOG - 1:0] 		RS_in_regA;       // reg#/intermediate for REGA of Dispatched INS.
      reg [1:0] [`PRF_SIZE_LOG - 1:0] 		RS_in_regB;       // reg#/intermediate for REGB of Dispatched INS.
      reg [1:0] [1:0] 				RS_in_opa_sel;    // Notification bit for RS_in_opa
      reg [1:0] [1:0] 				RS_in_opb_sel;    // Notification bit for RS_in_opb
      reg [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_in_dest;       // Destination PRF Number for Dispatched INS
      reg [1:0] [`ROB_SIZE_LOG-1:0] 	        RS_in_ROB;        // ROB Number for Dispatched INS

      wire [1:0]		   		RS_out_valid;
      wire [1:0] [63:0] 	   		RS_out_NPC;
      wire [1:0] [31:0] 	   		RS_out_IR;
      wire [1:0] [`ALU_FUNC_NUM_LOG-1:0]      	RS_out_alu_func;
      wire [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regA;
      wire [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regB;
      wire [1:0] [1:0] 				RS_out_opa_sel;
      wire [1:0] [1:0] 				RS_out_opb_sel;
      wire [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_dest;
      wire [1:0] [`ROB_SIZE_LOG-1:0] 	        RS_out_ROB;
      wire [1:0] 			        RS_full;


      // Expected output value
      reg [1:0]		   			RS_exp_valid;
      reg [1:0] [63:0] 	   			RS_exp_NPC;
      reg [1:0] [31:0] 	   			RS_exp_IR;
      reg [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	RS_exp_alu_func;
      reg [1:0] [`PRF_SIZE_LOG-1:0] 		RS_exp_regA;
      reg [1:0] [`PRF_SIZE_LOG-1:0] 		RS_exp_regB;
      reg [1:0] [1:0] 				RS_exp_opa_sel;
      reg [1:0] [1:0] 				RS_exp_opb_sel;
      reg [1:0] [`PRF_SIZE_LOG-1:0] 		RS_exp_dest;
      reg [1:0] [`ROB_SIZE_LOG-1:0] 		RS_exp_ROB;
      reg [1:0]                         	RS_exp_N_of_Ready;  // Number of Instructions that are ready. At most two
      reg [1:0] 				RS_exp_full;	



	//--------------Test bench functions---------------
	task exit_on_error(input [32:0] i_idx);
		begin
					$display("Incorrect at time %4.0f, i=%d", $time, i_idx);
					$display("ENDING TESTBENCH : ERROR !");
					$display("@@@Failed");					
					$finish;
		end
	endtask
	
	integer i;
	reg correct;
	always @(posedge clock) begin
		if (i<12) begin
			RS_exp_valid = 0;
			RS_exp_full = (i==10) ? 3'b11 : 0;
		end
		else if (i<20) begin
			RS_exp_valid = 3;
			RS_exp_full = 0; 
			RS_exp_NPC[0] = i-12;
			RS_exp_IR[0] = i-12;
			RS_exp_alu_func[0] = i-12;
			RS_exp_regA[0] = i-12;
			RS_exp_regB[0] = i-12;
			RS_exp_opa_sel[0] = 0;
			RS_exp_opb_sel[0] = 0;		
			RS_exp_dest[0] = 20 + i;	
			RS_exp_ROB[0] = i-12;
			RS_exp_NPC[1] = i-11;
			RS_exp_IR[1] = i-11;
			RS_exp_alu_func[1] = i-11;
			RS_exp_regA[1] = i-11;
			RS_exp_regB[1] = i-11;
			RS_exp_opa_sel[1] = 0;
			RS_exp_opb_sel[1] = 0;		
			RS_exp_dest[1] = 21 + i;	
			RS_exp_ROB[1] = i-11;
		end
		else if (i == 20) begin
			RS_exp_valid = 3;
			RS_exp_full = 0; 
			RS_exp_NPC[0] = i-10;
			RS_exp_IR[0] = i-10;
			RS_exp_alu_func[0] = i-10;
			RS_exp_regA[0] = i-10;
			RS_exp_regB[0] = i-10;
			RS_exp_opa_sel[0] = 0;
			RS_exp_opb_sel[0] = 0;		
			RS_exp_dest[0] = 22 + i;	
			RS_exp_ROB[0] = i-10;
			RS_exp_NPC[1] = i-9;
			RS_exp_IR[1] = i-9;
			RS_exp_alu_func[1] = i-9;
			RS_exp_regA[1] = i-9;
			RS_exp_regB[1] = i-9;
			RS_exp_opa_sel[1] = 0;
			RS_exp_opb_sel[1] = 0;		
			RS_exp_dest[1] = 23 + i;	
			RS_exp_ROB[1] = i-9;
		end
		else if (i<27) begin
			RS_exp_valid = 3;
			RS_exp_full = 2'b01; 
			RS_exp_NPC[0] = i-9;
			RS_exp_IR[0] = i-9;
			RS_exp_alu_func[0] = i-9;
			RS_exp_regA[0] = i-9;
			RS_exp_regB[0] = i-9;
			RS_exp_opa_sel[0] = 0;
			RS_exp_opb_sel[0] = 0;		
			RS_exp_dest[0] = 23 + i;	
			RS_exp_ROB[0] = i-9;
		end
		else if (i<30) begin
			RS_exp_valid = 3;
			RS_exp_full = 2'b01; 
			RS_exp_NPC[1] = 2*i-35;
			RS_exp_IR[1] = 2*i-35;
			RS_exp_alu_func[1] = 2*i-35;
			RS_exp_regA[1] = 2*i-35;
			RS_exp_regB[1] = 2*i-35;
			RS_exp_opa_sel[1] = 0;
			RS_exp_opb_sel[1] = 0;		
			RS_exp_dest[1] = 2*i-3;	
			RS_exp_ROB[1] = 2*i-35;
		end
		else if (i==30) begin
			RS_exp_valid = 3;
			RS_exp_full = 2'b01; 
			RS_exp_NPC[1] = 18;
			RS_exp_IR[1] = 18;
			RS_exp_alu_func[1] = 18;
			RS_exp_regA[1] = 18;
			RS_exp_regB[1] = 18;
			RS_exp_opa_sel[1] = 0;
			RS_exp_opb_sel[1] = 0;		
			RS_exp_dest[1] = 50;	
			RS_exp_ROB[1] = 18;
		end
		else if (i==31) begin
			RS_exp_valid = 3;
			RS_exp_full = 2'b01; 
			RS_exp_NPC[1] = 20;
			RS_exp_IR[1] = 20;
			RS_exp_alu_func[1] = 20;
			RS_exp_regA[1] = 20;
			RS_exp_regB[1] = 20;
			RS_exp_opa_sel[1] = 0;
			RS_exp_opb_sel[1] = 0;		
			RS_exp_dest[1] = 52;	
			RS_exp_ROB[1] = 20;
		end
		else if (i==32) begin
			RS_exp_valid = 3;
			RS_exp_full = 2'b01; 
			RS_exp_NPC[1] = 28;
			RS_exp_IR[1] = 28;
			RS_exp_alu_func[1] = 28;
			RS_exp_regA[1] = 28;
			RS_exp_regB[1] = 28;
			RS_exp_opa_sel[1] = 0;
			RS_exp_opb_sel[1] = 0;		
			RS_exp_dest[1] = 60;	
			RS_exp_ROB[1] = 28;
		end
		else if (i==33) begin
			RS_exp_valid = 3;
			RS_exp_full = 0; 
			RS_exp_NPC[0] = 25;
			RS_exp_IR[0] = 25;
			RS_exp_alu_func[0] = 25;
			RS_exp_regA[0] = `PRF_ZERO_REG;
			RS_exp_regB[0] = `PRF_ZERO_REG;
			RS_exp_opa_sel[0] = 1;
			RS_exp_opb_sel[0] = 1;		
			RS_exp_dest[0] = 57;	
			RS_exp_ROB[0] = 25;
			RS_exp_NPC[1] = 29;
			RS_exp_IR[1] = 29;
			RS_exp_alu_func[1] = 29;
			RS_exp_regA[1] = 29;
			RS_exp_regB[1] = 29;
			RS_exp_opa_sel[1] = 0;
			RS_exp_opb_sel[1] = 0;		
			RS_exp_dest[1] = 61;	
			RS_exp_ROB[1] = 29;

		end
		else if (i==34) begin
			RS_exp_valid = 3;
			RS_exp_full = 0; 
			RS_exp_NPC[0] = 26;
			RS_exp_IR[0] = 26;
			RS_exp_alu_func[0] = 26;
			RS_exp_regA[0] = `PRF_ZERO_REG;
			RS_exp_regB[0] = `PRF_ZERO_REG;
			RS_exp_opa_sel[0] = 1;
			RS_exp_opb_sel[0] = 1;		
			RS_exp_dest[0] = 58;	
			RS_exp_ROB[0] = 26;
			RS_exp_NPC[1] = 30;
			RS_exp_IR[1] = 30;
			RS_exp_alu_func[1] = 30;
			RS_exp_regA[1] = 30;
			RS_exp_regB[1] = 30;
			RS_exp_opa_sel[1] = 0;
			RS_exp_opb_sel[1] = 0;		
			RS_exp_dest[1] = 62;	
			RS_exp_ROB[1] = 30;
		end
		else if (i==35) begin
			RS_exp_valid = 3;
			RS_exp_full = 0; 
			RS_exp_NPC[0] = 22;
			RS_exp_IR[0] = 22;
			RS_exp_alu_func[0] = 22;
			RS_exp_regA[0] = 22;
			RS_exp_regB[0] = 22;
			RS_exp_opa_sel[0] = 0;
			RS_exp_opb_sel[0] = 0;		
			RS_exp_dest[0] = 54;	
			RS_exp_ROB[0] = 22;
			RS_exp_NPC[1] = 31;
			RS_exp_IR[1] = 31;
			RS_exp_alu_func[1] = 31;
			RS_exp_regA[1] = 31;
			RS_exp_regB[1] = 31;
			RS_exp_opa_sel[1] = 0;
			RS_exp_opb_sel[1] = 0;		
			RS_exp_dest[1] = 63;	
			RS_exp_ROB[1] = 31;
		end 
		else begin
			RS_exp_valid = 0;
			RS_exp_full =  0;
		end		

	end


	always @(posedge clock) begin
		if (i<12) begin
			correct = RS_out_valid == RS_exp_valid;
			correct = correct && (RS_full == RS_exp_full);
		end
		else if (i<21) begin
			correct = RS_out_valid == RS_exp_valid;
			correct = correct && (RS_full == RS_exp_full);
			correct = correct && (RS_out_NPC == RS_exp_NPC);
			correct = correct && (RS_out_IR == RS_exp_IR);
			correct = correct && (RS_out_alu_func == RS_exp_alu_func);
			correct = correct && (RS_out_regA == RS_exp_regA);
			correct = correct && (RS_out_regB == RS_exp_regB);
			correct = correct && (RS_out_opa_sel == RS_exp_opa_sel);	
			correct = correct && (RS_out_opb_sel == RS_exp_opb_sel);
			correct = correct && (RS_out_dest == RS_exp_dest);
			correct = correct && (RS_out_ROB == RS_exp_ROB);		
		end
		else if (i<27) begin
			correct = RS_out_valid == RS_exp_valid;
			correct = correct && (RS_full == RS_exp_full);
			correct = correct && (RS_out_NPC[0] == RS_exp_NPC[0]);
			correct = correct && (RS_out_IR[0] == RS_exp_IR[0]);
			correct = correct && (RS_out_alu_func[0] == RS_exp_alu_func[0]);
			correct = correct && (RS_out_regA[0] == RS_exp_regA[0]);
			correct = correct && (RS_out_regB[0] == RS_exp_regB[0]);
			correct = correct && (RS_out_opa_sel[0] == RS_exp_opa_sel[0]);	
			correct = correct && (RS_out_opb_sel[0] == RS_exp_opb_sel[0]);
			correct = correct && (RS_out_dest[0] == RS_exp_dest[0]);
			correct = correct && (RS_out_ROB[0] == RS_exp_ROB[0]);	
		end
		else if (i<33) begin
			correct = RS_out_valid == RS_exp_valid;
			correct = correct && (RS_full == RS_exp_full);
			correct = correct && (RS_out_NPC[1] == RS_exp_NPC[1]);
			correct = correct && (RS_out_IR[1] == RS_exp_IR[1]);
			correct = correct && (RS_out_alu_func[1] == RS_exp_alu_func[1]);
			correct = correct && (RS_out_regA[1] == RS_exp_regA[1]);
			correct = correct && (RS_out_regB[1] == RS_exp_regB[1]);
			correct = correct && (RS_out_opa_sel[1] == RS_exp_opa_sel[1]);	
			correct = correct && (RS_out_opb_sel[1] == RS_exp_opb_sel[1]);
			correct = correct && (RS_out_dest[1] == RS_exp_dest[1]);
			correct = correct && (RS_out_ROB[1] == RS_exp_ROB[1]);	
		end
		else if (i<36) begin
			correct = RS_out_valid == RS_exp_valid;
			correct = correct && (RS_full == RS_exp_full);
			correct = correct && (RS_out_NPC == RS_exp_NPC);
			correct = correct && (RS_out_IR == RS_exp_IR);
			correct = correct && (RS_out_alu_func == RS_exp_alu_func);
			correct = correct && (RS_out_regA == RS_exp_regA);
			correct = correct && (RS_out_regB == RS_exp_regB);
			correct = correct && (RS_out_opa_sel == RS_exp_opa_sel);	
			correct = correct && (RS_out_opb_sel == RS_exp_opb_sel);
			correct = correct && (RS_out_dest == RS_exp_dest);
			correct = correct && (RS_out_ROB == RS_exp_ROB);
		end
		else begin
			correct = RS_out_valid == RS_exp_valid;
			correct = correct && (RS_full == RS_exp_full);
		end
//		if (!correct) exit_on_error(i);
	end


	// clock 
	always
	  #5 clock=~clock;

	RS_8 RS_8(     
			      reset,            
			      clock,            
			      branch_mis,       
			      valid_list,       
			      RS_in_valid,      
			      RS_in_not_erase,  
			      RS_in_NPC,        
			      RS_in_IR,         
			      RS_in_alu_func,   
			      RS_in_regA,       
			      RS_in_regB,       
			      RS_in_opa_sel,    
			      RS_in_opb_sel,    
			      RS_in_dest,       
			      RS_in_ROB,        
			      RS_out_valid,
			      RS_out_NPC,
			      RS_out_IR,
			      RS_out_alu_func,
			      RS_out_regA,
			      RS_out_regB,
			      RS_out_opa_sel,
			      RS_out_opb_sel,
			      RS_out_dest,
			      RS_out_ROB,  
			      RS_full			      
			      );

	
	// Test inputs
	initial 
	begin
		reset = 1;
		clock = 0;	
		branch_mis = 0;
		valid_list = 64'b0;
		RS_in_not_erase = 0;
		RS_in_valid[0] = 1'b0;
		RS_in_NPC[0] = 0;
		RS_in_IR[0] = `NOOP_INST;
		RS_in_alu_func[0] = $random;
		RS_in_regA[0] = {$random, $random};
		RS_in_regB[0] = {$random, $random};
		RS_in_dest[0] = $random;
		RS_in_ROB[0] = $random;
		RS_in_opa_sel[0] = 0;
		RS_in_opb_sel[0] = 0;
		RS_in_valid[1] = 1'b0;
		RS_in_NPC[1] = 0;
		RS_in_IR[1] = `NOOP_INST;
		RS_in_alu_func[1] = $random;
		RS_in_regA[1] = {$random, $random};
		RS_in_regB[1] = {$random, $random};
		RS_in_dest[1] = $random;
		RS_in_ROB[1] = $random;
		RS_in_opa_sel[1] = 0;
		RS_in_opb_sel[1] = 0;
		i = 0;
		@(negedge clock) 
		RS_in_not_erase = 2'b00;
		@(negedge clock) 
		for (i=0; i<8; i=i+2) begin
			@(negedge clock) 
			RS_in_not_erase = 2'b00;
			reset = 0;
			RS_in_valid[0] = 1'b1;
			RS_in_NPC[0] = i;
			RS_in_IR[0] = i;
			RS_in_alu_func[0] = i;
			RS_in_regA[0] = i;
			RS_in_regB[0] = i;
			RS_in_dest[0] = 32+i;
			RS_in_ROB[0] = i;
			RS_in_opa_sel[0] = 0;
			RS_in_opb_sel[0] = 0;
			RS_in_valid[1] = 1'b1;
			RS_in_NPC[1] = i+1;
			RS_in_IR[1] = i+1;
			RS_in_alu_func[1] = i+1;
			RS_in_regA[1] = i+1;
			RS_in_regB[1] = i+1;
			RS_in_dest[1] = 33+i;
			RS_in_ROB[1] = i+1;
			RS_in_opa_sel[1] = 0;
			RS_in_opb_sel[1] = 0;				
		end
		for (i=8; i<20; i=i+2) begin
			@(negedge clock) 
			RS_in_not_erase = 2'b00;
			valid_list=64'hffff_ffff_f0ff_ffff;
			RS_in_NPC[0] = i;
			RS_in_IR[0] = i;
			RS_in_alu_func[0] = i;
			RS_in_regA[0] = i;
			RS_in_regB[0] = i;
			RS_in_dest[0] = 32+i;
			RS_in_ROB[0] = i;
			RS_in_NPC[1] = i+1;
			RS_in_IR[1] = i+1;
			RS_in_alu_func[1] = i+1;
			RS_in_regA[1] = i+1;
			RS_in_regB[1] = i+1;
			RS_in_dest[1] = 33+i;
			RS_in_ROB[1] = i+1;
		end
		for (i=20; i<26; i=i+1) begin
			@(negedge clock) 
			RS_in_not_erase = 2'b10;
			RS_in_NPC[0] = i;
			RS_in_IR[0] = i;
			RS_in_alu_func[0] = i;
			RS_in_regA[0] = i;
			RS_in_regB[0] = i;
			RS_in_dest[0] = 32+i;
			RS_in_ROB[0] = i;
			RS_in_opa_sel[0] = (i==25);
			RS_in_opb_sel[0] = (i==25);
			RS_in_valid[1] = 1'b0;
			RS_in_NPC[1] = $random;
			RS_in_IR[1] = $random;
			RS_in_alu_func[1] = $random;
			RS_in_regA[1] = $random;
			RS_in_regB[1] = $random;
			RS_in_dest[1] = $random;
			RS_in_ROB[1] = $random;
			RS_in_opa_sel[1] = $random;
			RS_in_opb_sel[1] = $random;
		end
		for (i=26; i<32; i=i+1) begin
			@(negedge clock) 
			RS_in_not_erase = 2'b01;
			RS_in_NPC[0] = $random;
			RS_in_IR[0] = $random;
			RS_in_alu_func[0] = $random;
			RS_in_regA[0] = $random;
			RS_in_regB[0] = $random;
			RS_in_dest[0] = $random;
			RS_in_ROB[0] = $random;
			RS_in_opa_sel[0] = $random;
			RS_in_opb_sel[0] = $random;
			RS_in_valid[0] = 1'b0;
			RS_in_valid[1] = 1'b1;
			RS_in_NPC[1] = i;
			RS_in_IR[1] = i;
			RS_in_alu_func[1] = i;
			RS_in_regA[1] = i;
			RS_in_regB[1] = i;
			RS_in_dest[1] = 32+i;
			RS_in_ROB[1] = i;
			RS_in_opa_sel[1] = (i==26);
			RS_in_opb_sel[1] = (i==26);
		end
		@(negedge clock) 
		RS_in_valid = 0;
		for (i=33; i<40; i=i+1) begin
			RS_in_not_erase = 2'b00;
			RS_in_NPC[0] = $random;
			RS_in_IR[0] = $random;
			RS_in_alu_func[0] = $random;
			RS_in_regA[0] = $random;
			RS_in_regB[0] = $random;
			RS_in_dest[0] = $random;
			RS_in_ROB[0] = $random;
			RS_in_opa_sel[0] = $random;
			RS_in_opb_sel[0] = $random;
			RS_in_NPC[1] = $random;
			RS_in_IR[1] = $random;
			RS_in_alu_func[1] = $random;
			RS_in_regA[1] = $random;
			RS_in_regB[1] = $random;
			RS_in_dest[1] = $random;
			RS_in_ROB[1] = $random;
			RS_in_opa_sel[1] = $random;
			RS_in_opb_sel[1] = $random;
			@(negedge clock); 	
		end
		valid_list=64'hffff_ffff_ffff_ffff;		
		branch_mis=1'b1;
		for (i=40; i<48; i=i+1) begin
			RS_in_not_erase = 2'b00;
			@(negedge clock);
		end
		@(negedge clock) 
		$display("ENDING TESTBENCH : SUCCESS!");
		$display("@@@Passed");		
		$finish;
 	end // initial
endmodule
