module NoO_4 (	// INPUTS
		input [3:0] Nin,
		// OUTPUTS
		output [1:0] Nout
		);
   assign Nout[0] = (|Nin);
   assign Nout[1] = ~ ((Nin == 4'b0000) | (Nin == 4'b0001) | (Nin == 4'b0100) | (Nin == 4'b1000) | (Nin == 4'b0010));

endmodule // NoO_4

module NoO_8 (// INPUTS
	      input [7:0] Nin,
	      // OUTPUTS
	      output [1:0] Nout
	      );

   wire [1:0]		   N1out,N2out;
      
   NoO_4 n1 (Nin[7:4], N1out);
   NoO_4 n2 (Nin[3:0], N2out);
   NoO_4 nt ({N2out, N1out}, Nout);

endmodule // NoO_8

module NoO_16 (// INPUTS
	      input [15:0] Nin,
	      // OUTPUTS
	      output [1:0] Nout
	      );

   wire [1:0]		   N1out,N2out;
   
   NoO_8 n1 (Nin[15:8], N1out);
   NoO_8 n2 (Nin[7:0], N2out);
   NoO_4 nt ({N2out, N1out}, Nout);

endmodule // NoO_8

