module Top_RS (
	       //////////
	       // Inputs
	       //////////
	       input        				reset;
	       input        				clock;
	       input        				branch_mis;
	       input [`PRF_SIZE-1:0] 			valid_list; 
	       input 					FU_MEM_stall;
	       
	       //Inputs from RAT
	       input [1:0] [`PRF_SIZE_LOG-1:0] 		RAT_regA;         // reg A value
	       input [1:0] [`PRF_SIZE_LOG-1:0] 		RAT_regB;         // reg B value
	       input [1:0] [`PRF_SIZE_LOG-1:0] 		RAT_dest_reg;     // destination (writeback) register index
	       
	       //Inputs from Decoder
	       input [1:0] [63:0] 			id_NPC;
	       input [1:0] [31:0] 			id_IR;
	       input [1:0] [1:0] 			id_opa_select;    // ALU opa mux select (ALU_OPA_xxx *)
	       input [1:0] [1:0] 			id_opb_select;    // ALU opb mux select (ALU_OPB_xxx *)
   	                                                                  // (ZERO_REG if no writeback)
	       input [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	id_alu_func;      // ALU function select (ALU_xxx *)
	       input [1:0]        			id_rd_mem;        // does inst read memory?
	       input [1:0]        			id_wr_mem;        // does inst write memory?
	       input [1:0]  			        id_cond_branch;   // is inst a conditional branch?
	       input [1:0]        			id_uncond_branch; // is inst an unconditional branch 
	                                                                  // or jump?
	       input [1:0]       			id_halt;          // is inst a halt 00 01 10 11
	       input [1:0]       			id_illegal;       // is inst an illegal inst 00 01 10 11
	       input [1:0]       			id_valid_inst;    // is inst a valid instruction to be  00 01 10 11
	       // counted for CPI calculations?
	       
	       //Inputs from ROB
	       input [`ROB_SIZE_LOG-1:0] 		ROB_head;

	       //Inputs from PRF
	       input [1:0]                              PRF_RS_N_of_Issue;
	        
	       //////////
	       //Outputs
	       //////////
	       //Outputs for FU
	       output [1:0]		   		RS_out_valid;
	       output [1:0] [63:0] 	   		RS_out_NPC;
	       output [1:0] [31:0] 	   		RS_out_IR;
	       output [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	RS_out_alu_func;
	       output [1:0] [`PRF_SIZE_LOG-1:0] 	RS_out_regA;
	       output [1:0] [`PRF_SIZE_LOG-1:0]        	RS_out_regB;
	       output [1:0] [1:0]	       		RS_out_opa_sel;
	       output [1:0] [1:0]     	   		RS_out_opb_sel;
	       output [1:0] [`PRF_SIZE_LOG-1:0]        	RS_out_dest;
	       output [1:0] [`ROB_SIZE_LOG-1:0]        	RS_out_ROB;
	       
	       // Outputs for front End
	       output [3:0] [1:0]		  		RS_full;
	       
	       );
   logic [2:0] 							counter;
   logic [7:0]							RS_Arbiter_in_N_of_Ready;
   logic [1:0] [7:0] 						RS_Arbiter_out_N_of_Issue;
   
							

   wire [3:0] [1:0] 					RS_in_valid;
   wire [3:0] [1:0] [63:0] 				RS_in_NPC;
   wire [3:0] [1:0] [31:0] 				RS_in_IR;
   wire [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 		RS_in_alu_func;
   wire [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 		RS_in_regA;
   wire [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 		RS_in_regB;
   wire [3:0] [1:0] 					RS_in_opa_sel;
   wire [3:0] [1:0] 					RS_in_opb_sel;
   wire [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 		RS_in_dest;
   wire [3:0] [1:0] [`ROB_SIZE_LOG-1:0] 		RS_in_ROB;
   
   wire inst_valid = id_valid_inst & (~id_illegal) & (~id_halt);

   assign RS_in_valid[0] = inst_valid & (~RS_in_valid[1]) & (~RS_in_valid[2]) & (~RS_in_valid[3]);
   assign RS_in_valid[1] = inst_valid & (id_cond_branch | id_uncond_branch);
   assign RS_in_valid[2] = inst_valid & (id_alu_func == `ALU_MULQ);
   assign RS_in_valid[3] = inst_valid & (id_rd_mem | id_wr_mem);

   genvar i,j;
   generate for (i=0; i<4; i=i+1) begin
      for (i=0; j < 2; j = j+1) begin
	 assign RS_in_NPC[i][j] 		= RS_in_valid[i][j] ? id_NPC[j] : 0;
	 assign RS_in_IR[i][j] 			= RS_in_valid[i][j] ? id_IR[j] : `NOOP_INST;
	 assign RS_in_alu_func[i][j] 		= RS_in_valid[i][j] ? id_alu_func[j] : 0;
	 assign RS_in_regA[i][j] 		= RS_in_valid[i][j] ? RAT_regA[j] : 0;
	 assign RS_in_regB[i][j] 		= RS_in_valid[i][j] ? RAT_regB[j] : 0;
	 assign RS_in_opa_sel[i][j] 		= RS_in_valid[i][j] ? id_opa_select[j] : 0;
	 assign RS_in_opb_sel[i][j]		= RS_in_valid[i][j] ? id_opb_select[j] : 0;
	 assign RS_in_dest[i][j] 		= RS_in_valid[i][j] ? RAT_dest_reg[j] : 0;
	 assign RS_in_ROB[i][j] 		= RS_in_valid[i][j] ? ROB_head[j] : 0;
      end // for (i=0; j < 2; j = j+1)
   end // for (i=0; i<4; i=i+1)
      
   endgenerate


   reservation_station_8 ALU( 
			      .reset(reset), 
			      .clock(clock), 
			      .branch_mis(branch_mis),
			      .valid_list(valid_list),
			      .RS_in_valid(RS_in_valid[0]),
                              .RS_in_N_of_Issue(RS_Arbiter_out_N_of_Issue[1][1:0] | RS_Arbiter_out_N_of_Issue[0][1:0]),
			      .RS_in_NPC(RS_in_NPC[0]),
			      .RS_in_IR(RS_in_IR[0]),				
			      .RS_in_alu_func(RS_in_alu_func[0]),
			      .RS_in_regA(RS_in_regA[0]),
			      .RS_in_regB(RS_in_regB[0]),
			      .RS_in_opa_sel(RS_in_opa_sel[0]),
			      .RS_in_opb_sel(RS_in_opb_sel[0]),
			      .RS_in_dest(RS_in_dest[0]),
			      .RS_in_ROB(RS_in_ROB[0]),
			      .RS_out_valid(RS_ALU_valid),
			      .RS_out_NPC(RS_ALU_NPC),
			      .RS_out_IR(RS_ALU_IR),
			      .RS_out_alu_func(RS_ALU_alu_func),
			      .RS_out_regA(RS_ALU_regA),
			      .RS_out_regB(RS_ALU_regB),
			      .RS_out_opa_sel(RS_ALU_opa_sel),
			      .RS_out_opb_sel(RS_ALU_opb_sel),
			      .RS_out_dest(RS_ALU_dest),
			      .RS_out_RON(RS_ALU_ROB),
			      .RS_out_N_of_Ready(RS_Arbiter_in_N_of_Ready[1:0]),
			      .RS_full(RS_full[0])
   			    );

   reservation_station_4 BR( 
			      .reset(reset), 
			      .clock(clock), 
			      .branch_mis(branch_mis),
			      .valid_list(valid_list),
			      .RS_in_valid(RS_in_valid[1]),
                              .RS_in_N_of_Issue(RS_Arbiter_out_N_of_Issue[1][3:2] | RS_Arbiter_out_N_of_Issue[0][3:2]),
			      .RS_in_NPC(RS_in_NPC[1]),
			      .RS_in_IR(RS_in_IR[1]),				
			      .RS_in_alu_func(RS_in_alu_func[1]),
			      .RS_in_regA(RS_in_regA[1]),
			      .RS_in_regB(RS_in_regB[1]),
			      .RS_in_opa_sel(RS_in_opa_sel[1]),
			      .RS_in_opb_sel(RS_in_opb_sel[1]),
			      .RS_in_dest(RS_in_dest[1]),
			      .RS_in_ROB(RS_in_ROB[1]),
			      .RS_out_valid(RS_BR _valid),
			      .RS_out_NPC(RS_BR _NPC),
			      .RS_out_IR(RS_BR_IR),
			      .RS_out_alu_func(RS_ALU_BR_func),
			      .RS_out_regA(RS_BR_regA),
			      .RS_out_regB(RS_BR_regB),
			      .RS_out_opa_sel(RS_BR_opa_sel),
			      .RS_out_opb_sel(RS_BR_opb_sel),
			      .RS_out_dest(RS_BR_dest),
			      .RS_out_RON(RS_BR_ROB),
			      .RS_out_N_of_Ready(RS_Arbiter_in_N_of_Ready[3:2]),
			      .RS_full(RS_full[1])
   			    );
   
   reservation_station_4 MULT( 
			      .reset(reset), 
			      .clock(clock), 
			      .branch_mis(branch_mis),
			      .valid_list(valid_list),
			      .RS_in_valid(RS_in_valid[2]),
                              .RS_in_N_of_Issue(RS_Arbiter_out_N_of_Issue[1][5:4] | RS_Arbiter_out_N_of_Issue[0][5:4]),
			      .RS_in_NPC(RS_in_NPC[2]),
			      .RS_in_IR(RS_in_IR[2]),				
			      .RS_in_alu_func(RS_in_alu_func[2]),
			      .RS_in_regA(RS_in_regA[2]),
			      .RS_in_regB(RS_in_regB[2]),
			      .RS_in_opa_sel(RS_in_opa_sel[2]),
			      .RS_in_opb_sel(RS_in_opb_sel[2]),
			      .RS_in_dest(RS_in_dest[2]),
			      .RS_in_ROB(RS_in_ROB[2]),
			      .RS_out_valid(RS_MULT _valid),
			      .RS_out_NPC(RS_MULT _NPC),
			      .RS_out_IR(RS_MULT_IR),
			      .RS_out_alu_func(RS_ALU_MULT_func),
			      .RS_out_regA(RS_MULT_regA),
			      .RS_out_regB(RS_MULT_regB),
			      .RS_out_opa_sel(RS_MULT_opa_sel),
			      .RS_out_opb_sel(RS_MULT_opb_sel),
			      .RS_out_dest(RS_MULT_dest),
			      .RS_out_RON(RS_MULT_ROB),
			      .RS_out_N_of_Ready(RS_Arbiter_in_N_of_Ready[5:4]),
			      .RS_full(RS_full[2])
  			    );
   
     reservation_station_4 MEM( 
			      .reset(reset), 
			      .clock(clock), 
			      .branch_mis(branch_mis),
			      .valid_list(valid_list),
			      .RS_in_valid(RS_in_valid[3]),
                              .RS_in_N_of_Issue(RS_Arbiter_out_N_of_Issue[1][7:6] | RS_Arbiter_out_N_of_Issue[0][7:6]),
			      .RS_in_NPC(RS_in_NPC[3]),
			      .RS_in_IR(RS_in_IR[3]),				
			      .RS_in_alu_func(RS_in_alu_func[3]),
			      .RS_in_regA(RS_in_regA[3]),
			      .RS_in_regB(RS_in_regB[3]),
			      .RS_in_opa_sel(RS_in_opa_sel[3]),
			      .RS_in_opb_sel(RS_in_opb_sel[3]),
			      .RS_in_dest(RS_in_dest[3]),
			      .RS_in_ROB(RS_in_ROB[3]),
			      .RS_out_valid(RS_MEM _valid),
			      .RS_out_NPC(RS_MEM _NPC),
			      .RS_out_IR(RS_MEM_IR),
			      .RS_out_alu_func(RS_ALU_MEM_func),
			      .RS_out_regA(RS_MEM_regA),
			      .RS_out_regB(RS_MEM_regB),
			      .RS_out_opa_sel(RS_MEM_opa_sel),
			      .RS_out_opb_sel(RS_MEM_opb_sel),
			      .RS_out_dest(RS_MEM_dest),
			      .RS_out_RON(RS_MEM_ROB),
			      .RS_out_N_of_Ready(RS_Arbiter_in_N_of_Ready[7:6]),
			      .RS_full(RS_full[3])
   			    );

   rps_8 RS_Arbiter(.req(RS_Arbiter_in_N_of_Ready), .en(PRF_RS_N_of_Issue), .count(counter), .gnt(RS_Arbiter_out_N_of_Issue))


     always_ff@(posedge clock)
       begin
	  if(reset)
	    counter <= `SD 0;
	  else
	    counter <= `SD counter + 1;
       end

   
endmodule // Top_RS
