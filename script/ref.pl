#!/usr/bin/env perl
use strict;
use warnings;

open FILER, '< MonteCarlo.200.Sep14.csv' or die $!;        #read file
open FILEW, '> grep.txt' or die $!;			   #write file

my $string1 = "riseDelay800m";
my $string2 = "rise2Delay800m";
my @newf = grep /$string1|$string2/, <FILER>;
print FILEW "@newf";

close FILER;						   
close FILEW;						   # close files

###################################################################


open FILER, '< grep.txt' or die $!;			   #read file
open FILEW, '> test2.txt' or die $!;			   #write file

my $save2 = 0;
my $num = 1;
my $errcnt = 0;

while (<FILER>) {

 my @name = split(/,/ );

 if ($num%2){ 						    #odd lines
	if($name[3] eq "eval err"){
		$save2 = 1;
	} 
        elsif($name[3]>0 && $name[3]<2.5e-9) {
 		print FILEW "$name[3]\n";
		$save2 = 0;
	  }
	elsif ($name[3]<0) {
		$save2 = 1;
	}
	else {
		$errcnt++;
		$save2 = 0;
	}
 }
 else {							    #even lines
	if($save2){
		if($name[3] eq "eval err"){
			$errcnt++;
		} 
 		else {
			print FILEW "$name[3]\n";
		}
	}
 }

 $num++;
}

print FILEW "err_cnt is $errcnt";

close FILER;						   
close FILEW;						   # close files
