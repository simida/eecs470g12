#!/usr/bin/env perl
use strict;
use warnings;

open FILEW, '> grep.txt' or die $!;			   #write file

my $dir='./EBR';
foreach my $fp (glob("$dir/*")){
	my @filename=split(/_/,$fp);
	my @filename2=split(/EBR/,$filename[0]);
	print FILEW "$filename2[1]\t\t\t";
	open my $fh, "<",$fp or die "can't read open '$fp'";
	my $dist_t=0;
	my $cnt=0;
	my $result=0;
	while(<$fh>){
		my @name = split(/=/ );
		$dist_t = $dist_t + $name[3];
		$cnt++;
		
	}
	$result=$dist_t/$cnt;
	print FILEW "$result\n";
	close $fh or die "can't close '$fp'";
}
