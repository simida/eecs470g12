#!/usr/bin/env perl
use strict;
use warnings;

open FILEW, '> MSHR.txt' or die $!;			   #write file

my $dir='./MSHR';
print FILEW "testcase\t#nonblocking saved cycles\n";
foreach my $fp (glob("$dir/*")){
	my @filename=split(/MSHR/,$fp);
	my @filename2=split(/_mshr/,$filename[1]);
	print FILEW "$filename2[0]\t";
	open my $fh, "<",$fp or die "can't read open '$fp'";
	my $mshr_val_cnt=0;
	my $inst_cnt=0;
	my $result=0;
	while(<$fh>){
		my @name = split(/=/ );
		$mshr_val_cnt = $name[1];
		$inst_cnt = $name[3];
		
	}
	if($inst_cnt!=0){
		$result=$mshr_val_cnt/$inst_cnt;
	}
	else {
		$result=0;
	}
	print FILEW "$result\n";
	close $fh or die "can't close '$fp'";
}
