/////////////////////////////////////////////////////////////////////////
//                                                                     //
//                                                                     //
//   Modulename :  testbench.v                                         //
//                                                                     //
//  Description :  Testbench module for the verisimple pipeline;       //
//                                                                     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////


module testbench;

  // Registers and wires used in the testbench
  logic        clock;
  logic        reset;
  logic [31:0] clock_count;
  logic [31:0] instr_count;
  logic [31:0] RS_ALU_count;
  logic [31:0] RS_MULT_count;
  logic [31:0] RS_BR_count;
  logic [31:0] RS_MEM_count;
  logic [31:0] ROB_count;
  logic [31:0] BRAT_count;
  logic [31:0] cdb_stall_count;
  logic [31:0] branch_count;
  logic [31:0] branch_mis_count;
  int          wb_fileno;
  int          br_fileno;
  int          lq_fileno;
  int          mshr_fileno;
  int          dcache_fileno;
  int          icache_fileno;

  int mshr_valcmd_count;
  int mshr_valcmd_count_all;
  int rd_dmem_cnt;
  int vicdcache_hit_cnt;
  int dcache_hit_cnt;

  int rd_imem_cnt ;
  int vicicache_hit_cnt;
  int icache_hit_cnt;
  int pref_icache_hit_cnt;

  logic  [1:0] proc2mem_command;
  logic [63:0] proc2mem_addr;
  logic [63:0] proc2mem_data;
  logic  [3:0] mem2proc_response;
  logic [63:0] mem2proc_data;
  logic  [3:0] mem2proc_tag;

  logic  [3:0] pipeline_completed_insts;
  logic  [3:0] pipeline_error_status;
  logic [1:0] [4:0] pipeline_commit_wr_idx;
  logic [1:0][63:0] pipeline_commit_wr_data;
  logic [1:0]       pipeline_commit_wr_en;
  logic [1:0][63:0] pipeline_commit_NPC;

logic												D1_LSQ_DC_rd_mem;
logic												D1_LSQ_DC_rd_memreg;
logic												D1_LSQ_DC_wr_mem;
logic	 [63:0]								D1_LSQ_DC_addr;
logic	 [63:0]								D1_LSQ_DC_NPC;
logic	 [31:0]								D1_LSQ_DC_IR;
logic	 [`BRAT_SIZE_LOG:0]		D1_LSQ_DC_BRAT_num;
logic	 [`BRAT_SIZE_LOG:0]		D1_LSQ_DC_BRAT_numreg;
logic	 [`ROB_SIZE_LOG-1:0]	D1_LSQ_DC_ROB_num;
logic	 [`PRF_SIZE_LOG-1:0]	D1_LSQ_DC_PRF_num;
//logic												D1_DC_Dcache_wr1_enable;
logic  [63:0]								D1_LSQ_DC_wr_value;

logic	[63:0]			 next_Dcache_LSQ_data;
logic	[63:0]			 next_Dcache_LSQ_rd_addr; 
logic							 next_Dcache_LSQ_rd_valid	;
logic	[`BRAT_SIZE_LOG:0]	next_LSQ_DC_BRAT_num;
logic [`BRAT_SIZE_LOG:0]	next_D1_LSQ_DC_BRAT_num;
logic											next_D1_LSQ_DC_rd_mem;
logic 										next_D1_LSQ_DC_wr_mem;
logic	[`ROB_SIZE_LOG-1:0]	  next_LSQ_DC_ROB_num;
logic	[`PRF_SIZE_LOG-1:0]	  next_LSQ_DC_PRF_num;

//prefetch memory
logic [`MEM_LATENCY_IN_CYCLES_P1+1:0]	MSHR_regfl_shifter;
logic [`MEM_LATENCY_IN_CYCLES_P1+1:0]	next_MSHR_regfl_shifter;

//logic	[63:0]				next_DC_Dmem_addr;
//logic [1:0]					next_DC_Dmem_command;
//logic [63:0]				next_DC_Dmem_data;

logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_out_entry;
logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_in_entry;

logic [`MSHR_SIZE_LOG-1:0]		MSHR_count;
logic [`MSHR_SIZE_LOG-1:0]		next_MSHR_count;
logic  				Dcache_LSQ_rd_validreg;
logic [`BRAT_SIZE_LOG:0] 		Dcache_LSQ_BRAT_numreg;


logic  Dmem_out;
logic  Dcache_out;
logic  LD_BUF_in;
logic  LD_BUF_out;
logic[63:0]			 next_Dcache_LSQ_rd_NPC; 
logic[31:0]			 next_Dcache_LSQ_rd_IR; 


logic										     LD_BUF_empty;
		 
   //logic to Dcache Controller
logic 				     LSQ_DC_rd_mem;
logic 				     LSQ_DC_wr_mem;
logic [63:0] 			     LSQ_DC_addr;
logic [`PRF_SIZE_LOG-1:0] 		     LSQ_DC_PRF_num;
logic [`BRAT_SIZE_LOG:0] 		     LSQ_DC_BRAT_num;
logic [`ROB_SIZE_LOG-1:0] 		     LSQ_DC_ROB_num;
logic [`PRF_SIZE_LOG-1:0] 		     LSQ_DC_regA;
logic [63:0] 			     LSQ_DC_NPC;
logic [31:0] 			     LSQ_DC_IR;

logic [63:0] 			     Dcache_data;
logic 				     Dcache_valid;
wire  [`BRAT_SIZE:0]		     	     BRAT_squash_aid;
logic [1:0]				     BRAT_CMT_v;
logic [1:0] [`BRAT_SIZE_LOG-1:0]	     BRAT_CMT_num;


  // Instantiate the Pipeline
  pipeline pipeline (// Inputs
                       .clock             (clock),
                       .reset             (reset),
                       .mem2proc_response (mem2proc_response),
                       .mem2proc_data     (mem2proc_data),
                       .mem2proc_tag      (mem2proc_tag),

                        // Outputs
                       .proc2mem_command  (proc2mem_command),
                       .proc2mem_addr     (proc2mem_addr),
                       .proc2mem_data     (proc2mem_data),

                       .pipeline_completed_insts(pipeline_completed_insts),
                       .pipeline_error_status(pipeline_error_status),
                       .pipeline_commit_wr_data(pipeline_commit_wr_data),
                       .pipeline_commit_wr_idx(pipeline_commit_wr_idx),
                       .pipeline_commit_wr_en(pipeline_commit_wr_en),
                       .pipeline_commit_NPC(pipeline_commit_NPC),

.D1_LSQ_DC_rd_mem(D1_LSQ_DC_rd_mem),
.D1_LSQ_DC_rd_memreg(D1_LSQ_DC_rd_memreg),
.D1_LSQ_DC_wr_mem(D1_LSQ_DC_wr_mem),
.D1_LSQ_DC_addr(D1_LSQ_DC_addr),
.D1_LSQ_DC_NPC(D1_LSQ_DC_NPC),
.D1_LSQ_DC_IR(D1_LSQ_DC_IR),
.D1_LSQ_DC_BRAT_num(D1_LSQ_DC_BRAT_num),
.D1_LSQ_DC_BRAT_numreg(D1_LSQ_DC_BRAT_numreg),
.D1_LSQ_DC_ROB_num(D1_LSQ_DC_ROB_num),
.D1_LSQ_DC_PRF_num(D1_LSQ_DC_PRF_num),
//.D1_DC_Dcache_wr1_enable(D1_DC_Dcache_wr1_enable),
.D1_LSQ_DC_wr_value(D1_LSQ_DC_wr_value),

.next_Dcache_LSQ_data,
.next_Dcache_LSQ_rd_addr, 
.next_Dcache_LSQ_rd_valid	,
.next_LSQ_DC_BRAT_num,
.next_D1_LSQ_DC_BRAT_num,
.next_D1_LSQ_DC_rd_mem,
.next_D1_LSQ_DC_wr_mem,
.next_LSQ_DC_ROB_num,
.next_LSQ_DC_PRF_num,

				//prefetch memory
.MSHR_regfl_shifter,
.next_MSHR_regfl_shifter,

//.next_DC_Dmem_addr,
//.next_DC_Dmem_command,
//.next_DC_Dmem_data,

.LD_BUF_out_entry,
.LD_BUF_in_entry,

.MSHR_count,
.next_MSHR_count,
.Dcache_LSQ_rd_validreg,
.Dcache_LSQ_BRAT_numreg,


.Dmem_out,
.Dcache_out,
.LD_BUF_in,
.LD_BUF_out,
.next_Dcache_LSQ_rd_NPC, 
.next_Dcache_LSQ_rd_IR, 


.LD_BUF_empty,

   //logic to Dcache Controller
.LSQ_DC_rd_mem,
.LSQ_DC_wr_mem,
.LSQ_DC_addr,
.LSQ_DC_PRF_num,
.LSQ_DC_BRAT_num,
.LSQ_DC_ROB_num,
.LSQ_DC_regA,
.LSQ_DC_NPC,
.LSQ_DC_IR,

.Dcache_data,
.Dcache_valid,
.BRAT_squash_aid,
.BRAT_CMT_v,
.BRAT_CMT_num

                      );


  // Instantiate the Data Memory
  mem memory (// Inputs
            .clk               (clock),
            .proc2mem_command  (proc2mem_command),
            .proc2mem_addr     (proc2mem_addr),
            .proc2mem_data     (proc2mem_data),

             // Outputs

            .mem2proc_response (mem2proc_response),
            .mem2proc_data     (mem2proc_data),
            .mem2proc_tag      (mem2proc_tag)
           );

  // Generate System Clock
  always
  begin
    #(`VERILOG_CLOCK_PERIOD/2.0);
    clock = ~clock;
  end

  // Task to display # of elapsed clock edges
  task show_clk_count;
          real cpi;
    int instr_cnt;
          begin
    	instr_cnt = instr_count + pipeline_completed_insts - 1;
       		cpi = (clock_count + 1.0) / instr_cnt;
       		$display("@@  %0d cycles / %0d instrs = %f CPI\n@@",
          	clock_count+1, instr_cnt, cpi);
             	$display("@@  %4.2f ns total time to execute\n@@\n",
                      	clock_count*`VIRTUAL_CLOCK_PERIOD);
          end
          
  endtask  // task show_clk_count 

  // Task to display # of elapsed clock edges
  task show_branch_count;
        real misrate;
        begin
     		misrate = branch_mis_count *100 / branch_count;
     		$display("@@  %0d misses / %0d branchs = %2.1f%% Miss Rate\n@@",
        	branch_mis_count, branch_count, misrate);
        end
        
  endtask  // task show_clk_count 

  // Task to display # of elapsed clock edges
  task show_full_count;
        begin
     		$display("@@  %0d RS_ALU_full @@", RS_ALU_count);
     		$display("@@  %0d RS_BR_full @@", RS_BR_count);
     		$display("@@  %0d RS_MULT_full @@", RS_MULT_count);
     		$display("@@  %0d RS_MEM_full @@", RS_MEM_count);
     		$display("@@  %0d ROB_full @@", ROB_count);
     		$display("@@  %0d BRAT_full @@", BRAT_count);
     		$display("@@  %0d cdb_stall @@", cdb_stall_count);

        end
        
  endtask  // task show_clk_count 

  // Show contents of a range of Unified Memory, in both hex and decimal
  task show_mem_with_decimal;
   input [31:0] start_addr;
   input [31:0] end_addr;
   int showing_data;
   begin
    $display("@@@");
    showing_data=0;
    for(int k=start_addr;k<=end_addr; k=k+1)
      if (memory.unified_memory[k] != 0)
      begin
        $display("@@@ mem[%5d] = %x : %0d", k*8, memory.unified_memory[k], 
                                                 memory.unified_memory[k]);
        showing_data=1;
      end
      else if(showing_data!=0)
      begin
        $display("@@@");
        showing_data=0;
      end
    $display("@@@");
   end
  endtask  // task show_mem_with_decimal

  initial
  begin
    `ifdef DUMP
      $vcdplusdeltacycleon;
      $vcdpluson();
      $vcdplusmemon(memory.unified_memory);
    `endif
      
    clock = 1'b0;
    reset = 1'b0;

    // Pulse the reset signal
    $display("@@\n@@\n@@  %t  Asserting System reset......", $realtime);
    reset = 1'b1;
    @(posedge clock);
    @(posedge clock);

    $readmemh("program.mem", memory.unified_memory);

    @(posedge clock);
    @(posedge clock);
    `SD;
    // This reset is at an odd time to avoid the pos & neg clock edges

    reset = 1'b0;
    $display("@@  %t  Deasserting System reset......\n@@\n@@", $realtime);

    wb_fileno = $fopen("writeback.out");
    br_fileno = $fopen("earlybranch.out");
    lq_fileno = $fopen("loadqueue.out");
 	mshr_fileno = $fopen("mshr.out");
	dcache_fileno = $fopen("dcache.out");
	icache_fileno = $fopen("icache.out");
    
  end


  // Count the number of posedges and number of instructions completed
  // till simulation ends
  always @(posedge clock or posedge reset)
  begin
    if(reset)
    begin
      clock_count <= `SD 0;
      instr_count <= `SD 0;

    end
    else
    begin
      clock_count <= `SD (clock_count + 1);
      instr_count <= `SD (instr_count + pipeline_completed_insts);
    end
  end  



  // Count the number of posedges and number of instructions completed
  // till simulation ends
  always @(posedge clock)
  begin
  	if (Complete_Retire_Stage.next_BRAT_branch_miss[0])
		$fdisplay(br_fileno, "ROB#=%2d, ROB_head=%2d, Distance=%2d", pipeline.EX_CMP_ROB[0], pipeline.ROB_head, pipeline.EX_CMP_ROB[0]-pipeline.ROB_head );   
  	if (Complete_Retire_Stage.next_BRAT_branch_miss[1])
		$fdisplay(br_fileno, "ROB#=%2d, ROB_head=%2d, Distance=%2d", pipeline.EX_CMP_ROB[1], pipeline.ROB_head, pipeline.EX_CMP_ROB[1]-pipeline.ROB_head ); 	
  	if (pipeline.LSQ.lq1.LQ_PRF_valid)
		$fdisplay(lq_fileno, "ROB#=%2d, ROB_head=%2d, Distance=%2d", pipeline.LSQ.lq1.LQ_PRF_ROB, pipeline.ROB_head, pipeline.LSQ.lq1.LQ_PRF_ROB-pipeline.ROB_head );   
  	if (pipeline.LSQ.lq1.LQ_Dcache_valid)
		$fdisplay(lq_fileno, "ROB#=%2d, ROB_head=%2d, Distance=%2d", pipeline.LSQ.lq1.LQ_Dcache_ROB, pipeline.ROB_head, pipeline.LSQ.lq1.LQ_Dcache_ROB-pipeline.ROB_head );   			    
  end

  // Count the number of MSHR valid command (for load)
  // till simulation ends
  always @(posedge clock)
  begin
		if(reset) begin
			mshr_valcmd_count_all = 0;
		end
		else begin
			mshr_valcmd_count=0;

			for(int p=0;p<16;p++) begin
				mshr_valcmd_count = mshr_valcmd_count + pipeline.Dcache.MSHR_regfl_valid_commandreg[p];
			end 
			if(mshr_valcmd_count>1) begin
				mshr_valcmd_count_all=mshr_valcmd_count_all+mshr_valcmd_count-1;
			end
		end   			    
  end

  // Count the number of Dcache cache hit (for load) (total + victim )
  // till simulation ends
  always @(posedge clock)
  begin
		if(reset) begin
			rd_dmem_cnt = 0;
			vicdcache_hit_cnt=0;
			dcache_hit_cnt=0;
		end
		else begin
			if(pipeline.LSQ_DC_rd_mem) begin
				rd_dmem_cnt++;
			end
			if(pipeline.DUT_dcachemem.vic_cache_valid)begin
				vicdcache_hit_cnt++;
			end
			if(pipeline.DUT_dcachemem.rd_valid_t)begin
				dcache_hit_cnt++;
			end
		end   			    
  end

  // Count the number of Icache cache hit (total + victim )
  // till simulation ends
  always @(posedge clock)
  begin
		if(reset) begin
			rd_imem_cnt = 0;
			vicicache_hit_cnt=0;
			icache_hit_cnt=0;
			pref_icache_hit_cnt=0;
		end
		else begin
			if(pipeline.FrontEnd_0.icache_0.changed_addr) begin
				rd_imem_cnt++;
			end
			if(pipeline.FrontEnd_0.cachememory.vic_cache_valid)begin
				vicicache_hit_cnt++;
			end
			if(pipeline.FrontEnd_0.icache_0.changed_addr && pipeline.FrontEnd_0.cachememory.rd_valid_t)begin
				icache_hit_cnt++;
			end
			if(pipeline.FrontEnd_0.icache_0.changed_addr && !pipeline.FrontEnd_0.icache_0.IB_miss)begin
				pref_icache_hit_cnt++;
			end
		end   			    
  end


  // Count the number of posedges and number of instructions completed
  // till simulation ends
  always @(posedge clock or posedge reset)
  begin
    if(reset)
    begin
 	 RS_ALU_count 		<= `SD 0;
	 RS_MULT_count 		<= `SD 0;
	 RS_BR_count 		<= `SD 0;
	 RS_MEM_count 		<= `SD 0;
	 ROB_count 		<= `SD 0;
	 BRAT_count 		<= `SD 0;
	 cdb_stall_count 	<= `SD 0;
    end
    else
    begin
 	 RS_ALU_count 		<= `SD RS_ALU_count + pipeline.RS_full[0][0]+ pipeline.RS_full[0][1];
	 RS_MULT_count 		<= `SD RS_MULT_count + pipeline.RS_full[2][0]+ pipeline.RS_full[2][1];
	 RS_BR_count 		<= `SD RS_BR_count + pipeline.RS_full[1][0]+ pipeline.RS_full[1][1];
	 RS_MEM_count 		<= `SD RS_MEM_count + pipeline.RS_full[3][0]+ pipeline.RS_full[3][1];
	 ROB_count 		<= `SD ROB_count + pipeline.ROB_full + pipeline.ROB_full_1left;
	 BRAT_count 		<= `SD BRAT_count + pipeline.BRAT_full + pipeline.BRAT_full_1left;
	 cdb_stall_count 	<= `SD cdb_stall_count + pipeline.EX_IS_stall[0][0] + pipeline.EX_IS_stall[0][1] + pipeline.EX_IS_stall[1][0] + pipeline.EX_IS_stall[1][1]
				    + pipeline.EX_IS_stall[2][0] + pipeline.EX_IS_stall[2][1] + pipeline.EX_IS_stall[3][0] + pipeline.EX_IS_stall[3][1];				    
    end
  end  
 
  logic [1:0] branch_mis;

  assign branch_mis[0] = pipeline.BRAT_CMT_v[0] && pipeline.Complete_Retire_Stage.brat_mis_regfl[pipeline.BRAT_CMT_num[0]];
  assign branch_mis[1] = pipeline.BRAT_CMT_v[1] && pipeline.Complete_Retire_Stage.brat_mis_regfl[pipeline.BRAT_CMT_num[1]];


  // Count the number of posedges and number of instructions completed
  // till simulation ends
  always @(posedge clock or posedge reset)
  begin
    if(reset)
    begin
      branch_count <= `SD 0;
      branch_mis_count <= `SD 0;
    end
    else
    begin
      branch_count <= `SD (branch_count + pipeline.BRAT_CMT_v[0] + pipeline.BRAT_CMT_v[1]);
      branch_mis_count <= `SD (branch_mis_count + branch_mis[0]+ branch_mis[1]);
    end
  end  




  always @(negedge clock)
  begin
    if(reset)
      $display("@@\n@@  %t : System STILL at reset, can't show anything\n@@",
               $realtime);
    else
    begin
      `SD;

       // print the writeback information to writeback.out
       if(pipeline_completed_insts>0) begin
         if(pipeline_commit_wr_en[0]) begin
	   if (pipeline_commit_wr_idx[0]!=`ZERO_REG)
		 $fdisplay(wb_fileno, "PC=%x, REG[%d]=%x",
		             pipeline_commit_NPC[0]-4,
		             pipeline_commit_wr_idx[0],
		             pipeline_commit_wr_data[0]);

	   else 
         	 $fdisplay(wb_fileno, "PC=%x, ---",pipeline_commit_NPC[0]-4);
	 end
         if(pipeline_commit_wr_en[1]) begin
	   if (pipeline_commit_wr_idx[1]!=`ZERO_REG)
		 $fdisplay(wb_fileno, "PC=%x, REG[%d]=%x",
		             pipeline_commit_NPC[1]-4,
		             pipeline_commit_wr_idx[1],
		             pipeline_commit_wr_data[1]);
	   else 
         	 $fdisplay(wb_fileno, "PC=%x, ---",pipeline_commit_NPC[1]-4);
	 end
      end

      // deal with any halting conditions
      if(pipeline_error_status!=`NO_ERROR)
      begin
        $display("@@@ Unified Memory contents hex on left, decimal on right: ");
        show_mem_with_decimal(0,`MEM_64BIT_LINES - 1); 
          // 8Bytes per line, 16kB total

        $display("@@  %t : System halted\n@@", $realtime);

        case(pipeline_error_status)
          `HALTED_ON_MEMORY_ERROR:  
              $display("@@@ System halted on memory error");
          `HALTED_ON_HALT:          
              $display("@@@ System halted on HALT instruction");
          `HALTED_ON_ILLEGAL:
              $display("@@@ System halted on illegal instruction");
          default: 
              $display("@@@ System halted on unknown error code %x",
                       pipeline_error_status);
        endcase
        $display("@@@\n@@");
        show_clk_count;
        show_branch_count;
		show_full_count;
		
		$fdisplay(mshr_fileno, "mshr_valid = %d = instr_count = %d",mshr_valcmd_count_all,instr_count);
		$fdisplay(dcache_fileno, "vicdcache_hit = %d = dcache_hit_cnt = %d =  rd_dmem_count = %d",vicdcache_hit_cnt,dcache_hit_cnt,rd_dmem_cnt);
		$fdisplay(icache_fileno, "vicicache_hit = %d = icache_hit_cnt = %d =  pref_icache_hit = %d = rd_dmem_count = %d",vicicache_hit_cnt,icache_hit_cnt, pref_icache_hit_cnt,rd_imem_cnt);

        $fclose(wb_fileno);
		$fclose(mshr_fileno);
		$fclose(dcache_fileno);
		$fclose(icache_fileno);
        #100 $finish;
      end

    end  // if(reset)
  end 

endmodule  // module testbench

