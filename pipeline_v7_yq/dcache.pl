#!/usr/bin/env perl
use strict;
use warnings;

open FILEW, '> dcache.txt' or die $!;			   #write file

my $dir='./DCACHE';
print FILEW "testcase\t#vic_hitrate\t#dcache_hitrate_total\n";
foreach my $fp (glob("$dir/*")){
	my @filename=split(/DCACHE/,$fp);
	my @filename2=split(/_dcache/,$filename[1]);
	print FILEW "$filename2[0]\t";
	open my $fh, "<",$fp or die "can't read open '$fp'";
	my $vic_cnt=0;
	my $dcachet_cnt=0;
	my $read_cnt=0;
	my $vic_result=0;
	my $dcachet_result=0;
	while(<$fh>){
		my @name = split(/=/ );
		$vic_cnt = $name[1];
		$dcachet_cnt = $name[3];
		$read_cnt = $name[5];
		
	}
	if($read_cnt!=0){
		$vic_result=$vic_cnt/$read_cnt;
		$dcachet_result=$dcachet_cnt/$read_cnt;
	}

	print FILEW "$vic_result\t$dcachet_result\n";
	close $fh or die "can't close '$fp'";
}
