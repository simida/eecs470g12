#!/usr/bin/env perl
use strict;
use warnings;

open FILEW, '> EBR.txt' or die $!;			   #write file

my $dir='./EBR';
print FILEW "testcase\t#ROB entry erased\n";
foreach my $fp (glob("$dir/*")){
	my @filename=split(/EBR/,$fp);
	my @filename2=split(/_earlybranch/,$filename[1]);
	print FILEW "$filename2[0]\t";
	open my $fh, "<",$fp or die "can't read open '$fp'";
	my $dist_t=0;
	my $cnt=0;
	my $result=0;
	while(<$fh>){
		my @name = split(/=/ );
		$dist_t = $dist_t + $name[3];
		$cnt++;
		
	}
	if($cnt){
		$result=$dist_t/$cnt;
	}
	else {
		$result=0;
	}
	print FILEW "$result\n";
	close $fh or die "can't close '$fp'";
}
