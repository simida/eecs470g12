#!/usr/bin/env perl
use strict;
use warnings;

open FILEW, '> icache.txt' or die $!;			   #write file

my $dir='./ICACHE';
print FILEW "testcase\tvic_hitrate\ticache_hitrate_total\tpref_hitrate\n";
foreach my $fp (glob("$dir/*")){
	my @filename=split(/ICACHE/,$fp);
	my @filename2=split(/_icache/,$filename[1]);
	print FILEW "$filename2[0]\t";
	open my $fh, "<",$fp or die "can't read open '$fp'";
	my $vic_cnt=0;
	my $icachet_cnt=0;
	my $pref_cnt=0;
	my $read_cnt=0;
	my $vic_result=0;
	my $icachet_result=0;
	my $pref_result=0;
	while(<$fh>){
		my @name = split(/=/ );
		$vic_cnt = $name[1];
		$icachet_cnt = $name[3];
		$pref_cnt = $name[5];
		$read_cnt = $name[7];
		
	}
	if($read_cnt!=0){
		$vic_result=$vic_cnt/$read_cnt;
		$icachet_result=$icachet_cnt/$read_cnt;
		$pref_result=$pref_cnt/$read_cnt;
	}

	print FILEW "$vic_result\t$icachet_result\t$pref_result\n";
	close $fh or die "can't close '$fp'";
}
