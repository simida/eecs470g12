#!/usr/bin/env perl
use strict;
use warnings;

open FILEW, '> load.txt' or die $!;			   #write file

my $dir='./LOAD';
print FILEW "testcase\t#load_ROB_saved\n";
foreach my $fp (glob("$dir/*")){
	my @filename=split(/LOAD/,$fp);
	my @filename2=split(/_load/,$filename[1]);
	print FILEW "$filename2[0]\t";
	open my $fh, "<",$fp or die "can't read open '$fp'";
	my $dist_t=0;
	my $cnt=0;
	my $result=0;
	while(<$fh>){
		my @name = split(/=/ );
		$dist_t = $dist_t + $name[3];
		$cnt++;
		
	}
	if($cnt!=0){
		$result=$dist_t/$cnt;
	}

	print FILEW "$result\n";
	close $fh or die "can't close '$fp'";
}
