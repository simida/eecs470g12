# Begin_DVE_Session_Save_Info
# DVE full session
# Saved on Sun Dec 1 19:30:16 2013
# Designs open: 1
#   Sim: /afs/umich.edu/user/z/h/zhyiqun/Desktop/EECS470/eecs470g12/pipeline_v5/dve_simv
# Toplevel windows open: 1
# 	TopLevel.1
#   Source.1: NoO_16
#   Group count = 1
#   Group pipeline signal count = 170
# End_DVE_Session_Save_Info

# DVE version: E-2011.03_Full64
# DVE build date: Feb 23 2011 21:10:05


#<Session mode="Full" path="/afs/umich.edu/user/z/h/zhyiqun/Desktop/EECS470/eecs470g12/pipeline_v5/DVEfiles/session.tcl" type="Debug">

gui_set_loading_session_type Post
gui_continuetime_set

# Close design
if { [gui_sim_state -check active] } {
    gui_sim_terminate
}
gui_close_db -all
gui_expr_clear_all

# Close all windows
gui_close_window -type Console
gui_close_window -type Wave
gui_close_window -type Source
gui_close_window -type Schematic
gui_close_window -type Data
gui_close_window -type DriverLoad
gui_close_window -type List
gui_close_window -type Memory
gui_close_window -type HSPane
gui_close_window -type DLPane
gui_close_window -type Assertion
gui_close_window -type CovHier
gui_close_window -type CoverageTable
gui_close_window -type CoverageMap
gui_close_window -type CovDensity
gui_close_window -type CovDetail
gui_close_window -type Local
gui_close_window -type Stack
gui_close_window -type Watch
gui_close_window -type Grading
gui_close_window -type Group
gui_close_window -type Transaction



# Application preferences
gui_set_pref_value -key app_default_font -value {Helvetica,10,-1,5,50,0,0,0,0,0}
gui_src_preferences -tabstop 8 -maxbits 24 -windownumber 1
#<WindowLayout>

# DVE Topleve session: 


# Create and position top-level windows :TopLevel.1

if {![gui_exist_window -window TopLevel.1]} {
    set TopLevel.1 [ gui_create_window -type TopLevel \
       -icon $::env(DVE)/auxx/gui/images/toolbars/dvewin.xpm] 
} else { 
    set TopLevel.1 TopLevel.1
}
gui_show_window -window ${TopLevel.1} -show_state normal -rect {{27 69} {2515 1408}}

# ToolBar settings
gui_set_toolbar_attributes -toolbar {TimeOperations} -dock_state top
gui_set_toolbar_attributes -toolbar {TimeOperations} -offset 0
gui_show_toolbar -toolbar {TimeOperations}
gui_set_toolbar_attributes -toolbar {&File} -dock_state top
gui_set_toolbar_attributes -toolbar {&File} -offset 0
gui_show_toolbar -toolbar {&File}
gui_set_toolbar_attributes -toolbar {&Edit} -dock_state top
gui_set_toolbar_attributes -toolbar {&Edit} -offset 0
gui_show_toolbar -toolbar {&Edit}
gui_set_toolbar_attributes -toolbar {Simulator} -dock_state top
gui_set_toolbar_attributes -toolbar {Simulator} -offset 0
gui_show_toolbar -toolbar {Simulator}
gui_set_toolbar_attributes -toolbar {Interactive Rewind} -dock_state top
gui_set_toolbar_attributes -toolbar {Interactive Rewind} -offset 0
gui_show_toolbar -toolbar {Interactive Rewind}
gui_set_toolbar_attributes -toolbar {Signal} -dock_state top
gui_set_toolbar_attributes -toolbar {Signal} -offset 0
gui_show_toolbar -toolbar {Signal}
gui_set_toolbar_attributes -toolbar {&Scope} -dock_state top
gui_set_toolbar_attributes -toolbar {&Scope} -offset 0
gui_show_toolbar -toolbar {&Scope}
gui_set_toolbar_attributes -toolbar {&Trace} -dock_state top
gui_set_toolbar_attributes -toolbar {&Trace} -offset 0
gui_show_toolbar -toolbar {&Trace}
gui_set_toolbar_attributes -toolbar {BackTrace} -dock_state top
gui_set_toolbar_attributes -toolbar {BackTrace} -offset 0
gui_show_toolbar -toolbar {BackTrace}
gui_set_toolbar_attributes -toolbar {&Window} -dock_state top
gui_set_toolbar_attributes -toolbar {&Window} -offset 0
gui_show_toolbar -toolbar {&Window}
gui_set_toolbar_attributes -toolbar {Zoom} -dock_state top
gui_set_toolbar_attributes -toolbar {Zoom} -offset 0
gui_show_toolbar -toolbar {Zoom}
gui_set_toolbar_attributes -toolbar {Zoom And Pan History} -dock_state top
gui_set_toolbar_attributes -toolbar {Zoom And Pan History} -offset 0
gui_show_toolbar -toolbar {Zoom And Pan History}
gui_set_toolbar_attributes -toolbar {Grid} -dock_state top
gui_set_toolbar_attributes -toolbar {Grid} -offset 0
gui_show_toolbar -toolbar {Grid}

# End ToolBar settings

# Docked window settings
set HSPane.1 [gui_create_window -type HSPane -parent ${TopLevel.1} -dock_state left -dock_on_new_line true -dock_extent 186]
set Hier.1 [gui_share_window -id ${HSPane.1} -type Hier]
gui_set_window_pref_key -window ${HSPane.1} -key dock_width -value_type integer -value 186
gui_set_window_pref_key -window ${HSPane.1} -key dock_height -value_type integer -value -1
gui_set_window_pref_key -window ${HSPane.1} -key dock_offset -value_type integer -value 0
gui_update_layout -id ${HSPane.1} {{left 0} {top 0} {width 185} {height 1077} {dock_state left} {dock_on_new_line true} {child_hier_colhier 245} {child_hier_coltype 96} {child_hier_col1 0} {child_hier_col2 1}}
set DLPane.1 [gui_create_window -type DLPane -parent ${TopLevel.1} -dock_state left -dock_on_new_line true -dock_extent 471]
set Data.1 [gui_share_window -id ${DLPane.1} -type Data]
gui_set_window_pref_key -window ${DLPane.1} -key dock_width -value_type integer -value 471
gui_set_window_pref_key -window ${DLPane.1} -key dock_height -value_type integer -value 1077
gui_set_window_pref_key -window ${DLPane.1} -key dock_offset -value_type integer -value 0
gui_update_layout -id ${DLPane.1} {{left 0} {top 0} {width 470} {height 1077} {dock_state left} {dock_on_new_line true} {child_data_colvariable 297} {child_data_colvalue 53} {child_data_coltype 134} {child_data_col1 0} {child_data_col2 1} {child_data_col3 2}}
set Console.1 [gui_create_window -type Console -parent ${TopLevel.1} -dock_state bottom -dock_on_new_line true -dock_extent 174]
gui_set_window_pref_key -window ${Console.1} -key dock_width -value_type integer -value 2489
gui_set_window_pref_key -window ${Console.1} -key dock_height -value_type integer -value 174
gui_set_window_pref_key -window ${Console.1} -key dock_offset -value_type integer -value 0
gui_update_layout -id ${Console.1} {{left 0} {top 0} {width 2488} {height 173} {dock_state bottom} {dock_on_new_line true}}
#### Start - Readjusting docked view's offset / size
set dockAreaList { top left right bottom }
foreach dockArea $dockAreaList {
  set viewList [gui_ekki_get_window_ids -active_parent -dock_area $dockArea]
  foreach view $viewList {
      if {[lsearch -exact [gui_get_window_pref_keys -window $view] dock_width] != -1} {
        set dockWidth [gui_get_window_pref_value -window $view -key dock_width]
        set dockHeight [gui_get_window_pref_value -window $view -key dock_height]
        set offset [gui_get_window_pref_value -window $view -key dock_offset]
        if { [string equal "top" $dockArea] || [string equal "bottom" $dockArea]} {
          gui_set_window_attributes -window $view -dock_offset $offset -width $dockWidth
        } else {
          gui_set_window_attributes -window $view -dock_offset $offset -height $dockHeight
        }
      }
  }
}
#### End - Readjusting docked view's offset / size
gui_sync_global -id ${TopLevel.1} -option true

# MDI window settings
set Source.1 [gui_create_window -type {Source}  -parent ${TopLevel.1}]
gui_show_window -window ${Source.1} -show_state maximized
gui_update_layout -id ${Source.1} {{show_state maximized} {dock_state undocked} {dock_on_new_line false}}

# End MDI window settings

gui_set_env TOPLEVELS::TARGET_FRAME(Source) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(Schematic) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(PathSchematic) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(Wave) none
gui_set_env TOPLEVELS::TARGET_FRAME(List) none
gui_set_env TOPLEVELS::TARGET_FRAME(Memory) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(DriverLoad) none
gui_update_statusbar_target_frame ${TopLevel.1}

#</WindowLayout>

#<Database>

# DVE Open design session: 

if { [llength [lindex [gui_get_db -design Sim] 0]] == 0 } {
gui_set_env SIMSETUP::SIMARGS {{+vc +memcbk}}
gui_set_env SIMSETUP::SIMEXE {dve_simv}
gui_set_env SIMSETUP::ALLOW_POLL {0}
if { ![gui_is_db_opened -db {/afs/umich.edu/user/z/h/zhyiqun/Desktop/EECS470/eecs470g12/pipeline_v5/dve_simv}] } {
gui_sim_run Ucli -exe dve_simv -args {-ucligui  +vc +memcbk} -dir /afs/umich.edu/user/z/h/zhyiqun/Desktop/EECS470/eecs470g12/pipeline_v5 -nosource
}
}
if { ![gui_sim_state -check active] } {error "Simulator did not start correctly" error}
gui_set_precision 1s
gui_set_time_units 1s
#</Database>

# DVE Global setting session: 


# Global: Breakpoints

# Global: Bus

# Global: Expressions

# Global: Signal Time Shift

# Global: Signal Compare

# Global: Signal Groups
gui_load_child_values {testbench.pipeline}

set pipeline pipeline
gui_sg_create ${pipeline}
gui_sg_addsignal -group ${pipeline} { testbench.pipeline.DC_Dcache_wr2_tag testbench.pipeline.pipeline_commit_wr_data testbench.pipeline.IS_EX_dest testbench.pipeline.IS_EX_ROB testbench.pipeline.ID_IS_BRAT testbench.pipeline.ID_IS_IR testbench.pipeline.ID_IS_ROB testbench.pipeline.FE_ID_alu_func testbench.pipeline.BRAT_CMT_num testbench.pipeline.FE_ID_dest_reg testbench.pipeline.FE_prf_freed testbench.pipeline.proc2mem_addr testbench.pipeline.ID_IS_rs_valid testbench.pipeline.RET_arfn testbench.pipeline.IS_EX_valid testbench.pipeline.FE_ID_illegal testbench.pipeline.PRF_CDB_BRAT testbench.pipeline.Dcache_LSQ_rd_addr testbench.pipeline.DC_Dcache_wr1_enable testbench.pipeline.IS_EX_BRAT testbench.pipeline.PRF_CDB_NPC testbench.pipeline.ROB_tail testbench.pipeline.Dcache_CDB_NPC testbench.pipeline.FE_ID_opa_select testbench.pipeline.DC_Dcache_rd_idx testbench.pipeline.EX_CMP_branch_addr testbench.pipeline.pipeline_commit_NPC testbench.pipeline.Imem2proc_response testbench.pipeline.LSQ_DC_addr testbench.pipeline.mem2proc_data testbench.pipeline.EX_CMP_NPC testbench.pipeline.ROB_full testbench.pipeline.LQ_PRF_ROB testbench.pipeline.ID_IS_opa_sel testbench.pipeline.FE_ID_IR testbench.pipeline.IS_EX_opa_sel testbench.pipeline.LSQ_DC_wr_value testbench.pipeline.clock testbench.pipeline.LQ_PRF_IR testbench.pipeline.EX_CMP_dest testbench.pipeline.LQ_PRF_regA testbench.pipeline.FE_ID_valid_inst testbench.pipeline.BRAT_full_1left testbench.pipeline.PRF_CDB_value testbench.pipeline.reset testbench.pipeline.IS_EX_NPC testbench.pipeline.IS_EX_ld_or_st testbench.pipeline.BRAT_empty testbench.pipeline.LSQ_DC_PRF_num testbench.pipeline.LQ_PRF_valid testbench.pipeline.ID_IS_opb_sel testbench.pipeline.IS_EX_opb_sel testbench.pipeline.BRAT_CMT_takeornot testbench.pipeline.SQ_tail testbench.pipeline.EX_CMP_result testbench.pipeline.PRF_CDB_valid testbench.pipeline.IS_EX_ls_regA testbench.pipeline.DC_Dcache_wr1_data testbench.pipeline.ID_IS_NPC testbench.pipeline.FE_ID_uncond_branch testbench.pipeline.FE_ID_regA testbench.pipeline.EX_CMP_valid testbench.pipeline.FE_ID_regB testbench.pipeline.IS_EX_alu_func testbench.pipeline.Dmem2proc_response testbench.pipeline.EX_CMP_BRAT testbench.pipeline.SQ_CMP testbench.pipeline.mem2proc_response testbench.pipeline.SQ_full testbench.pipeline.FE_ID_rd_mem testbench.pipeline.DC_Dcache_rd_tag testbench.pipeline.FE_ID_wr_mem testbench.pipeline.RET_issue_store testbench.pipeline.LQ_PRF_BRAT testbench.pipeline.IS_ID_valid_list testbench.pipeline.FE_valid_list testbench.pipeline.IS_EX_regB_value testbench.pipeline.RET_illegal testbench.pipeline.ALU_LSQ_ld_or_st testbench.pipeline.ROB_head testbench.pipeline.FE_ID_cond_branch testbench.pipeline.ALU_LSQ_valid testbench.pipeline.ALU_LSQ_ROB testbench.pipeline.LQ_PRF_NPC testbench.pipeline.RET_prfn testbench.pipeline.pipeline_commit_wr_idx testbench.pipeline.EX_CMP_branch testbench.pipeline.ID_IS_uncn_br testbench.pipeline.FE_ID_BRAT testbench.pipeline.IS_EX_uncn_br testbench.pipeline.IS_EX_out_sq_num testbench.pipeline.MSHR_full testbench.pipeline.FE_predict_addr testbench.pipeline.proc2Imem_addr testbench.pipeline.ID_IS_ld_or_st testbench.pipeline.IS_EX_IR testbench.pipeline.FE_ID_dest_arfreg testbench.pipeline.BRAT_squash testbench.pipeline.LSQ_DC_BRAT_num testbench.pipeline.proc2Imem_command testbench.pipeline.LSQ_DC_ROB_num testbench.pipeline.BRAT_tail testbench.pipeline.ALU_LSQ_IR testbench.pipeline.RET_valid testbench.pipeline.ID_IS_alu_func testbench.pipeline.BRAT_CMT_pc testbench.pipeline.LSQ_DC_IR testbench.pipeline.LSQ_DC_regA testbench.pipeline.BRAT_full testbench.pipeline.BRAT_CMP_targetaddr testbench.pipeline.pipeline_error_status testbench.pipeline.LQ_full testbench.pipeline.Dcache_valid testbench.pipeline.LQ_PRF_forward_PRF testbench.pipeline.ALU_LSQ_NPC testbench.pipeline.proc2mem_data testbench.pipeline.DC_Dcache_wr1_idx testbench.pipeline.write_conflict testbench.pipeline.mem2proc_tag testbench.pipeline.IS_EX_regA_value testbench.pipeline.FE_ID_halt testbench.pipeline.Dcache_CDB_IR testbench.pipeline.EX_CMP_IR testbench.pipeline.BRAT_CMP_num }
gui_sg_addsignal -group ${pipeline} { testbench.pipeline.RET_free_enable testbench.pipeline.BRAT_CMT_targetaddr testbench.pipeline.EX_IS_stall testbench.pipeline.BRAT_CMT_v testbench.pipeline.Dcache_data testbench.pipeline.proc2mem_command testbench.pipeline.LSQ_DC_rd_mem testbench.pipeline.DC_Dcache_wr2_idx testbench.pipeline.LSQ_DC_wr_mem testbench.pipeline.ALU_LSQ_regA testbench.pipeline.pipeline_commit_wr_en testbench.pipeline.ID_IS_out_sq_num testbench.pipeline.PRF_CDB_IR testbench.pipeline.ALU_LSQ_SQ_addr testbench.pipeline.FE_predict_taken testbench.pipeline.FE_ID_NPC testbench.pipeline.Dcache_CDB_regA testbench.pipeline.LSQ_DC_NPC testbench.pipeline.FE_branch1 testbench.pipeline.FE_branch2 testbench.pipeline.DC_Dcache_wr2_enable testbench.pipeline.RS_full testbench.pipeline.BRAT_CMP_miss testbench.pipeline.LD_BUF_full testbench.pipeline.PRF_CDB_ROB testbench.pipeline.proc2Dmem_addr testbench.pipeline.DC_Dcache_wr2_data testbench.pipeline.FE_ID_opb_select testbench.pipeline.Dcache_CDB_ROB testbench.pipeline.Dcache_CDB_value testbench.pipeline.ID_IS_regA testbench.pipeline.DC_Dcache_wr1_tag testbench.pipeline.ID_IS_regB testbench.pipeline.pipeline_completed_insts testbench.pipeline.ID_IS_dest testbench.pipeline.ALU_LSQ_BRAT testbench.pipeline.ROB_full_1left testbench.pipeline.RET_halt testbench.pipeline.EX_CMP_ROB testbench.pipeline.proc2Dmem_command testbench.pipeline.ALU_LSQ_MEM_addr testbench.pipeline.BRAT_squash_aid testbench.pipeline.PRF_CDB_regA testbench.pipeline.Dcache_CDB_BRAT testbench.pipeline.Dcache_CDB_valid testbench.pipeline.IS_ID_Not_Erase }

# Global: Highlighting

# Post database loading setting...

# Restore C1 time
gui_set_time -C1_only 74



# Save global setting...

# Wave/List view global setting
gui_cov_show_value -switch false

# Close all empty TopLevel windows
foreach __top [gui_ekki_get_window_ids -type TopLevel] {
    if { [llength [gui_ekki_get_window_ids -parent $__top]] == 0} {
        gui_close_window -window $__top
    }
}
gui_set_loading_session_type noSession
# DVE View/pane content session: 


# Hier 'Hier.1'
gui_show_window -window ${Hier.1}
gui_list_set_filter -id ${Hier.1} -list { {Package 1} {All 1} {Process 1} {UnnamedProcess 1} {Function 1} {Block 1} {OVA Unit 1} {LeafScCell 1} {LeafVlgCell 1} {Interface 1} {LeafVhdCell 1} {$unit 1} {NamedBlock 1} {Task 1} {VlgPackage 1} {ClassDef 1} }
gui_list_set_filter -id ${Hier.1} -text {*}
gui_hier_list_init -id ${Hier.1}
gui_change_design -id ${Hier.1} -design Sim
catch {gui_list_expand -id ${Hier.1} testbench}
catch {gui_list_select -id ${Hier.1} {testbench.pipeline}}
gui_view_scroll -id ${Hier.1} -vertical -set 0
gui_view_scroll -id ${Hier.1} -horizontal -set 0

# Data 'Data.1'
gui_list_set_filter -id ${Data.1} -list { {Buffer 1} {Input 1} {Others 1} {Linkage 1} {Output 1} {Parameter 1} {All 1} {Aggregate 1} {Event 1} {Assertion 1} {Constant 1} {Interface 1} {Signal 1} {$unit 1} {Inout 1} {Variable 1} }
gui_list_set_filter -id ${Data.1} -text {*}
gui_list_show_data -id ${Data.1} {testbench.pipeline}
gui_view_scroll -id ${Data.1} -vertical -set 0
gui_view_scroll -id ${Data.1} -horizontal -set 0
gui_view_scroll -id ${Hier.1} -vertical -set 0
gui_view_scroll -id ${Hier.1} -horizontal -set 0

# Source 'Source.1'
gui_src_value_annotate -id ${Source.1} -switch false
gui_set_env TOGGLE::VALUEANNOTATE 0
gui_open_source -id ${Source.1}  -replace -active NoO_16 /afs/umich.edu/user/z/h/zhyiqun/Desktop/EECS470/eecs470g12/pipeline_v5/verilog/NoO.v
gui_src_value_annotate -id ${Source.1} -switch true
gui_set_env TOGGLE::VALUEANNOTATE 1
gui_view_scroll -id ${Source.1} -vertical -set 0
gui_src_set_reusable -id ${Source.1}
# Restore toplevel window zorder
# The toplevel window could be closed if it has no view/pane
if {[gui_exist_window -window ${TopLevel.1}]} {
	gui_set_active_window -window ${TopLevel.1}
	gui_set_active_window -window ${Source.1}
	gui_set_active_window -window ${HSPane.1}
}
#</Session>

