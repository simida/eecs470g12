/////////////////////////////////////////////////////////////////////////
//                                                                     //
//                                                                     //
//   Modulename :  testbench.v                                         //
//                                                                     //
//  Description :  Testbench module for the verisimple pipeline;       //
//                                                                     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////


module testbench;

  // Registers and wires used in the testbench
  logic        clock;
  logic        reset;
  logic [31:0] clock_count;
  logic [31:0] instr_count;
  logic [31:0] RS_ALU_count;
  logic [31:0] RS_MULT_count;
  logic [31:0] RS_BR_count;
  logic [31:0] RS_MEM_count;
  logic [31:0] ROB_count;
  logic [31:0] BRAT_count;
  logic [31:0] cdb_stall_count;
  logic [31:0] branch_count;
  logic [31:0] branch_mis_count;
  int          wb_fileno;

  logic  [1:0] proc2mem_command;
  logic [63:0] proc2mem_addr;
  logic [63:0] proc2mem_data;
  logic  [3:0] mem2proc_response;
  logic [63:0] mem2proc_data;
  logic  [3:0] mem2proc_tag;

  logic  [3:0] pipeline_completed_insts;
  logic  [3:0] pipeline_error_status;
  logic [1:0] [4:0] pipeline_commit_wr_idx;
  logic [1:0][63:0] pipeline_commit_wr_data;
  logic [1:0]       pipeline_commit_wr_en;
  logic [1:0][63:0] pipeline_commit_NPC;



  // Instantiate the Pipeline
  pipeline pipeline (// Inputs
                       .clock             (clock),
                       .reset             (reset),
                       .mem2proc_response (mem2proc_response),
                       .mem2proc_data     (mem2proc_data),
                       .mem2proc_tag      (mem2proc_tag),

                        // Outputs
                       .proc2mem_command  (proc2mem_command),
                       .proc2mem_addr     (proc2mem_addr),
                       .proc2mem_data     (proc2mem_data),

                       .pipeline_completed_insts(pipeline_completed_insts),
                       .pipeline_error_status(pipeline_error_status),
                       .pipeline_commit_wr_data(pipeline_commit_wr_data),
                       .pipeline_commit_wr_idx(pipeline_commit_wr_idx),
                       .pipeline_commit_wr_en(pipeline_commit_wr_en),
                       .pipeline_commit_NPC(pipeline_commit_NPC)
                      );


  // Instantiate the Data Memory
  mem memory (// Inputs
            .clk               (clock),
            .proc2mem_command  (proc2mem_command),
            .proc2mem_addr     (proc2mem_addr),
            .proc2mem_data     (proc2mem_data),

             // Outputs

            .mem2proc_response (mem2proc_response),
            .mem2proc_data     (mem2proc_data),
            .mem2proc_tag      (mem2proc_tag)
           );

  // Generate System Clock
  always
  begin
    #(`VERILOG_CLOCK_PERIOD/2.0);
    clock = ~clock;
  end

  // Task to display # of elapsed clock edges
  task show_clk_count;
        real cpi;
	int instr_cnt;
        begin
		instr_cnt = instr_count + pipeline_completed_insts - 1;
     		cpi = (clock_count + 1.0) / instr_cnt;
     		$display("@@  %0d cycles / %0d instrs = %f CPI\n@@",
        	clock_count+1, instr_cnt, cpi);
           	$display("@@  %4.2f ns total time to execute\n@@\n",
                    	clock_count*`VIRTUAL_CLOCK_PERIOD);
        end
        
  endtask  // task show_clk_count 

  // Task to display # of elapsed clock edges
  task show_branch_count;
        real misrate;
        begin
     		misrate = branch_mis_count *100 / branch_count;
     		$display("@@  %0d misses / %0d branchs = %2.1f%% Miss Rate\n@@",
        	branch_mis_count, branch_count, misrate);
        end
        
  endtask  // task show_clk_count 

  // Task to display # of elapsed clock edges
  task show_full_count;
        begin
     		$display("@@  %0d RS_ALU_full @@", RS_ALU_count);
     		$display("@@  %0d RS_BR_full @@", RS_BR_count);
     		$display("@@  %0d RS_MULT_full @@", RS_MULT_count);
     		$display("@@  %0d RS_MEM_full @@", RS_MEM_count);
     		$display("@@  %0d ROB_full @@", ROB_count);
     		$display("@@  %0d BRAT_full @@", BRAT_count);
     		$display("@@  %0d cdb_stall @@", cdb_stall_count);

        end
        
  endtask  // task show_clk_count 

  // Show contents of a range of Unified Memory, in both hex and decimal
  task show_mem_with_decimal;
   input [31:0] start_addr;
   input [31:0] end_addr;
   int showing_data;
   begin
    $display("@@@");
    showing_data=0;
    for(int k=start_addr;k<=end_addr; k=k+1)
      if (memory.unified_memory[k] != 0)
      begin
        $display("@@@ mem[%5d] = %x : %0d", k*8, memory.unified_memory[k], 
                                                 memory.unified_memory[k]);
        showing_data=1;
      end
      else if(showing_data!=0)
      begin
        $display("@@@");
        showing_data=0;
      end
    $display("@@@");
   end
  endtask  // task show_mem_with_decimal

  initial
  begin
    `ifdef DUMP
      $vcdplusdeltacycleon;
      $vcdpluson();
      $vcdplusmemon(memory.unified_memory);
    `endif
      
    clock = 1'b0;
    reset = 1'b0;

    // Pulse the reset signal
    $display("@@\n@@\n@@  %t  Asserting System reset......", $realtime);
    reset = 1'b1;
    @(posedge clock);
    @(posedge clock);

    $readmemh("program.mem", memory.unified_memory);

    @(posedge clock);
    @(posedge clock);
    `SD;
    // This reset is at an odd time to avoid the pos & neg clock edges

    reset = 1'b0;
    $display("@@  %t  Deasserting System reset......\n@@\n@@", $realtime);

    wb_fileno = $fopen("writeback.out");
    
  end


  // Count the number of posedges and number of instructions completed
  // till simulation ends
  always @(posedge clock or posedge reset)
  begin
    if(reset)
    begin
      clock_count <= `SD 0;
      instr_count <= `SD 0;
    end
    else
    begin
      clock_count <= `SD (clock_count + 1);
      instr_count <= `SD (instr_count + pipeline_completed_insts);
    end
  end  

/*
  // Count the number of posedges and number of instructions completed
  // till simulation ends
  always @(posedge clock or posedge reset)
  begin
    if(reset)
    begin
 	 RS_ALU_count 		<= `SD 0;
	 RS_MULT_count 		<= `SD 0;
	 RS_BR_count 		<= `SD 0;
	 RS_MEM_count 		<= `SD 0;
	 ROB_count 		<= `SD 0;
	 BRAT_count 		<= `SD 0;
	 cdb_stall_count 	<= `SD 0;
    end
    else
    begin
 	 RS_ALU_count 		<= `SD RS_ALU_count + pipeline.RS_full[0][0]+ pipeline.RS_full[0][1];
	 RS_MULT_count 		<= `SD RS_MULT_count + pipeline.RS_full[2][0]+ pipeline.RS_full[2][1];
	 RS_BR_count 		<= `SD RS_BR_count + pipeline.RS_full[1][0]+ pipeline.RS_full[1][1];
	 RS_MEM_count 		<= `SD RS_BR_count + pipeline.RS_full[3][0]+ pipeline.RS_full[3][1];
	 ROB_count 		<= `SD ROB_count + pipeline.ROB_full + pipeline.ROB_full_1left;
	 BRAT_count 		<= `SD BRAT_count + pipeline.BRAT_full + pipeline.BRAT_full_1left;
	 cdb_stall_count 	<= `SD cdb_stall_count + pipeline.EX_IS_stall[0][0] + pipeline.EX_IS_stall[0][1] + pipeline.EX_IS_stall[1][0] + pipeline.EX_IS_stall[1][1]
				    + pipeline.EX_IS_stall[2][0] + pipeline.EX_IS_stall[2][1] + pipeline.EX_IS_stall[3][0] + pipeline.EX_IS_stall[3][1] + pipeline.EX_LSQ_not_erase;				    
    end
  end  



 
  logic [1:0] branch_mis;

  assign branch_mis[0] = pipeline.BRAT_CMT_v[0] && pipeline.Complete_Retire_Stage.brat_mis_regfl[pipeline.BRAT_CMT_num[0]];
  assign branch_mis[1] = pipeline.BRAT_CMT_v[1] && pipeline.Complete_Retire_Stage.brat_mis_regfl[pipeline.BRAT_CMT_num[1]];


  // Count the number of posedges and number of instructions completed
  // till simulation ends
  always @(posedge clock or posedge reset)
  begin
    if(reset)
    begin
      branch_count <= `SD 0;
      branch_mis_count <= `SD 0;
    end
    else
    begin
      branch_count <= `SD (branch_count + pipeline.BRAT_CMT_v[0] + pipeline.BRAT_CMT_v[1]);
      branch_mis_count <= `SD (branch_mis_count + branch_mis[0]+ branch_mis[1]);
    end
  end  
*/



  always @(negedge clock)
  begin
    if(reset)
      $display("@@\n@@  %t : System STILL at reset, can't show anything\n@@",
               $realtime);
    else
    begin
      `SD;

       // print the writeback information to writeback.out
       if(pipeline_completed_insts>0) begin
         if(pipeline_commit_wr_en[0]) begin
	   if (pipeline_commit_wr_idx[0]!=`ZERO_REG)
		 $fdisplay(wb_fileno, "PC=%x, REG[%d]=%x",
		             pipeline_commit_NPC[0]-4,
		             pipeline_commit_wr_idx[0],
		             pipeline_commit_wr_data[0]);

	   else 
         	 $fdisplay(wb_fileno, "PC=%x, ---",pipeline_commit_NPC[0]-4);
	 end
         if(pipeline_commit_wr_en[1]) begin
	   if (pipeline_commit_wr_idx[1]!=`ZERO_REG)
		 $fdisplay(wb_fileno, "PC=%x, REG[%d]=%x",
		             pipeline_commit_NPC[1]-4,
		             pipeline_commit_wr_idx[1],
		             pipeline_commit_wr_data[1]);
	   else 
         	 $fdisplay(wb_fileno, "PC=%x, ---",pipeline_commit_NPC[1]-4);
	 end
      end

      // deal with any halting conditions
      if(pipeline_error_status!=`NO_ERROR)
      begin
        $display("@@@ Unified Memory contents hex on left, decimal on right: ");
        show_mem_with_decimal(0,`MEM_64BIT_LINES - 1); 
          // 8Bytes per line, 16kB total

        $display("@@  %t : System halted\n@@", $realtime);

        case(pipeline_error_status)
          `HALTED_ON_MEMORY_ERROR:  
              $display("@@@ System halted on memory error");
          `HALTED_ON_HALT:          
              $display("@@@ System halted on HALT instruction");
          `HALTED_ON_ILLEGAL:
              $display("@@@ System halted on illegal instruction");
          default: 
              $display("@@@ System halted on unknown error code %x",
                       pipeline_error_status);
        endcase
        $display("@@@\n@@");
        show_clk_count;
        show_branch_count;
	show_full_count;
        $fclose(wb_fileno);
        #100 $finish;
      end

    end  // if(reset)
  end 

endmodule  // module testbench

