//NOTE: cache wr_enable is asyn, rd_valid is syn
module dcache(
			  input         clock,
			  input         reset,
				//input from LSQ
				input						LSQ_DC_rd_mem,
				input	 [63:0]		LSQ_DC_addr,
				input  					LSQ_DC_wr_mem,
				input	 [63:0]		LSQ_DC_wr_value,
				input	 [`BRAT_SIZE_LOG:0]	LSQ_DC_BRAT_num,
				input	 [`ROB_SIZE_LOG-1:0]	LSQ_DC_ROB_num,
				input  [`PRF_SIZE_LOG-1:0]	LSQ_DC_PRF_num,
				input  [63:0]								LSQ_DC_NPC,
				input  [31:0]								LSQ_DC_IR,
				//input from Dcache
			  input  [63:0] Dcache_data,
			  input         Dcache_valid,
				//input from Dmemory
				input  [63:0]	Dmem_DC_data,
				input	 [3:0]	Dmem_DC_response,
				input	 [3:0]	Dmem_DC_tag,
				//input from ROB
				input  [`BRAT_SIZE:0]	BRAT_squash,
				//input from BRAT_CMT Stage
				input [1:0]											BRAT_CMT_v,
				input [1:0][`BRAT_SIZE_LOG-1:0]	BRAT_CMT_num,

				//output to LSQ
				output 	logic					MSHR_full,//not use head/tail, but just count
				output  logic					LD_BUF_full,
				output	logic [63:0]	Dcache_LSQ_data,
				output	logic [63:0]	Dcache_LSQ_rd_addr,
				output	logic					Dcache_LSQ_rd_valid,
				output  logic	[`ROB_SIZE_LOG-1:0]	Dcache_LSQ_ROB_num,
				output  logic [`PRF_SIZE_LOG-1:0] Dcache_LSQ_PRF_num,
				output  logic [`BRAT_SIZE_LOG:0] Dcache_LSQ_BRAT_num,//1 bit larger
				output  logic [63:0] 	Dcache_LSQ_rd_NPC,
				output  logic [31:0] 	Dcache_LSQ_rd_IR,
			  //output to Dcache	
				output	logic [`SET_IDX_SIZE_LOG-1:0]					DC_Dcache_wr1_idx, DC_Dcache_wr2_idx,
				output	logic [`DCACHE_TAG_SIZE_LOG-1:0]			DC_Dcache_wr1_tag, DC_Dcache_wr2_tag,
				output	logic [63:0]													DC_Dcache_wr1_data, DC_Dcache_wr2_data,
				output	logic																	DC_Dcache_wr1_enable, DC_Dcache_wr2_enable,
				output	logic [`SET_IDX_SIZE_LOG-1:0]					DC_Dcache_rd_idx,
				output	logic [`DCACHE_TAG_SIZE_LOG-1:0]			DC_Dcache_rd_tag,
				//output to Dmem
				output	logic	[63:0]				DC_Dmem_addr,
				output	logic [1:0]					DC_Dmem_command,
				output	logic [63:0]				DC_Dmem_data
        );

  logic												D1_LSQ_DC_rd_mem;
	logic	 [63:0]								D1_LSQ_DC_addr;
	logic	 [63:0]								D1_LSQ_DC_NPC;
	logic	 [31:0]								D1_LSQ_DC_IR;
	logic	 [`BRAT_SIZE_LOG:0]		D1_LSQ_DC_BRAT_num;
	logic	 [`ROB_SIZE_LOG-1:0]	D1_LSQ_DC_ROB_num;
	logic	 [`PRF_SIZE_LOG-1:0]	D1_LSQ_DC_PRF_num;
	logic	 [1:0]								D1_LSQ_DC_command;
	logic												D1_DC_Dcache_wr1_enable;

	logic	[63:0]			 next_Dcache_LSQ_data;
	logic	[63:0]			 next_Dcache_LSQ_rd_addr; 
	logic							 next_Dcache_LSQ_rd_valid	;
	logic	[`BRAT_SIZE_LOG:0]	next_LSQ_DC_BRAT_num;
	logic [`BRAT_SIZE_LOG:0]	next_D1_LSQ_DC_BRAT_num;
	logic	[`ROB_SIZE_LOG-1:0]	  next_LSQ_DC_ROB_num;
	logic	[`PRF_SIZE_LOG-1:0]	  next_LSQ_DC_PRF_num;

  logic [15:0][63:0]								MSHR_regfl_NPC;//need to store?			
  logic [15:0][31:0]								MSHR_regfl_IR;//need to store?			
  logic [15:0][63:0]								MSHR_regfl_addr;//need to store?			
	logic [15:0]											MSHR_regfl_valid;
	logic [15:0]											MSHR_regfl_invalid;//if this squash
	logic [15:0][1:0]									MSHR_regfl_valid_command;//??? to resolve squash case
	logic [15:0][`BRAT_SIZE_LOG:0]	MSHR_regfl_BRAT_num;
	logic [15:0][`BRAT_SIZE_LOG:0]	MSHR_regfl_BRAT_num_wire;
	logic [15:0][`ROB_SIZE_LOG-1:0]		MSHR_regfl_ROB_num;
	logic [15:0]											MSHR_regfl_wr_enable;
	logic [15:0][`PRF_SIZE_LOG-1:0]		MSHR_regfl_PRF_num;

  logic	[63:0]				next_DC_Dmem_addr;
  logic [1:0]					next_DC_Dmem_command;
	logic [63:0]				next_DC_Dmem_data;
  //LD_BUF: for structural hazard between mem ld output and cache ld output 
	logic [`LD_BUF_SIZE-1:0][63:0]						   LD_BUF_data;
	logic [`LD_BUF_SIZE-1:0][63:0]						   LD_BUF_addr;
	logic [`LD_BUF_SIZE-1:0][63:0]						   LD_BUF_NPC;
	logic [`LD_BUF_SIZE-1:0][31:0]						   LD_BUF_IR;
	logic [`LD_BUF_SIZE-1:0]      						   LD_BUF_valid;
	logic [`LD_BUF_SIZE-1:0][`BRAT_SIZE_LOG:0] 	 LD_BUF_BRAT_num;//when squash
	logic [`LD_BUF_SIZE-1:0][`BRAT_SIZE_LOG:0] 	 LD_BUF_BRAT_num_wire;
	logic [`LD_BUF_SIZE-1:0][`ROB_SIZE_LOG-1:0]  LD_BUF_ROB_num;
	logic [`LD_BUF_SIZE-1:0][`PRF_SIZE_LOG-1:0]  LD_BUF_PRF_num;
	logic [`LD_BUF_SIZE-1:0]										 LD_BUF_invalid;

  logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_out_entry;
  logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_in_entry;

  logic [`MSHR_SIZE_LOG-1:0]		MSHR_count;
	logic  Dmem_out;
	logic  Dcache_out;
	logic  LD_BUF_in;
	logic  LD_BUF_out;
	logic[63:0]			 next_Dcache_LSQ_rd_NPC; 
	logic[31:0]			 next_Dcache_LSQ_rd_IR; 

	wire										     next_LD_BUF_empty;
	logic										     LD_BUF_empty;

assign LD_BUF_full = LD_BUF_valid == 2'b11;//assume 2 entry for LD_BUF
assign next_LD_BUF_empty = LD_BUF_valid == 2'b0;

	always_comb begin
		//default
		DC_Dmem_command = `BUS_NONE;
		next_Dcache_LSQ_rd_valid	= 1'b0;
	  DC_Dcache_wr1_enable = 1'b0;
	  DC_Dcache_wr2_enable = 1'b0;
		MSHR_regfl_invalid = 16'b0;
		Dmem_out = 1'b0;
		Dcache_out = 1'b0;

		LD_BUF_in = 1'b0;
		LD_BUF_out = 1'b0;
		LD_BUF_invalid={`LD_BUF_SIZE'b0};
		next_D1_LSQ_DC_BRAT_num={1'b0,LSQ_DC_BRAT_num};

		if(!Dcache_valid) begin//read cache miss
			if(LSQ_DC_rd_mem)begin
				DC_Dmem_addr 		= LSQ_DC_addr;
				DC_Dmem_command = `BUS_LOAD;
			end
		end
		else begin
			if(LSQ_DC_rd_mem)begin
				 next_Dcache_LSQ_data 		= Dcache_data;
				 next_Dcache_LSQ_rd_addr	= LSQ_DC_addr; 
				 next_Dcache_LSQ_rd_NPC		= LSQ_DC_NPC; 
				 next_Dcache_LSQ_rd_IR		= LSQ_DC_IR; 
				 next_Dcache_LSQ_rd_valid	= 1'b1;
				 next_LSQ_DC_BRAT_num = {1'b0,LSQ_DC_BRAT_num};
				 next_LSQ_DC_ROB_num = LSQ_DC_ROB_num;
				 next_LSQ_DC_PRF_num = LSQ_DC_PRF_num;
	  	end
		end
		//LSQ store to memory
		if(LSQ_DC_wr_mem)begin
				DC_Dmem_addr    = LSQ_DC_addr;
				DC_Dmem_command = `BUS_STORE;
				DC_Dmem_data		= LSQ_DC_wr_value;
		end

		//LSQ store to cache
		if(LSQ_DC_wr_mem) begin
			{DC_Dcache_wr2_tag,DC_Dcache_wr2_idx} = LSQ_DC_addr[63:`BLOCK_SIZE_LOG];
			DC_Dcache_wr2_data   							=  LSQ_DC_wr_value;
			DC_Dcache_wr2_enable						= 1'b1;
		end

		//mem ld output write back to cache
		if(MSHR_regfl_valid_command[Dmem_DC_tag]==2'b1 && MSHR_regfl_wr_enable[Dmem_DC_tag])begin//timing???
				 DC_Dcache_wr1_data 										= Dmem_DC_data;
				 {DC_Dcache_wr1_tag,DC_Dcache_wr1_idx} 	= MSHR_regfl_addr[Dmem_DC_tag][63:`BLOCK_SIZE_LOG];
				 DC_Dcache_wr1_enable 									= 1'b1;///
		end
		//if want to write to same cache addr
		if(DC_Dcache_wr1_enable && DC_Dcache_wr2_enable) begin
				if(DC_Dcache_wr1_tag == DC_Dcache_wr2_tag) begin
					if(DC_Dcache_wr1_idx == DC_Dcache_wr2_idx) begin
							DC_Dcache_wr1_enable = 1'b0;//LSQ comes later
					end
				end
		end

		//BRAT_CMT
		if(BRAT_CMT_v[1]&&(next_LSQ_DC_BRAT_num=={1'b0,BRAT_CMT_num[1]} ))
				 next_LSQ_DC_BRAT_num = {1'b1,`BRAT_SIZE_LOG'b0};
		else if(BRAT_CMT_v[0]&&(next_LSQ_DC_BRAT_num=={1'b0,BRAT_CMT_num[0]} ))
				 next_LSQ_DC_BRAT_num = {1'b1,`BRAT_SIZE_LOG'b0};

		if(BRAT_CMT_v[1]&&(next_D1_LSQ_DC_BRAT_num=={1'b0,BRAT_CMT_num[1]} ))
				 next_D1_LSQ_DC_BRAT_num = {1'b1,`BRAT_SIZE_LOG'b0};
		else if(BRAT_CMT_v[0]&&(next_LSQ_DC_BRAT_num=={1'b0,BRAT_CMT_num[0]} ))
				 next_D1_LSQ_DC_BRAT_num = {1'b1,`BRAT_SIZE_LOG'b0};

		for(int k=0;k<`MSHR_SIZE;k++)begin
				if(BRAT_CMT_v[1]&&(MSHR_regfl_BRAT_num[k]=={1'b0,BRAT_CMT_num[1]} ))
						MSHR_regfl_BRAT_num_wire[k] ={1'b1,`BRAT_SIZE_LOG'b0};
				else if(BRAT_CMT_v[0]&&(MSHR_regfl_BRAT_num[k]=={1'b0,BRAT_CMT_num[0]} ))
						MSHR_regfl_BRAT_num_wire[k] ={1'b1,`BRAT_SIZE_LOG'b0};
				else
						MSHR_regfl_BRAT_num_wire[k] = MSHR_regfl_BRAT_num[k];
		end

		for(int l=0;l<`LD_BUF_SIZE;l++) begin
				if(BRAT_CMT_v[1]&&(LD_BUF_BRAT_num[l]=={1'b0,BRAT_CMT_num[1]} ))
						LD_BUF_BRAT_num_wire[l]={1'b1,`BRAT_SIZE_LOG'b0};
				else if(BRAT_CMT_v[0]&&(LD_BUF_BRAT_num[l]=={1'b0,BRAT_CMT_num[0]} ))
						LD_BUF_BRAT_num_wire[l]={1'b1,`BRAT_SIZE_LOG'b0};
				else
						LD_BUF_BRAT_num_wire[l]= LD_BUF_BRAT_num[l];
		end

		//squash MSHR(move from seq to comb)
		for(int i = 0;i<`MSHR_SIZE;i++)begin
			if(MSHR_regfl_valid_command[i]) begin
				if(BRAT_squash[MSHR_regfl_BRAT_num[i][`BRAT_SIZE_LOG-1:0]])
						MSHR_regfl_invalid[i] = 1'b1;
			 end
		end

		//squash LD_BUF
		for(int j=0;j<`LD_BUF_SIZE;j++) begin
			if(BRAT_squash[LD_BUF_BRAT_num[j][`BRAT_SIZE_LOG-1:0]]) begin
					LD_BUF_invalid[j] = 1'b1;
			end
		end
		
		//Output to LSQ/CDB from where
		if(MSHR_regfl_valid_command[Dmem_DC_tag]==2'b1)begin
				Dmem_out = 1'b1;
		end

		if(Dmem_out) begin
				if(!LD_BUF_empty) begin//next ???
					LD_BUF_out=1'b1;
					if(LD_BUF_valid[0])
						LD_BUF_out_entry = 1'b0;
					else
						LD_BUF_out_entry = 1'b1;//assume 2entry, must be one of them

					if(next_Dcache_LSQ_rd_valid) begin
							LD_BUF_in = 1'b1;
							if(~LD_BUF_valid[0])
								LD_BUF_in_entry = 1'b0;
							else 
								LD_BUF_in_entry = 1'b1;//assume 2entry, must be one of them
					end
				end
				else begin
					if(next_Dcache_LSQ_rd_valid) begin
							Dcache_out = 1'b1;
					end
				end
		end
		//MSHR_full
		if(MSHR_count == 4'hf) //assume 15 entry
			MSHR_full = 1'b1;
		else
			MSHR_full = 1'b0;
	end

  always_ff @(posedge clock)
  begin
    if(reset)
    begin
			  Dcache_LSQ_rd_valid	<= `SD 1'b0;

				for(int m = 0; m<16;m++)begin
					MSHR_regfl_valid[m] <= `SD 1'b0;
					MSHR_regfl_valid_command[m] <= `SD 2'b0;
					MSHR_regfl_wr_enable[m] <= `SD 1'b0;
				end

				LD_BUF_valid        <= `SD {`LD_BUF_SIZE'b0};
				LD_BUF_empty			  <= `SD 1'b1; 

				MSHR_count 				<= `SD {`MSHR_SIZE_LOG'b0};
    end
    else
    begin
				D1_LSQ_DC_rd_mem 		<= `SD LSQ_DC_rd_mem;
				D1_LSQ_DC_addr   		<= `SD LSQ_DC_addr;
				D1_LSQ_DC_NPC   		<= `SD LSQ_DC_NPC;
				D1_LSQ_DC_IR    		<= `SD LSQ_DC_IR;
				D1_LSQ_DC_BRAT_num  <= `SD next_D1_LSQ_DC_BRAT_num;//???
				D1_LSQ_DC_ROB_num   <= `SD LSQ_DC_ROB_num;
				D1_LSQ_DC_PRF_num   <= `SD LSQ_DC_PRF_num;
				D1_LSQ_DC_command 	<= `SD {LSQ_DC_wr_mem,LSQ_DC_rd_mem};

			  Dcache_LSQ_rd_valid	<= `SD 1'b0;
				LD_BUF_empty			  <= `SD next_LD_BUF_empty; 

			if(Dmem_DC_response && Dmem_DC_tag) MSHR_count <= `SD MSHR_count;//1 in, 1 out
			else if(Dmem_DC_response)	MSHR_count <= `SD MSHR_count+{`MSHR_SIZE_LOG'b1};//1 in
			else if(Dmem_out)	MSHR_count <= `SD MSHR_count-{`MSHR_SIZE_LOG'b1};//1 out

			//BRAT_squash
			for(int t=0;t<`MSHR_SIZE;t++) begin
				if(MSHR_regfl_invalid[t]) begin
						MSHR_regfl_valid_command[t] <= `SD 2'b0;
						MSHR_regfl_wr_enable[t] <= `SD 1'b0;
				end
			end
			//LD_BUF_squash
			for(int h=0;h<`LD_BUF_SIZE;h++) begin
					if(LD_BUF_invalid[h])
							LD_BUF_valid[h] <=`SD 1'b0;
			end
			//Send to access memory
		  if(Dmem_DC_response)begin
				MSHR_regfl_NPC[Dmem_DC_response] 	<= `SD D1_LSQ_DC_NPC;
				MSHR_regfl_IR[Dmem_DC_response] 	<= `SD D1_LSQ_DC_IR;
				MSHR_regfl_addr[Dmem_DC_response] 	<= `SD D1_LSQ_DC_addr;
				MSHR_regfl_BRAT_num[Dmem_DC_response] <= `SD D1_LSQ_DC_BRAT_num;
				MSHR_regfl_ROB_num[Dmem_DC_response] 	<= `SD D1_LSQ_DC_ROB_num;
				MSHR_regfl_PRF_num[Dmem_DC_response] 	<= `SD D1_LSQ_DC_PRF_num;
				MSHR_regfl_valid[Dmem_DC_response] 	<= `SD 1'b1;
				MSHR_regfl_valid_command[Dmem_DC_response] 	<= `SD D1_LSQ_DC_command;
				if(D1_LSQ_DC_command==`BUS_LOAD) 
						MSHR_regfl_wr_enable[Dmem_DC_response] <= `SD 1'b1;
			//STORE goes into MSHR, it clears 'DC_Dcache_wr1_enable' of prev LOAD
				if(D1_LSQ_DC_command==`BUS_STORE) begin
						for(int t=0;t<`MSHR_SIZE;t++) begin
							if(MSHR_regfl_valid_command[t]==`BUS_LOAD) begin
									if(MSHR_regfl_addr[t]==D1_LSQ_DC_addr)
											MSHR_regfl_wr_enable[t] <= `SD 1'b0;
							end
						end
				end
			end
			
			//Handle data from memory
			if(Dmem_DC_tag)begin//no matter squash or not, move head
				MSHR_regfl_valid[Dmem_DC_tag]  <= `SD 1'b0;
				MSHR_regfl_valid_command[Dmem_DC_tag]<= `SD 2'b0;//can be optimized
				MSHR_regfl_wr_enable[Dmem_DC_tag]		<= `SD 1'b0;
			end

			//change by BRAT_CMT
			LD_BUF_BRAT_num <= `SD LD_BUF_BRAT_num_wire;
			MSHR_regfl_BRAT_num <= `SD MSHR_regfl_BRAT_num_wire;

			//output to LSQ/CDB
					 //cache hit not able to send to LSQ
			if(LD_BUF_in) begin
					   LD_BUF_data[LD_BUF_in_entry] 	  <= `SD next_Dcache_LSQ_data;
					   LD_BUF_addr[LD_BUF_in_entry] 	  <= `SD next_Dcache_LSQ_rd_addr;
					   LD_BUF_NPC[LD_BUF_in_entry] 	  	<= `SD next_Dcache_LSQ_rd_NPC;
					   LD_BUF_IR[LD_BUF_in_entry] 	  	<= `SD next_Dcache_LSQ_rd_IR;
					   LD_BUF_valid[LD_BUF_in_entry]	  <= `SD 1'b1;
					   LD_BUF_BRAT_num[LD_BUF_in_entry] <= `SD next_LSQ_DC_BRAT_num;
					   LD_BUF_ROB_num[LD_BUF_in_entry] <= `SD next_LSQ_DC_ROB_num;
					   LD_BUF_PRF_num[LD_BUF_in_entry] <= `SD next_LSQ_DC_PRF_num;
			end

			if(Dmem_out)begin
					 Dcache_LSQ_data 			<= `SD Dmem_DC_data;
					 Dcache_LSQ_rd_addr		<= `SD MSHR_regfl_addr[Dmem_DC_tag];
					 Dcache_LSQ_rd_NPC		<= `SD MSHR_regfl_NPC[Dmem_DC_tag];
					 Dcache_LSQ_rd_IR 		<= `SD MSHR_regfl_IR[Dmem_DC_tag];
					 Dcache_LSQ_rd_valid	<= `SD 1'b1;
					 Dcache_LSQ_ROB_num		<= `SD MSHR_regfl_ROB_num[Dmem_DC_tag];
					 Dcache_LSQ_BRAT_num	<= `SD MSHR_regfl_BRAT_num[Dmem_DC_tag];
					 Dcache_LSQ_PRF_num		<= `SD MSHR_regfl_PRF_num[Dmem_DC_tag];
			end

			if(LD_BUF_out)begin
					 Dcache_LSQ_data 			<= `SD LD_BUF_data[LD_BUF_out_entry];
					 Dcache_LSQ_rd_addr		<= `SD LD_BUF_addr[LD_BUF_out_entry];
					 Dcache_LSQ_rd_NPC		<= `SD LD_BUF_NPC[LD_BUF_out_entry];
					 Dcache_LSQ_rd_IR 		<= `SD LD_BUF_IR[LD_BUF_out_entry];
					 Dcache_LSQ_rd_valid	<= `SD 1'b1;
					 Dcache_LSQ_ROB_num		<= `SD LD_BUF_ROB_num[LD_BUF_out_entry];
					 Dcache_LSQ_BRAT_num	<= `SD LD_BUF_BRAT_num[LD_BUF_out_entry];
					 Dcache_LSQ_PRF_num		<= `SD LD_BUF_PRF_num[LD_BUF_out_entry];
					 LD_BUF_valid[LD_BUF_out_entry] <= `SD 1'b0; 
			end	

			if(Dcache_out) begin
					 Dcache_LSQ_data 			<= `SD next_Dcache_LSQ_data;
					 Dcache_LSQ_rd_addr		<= `SD next_Dcache_LSQ_rd_addr;
					 Dcache_LSQ_rd_NPC		<= `SD next_Dcache_LSQ_rd_NPC;
					 Dcache_LSQ_rd_IR			<= `SD next_Dcache_LSQ_rd_IR;
					 Dcache_LSQ_rd_valid	<= `SD 1'b1;
					 Dcache_LSQ_ROB_num		<= `SD next_LSQ_DC_ROB_num;
					 Dcache_LSQ_BRAT_num	<= `SD next_LSQ_DC_BRAT_num;
					 Dcache_LSQ_PRF_num		<= `SD next_LSQ_DC_PRF_num;
			end

			if(LSQ_DC_rd_mem) begin
				{DC_Dcache_rd_tag,DC_Dcache_rd_idx} <= `SD LSQ_DC_addr[63:`BLOCK_SIZE_LOG];
			end
			

    end
  end

endmodule

