

module FrontEnd(
				//------From/To Mem------					
				input [3:0]   Imem2proc_response, // Tag from memory about current request
				input [63:0]  mem2proc_data,     // Data coming back from memory
				input [3:0]   mem2proc_tag,      // Tag from memory about current reply

				output logic [1:0]  proc2Imem_command,  // command sent to memory
				output logic [63:0] proc2Imem_addr,     // Address sent to memory


				//------To RS & ROB------	
	       output [1:0] [31:0] 			FE_ID_IR,
	       output [1:0] [63:0] 			FE_ID_NPC,

	       output [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_regA,         // reg A value
	       output [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_regB,         // reg B value
	       output [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_dest_reg,     // destination (writeback) register index
	       output [1:0] [`ARF_SIZE_LOG-1:0] 		FE_ID_dest_arfreg,     // destination (writeback) register index
	       
	       //outputs from Decoder
	       output [1:0] [1:0] 			FE_ID_opa_select,    // ALU opa mux select (ALU_OPA_xxx *)
	       output [1:0] [1:0] 			FE_ID_opb_select,    // ALU opb mux select (ALU_OPB_xxx *)
   	                                                                  // (ZERO_REG if no writeback)
	       output [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	FE_ID_alu_func,      // ALU function select (ALU_xxx *)
	       output [1:0]        			FE_ID_rd_mem,        // does inst read memory?
	       output [1:0]        			FE_ID_wr_mem,        // does inst write memory?
	       output [1:0]  			      FE_ID_cond_branch,   // is inst a conditional branch?
	       output [1:0]        			FE_ID_uncond_branch, // is inst an unconditional branch 
	                                                                  // or jump?
	       output [1:0]       			FE_ID_halt,          // is inst a halt 00 01 10 11
	       output [1:0]       			FE_ID_illegal,       // is inst an illegal inst 00 01 10 11
	       output logic [1:0]      	FE_ID_valid_inst,    // is inst a valid instruction to be  00 01 11
				 output branch1, branch2,

				//------To RS------
				output logic [63:0] FE_valid_list,
				output logic [1:0] [`PRF_IDX-1:0]	FE_prf_freed,


				//------From ROB------	
				input logic       								 			ROB_full,
				input logic       								 			ROB_full_1left,
				input logic [1:0] [`PRF_SIZE_LOG-1:0]   RET_prfn,
				input logic [1:0] [`ARF_SIZE_LOG-1:0]   RET_arfn,
				input logic [1:0] 											RET_valid,

				//inputs for early branch resolution
				input logic														BRAT_CMP_miss,
				input logic [`BRAT_SIZE_LOG-1:0]			BRAT_CMP_num,
				input logic [63:0]										BRAT_CMP_targetaddr,

				input logic [1:0]											BRAT_CMT_v,
				input logic [1:0]											BRAT_CMT_takeornot,
				input logic [1:0] [63:0]							BRAT_CMT_pc,
				input logic [1:0] [63:0]							BRAT_CMT_targetaddr,

				input logic 													BRAT_empty,
				input logic [`BRAT_SIZE_LOG-1:0]			BRAT_tail,
				input logic 													BRAT_full,
				input logic 													BRAT_full_1left,


				output logic [1:0] [`BRAT_SIZE_LOG:0]		 		BRAT_TAG,
				output logic [1:0] FE_predict_taken,
				output logic [1:0] [63:0] FE_predict_addr,
				
				//------From RS------	
				input [3:0] [1:0] RS_full,

				input clock,
				input reset
);


				// Icache wires
				logic [63:0] cachemem_data;
				logic        cachemem_valid;
				logic  [4:0] Icache_rd_idx;
				logic [23:0] Icache_rd_tag;
				logic  [4:0] Icache_wr_idx;
				logic [23:0] Icache_wr_tag;
				logic        Icache_wr_en;
				logic [63:0] Icache_data_out, proc2Icache_addr;
				logic        Icache_valid_out;


					//------------Fetcher--Decoder------
					logic	[1:0] id_valid_inst; 

					//------------Decoder--Rat------
					logic [1:0] [4:0] ra_idx_out,rb_idx_out;

	wire [31:0] [`PRF_IDX-1:0] RRAT_registers;

	//rename happens only when instr is valid and dest_reg is not ZERO_REG
	wire instr0_rename=(FE_ID_valid_inst[0] && (FE_ID_dest_arfreg[0] != `ZERO_REG));
	wire instr1_rename=(FE_ID_valid_inst[1] && (FE_ID_dest_arfreg[1] != `ZERO_REG));


	//write to RAT only when need rename and rename succeeds
	wire rename_v1,rename_v2;
	wire RAT_wr1 = instr0_rename && rename_v1;
	wire RAT_wr2 = instr1_rename && rename_v2;
	logic [5:0] freed_prf1,freed_prf2;
	assign FE_prf_freed[0]=freed_prf1;
	assign FE_prf_freed[1]=freed_prf2;


	//RAS
	logic pop,push;
	logic [63:0] RAS_predict_addr;
	logic [63:0] PC;
	logic [63:0] push_PC;
	logic [1:0] [`RAS_SIZE-1:0] [63:0] BRAS_cp_reg;
	logic [1:0] [`RAS_LOG2:0] BRAS_cp_head;
	logic [`RAS_SIZE-1:0] [63:0] BRAS_ld_reg;
	logic [`RAS_LOG2:0] BRAS_ld_head;
	//BTB
	logic [1:0] rd_branch_valid_out;
	logic [1:0] [63:0] BTB_predict_addr;
	//BP
	logic [1:0] BP_taken;
	//BRAT, BFRL
	//logic branch1, branch2;


	wire [1:0] [63:0] PC_offset;
	wire [1:0] JUMP_bsr,JUMP_push, RETURN;
	wire [1:0] [63:0] br_disp;





//----------------------FE Control-------------------------------


//---------NPC calc(Next Fetch)----
//---[Function]:	Calc what control signal send to Fetcher (decide next_PC)
//---[Output]: 		[1:0]PC_ctrl, [63:0]target_pc
//---[Input]: 		Many

//---------Id Valid bit calc(Whether Instructin is valid fetched)----
//---[Function]:	Shouldn't send any invalid instr to next stage
//---[Output]: 		[1:0]FE_ID_valid_inst
//---[Input]: 		Many
//---Comment: Similar to next_PC calc, only difference is when accept2 and the first branch is taken


	//Output Control Signal
			logic [1:0] PC_ctrl;
			logic [63:0] target_pc;

//Priority
	//Input Logic Signal			
			//1. Branch Mispred Squash 			
			//input BRAT_CMP_miss
			//input [63:0]	BRAT_CMP_targetaddr

			//2. Fetch Failure -- 0 instr out			
			//---From Fetcher---
			//wire Icache_valid_out;
			wire [1:0] if_valid_inst;

			//3.Structure Hazard -- 0 accepted
			//---From ROB---
			wire [1:0] ROB_full_2 ={ROB_full,ROB_full_1left};
			wire [1:0] BRAT_full_2={BRAT_full,BRAT_full_1left};
			//---From FE---
			wire	[1:0] FRL_empty;

			//4.Prediction
			logic [1:0] predict_taken;
			logic [1:0] [63:0] predict_addr; 
	
			assign FE_predict_taken = predict_taken;
			assign FE_predict_addr = predict_addr;
			//assign FE_ID_valid_inst[0]=instr0_accept&id_valid_inst[0];
			//assign FE_ID_valid_inst[1]=instr1_accept&id_valid_inst[0];

			logic instr0_accept, instr1_accept, accept0, accept1, accept2;	
			//How Many Instr will be accepted by BackEnd
			assign accept0 = ~instr0_accept;
			assign accept1 = instr0_accept & ~instr1_accept;
			assign accept2 = instr0_accept & instr1_accept;
			wire ALU0 = (FE_ID_alu_func[0] != `ALU_MULQ) && !(FE_ID_cond_branch[0] | FE_ID_uncond_branch[0]) && !(FE_ID_rd_mem[0] | FE_ID_wr_mem[0]);
			wire ALU1 = (FE_ID_alu_func[1] != `ALU_MULQ) && !(FE_ID_cond_branch[1] | FE_ID_uncond_branch[1]) && !(FE_ID_rd_mem[1] | FE_ID_wr_mem[1]);
			always_comb
				begin
					//Fetch success or not
					instr0_accept=if_valid_inst[0];
					instr1_accept=if_valid_inst[1];
					//FreeRegisterList Stuct Hazard
					if((FE_ID_dest_arfreg[0] != `ZERO_REG) && FRL_empty[1]) instr0_accept=0;
					if((FE_ID_dest_arfreg[1] != `ZERO_REG) && FRL_empty[1]) instr1_accept=0;
					if((FE_ID_dest_arfreg[0] != `ZERO_REG) && FRL_empty[0] && (FE_ID_dest_arfreg[1] != `ZERO_REG)) instr1_accept=0;
					//ROB Stuct Hazard
					if(ROB_full_2[1]) begin instr0_accept=0; instr1_accept=0; end
					if(ROB_full_2[0]) instr1_accept=0;
					//RS/BRAT Stuct Hazard
					if((FE_ID_cond_branch[0] | FE_ID_uncond_branch[0]) && (RS_full[1][1] | BRAT_full_2[1])) instr0_accept=0;
					if((FE_ID_cond_branch[1] | FE_ID_uncond_branch[1]) && (RS_full[1][1] | BRAT_full_2[1])) instr1_accept=0;
					if((FE_ID_cond_branch[0] | FE_ID_uncond_branch[0]) && (RS_full[1][0] | BRAT_full_2[0]) && (FE_ID_cond_branch[1] | FE_ID_uncond_branch[1])) instr1_accept=0;
			
					if((FE_ID_alu_func[0] == `ALU_MULQ) && RS_full[2][1]) instr0_accept=0;
					if((FE_ID_alu_func[1] == `ALU_MULQ) && RS_full[2][1]) instr1_accept=0;
					if((FE_ID_alu_func[0] == `ALU_MULQ) && RS_full[2][0] && (FE_ID_alu_func[1] == `ALU_MULQ)) instr1_accept=0;
			
					if((FE_ID_rd_mem[0] | FE_ID_wr_mem[0]) && RS_full[3][1]) instr0_accept=0;
					if((FE_ID_rd_mem[1] | FE_ID_wr_mem[1]) && RS_full[3][1]) instr1_accept=0;
					if((FE_ID_rd_mem[1] | FE_ID_wr_mem[1]) && RS_full[3][0] && (FE_ID_rd_mem[0] | FE_ID_wr_mem[0])) instr1_accept=0;
			
					if(ALU0 && RS_full[0][1]) instr0_accept=0;
					if(ALU1 && RS_full[0][1]) instr1_accept=0;
					if(ALU0 && RS_full[0][0] && ALU1) instr1_accept=0;
				end

			always_comb
				begin
					target_pc=BRAT_CMP_targetaddr;
					PC_ctrl=`PC_PLUS8;
					FE_ID_valid_inst=2'b00;

					if(BRAT_CMP_miss)
						begin
							PC_ctrl=`PC_BRANCH;
							target_pc=BRAT_CMP_targetaddr;
							FE_ID_valid_inst=2'b00;
						end
					else if(accept0)
						begin
							PC_ctrl=`PC_STALL;
							FE_ID_valid_inst=2'b00;
						end
					else if(accept1)
						begin
							PC_ctrl=predict_taken[0]? `PC_BRANCH : `PC_PLUS4;
							target_pc=predict_addr[0];
							FE_ID_valid_inst=2'b01;
						end
					else if(accept2)
						begin
								PC_ctrl=(predict_taken[0]|predict_taken[1])? `PC_BRANCH : `PC_PLUS8;
								target_pc=predict_taken[0]? predict_addr[0] : predict_addr[1];
								FE_ID_valid_inst[0]=1'b1;
								FE_ID_valid_inst[1]=predict_taken[0]? 1'b0 : 1'b1;
						end
				end 

			wire [`BRAT_SIZE_LOG-1:0] BRAT_tail_min1 = BRAT_tail-1'b1;
			wire [`BRAT_SIZE_LOG-1:0] BRAT_tail_plus1 = BRAT_tail+1'b1;
			always_comb
				begin
					BRAT_TAG[0] = BRAT_tail_min1;
					BRAT_TAG[1] = BRAT_tail_min1;

					if (branch1) begin
							BRAT_TAG[0] = BRAT_tail;
							BRAT_TAG[1] = branch2 ? BRAT_tail_plus1 : BRAT_tail;
						end
					else if (branch2) begin
							BRAT_TAG[0] = BRAT_empty ? 3'b100 : BRAT_tail_min1;
							BRAT_TAG[1] = BRAT_tail;						
						end
					else if(BRAT_empty) 
						begin
							BRAT_TAG[0] = 3'b100;
							BRAT_TAG[1] = 3'b100;
						end
					
				end



//---------Module Ctrl Signal (Control signal needed for RAT, FRL, Predictor, BTB and RAS)----
//---[Function]:	Module inside the FE need control signal to operate
//---[Output]: 		predict_taken, predict_addr, pop,push , branch1, branch2
//---[Input]: 		BTB_predict_addr, RAS_predict_addr, PC+offset, BP_taken

/*
//FRL / RAT
branch1, branch2, //if first instr want to copy BRAT: branch1 will be 1
//BTB
BTB_predict_addr
//RAS
pop,push
RAS_predict_addr
//BP
BP_taken
*/

assign RETURN[0] = FE_ID_IR[0][31:26]==`JSR_GRP && FE_ID_IR[0][15];
assign RETURN[1] = FE_ID_IR[1][31:26]==`JSR_GRP && FE_ID_IR[1][15];
assign JUMP_push[0] = (FE_ID_IR[0][31:26]==`JSR_GRP && FE_ID_IR[0][14]);
assign JUMP_push[1] = (FE_ID_IR[1][31:26]==`JSR_GRP && FE_ID_IR[1][14]);

assign JUMP_bsr[0] = (FE_ID_IR[0][31:26]==`BR_INST) || (FE_ID_IR[0][31:26]==`BSR_INST);
assign JUMP_bsr[1] = (FE_ID_IR[1][31:26]==`BR_INST) || (FE_ID_IR[1][31:26]==`BSR_INST);

//??? Have queation about branch disp part ????

assign br_disp[0]  = { {41{FE_ID_IR[0][20]}}, FE_ID_IR[0][20:0], 2'b00 };
assign br_disp[1]  = { {41{FE_ID_IR[1][20]}}, FE_ID_IR[1][20:0], 2'b00 };
assign PC_offset[0] = FE_ID_NPC[0] + br_disp[0];
assign PC_offset[1] = FE_ID_NPC[1] + br_disp[1];


//predict_taken: if Unconditional or Cond&BP_taken
assign predict_taken[0] =(FE_ID_cond_branch[0] & BP_taken[0]) || FE_ID_uncond_branch[0];
assign predict_taken[1] =(FE_ID_cond_branch[1] & BP_taken[1]) || FE_ID_uncond_branch[1];
//assign predict_taken = 2'b00;

//predict_addr: 
always_comb
	begin
		predict_addr[0]=64'b0;
		if(JUMP_push[0]) predict_addr[0] =  BTB_predict_addr[0];
		else if(RETURN[0]) predict_addr[0] = RAS_predict_addr;
		else if(FE_ID_cond_branch[0] || JUMP_bsr[0]) predict_addr[0] = PC_offset[0];

		predict_addr[1]=64'b0;
		if(JUMP_push[1]) predict_addr[1] =  BTB_predict_addr[1];
		else if(RETURN[1]) predict_addr[1] = RAS_predict_addr;
		else if(FE_ID_cond_branch[1] || JUMP_bsr[1]) predict_addr[1] = PC_offset[1];
	end


//pop, push, branch1, branch2
assign branch1 = FE_ID_valid_inst[0] && (FE_ID_cond_branch[0] | FE_ID_uncond_branch[0]);//branch + dispatch
assign branch2 = FE_ID_valid_inst[1] && (FE_ID_cond_branch[1] | FE_ID_uncond_branch[1]);//branch + dispatch
assign pop = (FE_ID_valid_inst[0] && RETURN[0]) || (FE_ID_valid_inst[1] && RETURN[1]);//return + dispatch

always_comb begin //jump + dispatch
	push = 1'b0;
	push_PC = PC;
	if (FE_ID_valid_inst[0] && (FE_ID_dest_arfreg[0] != `ZERO_REG) && (JUMP_push[0]||JUMP_bsr[0])) begin
		push = 1'b1;
		push_PC = PC + 64'd4;
	end
	if (FE_ID_valid_inst[1] && (FE_ID_dest_arfreg[1] != `ZERO_REG) && (JUMP_push[1]||JUMP_bsr[1])) begin
		push = 1'b1;
		push_PC = PC + 64'd8;
	end
end

//----------------------FETCH-------------------------------

  // Actual cache (data and tag RAMs)
  cache cachememory (// inputs
            .clock(clock),
            .reset(reset),

						//Interface From Mem
            .wr1_data(mem2proc_data),

						//Interface From Cache controler
            .wr1_en(Icache_wr_en),
            .wr1_idx(Icache_wr_idx),
            .wr1_tag(Icache_wr_tag),            
            .rd1_idx(Icache_rd_idx),
            .rd1_tag(Icache_rd_tag),

            // outputs
						//Interface To Cache controler
            .rd_data_t(cachemem_data),
            .rd_valid_t(cachemem_valid)
           );

  // Cache controller
  icache icache_0(
					// inputs 							
          .clock(clock),
          .reset(reset),

					//Interface From Fetcher
          .proc2Icache_addr(proc2Icache_addr),
					//Interface to Fetcher
          .Icache_data_out(Icache_data_out),
          .Icache_valid_out(Icache_valid_out),


					//Interface to MEM
          .proc2Imem_command(proc2Imem_command),
          .proc2Imem_addr(proc2Imem_addr),
					//Interface From Mem
          .Imem2proc_response(Imem2proc_response),
          .Imem2proc_data(mem2proc_data),
          .Imem2proc_tag(mem2proc_tag),


					//Interface to ICache
          .current_index(Icache_rd_idx),
          .current_tag(Icache_rd_tag),
          .last_index(Icache_wr_idx),
          .last_tag(Icache_wr_tag),
          .data_write_enable(Icache_wr_en),
					//Interface From ICache
          .cachemem_data(cachemem_data),
          .cachemem_valid(cachemem_valid)
         );



	Fetcher Fetcher1(
					//-----------To Cache controler------------------
					.proc2Icache_addr(proc2Icache_addr),
					//-----------From Cache controler----------------
				  .Icache2proc_data(Icache_data_out),		   
				  .Icache_valid(Icache_valid_out),

					//-----------To Decoder/RS/ROB------------------
					//First instr
					.NPC1(FE_ID_NPC[0]),			     
				  .IR1(FE_ID_IR[0]),			        
				  .if_valid_inst1(if_valid_inst[0]),
					//Second instr
					.NPC2(FE_ID_NPC[1]),			     
				  .IR2(FE_ID_IR[1]),			        
				  .if_valid_inst2(if_valid_inst[1]),


					//-----------From FrontEnd------------------
				  .PC_ctrl(PC_ctrl),              
					.target_pc(target_pc),           
					.PC(PC),
					
				  .clock(clock),
				  .reset(reset)
        ); 


	RAS RAS1(	   
		 .pop(pop),
		 .predicted_PC(RAS_predict_addr),
	   
	   .push(push),
	   .push_PC(push_PC),
		

	   .flush(BRAT_CMP_miss), 
	   .BRAS_ld_reg(BRAS_ld_reg),
	   .BRAS_ld_head(BRAS_ld_head),
	   .BRAS_cp_reg(BRAS_cp_reg),
	   .BRAS_cp_head(BRAS_cp_head),

	   .clock(clock),
	   .reset(reset)
	   );

	BTB_dm_64 BTB(
		  .rd_branch_pc({PC+64'h4,PC}),
		  .BRAT_CMT_pc(BRAT_CMT_pc),
		  .BRAT_CMT_v(BRAT_CMT_v),
		  .BRAT_CMT_targetaddr(BRAT_CMT_targetaddr),

			.rd_branch_target_out(BTB_predict_addr),
		  .rd_branch_valid_out(rd_branch_valid_out),

		  .clock(clock),
		  .reset(reset)
		  );

 Branch_Predictor BP(
			.FE_PC({PC,PC+64'h4}),
			
			.FE_taken(BP_taken),
			
			.BRAT_CMT_pc(BRAT_CMT_pc),
			.BRAT_CMT_takeornot(BRAT_CMT_takeornot),
			.BRAT_CMT_v(BRAT_CMT_v),

		  .clock(clock),
		  .reset(reset)			
			);



//----------------------DECODE-------------------------------

	wire [1:0] cpuid_out;
	wire [1:0] ldl_mem_out;
	wire [1:0] stc_mem_out;
	Decoder Decoder0(
          //-------From Fetcher------
					.IR(FE_ID_IR[0]),
				  .if_valid_inst(if_valid_inst[0]),

					//-------To Renamer------
					.ra_idx_out(ra_idx_out[0]),
			  	.rb_idx_out(rb_idx_out[0]),
			  	.dest_idx_out(FE_ID_dest_arfreg[0]),


					//-------To RS/ROB------
					.opa_select_out(FE_ID_opa_select[0]),
					.opb_select_out(FE_ID_opb_select[0]),
					.alu_func_out(FE_ID_alu_func[0]),
					.rd_mem_out(FE_ID_rd_mem[0]),
					.wr_mem_out(FE_ID_wr_mem[0]),
					.ldl_mem_out(ldl_mem_out[0]),
					.stc_mem_out(stc_mem_out[0]),
					.cond_branch_out(FE_ID_cond_branch[0]),
					.uncond_branch_out(FE_ID_uncond_branch[0]),
					.halt_out(FE_ID_halt[0]),
					.cpuid_out(cpuid_out[0]),
					.illegal_out(FE_ID_illegal[0]),
					.id_valid_inst_out(id_valid_inst[0])
        );

	Decoder Decoder1(
          //-------From Fetcher------
					.IR(FE_ID_IR[1]),
				  .if_valid_inst(if_valid_inst[1]),

					//-------To Renamer------
					.ra_idx_out(ra_idx_out[1]),
			  	.rb_idx_out(rb_idx_out[1]),
			  	.dest_idx_out(FE_ID_dest_arfreg[1]),

					//-------To RS/ROB------
					.opa_select_out(FE_ID_opa_select[1]),
					.opb_select_out(FE_ID_opb_select[1]),
					.alu_func_out(FE_ID_alu_func[1]),
					.rd_mem_out(FE_ID_rd_mem[1]),
					.wr_mem_out(FE_ID_wr_mem[1]),
					.ldl_mem_out(ldl_mem_out[1]),
					.stc_mem_out(stc_mem_out[1]),
					.cond_branch_out(FE_ID_cond_branch[1]),
					.uncond_branch_out(FE_ID_uncond_branch[1]),
					.halt_out(FE_ID_halt[1]),
					.cpuid_out(cpuid_out[1]),
					.illegal_out(FE_ID_illegal[1]),
					.id_valid_inst_out(id_valid_inst[1])
        );





//----------------------RENAME-------------------------------
	RAT RAT1(
				//------From Decoder------
					.arf_regA(ra_idx_out[0]),
					.arf_regB(rb_idx_out[0]), 
					.arf_dest(FE_ID_dest_arfreg[0]),
					.wr_en(RAT_wr1),

					.arf_regA1(ra_idx_out[1]),
					.arf_regB1(rb_idx_out[1]), 
					.arf_dest1(FE_ID_dest_arfreg[1]),
					.wr_en1(RAT_wr2),

				//------To RS----------
					.prf_regA(FE_ID_regA[0]),
					.prf_regB(FE_ID_regB[0]),
					.prf_regA1(FE_ID_regA[1]),
					.prf_regB1(FE_ID_regB[1]),

				//------From FRL------
					.prf_dest(FE_ID_dest_reg[0]),
					.prf_dest1(FE_ID_dest_reg[1]),


					.BRAT_tail(BRAT_tail),
					.branch1(branch1), .branch2(branch2), //if first instr want to copy BRAT: branch1 will be 1

					.BRAT_CMP_num(BRAT_CMP_num), //which BRAT used to overwrite RAT
					.BRAT_CMP_miss(BRAT_CMP_miss),
					.BRAS_cp_reg(BRAS_cp_reg),
					.BRAS_cp_head(BRAS_cp_head),
					.BRAS_ld_reg(BRAS_ld_reg),
					.BRAS_ld_head(BRAS_ld_head),

					.clock(clock),
					.reset(reset)
);


	FreeRegList FRL(
					//-------From Decoder----------//rename controler
					.rename1(instr0_rename), .rename2(instr1_rename),

					//-------To RAT----------------//rename result
					.rename_v1(rename_v1), .rename_v2(rename_v2),
					.prf1(FE_ID_dest_reg[0]), .prf2(FE_ID_dest_reg[1]),

					.FRL_empty(FRL_empty), //not necessary to stall if instr don't write reg


					//-------From RRAT----------
					.free_v1(RET_valid[0]), .free_v2(RET_valid[1]), //used 2-way
					.freed_prf1(freed_prf1), .freed_prf2(freed_prf2), //used 2-way

					//------From ROB----------
					.BRAT_tail(BRAT_tail),
					.BRAT_CMP_num(BRAT_CMP_num), //which BRAT used to overwrite RAT
					.BRAT_CMP_miss(BRAT_CMP_miss),
					.valid_list(FE_valid_list),

					.branch1(branch1), .branch2(branch2), //if first instr want to copy BRAT: branch1 will be 1
					.clock(clock),
					.reset(reset)
			);


	RRAT RRAT1(
				//------From ROB----------
				.wr_en(RET_valid[0]), .wr_en2(RET_valid[1]),
				.arf_dest1(RET_arfn[0]),
				.prf_dest1(RET_prfn[0]),
				.arf_dest2(RET_arfn[1]),
				.prf_dest2(RET_prfn[1]),

				//------To FRL----------
				.freed_prf1(freed_prf1), .freed_prf2(freed_prf2),

				.clock(clock),
				.reset(reset)
			);

endmodule
