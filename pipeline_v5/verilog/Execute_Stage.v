/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  Execute_Stage.v                                     //
//                                                                     //
//  Description :  This module is the Execute Stage (with ppl regs) of // 
//                 the Pipeline.                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////


module Execute_Stage (
		      //////////
		      // Inputs
		      //////////  
		      input clock,
		      input reset,
		      input branch_mis,
		      input [`BRAT_SIZE:0] 				BRAT_squash,
		      
		      // Inputs from IS		      
		      input [3:0] [1:0] 	                	IS_EX_valid,
		      input [3:0] [1:0] [63:0]				IS_EX_NPC,
		      input [3:0] [1:0] [31:0] 				IS_EX_IR,
		      input [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	IS_EX_alu_func,
		      input [2:0] [1:0] [63:0] 				IS_EX_regA_value,
		      input [3:0] [1:0] [63:0] 				IS_EX_regB_value,
		      input [1:0] [`PRF_SIZE_LOG-1:0] 			IS_EX_ls_regA,
		      input [3:0] [1:0] [1:0] 		    		IS_EX_opa_sel,
		      input [3:0] [1:0] [1:0] 				IS_EX_opb_sel,
		      input [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 		IS_EX_dest,
		      input [3:0] [1:0] [`ROB_SIZE_LOG-1:0]     	IS_EX_ROB,
		      input [3:0] [1:0] [`BRAT_SIZE_LOG:0]		IS_EX_BRAT,
		      input [1:0] 				      	IS_EX_uncn_br,
		      input [1:0]					IS_EX_ld_or_st,
		      input [1:0] [`SQ_SIZE_LOG-1:0]	       		IS_EX_out_sq_num,
		      
		      // Inputs from PRF to CDB
		      input 					       	PRF_CDB_valid,
		      input  [63:0]			       		PRF_CDB_NPC,
		      input  [31:0]			       		PRF_CDB_IR,
		      input  [`ROB_SIZE_LOG-1:0]	       		PRF_CDB_ROB,
		      input	[`BRAT_SIZE_LOG:0]	       		PRF_CDB_BRAT,
		      input  [63:0]	       				PRF_CDB_value,
		      input  [`PRF_SIZE_LOG-1:0]	       		PRF_CDB_regA,

		      // Inputs from Dcache to CDB
		      input 					       	Dcache_CDB_valid,
		      input  [63:0]			       		Dcache_CDB_NPC,
		      input  [31:0]			       		Dcache_CDB_IR,
		      input  [`ROB_SIZE_LOG-1:0]	       		Dcache_CDB_ROB,
		      input	[`BRAT_SIZE_LOG:0]	       		Dcache_CDB_BRAT,
		      input  [63:0]	       				Dcache_CDB_value,
		      input  [`PRF_SIZE_LOG-1:0]	       		Dcache_CDB_regA,

		      //Inputs from BRAT CMT Stage
		      input [1:0]					BRAT_CMT_v,
		      input [1:0] [`BRAT_SIZE_LOG-1:0]			BRAT_CMT_num,

		      //Inputs from LQ
		      input [1:0] 					LQ_full,

		      ///////////
		      // Outputs
		      ///////////
		      
		      // Outputs for IS
		      output logic [3:0] [1:0] 				EX_IS_stall,

		      // Outputs for LSQ
		      output logic [1:0]        	                ALU_LSQ_valid,
		      output logic [1:0] [63:0]				ALU_LSQ_NPC,
		      output logic [1:0] [31:0]				ALU_LSQ_IR,
		      output logic [1:0] 		       		ALU_LSQ_ld_or_st,  // 0 for ld, 1 for st
		      output logic [1:0] [63:0]				ALU_LSQ_MEM_addr,
		      output logic [1:0] [`PRF_SIZE_LOG-1:0]	       	ALU_LSQ_regA,
		      output logic [1:0] [`ROB_SIZE_LOG-1:0]	       	ALU_LSQ_ROB,
		      output logic [1:0] [`BRAT_SIZE_LOG:0]	       	ALU_LSQ_BRAT,
		      output logic [1:0] [`SQ_SIZE_LOG-1:0]	       	ALU_LSQ_SQ_addr,
		      
		      // Outputs for CMP
		      output 	   [1:0] 	                	EX_CMP_valid,
		      output logic [1:0] [63:0]				EX_CMP_NPC,
		      output logic [1:0] [31:0] 			EX_CMP_IR,
		      output logic [1:0] [`PRF_SIZE_LOG-1:0]  		EX_CMP_dest,
		      output logic [1:0] [63:0] 			EX_CMP_result,
		      output logic [1:0] [`ROB_SIZE_LOG-1:0]    	EX_CMP_ROB,
		      output logic [1:0] 				EX_CMP_branch,
		      output logic [1:0] [63:0]				EX_CMP_branch_addr,
		      output 	   [1:0] [`BRAT_SIZE_LOG:0] 		EX_CMP_BRAT
		      );

   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 		FU_dest_out;
   logic [3:0] [1:0] [63:0] 				FU_NPC_out;
   logic [3:0] [1:0] [31:0] 				FU_IR_out;
   logic [3:0] [1:0] [`ROB_SIZE_LOG-1:0] 		FU_ROB_out;
   logic [1:0] 						FU_branch_out;
   logic [1:0] [63:0]					FU_branch_addr_out;
   logic [3:0] [1:0][63:0] 				FU_result_out;
   logic [3:0] [1:0] 					FU_valid_out;
   logic [3:0] [1:0] [`BRAT_SIZE_LOG:0] 		FU_BRAT_out;
  
   
   logic [2:0] 						FU_cdb_priority;
   logic [1:0] [3:0] [1:0] 				FU_cdb_choose;
   logic [3:0] [1:0] 					FU_rejected;
   logic [1:0] 						MULT_rejected;

   logic [1:0] 						EX_CMP_validreg;
   logic [1:0] [`BRAT_SIZE_LOG:0] 			EX_CMP_BRATreg;

   logic [1:0] 						ALU_LSQ_validreg;
   logic [1:0] [`BRAT_SIZE_LOG:0] 			ALU_LSQ_BRATreg;
   
   logic [1:0] 						ALU_LSQ_valid_tmp;
   logic [1:0] [63:0] 					ALU_LSQ_NPC_tmp;
   logic [1:0] [31:0] 					ALU_LSQ_IR_tmp;
   logic [1:0] 						ALU_LSQ_ld_or_st_tmp;  // 0 for ld; 1 for st
   logic [1:0] [63:0] 					ALU_LSQ_MEM_addr_tmp;
   logic [1:0] [`PRF_SIZE_LOG-1:0] 			ALU_LSQ_regA_tmp;
   logic [1:0] [`ROB_SIZE_LOG-1:0] 			ALU_LSQ_ROB_tmp;
   logic [1:0] [`BRAT_SIZE_LOG:0] 			ALU_LSQ_BRAT_tmp;
   logic [1:0] [`SQ_SIZE_LOG-1:0] 			ALU_LSQ_SQ_addr_tmp;
   
   
   genvar 						i,j,k;
   
   
   rps_8 cdb_chooser(.req(FU_valid_out),
		     .en(2'b11),
		     .count(FU_cdb_priority),
		     .gnt(FU_cdb_choose)
		     );
   
   assign FU_rejected = FU_valid_out & ~(FU_cdb_choose[0]| FU_cdb_choose[1]);
   assign EX_IS_stall[0] = FU_rejected[0];
   assign EX_IS_stall[1] = FU_rejected[1];
   assign EX_IS_stall[2] = MULT_rejected;   
   assign FU_cdb_priority = 3'b110;
   

   assign FU_valid_out[3][1] 	=  Dcache_CDB_valid;
   assign FU_NPC_out[3][1]  	=  Dcache_CDB_NPC; 
   assign FU_ROB_out[3][1]  	=  Dcache_CDB_ROB; 
   assign FU_IR_out[3][1]  	=  Dcache_CDB_IR; 
   assign FU_result_out[3][1]  	=  Dcache_CDB_value; 
   assign FU_dest_out[3][1]  	=  Dcache_CDB_regA;
   assign FU_BRAT_out[3][1]	=  Dcache_CDB_BRAT;
   
   assign FU_valid_out[3][0] 	=  PRF_CDB_valid;
   assign FU_NPC_out[3][0]  	=  PRF_CDB_NPC; 
   assign FU_ROB_out[3][0]  	=  PRF_CDB_ROB; 
   assign FU_IR_out[3][0]  	=  PRF_CDB_IR; 
   assign FU_result_out[3][0]  	=  PRF_CDB_value; 
   assign FU_dest_out[3][0]  	=  PRF_CDB_regA;
   assign FU_BRAT_out[3][0]	=  PRF_CDB_BRAT;

   generate for(i=0;i<2;i++)
     begin
	assign EX_CMP_valid[i] = (branch_mis & BRAT_squash[EX_CMP_BRAT[i]]) ? 0 : EX_CMP_validreg[i];
	assign EX_CMP_BRAT[i] = ((BRAT_CMT_v[1] && (EX_CMP_BRATreg[i] == {1'b0,BRAT_CMT_num[1]})) || (BRAT_CMT_v[0] && (EX_CMP_BRATreg[i] == {1'b0,BRAT_CMT_num[0]})) ) ? {1'b1,`BRAT_SIZE_LOG'b0} : EX_CMP_BRATreg[i];
     end
   endgenerate

   always_comb
     begin
	ALU_LSQ_valid = ALU_LSQ_valid_tmp;
	ALU_LSQ_NPC = ALU_LSQ_NPC_tmp;
	ALU_LSQ_IR = ALU_LSQ_IR_tmp;
	ALU_LSQ_ld_or_st = ALU_LSQ_ld_or_st_tmp;  // 0 for ld; 1 for st
	ALU_LSQ_MEM_addr = ALU_LSQ_MEM_addr_tmp;
	ALU_LSQ_regA = ALU_LSQ_regA_tmp;
	ALU_LSQ_ROB = ALU_LSQ_ROB_tmp;
	ALU_LSQ_BRAT = ALU_LSQ_BRAT_tmp;
	ALU_LSQ_SQ_addr = ALU_LSQ_SQ_addr_tmp;
	EX_IS_stall[3] = 2'b00;
	case(LQ_full)
	  2'b00:;
	  2'b01:
	    begin
	       if((ALU_LSQ_valid_tmp & ~ALU_LSQ_ld_or_st_tmp) == 2'b10)
		 begin
		    ALU_LSQ_valid[0] = ALU_LSQ_valid_tmp[1];
		    ALU_LSQ_NPC[0] = ALU_LSQ_NPC_tmp[1];
		    ALU_LSQ_IR[0] = ALU_LSQ_IR_tmp[1];
		    ALU_LSQ_ld_or_st[0] = ALU_LSQ_ld_or_st_tmp[1];  // 0 for ld; 1 for st
		    ALU_LSQ_MEM_addr[0] = ALU_LSQ_MEM_addr_tmp[1];
		    ALU_LSQ_regA[0] = ALU_LSQ_regA_tmp[1];
		    ALU_LSQ_ROB[0] = ALU_LSQ_ROB_tmp[1];
		    ALU_LSQ_BRAT[0] = ALU_LSQ_BRAT_tmp[1];
		    ALU_LSQ_SQ_addr[0] = ALU_LSQ_SQ_addr_tmp[1];
		    
		    ALU_LSQ_valid[1] = ALU_LSQ_ld_or_st_tmp[0] ? ALU_LSQ_valid_tmp[0] : 1'b0;
		    ALU_LSQ_NPC[1] = ALU_LSQ_NPC_tmp[0];
		    ALU_LSQ_IR[1] = ALU_LSQ_IR_tmp[0];
		    ALU_LSQ_ld_or_st[1] = ALU_LSQ_ld_or_st_tmp[0];  // 1 for ld; 0 for st
		    ALU_LSQ_MEM_addr[1] = ALU_LSQ_MEM_addr_tmp[0];
		    ALU_LSQ_regA[1] = ALU_LSQ_regA_tmp[0];
		    ALU_LSQ_ROB[1] = ALU_LSQ_ROB_tmp[0];
		    ALU_LSQ_BRAT[1] = ALU_LSQ_BRAT_tmp[0];
		    ALU_LSQ_SQ_addr[1] = ALU_LSQ_SQ_addr_tmp[0];
		 end // if (ALU_LSQ_valid_tmp == 2'b10)
	       else if((ALU_LSQ_valid_tmp & ~ALU_LSQ_ld_or_st_tmp) == 2'b11)
		 begin
		    ALU_LSQ_valid[1] = 1'b0;
		    EX_IS_stall[3] = 2'b10;
		 end
	    end
	  2'b10:;
	  2'b11:
	    begin
	       if(~ALU_LSQ_ld_or_st_tmp[0])
		 begin
		    ALU_LSQ_valid[0] = 1'b0;
		    EX_IS_stall[3][0] = ALU_LSQ_valid_tmp[0];
		 end
	       if(~ALU_LSQ_ld_or_st_tmp[1])
		 begin
		    ALU_LSQ_valid[1] = 1'b0;
		    EX_IS_stall[3][1] = ALU_LSQ_valid_tmp[1];
		 end
	    end
	endcase // case (LQ_full)
     end // always_comb begin

   wor [1:0] 						EX_CMP_valid_tmp;
   wor [1:0] [63:0] 					EX_CMP_NPC_tmp;
   wor [1:0] [31:0] 					EX_CMP_IR_tmp;
   wor [1:0] [`PRF_SIZE_LOG-1:0] 			EX_CMP_dest_tmp;
   wor [1:0] [63:0] 					EX_CMP_result_tmp;
   wor [1:0] [`ROB_SIZE_LOG-1:0] 			EX_CMP_ROB_tmp;
   wor [1:0] 						EX_CMP_branch_tmp;
   wor [1:0] [63:0] 					EX_CMP_branch_addr_tmp;
   wor [1:0] [`BRAT_SIZE_LOG:0] 			EX_CMP_BRAT_tmp;
   
   		      
								    
   generate for(i = 0;i < 2; i ++)
     begin
	for(j = 0; j < 4; j ++)
	  begin
	     for (k = 0; k < 2; k++)
	       begin
		  assign EX_CMP_valid_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_valid_out[j][k] : 0;
		  assign EX_CMP_NPC_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_NPC_out[j][k] : 0;
		  assign EX_CMP_IR_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_IR_out[j][k] : 0;
		  assign EX_CMP_dest_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_dest_out[j][k] : 0;
		  assign EX_CMP_result_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_result_out[j][k] : 0;
		  assign EX_CMP_ROB_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_ROB_out[j][k] : 0;
		  assign EX_CMP_BRAT_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_BRAT_out[j][k] : 0; 
		  assign EX_CMP_branch_tmp[i] 	= FU_cdb_choose[i][1][k] ?  FU_branch_out[k] : 0;  
		  assign EX_CMP_branch_addr_tmp[i] = FU_cdb_choose[i][1][k] ?  FU_branch_addr_out[k] : 0; 		    
	       end // for (k = 0; k < 2; k++)
	  end // for (j = 0; j < 4; j ++)
     end // for (i = 0;i < 2; i ++)
   endgenerate

   always_ff@(posedge clock)
     begin
	if(reset)
	  begin
	     EX_CMP_validreg 	<= `SD 0;
	     EX_CMP_NPC 	<= `SD 0;
	     EX_CMP_IR 		<= `SD 0;
	     EX_CMP_dest 	<= `SD 0;
	     EX_CMP_result 	<= `SD 0;
	     EX_CMP_ROB		<= `SD 0;
	     EX_CMP_branch	<= `SD 0;
	     EX_CMP_branch_addr	<= `SD 0;
	     EX_CMP_BRATreg     <= `SD 0;
	  end // if (reset)
	else
	  begin
	     EX_CMP_validreg 	<= `SD EX_CMP_valid_tmp;
	     EX_CMP_NPC 	<= `SD EX_CMP_NPC_tmp;
	     EX_CMP_IR 		<= `SD EX_CMP_IR_tmp;
	     EX_CMP_dest 	<= `SD EX_CMP_dest_tmp;
	     EX_CMP_result 	<= `SD EX_CMP_result_tmp;
	     EX_CMP_ROB		<= `SD EX_CMP_ROB_tmp;
	     EX_CMP_branch	<= `SD EX_CMP_branch_tmp;
	     EX_CMP_branch	<= `SD EX_CMP_branch_tmp;
	     EX_CMP_branch_addr	<= `SD EX_CMP_branch_addr_tmp;
	     EX_CMP_BRATreg     <= `SD EX_CMP_BRAT_tmp;
	  end // if (reset)
     end

   FU_ALU ALU0(
	       .clock(clock),               // system clock
	       .reset(reset),               // system reset
	       .ALU_valid(IS_EX_valid[0][0]),
	       .ALU_NPC(IS_EX_NPC[0][0]),
	       .ALU_IR(IS_EX_IR[0][0]),
	       .ALU_alu_func(IS_EX_alu_func[0][0]),
	       .ALU_regA(IS_EX_regA_value[0][0]),
	       .ALU_regB(IS_EX_regB_value[0][0]),
	       .ALU_opa_sel(IS_EX_opa_sel[0][0]),
	       .ALU_opb_sel(IS_EX_opb_sel[0][0]),
	       .ALU_dest(IS_EX_dest[0][0]),
	       .ALU_ROB(IS_EX_ROB[0][0]),
	       .ALU_BRAT(IS_EX_BRAT[0][0]),
	       
	       .FU_ALU_NPC_out(FU_NPC_out[0][0]),
	       .FU_ALU_IR_out(FU_IR_out[0][0]),
	       .FU_ALU_dest_out(FU_dest_out[0][0]),
	       .FU_ALU_ROB_out(FU_ROB_out[0][0]),
	       .FU_ALU_result_out(FU_result_out[0][0]),  
	       .FU_ALU_valid_out(FU_valid_out[0][0]),
	       .FU_ALU_BRAT_out(FU_BRAT_out[0][0])
	       );
   
   FU_ALU ALU1(
	       .clock(clock),               // system clock
	       .reset(reset),               // system reset
	       .ALU_valid(IS_EX_valid[0][1]),
	       .ALU_NPC(IS_EX_NPC[0][1]),
	       .ALU_IR(IS_EX_IR[0][1]),
	       .ALU_alu_func(IS_EX_alu_func[0][1]),
	       .ALU_regA(IS_EX_regA_value[0][1]),
	       .ALU_regB(IS_EX_regB_value[0][1]),
	       .ALU_opa_sel(IS_EX_opa_sel[0][1]),
	       .ALU_opb_sel(IS_EX_opb_sel[0][1]),
	       .ALU_dest(IS_EX_dest[0][1]),
	       .ALU_ROB(IS_EX_ROB[0][1]),
	       .ALU_BRAT(IS_EX_BRAT[0][1]),
	       
	       .FU_ALU_NPC_out(FU_NPC_out[0][1]),
	       .FU_ALU_IR_out(FU_IR_out[0][1]),
	       .FU_ALU_dest_out(FU_dest_out[0][1]),
	       .FU_ALU_ROB_out(FU_ROB_out[0][1]),
	       .FU_ALU_result_out(FU_result_out[0][1]),  
	       .FU_ALU_valid_out(FU_valid_out[0][1]),
	       .FU_ALU_BRAT_out(FU_BRAT_out[0][1])
	       );
   
   FU_BR BR0(
	     .clock(clock),               // system clock
	     .reset(reset),               // system reset
	     .BR_valid(IS_EX_valid[1][0]),
	     .BR_NPC(IS_EX_NPC[1][0]),
	     .BR_IR(IS_EX_IR[1][0]),
	     .BR_alu_func(IS_EX_alu_func[1][0]),
	     .BR_regA(IS_EX_regA_value[1][0]),
	     .BR_regB(IS_EX_regB_value[1][0]),
	     .BR_opa_sel(IS_EX_opa_sel[1][0]),
	     .BR_opb_sel(IS_EX_opb_sel[1][0]),
	     .BR_dest(IS_EX_dest[1][0]),
	     .BR_ROB(IS_EX_ROB[1][0]),
	     .BR_uncn_br(IS_EX_uncn_br[0]),
	     .BR_BRAT(IS_EX_BRAT[1][0]),
	     
	     .FU_BR_NPC_out(FU_NPC_out[1][0]),
	     .FU_BR_IR_out(FU_IR_out[1][0]),
	     .FU_BR_dest_out(FU_dest_out[1][0]),
	     .FU_BR_ROB_out(FU_ROB_out[1][0]),
	     .FU_BR_branch_out(FU_branch_out[0]),  // is this a taken branch?
	     .FU_BR_branch_addr_out(FU_branch_addr_out[0]),	
	     .FU_BR_result_out(FU_result_out[1][0]),   
	     .FU_BR_valid_out(FU_valid_out[1][0]),
	     .FU_BR_BRAT_out(FU_BRAT_out[1][0])
	     );
   
   FU_BR BR1(
	     .clock(clock),               // system clock
	     .reset(reset),               // system reset
	     .BR_valid(IS_EX_valid[1][1]),
	     .BR_NPC(IS_EX_NPC[1][1]),
	     .BR_IR(IS_EX_IR[1][1]),
	     .BR_alu_func(IS_EX_alu_func[1][1]),
	     .BR_regA(IS_EX_regA_value[1][1]),
	     .BR_regB(IS_EX_regB_value[1][1]),
	     .BR_opa_sel(IS_EX_opa_sel[1][1]),
	     .BR_opb_sel(IS_EX_opb_sel[1][1]),
	     .BR_dest(IS_EX_dest[1][1]),
	     .BR_ROB(IS_EX_ROB[1][1]),
	     .BR_uncn_br(IS_EX_uncn_br[1]),
	     .BR_BRAT(IS_EX_BRAT[1][1]),

	     .FU_BR_NPC_out(FU_NPC_out[1][1]),
	     .FU_BR_IR_out(FU_IR_out[1][1]),
	     .FU_BR_dest_out(FU_dest_out[1][1]),
	     .FU_BR_ROB_out(FU_ROB_out[1][1]),
	     .FU_BR_branch_out(FU_branch_out[1]),  // is this a taken branch?
	     .FU_BR_branch_addr_out(FU_branch_addr_out[1]),	
	     .FU_BR_result_out(FU_result_out[1][1]),   
	     .FU_BR_valid_out(FU_valid_out[1][1]),
	     .FU_BR_BRAT_out(FU_BRAT_out[1][1])
	     );

   
   
   FU_MULT MULT0(
		 .clock(clock),               // system clock
		 .reset(reset),               // system reset
		 .MULT_valid(IS_EX_valid[2][0]),
		 .MULT_stall(FU_rejected[2][0]),
		 .MULT_NPC(IS_EX_NPC[2][0]),
		 .MULT_IR(IS_EX_IR[2][0]),
		 .MULT_alu_func(IS_EX_alu_func[2][0]),
		 .MULT_regA(IS_EX_regA_value[2][0]),
		 .MULT_regB(IS_EX_regB_value[2][0]),
		 .MULT_opa_sel(IS_EX_opa_sel[2][0]),
		 .MULT_opb_sel(IS_EX_opb_sel[2][0]),
		 .MULT_dest(IS_EX_dest[2][0]),
		 .MULT_ROB(IS_EX_ROB[2][0]),
		 .MULT_BRAT(IS_EX_BRAT[2][0]),
		 .BRAT_squash(BRAT_squash),
		 .BRAT_CMT_v(BRAT_CMT_v),
		 .BRAT_CMT_num(BRAT_CMT_num),
		 
		 .FU_MULT_NPC_out(FU_NPC_out[2][0]),
		 .FU_MULT_IR_out(FU_IR_out[2][0]),
		 .FU_MULT_dest_out(FU_dest_out[2][0]),
		 .FU_MULT_ROB_out(FU_ROB_out[2][0]),
		 .FU_MULT_result_out(FU_result_out[2][0]),  // is this a taken branch?
		 .FU_MULT_valid_out(FU_valid_out[2][0]),
		 .FU_MULT_stall_out(MULT_rejected[0]),
		 .FU_MULT_BRAT_out(FU_BRAT_out[2][0])
		 );

   FU_MULT MULT1(
		 .clock(clock),               // system clock
		 .reset(reset),               // system reset
		 .MULT_valid(IS_EX_valid[2][1]),
		 .MULT_stall(FU_rejected[2][1]),
		 .MULT_NPC(IS_EX_NPC[2][1]),
		 .MULT_IR(IS_EX_IR[2][1]),
		 .MULT_alu_func(IS_EX_alu_func[2][1]),
		 .MULT_regA(IS_EX_regA_value[2][1]),
		 .MULT_regB(IS_EX_regB_value[2][1]),
		 .MULT_opa_sel(IS_EX_opa_sel[2][1]),
		 .MULT_opb_sel(IS_EX_opb_sel[2][1]),
		 .MULT_dest(IS_EX_dest[2][1]),
		 .MULT_ROB(IS_EX_ROB[2][1]),
		 .MULT_BRAT(IS_EX_BRAT[2][1]),
		 .BRAT_squash(BRAT_squash),
		 .BRAT_CMT_v(BRAT_CMT_v),
		 .BRAT_CMT_num(BRAT_CMT_num),
		 
		 .FU_MULT_NPC_out(FU_NPC_out[2][1]),
		 .FU_MULT_IR_out(FU_IR_out[2][1]),
		 .FU_MULT_dest_out(FU_dest_out[2][1]),
		 .FU_MULT_ROB_out(FU_ROB_out[2][1]),
		 .FU_MULT_result_out(FU_result_out[2][1]),  // is this a taken branch?
		 .FU_MULT_valid_out(FU_valid_out[2][1]),
		 .FU_MULT_stall_out(MULT_rejected[1]),
		 .FU_MULT_BRAT_out(FU_BRAT_out[2][1])
		 );
   
   FU_MEM MEM0(
	       .clock(clock),               // system clock
	       .reset(reset),               // system reset
	       
	       .MEM_valid(IS_EX_valid[3][0]),
	       .MEM_NPC(IS_EX_NPC[3][0]),
	       .MEM_IR(IS_EX_IR[3][0]),
	       .MEM_alu_func(IS_EX_alu_func[3][0]),
	       .MEM_regA(IS_EX_ls_regA[0]),
	       .MEM_regB(IS_EX_regB_value[3][0]),
	       .MEM_opa_sel(IS_EX_opa_sel[3][0]),
	       .MEM_opb_sel(IS_EX_opb_sel[3][0]),
	       .MEM_dest(IS_EX_dest[3][0]),
	       .MEM_ROB(IS_EX_ROB[3][0]),
	       .MEM_BRAT(IS_EX_BRAT[3][0]),
	       .MEM_ld_or_st(IS_EX_ld_or_st[0]),
	       .MEM_sq_num(IS_EX_out_sq_num[0]),

	       .FU_MEM_valid_out(ALU_LSQ_valid_tmp[0]),
	       .FU_MEM_NPC_out(ALU_LSQ_NPC_tmp[0]),
	       .FU_MEM_IR_out(ALU_LSQ_IR_tmp[0]),
	       .FU_MEM_ld_or_st_out(ALU_LSQ_ld_or_st_tmp[0]),
	       .FU_MEM_MEM_addr_out(ALU_LSQ_MEM_addr_tmp[0]), 
	       .FU_MEM_regA_out(ALU_LSQ_regA_tmp[0]),
	       .FU_MEM_ROB_out(ALU_LSQ_ROB_tmp[0]),  
	       .FU_MEM_BRAT_out(ALU_LSQ_BRAT_tmp[0]),
	       .FU_MEM_SQ_addr_out(ALU_LSQ_SQ_addr_tmp[0])
              );
   
   FU_MEM MEM1(
	       .clock(clock),               // system clock
	       .reset(reset),               // system reset
	       
	       .MEM_valid(IS_EX_valid[3][1]),
	       .MEM_NPC(IS_EX_NPC[3][1]),
	       .MEM_IR(IS_EX_IR[3][1]),
	       .MEM_alu_func(IS_EX_alu_func[3][1]),
	       .MEM_regA(IS_EX_ls_regA[1]),
	       .MEM_regB(IS_EX_regB_value[3][1]),
	       .MEM_opa_sel(IS_EX_opa_sel[3][1]),
	       .MEM_opb_sel(IS_EX_opb_sel[3][1]),
	       .MEM_dest(IS_EX_dest[3][1]),
	       .MEM_ROB(IS_EX_ROB[3][1]),
	       .MEM_BRAT(IS_EX_BRAT[3][1]),
	       .MEM_ld_or_st(IS_EX_ld_or_st[1]),
	       .MEM_sq_num(IS_EX_out_sq_num[1]),

	       .FU_MEM_valid_out(ALU_LSQ_valid_tmp[1]),
	       .FU_MEM_NPC_out(ALU_LSQ_NPC_tmp[1]),
	       .FU_MEM_IR_out(ALU_LSQ_IR_tmp[1]),
	       .FU_MEM_ld_or_st_out(ALU_LSQ_ld_or_st_tmp[1]),
	       .FU_MEM_MEM_addr_out(ALU_LSQ_MEM_addr_tmp[1]), 
	       .FU_MEM_regA_out(ALU_LSQ_regA_tmp[1]),
	       .FU_MEM_ROB_out(ALU_LSQ_ROB_tmp[1]),  
	       .FU_MEM_BRAT_out(ALU_LSQ_BRAT_tmp[1]),
	       .FU_MEM_SQ_addr_out(ALU_LSQ_SQ_addr_tmp[1])
	       );

endmodule

