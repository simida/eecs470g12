module Complete_Retire_Stage(
		
	  //////////
	  // Inputs
	  //////////
		input								 					   clock,
		input 													 reset,
	
		//Inputs from FE
		input [1:0] [`PRF_SIZE_LOG-1:0]	 FE_ID_dest_prfreg,
		input [1:0] [`ARF_SIZE_LOG-1:0]	 FE_ID_dest_arfreg,
		input	[1:0]							 				 ID_valid_inst,//is inst a valid instruction to be 00 01 10 11
	  
		//Inputs from decoder
		input [1:0]							 				 ID_branch,//is branch or not (diff)
		input [1:0] 									 	 ID_store, //is store or not
	  input [1:0] [63:0] 					  	 FE_ID_NPC,
    input [1:0]											 FE_ID_illegal,
		input [1:0]											 FE_ID_halt,	
		//Inputs from Execution Complete
		input	[1:0]					 		 				 EX_CMP_branch,//branch taken or not
		input [1:0] [63:0]				 		 	 EX_CMP_result,//ex_targetaddr
		input [1:0]  				 		 				 EX_CMP_valid,
		input [1:0] [`ROB_SIZE_LOG-1:0]  EX_CMP_ROB,
		//Inputs about STORE issued to dcache from lsq
		input														 SQ_CMP, 
		
		//Inputs from branch predictor
		input	[1:0]					 		 				 branch_pred,
		input [1:0] [63:0]   		 				 branch_predaddr,
		
	  //////////
	  //Outputs
	  /////////
		//ROB related output
		output logic [`ROB_SIZE_LOG-1:0]   ROB_head,
		output logic [`ROB_SIZE_LOG-1:0]	 ROB_tail,
	  output 			       								 ROB_full,
		output 			       								 ROB_full_1left,
			
		//Retire signals
		output reg [1:0]					 						   RET_issue_store,
		output logic [1:0] [`PRF_SIZE_LOG-1:0]   RET_prfn,
		output logic [1:0] [`ARF_SIZE_LOG-1:0]   RET_arfn,
		output reg [1:0] 											   RET_valid,//ROB_retire is valid
		output reg [1:0]											   RET_halt,
	  output reg [1:0]												 RET_illegal,
		
		//Outputs for early branch resolution
		output logic															BRAT_CMP_miss,
		output logic [`BRAT_SIZE_LOG-1:0]					BRAT_CMP_num,
		output logic [63:0]												BRAT_CMP_targetaddr,

		output logic [1:0]												BRAT_CMT_v,
		output logic [1:0] [`BRAT_SIZE_LOG-1:0]		BRAT_CMT_num,
		output logic [1:0]												BRAT_CMT_takeornot,
		output logic [1:0] [63:0]									BRAT_CMT_pc,
		output logic [1:0] [63:0]									BRAT_CMT_targetaddr,

		output logic [`BRAT_SIZE_LOG-1:0]					BRAT_tail,
		output logic 															BRAT_full,
		output logic 															BRAT_full_1left,

		output logic [`BRAT_SIZE-1:0]							BRAT_squash,//to RS, 1 should be squashed
		output logic															BRAT_empty
		);

wire [`ROB_SIZE_LOG-1:0] ROB_tail_p1;
wire [`ROB_SIZE_LOG-1:0] ROB_tail_p2;
wire [`ROB_SIZE_LOG-1:0] ROB_head_p1;
wire [`ROB_SIZE_LOG-1:0] ROB_head_p2;
wire [`ROB_SIZE_LOG:0]	 ROB_cur_size;
wire										 next_ROB_empty;
wire							 			 next_ROB_empty_1left;

wire						         ROB_retire1;
wire						         ROB_retire2;
wire [`ROB_SIZE_LOG:0]   next_ROB_iocnt;

logic [`ROB_SIZE_LOG-1:0] next_ROB_tail;
logic [`ROB_SIZE_LOG-1:0] next_ROB_head;
logic			  						  next_issue1_store;
logic							  		  next_issue2_store;
						
logic										 move_ROB_tail;
logic										 ROB_empty;
logic										 ROB_empty_1left;
logic [`ROB_SIZE_LOG:0]  ROB_iocnt;
logic [1:0]							 ROB_incount;
logic [1:0]							 ROB_outcount;

logic [`ROB_SIZE-1:0] [`PRF_SIZE_LOG-1:0] prfdest_regfl;
logic [`ROB_SIZE-1:0] [`ARF_SIZE_LOG-1:0] arfdest_regfl;
logic [`ROB_SIZE-1:0] [63:0]						  npc_regfl;
logic [`ROB_SIZE-1:0] 									  bran_v_regfl;
logic [`ROB_SIZE-1:0] 									  store_v_regfl;
logic [`ROB_SIZE-1:0] [63:0]    		      bran_predaddr_regfl;
logic [`ROB_SIZE-1:0] 			    		      bran_pred_ornot_regfl;
logic [`ROB_SIZE-1:0] [63:0]    		      bran_targetaddr_regfl;
logic [`ROB_SIZE-1:0] 				  		      bran_ornot_regfl;
logic [`ROB_SIZE-1:0] 									  ex_fin_regfl;
logic [`ROB_SIZE-1:0] 									  illegal_regfl;
logic [`ROB_SIZE-1:0] 									  halt_regfl;
logic [`BRAT_SIZE-1:0] [`ROB_SIZE_LOG-1:0] brat_entry_regfl;//need clear this when ROB_retired???
logic [`BRAT_SIZE-1:0] 										brat_entry_v_regfl;



//Early branch resolution
logic [1:0]												next_BRAT_branch_miss;
logic [1:0] [63:0]								next_BRAT_branch_addr;
logic [1:0]	[`BRAT_SIZE_LOG-1:0]	BRAT_fin_entry;
logic [`BRAT_SIZE-1:0]						next_BRAT_squash;
logic [1:0] [`ROB_SIZE_LOG:0]	    dis;//1 bit larger
logic 														move_BRAT_tail;
logic [63:0]										  next_BRAT_CMP_targetaddr;
logic [`BRAT_SIZE_LOG-1:0]				next_BRAT_CMP_num;
logic [`BRAT_SIZE_LOG-1:0]				next_BRAT_tail;
logic [`BRAT_SIZE_LOG-1:0]			  next_BRAT_head;
logic [`BRAT_SIZE_LOG:0]				  BRAT_iocnt;
logic [1:0]											  BRAT_incount;
logic [1:0]							          BRAT_outcount;
wire [`ROB_SIZE_LOG:0]   					next_BRAT_iocnt;


logic [`BRAT_SIZE_LOG-1:0]			BRAT_head;
wire [`BRAT_SIZE_LOG-1:0] BRAT_tail_p1;
wire [`BRAT_SIZE_LOG-1:0] BRAT_tail_p2;
wire [`BRAT_SIZE_LOG-1:0] BRAT_head_p1;
wire [`BRAT_SIZE_LOG-1:0] BRAT_head_p2;
wire [`BRAT_SIZE_LOG:0]	  BRAT_cur_size;
wire										  next_BRAT_empty;
wire							 			  next_BRAT_empty_1left;

logic											BRAT_empty_1left;


// For test use
logic [`BRAT_SIZE-1:0] 										brat_mis_regfl;
logic [1:0] 											next_brat_miss;



//ROB_ALLOCATE
assign ROB_tail_p1 = ROB_tail + 1'd1;
assign ROB_tail_p2 = ROB_tail + 2'd2;
assign ROB_head_p1 = ROB_head + 1'd1;
assign ROB_head_p2 = ROB_head + 2'd2;


assign ROB_cur_size = (next_ROB_tail >= next_ROB_head) ? (next_ROB_tail - next_ROB_head) : (next_ROB_tail + `ROB_SIZE - next_ROB_head); 
assign next_ROB_iocnt = (move_ROB_tail) ? ROB_cur_size : (ROB_iocnt+ROB_incount-ROB_outcount);//
assign ROB_full = ROB_iocnt ==`ROB_SIZE;
assign ROB_full_1left = ROB_iocnt == {`ROB_SIZE-1};
assign next_ROB_empty = next_ROB_iocnt == 0;
assign next_ROB_empty_1left = next_ROB_iocnt == 1;

//ROB_RETIRE
assign ROB_retire1 = !ROB_empty && ex_fin_regfl[ROB_head];
assign ROB_retire2 = !ROB_empty_1left && ROB_retire1 && ex_fin_regfl[ROB_head_p1] && !halt_regfl[ROB_head];  

//BRAT_ALLOCATE
assign BRAT_tail_p1 = BRAT_tail + 1'd1;
assign BRAT_tail_p2 = BRAT_tail + 2'd2;
assign BRAT_head_p1 = BRAT_head + 1'd1;
assign BRAT_head_p2 = BRAT_head + 2'd2;

assign BRAT_cur_size = (next_BRAT_tail >= next_BRAT_head) ? (next_BRAT_tail - next_BRAT_head) : (next_BRAT_tail + `BRAT_SIZE - next_BRAT_head); 
assign next_BRAT_iocnt = (move_BRAT_tail) ? BRAT_cur_size : (BRAT_iocnt + BRAT_incount- BRAT_outcount);//replace move_BRAT_tail with move_ROB_tail
assign BRAT_full = BRAT_iocnt ==`BRAT_SIZE;
assign BRAT_full_1left = BRAT_iocnt == {`BRAT_SIZE-1};
assign next_BRAT_empty = next_BRAT_iocnt == 0;
assign next_BRAT_empty_1left = next_BRAT_iocnt == 1;

//BRAT_RETIRE
assign BRAT_retire1 = !BRAT_empty && ex_fin_regfl[brat_entry_regfl[BRAT_head]];
assign BRAT_retire2 = !BRAT_empty_1left && BRAT_retire1 && ex_fin_regfl[brat_entry_regfl[BRAT_head_p1]];

always_comb begin

	//default
	next_ROB_head = ROB_head;
	next_ROB_tail = ROB_tail;
  move_ROB_tail = 1'b0;
  move_BRAT_tail = 1'b0;
	ROB_incount = 2'd0;
	ROB_outcount = 2'd0;
  
	next_issue1_store = 1'b0;
  next_issue2_store = 1'b0;
	
  //EBR default
	next_BRAT_head = BRAT_head;
	next_BRAT_tail = BRAT_tail;
	BRAT_incount = 2'd0;
	BRAT_outcount = 2'd0;
	
	next_BRAT_branch_miss = 2'b0;
	next_BRAT_branch_addr = 128'b0;
	dis = 0;
	next_BRAT_squash = {`BRAT_SIZE'b0};
	next_BRAT_CMP_targetaddr = 64'b0;
	next_BRAT_CMP_num= 0;

	BRAT_fin_entry=0;
	
	//ROB_HEAD
	if(ROB_retire1) begin
			next_ROB_head = ROB_head_p1;
			ROB_outcount = 2'd1;
	end
	
	if(ROB_retire2) begin
			next_ROB_head = ROB_head_p2;
			ROB_outcount = 2'd2;
	end

	//BRAT_HEAD
	if(BRAT_retire1) begin
			next_BRAT_head = BRAT_head_p1;
			BRAT_outcount = 2'd1;
	end
	
	if(BRAT_retire2) begin
			next_BRAT_head = BRAT_head_p2;
			BRAT_outcount = 2'd2;
	end

	//Early branch resolution
	if(EX_CMP_valid[0] && bran_v_regfl[EX_CMP_ROB[0]]) begin //if completed inst is a branch
		if (EX_CMP_branch[0]) begin
			next_BRAT_branch_addr[0]= EX_CMP_result[0];
			if(bran_pred_ornot_regfl[EX_CMP_ROB[0]])
				next_BRAT_branch_miss[0] = EX_CMP_result[0]!=bran_predaddr_regfl[EX_CMP_ROB[0]];
			else 
			  next_BRAT_branch_miss[0] = 1;
		end
		else begin
				next_BRAT_branch_miss[0] = bran_pred_ornot_regfl[EX_CMP_ROB[0]] ? 1 : 0;
				next_BRAT_branch_addr[0] = npc_regfl[EX_CMP_ROB[0]];
		end
	end
	
	if(EX_CMP_valid[1] && bran_v_regfl[EX_CMP_ROB[1]]) begin //if completed inst is a branch
		if (EX_CMP_branch[1]) begin
			next_BRAT_branch_addr[1]= EX_CMP_result[1];
			if(bran_pred_ornot_regfl[EX_CMP_ROB[1]])
				next_BRAT_branch_miss[1] = EX_CMP_result[1]!=bran_predaddr_regfl[EX_CMP_ROB[1]];
			else 
			  next_BRAT_branch_miss[1] = 1;
		end
		else begin
				next_BRAT_branch_miss[1] = bran_pred_ornot_regfl[EX_CMP_ROB[1]] ? 1 : 0;
				next_BRAT_branch_addr[1] = npc_regfl[EX_CMP_ROB[1]];
		end
	end

	if(next_BRAT_branch_miss == 2'b11) begin//select where to start squash
				for(int j=0;j<`BRAT_SIZE;j++) begin
							if(EX_CMP_ROB[0] == brat_entry_regfl[j] && brat_entry_v_regfl[j] )
									BRAT_fin_entry[0] = j;
				end
				dis[0] = BRAT_fin_entry[0]-BRAT_head;

				for(int j=0;j<`BRAT_SIZE;j++) begin
							if(EX_CMP_ROB[1] == brat_entry_regfl[j] && brat_entry_v_regfl[j] )
									BRAT_fin_entry[1] = j;
				end
				dis[1] = BRAT_fin_entry[1]-BRAT_head;
		
				if(dis[0]<dis[1]) begin
						next_BRAT_tail=BRAT_fin_entry[0]+1'b1;
						next_BRAT_CMP_targetaddr = EX_CMP_result[0];
						next_BRAT_CMP_num=BRAT_fin_entry[0];
						for(int m = 0; m<`BRAT_SIZE;m++)begin
							if (BRAT_fin_entry[0]<BRAT_tail) begin
								if(m>=BRAT_fin_entry[0] && m<BRAT_tail)//>= might have long latency
									next_BRAT_squash[m]=1'b1;
							end
							else begin
									if (m>=BRAT_fin_entry[0] || m<BRAT_tail)
										next_BRAT_squash[m]=1'b1;			
							end
						end
						next_ROB_tail = EX_CMP_ROB[0]+1'b1;
						move_ROB_tail = 1;
						move_BRAT_tail = BRAT_fin_entry[0] != (BRAT_tail-1'b1);
				end
				else begin
						next_BRAT_tail=BRAT_fin_entry[1]+1'b1;
						next_BRAT_CMP_targetaddr = EX_CMP_result[1];
						next_BRAT_CMP_num=BRAT_fin_entry[1];
						for(int m = 0; m<`BRAT_SIZE;m++)begin
								if (BRAT_fin_entry[1]<BRAT_tail) begin
									if(m>=BRAT_fin_entry[1] && m<BRAT_tail)//>= might have long latency
										next_BRAT_squash[m]=1'b1;
								end 
								else begin
									if (m>=BRAT_fin_entry[1] || m<BRAT_tail)
										next_BRAT_squash[m]=1'b1;			
								end
						end
						next_ROB_tail = EX_CMP_ROB[1]+1'b1;
						move_ROB_tail = 1;
						move_BRAT_tail = BRAT_fin_entry[1] != (BRAT_tail-1'b1);
				end
				
	end

	if(next_BRAT_branch_miss == 2'b01) begin //target addr
			for(int j=0;j<`BRAT_SIZE;j++) begin
							if(EX_CMP_ROB[0] == brat_entry_regfl[j] && brat_entry_v_regfl[j])
									BRAT_fin_entry[0] = j;
				end			

				for(int j=0;j<`BRAT_SIZE;j++) begin
							if(EX_CMP_ROB[1] == brat_entry_regfl[j] && brat_entry_v_regfl[j])
									BRAT_fin_entry[1] = j;
				end
		  next_BRAT_tail=BRAT_fin_entry[0]+1'b1;
			next_BRAT_CMP_targetaddr = EX_CMP_result[0];
			next_BRAT_CMP_num=BRAT_fin_entry[0];
			next_ROB_tail = EX_CMP_ROB[0]+1'b1;
			move_ROB_tail = 1;
			move_BRAT_tail = BRAT_fin_entry[0] != (BRAT_tail-1'b1);
			for(int m = 0; m<`BRAT_SIZE;m++)begin
					if (BRAT_fin_entry[0]<BRAT_tail) begin
						if(m>=BRAT_fin_entry[0] && m<BRAT_tail)//>= might have long latency
							next_BRAT_squash[m]=1'b1;
					end
					else begin
						if (m>=BRAT_fin_entry[0] || m<BRAT_tail)
							next_BRAT_squash[m]=1'b1;			
					end
			end
			
	end
	if(next_BRAT_branch_miss == 2'b10) begin //target addr
			for(int j=0;j<`BRAT_SIZE;j++) begin
							if(EX_CMP_ROB[0] == brat_entry_regfl[j] && brat_entry_v_regfl[j] )
									BRAT_fin_entry[0] = j;
				end

			for(int j=0;j<`BRAT_SIZE;j++) begin
							if(EX_CMP_ROB[1] == brat_entry_regfl[j] && brat_entry_v_regfl[j])
									BRAT_fin_entry[1] = j;
				end
		  next_BRAT_tail=BRAT_fin_entry[1]+1'b1;
			next_BRAT_CMP_targetaddr = EX_CMP_result[1];
			next_BRAT_CMP_num=BRAT_fin_entry[1];
			next_ROB_tail = EX_CMP_ROB[1]+1'b1;
			move_ROB_tail = 1;
			move_BRAT_tail = BRAT_fin_entry[1] != (BRAT_tail-1'b1);
			for(int m = 0; m<`BRAT_SIZE;m++)begin
					if (BRAT_fin_entry[1]<BRAT_tail) begin
						if(m>=BRAT_fin_entry[1] && m<BRAT_tail)//>= might have long latency
							next_BRAT_squash[m]=1'b1;
					end 
					else begin
						if (m>=BRAT_fin_entry[1] || m<BRAT_tail)
							next_BRAT_squash[m]=1'b1;			
					end
			end
	end

	if (next_BRAT_branch_miss) begin
		if (ID_valid_inst[0] && ID_branch[0] && ID_valid_inst[1] && ID_branch[1]) begin
			  next_BRAT_squash[BRAT_tail] = 1'b1;
			  next_BRAT_squash[BRAT_tail_p1] = 1'b1;
		end
		else if ((ID_valid_inst[0] && ID_branch[0]) || (ID_valid_inst[1] && ID_branch[1])) begin
				next_BRAT_squash[BRAT_tail] = 1'b1;
		end
	end
	
	//TAIL
	if(!move_ROB_tail) begin
			if(ID_valid_inst[0]) begin//
				next_ROB_tail = ROB_tail_p1;
				ROB_incount = 2'd1;
				
				if(ID_branch[0]) begin
					next_BRAT_tail = BRAT_tail_p1;
					BRAT_incount = 2'd1;
				end

				if(ID_valid_inst[1]) begin
					next_ROB_tail = ROB_tail_p2;
					ROB_incount = 2'd2;

					if(ID_branch[1]) begin
						next_BRAT_tail = ID_branch[0] ? BRAT_tail_p2 : BRAT_tail_p1;
						BRAT_incount = ID_branch[0] ?  2'd2 : 2'd1;
					end
				end
			end
  end

 //STORE
	next_issue1_store = store_v_regfl[next_ROB_head];
	next_issue2_store = !bran_v_regfl[next_ROB_head] && store_v_regfl[next_ROB_head+1'b1];//F
end //always_comb

// ===================================================
// Sequential Block
// ===================================================

always_ff @(posedge clock) begin
	if(reset) begin
		ROB_head 	  				<= `SD {`ROB_SIZE_LOG'b0};
		ROB_tail 			  		<= `SD {`ROB_SIZE_LOG'b0};
		ROB_iocnt			  		<= `SD {`ROB_SIZE_LOG+1{1'b0}}; 
		ROB_empty			  		<= `SD 1'b1; 
		ROB_empty_1left 		<= `SD 1'b0; 
		RET_issue_store[0]  <= `SD 1'b0;
		RET_issue_store[1]  <= `SD 1'b0;
		RET_valid[0]    		<= `SD 1'b0;
		RET_valid[1]    		<= `SD 1'b0;
		
		BRAT_head 	  			<= `SD {`BRAT_SIZE_LOG'b0};
		BRAT_tail 			  	<= `SD {`BRAT_SIZE_LOG'b0};
		BRAT_iocnt			  	<= `SD {`BRAT_SIZE_LOG+1{1'b0}}; 
		BRAT_empty			  	<= `SD 1'b1; 
		BRAT_empty_1left 		<= `SD 1'b0; 
		BRAT_CMP_miss 			<= `SD 1'b0;         
		BRAT_CMP_num 			  <= `SD {`BRAT_SIZE_LOG'b0};         
  	BRAT_CMP_targetaddr <= `SD 64'b0; 
  	BRAT_CMT_v  				<= `SD 2'b0;
  	BRAT_CMT_num  			<= `SD {2{`BRAT_SIZE_LOG'b0}}; 
  	BRAT_CMT_takeornot  <= `SD 2'b0;
  	BRAT_CMT_pc					<= `SD 128'b0;
  	BRAT_CMT_targetaddr <= `SD 128'b0;
		BRAT_tail						 <= `SD {`BRAT_SIZE_LOG'b0};
		BRAT_squash					 <= `SD {`BRAT_SIZE'b0};
		RET_halt             <= `SD 2'b0;
		RET_illegal					<= `SD 2'b0;

		for(int m = 0; m < `ROB_SIZE; m++) begin
			ex_fin_regfl[m] <= `SD 1'b0;
			halt_regfl[m]   <= `SD 1'b0;
		  illegal_regfl[m] <= `SD 1'b0;
			bran_pred_ornot_regfl[m] <= `SD 64'b0;
			store_v_regfl[m] <= `SD 1'b0;
			bran_v_regfl[m] <= `SD 1'b0;
		end
		for(int m=0;m<`BRAT_SIZE;m++) begin
			brat_entry_v_regfl[m] <= `SD 1'b0;
			brat_mis_regfl[m] <= `SD 1'b0;
		end
	end//reset
	else begin		
		RET_issue_store[0]	 <= `SD next_issue1_store;
		RET_issue_store[1]	 <= `SD next_issue2_store;
		RET_valid[0]  		   <= `SD ROB_retire1;
		RET_valid[1]  		   <= `SD ROB_retire2;
		RET_illegal[0] 		   <= `SD !ROB_empty && illegal_regfl[ROB_head];
		RET_illegal[1] 		   <= `SD !ROB_empty_1left && ROB_retire1 && illegal_regfl[ROB_head_p1];
		ROB_head 	 					 <= `SD next_ROB_head;
		ROB_tail 			 			 <= `SD next_ROB_tail;
		ROB_iocnt			 			 <= `SD next_ROB_iocnt; 
		ROB_empty 		 			 <= `SD next_ROB_empty; 
		ROB_empty_1left			 <= `SD next_ROB_empty_1left; 
		
		BRAT_head 	 				 <= `SD next_BRAT_head;
		BRAT_iocnt			 		 <= `SD next_BRAT_iocnt; 
		BRAT_empty 		 			 <= `SD next_BRAT_empty; 
		BRAT_empty_1left		 <= `SD next_BRAT_empty_1left; 
		BRAT_CMP_miss 			 <= `SD	next_BRAT_branch_miss[0] | next_BRAT_branch_miss[1];         
  	BRAT_CMP_num			   <= `SD next_BRAT_CMP_num; 
  	BRAT_CMP_targetaddr  <= `SD next_BRAT_CMP_targetaddr; 
		BRAT_tail						 <= `SD next_BRAT_tail;
		BRAT_squash					 <= `SD next_BRAT_squash;
	
	//ALLOCATE 
		if(ID_valid_inst[0]) begin
				prfdest_regfl[ROB_tail]		   	 	<= `SD FE_ID_dest_prfreg[0];
				arfdest_regfl[ROB_tail]		   	 	<= `SD FE_ID_dest_arfreg[0];
				npc_regfl[ROB_tail]				   	 	<= `SD FE_ID_NPC[0];
				bran_v_regfl[ROB_tail]          <= `SD ID_branch[0];
				store_v_regfl[ROB_tail]         <= `SD ID_store[0];
				bran_predaddr_regfl[ROB_tail]	  <= `SD branch_predaddr[0]; 
				bran_pred_ornot_regfl[ROB_tail] <= `SD branch_pred[0]; 
				ex_fin_regfl[ROB_tail]          <= `SD FE_ID_halt[0]; 
				illegal_regfl[ROB_tail]				  <= `SD FE_ID_illegal[0];
				halt_regfl[ROB_tail]						<= `SD FE_ID_halt[0];

				if(ID_branch[0] && !(next_BRAT_branch_miss)) begin
					 brat_entry_regfl[BRAT_tail]	 <= `SD ROB_tail;	
					 brat_entry_v_regfl[BRAT_tail] <= `SD 1'b1;
					 brat_mis_regfl[BRAT_tail] <= `SD 1'b0;
				end

				if(ID_valid_inst[1]) begin
						prfdest_regfl[ROB_tail_p1]		   	 <= `SD FE_ID_dest_prfreg[1];
						arfdest_regfl[ROB_tail_p1]		   	 <= `SD FE_ID_dest_arfreg[1];
						npc_regfl[ROB_tail_p1]				   	 <= `SD FE_ID_NPC[1];
						bran_v_regfl[ROB_tail_p1]          <= `SD ID_branch[1];
						store_v_regfl[ROB_tail_p1]         <= `SD ID_store[1];
						bran_predaddr_regfl[ROB_tail_p1]	 <= `SD branch_predaddr[1]; 
						bran_pred_ornot_regfl[ROB_tail_p1] <= `SD branch_pred[1]; 
						ex_fin_regfl[ROB_tail_p1]          <= `SD FE_ID_halt[1]; 
						illegal_regfl[ROB_tail_p1]				 <= `SD FE_ID_illegal[1];
						halt_regfl[ROB_tail_p1]						 <= `SD FE_ID_halt[1];
				  if (!next_BRAT_branch_miss) begin
						if(ID_branch[1] & ID_branch[0]) begin
						 brat_entry_regfl[BRAT_tail_p1] <= `SD ROB_tail_p1;	
						 brat_entry_v_regfl[BRAT_tail_p1] <= `SD 1'b1;
						end
						else if(ID_branch[1] & ~ID_branch[0]) begin
						 brat_entry_regfl[BRAT_tail] <= `SD ROB_tail_p1;	
						 brat_entry_v_regfl[BRAT_tail] <= `SD 1'b1;
						 brat_mis_regfl[BRAT_tail] <= `SD 1'b0;
						end
					end
				end
		end
		
		//EXECUTION COMPLETE 
		if(EX_CMP_valid[0]) begin
				ex_fin_regfl[EX_CMP_ROB[0]]          <= `SD 1'b1; 
				bran_targetaddr_regfl[EX_CMP_ROB[0]] <= `SD EX_CMP_result[0]; 
				bran_ornot_regfl[EX_CMP_ROB[0]]			 <= `SD EX_CMP_branch[0];
				if (next_BRAT_branch_miss[0])
					brat_mis_regfl[BRAT_fin_entry[0]] <= `SD next_BRAT_branch_miss[0];
		end
	
		if(EX_CMP_valid[1]) begin
				ex_fin_regfl[EX_CMP_ROB[1]]          <= `SD 1'b1; 
				bran_targetaddr_regfl[EX_CMP_ROB[1]] <= `SD EX_CMP_result[1]; 
				bran_ornot_regfl[EX_CMP_ROB[1]]			 <= `SD EX_CMP_branch[1];
				if (next_BRAT_branch_miss[1])
					brat_mis_regfl[BRAT_fin_entry[1]] <= `SD next_BRAT_branch_miss[1];
		end

		if(SQ_CMP && store_v_regfl[ROB_head]) begin//must be store
			  ex_fin_regfl[ROB_head] <= `SD 1'b1;	
		end

 		//BRAT SQUASH
	  if(next_BRAT_branch_miss == 2'b11) begin//select where to start squash
				if(dis[0]<dis[1]) begin
						for(int m = 0; m<`BRAT_SIZE;m++)begin
							if (BRAT_fin_entry[0]<BRAT_tail) begin
								if(m>=BRAT_fin_entry[0] && m<BRAT_tail)//>= might have long latency
									brat_entry_v_regfl[m] <= `SD 1'b0;
							end 
							else begin
								if (m>=BRAT_fin_entry[0] || m<BRAT_tail)
									brat_entry_v_regfl[m] <= `SD 1'b0;			
							end
						end
				end
				else begin
						for(int m = 0; m<`BRAT_SIZE;m++)begin
							if (BRAT_fin_entry[1]<BRAT_tail) begin
								if(m>=BRAT_fin_entry[1] && m<BRAT_tail)//>= might have long latency
									brat_entry_v_regfl[m] <= `SD 1'b0;
							end 
							else begin
								if (m>=BRAT_fin_entry[1] || m<BRAT_tail)
									brat_entry_v_regfl[m] <= `SD 1'b0;			
							end
						end
				end				
	  end
	  else if(next_BRAT_branch_miss == 2'b01) begin //target addr
			for(int m = 0; m<`BRAT_SIZE;m++)begin
					if (BRAT_fin_entry[0]<BRAT_tail) begin
						if(m>BRAT_fin_entry[0] && m<BRAT_tail)//>= might have long latency
							brat_entry_v_regfl[m] <= `SD 1'b0;
					end 
					else begin
						if (m>BRAT_fin_entry[0] || m<BRAT_tail)
							brat_entry_v_regfl[m] <= `SD 1'b0;			
					end
			end
			
	  end
	  else if(next_BRAT_branch_miss == 2'b10) begin //target addr
			for(int m = 0; m<`BRAT_SIZE;m++)begin
					if (BRAT_fin_entry[1]<BRAT_tail) begin
						if(m>BRAT_fin_entry[1] && m<BRAT_tail)//>= might have long latency
							brat_entry_v_regfl[m] <= `SD 1'b0;			
					end 
					else begin
						if (m>BRAT_fin_entry[1] || m<BRAT_tail)
							brat_entry_v_regfl[m] <= `SD 1'b0;			
					end
			end
	  end

		//RETIRE 
		if(ROB_retire1) begin
			 RET_prfn[0]        <= `SD prfdest_regfl[ROB_head];
			 RET_arfn[0]        <= `SD arfdest_regfl[ROB_head];
			 RET_halt[0]				<= `SD halt_regfl[ROB_head];		
		end
		
		if(ROB_retire2) begin
			 RET_prfn[1]        <= `SD prfdest_regfl[ROB_head_p1];
			 RET_arfn[1]        <= `SD arfdest_regfl[ROB_head_p1];
			 RET_halt[1]				<= `SD halt_regfl[ROB_head_p1] ;
		end
		//BRAT RETIRE
  	BRAT_CMT_v						<= `SD {BRAT_retire2,BRAT_retire1};
		if(BRAT_retire1)begin
  		BRAT_CMT_takeornot[0] <= `SD bran_ornot_regfl[brat_entry_regfl[BRAT_head]]; 
  		BRAT_CMT_pc[0]				<= `SD npc_regfl[brat_entry_regfl[BRAT_head]]-64'd4;
  	  BRAT_CMT_targetaddr[0]<= `SD bran_targetaddr_regfl[brat_entry_regfl[BRAT_head]]; 
			BRAT_CMT_num[0]				<= `SD BRAT_head;
			brat_entry_v_regfl[BRAT_head] <= `SD 1'b0;					
		end
		if(BRAT_retire2)begin
			BRAT_CMT_takeornot[1] <= `SD bran_ornot_regfl[brat_entry_regfl[BRAT_head_p1]]; 
  		BRAT_CMT_pc[1]				<= `SD npc_regfl[brat_entry_regfl[BRAT_head_p1]]-64'd4;
  	  BRAT_CMT_targetaddr[1]<= `SD bran_targetaddr_regfl[brat_entry_regfl[BRAT_head_p1]]; 
			BRAT_CMT_num[1]				<= `SD BRAT_head_p1;
			brat_entry_v_regfl[BRAT_head_p1] <= `SD 1'b0;					
		end

	end
end//always_ff





endmodule
