/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  RS.v   	                                       //
//                                                                     //
//  Description :  This module is the single Reservation Station of    // 
//                 the Pipeline.                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

/****************************************************************
 			NEXT_D_I_Decoders
 ****************************************************************/
module Next_D_I_Decoder_4(current_D_I, next_D_I);

   // INPUTS
   input [3 : 0] current_D_I;
   // OUTPUTS
   output logic [1 : 0] next_D_I;
   
   always_comb
     begin
	if(current_D_I[0]) next_D_I = 2'h1;
	else if(current_D_I[1]) next_D_I = 2'h2;
	else if(current_D_I[2]) next_D_I = 2'h3;
	else next_D_I = 2'h0;   		  
     end

endmodule

/****************************************************************/

module Next_D_I_Decoder_8(current_D_I, next_D_I);

   // INPUTS
   input [7 : 0] current_D_I;
   output logic [2 : 0] next_D_I;
   
   always_comb
   begin
	if(current_D_I[0]) next_D_I = 3'h1;
	else if(current_D_I[1]) next_D_I = 3'h2;
	else if(current_D_I[2]) next_D_I = 3'h3;
	else if(current_D_I[3]) next_D_I = 3'h4;
	else if(current_D_I[4]) next_D_I = 3'h5;
	else if(current_D_I[5]) next_D_I = 3'h6;
	else if(current_D_I[6]) next_D_I = 3'h7;
	else next_D_I = 3'h0;   		  
  end
	
endmodule // Next_D_I_Decoder_8

/****************************************************************
 ****************************************************************
 ****************************************************************
 				RS_MEM
 ****************************************************************
 ****************************************************************
 ****************************************************************/

module RS_MEM(
	      //////////
	      // Inputs
	      //////////
	      
	      input 				        reset,            // Syetem Reset
	      input 				        clock,            // System Clock
	      
	      // CONTROL SIGNALS
	      
	      input     			       	branch_mis,       // Branch Misprediction from ROB. 00 if no, 01 if first mis, 10 if second mis, 11 if all miss
	      input [`BRAT_SIZE:0]			branch_mis_brat,  // BRAT number of branch mis
	      input [`PRF_SIZE-1:0]		        valid_list,       // Read Valid List to see who can be executed
	      input [1:0]			        RS_in_valid,      // IF there is one/two Instructions dispatched
	      input [1:0]                              	RS_in_not_erase,  // Signal from RS Arbiter. It tells each RS how many entries will they actually issue
	      
	      // DISPATCHED INSTRUCTION INFORMATION
	      
	      input [1:0] [63:0] 			RS_in_NPC,        // NPC for Dispatched INS
	      input [1:0] [31:0] 			RS_in_IR,         // IR for Dispatched INS  
	      input [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	RS_in_alu_func,   // ALU FUNCTION for Dispatched INS   
	      input [1:0] [`PRF_SIZE_LOG - 1:0]        	RS_in_regA,       // reg#/intermediate for REGA of Dispatched INS.
	      input [1:0] [`PRF_SIZE_LOG - 1:0]        	RS_in_regB,       // reg#/intermediate for REGB of Dispatched INS
	      input [1:0] [1:0] 		       	RS_in_opa_sel,    // Notification bit for RS_in_opa
	      input [1:0] [1:0] 		       	RS_in_opb_sel,    // Notification bit for RS_in_opb
	      input [1:0] [`PRF_SIZE_LOG-1:0] 	 	RS_in_dest,       // Destination PRF Number for Dispatched INS
	      input [1:0] [`ROB_SIZE_LOG-1:0] 		RS_in_ROB,        // ROB Number for Dispatched INS
	      input [1:0] [`BRAT_SIZE_LOG:0]		RS_in_BRAT,	  // BRAT Number for Dispatched INS
	      input [`SQ_SIZE_LOG-1:0]			RS_in_sq_tail,
	      
	      
	      // Early Branch Resolution
	      input [1:0]				BRAT_CMT_v,	
	      input [1:0][`BRAT_SIZE_LOG-1:0]		BRAT_CMT_num,
	      
	      //////////
	      //Outputs
	      //////////
	      
	      // Outputs for FU
	      
	      output  [1:0]		   	       	RS_out_valid,
	      output  [1:0] [63:0] 	   	       	RS_out_NPC,
	      output  [1:0] [31:0] 	   	       	RS_out_IR,
	      output  [1:0] [`ALU_FUNC_NUM_LOG-1:0]    	RS_out_alu_func,
	      output  [1:0] [`PRF_SIZE_LOG-1:0]         RS_out_regA,
	      output  [1:0] [`PRF_SIZE_LOG-1:0]         RS_out_regB,
	      output  [1:0] [1:0] 		       	RS_out_opa_sel,
	      output  [1:0] [1:0] 		       	RS_out_opb_sel,
	      output  [1:0] [`PRF_SIZE_LOG-1:0]         RS_out_dest,
	      output  [1:0] [`ROB_SIZE_LOG-1:0]         RS_out_ROB,
	      output  [1:0] [`BRAT_SIZE_LOG:0]	       	RS_out_BRAT,	
	      output  [1:0]			       	RS_out_ld_or_st,    // whether it is a ld or st
	      output  [1:0] [`SQ_SIZE_LOG-1:0]		RS_out_sq_num,
	      
	      // Outputs for front End
	      
	      output [1:0] 			       	RS_full
	      
	      );
   
   // Head/Tail for determining next dispatch/issue priority
   
   reg [`RS_MEM_SIZE_LOG-1:0] 				head, tail;
   wire [1:0] [`RS_MEM_SIZE_LOG-1:0] 			next_head, next_tail;

   // All Values stored in RS
   
   reg [`RS_MEM_SIZE-1:0] [63:0] 			NPC;
   reg [`RS_MEM_SIZE-1:0] [31:0] 			IR;
   reg [`RS_MEM_SIZE-1:0] [`ALU_FUNC_NUM_LOG-1:0] 	alu_func; 
   reg [`RS_MEM_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		opa;        //contains RegA#/opa_sel
   reg [`RS_MEM_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		opb;        //contains RegB#/opb_sel
   reg [`RS_MEM_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		dest;        
   reg [`RS_MEM_SIZE-1:0] 				opb_valid;             
   reg [`RS_MEM_SIZE-1:0] [1:0]				opa_sel;   //indicate whether Reg#/sel
   reg [`RS_MEM_SIZE-1:0] [1:0]				opb_sel;   //indicate whether Reg#/sel
   reg [`RS_MEM_SIZE-1:0] [`ROB_SIZE_LOG-1:0] 		ROB;
   reg [`RS_MEM_SIZE-1:0] [`BRAT_SIZE_LOG:0] 		BRAT;
   reg [`RS_MEM_SIZE-1:0]  				ld_or_st;
   reg [`RS_MEM_SIZE-1:0] [`SQ_SIZE_LOG-1:0] 		sq_num;
   reg [`RS_MEM_SIZE-1:0] 				in_use;            

   // Wires for Issue/Dispatch logics
   
   wire [1:0] [`RS_MEM_SIZE-1:0] 			rs_load_in;
   wire [1:0] [`RS_MEM_SIZE-1:0] 			rs_free_in;
   wire [1:0] [`RS_MEM_SIZE-1:0] 			rs_free_in_tmp;
   wire [`RS_MEM_SIZE-1:0] 				rs_avail_out;
   wire [`RS_MEM_SIZE-1:0] 				rs_ready_out;
   wire [1:0]						num_of_avail;
   wire [1:0] 						rs_ld_or_st_in;
   wire [1:0] [`SQ_SIZE_LOG-1:0] 		       	rs_sq_num_in;
   

   // Output Wires

   wor [1:0] 						RS_out_valid_tmp; 
   wor [1:0] [63:0] 					RS_out_NPC_tmp;
   wor [1:0] [31:0] 					RS_out_IR_tmp;
   wor [1:0] [`ALU_FUNC_NUM_LOG-1:0] 			RS_out_alu_func_tmp;
   wor [1:0] [`PRF_SIZE_LOG-1:0] 			RS_out_regA_tmp;
   wor [1:0] [`PRF_SIZE_LOG-1:0] 			RS_out_regB_tmp;
   wor [1:0] [1:0] 					RS_out_opa_sel_tmp;
   wor [1:0] [1:0] 					RS_out_opb_sel_tmp;
   wor [1:0] [`PRF_SIZE_LOG-1:0] 			RS_out_dest_tmp;
   wor [1:0] [`ROB_SIZE_LOG-1:0] 			RS_out_ROB_tmp;  
   wor [1:0] 						RS_out_ld_or_st_tmp;
   wor [1:0] [`SQ_SIZE_LOG-1:0] 			RS_out_sq_num_tmp;
   wor [1:0] [`BRAT_SIZE_LOG:0] 			RS_out_BRAT_tmp;  

   genvar j,k;
   
   generate 
	for (j=0; j<`RS_MEM_SIZE; j=j+1) begin
		assign rs_avail_out[j] = ~in_use[j]; 
		assign rs_ready_out[j] = in_use[j] & opb_valid[j] & (!branch_mis_brat | !branch_mis_brat[BRAT[j]]); 
	end
   endgenerate

   `NoO(`RS_MEM_SIZE) N_of_avail_out_calc (.Nin(rs_avail_out/*|rs_free_in[0]|rs_free_in[1]*/), .Nout(num_of_avail));
   
   assign RS_full = (!num_of_avail) ? 2'b11 : (num_of_avail == 2'b10 | num_of_avail == 2'b01);
   
   assign rs_ld_or_st_in = {(RS_in_IR[1][31:26] == `STQ_INST), (RS_in_IR[0][31:26] == `STQ_INST)} & RS_in_valid;
   assign rs_sq_num_in = (rs_ld_or_st_in == 2'b11) ? {RS_in_sq_tail+`SQ_SIZE_LOG'h1, RS_in_sq_tail} :
			 (rs_ld_or_st_in == 2'b10) ? {RS_in_sq_tail, `SQ_SIZE_LOG'h0} :
			 (rs_ld_or_st_in == 2'b01) ? {`SQ_SIZE_LOG'h0, RS_in_sq_tail} :
			 {`SQ_SIZE_LOG'h0, `SQ_SIZE_LOG'h0};
   
   
   //Arbiters for dispatch in & issue out  
   `RPS(`RS_MEM_SIZE)  rps_dispatch (.req(rs_avail_out/*|rs_free_in[0]|rs_free_in[1]*/), .en(RS_in_valid), .gnt(rs_load_in), .count(tail));
   `RPS(`RS_MEM_SIZE)  rps_issue (.req(rs_ready_out), .en(2'b11), .gnt(rs_free_in_tmp), .count(head));

   assign rs_free_in[0] = (RS_in_not_erase[0]) ? 0 : rs_free_in_tmp[0];
   assign rs_free_in[1] = (RS_in_not_erase[1]) ? 0 : rs_free_in_tmp[1];
   
   `DECODER(`RS_MEM_SIZE) I_dec_0(.current_D_I(rs_free_in[0]), .next_D_I(next_head[0]));
   `DECODER(`RS_MEM_SIZE) D_dec_0(.current_D_I(rs_load_in[0]), .next_D_I(next_tail[0]));
   `DECODER(`RS_MEM_SIZE) I_dec_1(.current_D_I(rs_free_in[1]), .next_D_I(next_head[1]));
   `DECODER(`RS_MEM_SIZE) D_dec_1(.current_D_I(rs_load_in[1]), .next_D_I(next_tail[1]));

   // Head Tail Changes
   
   always @(posedge clock) begin
	if (reset) begin
		head <= `SD 3'b000;
		tail <= `SD 3'b000;
	end
	else begin
		if (rs_free_in[1])
			head <= `SD next_head[1];
		else if (rs_free_in[0])
			head <= `SD next_head[0];
		if (rs_load_in[1])
			tail <= `SD next_tail[1];
		else if (rs_load_in[0])
			tail <= `SD next_tail[0]; 
	end
   end // always @ (posedge clock

   // Posedge Clock Processing

   generate for (j=0; j<`RS_MEM_SIZE; j=j+1) begin : dispatch
      always @(posedge clock) begin
	 if (reset) begin
	    // Reset when reset == 1 or Branch Mispredict
	    NPC[j] <= `SD 64'b0;
	    IR[j] <= `SD `NOOP_INST;
	    alu_func[j] <= `SD 6'b0;
	    opa[j] <= `SD 6'b0;
	    opb[j] <= `SD 6'b0;
	    dest[j] <= `SD 6'b0;
	    opb_valid[j] <= `SD 1'b0;
	    opa_sel[j] <= `SD 2'b0;
	    opb_sel[j] <= `SD 2'b0;
	    ROB[j] <= `SD 1'b0;
	    BRAT[j] <= `SD 1'b0;
	    ld_or_st[j] <= `SD 1'b0;
	    sq_num[j] <= `SD `SQ_SIZE_LOG'b0;
	    in_use[j] <= `SD 1'b0;	
	 end
	 else if (branch_mis && in_use[j] && branch_mis_brat[BRAT[j]]) begin
	    NPC[j] <= `SD 64'b0;
	    IR[j] <= `SD `NOOP_INST;
	    alu_func[j] <= `SD 6'b0;
	    opa[j] <= `SD 6'b0;
	    opb[j] <= `SD 6'b0;
	    dest[j] <= `SD 6'b0;
	    opb_valid[j] <= `SD 1'b0;
	    opa_sel[j] <= `SD 2'b0;
	    opb_sel[j] <= `SD 2'b0;
	    ROB[j] <= `SD 1'b0;
	    BRAT[j] <= `SD 1'b0;
	    ld_or_st[j] <= `SD 1'b0;
	    sq_num[j] <= `SD `SQ_SIZE_LOG'b0;
	    in_use[j] <= `SD 1'b0;					
	 end
	 else if (rs_load_in[0][j]) begin
	    // No Reset, Check whether there is something to dispatch. Check its valid bit after dispatched
	    // If dispatch & issue at same cycle, this code will overwrite the formal state.
            NPC[j] <= `SD RS_in_NPC[0];
	    IR[j] <= `SD RS_in_IR[0];
	    alu_func[j] <= `SD RS_in_alu_func[0];
	    opa[j] <= `SD  RS_in_regA[0];
	    opb[j] <= `SD  RS_in_regB[0];
	    dest[j] <= `SD RS_in_dest[0];
	    opb_valid[j] <= `SD valid_list[RS_in_regB[0][5:0]];
	    opa_sel[j] <= `SD RS_in_opa_sel[0];
 	    opb_sel[j] <= `SD RS_in_opb_sel[0];
	    ld_or_st[j] <= `SD rs_ld_or_st_in[0];
	    sq_num[j] <= `SD rs_sq_num_in[0];
	    ROB[j] <= `SD RS_in_ROB[0];
	    BRAT[j] <= `SD RS_in_BRAT[0];
	    in_use[j] <= `SD 1'b1;
	 end // if (rs_load_in[0][j])
	 else if (rs_load_in[1][j]) begin
	    // No Reset, Check whether there is something to dispatch. Check its valid bit after dispatched
	    // If dispatch & issue at same cycle, this code will overwrite the formal state.
            NPC[j] <= `SD RS_in_NPC[1];
	    IR[j] <= `SD RS_in_IR[1];
	    alu_func[j] <= `SD RS_in_alu_func[1];
	    opa[j] <= `SD  RS_in_regA[1];
	    opb[j] <= `SD  RS_in_regB[1];
	    dest[j] <= `SD RS_in_dest[1];
	    opb_valid[j] <= `SD valid_list[RS_in_regB[1][5:0]];
	    opa_sel[j] <= `SD RS_in_opa_sel[1];
 	    opb_sel[j] <= `SD RS_in_opb_sel[1];
	    ld_or_st[j] <= `SD rs_ld_or_st_in[1];
	    sq_num[j] <= `SD rs_sq_num_in[1];
	    ROB[j] <= `SD RS_in_ROB[1];
	    BRAT[j] <= `SD RS_in_BRAT[1];
	    in_use[j] <= `SD 1'b1;
	 end
	 else if (rs_free_in[0][j] | rs_free_in[1][j]) begin
	    // No Need to Dispatch in and This block is issued. Clean the block
	    in_use[j] <= `SD 1'b0;
	 end
	 else begin
	    // Old Instr still stay. Check Valid Bits.
	    if (in_use[j] & !opb_valid[j] & valid_list[opb[j][5:0]])
	      opb_valid[j] <= `SD 1'b1;
	    if (BRAT_CMT_v[0] && BRAT[j]==BRAT_CMT_num[0])
	      BRAT[j] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};
	    if (BRAT_CMT_v[1] && BRAT[j]==BRAT_CMT_num[1])
	      BRAT[j] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};		
	 end
      end // always @ (posedge clock)
   end // block: dispatch
   endgenerate

   // Individual outputs to FU
   
   
   
   generate 
	for (k=0; k < 2; k=k+1) begin : issue
	   for (j=0; j<`RS_MEM_SIZE; j=j+1) begin
	      assign RS_out_valid_tmp[k]	= rs_free_in_tmp[k][j];
	      assign RS_out_NPC_tmp[k] 		= rs_free_in_tmp[k][j] ? NPC[j] : 64'b0;        
	      assign RS_out_IR_tmp[k] 		= rs_free_in_tmp[k][j] ? IR[j] : 32'b0;        
	      assign RS_out_alu_func_tmp[k] 	= rs_free_in_tmp[k][j] ? alu_func[j] : `ALU_FUNC_NUM_LOG'b0; 
	      assign RS_out_regA_tmp[k]		= rs_free_in_tmp[k][j] ? opa[j] : `PRF_SIZE_LOG'b0;       
	      assign RS_out_regB_tmp[k] 	= rs_free_in_tmp[k][j] ? opb[j] : `PRF_SIZE_LOG'b0;      
	      assign RS_out_dest_tmp[k] 	= rs_free_in_tmp[k][j] ? dest[j] : `PRF_SIZE_LOG'b0;       
	      assign RS_out_opa_sel_tmp[k] 	= rs_free_in_tmp[k][j] ? opa_sel[j] : 2'b0;
	      assign RS_out_opb_sel_tmp[k] 	= rs_free_in_tmp[k][j] ? opb_sel[j] : 2'b0;
	      assign RS_out_ROB_tmp[k] 		= rs_free_in_tmp[k][j] ? ROB[j] : `ROB_SIZE_LOG'b0;
	      assign RS_out_ld_or_st_tmp[k]	= rs_free_in_tmp[k][j] ? ld_or_st[j] : 1'b0;
	      assign RS_out_sq_num_tmp[k] 	= rs_free_in_tmp[k][j] ? sq_num[j] : `SQ_SIZE_LOG'b0;
	      assign RS_out_BRAT_tmp[k]		= rs_free_in_tmp[k][j] ? BRAT[j] : `BRAT_SIZE_LOG'b0;
	      
	   end
	end
   endgenerate

   assign RS_out_valid 	= RS_out_valid_tmp;
   assign RS_out_NPC = RS_out_NPC_tmp;
   assign RS_out_IR = RS_out_IR_tmp;
   assign RS_out_alu_func = RS_out_alu_func_tmp;
   assign RS_out_regA = RS_out_regA_tmp;
   assign RS_out_regB = RS_out_regB_tmp;
   assign RS_out_opa_sel = RS_out_opa_sel_tmp;
   assign RS_out_opb_sel = RS_out_opb_sel_tmp;
   assign RS_out_dest = RS_out_dest_tmp;
   assign RS_out_ROB = RS_out_ROB_tmp;
   assign RS_out_ld_or_st = RS_out_ld_or_st_tmp;
   assign RS_out_sq_num = RS_out_sq_num_tmp;
   

   assign RS_out_BRAT[0] = (BRAT_CMT_v[0] && RS_out_BRAT_tmp[0]==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
			   (BRAT_CMT_v[1] && RS_out_BRAT_tmp[0]==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : RS_out_BRAT_tmp[0];

   assign RS_out_BRAT[1] = (BRAT_CMT_v[0] && RS_out_BRAT_tmp[1]==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
			   (BRAT_CMT_v[1] && RS_out_BRAT_tmp[1]==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : RS_out_BRAT_tmp[1];


endmodule // RS_MEM

/****************************************************************
 ****************************************************************
 ****************************************************************
 				RS_BR
 ****************************************************************
 ****************************************************************
 ****************************************************************/

module RS_BR(
	     //////////
	     // Inputs
	     //////////
	     
	     input 				        reset,            // Syetem Reset
	     input 				        clock,            // System Clock
	     
	     // CONTROL SIGNALS
	     
	     input     			        	branch_mis,       // Branch Misprediction from ROB. 00 if no, 01 if first mis, 10 if second mis, 11 if all miss
	     input [`BRAT_SIZE:0]			branch_mis_brat,  // BRAT number of branch mis
	     input [`PRF_SIZE-1:0]		        valid_list,       // Read Valid List to see who can be executed
	     input [1:0]			        RS_in_valid,      // IF there is one/two Instructions dispatched
	     input [1:0]                              	RS_in_not_erase,  // Signal from RS Arbiter. It tells each RS how many entries will they actually issue
	     
	     // DISPATCHED INSTRUCTION INFORMATION
	     
	     input [1:0] [63:0] 	       		RS_in_NPC,        // NPC for Dispatched INS
	     input [1:0] [31:0] 		       	RS_in_IR,         // IR for Dispatched INS  
	     input [1:0] [`ALU_FUNC_NUM_LOG-1:0]       	RS_in_alu_func,   // ALU FUNCTION for Dispatched INS   
	     input [1:0] [`PRF_SIZE_LOG - 1:0] 		RS_in_regA,       // reg#/intermediate for REGA of Dispatched INS.
	     input [1:0] [`PRF_SIZE_LOG - 1:0] 		RS_in_regB,       // reg#/intermediate for REGB of Dispatched INS.
	     input [1:0] [1:0] 		       		RS_in_opa_sel,    // Notification bit for RS_in_opa
	     input [1:0] [1:0] 				RS_in_opb_sel,    // Notification bit for RS_in_opb
	     input [1:0] [`PRF_SIZE_LOG-1:0] 	       	RS_in_dest,       // Destination PRF Number for Dispatched INS
	     input [1:0] [`ROB_SIZE_LOG-1:0] 	       	RS_in_ROB,        // ROB Number for Dispatched INS
	     input [1:0] [`BRAT_SIZE_LOG:0]    		RS_in_BRAT,	  // BRAT Number for Dispatched INS
	     input [1:0]	       			RS_in_uncn_br,    // whether it is a conditional branch
	     
	     // Early Branch Resolution
	     input [1:0]			       	BRAT_CMT_v,	
	     input [1:0][`BRAT_SIZE_LOG-1:0]	       	BRAT_CMT_num,
	     
	     //////////
	     //Outputs
	     //////////
	     
	     // Outputs for FU
	     
	     output  [1:0]		   	       	RS_out_valid,
	     output  [1:0] [63:0] 	   	       	RS_out_NPC,
	     output  [1:0] [31:0] 	   	       	RS_out_IR,
	     output  [1:0] [`ALU_FUNC_NUM_LOG-1:0]     	RS_out_alu_func,
	     output  [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regA,
	     output  [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regB,
	     output  [1:0] [1:0] 		       	RS_out_opa_sel,
	     output  [1:0] [1:0] 		       	RS_out_opb_sel,
	     output  [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_dest,
	     output  [1:0] [`ROB_SIZE_LOG-1:0] 	        RS_out_ROB,
	     output  [1:0] [`BRAT_SIZE_LOG:0]	       	RS_out_BRAT,	
	     output  [1:0]			       	RS_out_uncn_br,    // whether it is a conditional branch
	     
	     // Outputs for front End
	     
	     output [1:0] 			       	RS_full
	     
	     );
   
   // Head/Tail for determining next dispatch/issue priority
   
   reg [`RS_BR_SIZE_LOG-1:0] 				head, tail;
   wire [1:0] [`RS_BR_SIZE_LOG-1:0] 			next_head, next_tail;

   // All Values stored in RS
   
   reg [`RS_BR_SIZE-1:0] [63:0] 			NPC;
   reg [`RS_BR_SIZE-1:0] [31:0] 			IR;
   reg [`RS_BR_SIZE-1:0] [`ALU_FUNC_NUM_LOG-1:0] 	alu_func; 
   reg [`RS_BR_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		opa;        //contains RegA#/opa_sel
   reg [`RS_BR_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		opb;        //contains RegB#/opb_sel
   reg [`RS_BR_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		dest;        
   reg [`RS_BR_SIZE-1:0] 				opa_valid;         
   reg [`RS_BR_SIZE-1:0] 				opb_valid;         
   reg [`RS_BR_SIZE-1:0] [1:0]				opa_sel;   //indicate whether Reg#/sel
   reg [`RS_BR_SIZE-1:0] [1:0]				opb_sel;   //indicate whether Reg#/sel
   reg [`RS_BR_SIZE-1:0] [`ROB_SIZE_LOG-1:0] 		ROB;
   reg [`RS_BR_SIZE-1:0] [`BRAT_SIZE_LOG:0] 		BRAT;
   reg [`RS_BR_SIZE-1:0] 				uncn_br;  
   reg [`RS_BR_SIZE-1:0] 				in_use;            

   // Wires for Issue/Dispatch logics
   
   wire [1:0] [`RS_BR_SIZE-1:0] 			rs_load_in;
   wire [1:0] [`RS_BR_SIZE-1:0] 			rs_free_in;
   wire [1:0] [`RS_BR_SIZE-1:0] 			rs_free_in_tmp;
   wire [`RS_BR_SIZE-1:0] 				rs_avail_out;
   wire [`RS_BR_SIZE-1:0] 				rs_ready_out;
   wire [1:0]						num_of_avail;

   genvar j,k;
   
   generate 
	for (j=0; j<`RS_BR_SIZE; j=j+1) begin
		assign rs_avail_out[j] = ~in_use[j]; 
		assign rs_ready_out[j] = in_use[j] & opa_valid[j] & opb_valid[j] & (!branch_mis_brat | !branch_mis_brat[BRAT[j]]); 
	end
   endgenerate

   `NoO(`RS_BR_SIZE) N_of_avail_out_calc (.Nin(rs_avail_out/*|rs_free_in[0]|rs_free_in[1]*/), .Nout(num_of_avail));
   
   assign RS_full = (!num_of_avail) ? 2'b11 : (num_of_avail == 2'b10 | num_of_avail == 2'b01);
   
   //Arbiters for dispatch in & issue out  
   `RPS(`RS_BR_SIZE)  rps_dispatch (.req(rs_avail_out/*|rs_free_in[0]|rs_free_in[1]*/), .en(RS_in_valid), .gnt(rs_load_in), .count(tail));
   `RPS(`RS_BR_SIZE)  rps_issue (.req(rs_ready_out), .en(2'b11), .gnt(rs_free_in_tmp), .count(head));

   assign rs_free_in[0] = (RS_in_not_erase[0]) ? 0 : rs_free_in_tmp[0];
   assign rs_free_in[1] = (RS_in_not_erase[1]) ? 0 : rs_free_in_tmp[1];
   
   `DECODER(`RS_BR_SIZE) I_dec_0(.current_D_I(rs_free_in[0]), .next_D_I(next_head[0]));
   `DECODER(`RS_BR_SIZE) D_dec_0(.current_D_I(rs_load_in[0]), .next_D_I(next_tail[0]));
   `DECODER(`RS_BR_SIZE) I_dec_1(.current_D_I(rs_free_in[1]), .next_D_I(next_head[1]));
   `DECODER(`RS_BR_SIZE) D_dec_1(.current_D_I(rs_load_in[1]), .next_D_I(next_tail[1]));

   always @(posedge clock) begin
	if (reset) begin
		head <= `SD `RS_BR_SIZE_LOG'b0;
		tail <= `SD `RS_BR_SIZE_LOG'b0;
	end
	else begin
		if (rs_free_in[1])
			head <= `SD next_head[1];
		else if (rs_free_in[0])
			head <= `SD next_head[0];
		if (rs_load_in[1])
			tail <= `SD next_tail[1];
		else if (rs_load_in[0])
			tail <= `SD next_tail[0]; 
	end
   end // always @ (posedge clock

   generate for (j=0; j<`RS_BR_SIZE; j=j+1) begin : dispatch
      always @(posedge clock) begin
	 if (reset) begin
	    // Reset when reset == 1 or Branch Mispredict
	    NPC[j] <= `SD 64'b0;
	    IR[j] <= `SD `NOOP_INST;
	    alu_func[j] <= `SD 6'b0;
	    opa[j] <= `SD 6'b0;
	    opb[j] <= `SD 6'b0;
	    dest[j] <= `SD 6'b0;
	    opa_valid[j] <= `SD 1'b0;
	    opb_valid[j] <= `SD 1'b0;
	    opa_sel[j] <= `SD 2'b0;
	    opb_sel[j] <= `SD 2'b0;
	    ROB[j] <= `SD 1'b0;
	    BRAT[j] <= `SD 1'b0;
	    uncn_br[j] <= `SD 1'b0;
	    in_use[j] <= `SD 1'b0;	
	 end
	 else if (branch_mis && in_use[j] && branch_mis_brat[BRAT[j]]) begin
	    NPC[j] <= `SD 64'b0;
	    IR[j] <= `SD `NOOP_INST;
	    alu_func[j] <= `SD 6'b0;
	    opa[j] <= `SD 6'b0;
	    opb[j] <= `SD 6'b0;
	    dest[j] <= `SD 6'b0;
	    opa_valid[j] <= `SD 1'b0;
	    opb_valid[j] <= `SD 1'b0;
	    opa_sel[j] <= `SD 2'b0;
	    opb_sel[j] <= `SD 2'b0;
	    ROB[j] <= `SD 1'b0;
	    BRAT[j] <= `SD 1'b0;
	    uncn_br[j] <= `SD 1'b0;
	    in_use[j] <= `SD 1'b0;					
	 end
	 else if (rs_load_in[0][j]) begin
	    // No Reset, Check whether there is something to dispatch. Check its valid bit after dispatched
	    // If dispatch & issue at same cycle, this code will overwrite the formal state.
            NPC[j] <= `SD RS_in_NPC[0];
	    IR[j] <= `SD RS_in_IR[0];
	    alu_func[j] <= `SD RS_in_alu_func[0];
	    opa[j] <= `SD  RS_in_regA[0];
	    opb[j] <= `SD  RS_in_regB[0];
	    dest[j] <= `SD RS_in_dest[0];
	    opa_valid[j] <= `SD valid_list[RS_in_regA[0][5:0]];
	    opb_valid[j] <= `SD valid_list[RS_in_regB[0][5:0]];
	    opa_sel[j] <= `SD RS_in_opa_sel[0];
 	    opb_sel[j] <= `SD RS_in_opb_sel[0];
	    uncn_br[j] <= `SD RS_in_uncn_br[0];
	    ROB[j] <= `SD RS_in_ROB[0];
	    BRAT[j] <= `SD RS_in_BRAT[0];
	    in_use[j] <= `SD 1'b1;
	 end // if (rs_load_in[0][j])
	 else if (rs_load_in[1][j]) begin
	    // No Reset, Check whether there is something to dispatch. Check its valid bit after dispatched
	    // If dispatch & issue at same cycle, this code will overwrite the formal state.
            NPC[j] <= `SD RS_in_NPC[1];
	    IR[j] <= `SD RS_in_IR[1];
	    alu_func[j] <= `SD RS_in_alu_func[1];
	    opa[j] <= `SD  RS_in_regA[1];
	    opb[j] <= `SD  RS_in_regB[1];
	    dest[j] <= `SD RS_in_dest[1];
	    opa_valid[j] <= `SD valid_list[RS_in_regA[1][5:0]];
	    opb_valid[j] <= `SD valid_list[RS_in_regB[1][5:0]];
	    opa_sel[j] <= `SD RS_in_opa_sel[1];
 	    opb_sel[j] <= `SD RS_in_opb_sel[1];
	    uncn_br[j] <= `SD RS_in_uncn_br[1];
	    ROB[j] <= `SD RS_in_ROB[1];
	    BRAT[j] <= `SD RS_in_BRAT[1];
	    in_use[j] <= `SD 1'b1;
	 end
	 else if (rs_free_in[0][j] | rs_free_in[1][j]) begin
	    // No Need to Dispatch in and This block is issued. Clean the block
	    in_use[j] <= `SD 1'b0;
	 end
	 else begin
	    // Old Instr still stay. Check Valid Bits.
	    if (in_use[j] & !opa_valid[j] & valid_list[opa[j][5:0]])
	      opa_valid[j] <= `SD 1'b1;
	    if (in_use[j] & !opb_valid[j] & valid_list[opb[j][5:0]])
	      opb_valid[j] <= `SD 1'b1;	
	    if (BRAT_CMT_v[0] && BRAT[j]==BRAT_CMT_num[0])
	      BRAT[j] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};
	    if (BRAT_CMT_v[1] && BRAT[j]==BRAT_CMT_num[1])
	      BRAT[j] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};		
	 end
      end // always @ (posedge clock)
   end // block: dispatch
   endgenerate
   
   // Individual outputs to FU
   wor [1:0]				        RS_out_valid_tmp; 
   wor [1:0] [63:0] 				RS_out_NPC_tmp;
   wor [1:0] [31:0] 				RS_out_IR_tmp;
   wor [1:0] [`ALU_FUNC_NUM_LOG-1:0] 		RS_out_alu_func_tmp;
   wor [1:0] [`PRF_SIZE_LOG-1:0] 		RS_out_regA_tmp;
   wor [1:0] [`PRF_SIZE_LOG-1:0] 		RS_out_regB_tmp;
   wor [1:0] [1:0] 				RS_out_opa_sel_tmp;
   wor [1:0] [1:0] 				RS_out_opb_sel_tmp;
   wor [1:0] [`PRF_SIZE_LOG-1:0] 		RS_out_dest_tmp;
   wor [1:0] [`ROB_SIZE_LOG-1:0] 		RS_out_ROB_tmp;  
   wor [1:0] 					RS_out_uncn_br_tmp; 
   wor [1:0] [`BRAT_SIZE_LOG:0] 		RS_out_BRAT_tmp;  
   

   generate 
	for (k=0; k < 2; k=k+1) begin : issue
	   for (j=0; j<`RS_BR_SIZE; j=j+1) begin
	      assign RS_out_valid_tmp[k]	= rs_free_in_tmp[k][j];
	      assign RS_out_NPC_tmp[k] 		= rs_free_in_tmp[k][j] ? NPC[j] : 64'b0;        
	      assign RS_out_IR_tmp[k] 		= rs_free_in_tmp[k][j] ? IR[j] : 32'b0;        
	      assign RS_out_alu_func_tmp[k] 	= rs_free_in_tmp[k][j] ? alu_func[j] : `ALU_FUNC_NUM_LOG'b0; 
	      assign RS_out_regA_tmp[k]		= rs_free_in_tmp[k][j] ? opa[j] : `PRF_SIZE_LOG'b0;       
	      assign RS_out_regB_tmp[k] 	= rs_free_in_tmp[k][j] ? opb[j] : `PRF_SIZE_LOG'b0;      
	      assign RS_out_dest_tmp[k] 	= rs_free_in_tmp[k][j] ? dest[j] : `PRF_SIZE_LOG'b0;       
	      assign RS_out_opa_sel_tmp[k] 	= rs_free_in_tmp[k][j] ? opa_sel[j] : 2'b0;
	      assign RS_out_opb_sel_tmp[k] 	= rs_free_in_tmp[k][j] ? opb_sel[j] : 2'b0;
	      assign RS_out_ROB_tmp[k] 		= rs_free_in_tmp[k][j] ? ROB[j] : `ROB_SIZE_LOG'b0;
	      assign RS_out_uncn_br_tmp[k]	= rs_free_in_tmp[k][j] ? uncn_br[j] : 1'b0;
	      assign RS_out_BRAT_tmp[k]		= rs_free_in_tmp[k][j] ? BRAT[j] : `BRAT_SIZE_LOG'b0;
	   end
	end
   endgenerate

   assign RS_out_valid 	= RS_out_valid_tmp;
   assign RS_out_NPC = RS_out_NPC_tmp;
   assign RS_out_IR = RS_out_IR_tmp;
   assign RS_out_alu_func = RS_out_alu_func_tmp;
   assign RS_out_regA = RS_out_regA_tmp;
   assign RS_out_regB = RS_out_regB_tmp;
   assign RS_out_opa_sel = RS_out_opa_sel_tmp;
   assign RS_out_opb_sel = RS_out_opb_sel_tmp;
   assign RS_out_dest = RS_out_dest_tmp;
   assign RS_out_ROB = RS_out_ROB_tmp;
   assign RS_out_uncn_br = RS_out_uncn_br_tmp;

   assign RS_out_BRAT[0] = (BRAT_CMT_v[0] && RS_out_BRAT_tmp[0]==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
			   (BRAT_CMT_v[1] && RS_out_BRAT_tmp[0]==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : RS_out_BRAT_tmp[0];

   assign RS_out_BRAT[1] = (BRAT_CMT_v[0] && RS_out_BRAT_tmp[1]==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
			   (BRAT_CMT_v[1] && RS_out_BRAT_tmp[1]==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : RS_out_BRAT_tmp[1];


endmodule // RS_BR

/****************************************************************
 ****************************************************************
 ****************************************************************
 			      RS_MULT
 ****************************************************************
 ****************************************************************
 ****************************************************************/

module RS_MULT(
	       //////////
	       // Inputs
	       //////////
	       
	       input 				        reset,            // Syetem Reset
	       input 				        clock,            // System Clock
	       
	       // CONTROL SIGNALS
	       
	       input     			        branch_mis,       // Branch Misprediction from ROB. 00 if no, 01 if first mis, 10 if second mis, 11 if all miss
	       input [`BRAT_SIZE:0]			branch_mis_brat,  // BRAT number of branch mis
	       input [`PRF_SIZE-1:0]		        valid_list,       // Read Valid List to see who can be executed
	       input [1:0]			        RS_in_valid,      // IF there is one/two Instructions dispatched
	       input [1:0]                               RS_in_not_erase,  // Signal from RS Arbiter. It tells each RS how many entries will they actually issue
	       
	       // DISPATCHED INSTRUCTION INFORMATION
	       
	       input [1:0] [63:0] 				RS_in_NPC,        // NPC for Dispatched INS
	       input [1:0] [31:0] 				RS_in_IR,         // IR for Dispatched INS  
	       input [1:0] [`ALU_FUNC_NUM_LOG-1:0] 		RS_in_alu_func,   // ALU FUNCTION for Dispatched INS   
	       input [1:0] [`PRF_SIZE_LOG - 1:0] 		RS_in_regA,       // reg#/intermediate for REGA of Dispatched INS.
	       input [1:0] [`PRF_SIZE_LOG - 1:0] 		RS_in_regB,       // reg#/intermediate for REGB of Dispatched INS.
	       input [1:0] [1:0] 		       		RS_in_opa_sel,    // Notification bit for RS_in_opa
	       input [1:0] [1:0] 				RS_in_opb_sel,    // Notification bit for RS_in_opb
	       input [1:0] [`PRF_SIZE_LOG-1:0] 	        	RS_in_dest,       // Destination PRF Number for Dispatched INS
	       input [1:0] [`ROB_SIZE_LOG-1:0] 	        	RS_in_ROB,        // ROB Number for Dispatched INS
	       input [1:0] [`BRAT_SIZE_LOG:0]			RS_in_BRAT,	  // BRAT Number for Dispatched INS
	       
	       // Early Branch Resolution
	       input [1:0]					BRAT_CMT_v,	
	       input [1:0][`BRAT_SIZE_LOG-1:0]			BRAT_CMT_num,
	       
	       //////////
	       //Outputs
	       //////////
	       
	       // Outputs for FU
	       
	       output  [1:0]		   			RS_out_valid,
	       output  [1:0] [63:0] 	   			RS_out_NPC,
	       output  [1:0] [31:0] 	   			RS_out_IR,
	       output  [1:0] [`ALU_FUNC_NUM_LOG-1:0]      	RS_out_alu_func,
	       output  [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regA,
	       output  [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regB,
	       output  [1:0] [1:0] 				RS_out_opa_sel,
	       output  [1:0] [1:0] 				RS_out_opb_sel,
	       output  [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_dest,
	       output  [1:0] [`ROB_SIZE_LOG-1:0] 	        RS_out_ROB,
	       output  [1:0] [`BRAT_SIZE_LOG:0]			RS_out_BRAT,	
	       
	       // Outputs for front End
	       
	       output [1:0] 			        	RS_full
	       
	       );
   
   // Head/Tail for determining next dispatch/issue priority
   
   reg [`RS_MULT_SIZE_LOG-1:0] 				head, tail;
   wire [1:0] [`RS_MULT_SIZE_LOG-1:0] 			next_head, next_tail;

   // All Values stored in RS
   
   reg [`RS_MULT_SIZE-1:0] [63:0] 			NPC;
   reg [`RS_MULT_SIZE-1:0] [31:0] 			IR;
   reg [`RS_MULT_SIZE-1:0] [`ALU_FUNC_NUM_LOG-1:0] 	alu_func; 
   reg [`RS_MULT_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		opa;        //contains RegA#/opa_sel
   reg [`RS_MULT_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		opb;        //contains RegB#/opb_sel
   reg [`RS_MULT_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		dest;        
   reg [`RS_MULT_SIZE-1:0] 				opa_valid;         
   reg [`RS_MULT_SIZE-1:0] 				opb_valid;         
   reg [`RS_MULT_SIZE-1:0] [1:0]				opa_sel;   //indicate whether Reg#/sel
   reg [`RS_MULT_SIZE-1:0] [1:0]				opb_sel;   //indicate whether Reg#/sel
   reg [`RS_MULT_SIZE-1:0] [`ROB_SIZE_LOG-1:0] 		ROB;
   reg [`RS_MULT_SIZE-1:0] [`BRAT_SIZE_LOG:0] 		BRAT;
   reg [`RS_MULT_SIZE-1:0] 				in_use;            

   // Wires for Issue/Dispatch logics
   
   wire [1:0] [`RS_MULT_SIZE-1:0] 			rs_load_in;
   wire [1:0] [`RS_MULT_SIZE-1:0] 			rs_free_in;
   wire [1:0] [`RS_MULT_SIZE-1:0] 			rs_free_in_tmp;
   wire [`RS_MULT_SIZE-1:0] 				rs_avail_out;
   wire [`RS_MULT_SIZE-1:0] 				rs_ready_out;
   wire [1:0]						num_of_avail;

   genvar j,k;
   
   generate 
	for (j=0; j<`RS_MULT_SIZE; j=j+1) begin
		assign rs_avail_out[j] = ~in_use[j];
		assign rs_ready_out[j] = in_use[j] & opa_valid[j] & opb_valid[j] & (!branch_mis_brat | !branch_mis_brat[BRAT[j]]); 
	end
   endgenerate


   `NoO(`RS_MULT_SIZE) N_of_avail_out_calc (.Nin(rs_avail_out/*|rs_free_in[0]|rs_free_in[1]*/), .Nout(num_of_avail));
   
   assign RS_full = (!num_of_avail) ? 2'b11 : (num_of_avail == 2'b10 | num_of_avail == 2'b01);
   
   //Arbiters for dispatch in & issue out  
   `RPS(`RS_MULT_SIZE)  rps_dispatch (.req(rs_avail_out/*|rs_free_in[0]|rs_free_in[1]*/), .en(RS_in_valid), .gnt(rs_load_in), .count(tail));
   `RPS(`RS_MULT_SIZE)  rps_issue (.req(rs_ready_out), .en(2'b11), .gnt(rs_free_in_tmp), .count(head));

   assign rs_free_in[0] = (RS_in_not_erase[0]) ? 0 : rs_free_in_tmp[0];
   assign rs_free_in[1] = (RS_in_not_erase[1]) ? 0 : rs_free_in_tmp[1];
   
   `DECODER(`RS_MULT_SIZE) I_dec_0(.current_D_I(rs_free_in[0]), .next_D_I(next_head[0]));
   `DECODER(`RS_MULT_SIZE) D_dec_0(.current_D_I(rs_load_in[0]), .next_D_I(next_tail[0]));
   `DECODER(`RS_MULT_SIZE) I_dec_1(.current_D_I(rs_free_in[1]), .next_D_I(next_head[1]));
   `DECODER(`RS_MULT_SIZE) D_dec_1(.current_D_I(rs_load_in[1]), .next_D_I(next_tail[1]));

   always @(posedge clock) begin
	if (reset) begin
		head <= `SD 3'b000;
		tail <= `SD 3'b000;
	end
	else begin
		if (rs_free_in[1])
			head <= `SD next_head[1];
		else if (rs_free_in[0])
			head <= `SD next_head[0];
		if (rs_load_in[1])
			tail <= `SD next_tail[1];
		else if (rs_load_in[0])
			tail <= `SD next_tail[0]; 
	end
   end // always @ (posedge clock


   generate
	for (j=0; j<`RS_MULT_SIZE; j=j+1) begin : dispatch
		always @(posedge clock) begin
			if (reset) begin
			   // Reset when reset == 1 or Branch Mispredict
				NPC[j] <= `SD 64'b0;
				IR[j] <= `SD `NOOP_INST;
				alu_func[j] <= `SD 6'b0;
				opa[j] <= `SD 6'b0;
				opb[j] <= `SD 6'b0;
				dest[j] <= `SD 6'b0;
				opa_valid[j] <= `SD 1'b0;
				opb_valid[j] <= `SD 1'b0;
				opa_sel[j] <= `SD 2'b0;
				opb_sel[j] <= `SD 2'b0;
				ROB[j] <= `SD 1'b0;
				BRAT[j] <= `SD 1'b0;
				in_use[j] <= `SD 1'b0;	
			end
		        else if (branch_mis && in_use[j] && branch_mis_brat[BRAT[j]]) begin
				NPC[j] <= `SD 64'b0;
				IR[j] <= `SD `NOOP_INST;
				alu_func[j] <= `SD 6'b0;
				opa[j] <= `SD 6'b0;
				opb[j] <= `SD 6'b0;
				dest[j] <= `SD 6'b0;
				opa_valid[j] <= `SD 1'b0;
				opb_valid[j] <= `SD 1'b0;
				opa_sel[j] <= `SD 2'b0;
				opb_sel[j] <= `SD 2'b0;
				ROB[j] <= `SD 1'b0;
				BRAT[j] <= `SD 1'b0;
				in_use[j] <= `SD 1'b0;					
			end
			else if (rs_load_in[0][j]) begin
			   // No Reset, Check whether there is something to dispatch. Check its valid bit after dispatched
			   // If dispatch & issue at same cycle, this code will overwrite the formal state.
                                NPC[j] <= `SD RS_in_NPC[0];
				IR[j] <= `SD RS_in_IR[0];
				alu_func[j] <= `SD RS_in_alu_func[0];
				opa[j] <= `SD  RS_in_regA[0];
				opb[j] <= `SD  RS_in_regB[0];
				dest[j] <= `SD RS_in_dest[0];
				opa_valid[j] <= `SD valid_list[RS_in_regA[0][5:0]];
				opb_valid[j] <= `SD valid_list[RS_in_regB[0][5:0]];
				opa_sel[j] <= `SD RS_in_opa_sel[0];
 				opb_sel[j] <= `SD RS_in_opb_sel[0];
				ROB[j] <= `SD RS_in_ROB[0];
				BRAT[j] <= `SD RS_in_BRAT[0];
				in_use[j] <= `SD 1'b1;
			end // if (rs_load_in[0][j])
		   	else if (rs_load_in[1][j]) begin
			   // No Reset, Check whether there is something to dispatch. Check its valid bit after dispatched
			   // If dispatch & issue at same cycle, this code will overwrite the formal state.
                                NPC[j] <= `SD RS_in_NPC[1];
				IR[j] <= `SD RS_in_IR[1];
				alu_func[j] <= `SD RS_in_alu_func[1];
				opa[j] <= `SD  RS_in_regA[1];
				opb[j] <= `SD  RS_in_regB[1];
				dest[j] <= `SD RS_in_dest[1];
				opa_valid[j] <= `SD valid_list[RS_in_regA[1][5:0]];
				opb_valid[j] <= `SD valid_list[RS_in_regB[1][5:0]];
				opa_sel[j] <= `SD RS_in_opa_sel[1] ;
 				opb_sel[j] <= `SD RS_in_opb_sel[1] ;
				ROB[j] <= `SD RS_in_ROB[1];
				BRAT[j] <= `SD RS_in_BRAT[1];
				in_use[j] <= `SD 1'b1;
			end
			else if (rs_free_in[0][j] | rs_free_in[1][j]) begin
			   // No Need to Dispatch in and This block is issued. Clean the block
				in_use[j] <= `SD 1'b0;
			end
			else begin
			   // Old Instr still stay. Check Valid Bits.
				if (in_use[j] & !opa_valid[j] & valid_list[opa[j][5:0]])
					opa_valid[j] <= `SD 1'b1;
				if (in_use[j] & !opb_valid[j] & valid_list[opb[j][5:0]])
					opb_valid[j] <= `SD 1'b1;	
				if (BRAT_CMT_v[0] && BRAT[j]==BRAT_CMT_num[0])
					BRAT[j] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};
				if (BRAT_CMT_v[1] && BRAT[j]==BRAT_CMT_num[1])
					BRAT[j] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};		
			end
		end // always @ (posedge clock)
	end // block: dispatch
   endgenerate

// Individual outputs to FU
      wor [1:0]				        RS_out_valid_tmp; 
      wor [1:0] [63:0] 	   			RS_out_NPC_tmp;
      wor [1:0] [31:0] 	   			RS_out_IR_tmp;
      wor [1:0] [`ALU_FUNC_NUM_LOG-1:0]      	RS_out_alu_func_tmp;
      wor [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regA_tmp;
      wor [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regB_tmp;
      wor [1:0] [1:0] 				RS_out_opa_sel_tmp;
      wor [1:0] [1:0] 				RS_out_opb_sel_tmp;
      wor [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_dest_tmp;
      wor [1:0] [`ROB_SIZE_LOG-1:0] 	        RS_out_ROB_tmp;  
      wor [1:0] [`BRAT_SIZE_LOG:0] 	        RS_out_BRAT_tmp;  



   generate 
	for (k=0; k < 2; k=k+1) begin : issue
	   for (j=0; j<`RS_MULT_SIZE; j=j+1) begin
	      assign RS_out_valid_tmp[k]	= rs_free_in_tmp[k][j];
	      assign RS_out_NPC_tmp[k] 		= rs_free_in_tmp[k][j] ? NPC[j] : 64'b0;        
	      assign RS_out_IR_tmp[k] 		= rs_free_in_tmp[k][j] ? IR[j] : 32'b0;        
	      assign RS_out_alu_func_tmp[k] 	= rs_free_in_tmp[k][j] ? alu_func[j] : `ALU_FUNC_NUM_LOG'b0; 
	      assign RS_out_regA_tmp[k]		= rs_free_in_tmp[k][j] ? opa[j] : `PRF_SIZE_LOG'b0;       
	      assign RS_out_regB_tmp[k] 	= rs_free_in_tmp[k][j] ? opb[j] : `PRF_SIZE_LOG'b0;      
	      assign RS_out_dest_tmp[k] 	= rs_free_in_tmp[k][j] ? dest[j] : `PRF_SIZE_LOG'b0;       
	      assign RS_out_opa_sel_tmp[k] 	= rs_free_in_tmp[k][j] ? opa_sel[j] : 2'b0;
	      assign RS_out_opb_sel_tmp[k] 	= rs_free_in_tmp[k][j] ? opb_sel[j] : 2'b0;
	      assign RS_out_ROB_tmp[k] 		= rs_free_in_tmp[k][j] ? ROB[j] : `ROB_SIZE_LOG'b0;
	      assign RS_out_BRAT_tmp[k]		= rs_free_in_tmp[k][j] ? BRAT[j] : `BRAT_SIZE_LOG'b0;
	   end
	end
   endgenerate

   assign RS_out_valid 	= RS_out_valid_tmp;
   assign RS_out_NPC = RS_out_NPC_tmp;
   assign RS_out_IR = RS_out_IR_tmp;
   assign RS_out_alu_func = RS_out_alu_func_tmp;
   assign RS_out_regA = RS_out_regA_tmp;
   assign RS_out_regB = RS_out_regB_tmp;
   assign RS_out_opa_sel = RS_out_opa_sel_tmp;
   assign RS_out_opb_sel = RS_out_opb_sel_tmp;
   assign RS_out_dest = RS_out_dest_tmp;
   assign RS_out_ROB = RS_out_ROB_tmp;

   assign RS_out_BRAT[0] = (BRAT_CMT_v[0] && RS_out_BRAT_tmp[0]==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
					(BRAT_CMT_v[1] && RS_out_BRAT_tmp[0]==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : RS_out_BRAT_tmp[0];

   assign RS_out_BRAT[1] = (BRAT_CMT_v[0] && RS_out_BRAT_tmp[1]==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
					(BRAT_CMT_v[1] && RS_out_BRAT_tmp[1]==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : RS_out_BRAT_tmp[1];


endmodule // RS_MULT


/****************************************************************
 ****************************************************************
 ****************************************************************
 			      RS_ALU
 ****************************************************************
 ****************************************************************
 ****************************************************************/

module RS_ALU(
	      //////////
	      // Inputs
	      //////////
	      
	      input 				        reset,            // Syetem Reset
	      input 				        clock,            // System Clock
	      
	      // CONTROL SIGNALS
	      
	      input     			        branch_mis,       // Branch Misprediction from ROB. 00 if no, 01 if first mis, 10 if second mis, 11 if all miss
	      input [`BRAT_SIZE:0]			branch_mis_brat,  // BRAT number of branch mis
	      input [`PRF_SIZE-1:0]		        valid_list,       // Read Valid List to see who can be executed
	      input [1:0]			        RS_in_valid,      // IF there is one/two Instructions dispatched
	      input [1:0]                               RS_in_not_erase,  // Signal from RS Arbiter. It tells each RS how many entries will they actually issue
	      
	      // DISPATCHED INSTRUCTION INFORMATION
	      
	      input [1:0] [63:0] 				RS_in_NPC,        // NPC for Dispatched INS
	      input [1:0] [31:0] 				RS_in_IR,         // IR for Dispatched INS  
	      input [1:0] [`ALU_FUNC_NUM_LOG-1:0] 		RS_in_alu_func,   // ALU FUNCTION for Dispatched INS   
	      input [1:0] [`PRF_SIZE_LOG - 1:0] 		RS_in_regA,       // reg#/intermediate for REGA of Dispatched INS.
	      input [1:0] [`PRF_SIZE_LOG - 1:0] 		RS_in_regB,       // reg#/intermediate for REGB of Dispatched INS.
	      input [1:0] [1:0] 		       		RS_in_opa_sel,    // Notification bit for RS_in_opa
	      input [1:0] [1:0] 				RS_in_opb_sel,    // Notification bit for RS_in_opb
	      input [1:0] [`PRF_SIZE_LOG-1:0] 	        	RS_in_dest,       // Destination PRF Number for Dispatched INS
	      input [1:0] [`ROB_SIZE_LOG-1:0] 	        	RS_in_ROB,        // ROB Number for Dispatched INS
	      input [1:0] [`BRAT_SIZE_LOG:0]			RS_in_BRAT,	  // BRAT Number for Dispatched INS
	      
	      // Early Branch Resolution
	      input [1:0]					BRAT_CMT_v,	
	      input [1:0][`BRAT_SIZE_LOG-1:0]			BRAT_CMT_num,
	      
	      //////////
	      //Outputs
	      //////////
	      
	      // Outputs for FU
	      
	      output  [1:0]		   			RS_out_valid,
	      output  [1:0] [63:0] 	   			RS_out_NPC,
	      output  [1:0] [31:0] 	   			RS_out_IR,
	      output  [1:0] [`ALU_FUNC_NUM_LOG-1:0]      	RS_out_alu_func,
	      output  [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regA,
	      output  [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regB,
	      output  [1:0] [1:0] 				RS_out_opa_sel,
	      output  [1:0] [1:0] 				RS_out_opb_sel,
	      output  [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_dest,
	      output  [1:0] [`ROB_SIZE_LOG-1:0] 	        RS_out_ROB,
	      output  [1:0] [`BRAT_SIZE_LOG:0]			RS_out_BRAT,	
	      
	      // Outputs for front End
	      
	      output [1:0] 			        	RS_full
	      
	      );
   
   // Head/Tail for determining next dispatch/issue priority
   
   reg [`RS_ALU_SIZE_LOG-1:0] 				head, tail;
   wire [1:0] [`RS_ALU_SIZE_LOG-1:0] 			next_head, next_tail;

   // All Values stored in RS
   
   reg [`RS_ALU_SIZE-1:0] [63:0] 			NPC;
   reg [`RS_ALU_SIZE-1:0] [31:0] 			IR;
   reg [`RS_ALU_SIZE-1:0] [`ALU_FUNC_NUM_LOG-1:0] 	alu_func; 
   reg [`RS_ALU_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		opa;        //contains RegA#/opa_sel
   reg [`RS_ALU_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		opb;        //contains RegB#/opb_sel
   reg [`RS_ALU_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		dest;        
   reg [`RS_ALU_SIZE-1:0] 				opa_valid;         
   reg [`RS_ALU_SIZE-1:0] 				opb_valid;         
   reg [`RS_ALU_SIZE-1:0] [1:0]				opa_sel;   //indicate whether Reg#/sel
   reg [`RS_ALU_SIZE-1:0] [1:0]				opb_sel;   //indicate whether Reg#/sel
   reg [`RS_ALU_SIZE-1:0] [`ROB_SIZE_LOG-1:0] 		ROB;
   reg [`RS_ALU_SIZE-1:0] [`BRAT_SIZE_LOG:0] 		BRAT;
   reg [`RS_ALU_SIZE-1:0] 				in_use;            

   // Wires for Issue/Dispatch logics
   
   wire [1:0] [`RS_ALU_SIZE-1:0] 			rs_load_in;
   wire [1:0] [`RS_ALU_SIZE-1:0] 			rs_free_in;
   wire [1:0] [`RS_ALU_SIZE-1:0] 			rs_free_in_tmp;
   wire [`RS_ALU_SIZE-1:0] 				rs_avail_out;
   wire [`RS_ALU_SIZE-1:0] 				rs_ready_out;
   wire [1:0]						num_of_avail;

   genvar j,k;
   
   generate 
	for (j=0; j<`RS_ALU_SIZE; j=j+1) begin
		assign rs_avail_out[j] = ~in_use[j];
		assign rs_ready_out[j] = in_use[j] & opa_valid[j] & opb_valid[j] & (!branch_mis_brat | !branch_mis_brat[BRAT[j]]); 
	end
   endgenerate

   `NoO(`RS_ALU_SIZE) N_of_avail_out_calc (.Nin(rs_avail_out/*|rs_free_in[0]|rs_free_in[1]*/), .Nout(num_of_avail));
   
   assign RS_full = (!num_of_avail) ? 2'b11 : (num_of_avail == 2'b10 | num_of_avail == 2'b01);
   
   //Arbiters for dispatch in & issue out  
   `RPS(`RS_ALU_SIZE)  rps_dispatch (.req(rs_avail_out/*|rs_free_in[0]|rs_free_in[1]*/), .en(RS_in_valid), .gnt(rs_load_in), .count(tail));
   `RPS(`RS_ALU_SIZE)  rps_issue (.req(rs_ready_out), .en(2'b11), .gnt(rs_free_in_tmp), .count(head));

   assign rs_free_in[0] = (RS_in_not_erase[0]) ? 0 : rs_free_in_tmp[0];
   assign rs_free_in[1] = (RS_in_not_erase[1]) ? 0 : rs_free_in_tmp[1];
   
   `DECODER(`RS_ALU_SIZE) I_dec_0(.current_D_I(rs_free_in[0]), .next_D_I(next_head[0]));
   `DECODER(`RS_ALU_SIZE) D_dec_0(.current_D_I(rs_load_in[0]), .next_D_I(next_tail[0]));
   `DECODER(`RS_ALU_SIZE) I_dec_1(.current_D_I(rs_free_in[1]), .next_D_I(next_head[1]));
   `DECODER(`RS_ALU_SIZE) D_dec_1(.current_D_I(rs_load_in[1]), .next_D_I(next_tail[1]));

   always @(posedge clock) begin
	if (reset) begin
		head <= `SD `RS_ALU_SIZE_LOG'b0;
		tail <= `SD `RS_ALU_SIZE_LOG'b0;
	end
	else begin
		if (rs_free_in[1])
			head <= `SD next_head[1];
		else if (rs_free_in[0])
			head <= `SD next_head[0];
		if (rs_load_in[1])
			tail <= `SD next_tail[1];
		else if (rs_load_in[0])
			tail <= `SD next_tail[0]; 
	end
   end // always @ (posedge clock


   generate
	for (j=0; j<`RS_ALU_SIZE; j=j+1) begin : dispatch
		always @(posedge clock) begin
			if (reset) begin
			   // Reset when reset == 1 or Branch Mispredict
				NPC[j] <= `SD 64'b0;
				IR[j] <= `SD `NOOP_INST;
				alu_func[j] <= `SD 6'b0;
				opa[j] <= `SD 6'b0;
				opb[j] <= `SD 6'b0;
				dest[j] <= `SD 6'b0;
				opa_valid[j] <= `SD 1'b0;
				opb_valid[j] <= `SD 1'b0;
				opa_sel[j] <= `SD 2'b0;
				opb_sel[j] <= `SD 2'b0;
				ROB[j] <= `SD 1'b0;
				BRAT[j] <= `SD 1'b0;
				in_use[j] <= `SD 1'b0;	
			end
		        else if (branch_mis && in_use[j] && branch_mis_brat[BRAT[j]]) begin
				NPC[j] <= `SD 64'b0;
				IR[j] <= `SD `NOOP_INST;
				alu_func[j] <= `SD 6'b0;
				opa[j] <= `SD 6'b0;
				opb[j] <= `SD 6'b0;
				dest[j] <= `SD 6'b0;
				opa_valid[j] <= `SD 1'b0;
				opb_valid[j] <= `SD 1'b0;
				opa_sel[j] <= `SD 2'b0;
				opb_sel[j] <= `SD 2'b0;
				ROB[j] <= `SD 1'b0;
				BRAT[j] <= `SD 1'b0;
				in_use[j] <= `SD 1'b0;					
			end
			else if (rs_load_in[0][j]) begin
			   // No Reset, Check whether there is something to dispatch. Check its valid bit after dispatched
			   // If dispatch & issue at same cycle, this code will overwrite the formal state.
                                NPC[j] <= `SD RS_in_NPC[0];
				IR[j] <= `SD RS_in_IR[0];
				alu_func[j] <= `SD RS_in_alu_func[0];
				opa[j] <= `SD  RS_in_regA[0];
				opb[j] <= `SD  RS_in_regB[0];
				dest[j] <= `SD RS_in_dest[0];
				opa_valid[j] <= `SD valid_list[RS_in_regA[0][5:0]];
				opb_valid[j] <= `SD valid_list[RS_in_regB[0][5:0]];
				opa_sel[j] <= `SD RS_in_opa_sel[0];
 				opb_sel[j] <= `SD RS_in_opb_sel[0];
				ROB[j] <= `SD RS_in_ROB[0];
				BRAT[j] <= `SD RS_in_BRAT[0];
				in_use[j] <= `SD 1'b1;
			end // if (rs_load_in[0][j])
		   	else if (rs_load_in[1][j]) begin
			   // No Reset, Check whether there is something to dispatch. Check its valid bit after dispatched
			   // If dispatch & issue at same cycle, this code will overwrite the formal state.
                                NPC[j] <= `SD RS_in_NPC[1];
				IR[j] <= `SD RS_in_IR[1];
				alu_func[j] <= `SD RS_in_alu_func[1];
				opa[j] <= `SD  RS_in_regA[1];
				opb[j] <= `SD  RS_in_regB[1];
				dest[j] <= `SD RS_in_dest[1];
				opa_valid[j] <= `SD valid_list[RS_in_regA[1][5:0]];
				opb_valid[j] <= `SD valid_list[RS_in_regB[1][5:0]];
				opa_sel[j] <= `SD RS_in_opa_sel[1] ;
 				opb_sel[j] <= `SD RS_in_opb_sel[1] ;
				ROB[j] <= `SD RS_in_ROB[1];
				BRAT[j] <= `SD RS_in_BRAT[1];
				in_use[j] <= `SD 1'b1;
			end
			else if (rs_free_in[0][j] | rs_free_in[1][j]) begin
			   // No Need to Dispatch in and This block is issued. Clean the block
				in_use[j] <= `SD 1'b0;
			end
			else begin
			   // Old Instr still stay. Check Valid Bits.
				if (in_use[j] & !opa_valid[j] & valid_list[opa[j][5:0]])
					opa_valid[j] <= `SD 1'b1;
				if (in_use[j] & !opb_valid[j] & valid_list[opb[j][5:0]])
					opb_valid[j] <= `SD 1'b1;		
				if (BRAT_CMT_v[0] && BRAT[j]==BRAT_CMT_num[0])
					BRAT[j] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};
				if (BRAT_CMT_v[1] && BRAT[j]==BRAT_CMT_num[1])
					BRAT[j] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};				

			end
		end // always @ (posedge clock)
	end // block: dispatch
   endgenerate

// Individual outputs to FU
      wor [1:0]				        RS_out_valid_tmp; 
      wor [1:0] [63:0] 	   			RS_out_NPC_tmp;
      wor [1:0] [31:0] 	   			RS_out_IR_tmp;
      wor [1:0] [`ALU_FUNC_NUM_LOG-1:0]      	RS_out_alu_func_tmp;
      wor [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regA_tmp;
      wor [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_regB_tmp;
      wor [1:0] [1:0] 				RS_out_opa_sel_tmp;
      wor [1:0] [1:0] 				RS_out_opb_sel_tmp;
      wor [1:0] [`PRF_SIZE_LOG-1:0] 	        RS_out_dest_tmp;
      wor [1:0] [`ROB_SIZE_LOG-1:0] 	        RS_out_ROB_tmp;  
      wor [1:0] [`BRAT_SIZE_LOG:0] 	        RS_out_BRAT_tmp;  



   generate 
	for (k=0; k < 2; k=k+1) begin : issue
	   for (j=0; j<`RS_ALU_SIZE; j=j+1) begin
	      assign RS_out_valid_tmp[k]	= rs_free_in_tmp[k][j];
	      assign RS_out_NPC_tmp[k] 		= rs_free_in_tmp[k][j] ? NPC[j] : 64'b0;        
	      assign RS_out_IR_tmp[k] 		= rs_free_in_tmp[k][j] ? IR[j] : 32'b0;        
	      assign RS_out_alu_func_tmp[k] 	= rs_free_in_tmp[k][j] ? alu_func[j] : `ALU_FUNC_NUM_LOG'b0; 
	      assign RS_out_regA_tmp[k]		= rs_free_in_tmp[k][j] ? opa[j] : `PRF_SIZE_LOG'b0;       
	      assign RS_out_regB_tmp[k] 	= rs_free_in_tmp[k][j] ? opb[j] : `PRF_SIZE_LOG'b0;      
	      assign RS_out_dest_tmp[k] 	= rs_free_in_tmp[k][j] ? dest[j] : `PRF_SIZE_LOG'b0;       
	      assign RS_out_opa_sel_tmp[k] 	= rs_free_in_tmp[k][j] ? opa_sel[j] : 2'b0;
	      assign RS_out_opb_sel_tmp[k] 	= rs_free_in_tmp[k][j] ? opb_sel[j] : 2'b0;
	      assign RS_out_ROB_tmp[k] 		= rs_free_in_tmp[k][j] ? ROB[j] : `ROB_SIZE_LOG'b0;
	      assign RS_out_BRAT_tmp[k]		= rs_free_in_tmp[k][j] ? BRAT[j] : `BRAT_SIZE_LOG'b0;
	   end
	end
   endgenerate

   assign RS_out_valid 	= RS_out_valid_tmp;
   assign RS_out_NPC = RS_out_NPC_tmp;
   assign RS_out_IR = RS_out_IR_tmp;
   assign RS_out_alu_func = RS_out_alu_func_tmp;
   assign RS_out_regA = RS_out_regA_tmp;
   assign RS_out_regB = RS_out_regB_tmp;
   assign RS_out_opa_sel = RS_out_opa_sel_tmp;
   assign RS_out_opb_sel = RS_out_opb_sel_tmp;
   assign RS_out_dest = RS_out_dest_tmp;
   assign RS_out_ROB = RS_out_ROB_tmp;

   assign RS_out_BRAT[0] = (BRAT_CMT_v[0] && RS_out_BRAT_tmp[0]==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
					(BRAT_CMT_v[1] && RS_out_BRAT_tmp[0]==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : RS_out_BRAT_tmp[0];

   assign RS_out_BRAT[1] = (BRAT_CMT_v[0] && RS_out_BRAT_tmp[1]==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
					(BRAT_CMT_v[1] && RS_out_BRAT_tmp[1]==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : RS_out_BRAT_tmp[1];

endmodule // RS_8










