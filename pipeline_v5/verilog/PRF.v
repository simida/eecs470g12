/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  PRF.v                                               //
//                                                                     //
//  Description :  This module creates the Physical Register used by   // 
//                 the Pipeline.                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

module PRF(
	   input  [2:0] [1:0] [`PRF_SIZE_LOG-1:0] rda_idx,
	   input  [3:0] [1:0] [`PRF_SIZE_LOG-1:0] rdb_idx,	// read index
	   input  [`PRF_SIZE_LOG-1:0] LQ_regA,
	   input  [`PRF_SIZE_LOG-1:0] SQ_regA,
	   input  [1:0] [`PRF_SIZE_LOG-1:0] wr_idx,			// write index
	   input  [1:0] [63:0] wr_data,					// write data
	   input  [1:0]  wr_en,						// write enable
	   input  clock,	
	   output logic [2:0][1:0][63:0] rda_out,// clock
	   output logic [3:0][1:0][63:0] rdb_out,		// read data
	   output logic [63:0] LQ_value,
	   output logic [63:0] SQ_value,
	   output write_conflict				  		// for test use
	   );
   
  logic  [`PRF_SIZE-1:0] [63:0] registers;   // 32, 64-bit Registers
  logic	 [`PRF_SIZE-1:0] [63:0] next_registers;

  assign write_conflict = wr_en[0] & wr_en[1] & (wr_idx[0]==wr_idx[1]);

  genvar i,j;
   generate for (i=0; i<3; i=i+1) begin
      for (j=0; j<2; j=j+1) begin
	 always_comb begin
	    
	    //
	    // Read port A
	    //
	    if (rda_idx[i][j] == `PRF_ZERO_REG)
	      rda_out[i][j] = 64'b0;
	    else if (wr_en[0] && (wr_idx[0] == rda_idx[i][j]))
	      rda_out[i][j] = wr_data[0];  // internal forwarding 0
	    else if (wr_en[1] && (wr_idx[1] == rda_idx[i][j]))
	      rda_out[i][j] = wr_data[1];  // internal forwarding 1
	    else
	      rda_out[i][j] = registers[rda_idx[i][j]];
	 end // always_comb begin
      end // for (j=0; j<2; j=j+1)
   end // for (i=0; i<3; i=i+1)
      
   endgenerate
   
   generate for (i=0; i<4; i=i+1) begin
      for (j=0; j<2; j=j+1) begin
	 always_comb begin
	    //
	    // Read port B
	    //
	    if (rdb_idx[i][j] == `PRF_ZERO_REG)
	      rdb_out[i][j] = 64'b0;
	    else if (wr_en[0] && (wr_idx[0] == rdb_idx[i][j]))
	      rdb_out[i][j] = wr_data[0];  // internal forwarding 0
	    else if (wr_en[1] && (wr_idx[1] == rdb_idx[i][j]))
	      rdb_out[i][j] = wr_data[1];  // internal forwarding 1
	    else
	      rdb_out[i][j] = registers[rdb_idx[i][j]];
	 end // always_comb begin
      end // for (j=0; j<2; j=j+1)
   end // for (i=0; i<3; i=i+1)
      
   endgenerate

   always_comb begin
      if (LQ_regA == `PRF_ZERO_REG)
	LQ_value = 64'b0;
      else if (wr_en[0] && (wr_idx[0] == LQ_regA))
	LQ_value = wr_data[0];  // internal forwarding 0
      else if (wr_en[1] && (wr_idx[1] == LQ_regA))
	LQ_value = wr_data[1];  // internal forwarding 1
      else
	LQ_value = registers[LQ_regA];
      
      if (SQ_regA == `PRF_ZERO_REG)
	SQ_value = 64'b0;
      else if (wr_en[0] && (wr_idx[0] == SQ_regA))
	SQ_value = wr_data[0];  // internal forwarding 0
      else if (wr_en[1] && (wr_idx[1] == SQ_regA))
	SQ_value = wr_data[1];  // internal forwarding 1
      else
	SQ_value = registers[SQ_regA];
   end // always_comb begin
   
   
  //
  // Write port
  //
  always_comb begin
	next_registers = registers;
	if (wr_en[0])
		next_registers[wr_idx[0]] = wr_data[0];
	if (wr_en[1])
		next_registers[wr_idx[1]] = wr_data[1];
  end

	 
  always_ff @(posedge clock) 
      registers <= `SD next_registers;




endmodule // PRF
