module test();

   logic clock;
   logic reset;
   
   // Control Signals
   logic branch_mis;
   logic [`BRAT_SIZE:0] branch_mis_brat;
   logic [`PRF_SIZE:0] 	valid_list;
   logic 		LSQ_not_erase_PRF;
   logic 		LSQ_not_erase_Dcache;
   logic [1:0] 		BRAT_CMT_v;
   logic [1:0] [`BRAT_SIZE_LOG-1:0] BRAT_CMT_num;
   
   // Logics from MEM ALU
   logic [1:0] 			    ALU_LSQ_valid;
   logic [1:0] [63:0] 		    ALU_LSQ_NPC;
   logic [1:0] [31:0] 		    ALU_LSQ_IR;
   logic [1:0] 			    ALU_LSQ_ld_or_st;
   logic [1:0] [63:0] 		    ALU_LSQ_MEM_addr;
   logic [1:0] [`PRF_SIZE_LOG-1:0]  ALU_LSQ_regA;
   logic [1:0] [`ROB_SIZE_LOG-1:0]  ALU_LSQ_ROB;
   logic [1:0] [`BRAT_SIZE_LOG:0]   ALU_LSQ_BRAT;
   
   // Logics from SQ
   logic [`SQ_SIZE-1:0] 	    sq_valid;
   logic [`SQ_SIZE-1:0] 	    sq_addr_valid;
   logic [`SQ_SIZE-1:0] [63:0] 	    sq_MEM_addr;
   logic [`SQ_SIZE-1:0] [`PRF_SIZE_LOG-1:0] sq_regA;
   logic [`SQ_SIZE-1:0] [`ROB_SIZE_LOG-1:0] sq_ROB;
   logic [`SQ_SIZE_LOG-1:0] 		    sq_head;
   logic [`SQ_SIZE_LOG-1:0] 		    sq_tail;

   // Logics from ROB
   logic [`ROB_SIZE_LOG-1:0] 		    ROB_head;
   logic [1:0] 				    ROB_head_is_store;

   // Logics for PRF_after
   logic 				    LQ_PRF_valid;
   logic [63:0] 			    LQ_PRF_NPC;
   logic [31:0] 			    LQ_PRF_IR;
   logic [`ROB_SIZE_LOG-1:0] 		    LQ_PRF_ROB;
   logic [`BRAT_SIZE_LOG:0] 		    LQ_PRF_BRAT;
   logic [`PRF_SIZE_LOG-1:0] 		    LQ_PRF_forward_PRF;
   logic [`PRF_SIZE_LOG-1:0] 		    LQ_PRF_regA;
   
   // Logics for Dcache
   logic 				    LQ_Dcache_valid;
   logic [63:0] 			    LQ_Dcache_NPC;
   logic [31:0] 			    LQ_Dcache_IR;
   logic [`ROB_SIZE_LOG-1:0] 		    LQ_Dcache_ROB;
   logic [`BRAT_SIZE_LOG:0] 		    LQ_Dcache_BRAT;
   logic [63:0] 			    LQ_Dcache_MEM_addr;
   logic [`PRF_SIZE_LOG-1:0] 		    LQ_Dcache_regA;
   
   // Logics for PRF_pipeline_before
   logic [1:0] 				    LQ_full;
   

   lq lq_test(.*);

   initial
     begin
	clock = 0;
	reset = 1;
   
	branch_mis = 0;
	branch_mis_brat = 0;
	valid_list = 0;
	valid_list[6] = 1;
	valid_list[20] = 1;
	LSQ_not_erase_PRF = 0;
	LSQ_not_erase_Dcache = 0;
	BRAT_CMT_v = 0;
	BRAT_CMT_num = 0;
	ROB_head = `ROB_SIZE_LOG'd0;
	ROB_head_is_store = 0;
	
	
	ALU_LSQ_valid = 0;
   	ALU_LSQ_NPC = 0;
   	ALU_LSQ_IR = 0;
	ALU_LSQ_ld_or_st = 0;
   	ALU_LSQ_MEM_addr = 0;
	ALU_LSQ_regA = 0;
	ALU_LSQ_ROB = 0;
	ALU_LSQ_BRAT = 0;
	
	sq_valid = `SQ_SIZE'b111111;
	
    	sq_addr_valid[0] = 1;
	sq_MEM_addr[0] = 64'hc;
	sq_regA[0] = `PRF_SIZE_LOG'd4;
	sq_ROB[0] = `ROB_SIZE_LOG'd0;
	
	sq_addr_valid[1] = 0;
	sq_MEM_addr[1] = 0;
	sq_regA[1] = `PRF_SIZE_LOG'd4;;
	sq_ROB[1] = `ROB_SIZE_LOG'd1;

	sq_addr_valid[2] = 0;
	sq_MEM_addr[2] = 0;
	sq_regA[2] = `PRF_SIZE_LOG'd6;;
	sq_ROB[2] = `ROB_SIZE_LOG'd2;

	sq_addr_valid[3] = 1;
	sq_MEM_addr[3] = 64'h8;
	sq_regA[3] = `PRF_SIZE_LOG'd6;
	sq_ROB[3] = `ROB_SIZE_LOG'd3;

	sq_addr_valid[4] = 0;
	sq_MEM_addr[4] = 0;
	sq_regA[4] = `PRF_SIZE_LOG'd20;
	sq_ROB[4] = `ROB_SIZE_LOG'd8;

	sq_addr_valid[5] = 1;
	sq_MEM_addr[5] = 64'h8;
	sq_regA[5] = `PRF_SIZE_LOG'd20;
	sq_ROB[5] = `ROB_SIZE_LOG'd26;

	sq_addr_valid[6] = 0;
	sq_MEM_addr[6] = 0;
	sq_regA[6] = `PRF_SIZE_LOG'd20;
	sq_ROB[6] = `ROB_SIZE_LOG'd8;

	sq_addr_valid[7] = 0;
	sq_MEM_addr[7] = 0;
	sq_regA[7] = `PRF_SIZE_LOG'd20;
	sq_ROB[7] = `ROB_SIZE_LOG'd8;
	
	sq_head = `SQ_SIZE_LOG'h0;
	sq_tail = `SQ_SIZE_LOG'h5;

	@(negedge clock);
	reset = 0;
	@(negedge clock);
	ALU_LSQ_valid = 2'h3;
	ALU_LSQ_ld_or_st = 2'h0;
   	ALU_LSQ_MEM_addr = {64'h4,64'h8};
	ALU_LSQ_regA = {5'd1,5'd2};
	ALU_LSQ_ROB = {5'd5,5'd6};
	ALU_LSQ_BRAT = {3'b100,3'b100};
	@(negedge clock);
	ALU_LSQ_valid = 2'h3;
	ALU_LSQ_ld_or_st = 2'h0;
   	ALU_LSQ_MEM_addr = {64'hc,64'h10};
	ALU_LSQ_regA = {5'd3,5'd4};
	ALU_LSQ_ROB = {5'd9,5'd10};
	ALU_LSQ_BRAT = {3'b100,3'b100};

	sq_addr_valid[4] = 1;
	sq_MEM_addr[4] = 64'hc;
	@(negedge clock);
	ALU_LSQ_valid = 0;

	valid_list[4] = 1;
	sq_addr_valid[1] = 1;
	sq_MEM_addr[1] = 64'h4;

	sq_addr_valid[2] = 1;
	sq_MEM_addr[2] = 64'hc;

	@(negedge clock);
	sq_addr_valid[6] = 1;
	sq_MEM_addr[6] = 64'h1c;
	@(negedge clock);
	@(negedge clock);
	$finish;
	

	
	  

	
     end

   always
     begin
	#10 clock = ~clock;
	
     end
endmodule