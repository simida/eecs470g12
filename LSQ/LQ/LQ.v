/////////////////////////////////////////////////////////////////////
//                                                                 //
// Module: LSQ.v                                                   //
//                                                                 //
// Description: This is a conservative LSQ with bypass & forward   //
//                                                                 //
/////////////////////////////////////////////////////////////////////

module lq(
	  ////////////////
	  // Inputs
	  ////////////////
	  input clock,
	  input reset,
	  
	  // Control Signals
	  input 					branch_mis,
	  input [`BRAT_SIZE:0]				branch_mis_brat,
	  input [`PRF_SIZE:0]				valid_list,
	  input 					LSQ_not_erase_PRF,
	  input						LSQ_not_erase_Dcache,
	  input [1:0]                                   BRAT_CMT_v,
	  input [1:0] [`BRAT_SIZE_LOG-1:0]		BRAT_CMT_num,
	  
	  // Inputs from MEM ALU
	  input [1:0] 					ALU_LSQ_valid,
	  input [1:0] [63:0]				ALU_LSQ_NPC,
	  input [1:0] [31:0]				ALU_LSQ_IR,
	  input [1:0] 					ALU_LSQ_ld_or_st,  // 0 for ld, 1 for st
	  input [1:0] [63:0]				ALU_LSQ_MEM_addr,
	  input [1:0] [`PRF_SIZE_LOG-1:0]		ALU_LSQ_regA,
	  input [1:0] [`ROB_SIZE_LOG-1:0]		ALU_LSQ_ROB,
	  input [1:0] [`BRAT_SIZE_LOG:0]		ALU_LSQ_BRAT,
	  
	  // Inputs from SQ
	  input [`SQ_SIZE-1:0]				sq_valid,
	  input [`SQ_SIZE-1:0]				sq_addr_valid,
	  input [`SQ_SIZE-1:0] [63:0]			sq_MEM_addr,
	  input [`SQ_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 	sq_regA,
	  input [`SQ_SIZE-1:0] [`ROB_SIZE_LOG-1:0]	sq_ROB,
	  input [`SQ_SIZE_LOG-1:0]			sq_head,
	  input [`SQ_SIZE_LOG-1:0]			sq_tail,

	  // Inputs from ROB
	  input [`ROB_SIZE_LOG-1:0]			ROB_head,
	  input [1:0]					ROB_head_is_store,

	  ////////////////
	  // Outputs
	  ////////////////

	  // Outputs for PRF_after
	  output 					LQ_PRF_valid,
	  output [63:0]					LQ_PRF_NPC,
	  output [31:0]					LQ_PRF_IR,
	  output [`ROB_SIZE_LOG-1:0]			LQ_PRF_ROB,
	  output [`BRAT_SIZE_LOG:0]			LQ_PRF_BRAT,
	  output [`PRF_SIZE_LOG-1:0]			LQ_PRF_forward_PRF,
	  output [`PRF_SIZE_LOG-1:0]			LQ_PRF_regA,

	  // Outputs for Dcache
	  output 					LQ_Dcache_valid,
	  output [63:0]					LQ_Dcache_NPC,
	  output [31:0]					LQ_Dcache_IR,
	  output [`ROB_SIZE_LOG-1:0]			LQ_Dcache_ROB,
	  output [`BRAT_SIZE_LOG:0]			LQ_Dcache_BRAT,
	  output [63:0]					LQ_Dcache_MEM_addr,
	  output [`PRF_SIZE_LOG-1:0]			LQ_Dcache_regA,

	  // Outputs for PRF_pipeline_before
	  output [1:0]					LQ_full
	  );

   // Registers for LQ
   logic [`LQ_SIZE-1:0] [63:0] 				lq_NPC;
   logic [`LQ_SIZE-1:0] [31:0] 				lq_IR;
   logic [`LQ_SIZE-1:0] 				lq_valid;
   logic [`LQ_SIZE-1:0] [63:0] 				lq_MEM_addr;
   logic [`LQ_SIZE-1:0] [`ROB_SIZE_LOG-1:0] 		lq_ROB;
   logic [`LQ_SIZE-1:0] [`BRAT_SIZE_LOG:0] 		lq_BRAT;
   logic [`LQ_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		lq_regA;
   
   // Wires for Dispatch LD
   logic [1:0] [`LQ_SIZE-1:0] 				lq_in_valid;
   logic [1:0] 						lq_num_of_avail;
   
   
   // Wires for Issue LD
   logic [`LQ_SIZE-1:0] [`SQ_SIZE-1:0] 			ls_in_front;
   logic [`LQ_SIZE-1:0] [`SQ_SIZE-1:0] 			ls_addr_same;
   logic [`LQ_SIZE-1:0] [`SQ_SIZE-1:0] 			ls_potential_forward_path;
   logic [`LQ_SIZE-1:0] [`SQ_SIZE-1:0] 			ls_known_forward_path;
   logic [`LQ_SIZE-1:0] [`SQ_SIZE-1:0] 			ls_all_forward_path;
   logic [`LQ_SIZE-1:0] [`SQ_SIZE-1:0] 			ls_all_forward_path_reverse;
   logic [`LQ_SIZE-1:0] [`SQ_SIZE-1:0] 			ls_latest_forward_path_reverse;
   logic [`LQ_SIZE-1:0] [`SQ_SIZE-1:0] 			ls_latest_forward_path;
   logic [`SQ_SIZE-1:0] 				sq_regA_valid;            // In the future this should be given as input
   logic [`LQ_SIZE-1:0] 				ready_to_go_PRF;
   logic [`LQ_SIZE-1:0] 				ready_to_go_Dcache;
   wor   [`LQ_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		forward_PRF;  
   logic [`LQ_SIZE-1:0] 				go_PRF;
   logic [`LQ_SIZE-1:0] 				go_Dcache; 
   logic [`LQ_SIZE-1:0] [`ROB_SIZE_LOG-1:0] 		lq_ROB_true;
   logic [`SQ_SIZE-1:0] [`ROB_SIZE_LOG-1:0] 		sq_ROB_true;
   logic [`LQ_SIZE-1:0] 				lq_free_valid_PRF;
   logic [`LQ_SIZE-1:0] 				lq_free_valid_Dcache;
   
   // Wires for PRF outputs
   wor 							LQ_PRF_valid_tmp;
   wor [63:0] 						LQ_PRF_NPC_tmp;
   wor [31:0] 						LQ_PRF_IR_tmp;
   wor [`ROB_SIZE_LOG-1:0] 				LQ_PRF_ROB_tmp;
   wor [`BRAT_SIZE_LOG:0] 				LQ_PRF_BRAT_tmp;
   wor [`PRF_SIZE_LOG-1:0] 				LQ_PRF_forward_PRF_tmp;
   wor [`PRF_SIZE_LOG-1:0] 				LQ_PRF_regA_tmp;
   
   // Wires for Dcache outputs
   wor 							LQ_Dcache_valid_tmp;
   wor [63:0] 						LQ_Dcache_NPC_tmp;
   wor [31:0] 						LQ_Dcache_IR_tmp;
   wor [`ROB_SIZE_LOG-1:0] 				LQ_Dcache_ROB_tmp;
   wor [`BRAT_SIZE_LOG:0] 				LQ_Dcache_BRAT_tmp;
   wor [63:0] 						LQ_Dcache_MEM_addr_tmp;
   wor [`PRF_SIZE_LOG-1:0] 				LQ_Dcache_regA_tmp;
   
   genvar 						i,j,k;
   
   ////////////////////////////////////////////////////////////////
   // ISSUE LOGIC
   ////////////////////////////////////////////////////////////////

   // True lq ROB Calculation
   generate for(i=0;i<`LQ_SIZE;i++)
     begin
	assign lq_ROB_true[i] = (lq_ROB[i] - ROB_head);
     end
   endgenerate

   // True sq ROB Calculation
   generate for(i=0;i<`SQ_SIZE;i++)
     begin
	assign sq_ROB_true[i] = (sq_ROB[i] - ROB_head);
     end
   endgenerate

   // sq_regA_valid Calculation
   generate for(i=0;i<`SQ_SIZE;i++)
     begin
	assign sq_regA_valid[i] = valid_list[sq_regA[i]];
     end
   endgenerate
   
   // Calculating All useful imformation for issue
   generate for(i=0;i<`LQ_SIZE;i++)
     begin
	for(j=0;j<`SQ_SIZE;j++)
	  begin
	     assign ls_in_front[i][j] = sq_valid[j] && (sq_ROB_true[j] < lq_ROB_true[i]);
	     assign ls_addr_same[i][j] = sq_valid[j] && (lq_MEM_addr[i] == sq_MEM_addr[j]);
	     assign ls_potential_forward_path[i][j] = ls_in_front[i][j] && ~sq_addr_valid[j];
	     assign ls_known_forward_path[i][j] =  ls_in_front[i][j] && sq_addr_valid[j] && ls_addr_same[i][j];
	     assign ls_all_forward_path[i][j] = ls_potential_forward_path[i][j] | ls_known_forward_path[i][j];  // This includes known forward paths and all paths that have no address(potential forward paths)
	     assign ls_all_forward_path_reverse[i][j] = ls_all_forward_path[i][`SQ_SIZE-j-1];
	     // Here we path a psr to determine the latest forward path
	     assign ls_latest_forward_path[i][j] = ls_latest_forward_path_reverse[i][`SQ_SIZE-j-1];               // This result is the latest possible forward path. If all zero, it means all paths are impossible to forward
	     assign forward_PRF[i] = ls_latest_forward_path[i][j] ? sq_regA[j] : `PRF_SIZE_LOG'b0;
	  end // for (j=0;j<`SQ_SIZE;j++)
	
	`PSR(`SQ_SIZE) psr_latest_forward[i](.req(ls_all_forward_path_reverse[i]), .en(1'b1), .count(~(sq_tail)), .gnt(ls_latest_forward_path_reverse[i]));
	
	// Go PTF: it knows there is a forward path, and the latest forward path is known, and BRAT is still valid
	assign ready_to_go_PRF[i] = ((ls_known_forward_path[i] & ls_latest_forward_path[i] & sq_regA_valid) != `LQ_SIZE'b0) & (!branch_mis | !branch_mis_brat[lq_BRAT[i]]);  

	// Go Dcahce: it knows there is no possible forward paths. No forward path found or need to check. Also BRAT need to be valid
	assign ready_to_go_Dcache[i] = lq_valid[i] & (ls_all_forward_path[i] == `LQ_SIZE'b0) & (!branch_mis | !branch_mis_brat[lq_BRAT[i]]);  
	assign lq_free_valid_PRF[i] = go_PRF[i] & ~LSQ_not_erase_PRF;
	assign lq_free_valid_Dcache[i] = go_Dcache[i] & ~LSQ_not_erase_Dcache;
	
     end
   endgenerate

   `PS(`LQ_SIZE) ps_out_PRF (ready_to_go_PRF, 1'b1, go_PRF, );
   `PS(`LQ_SIZE) ps_out_Dcache (ready_to_go_Dcache, (ROB_head_is_store == 2'b0), go_Dcache, );
   
   

   generate for(i=0;i<`LQ_SIZE;i++)
     begin
	assign LQ_PRF_valid_tmp = go_PRF[i] ? 1 : 0;
	assign LQ_PRF_NPC_tmp   = go_PRF[i] ? lq_NPC[i] : 0;
	assign LQ_PRF_IR_tmp    = go_PRF[i] ? lq_IR[i] : 0;
	assign LQ_PRF_ROB_tmp   = go_PRF[i] ? lq_ROB[i] : 0;
	assign LQ_PRF_BRAT_tmp  = go_PRF[i] ? lq_BRAT[i] : 0;
	assign LQ_PRF_forward_PRF_tmp  = go_PRF[i] ? forward_PRF[i] : 0;
	assign LQ_PRF_regA_tmp  = go_PRF[i] ? lq_regA[i] : 0;
	
     end
   endgenerate

   generate for(i=0;i<`LQ_SIZE;i++)
     begin
	assign LQ_Dcache_valid_tmp = go_Dcache[i] ? 1 : 0;
	assign LQ_Dcache_NPC_tmp   = go_Dcache[i] ? lq_NPC[i] : 0;
	assign LQ_Dcache_IR_tmp    = go_Dcache[i] ? lq_IR[i] : 0;
	assign LQ_Dcache_ROB_tmp   = go_Dcache[i] ? lq_ROB[i] : 0;
	assign LQ_Dcache_BRAT_tmp  = go_Dcache[i] ? lq_BRAT[i] : 0;
	assign LQ_Dcache_MEM_addr_tmp  = go_Dcache[i] ? lq_MEM_addr[i] : 0;
	assign LQ_Dcache_regA_tmp  = go_Dcache[i] ? lq_regA[i] : 0;
     end
   endgenerate

   assign LQ_PRF_valid = LQ_PRF_valid_tmp;
   assign LQ_PRF_NPC   = LQ_PRF_NPC_tmp;
   assign LQ_PRF_IR    = LQ_PRF_IR_tmp;
   assign LQ_PRF_ROB   = LQ_PRF_ROB_tmp;
   assign LQ_PRF_BRAT  = (BRAT_CMT_v[0] && LQ_PRF_BRAT_tmp==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
			    (BRAT_CMT_v[1] && LQ_PRF_BRAT_tmp==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
			    LQ_PRF_BRAT_tmp;
   assign LQ_PRF_forward_PRF  = LQ_PRF_forward_PRF_tmp;
   assign LQ_PRF_regA  = LQ_PRF_regA_tmp;
   
   assign LQ_Dcache_valid = LQ_Dcache_valid_tmp;
   assign LQ_Dcache_NPC   = LQ_Dcache_NPC_tmp;
   assign LQ_Dcache_IR    = LQ_Dcache_IR_tmp;
   assign LQ_Dcache_ROB   = LQ_Dcache_ROB_tmp;
   assign LQ_Dcache_BRAT  = (BRAT_CMT_v[0] && LQ_Dcache_BRAT_tmp==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
			    (BRAT_CMT_v[1] && LQ_Dcache_BRAT_tmp==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
			    LQ_Dcache_BRAT_tmp;
   assign LQ_Dcache_MEM_addr  = LQ_Dcache_MEM_addr_tmp;
   assign LQ_Dcache_regA  = LQ_Dcache_regA_tmp;

   `NoO(`LQ_SIZE) N_of_avail_out_calc (.Nin(~lq_valid), .Nout(lq_num_of_avail));
   
   assign LQ_full = (!lq_num_of_avail) ? 2'b11 : (lq_num_of_avail == 2'b10 | lq_num_of_avail == 2'b01);
   

	     

   /****************************************************************
    
    Following Codes are ripple carried. They should be correct but slow
    
    ****************************************************************/
	     
   /*
    logic [`LQ_SIZE-1:0] 				stop_check;
    logic [`LQ_SIZE-1:0][`PRF_SIZE_LOG-1:0]  pointer;
    generate for(i=0;i<`LQ_SIZE;i++)
      begin
        always_comb
          begin
	      
	      ready_to_go[i] = lq_valid[i];
	      forward[i] = 0;
	      forward_PRF[i] = 0;
	      stop_check[i] = 0;
	      for(pointer[i]=sq_head;pointer[i]<sq_tail;pointer[i] = pointer[i]+`LQ_SIZE'b1)
	      begin
		  if(~sq_valid[pointer[i]] || stop_check[i]) ;
		  else if(~sq_addr_valid[pointer[i]]) ;
		  else if(sq_in_front[i][pointer[i]] && sq_addr_same[i][pointer[i]])
		    begin
		       ready_to_go[i] = valid_list[sq_regA[pointer[i]]];
		       stop_check[i] = 1;
		       forward[i] = 1;
		       forward_PRF[i] = sq_regA[pointer[i]];
		    end
		  else if(sq_in_front[i][pointer[i]] && ~sq_addr_same[i][pointer[i]])
		    ready_to_go[i] = 1;
	       end // for (pointer[i]=sq_head;pointer[i]<sq_tail;pointer[i] = (pointer[i] == ~`SQ_SIZE_LOG'b0) ? 0 :pointer[i]+1)
	  end // always_comb begin

	assign ready_to_go_PRF[i] = ready_to_go[i] & forward_PRF & (!branch_mis | !branch_mis_brat[lq_BRAT[i]]);
	assign ready_to_go_Dcache[i] = ready_to_go[i] & ~forward_PRF & (!branch_mis | !branch_mis_brat[lq_BRAT[i]]);

     end
   endgenerate
    
    */




   ////////////////////////////////////////////////////////////////
   // Dispatch logic
   ////////////////////////////////////////////////////////////////

   `RPS(`LQ_SIZE) rps_in (.req(~lq_valid), .en(ALU_LSQ_valid & (~ALU_LSQ_ld_or_st)), .gnt(lq_in_valid), .count(`LQ_SIZE_LOG'h0));
   
   generate
      for (i=0;i<`LQ_SIZE;i++)
	begin
	   always_ff@(posedge clock)
	     begin
		if(reset)
		  begin
		     lq_NPC[i] 	 	<= `SD 0;
		     lq_IR[i]  		<= `SD 0;
		     lq_valid[i] 	<= `SD 0;
		     lq_MEM_addr[i] 	<= `SD 0;
		     lq_ROB[i] 	 	<= `SD 0;
		     lq_BRAT[i]  	<= `SD 0;
		     lq_regA[i] 	<= `SD 0;
		  end
		else if(branch_mis && branch_mis_brat[lq_BRAT[i]])
		  begin
		     lq_NPC[i] 	 	<= `SD 0;
		     lq_IR[i]  		<= `SD 0;
		     lq_valid[i] 	<= `SD 0;
		     lq_MEM_addr[i] 	<= `SD 0;
		     lq_ROB[i] 	 	<= `SD 0;
		     lq_BRAT[i]  	<= `SD 0;
		     lq_regA[i] 	<= `SD 0;
		  end
		else if(lq_in_valid[0][i])
		  begin
		     lq_NPC[i] 	 	<= `SD ALU_LSQ_NPC[0];
		     lq_IR[i]  	 	<= `SD ALU_LSQ_IR[0];
		     lq_valid[i]       	<= `SD ALU_LSQ_valid[0];
		     lq_MEM_addr[i] 	<= `SD ALU_LSQ_MEM_addr[0];
		     lq_ROB[i] 	 	<= `SD ALU_LSQ_ROB[0];
		     lq_BRAT[i]       	<= `SD ALU_LSQ_BRAT[0];
		     lq_regA[i]        	<= `SD ALU_LSQ_regA[0];
		  end
		else if(lq_in_valid[1][i])
		  begin
		     lq_NPC[i] 	 	<= `SD ALU_LSQ_NPC[1];
		     lq_IR[i]  	 	<= `SD ALU_LSQ_IR[1];
		     lq_valid[i]       	<= `SD ALU_LSQ_valid[1];
		     lq_MEM_addr[i] 	<= `SD ALU_LSQ_MEM_addr[1];
		     lq_ROB[i] 	 	<= `SD ALU_LSQ_ROB[1];
		     lq_BRAT[i]         <= `SD ALU_LSQ_BRAT[1];
		     lq_regA[i]        	<= `SD ALU_LSQ_regA[1];
		  end
		else if(lq_free_valid_PRF[i] | lq_free_valid_Dcache[i])
		  begin
		     lq_NPC[i] 	 	<= `SD 0;
		     lq_IR[i]  		<= `SD 0;
		     lq_valid[i] 	<= `SD 0;
		     lq_MEM_addr[i] 	<= `SD 0;
		     lq_ROB[i] 	 	<= `SD 0;
		     lq_BRAT[i]  	<= `SD 0;
		     lq_regA[i] 	<= `SD 0;
		  end
		else
		  begin
		     if (BRAT_CMT_v[0] && lq_BRAT[i]==BRAT_CMT_num[0])
		       lq_BRAT[i] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};
		     if (BRAT_CMT_v[1] && lq_BRAT[i]==BRAT_CMT_num[1])
		       lq_BRAT[i] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};
		  end // else: !if(lq_free_valid[0][i] | lq_free_valid[1][i])
	     end // always_ff@ (posedge clock)
	end // for (i=0;i<`LQ_SIZE;i++)
   endgenerate
endmodule // lq

   
      

   
				   

   
   
   
			    
	   