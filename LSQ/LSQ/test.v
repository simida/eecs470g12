

module test();

   logic clock;
   logic reset;
   
   // Control Signals
   logic branch_mis;
   logic [`BRAT_SIZE:0] branch_mis_brat;
   logic [`PRF_SIZE:0] 	valid_list;
   logic 		LSQ_not_erase_PRF;
   logic [1:0] 		BRAT_CMT_v;
   logic [1:0] [`BRAT_SIZE_LOG-1:0] BRAT_CMT_num;
   
   // Logics from MEM ALU
   logic [1:0] 			    ALU_LSQ_valid;
   logic [1:0] [63:0] 		    ALU_LSQ_NPC;
   logic [1:0] [31:0] 		    ALU_LSQ_IR;
   logic [1:0] 			    ALU_LSQ_ld_or_st;
   logic [1:0] [63:0] 		    ALU_LSQ_MEM_addr;
   logic [1:0] [`PRF_SIZE_LOG-1:0]  ALU_LSQ_regA;
   logic [1:0] [`ROB_SIZE_LOG-1:0]  ALU_LSQ_ROB;
   logic [1:0] [`BRAT_SIZE_LOG:0]   ALU_LSQ_BRAT;

   // Logics from SQ
   logic [`SQ_SIZE-1:0] 	    sq_valid;
   logic [`SQ_SIZE-1:0] 	    sq_addr_valid;
   logic [`SQ_SIZE-1:0] [63:0] 	    sq_MEM_addr;
   logic [`SQ_SIZE-1:0] [`PRF_SIZE_LOG-1:0] sq_regA;
   logic [`SQ_SIZE-1:0] [`ROB_SIZE_LOG-1:0] sq_ROB;
   logic [`SQ_SIZE_LOG-1:0] 		    sq_head;
   logic [`SQ_SIZE_LOG-1:0] 		    sq_tail;

   // Logics from ROB
   logic [`ROB_SIZE_LOG-1:0] 		    ROB_head;
   logic 				    ROB_head_is_store;

   // Logics for PRF_after
   logic 				    LQ_PRF_valid;
   logic [63:0] 			    LQ_PRF_NPC;
   logic [31:0] 			    LQ_PRF_IR;
   logic [`ROB_SIZE_LOG-1:0] 		    LQ_PRF_ROB;
   logic [`BRAT_SIZE_LOG:0] 		    LQ_PRF_BRAT;
   logic [`PRF_SIZE_LOG-1:0] 		    LQ_PRF_forward_PRF;
   logic [`PRF_SIZE_LOG-1:0] 		    LQ_PRF_regA;
   
	//input to LSQ
	logic					MSHR_full;//??? not use head/tail, but just count???
	logic					LD_BUF_full;

	//output to Dcache Controller
	logic 			LSQ_DC_rd_mem;
	logic   			LSQ_DC_wr_mem;
	logic 	 [63:0]		LSQ_DC_addr;
	logic [`PRF_SIZE_LOG-1:0]	LSQ_DC_PRF_num;
	logic 	 [`BRAT_SIZE_LOG-1:0]	LSQ_DC_BRAT_num;
	logic 	 [`ROB_SIZE_LOG-1:0]	LSQ_DC_ROB_num;

   
   // Logics for PRF_pipeline_before
   logic [1:0] 				    LQ_full;



   logic  [1:0] [`SQ_SIZE_LOG-1:0]	ALU_LSQ_SQ_addr;


  // From Front End: Dispatch
   logic  [1:0]	 			instr_is_st; //make sure it's valid and store
   logic  [1:0] [`PRF_SIZE_LOG-1:0] 	regA;
   logic  [1:0] [`ROB_SIZE_LOG-1:0] 	ROB;
   logic  [1:0] [`BRAT_SIZE_LOG:0] 	BRAT;
   logic  [1:0] [63:0] 			NPC;
   logic  [1:0] [31:0] 			IR;

  //Early Branch Resolution
   logic  [`SQ_SIZE_LOG-1:0]		Bsq_tail; //From Front End, on a misprediction, replace current tail
  // Output to ROB: Issue
   logic 				SQ_CMP;
  // Outputs to FrontEnd
   logic  [1:0]				SQ_full;


   lsq lsq_test(.*);

   initial
     begin
	clock = 0;
	reset = 1;
	NPC=0;
	IR=0;
	ROB_head = `ROB_SIZE_LOG'd0;
	MSHR_full=0;
	LD_BUF_full=0;

   
	branch_mis = 0;
	branch_mis_brat = 0;
	LSQ_not_erase_PRF = 0;
	BRAT_CMT_v = 0;
	BRAT_CMT_num = 0;

	Bsq_tail = `SQ_SIZE_LOG'h6;


	ALU_LSQ_valid = 0;
   	ALU_LSQ_NPC = 0;
   	ALU_LSQ_IR = 0;
	ALU_LSQ_ld_or_st = 0;
   	ALU_LSQ_MEM_addr = 0;
	ALU_LSQ_regA = 0;
	ALU_LSQ_ROB = 0;
	ALU_LSQ_BRAT = 0;


	ALU_LSQ_SQ_addr=0;

	valid_list = 0;
	valid_list[6] = 1;
	valid_list[20] = 1;

	ROB_head_is_store = 0;

;
	@(negedge clock);
	BRAT=3'b100;	

	instr_is_st=1'b1;
	regA=`PRF_SIZE_LOG'd4;
	ROB=`ROB_SIZE_LOG'd0;
	reset = 0;

	@(negedge clock);
	instr_is_st=1'b1;
	regA=`PRF_SIZE_LOG'd4;
	ROB=`ROB_SIZE_LOG'd1;

	@(negedge clock);
	instr_is_st=1'b1;
	regA=`PRF_SIZE_LOG'd6;
	ROB=`ROB_SIZE_LOG'd2;

	@(negedge clock);
	instr_is_st=1'b1;
	regA=`PRF_SIZE_LOG'd6;
	ROB=`ROB_SIZE_LOG'd3;

	@(negedge clock);
	instr_is_st=1'b1;
	regA=`PRF_SIZE_LOG'd20;
	ROB=`ROB_SIZE_LOG'd8;

	@(negedge clock);
	instr_is_st=1'b1;
	regA=`PRF_SIZE_LOG'd20;
	ROB=`ROB_SIZE_LOG'd26;


	@(negedge clock);

	BRAT=3'b000;	

	instr_is_st=2'b11;
	regA={`PRF_SIZE_LOG'd20, `PRF_SIZE_LOG'd20};
	ROB={`ROB_SIZE_LOG'd28,`ROB_SIZE_LOG'd27};

	



	//ST 0<-12, 3<-12
	@(negedge clock);
	instr_is_st=2'b00;
	ALU_LSQ_valid = 2'h3;
	ALU_LSQ_ld_or_st = 2'b11;
	ALU_LSQ_SQ_addr = {3'd3,3'd0};
	ALU_LSQ_MEM_addr= {64'h8,64'hc};

	//ST 5<-8 
	@(negedge clock);
	ALU_LSQ_valid = 2'b01;
	ALU_LSQ_ld_or_st = 2'b01;
	ALU_LSQ_SQ_addr = {0,3'd5};
	ALU_LSQ_MEM_addr= {0,64'h8};


	@(negedge clock);
	ALU_LSQ_valid = 2'h0;
	ALU_LSQ_ld_or_st = 2'b00;


	@(negedge clock);
	ALU_LSQ_valid = 2'h3;
	ALU_LSQ_ld_or_st = 2'h0;
   	ALU_LSQ_MEM_addr = {64'h4,64'h8};
	ALU_LSQ_regA = {5'd1,5'd2};
	ALU_LSQ_ROB = {5'd5,5'd6};
	ALU_LSQ_BRAT = {3'b100,3'b100};

	//ST 4<-12
	@(negedge clock);
	ALU_LSQ_valid = 2'b01;
	ALU_LSQ_ld_or_st = 2'b01;
	ALU_LSQ_SQ_addr = {0,3'd4};
	ALU_LSQ_MEM_addr= {0,64'hc};



	@(negedge clock);
	ALU_LSQ_valid = 2'h3;
	ALU_LSQ_ld_or_st = 2'h0;
   	ALU_LSQ_MEM_addr = {64'hc,64'h10};
	ALU_LSQ_regA = {5'd3,5'd4};
	ALU_LSQ_ROB = {5'd9,5'd10};
	ALU_LSQ_BRAT = {3'b100,3'b100};



	/*sq_addr_valid[4] = 1;
	sq_MEM_addr[4] = 64'hc;*/

	//ST 1<-4  2<-12 
	@(negedge clock);
	ALU_LSQ_valid = 2'b11;
	ALU_LSQ_ld_or_st = 2'b11;
	ALU_LSQ_SQ_addr = {3'd2,3'd1};
	ALU_LSQ_MEM_addr= {64'hc, 64'h4};

	@(negedge clock);
	//ALU_LSQ_valid = 0;

	valid_list[4] = 1;


	/*sq_addr_valid[1] = 1;
	sq_MEM_addr[1] = 64'h4;
	sq_addr_valid[2] = 1;
	sq_MEM_addr[2] = 64'hc;*/

	//ST 6<-28
	ALU_LSQ_valid = 2'b01;
	ALU_LSQ_ld_or_st = 2'b01;
	ALU_LSQ_SQ_addr = {0,3'd6};
	ALU_LSQ_MEM_addr= {0,64'h1c};

	@(negedge clock);
	/*sq_addr_valid[6] = 1;
	sq_MEM_addr[6] = 64'h1c;*/

	ALU_LSQ_valid = 0;
	
	@(negedge clock);
	//branch_mis = 1;
	//branch_mis_brat=1;
	BRAT_CMT_v = 2'b01;
	BRAT_CMT_num = {0, 0};
	@(negedge clock);
	ROB_head_is_store = 1;
	@(negedge clock);
	ROB_head_is_store = 1;
	@(negedge clock);
	ROB_head_is_store = 1;
	@(negedge clock);




	$finish;
	  

	
     end

   always
     begin
	#10 clock = ~clock;
	
     end
endmodule
