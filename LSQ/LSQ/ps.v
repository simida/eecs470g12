/////////////////////////////////////////////////////////////////////
//                                                                 //
// Module: ps.v                                                    //
//                                                                 //
// Description: All Kinds of PS, RPS, DPS, PPS, MIPS, etc          //
//              (All but not my school application PS. WTF!!!! )   //
//                                                                 //
/////////////////////////////////////////////////////////////////////

/****************************************************************/
// Single select PS
/****************************************************************/
module ps2(req, en, gnt, req_up);
	input [1:0] req;
	input en;
	output [1:0] gnt;
	output req_up;

	assign gnt[0] = (en & req[0]) ? 1'b1 : 1'b0;
	assign gnt[1] = (en & ~req[0] & req[1])? 1'b1 : 1'b0;
	assign req_up = | req[1:0];	

endmodule // ps2

/****************************************************************/

module ps4(req, en, gnt, req_up);
	input [3:0] req;
	input en;
	output [3:0] gnt;
	output req_up;

	wire req_left, req_right;
	wire gnt_left, gnt_right;
	wire [3:0] temp;

	ps2 left(req[3:2], en, temp[3:2], req_left);
	ps2 right(req[1:0], en, temp[1:0], req_right);
	ps2 top({req_left, req_right}, en, {gnt_left, gnt_right}, req_up);

	assign gnt[3:2] = temp[3:2] & {gnt_left, gnt_left};
	assign gnt[1:0] = temp[1:0] & {gnt_right, gnt_right};

endmodule // ps4

/****************************************************************/

module ps8(req, en, gnt, req_up);
   input [7:0] req;
   input       en;
   output [7:0] gnt;
   output 	req_up;
   
   wire 	req_left, req_right;
   wire 	gnt_left, gnt_right;
   wire [7:0] 	temp;
   
   ps4 left(req[7:4], en, temp[7:4], req_left);
   ps4 right(req[3:0], en, temp[3:0], req_right);
   ps2 top({req_left, req_right}, en, {gnt_left, gnt_right}, req_up);
   
   assign gnt[7:4] = temp[7:4] & {gnt_left, gnt_left, gnt_left, gnt_left};
   assign gnt[3:0] = temp[3:0] & {gnt_right, gnt_right, gnt_right, gnt_right};

endmodule // ps8

/****************************************************************/

module ps16(req, en, gnt, req_up);
	input [15:0] req;
	input en;
	output [15:0] gnt;
	output req_up;

	wire req_left, req_right;
	wire gnt_left, gnt_right;
	wire [15:0] temp;

	ps8 left(req[15:8], en, temp[15:8], req_left);
	ps8 right(req[7:0], en, temp[7:0], req_right);
	ps2 top({req_left, req_right}, en, {gnt_left, gnt_right}, req_up);

	assign gnt[15:8] = temp[15:8] & {8{gnt_left}};
	assign gnt[7:0] = temp[7:0] & {8{gnt_right}};

endmodule // ps16

/****************************************************************/
// Single Select RPS
/****************************************************************/

module psr4(req, en, count ,gnt);

   input [3:0] req; 
   input      en; 
   input [1:0] count; 
   output logic [3:0] gnt; 
   logic [3:0] 	req_tmp;
   logic [3:0] 	gnt_tmp;
   
   wire 	req_left, req_right;
   wire 	gnt_left, gnt_right;
   wire [3:0] 	temp;

   always_comb begin
      if (count == 2'b00) req_tmp = req;
      else if (count == 2'b01) req_tmp = {req[0], req[3:1]};
      else if (count == 2'b10) req_tmp = {req[1:0], req[3:2]};
      else req_tmp = {req[2:0], req[3]};

   end
   
   ps2 left(req_tmp[3:2], en, temp[3:2], req_left);
   ps2 right(req_tmp[1:0], en, temp[1:0], req_right);
   ps2 top({req_left, req_right}, en, {gnt_left, gnt_right}, req_up);
   
   assign gnt_tmp[3:2] = temp[3:2] & {gnt_left, gnt_left};
   assign gnt_tmp[1:0] = temp[1:0] & {gnt_right, gnt_right};

   
   always_comb begin
      if (count == 2'b00) gnt = gnt_tmp;
      else if (count == 2'b01) gnt = {gnt_tmp[2:0], gnt_tmp[3]};
      else if (count == 2'b10) gnt = {gnt_tmp[1:0], gnt_tmp[3:2]};
      else gnt = {gnt_tmp, gnt_tmp[3:1]};		
   end
   
endmodule // psr4

/****************************************************************/

 module psr8(req, en, count ,gnt); 
   input [7:0] req; 
   input      en; 
   input [2:0] count; 
   output logic [7:0] gnt; 
   logic [7:0] 	req_tmp;
   logic [7:0] 	gnt_tmp;
   
   wire 	req_left, req_right;
   wire 	gnt_left, gnt_right;
   wire [7:0] 	temp;

   always_comb begin
      if (count == 3'b000) req_tmp = req;
      else if (count == 3'b001) req_tmp = {req[0], req[7:1]};
      else if (count == 3'b010) req_tmp = {req[1:0], req[7:2]};
      else if (count == 3'b011) req_tmp = {req[2:0], req[7:3]};
      else if (count == 3'b100) req_tmp = {req[3:0], req[7:4]};
      else if (count == 3'b101) req_tmp = {req[4:0], req[7:5]};
      else if (count == 3'b110) req_tmp = {req[5:0], req[7:6]};
      else req_tmp = {req[6:0], req[7]};
   end
   
   ps4 left(req_tmp[7:4], en, temp[7:4], req_left);
   ps4 right(req_tmp[3:0], en, temp[3:0], req_right);
   ps2 top({req_left, req_right}, en, {gnt_left, gnt_right}, req_up);
   
   assign gnt_tmp[7:4] = temp[7:4] & {gnt_left, gnt_left, gnt_left, gnt_left};
   assign gnt_tmp[3:0] = temp[3:0] & {gnt_right, gnt_right, gnt_right, gnt_right};

   always_comb begin
      if (count == 3'b000) gnt = gnt_tmp;
      else if (count == 3'b001) gnt = {gnt_tmp[6:0], gnt_tmp[7]};
      else if (count == 3'b010) gnt = {gnt_tmp[5:0], gnt_tmp[7:6]};
      else if (count == 3'b011) gnt = {gnt_tmp[4:0], gnt_tmp[7:5]};
      else if (count == 3'b100) gnt = {gnt_tmp[3:0], gnt_tmp[7:4]};
      else if (count == 3'b101) gnt = {gnt_tmp[2:0], gnt_tmp[7:3]};
      else if (count == 3'b110) gnt = {gnt_tmp[1:0], gnt_tmp[7:2]};
      else gnt = {gnt_tmp[0], gnt_tmp[7:1]};		
   end
   
endmodule // psr8

/****************************************************************/

 module psr16(req, en, count ,gnt); 
   input [15:0] req; 
   input      en; 
   input [3:0] count; 
   output logic [15:0] gnt; 
   logic [15:0] 	req_tmp;
   logic [15:0] 	gnt_tmp;
   
   wire 	req_left, req_right;
   wire 	gnt_left, gnt_right;
   wire [15:0] 	temp;

   always_comb begin
      case (count)
	4'b0000: req_tmp = req;
	4'b0001: req_tmp = {req[0], req[15:1]};
	4'b0010: req_tmp = {req[1:0], req[15:2]};
	4'b0011: req_tmp = {req[2:0], req[15:3]};
	4'b0100: req_tmp = {req[3:0], req[15:4]};
	4'b0101: req_tmp = {req[4:0], req[15:5]};
	4'b0110: req_tmp = {req[5:0], req[15:6]};
	4'b0111: req_tmp = {req[6:0], req[15:7]};
	4'b1000: req_tmp = {req[7:0], req[15:8]};
	4'b1001: req_tmp = {req[8:0], req[15:9]};
	4'b1010: req_tmp = {req[9:0], req[15:10]};
	4'b1011: req_tmp = {req[10:0], req[15:11]};
	4'b1100: req_tmp = {req[11:0], req[15:12]};
	4'b1101: req_tmp = {req[12:0], req[15:13]};
	4'b1110: req_tmp = {req[13:0], req[15:14]};
	4'b1111: req_tmp = {req[14:0], req[15]};
	default: req_tmp = req;
      endcase
   end
   
   ps8 left(req_tmp[15:8], en, temp[15:8], req_left);
   ps8 right(req_tmp[7:0], en, temp[7:0], req_right);
   ps2 top({req_left, req_right}, en, {gnt_left, gnt_right}, req_up);
   
   assign gnt_tmp[15:8] = temp[15:8] & {gnt_left, gnt_left, gnt_left, gnt_left,gnt_left, gnt_left, gnt_left, gnt_left};
   assign gnt_tmp[7:0] = temp[7:0] & {gnt_right, gnt_right, gnt_right, gnt_right,gnt_right, gnt_right, gnt_right, gnt_right};

      always_comb begin
      case (count)
	4'b0000: gnt = gnt_tmp;
	4'b0001: gnt = {gnt_tmp[14:0], gnt_tmp[15]};
	4'b0010: gnt = {gnt_tmp[13:0], gnt_tmp[15:14]};
	4'b0011: gnt = {gnt_tmp[12:0], gnt_tmp[15:13]};
	4'b0100: gnt = {gnt_tmp[11:0], gnt_tmp[15:12]};
	4'b0101: gnt = {gnt_tmp[10:0], gnt_tmp[15:11]};
	4'b0110: gnt = {gnt_tmp[9:0], gnt_tmp[15:10]};
	4'b0111: gnt = {gnt_tmp[8:0], gnt_tmp[15:9]};
	4'b1000: gnt = {gnt_tmp[7:0], gnt_tmp[15:8]};
	4'b1001: gnt = {gnt_tmp[6:0], gnt_tmp[15:7]};
	4'b1010: gnt = {gnt_tmp[5:0], gnt_tmp[15:6]};
	4'b1011: gnt = {gnt_tmp[4:0], gnt_tmp[15:5]};
	4'b1100: gnt = {gnt_tmp[3:0], gnt_tmp[15:4]};
	4'b1101: gnt = {gnt_tmp[2:0], gnt_tmp[15:3]};
	4'b1110: gnt = {gnt_tmp[1:0], gnt_tmp[15:2]};
	4'b1111: gnt = {gnt_tmp[0], gnt_tmp[15:1]};
	default: gnt = gnt_tmp;
      endcase
   end

   
endmodule

/****************************************************************/
// Double Select RPS
/****************************************************************/

module rps_4(req, en, count, gnt);
	input [3:0] req;
	input [1:0] count;
	input [1:0] en;
	output logic [1:0] [3:0] gnt;

	logic [3:0] req_tmp;
	logic [3:0] gnt_tmp_1, gnt_tmp_2;
	logic [1:0] [3:0] gnt_tmp;
	logic NULL;
	logic NULL_2;
	
	always_comb begin
		if (count == 2'b00) req_tmp = req;
		else if (count == 2'b01) req_tmp = {req[0], req[3:1]};
		else if (count == 2'b10) req_tmp = {req[1:0], req[3:2]};
		else req_tmp = {req[2:0], req[3]};
	end

	ps4 ps4_1 (req_tmp, 1'b1, gnt_tmp_1, NULL);
	ps4 ps4_2 (req_tmp ^ gnt_tmp_1, 1'b1, gnt_tmp_2, NULL_2);	

	always_comb begin
		case (en)
			2'b00: begin gnt_tmp[0] = 4'b0; 	gnt_tmp[1] = 4'b0;      end
			2'b01: begin gnt_tmp[0] = gnt_tmp_1; 	gnt_tmp[1] = 4'b0;      end
			2'b10: begin gnt_tmp[0] = 4'b0; 	gnt_tmp[1] = gnt_tmp_1; end
			2'b11: begin gnt_tmp[0] = gnt_tmp_1; 	gnt_tmp[1] = gnt_tmp_2; end
		endcase
		
	end

	always_comb begin
		if (count == 2'b00) gnt[0] = gnt_tmp[0];
		else if (count == 2'b01) gnt[0] = {gnt_tmp[0][2:0], gnt_tmp[0][3]};
		else if (count == 2'b10) gnt[0] = {gnt_tmp[0][1:0], gnt_tmp[0][3:2]};
		else gnt[0] = {gnt_tmp[0][0], gnt_tmp[0][3:1]};
	end

	always_comb begin
		if (count == 2'b00) gnt[1] = gnt_tmp[1];
		else if (count == 2'b01) gnt[1] = {gnt_tmp[1][2:0], gnt_tmp[1][3]};
		else if (count == 2'b10) gnt[1] = {gnt_tmp[1][1:0], gnt_tmp[1][3:2]};
		else gnt[1] = {gnt_tmp[1][0], gnt_tmp[1][3:1]};
	end

endmodule

/****************************************************************/

module rps_8(req, en, count, gnt);

	input [7:0] req;
	input [2:0] count;
	input [1:0] en;
	output logic [1:0] [7:0] gnt;

	logic [7:0] req_tmp;
	logic [7:0] gnt_tmp_1, gnt_tmp_2;
	logic [1:0] [7:0] gnt_tmp;
	logic NULL;
	logic NULL_2;
	
	always_comb begin
		if (count == 3'b000) req_tmp = req;
		else if (count == 3'b001) req_tmp = {req[0], req[7:1]};
		else if (count == 3'b010) req_tmp = {req[1:0], req[7:2]};
		else if (count == 3'b011) req_tmp = {req[2:0], req[7:3]};
		else if (count == 3'b100) req_tmp = {req[3:0], req[7:4]};
		else if (count == 3'b101) req_tmp = {req[4:0], req[7:5]};
		else if (count == 3'b110) req_tmp = {req[5:0], req[7:6]};
		else req_tmp = {req[6:0], req[7]};
	end

	ps8 ps8_1 (req_tmp, 1'b1, gnt_tmp_1, NULL);
	ps8 ps8_2 (req_tmp ^ gnt_tmp_1, 1'b1, gnt_tmp_2, NULL_2);	

	always_comb begin
		case (en)
			2'b00: begin gnt_tmp[0] = 8'b0; 	gnt_tmp[1] = 8'b0;      end
			2'b01: begin gnt_tmp[0] = gnt_tmp_1; 	gnt_tmp[1] = 8'b0;      end
			2'b10: begin gnt_tmp[0] = 8'b0; 	gnt_tmp[1] = gnt_tmp_1; end
			2'b11: begin gnt_tmp[0] = gnt_tmp_1; 	gnt_tmp[1] = gnt_tmp_2; end
		endcase
		
	end

	always_comb begin
		if (count == 3'b000) gnt[0] = gnt_tmp[0];
		else if (count == 3'b001) gnt[0] = {gnt_tmp[0][6:0], gnt_tmp[0][7]};
		else if (count == 3'b010) gnt[0] = {gnt_tmp[0][5:0], gnt_tmp[0][7:6]};
		else if (count == 3'b011) gnt[0] = {gnt_tmp[0][4:0], gnt_tmp[0][7:5]};
		else if (count == 3'b100) gnt[0] = {gnt_tmp[0][3:0], gnt_tmp[0][7:4]};
		else if (count == 3'b101) gnt[0] = {gnt_tmp[0][2:0], gnt_tmp[0][7:3]};
		else if (count == 3'b110) gnt[0] = {gnt_tmp[0][1:0], gnt_tmp[0][7:2]};
		else gnt[0] = {gnt_tmp[0], gnt_tmp[0][7:1]};		
	end

	always_comb begin
		if (count == 3'b000) gnt[1] = gnt_tmp[1];
		else if (count == 3'b001) gnt[1] = {gnt_tmp[1][6:0], gnt_tmp[1][7]};
		else if (count == 3'b010) gnt[1] = {gnt_tmp[1][5:0], gnt_tmp[1][7:6]};
		else if (count == 3'b011) gnt[1] = {gnt_tmp[1][4:0], gnt_tmp[1][7:5]};
		else if (count == 3'b100) gnt[1] = {gnt_tmp[1][3:0], gnt_tmp[1][7:4]};
		else if (count == 3'b101) gnt[1] = {gnt_tmp[1][2:0], gnt_tmp[1][7:3]};
		else if (count == 3'b110) gnt[1] = {gnt_tmp[1][1:0], gnt_tmp[1][7:2]};
		else gnt[1] = {gnt_tmp[1][0], gnt_tmp[1][7:1]};		
	end
	
endmodule

/****************************************************************/

module rps_16(req, en, count, gnt);

	input [15:0] req;
	input [3:0] count;
	input [1:0] en;
	output logic [1:0] [15:0] gnt;

	logic [15:0] req_tmp;
	logic [15:0] gnt_tmp_1, gnt_tmp_2;
	logic [1:0] [15:0] gnt_tmp;
	logic NULL;
	logic NULL_2;
	
	always_comb begin
		case (count)
			4'b0000: req_tmp = req;
			4'b0001: req_tmp = {req[0], req[15:1]};
			4'b0010: req_tmp = {req[1:0], req[15:2]};
			4'b0011: req_tmp = {req[2:0], req[15:3]};
			4'b0100: req_tmp = {req[3:0], req[15:4]};
			4'b0101: req_tmp = {req[4:0], req[15:5]};
			4'b0110: req_tmp = {req[5:0], req[15:6]};
			4'b0111: req_tmp = {req[6:0], req[15:7]};
			4'b1000: req_tmp = {req[7:0], req[15:8]};
			4'b1001: req_tmp = {req[8:0], req[15:9]};
			4'b1010: req_tmp = {req[9:0], req[15:10]};
			4'b1011: req_tmp = {req[10:0], req[15:11]};
			4'b1100: req_tmp = {req[11:0], req[15:12]};
			4'b1101: req_tmp = {req[12:0], req[15:13]};
			4'b1110: req_tmp = {req[13:0], req[15:14]};
			4'b1111: req_tmp = {req[14:0], req[15]};
			default: req_tmp = req;
		endcase
	end

	ps16 ps16_1 (req_tmp, 1'b1, gnt_tmp_1, NULL);
	ps16 ps16_2 (req_tmp ^ gnt_tmp_1, 1'b1, gnt_tmp_2, NULL_2);

	always_comb begin
		case (en)
			2'b00: begin gnt_tmp[0] = 8'b0; 	gnt_tmp[1] = 8'b0;      end
			2'b01: begin gnt_tmp[0] = gnt_tmp_1; 	gnt_tmp[1] = 8'b0;      end
			2'b10: begin gnt_tmp[0] = 8'b0; 	gnt_tmp[1] = gnt_tmp_1; end 
			2'b11: begin gnt_tmp[0] = gnt_tmp_1; 	gnt_tmp[1] = gnt_tmp_2; end
		endcase
		
	end

	always_comb begin
		case (count)
			4'b0000: gnt[0] = gnt_tmp[0];
			4'b0001: gnt[0] = {gnt_tmp[0][14:0], gnt_tmp[0][15]};
			4'b0010: gnt[0] = {gnt_tmp[0][13:0], gnt_tmp[0][15:14]};
			4'b0011: gnt[0] = {gnt_tmp[0][12:0], gnt_tmp[0][15:13]};
			4'b0100: gnt[0] = {gnt_tmp[0][11:0], gnt_tmp[0][15:12]};
			4'b0101: gnt[0] = {gnt_tmp[0][10:0], gnt_tmp[0][15:11]};
			4'b0110: gnt[0] = {gnt_tmp[0][9:0], gnt_tmp[0][15:10]};
			4'b0111: gnt[0] = {gnt_tmp[0][8:0], gnt_tmp[0][15:9]};
			4'b1000: gnt[0] = {gnt_tmp[0][7:0], gnt_tmp[0][15:8]};
			4'b1001: gnt[0] = {gnt_tmp[0][6:0], gnt_tmp[0][15:7]};
			4'b1010: gnt[0] = {gnt_tmp[0][5:0], gnt_tmp[0][15:6]};
			4'b1011: gnt[0] = {gnt_tmp[0][4:0], gnt_tmp[0][15:5]};
			4'b1100: gnt[0] = {gnt_tmp[0][3:0], gnt_tmp[0][15:4]};
			4'b1101: gnt[0] = {gnt_tmp[0][2:0], gnt_tmp[0][15:3]};
			4'b1110: gnt[0] = {gnt_tmp[0][1:0], gnt_tmp[0][15:2]};
			4'b1111: gnt[0] = {gnt_tmp[0][0], gnt_tmp[0][15:1]};
			default: gnt[0] = gnt_tmp[0];
		endcase
	end

	always_comb begin
		case (count)
			4'b0000: gnt[1] = gnt_tmp[1];
			4'b0001: gnt[1] = {gnt_tmp[1][14:0], gnt_tmp[1][15]};
			4'b0010: gnt[1] = {gnt_tmp[1][13:0], gnt_tmp[1][15:14]};
			4'b0011: gnt[1] = {gnt_tmp[1][12:0], gnt_tmp[1][15:13]};
			4'b0100: gnt[1] = {gnt_tmp[1][11:0], gnt_tmp[1][15:12]};
			4'b0101: gnt[1] = {gnt_tmp[1][10:0], gnt_tmp[1][15:11]};
			4'b0110: gnt[1] = {gnt_tmp[1][9:0], gnt_tmp[1][15:10]};
			4'b0111: gnt[1] = {gnt_tmp[1][8:0], gnt_tmp[1][15:9]};
			4'b1000: gnt[1] = {gnt_tmp[1][7:0], gnt_tmp[1][15:8]};
			4'b1001: gnt[1] = {gnt_tmp[1][6:0], gnt_tmp[1][15:7]};
			4'b1010: gnt[1] = {gnt_tmp[1][5:0], gnt_tmp[1][15:6]};
			4'b1011: gnt[1] = {gnt_tmp[1][4:0], gnt_tmp[1][15:5]};
			4'b1100: gnt[1] = {gnt_tmp[1][3:0], gnt_tmp[1][15:4]};
			4'b1101: gnt[1] = {gnt_tmp[1][2:0], gnt_tmp[1][15:3]};
			4'b1110: gnt[1] = {gnt_tmp[1][1:0], gnt_tmp[1][15:2]};
			4'b1111: gnt[1] = {gnt_tmp[1][0], gnt_tmp[1][15:1]};
			default: gnt[1] = gnt_tmp[1];
		endcase
	end

endmodule


