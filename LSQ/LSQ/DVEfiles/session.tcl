# Begin_DVE_Session_Save_Info
# DVE full session
# Saved on Sun Nov 24 18:31:13 2013
# Designs open: 1
#   Sim: /afs/umich.edu/user/j/i/jiwang/repos/eecs470g12/LSQ/LSQ/dve
# Toplevel windows open: 1
# 	TopLevel.1
#   Source.1: test
#   Group count = 2
#   Group sq1 signal count = 117
#   Group Group1 signal count = 0
# End_DVE_Session_Save_Info

# DVE version: E-2011.03_Full64
# DVE build date: Feb 23 2011 21:10:05


#<Session mode="Full" path="/afs/umich.edu/user/j/i/jiwang/repos/eecs470g12/LSQ/LSQ/DVEfiles/session.tcl" type="Debug">

gui_set_loading_session_type Post
gui_continuetime_set

# Close design
if { [gui_sim_state -check active] } {
    gui_sim_terminate
}
gui_close_db -all
gui_expr_clear_all

# Close all windows
gui_close_window -type Console
gui_close_window -type Wave
gui_close_window -type Source
gui_close_window -type Schematic
gui_close_window -type Data
gui_close_window -type DriverLoad
gui_close_window -type List
gui_close_window -type Memory
gui_close_window -type HSPane
gui_close_window -type DLPane
gui_close_window -type Assertion
gui_close_window -type CovHier
gui_close_window -type CoverageTable
gui_close_window -type CoverageMap
gui_close_window -type CovDensity
gui_close_window -type CovDetail
gui_close_window -type Local
gui_close_window -type Stack
gui_close_window -type Watch
gui_close_window -type Grading
gui_close_window -type Group
gui_close_window -type Transaction



# Application preferences
gui_set_pref_value -key app_default_font -value {Helvetica,10,-1,5,50,0,0,0,0,0}
gui_src_preferences -tabstop 8 -maxbits 24 -windownumber 1
#<WindowLayout>

# DVE Topleve session: 


# Create and position top-level windows :TopLevel.1

if {![gui_exist_window -window TopLevel.1]} {
    set TopLevel.1 [ gui_create_window -type TopLevel \
       -icon $::env(DVE)/auxx/gui/images/toolbars/dvewin.xpm] 
} else { 
    set TopLevel.1 TopLevel.1
}
gui_show_window -window ${TopLevel.1} -show_state normal -rect {{102 118} {2556 1399}}

# ToolBar settings
gui_set_toolbar_attributes -toolbar {TimeOperations} -dock_state top
gui_set_toolbar_attributes -toolbar {TimeOperations} -offset 0
gui_show_toolbar -toolbar {TimeOperations}
gui_set_toolbar_attributes -toolbar {&File} -dock_state top
gui_set_toolbar_attributes -toolbar {&File} -offset 0
gui_show_toolbar -toolbar {&File}
gui_set_toolbar_attributes -toolbar {&Edit} -dock_state top
gui_set_toolbar_attributes -toolbar {&Edit} -offset 0
gui_show_toolbar -toolbar {&Edit}
gui_set_toolbar_attributes -toolbar {Simulator} -dock_state top
gui_set_toolbar_attributes -toolbar {Simulator} -offset 0
gui_show_toolbar -toolbar {Simulator}
gui_set_toolbar_attributes -toolbar {Interactive Rewind} -dock_state top
gui_set_toolbar_attributes -toolbar {Interactive Rewind} -offset 0
gui_show_toolbar -toolbar {Interactive Rewind}
gui_set_toolbar_attributes -toolbar {Signal} -dock_state top
gui_set_toolbar_attributes -toolbar {Signal} -offset 0
gui_show_toolbar -toolbar {Signal}
gui_set_toolbar_attributes -toolbar {&Scope} -dock_state top
gui_set_toolbar_attributes -toolbar {&Scope} -offset 0
gui_show_toolbar -toolbar {&Scope}
gui_set_toolbar_attributes -toolbar {&Trace} -dock_state top
gui_set_toolbar_attributes -toolbar {&Trace} -offset 0
gui_show_toolbar -toolbar {&Trace}
gui_set_toolbar_attributes -toolbar {BackTrace} -dock_state top
gui_set_toolbar_attributes -toolbar {BackTrace} -offset 0
gui_show_toolbar -toolbar {BackTrace}
gui_set_toolbar_attributes -toolbar {&Window} -dock_state top
gui_set_toolbar_attributes -toolbar {&Window} -offset 0
gui_show_toolbar -toolbar {&Window}
gui_set_toolbar_attributes -toolbar {Zoom} -dock_state top
gui_set_toolbar_attributes -toolbar {Zoom} -offset 0
gui_show_toolbar -toolbar {Zoom}
gui_set_toolbar_attributes -toolbar {Zoom And Pan History} -dock_state top
gui_set_toolbar_attributes -toolbar {Zoom And Pan History} -offset 0
gui_show_toolbar -toolbar {Zoom And Pan History}
gui_set_toolbar_attributes -toolbar {Grid} -dock_state top
gui_set_toolbar_attributes -toolbar {Grid} -offset 0
gui_show_toolbar -toolbar {Grid}

# End ToolBar settings

# Docked window settings
set HSPane.1 [gui_create_window -type HSPane -parent ${TopLevel.1} -dock_state left -dock_on_new_line true -dock_extent 142]
set Hier.1 [gui_share_window -id ${HSPane.1} -type Hier]
gui_set_window_pref_key -window ${HSPane.1} -key dock_width -value_type integer -value 142
gui_set_window_pref_key -window ${HSPane.1} -key dock_height -value_type integer -value -1
gui_set_window_pref_key -window ${HSPane.1} -key dock_offset -value_type integer -value 0
gui_update_layout -id ${HSPane.1} {{left 0} {top 0} {width 141} {height 1018} {dock_state left} {dock_on_new_line true} {child_hier_colhier 140} {child_hier_coltype 100} {child_hier_col1 0} {child_hier_col2 1}}
set DLPane.1 [gui_create_window -type DLPane -parent ${TopLevel.1} -dock_state left -dock_on_new_line true -dock_extent 220]
set Data.1 [gui_share_window -id ${DLPane.1} -type Data]
gui_set_window_pref_key -window ${DLPane.1} -key dock_width -value_type integer -value 220
gui_set_window_pref_key -window ${DLPane.1} -key dock_height -value_type integer -value 1018
gui_set_window_pref_key -window ${DLPane.1} -key dock_offset -value_type integer -value 0
gui_update_layout -id ${DLPane.1} {{left 0} {top 0} {width 219} {height 1018} {dock_state left} {dock_on_new_line true} {child_data_colvariable 178} {child_data_colvalue 100} {child_data_coltype 40} {child_data_col1 0} {child_data_col2 1} {child_data_col3 2}}
set Console.1 [gui_create_window -type Console -parent ${TopLevel.1} -dock_state bottom -dock_on_new_line true -dock_extent 175]
gui_set_window_pref_key -window ${Console.1} -key dock_width -value_type integer -value 2457
gui_set_window_pref_key -window ${Console.1} -key dock_height -value_type integer -value 175
gui_set_window_pref_key -window ${Console.1} -key dock_offset -value_type integer -value 0
gui_update_layout -id ${Console.1} {{left 0} {top 0} {width 2454} {height 174} {dock_state bottom} {dock_on_new_line true}}
#### Start - Readjusting docked view's offset / size
set dockAreaList { top left right bottom }
foreach dockArea $dockAreaList {
  set viewList [gui_ekki_get_window_ids -active_parent -dock_area $dockArea]
  foreach view $viewList {
      if {[lsearch -exact [gui_get_window_pref_keys -window $view] dock_width] != -1} {
        set dockWidth [gui_get_window_pref_value -window $view -key dock_width]
        set dockHeight [gui_get_window_pref_value -window $view -key dock_height]
        set offset [gui_get_window_pref_value -window $view -key dock_offset]
        if { [string equal "top" $dockArea] || [string equal "bottom" $dockArea]} {
          gui_set_window_attributes -window $view -dock_offset $offset -width $dockWidth
        } else {
          gui_set_window_attributes -window $view -dock_offset $offset -height $dockHeight
        }
      }
  }
}
#### End - Readjusting docked view's offset / size
gui_sync_global -id ${TopLevel.1} -option true

# MDI window settings
set Source.1 [gui_create_window -type {Source}  -parent ${TopLevel.1}]
gui_show_window -window ${Source.1} -show_state maximized
gui_update_layout -id ${Source.1} {{show_state maximized} {dock_state undocked} {dock_on_new_line false}}

# End MDI window settings

gui_set_env TOPLEVELS::TARGET_FRAME(Source) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(Schematic) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(PathSchematic) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(Wave) none
gui_set_env TOPLEVELS::TARGET_FRAME(List) none
gui_set_env TOPLEVELS::TARGET_FRAME(Memory) ${TopLevel.1}
gui_set_env TOPLEVELS::TARGET_FRAME(DriverLoad) none
gui_update_statusbar_target_frame ${TopLevel.1}

#</WindowLayout>

#<Database>

# DVE Open design session: 

if { [llength [lindex [gui_get_db -design Sim] 0]] == 0 } {
gui_set_env SIMSETUP::SIMARGS {{-ucligui  +vc +memcbk}}
gui_set_env SIMSETUP::SIMEXE {/afs/umich.edu/user/j/i/jiwang/repos/eecs470g12/LSQ/LSQ/dve}
gui_set_env SIMSETUP::ALLOW_POLL {0}
if { ![gui_is_db_opened -db {/afs/umich.edu/user/j/i/jiwang/repos/eecs470g12/LSQ/LSQ/dve}] } {
gui_sim_run Ucli -exe dve -args {-ucligui  +vc +memcbk} -dir /afs/umich.edu/user/j/i/jiwang/repos/eecs470g12/LSQ/LSQ -nosource
}
}
if { ![gui_sim_state -check active] } {error "Simulator did not start correctly" error}
gui_set_precision 1s
gui_set_time_units 1s
#</Database>

# DVE Global setting session: 


# Global: Breakpoints

# Global: Bus

# Global: Expressions

# Global: Signal Time Shift

# Global: Signal Compare

# Global: Signal Groups

set sq1 sq1
gui_sg_create ${sq1}
gui_sg_addsignal -group ${sq1} { test.lsq_test.lq1.ALU_LSQ_BRAT test.lsq_test.lq1.ALU_LSQ_IR test.lsq_test.lq1.ALU_LSQ_MEM_addr test.lsq_test.sq1.ALU_LSQ_MEM_addr test.lsq_test.lq1.ALU_LSQ_NPC test.lsq_test.lq1.ALU_LSQ_ROB test.lsq_test.sq1.ALU_LSQ_SQ_addr test.lsq_test.lq1.ALU_LSQ_ld_or_st test.lsq_test.sq1.ALU_LSQ_ld_or_st test.lsq_test.lq1.ALU_LSQ_regA test.lsq_test.lq1.ALU_LSQ_valid test.lsq_test.sq1.BRAT test.lsq_test.lq1.BRAT_CMT_num test.lsq_test.sq1.BRAT_CMT_num test.lsq_test.lq1.BRAT_CMT_v test.lsq_test.sq1.BRAT_CMT_v test.lsq_test.sq1.Bsq_tail test.lsq_test.sq1.IR test.lsq_test.lq1.LQ_Dcache_BRAT test.lsq_test.lq1.LQ_Dcache_BRAT_tmp test.lsq_test.lq1.LQ_Dcache_IR test.lsq_test.lq1.LQ_Dcache_IR_tmp test.lsq_test.lq1.LQ_Dcache_MEM_addr test.lsq_test.lq1.LQ_Dcache_MEM_addr_tmp test.lsq_test.lq1.LQ_Dcache_NPC test.lsq_test.lq1.LQ_Dcache_NPC_tmp test.lsq_test.lq1.LQ_Dcache_ROB test.lsq_test.lq1.LQ_Dcache_ROB_tmp test.lsq_test.lq1.LQ_Dcache_regA test.lsq_test.lq1.LQ_Dcache_regA_tmp test.lsq_test.lq1.LQ_Dcache_valid test.lsq_test.lq1.LQ_Dcache_valid_tmp test.lsq_test.lq1.LQ_PRF_BRAT test.lsq_test.lq1.LQ_PRF_BRAT_tmp test.lsq_test.lq1.LQ_PRF_IR test.lsq_test.lq1.LQ_PRF_IR_tmp test.lsq_test.lq1.LQ_PRF_NPC test.lsq_test.lq1.LQ_PRF_NPC_tmp test.lsq_test.lq1.LQ_PRF_ROB test.lsq_test.lq1.LQ_PRF_ROB_tmp test.lsq_test.lq1.LQ_PRF_forward_PRF test.lsq_test.lq1.LQ_PRF_forward_PRF_tmp test.lsq_test.lq1.LQ_PRF_regA test.lsq_test.lq1.LQ_PRF_regA_tmp test.lsq_test.lq1.LQ_PRF_valid test.lsq_test.lq1.LQ_PRF_valid_tmp test.lsq_test.lq1.LQ_full test.lsq_test.sq1.LSQ_not_erase_Dcache test.lsq_test.lq1.LSQ_not_erase_Dcache test.lsq_test.lq1.LSQ_not_erase_PRF test.lsq_test.sq1.NPC test.lsq_test.sq1.ROB test.lsq_test.lq1.ROB_head test.lsq_test.sq1.ROB_head_is_store test.lsq_test.lq1.ROB_head_is_store test.lsq_test.sq1.SQ_CMP test.lsq_test.sq1.SQ_Dcache_BRAT test.lsq_test.sq1.SQ_Dcache_IR test.lsq_test.sq1.SQ_Dcache_MEM_addr test.lsq_test.sq1.SQ_Dcache_NPC test.lsq_test.sq1.SQ_Dcache_ROB test.lsq_test.sq1.SQ_Dcache_regB test.lsq_test.sq1.SQ_Dcache_valid test.lsq_test.sq1.SQ_full test.lsq_test.sq1.branch_mis test.lsq_test.lq1.branch_mis test.lsq_test.lq1.branch_mis_brat test.lsq_test.sq1.branch_mis_brat test.lsq_test.sq1.clock test.lsq_test.lq1.forward_PRF test.lsq_test.lq1.go_Dcache test.lsq_test.lq1.go_PRF test.lsq_test.sq1.instr_is_st test.lsq_test.lq1.lq_BRAT test.lsq_test.lq1.lq_IR test.lsq_test.lq1.lq_MEM_addr test.lsq_test.lq1.lq_NPC test.lsq_test.lq1.lq_ROB test.lsq_test.lq1.lq_ROB_true test.lsq_test.lq1.lq_free_valid_Dcache test.lsq_test.lq1.lq_free_valid_PRF test.lsq_test.lq1.lq_in_valid test.lsq_test.lq1.lq_num_of_avail test.lsq_test.lq1.lq_regA test.lsq_test.lq1.lq_valid test.lsq_test.sq1.regB test.lsq_test.lq1.reset test.lsq_test.sq1.sq_BRAT test.ALU_LSQ_SQ_addr test.ALU_LSQ_valid test.ALU_LSQ_ld_or_st test.ALU_LSQ_ld_or_st test.ALU_LSQ_SQ_addr test.ALU_LSQ_SQ_addr test.lsq_test.lq1.clock {test.lsq_test.lq1.sq_MEM_addr[7]} {test.lsq_test.lq1.sq_MEM_addr[6]} {test.lsq_test.lq1.sq_MEM_addr[5]} {test.lsq_test.lq1.sq_MEM_addr[4]} {test.lsq_test.lq1.sq_MEM_addr[3]} {test.lsq_test.lq1.sq_MEM_addr[2]} {test.lsq_test.lq1.sq_MEM_addr[1]} {test.lsq_test.lq1.sq_MEM_addr[0]} {test.ALU_LSQ_SQ_addr[1]} {test.ALU_LSQ_SQ_addr[0]} {test.ALU_LSQ_MEM_addr[1]} {test.ALU_LSQ_MEM_addr[0]} test.ALU_LSQ_ld_or_st test.ALU_LSQ_valid test.lsq_test.sq1.sq_head test.lsq_test.lq1.sq_regA test.lsq_test.lq1.sq_regA_valid test.lsq_test.sq1.sq_regB test.lsq_test.lq1.sq_tail test.lsq_test.sq1.sq_tail test.lsq_test.lq1.sq_valid test.lsq_test.lq1.valid_list }
gui_set_radix -radix {decimal} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[7]}}
gui_set_radix -radix {unsigned} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[7]}}
gui_set_radix -radix {decimal} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[6]}}
gui_set_radix -radix {unsigned} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[6]}}
gui_set_radix -radix {decimal} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[5]}}
gui_set_radix -radix {unsigned} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[5]}}
gui_set_radix -radix {decimal} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[4]}}
gui_set_radix -radix {unsigned} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[4]}}
gui_set_radix -radix {decimal} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[3]}}
gui_set_radix -radix {unsigned} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[3]}}
gui_set_radix -radix {decimal} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[2]}}
gui_set_radix -radix {unsigned} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[2]}}
gui_set_radix -radix {decimal} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[1]}}
gui_set_radix -radix {unsigned} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[1]}}
gui_set_radix -radix {decimal} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[0]}}
gui_set_radix -radix {unsigned} -signals {{Sim:test.lsq_test.lq1.sq_MEM_addr[0]}}
gui_set_radix -radix {decimal} -signals {{Sim:test.ALU_LSQ_MEM_addr[1]}}
gui_set_radix -radix {unsigned} -signals {{Sim:test.ALU_LSQ_MEM_addr[1]}}
gui_set_radix -radix {decimal} -signals {{Sim:test.ALU_LSQ_MEM_addr[0]}}
gui_set_radix -radix {unsigned} -signals {{Sim:test.ALU_LSQ_MEM_addr[0]}}
gui_set_radix -radix {decimal} -signals {Sim:test.lsq_test.sq1.sq_regB}
gui_set_radix -radix {unsigned} -signals {Sim:test.lsq_test.sq1.sq_regB}
set Group1 Group1
gui_sg_create ${Group1}

# Global: Highlighting

# Post database loading setting...

# Restore C1 time
gui_set_time -C1_only 0



# Save global setting...

# Wave/List view global setting
gui_cov_show_value -switch false

# Close all empty TopLevel windows
foreach __top [gui_ekki_get_window_ids -type TopLevel] {
    if { [llength [gui_ekki_get_window_ids -parent $__top]] == 0} {
        gui_close_window -window $__top
    }
}
gui_set_loading_session_type noSession
# DVE View/pane content session: 


# Hier 'Hier.1'
gui_show_window -window ${Hier.1}
gui_list_set_filter -id ${Hier.1} -list { {Package 1} {All 1} {Process 1} {UnnamedProcess 1} {Function 1} {Block 1} {OVA Unit 1} {LeafScCell 1} {LeafVlgCell 1} {Interface 1} {LeafVhdCell 1} {$unit 1} {NamedBlock 1} {Task 1} {VlgPackage 1} {ClassDef 1} }
gui_list_set_filter -id ${Hier.1} -text {*}
gui_hier_list_init -id ${Hier.1}
gui_change_design -id ${Hier.1} -design Sim
catch {gui_list_select -id ${Hier.1} {test}}
gui_view_scroll -id ${Hier.1} -vertical -set 0
gui_view_scroll -id ${Hier.1} -horizontal -set 0

# Data 'Data.1'
gui_list_set_filter -id ${Data.1} -list { {Buffer 1} {Input 1} {Others 1} {Linkage 1} {Output 1} {Parameter 1} {All 1} {Aggregate 1} {Event 1} {Assertion 1} {Constant 1} {Interface 1} {Signal 1} {$unit 1} {Inout 1} {Variable 1} }
gui_list_set_filter -id ${Data.1} -text {*}
gui_list_show_data -id ${Data.1} {test}
gui_show_window -window ${Data.1}
catch { gui_list_select -id ${Data.1} {test.ALU_LSQ_MEM_addr }}
gui_view_scroll -id ${Data.1} -vertical -set 0
gui_view_scroll -id ${Data.1} -horizontal -set 0
gui_view_scroll -id ${Hier.1} -vertical -set 0
gui_view_scroll -id ${Hier.1} -horizontal -set 0

# Source 'Source.1'
gui_src_value_annotate -id ${Source.1} -switch false
gui_set_env TOGGLE::VALUEANNOTATE 0
gui_open_source -id ${Source.1}  -replace -active test /afs/umich.edu/user/j/i/jiwang/repos/eecs470g12/LSQ/LSQ/test.v
gui_src_value_annotate -id ${Source.1} -switch true
gui_set_env TOGGLE::VALUEANNOTATE 1
gui_view_scroll -id ${Source.1} -vertical -set 34
gui_src_set_reusable -id ${Source.1}
# Restore toplevel window zorder
# The toplevel window could be closed if it has no view/pane
if {[gui_exist_window -window ${TopLevel.1}]} {
	gui_set_active_window -window ${TopLevel.1}
	gui_set_active_window -window ${Source.1}
	gui_set_active_window -window ${DLPane.1}
}
#</Session>

