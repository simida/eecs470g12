////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  File Name: pipeline_v3.v                                                  //
//                                                                            //
//  Description: The combination of all modules. A one-way OoO Processor      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------//


module pipeline (

		 ////////////////////
		 //     INPUTS     //
		 ////////////////////

		 input        clock,        // System clock
		 input        reset,        // System reset
		 input        [3:0]  mem2proc_response, // Tag from memory about current request
		 input        [63:0] mem2proc_data,     // Data coming back from memory
		 input        [3:0]  mem2proc_tag,      // Tag from memory about current reply\

		 ////////////////////
		 //     OUTPUTS    //
		 ////////////////////
		 
		 output logic [1:0]  proc2mem_command,  // command sent to memory
		 output logic [63:0] proc2mem_addr,     // Address sent to memory
		 output logic [63:0] proc2mem_data,     // Data sent to memory
		 
		 output logic [3:0]  		pipeline_completed_insts,
		 output logic [3:0]  		pipeline_error_status,
		 output logic [1:0][4:0]  	pipeline_commit_wr_idx,
		 output logic [1:0][63:0] 	pipeline_commit_wr_data,
		 output logic [1:0]	        pipeline_commit_wr_en,
		 output logic [1:0][63:0] 	pipeline_commit_NPC,

output  logic												D1_LSQ_DC_rd_mem,
output  logic												D1_LSQ_DC_rd_memreg,
output  logic												D1_LSQ_DC_wr_mem,
output	logic	 [63:0]								D1_LSQ_DC_addr,
output	logic	 [63:0]								D1_LSQ_DC_NPC,
output	logic	 [31:0]								D1_LSQ_DC_IR,
output	logic	 [`BRAT_SIZE_LOG:0]		D1_LSQ_DC_BRAT_num,
output	logic	 [`BRAT_SIZE_LOG:0]		D1_LSQ_DC_BRAT_numreg,
output	logic	 [`ROB_SIZE_LOG-1:0]	D1_LSQ_DC_ROB_num,
output	logic	 [`PRF_SIZE_LOG-1:0]	D1_LSQ_DC_PRF_num,
//output	logic												D1_DC_Dcache_wr1_enable,
output	logic  [63:0]								D1_LSQ_DC_wr_value,

output	logic	[63:0]			 next_Dcache_LSQ_data,
output	logic	[63:0]			 next_Dcache_LSQ_rd_addr, 
output	logic							 next_Dcache_LSQ_rd_valid	,
output	logic	[`BRAT_SIZE_LOG:0]	next_LSQ_DC_BRAT_num,
output	logic [`BRAT_SIZE_LOG:0]	next_D1_LSQ_DC_BRAT_num,
output	logic											next_D1_LSQ_DC_rd_mem,
output	logic 										next_D1_LSQ_DC_wr_mem,
output	logic	[`ROB_SIZE_LOG-1:0]	  next_LSQ_DC_ROB_num,
output	logic	[`PRF_SIZE_LOG-1:0]	  next_LSQ_DC_PRF_num,

				//prefetch memory
output	logic [`MEM_LATENCY_IN_CYCLES_P1+1:0]	MSHR_regfl_shifter,
output	logic [`MEM_LATENCY_IN_CYCLES_P1+1:0]	next_MSHR_regfl_shifter,

//output  logic	[63:0]				next_DC_Dmem_addr,
//output  logic [1:0]					next_DC_Dmem_command,
//output	logic [63:0]				next_DC_Dmem_data,

output  logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_out_entry,
output  logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_in_entry,

output  logic [`MSHR_SIZE_LOG-1:0]		MSHR_count,
output  logic [`MSHR_SIZE_LOG-1:0]		next_MSHR_count,
output  logic  				Dcache_LSQ_rd_validreg,
output  logic [`BRAT_SIZE_LOG:0] 		Dcache_LSQ_BRAT_numreg,


output	logic  Dmem_out,
output	logic  Dcache_out,
output	logic  LD_BUF_in,
output	logic  LD_BUF_out,
output	logic[63:0]			 next_Dcache_LSQ_rd_NPC, 
output	logic[31:0]			 next_Dcache_LSQ_rd_IR, 


output	logic										     LD_BUF_empty,
   //logic to Dcache Controller
output   logic 				     LSQ_DC_rd_mem,
output   logic 				     LSQ_DC_wr_mem,
output   logic [63:0] 			     LSQ_DC_addr,
output   logic [`PRF_SIZE_LOG-1:0] 		     LSQ_DC_PRF_num,
output   logic [`BRAT_SIZE_LOG:0] 		     LSQ_DC_BRAT_num,
output   logic [`ROB_SIZE_LOG-1:0] 		     LSQ_DC_ROB_num,
output   logic [`PRF_SIZE_LOG-1:0] 		     LSQ_DC_regA,
output   logic [63:0] 			     LSQ_DC_NPC,
output   logic [31:0] 			     LSQ_DC_IR,

output   logic [63:0] 			     Dcache_data,
output   logic 				     Dcache_valid,

output  [`BRAT_SIZE:0]		     	     BRAT_squash_aid,
output   logic [1:0]				     BRAT_CMT_v,
output   logic [1:0] [`BRAT_SIZE_LOG-1:0]	     BRAT_CMT_num
		 
                 );



   //////////////////////////
   // Outputs From FE
   //////////////////////////
   //------To RS & ROB------	
   logic [1:0] [31:0] 		     FE_ID_IR;
   logic [1:0] [63:0] 		     FE_ID_NPC;
   logic [1:0] [`BRAT_SIZE_LOG:0]    FE_ID_BRAT;
   logic [1:0] 			     FE_predict_taken;
   logic [1:0] [63:0]  		     FE_predict_addr;
   
   logic [1:0] [`PRF_SIZE_LOG-1:0]   FE_ID_regA;         // reg A value
   logic [1:0] [`PRF_SIZE_LOG-1:0]   FE_ID_regB;         // reg B value
   logic [1:0] [`PRF_SIZE_LOG-1:0]   FE_ID_dest_reg;     // destination (writeback) register index
   logic [1:0] [`ARF_SIZE_LOG-1:0]   FE_ID_dest_arfreg;     // destination (writeback) register index
   
   //logics from Decoder
   logic [1:0] [1:0] 		     FE_ID_opa_select;    // ALU opa mux select (ALU_OPA_xxx *)
   logic [1:0] [1:0] 		     FE_ID_opb_select;    // ALU opb mux select (ALU_OPB_xxx *)
   // (ZERO_REG if no writeback)
   logic [1:0] [`ALU_FUNC_NUM_LOG-1:0] FE_ID_alu_func;      // ALU function select (ALU_xxx *)
   logic [1:0] 			       FE_ID_rd_mem;        // does inst read memory?
   logic [1:0] 			       FE_ID_wr_mem;        // does inst write memory?
   logic [1:0] 			       FE_ID_cond_branch;   // is inst a conditional branch?
   logic [1:0] 			       FE_ID_uncond_branch; // is inst an unconditional branch 
   // or jump?
   logic [1:0] 			       FE_ID_halt;          // is inst a halt 00 01 10 11
   logic [1:0] 			       FE_ID_illegal;       // is inst an illegal inst 00 01 10 11
   logic [1:0] 			       FE_ID_valid_inst;    // is inst a valid instruction to be  00 01 10 11
   
   logic [`PRF_SIZE-1:0] 	       FE_valid_list;	       
   logic [1:0] [`PRF_IDX-1:0]	       FE_prf_freed;

   // Outputs for LSQ
   logic 			       FE_branch1, FE_branch2;
   

   // Memory interface/arbiter wires
   logic [63:0] 		       proc2Dmem_addr, proc2Imem_addr;
   logic [1:0] 			       proc2Dmem_command, proc2Imem_command;
   logic [3:0] 			       Imem2proc_response, Dmem2proc_response;
   
   //////////////////////////
   // Outputs From ID
   //////////////////////////
   
   //Logics for FU
   //0-ALU 1-MULT 2-BR 3-MEM
   logic [3:0] [1:0] 		                ID_IS_rs_valid;
   logic [3:0] [1:0] [63:0] 	       		ID_IS_NPC;
   logic [3:0] [1:0] [31:0] 	       		ID_IS_IR;
   logic [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	ID_IS_alu_func;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0]     	ID_IS_regA;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0]     	ID_IS_regB;
   logic [3:0] [1:0] [1:0] 		     	ID_IS_opa_sel;
   logic [3:0] [1:0] [1:0] 		     	ID_IS_opb_sel;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0]     	ID_IS_dest;
   logic [3:0] [1:0] [`ROB_SIZE_LOG-1:0]     	ID_IS_ROB;
   logic [3:0] [1:0] [`BRAT_SIZE_LOG:0]     	ID_IS_BRAT;
   logic [1:0] 					ID_IS_uncn_br;
   logic [1:0] 					ID_IS_ld_or_st;    // whether it is a ld or st
   logic [1:0] [`SQ_SIZE_LOG-1:0] 		ID_IS_out_sq_num;
   
   // Logics for Front End
   logic [3:0] [1:0] 				RS_full;
   
   //////////////////////////
   // Outputs From IS
   //////////////////////////

   //Logics for EX
   logic [3:0] [1:0] 				IS_EX_valid;
   logic [3:0] [1:0] [63:0] 			IS_EX_NPC;
   logic [3:0] [1:0] [31:0] 			IS_EX_IR;
   logic [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	IS_EX_alu_func;
   logic [2:0] [1:0] [63:0] 			IS_EX_regA_value;
   logic [3:0] [1:0] [63:0] 			IS_EX_regB_value;
   logic [1:0] [`PRF_SIZE_LOG-1:0] 		IS_EX_ls_regA;
   logic [3:0] [1:0] [1:0] 			IS_EX_opa_sel;
   logic [3:0] [1:0] [1:0] 			IS_EX_opb_sel;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 	IS_EX_dest;
   logic [3:0] [1:0] [`ROB_SIZE_LOG-1:0] 	IS_EX_ROB;
   logic [3:0] [1:0] [`BRAT_SIZE_LOG:0] 	IS_EX_BRAT;
   logic [1:0] 					IS_EX_uncn_br;
   logic [1:0] 					IS_EX_ld_or_st;
   logic [1:0] [`SQ_SIZE_LOG-1:0] 		IS_EX_out_sq_num;
   
   //Outputs for CDB
   logic 					PRF_CDB_valid;
   logic [63:0] 				PRF_CDB_NPC;
   logic [31:0] 				PRF_CDB_IR;
   logic [`ROB_SIZE_LOG-1:0] 			PRF_CDB_ROB;
   logic [`BRAT_SIZE_LOG:0] 			PRF_CDB_BRAT;
   logic [63:0] 				PRF_CDB_value;
   logic [`PRF_SIZE_LOG-1:0] 			PRF_CDB_regA;

   //Outputs for Dcache
   logic [63:0] 				LSQ_DC_wr_value;
						
   //Logics for ID
   logic [3:0] [1:0] 				IS_ID_Not_Erase;
   logic [`PRF_SIZE-1:0] 			IS_ID_valid_list;
   
   //Logics for Debugging
   logic [`PRF_SIZE-1:0][63:0]			PRF_value;
   logic [1:0] 				    	write_conflict;
    
   //////////////////////////
   // Outputs From EX
   //////////////////////////
   
   // Logics for IS
   logic [3:0] [1:0] 			     EX_IS_stall;

   // Outputs for LSQ
   logic [1:0] 				     ALU_LSQ_valid;
   logic [1:0] [63:0] 			     ALU_LSQ_NPC;
   logic [1:0] [31:0] 			     ALU_LSQ_IR;
   logic [1:0] 				     ALU_LSQ_ld_or_st;  // 0 for ld; 1 for st
   logic [1:0] [63:0] 			     ALU_LSQ_MEM_addr;
   logic [1:0] [`PRF_SIZE_LOG-1:0] 	     ALU_LSQ_regA;
   logic [1:0] [`ROB_SIZE_LOG-1:0] 	     ALU_LSQ_ROB;
   logic [1:0] [`BRAT_SIZE_LOG:0] 	     ALU_LSQ_BRAT;
   logic [1:0] [`SQ_SIZE_LOG-1:0] 	     ALU_LSQ_SQ_addr;
   
   // Logics for CMP
   logic [1:0] 				     EX_CMP_valid;
   logic [1:0] [63:0] 			     EX_CMP_NPC;
   logic [1:0] [31:0] 			     EX_CMP_IR;
   logic [1:0] [`PRF_SIZE_LOG-1:0] 	     EX_CMP_dest;
   logic [1:0] [63:0] 			     EX_CMP_result;
   logic [1:0] [`ROB_SIZE_LOG-1:0] 	     EX_CMP_ROB;
   logic [1:0] [`BRAT_SIZE_LOG:0] 	     EX_CMP_BRAT;
   logic [1:0] 				     EX_CMP_branch;
   logic [1:0] [63:0]			     EX_CMP_branch_addr;

   //////////////////////////
   // Outputs From LSQ
   //////////////////////////

   // Logics for PRF_after
   logic 				     LQ_PRF_valid;
   logic [63:0] 			     LQ_PRF_NPC;
   logic [31:0] 			     LQ_PRF_IR;
   logic [`ROB_SIZE_LOG-1:0] 		     LQ_PRF_ROB;
   logic [`BRAT_SIZE_LOG:0] 		     LQ_PRF_BRAT;
   logic [`PRF_SIZE_LOG-1:0] 		     LQ_PRF_forward_PRF;
   logic [`PRF_SIZE_LOG-1:0] 		     LQ_PRF_regA;
   
   // Logics for PRF_pipeline_before
   logic [1:0] 				     LQ_full;
   // Logics to FrontEnd
   logic [1:0] 				     SQ_full;
   logic [`SQ_SIZE_LOG-1:0]		     SQ_tail;
   // Logic to ROB
   logic 				     SQ_CMP;

   //////////////////////////
   // Outputs From Dcache
   //////////////////////////
   //output to LSQ
   logic 				     MSHR_full;//not use head/tail; but just count
   logic				     LQ_not_erase_Dcache;
   logic 				     SQ_not_erase_Dcache;
   logic 				     LD_BUF_full;
   logic [63:0] 			     Dcache_CDB_value;
   logic [63:0] 			     Dcache_LSQ_rd_addr;
   logic 				     Dcache_CDB_valid;
   logic [`ROB_SIZE_LOG-1:0] 		     Dcache_CDB_ROB;
   logic [`PRF_SIZE_LOG-1:0] 		     Dcache_CDB_regA;
   logic [`BRAT_SIZE_LOG:0] 		     Dcache_CDB_BRAT;//just for debug
   logic [63:0] 			     Dcache_CDB_NPC;
   logic [31:0] 			     Dcache_CDB_IR;

   // to Dcache	
   logic [`SET_IDX_SIZE_LOG-1:0] 	     DC_Dcache_wr1_idx, DC_Dcache_wr2_idx;
   logic [`DCACHE_TAG_SIZE_LOG-1:0] 	     DC_Dcache_wr1_tag,  DC_Dcache_wr2_tag;
   logic [63:0] 			     DC_Dcache_wr1_data,  DC_Dcache_wr2_data;
   logic 				     DC_Dcache_wr1_enable, DC_Dcache_wr2_enable;
   logic [`SET_IDX_SIZE_LOG-1:0] 	     DC_Dcache_rd_idx;
   logic [`DCACHE_TAG_SIZE_LOG-1:0] 	     DC_Dcache_rd_tag;
   // to Dmem

   
   //////////////////////////
   // Outputs From CMP/RET
   //////////////////////////
   
   //ROB related output
   logic [`ROB_SIZE_LOG-1:0] 		     ROB_head;
   logic [`ROB_SIZE_LOG-1:0] 		     ROB_tail;
   logic 				     ROB_full;
   logic 				     ROB_full_1left;
   
   //Retire signals
   logic [1:0] 				     RET_issue_store;
   logic [1:0] [`PRF_SIZE_LOG-1:0] 	     RET_prfn;
   logic [1:0] [`ARF_SIZE_LOG-1:0] 	     RET_arfn;
   logic [1:0] [63:0] 	     		     RET_NPC;
   logic [1:0] 				     RET_valid;//retire is valid
   logic [1:0]				     RET_illegal;
   logic [1:0]				     RET_halt;

   //Outputs for early branch resolution
   logic				     BRAT_CMP_miss;
   logic [`BRAT_SIZE-1:0]		     BRAT_squash;    		//to RS, 1 should be squashed
   logic 				     BRAT_empty;    		//to RS, 1 should be squashed
   logic [`BRAT_SIZE_LOG-1:0]		     BRAT_CMP_num;
   logic [63:0]				     BRAT_CMP_targetaddr;

   //Outputs for branch predictors
   logic [1:0]				     BRAT_CMT_takeornot;
   logic [1:0] [63:0]			     BRAT_CMT_pc;
   logic [1:0] [63:0]		             BRAT_CMT_targetaddr;
   logic [`BRAT_SIZE_LOG-1:0]		     BRAT_tail;
   logic 				     BRAT_full;
   logic 				     BRAT_full_1left;



   // Helping logics
   wire	 [1:0]				     RET_free_enable;

   assign BRAT_squash_aid = {1'b0, BRAT_squash};		
   assign RET_free_enable[0] = RET_valid[0] & (RET_prfn[0] != `PRF_ZERO_REG);
   assign RET_free_enable[1] = RET_valid[1] & (RET_prfn[1] != `PRF_ZERO_REG);			

   assign pipeline_completed_insts = RET_valid[0]+RET_valid[1];
   assign pipeline_error_status = (RET_halt) ? `HALTED_ON_HALT :
					(RET_illegal) ? `HALTED_ON_ILLEGAL : `NO_ERROR;


   assign pipeline_commit_wr_idx = RET_arfn;
   assign pipeline_commit_wr_data[0] = PRF_value[RET_prfn[0]];//Issue_Stage.PRF.next_registers[RET_prfn[0]];
   assign pipeline_commit_wr_data[1] = PRF_value[RET_prfn[1]];//Issue_Stage.PRF.next_registers[RET_prfn[1]];
   assign pipeline_commit_wr_en = RET_valid;
   assign pipeline_commit_NPC[0] = RET_NPC[0];//(RET_valid==2'b11) ? Complete_Retire_Stage.npc_regfl[ROB_head-5'h2] : Complete_Retire_Stage.npc_regfl[ROB_head-5'h1];
   assign pipeline_commit_NPC[1] = RET_NPC[1];//Complete_Retire_Stage.npc_regfl[ROB_head-5'h1];


   assign proc2mem_command = MSHR_full ? `BUS_NONE : (proc2Dmem_command==`BUS_NONE) ? proc2Imem_command : proc2Dmem_command;
   assign proc2mem_addr = (proc2Dmem_command==`BUS_NONE) ? proc2Imem_addr : proc2Dmem_addr;
   assign Dmem2proc_response = (proc2Dmem_command==`BUS_NONE) ? 0:  mem2proc_response;
   assign Imem2proc_response = (proc2Dmem_command==`BUS_NONE) ? mem2proc_response : 0;
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                   FRONT END STAGE                      //
   //                                                        //
   ////////////////////////////////////////////////////////////
   

   FrontEnd FrontEnd_0 (
			.clock(clock),
			.reset(reset),
			
			//------From/To Mem------
			.Imem2proc_response(Imem2proc_response),
			.mem2proc_data(mem2proc_data),
			.mem2proc_tag(mem2proc_tag),

			.proc2Imem_command(proc2Imem_command),
			.proc2Imem_addr(proc2Imem_addr),
			
			//------To RS & ROB------	
			.FE_ID_IR(FE_ID_IR),
			.FE_ID_NPC(FE_ID_NPC),
			.BRAT_TAG(FE_ID_BRAT),
			
			.FE_ID_regA(FE_ID_regA),         // reg A value
			.FE_ID_regB(FE_ID_regB),         // reg B value
			.FE_ID_dest_reg(FE_ID_dest_reg),     // destination (writeback) register index
			.FE_ID_dest_arfreg(FE_ID_dest_arfreg),     // destination (writeback) register index

		        .FE_predict_taken(FE_predict_taken),
		        .FE_predict_addr(FE_predict_addr),
			
			//outputs from Decoder
			.FE_ID_opa_select(FE_ID_opa_select),    // ALU opa mux select (ALU_OPA_xxx *)
			.FE_ID_opb_select(FE_ID_opb_select),    // ALU opb mux select (ALU_OPB_xxx *)
   	                // (ZERO_REG if no writeback)
			.FE_ID_alu_func(FE_ID_alu_func),      // ALU function select (ALU_xxx *)
			.FE_ID_rd_mem(FE_ID_rd_mem),        // does inst read memory?
			.FE_ID_wr_mem(FE_ID_wr_mem),        // does inst write memory?
			.FE_ID_cond_branch(FE_ID_cond_branch),   // is inst a conditional branch?
			.FE_ID_uncond_branch(FE_ID_uncond_branch), // is inst an unconditional branch 
			.FE_ID_halt(FE_ID_halt),          // is inst a halt 00 01 10 11
			.FE_ID_illegal(FE_ID_illegal),       // is inst an illegal inst 00 01 10 11
			.FE_ID_valid_inst(FE_ID_valid_inst),    // is inst a valid instruction to be  00 01 10 11
			
			
			//------From ROB------	
			.ROB_full(ROB_full),
			.ROB_full_1left(ROB_full_1left),
			.RET_prfn(RET_prfn),
			.RET_arfn(RET_arfn),
			.RET_valid(RET_free_enable),

			//------To BRAT------	
			.BRAT_empty(BRAT_empty),
			.BRAT_CMP_miss(BRAT_CMP_miss),
			.BRAT_CMP_num(BRAT_CMP_num),
			.BRAT_CMP_targetaddr(BRAT_CMP_targetaddr),
                        .BRAT_CMT_v(BRAT_CMT_v),
		        .BRAT_CMT_takeornot(BRAT_CMT_takeornot),
		        .BRAT_CMT_pc(BRAT_CMT_pc),
		        .BRAT_CMT_targetaddr(BRAT_CMT_targetaddr),
		        .BRAT_tail(BRAT_tail),
		        .BRAT_full(BRAT_full),
		        .BRAT_full_1left(BRAT_full_1left),

			//------From RS------	
			.RS_full(RS_full),

			// Outputs for LSQ
			.branch1(FE_branch1),
			.branch2(FE_branch2),

			//------To valid_list------
			.FE_valid_list(FE_valid_list),
			.FE_prf_freed(FE_prf_freed)
			
			);
    
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                  	   Dispatch Stage		     //
   //                            RS                          //
   //                                                        //
   ////////////////////////////////////////////////////////////

  Dispatch_Stage Dispatch_Stage (
				 //////////
				 // Inputs
				 //////////
				 .reset(reset),
	       			 .clock(clock),
				 
				 .BRAT_CMP_miss(BRAT_CMP_miss),
				 .BRAT_squash(BRAT_squash_aid),
				 .valid_list(IS_ID_valid_list), 
	       
				 //Inputs from FE_RAT
				 .FE_ID_regA(FE_ID_regA),         // reg A value
				 .FE_ID_regB(FE_ID_regB),         // reg B value
				 .FE_ID_dest_reg(FE_ID_dest_reg),     // destination (writeback) register index
				 .FE_ID_BRAT(FE_ID_BRAT),
				 
				 //Inputs from Decoder
				 .FE_ID_NPC(FE_ID_NPC), 
	       			 .FE_ID_IR(FE_ID_IR),
	       			 .FE_ID_opa_select(FE_ID_opa_select),    // ALU opa mux select (ALU_OPA_xxx *)
				 .FE_ID_opb_select(FE_ID_opb_select),    // ALU opb mux select (ALU_OPB_xxx *)
   	                                                                  // (ZERO_REG if no writeback)
				 .FE_ID_alu_func(FE_ID_alu_func),      // ALU function select (ALU_xxx *)
				 .FE_ID_rd_mem(FE_ID_rd_mem),        // does inst read memory?
	       			 .FE_ID_wr_mem(FE_ID_wr_mem),        // does inst write memory?
				 .FE_ID_cond_branch(FE_ID_cond_branch),   // is inst a conditional branch?
				 .FE_ID_uncond_branch(FE_ID_uncond_branch), // is inst an unconditional branch 
	                                                                  // or jump?
				 .FE_ID_halt(FE_ID_halt),          // is inst a halt 00 01 10 11
				 .FE_ID_illegal(FE_ID_illegal),       // is inst an illegal inst 00 01 10 11
				 .FE_ID_valid_inst(FE_ID_valid_inst),    // is inst a valid instruction to be  00 01 11
				                                         //  counted for CPI calculations?
				 
				 //Inputs from PRF
				 .IS_ID_Not_Erase(IS_ID_Not_Erase),
				 
				 //Inputs from ROB
				 .ROB_head(ROB_head),	
				 .ROB_tail(ROB_tail),	
				
				 // Early Branch Resolution
				 .BRAT_CMT_v(BRAT_CMT_v),
 				 .BRAT_CMT_num(BRAT_CMT_num),	

				 //Inputs from SQ
				 .LQ_full(LQ_full),
				 .SQ_full(SQ_full),
				 .SQ_tail(SQ_tail),

				 
				 //////////
				 //Outputs
				 //////////
				 //Outputs for FU
				 //0-ALU 1-MULT 2-BR 3-MEM
				 .ID_IS_rs_valid(ID_IS_rs_valid),
				 .ID_IS_NPC(ID_IS_NPC),
	       			 .ID_IS_IR(ID_IS_IR),
				 .ID_IS_alu_func(ID_IS_alu_func),
				 .ID_IS_regA(ID_IS_regA),
	            		 .ID_IS_regB(ID_IS_regB),
	       			 .ID_IS_opa_sel(ID_IS_opa_sel),
	       			 .ID_IS_opb_sel(ID_IS_opb_sel),
	       			 .ID_IS_dest(ID_IS_dest),
	            		 .ID_IS_ROB(ID_IS_ROB),
	            		 .ID_IS_BRAT(ID_IS_BRAT),
				 .ID_IS_uncn_br(ID_IS_uncn_br),
				 .ID_IS_ld_or_st(ID_IS_ld_or_st),
				 .ID_IS_out_sq_num(ID_IS_out_sq_num),

				 // Outputs for Front End
				 .RS_full(RS_full)


	       
	       );
   


   ////////////////////////////////////////////////////////////
   //                                                        //
   //                         ISSUE Stage                    //
   //                      PRF &  Valid List                 //
   //                                                        //
   ////////////////////////////////////////////////////////////
    Issue_Stage Issue_Stage(
			    ///////////
			    // Inputs
			    ///////////
			    .reset(reset),		// System Reset
			    .clock(clock),		// System Clock
			    .BRAT_squash(BRAT_squash_aid),	// Branch Misprediction Signal

			    
			    //Inputs from FE
			    .FE_valid_list(FE_valid_list),
			    .FE_prf_freed(FE_prf_freed),       // PRF Index of Freeing valid_list entries
			    
			    //Inputs from ID
			    .ID_IS_rs_valid(ID_IS_rs_valid),
			    .ID_IS_NPC(ID_IS_NPC),
			    .ID_IS_IR(ID_IS_IR),
			    .ID_IS_alu_func(ID_IS_alu_func),
			    .ID_IS_regA(ID_IS_regA),
			    .ID_IS_regB(ID_IS_regB),
			    .ID_IS_opa_sel(ID_IS_opa_sel),
			    .ID_IS_opb_sel(ID_IS_opb_sel),
			    .ID_IS_dest(ID_IS_dest),
			    .ID_IS_ROB(ID_IS_ROB),
	            	    .ID_IS_BRAT(ID_IS_BRAT),
		            .ID_IS_uncn_br(ID_IS_uncn_br),
			    .ID_IS_ld_or_st(ID_IS_ld_or_st),
			    .ID_IS_out_sq_num(ID_IS_out_sq_num),

			    //Inputs from LQ
			    .LQ_PRF_valid(LQ_PRF_valid),
		   	    .LQ_PRF_NPC(LQ_PRF_NPC),
		   	    .LQ_PRF_IR(LQ_PRF_IR),
			    .LQ_PRF_ROB(LQ_PRF_ROB),
			    .LQ_PRF_BRAT(LQ_PRF_BRAT),
			    .LQ_PRF_forward_PRF(LQ_PRF_forward_PRF),
			    .LQ_PRF_regA(LQ_PRF_regA),

			    //Inputs from SQ
			    .LSQ_DC_regA(LSQ_DC_regA),
			    
			    //Inputs from EX
			    .EX_IS_stall(EX_IS_stall),	// Whether the Execution Stage has a stall.
		   	                                // [3:0] for ALU, MULT, BR, MEM
		   	                                // [1:0] for 00, 01, 10, 11
			    
                    	    //Inputs from CMP(Complete Stage)
			    .CMP_wr_idx(EX_CMP_dest),     // The Index of writeback value
			    .CMP_wr_data(EX_CMP_result),	// The Data of writeback value
			    .CMP_wr_en(EX_CMP_valid),	// Write Enable of Execution(Complete) Stage


			    //Inputs from Retire Stage
			    .RET_valid(RET_valid),      // Write Enable of Freeing valid_list entries
			    .branch_mis(BRAT_CMP_miss),

		  	    // Early Branch Resolution
		            .BRAT_CMT_v(BRAT_CMT_v),
 			    .BRAT_CMT_num(BRAT_CMT_num),
			    
			    ///////////
			    // Outputs
			    ///////////
			    
			    //Outputs for EX
			    .IS_EX_valid(IS_EX_valid),
			    .IS_EX_NPC(IS_EX_NPC),
			    .IS_EX_IR(IS_EX_IR),
			    .IS_EX_alu_func(IS_EX_alu_func),
			    .IS_EX_regA_value(IS_EX_regA_value),
			    .IS_EX_regB_value(IS_EX_regB_value),
			    .IS_EX_ls_regA(IS_EX_ls_regA),
			    .IS_EX_opa_sel(IS_EX_opa_sel),
			    .IS_EX_opb_sel(IS_EX_opb_sel),
			    .IS_EX_dest(IS_EX_dest),
			    .IS_EX_ROB(IS_EX_ROB),
			    .IS_EX_BRAT(IS_EX_BRAT),
		            .IS_EX_uncn_br(IS_EX_uncn_br),
			    .IS_EX_ld_or_st(IS_EX_ld_or_st),
			    .IS_EX_out_sq_num(IS_EX_out_sq_num),

			    //Outputs for CDB
		   	    .PRF_CDB_valid(PRF_CDB_valid),
			    .PRF_CDB_NPC(PRF_CDB_NPC),
			    .PRF_CDB_IR(PRF_CDB_IR),
			    .PRF_CDB_ROB(PRF_CDB_ROB),
			    .PRF_CDB_BRAT(PRF_CDB_BRAT),
			    .PRF_CDB_value(PRF_CDB_value),
			    .PRF_CDB_regA(PRF_CDB_regA),

			    //Outputs for Dcache
			    .LSQ_DC_wr_value(LSQ_DC_wr_value),

			    //Outputs for ID
			    .IS_ID_Not_Erase(IS_ID_Not_Erase),
			    .IS_ID_valid_list(IS_ID_valid_list),
			    
			    //Outputs for Debugging
			    .PRF_value(PRF_value),
			    .write_conflict(write_conflict)
			    );
  
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                        EXECUTE STAGE                   //
   //                     ALU, MULT, BR, MEM                 //
   //                                                        //
   ////////////////////////////////////////////////////////////
   Execute_Stage Execute_Stage(
			       //////////
			       // Inputs
			       //////////  
			       .clock(clock),
			       .reset(reset),
			       .BRAT_squash(BRAT_squash_aid),
			       .branch_mis(BRAT_CMP_miss),
			       
			       // Inputs from IS		      
			       .IS_EX_valid(IS_EX_valid),
			       .IS_EX_NPC(IS_EX_NPC),
			       .IS_EX_IR(IS_EX_IR),
			       .IS_EX_alu_func(IS_EX_alu_func),
			       .IS_EX_regA_value(IS_EX_regA_value),
			       .IS_EX_regB_value(IS_EX_regB_value),
			       .IS_EX_ls_regA(IS_EX_ls_regA),
			       .IS_EX_opa_sel(IS_EX_opa_sel),
			       .IS_EX_opb_sel(IS_EX_opb_sel),
			       .IS_EX_dest(IS_EX_dest),
			       .IS_EX_ROB(IS_EX_ROB),
			       .IS_EX_BRAT(IS_EX_BRAT),
			       .IS_EX_uncn_br(IS_EX_uncn_br),
			       .IS_EX_ld_or_st(IS_EX_ld_or_st),
			       .IS_EX_out_sq_num(IS_EX_out_sq_num),

			       //Inputs from PRF to CDB
		   	       .PRF_CDB_valid(PRF_CDB_valid),
			       .PRF_CDB_NPC(PRF_CDB_NPC),
			       .PRF_CDB_IR(PRF_CDB_IR),
			       .PRF_CDB_ROB(PRF_CDB_ROB),
			       .PRF_CDB_BRAT(PRF_CDB_BRAT),
			       .PRF_CDB_value(PRF_CDB_value),
			       .PRF_CDB_regA(PRF_CDB_regA),

			       //Inputs from Dcache to CDB
		   	       .Dcache_CDB_valid(Dcache_CDB_valid),
			       .Dcache_CDB_NPC(Dcache_CDB_NPC),
			       .Dcache_CDB_IR(Dcache_CDB_IR),
			       .Dcache_CDB_ROB(Dcache_CDB_ROB),
			       .Dcache_CDB_BRAT(Dcache_CDB_BRAT),
			       .Dcache_CDB_value(Dcache_CDB_value),
			       .Dcache_CDB_regA(Dcache_CDB_regA),
			       
			       // Early Branch Resolution
			       .BRAT_CMT_v(BRAT_CMT_v),
 			       .BRAT_CMT_num(BRAT_CMT_num),
			       
			       ///////////
			       // Outputs
			       ///////////
			       
			       // Outputs for IS
			       .EX_IS_stall(EX_IS_stall),

			       // Outputs for LSQ
			       .ALU_LSQ_valid(ALU_LSQ_valid),
			       .ALU_LSQ_NPC(ALU_LSQ_NPC),
			       .ALU_LSQ_IR(ALU_LSQ_IR),
			       .ALU_LSQ_ld_or_st(ALU_LSQ_ld_or_st),  // 0 for ld, 1 for st
			       .ALU_LSQ_MEM_addr(ALU_LSQ_MEM_addr),
			       .ALU_LSQ_regA(ALU_LSQ_regA),
			       .ALU_LSQ_ROB(ALU_LSQ_ROB),
			       .ALU_LSQ_BRAT(ALU_LSQ_BRAT),
			       .ALU_LSQ_SQ_addr(ALU_LSQ_SQ_addr),
			       
			       // Outputs for CMP
			       .EX_CMP_valid(EX_CMP_valid),
			       .EX_CMP_NPC(EX_CMP_NPC),
			       .EX_CMP_IR(EX_CMP_IR),
			       .EX_CMP_dest(EX_CMP_dest),
			       .EX_CMP_result(EX_CMP_result),
			       .EX_CMP_ROB(EX_CMP_ROB),
			       .EX_CMP_BRAT(EX_CMP_BRAT),
			       .EX_CMP_branch(EX_CMP_branch),
			       .EX_CMP_branch_addr(EX_CMP_branch_addr)
			       
			       );

   ////////////////////////////////////////////////////////////
   //                                                        //
   //                             LSQ                        //
   //                                                        //
   ////////////////////////////////////////////////////////////
   
lsq LSQ(
	// Control Signals
	.clock(clock),
	.reset(reset),
	
	.branch_mis(BRAT_CMP_miss),
	.branch_mis_brat(BRAT_squash_aid),
	.BRAT_CMP_num(BRAT_CMP_num),
	
	.valid_list(IS_ID_valid_list),
	.LSQ_not_erase_PRF(1'b0),
	.BRAT_CMT_v(BRAT_CMT_v),
	.BRAT_CMT_num(BRAT_CMT_num),
	
	// Inputs from MEM ALU
	.ALU_LSQ_valid(ALU_LSQ_valid),
	.ALU_LSQ_NPC(ALU_LSQ_NPC),
	.ALU_LSQ_IR(ALU_LSQ_IR),
	.ALU_LSQ_ld_or_st(ALU_LSQ_ld_or_st),  // 0 for ld, 1 for st
	.ALU_LSQ_MEM_addr(ALU_LSQ_MEM_addr),
	.ALU_LSQ_regA(ALU_LSQ_regA),
	.ALU_LSQ_ROB(ALU_LSQ_ROB),
	.ALU_LSQ_BRAT(ALU_LSQ_BRAT),
	.ALU_LSQ_SQ_addr(ALU_LSQ_SQ_addr),
	
	// Inputs from ROB
	.ROB_head(ROB_head),
	.ROB_head_is_store(RET_issue_store[0]),
	// Outputs to ROB
        .SQ_CMP(SQ_CMP),
	
	// From Front End: Dispatch
	.instr_is_ld(FE_ID_valid_inst & FE_ID_rd_mem),
   	.instr_is_st(FE_ID_valid_inst & FE_ID_wr_mem), //make sure it's valid and store
	.regA(FE_ID_regA),
   	.BRAT(FE_ID_BRAT),
   	.NPC(FE_ID_NPC),
   	.IR(FE_ID_IR),

	.ROB_tail(ROB_tail),

	.FE_ID_valid_inst(FE_ID_valid_inst),    // is inst a valid instruction to be  00 01 11
	.FE_ID_wr_mem(FE_ID_wr_mem),        // does inst write memory?
	.branch1(FE_branch1), 
	.branch2(FE_branch2),
	.BRAT_tail(BRAT_tail),
	
	//input to LSQ
	.MSHR_full(MSHR_full),//??? not use head/tail, but just count???
	.LD_BUF_full(LD_BUF_full),
        .LQ_not_erase_Dcache(LQ_not_erase_Dcache),
	.SQ_not_erase_Dcache(SQ_not_erase_Dcache),
	
	//output to Dcache Controller
	.LSQ_DC_rd_mem(LSQ_DC_rd_mem),
	.LSQ_DC_wr_mem(LSQ_DC_wr_mem),
	.LSQ_DC_addr(LSQ_DC_addr),
	.LSQ_DC_PRF_num(LSQ_DC_PRF_num),
	.LSQ_DC_BRAT_num(LSQ_DC_BRAT_num),
	.LSQ_DC_ROB_num(LSQ_DC_ROB_num),
	.LSQ_DC_regA(LSQ_DC_regA),
	.LSQ_DC_NPC(LSQ_DC_NPC),
	.LSQ_DC_IR(LSQ_DC_IR),
	
	
	// Outputs for PRF_after
	.LQ_PRF_valid(LQ_PRF_valid),
	.LQ_PRF_NPC(LQ_PRF_NPC),
	.LQ_PRF_IR(LQ_PRF_IR),
	.LQ_PRF_ROB(LQ_PRF_ROB),
	.LQ_PRF_BRAT(LQ_PRF_BRAT),
	.LQ_PRF_forward_PRF(LQ_PRF_forward_PRF),
	.LQ_PRF_regA(LQ_PRF_regA),
	
	// Outputs for PRF_pipeline_before
	.LQ_full(LQ_full),
	// Outputs to FrontEnd
	.SQ_full(SQ_full),
	.sq_tail(SQ_tail)
	
	);

   ////////////////////////////////////////////////////////////
   //                                                        //
   //                   Dcahce & Dcache Controller           //
   //                                                        //
   ////////////////////////////////////////////////////////////

   dcache Dcache(
		 .clock(clock),
		 .reset(reset),
		 
		 //input from LSQ
		 .LSQ_DC_rd_mem(LSQ_DC_rd_mem),
		 .LSQ_DC_addr(LSQ_DC_addr),
		 .LSQ_DC_wr_mem(LSQ_DC_wr_mem),
		 .LSQ_DC_wr_value(LSQ_DC_wr_value),
		 .LSQ_DC_BRAT_num(LSQ_DC_BRAT_num),
		 .LSQ_DC_ROB_num(LSQ_DC_ROB_num),
		 .LSQ_DC_PRF_num(LSQ_DC_PRF_num),
		 .LSQ_DC_NPC(LSQ_DC_NPC),
		 .LSQ_DC_IR(LSQ_DC_IR),
		 
		 //input from Dcache
		 .Dcache_data(Dcache_data),
		 .Dcache_valid(Dcache_valid),
		 
		 //input from Dmemory
		 .Dmem_DC_data(mem2proc_data),
		 .Dmem_DC_response(Dmem2proc_response),
		 .Dmem_DC_tag(mem2proc_tag),
		
		 //input from ROB
		 .BRAT_squash(BRAT_squash_aid),
		 
		 //input from BRAT_CMT Stage
		 .BRAT_CMT_v(BRAT_CMT_v),
		 .BRAT_CMT_num(BRAT_CMT_num),

		 //input from mem.v
		 .mem2proc_response(mem2proc_response),
		 .mem2proc_tag(mem2proc_tag),
		 
		 //output to LSQ
		 .MSHR_full(MSHR_full),//not use head/tail, but just count
		 .LQ_not_erase_Dcache(LQ_not_erase_Dcache),
		 .SQ_not_erase_Dcache(SQ_not_erase_Dcache),
		 .LD_BUF_full(LD_BUF_full),
		 
		 .Dcache_LSQ_data(Dcache_CDB_value),
		 .Dcache_LSQ_rd_addr(Dcache_LSQ_rd_addr),
		 .Dcache_LSQ_rd_valid(Dcache_CDB_valid),
		 .Dcache_LSQ_ROB_num(Dcache_CDB_ROB),
		 .Dcache_LSQ_PRF_num(Dcache_CDB_regA),
		 .Dcache_LSQ_BRAT_num(Dcache_CDB_BRAT),//just for debug
		 .Dcache_LSQ_rd_NPC(Dcache_CDB_NPC),
		 .Dcache_LSQ_rd_IR(Dcache_CDB_IR),


		 //output to Dcache	
		 .DC_Dcache_wr1_idx(DC_Dcache_wr1_idx), 
		 .DC_Dcache_wr2_idx(DC_Dcache_wr2_idx),
		 .DC_Dcache_wr1_tag(DC_Dcache_wr1_tag), 
		 .DC_Dcache_wr2_tag(DC_Dcache_wr2_tag),
		 .DC_Dcache_wr1_data(DC_Dcache_wr1_data),
		 .DC_Dcache_wr2_data(DC_Dcache_wr2_data),
		 .DC_Dcache_wr1_enable(DC_Dcache_wr1_enable), 
		 .DC_Dcache_wr2_enable(DC_Dcache_wr2_enable),
		 .DC_Dcache_rd_idx(DC_Dcache_rd_idx),
		 .DC_Dcache_rd_tag(DC_Dcache_rd_tag),
		 
		 //output to Dmem
		 .DC_Dmem_addr(proc2Dmem_addr),
		 .DC_Dmem_command(proc2Dmem_command),
		 .DC_Dmem_data(proc2mem_data),

.D1_LSQ_DC_rd_mem(D1_LSQ_DC_rd_mem),
.D1_LSQ_DC_rd_memreg(D1_LSQ_DC_rd_memreg),
.D1_LSQ_DC_wr_mem(D1_LSQ_DC_wr_mem),
.D1_LSQ_DC_addr(D1_LSQ_DC_addr),
.D1_LSQ_DC_NPC(D1_LSQ_DC_NPC),
.D1_LSQ_DC_IR(D1_LSQ_DC_IR),
.D1_LSQ_DC_BRAT_num(D1_LSQ_DC_BRAT_num),
.D1_LSQ_DC_BRAT_numreg(D1_LSQ_DC_BRAT_numreg),
.D1_LSQ_DC_ROB_num(D1_LSQ_DC_ROB_num),
.D1_LSQ_DC_PRF_num(D1_LSQ_DC_PRF_num),
//.D1_DC_Dcache_wr1_enable(D1_DC_Dcache_wr1_enable),
.D1_LSQ_DC_wr_value(D1_LSQ_DC_wr_value),

.next_Dcache_LSQ_data,
.next_Dcache_LSQ_rd_addr, 
.next_Dcache_LSQ_rd_valid	,
.next_LSQ_DC_BRAT_num,
.next_D1_LSQ_DC_BRAT_num,
.next_D1_LSQ_DC_rd_mem,
.next_D1_LSQ_DC_wr_mem,
.next_LSQ_DC_ROB_num,
.next_LSQ_DC_PRF_num,

				//prefetch memory
.MSHR_regfl_shifter,
.next_MSHR_regfl_shifter,

//.next_DC_Dmem_addr,
//.next_DC_Dmem_command,
//.next_DC_Dmem_data,

.LD_BUF_out_entry,
.LD_BUF_in_entry,

.MSHR_count,
.next_MSHR_count,
.Dcache_LSQ_rd_validreg,
.Dcache_LSQ_BRAT_numreg,


.Dmem_out,
.Dcache_out,
.LD_BUF_in,
.LD_BUF_out,
.next_Dcache_LSQ_rd_NPC, 
.next_Dcache_LSQ_rd_IR, 


.LD_BUF_empty

		 
		 );

   
   dcachemem DUT_dcachemem(
			   .clock(clock), 
			   .reset(reset), 
			   .wr1_en(DC_Dcache_wr1_enable),
			   .wr1_idx(DC_Dcache_wr1_idx), 
			   .wr1_tag(DC_Dcache_wr1_tag), 
			   .wr1_data(DC_Dcache_wr1_data), 
			   .wr2_en(DC_Dcache_wr2_enable),
			   .wr2_idx(DC_Dcache_wr2_idx), 
			   .wr2_tag(DC_Dcache_wr2_tag), 
			   .wr2_data(DC_Dcache_wr2_data), 
			   
			   .rd1_idx(DC_Dcache_rd_idx),
			   .rd1_tag(DC_Dcache_rd_tag),
			   
			   .rd_data_t(Dcache_data),
			   .rd_valid_t(Dcache_valid)
				
			   );

   
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                     COMPLETE/RETIRE Stage              //
   //                             ROB                        //
   //                                                        //
   ////////////////////////////////////////////////////////////

//ROB only can complete one instruction.
   Complete_Retire_Stage Complete_Retire_Stage(
			  
					       //////////
					       // Inputs
					       //////////
			  		       .clock(clock),
			  		       .reset(reset),
					       
					       //Inputs from FE
			  		       .FE_ID_dest_prfreg(FE_ID_dest_reg),
					       .FE_ID_dest_arfreg(FE_ID_dest_arfreg),
			  	 	       .ID_valid_inst(FE_ID_valid_inst),//is inst a valid instruction to be 00 01 10 11
					       
					       //Inputs from decoder
					       .ID_branch({FE_branch2,FE_branch1}),//is branch or not (diff)
			  		       .ID_store(FE_ID_wr_mem), //is store or not
					       .FE_ID_NPC(FE_ID_NPC),
		                               .FE_ID_illegal(FE_ID_illegal),	
		                               .FE_ID_halt(FE_ID_halt),	
			  
					       //Inputs from Execution Complete
					       .EX_CMP_branch(EX_CMP_branch),
			  		       .EX_CMP_result(EX_CMP_branch_addr),//ex_targetaddr
			  	 	       .EX_CMP_valid(EX_CMP_valid),
					       .EX_CMP_ROB(EX_CMP_ROB),
					       .SQ_CMP(SQ_CMP),
					       .D1_LSQ_DC_wr_mem(D1_LSQ_DC_wr_mem),
			  
					       //Inputs from branch predictor
			  		       .branch_pred(FE_predict_taken),
			  		       .branch_predaddr(FE_predict_addr),
			  
					       //////////
					       //Outputs
					       /////////
					       //ROB related output
					       .ROB_head(ROB_head),
					       .ROB_tail(ROB_tail),
			  		       .ROB_full(ROB_full),
			  		       .ROB_full_1left(ROB_full_1left),
			  
					       //Retire signals
			  		       .RET_issue_store(RET_issue_store),
					       .RET_prfn(RET_prfn),
					       .RET_arfn(RET_arfn),
					       .RET_NPC(RET_NPC),
			  		       .RET_valid(RET_valid),			
					       .RET_illegal(RET_illegal),					       
					       .RET_halt(RET_halt),									

					       //Outputs for early branch resolution
					       .BRAT_CMP_miss(BRAT_CMP_miss),
					       .BRAT_squash(BRAT_squash),    		//to RS, 1 should be squashed
					       .BRAT_empty(BRAT_empty),
					       .BRAT_CMP_num(BRAT_CMP_num),
					       .BRAT_CMP_targetaddr(BRAT_CMP_targetaddr),

					       //Outputs for branch predictors
					       .BRAT_CMT_v(BRAT_CMT_v),
					       .BRAT_CMT_num(BRAT_CMT_num),
					       .BRAT_CMT_takeornot(BRAT_CMT_takeornot),
					       .BRAT_CMT_pc(BRAT_CMT_pc),
					       .BRAT_CMT_targetaddr(BRAT_CMT_targetaddr),
					       .BRAT_tail(BRAT_tail),
					       .BRAT_full(BRAT_full),
					       .BRAT_full_1left(BRAT_full_1left)
					       );
endmodule
