module testbench();


		logic								 					   clock;
		logic 													 reset;
	
		//Inputs from FE
		logic [1:0] [`PRF_SIZE_LOG-1:0]	 FE_ID_dest_prfreg;
		logic [1:0] [`ARF_SIZE_LOG-1:0]	 FE_ID_dest_arfreg;
		logic	[1:0]							 				 ID_valid_inst;//is inst a valid instruction to be 00 01 10 11
	  
		//Inputs from decoder
		logic [1:0]							 				 ID_branch;//is branch or not (diff)
		logic [1:0] 									 	 ID_store; //is store or not
	  logic [1:0] [63:0] 					  	 FE_ID_NPC;
		
		//Inputs from Execution Complete
		logic	[1:0]					 		 				 EX_CMP_branch;
		logic [1:0] [63:0]				 		 	 EX_CMP_result;//ex_targetaddr
		logic [1:0]  				 		 				 EX_CMP_valid;
		logic [1:0] [`ROB_SIZE_LOG-1:0]  EX_CMP_ROB;
		
		//Inputs from branch predictor
		logic	[1:0]					 		 				 branch_pred;
		logic [1:0] [63:0]   		 				 branch_predaddr;
		
	  //////////
	  //Outputs
	  /////////
		//ROB related output
		wire   [`ROB_SIZE_LOG-1:0]  		 ROB_head;
		wire   [`ROB_SIZE_LOG-1:0]	 		 ROB_tail;
		wire         									   ROB_full;
		wire         								     ROB_full_1left;
		
		//Retire signals
		wire   [1:0]					 						 RET_issue_store;
		wire   [1:0] [`PRF_SIZE_LOG-1:0]   RET_prfn;
		wire   [1:0] [`ARF_SIZE_LOG-1:0]   RET_arfn;
		wire   [1:0]											 RET_branch;
		wire   [1:0] [63:0]								 RET_branch_addr;
		wire   [1:0]                			 RET_branch_miss;
		wire   [1:0] 											 RET_valid;//retire is valid

Complete_Retire_Stage DUT(

		 .clock,
		 .reset,
		 .FE_ID_dest_prfreg,
		 .FE_ID_dest_arfreg,
		 .ID_valid_inst,//is inst a valid instruction to be 00 01 10 11
		 .ID_branch,//is branch or not (diff)
		 .ID_store, //is store or not
		 .FE_ID_NPC,
		 .EX_CMP_branch,
		 .EX_CMP_result,//ex_targetaddr
		 .EX_CMP_valid,
		 .EX_CMP_ROB,
		 .branch_pred,
		 .branch_predaddr,
		 .ROB_head,
		 .ROB_tail,
		 .ROB_full,
		 .ROB_full_1left,
		 .RET_issue_store,
		 .RET_prfn,
		 .RET_arfn,
		 .RET_branch_addr,
	   .RET_branch_miss,
	   .RET_valid//retire is valid
		);



task ALLOCATE1_1;//expected: dispatch one the other cycle
		for(int t1=0;t1<64;t1++) begin
			FE_ID_dest_prfreg[0] = FE_ID_dest_prfreg[0] + 1;
			FE_ID_dest_arfreg[0] = (FE_ID_dest_arfreg[0] + 1)%16;
			ID_valid_inst[0]=~ID_valid_inst[0];
			@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.iocnt,ROB_full);
		end
			@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.iocnt,ROB_full);
endtask

task ALLOCATE1_2;//no dispatch
		for(int t2=0;t2<64;t2++) begin
			FE_ID_dest_prfreg[1] = FE_ID_dest_prfreg[1] + 1;
			FE_ID_dest_arfreg[1] = (FE_ID_dest_arfreg[1] + 1)%16;
			ID_valid_inst[1]=~ID_valid_inst[1];
			@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.iocnt,ROB_full);
		end
			@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.iocnt,ROB_full);
endtask

task ALLOCATE2;//dispatch 2 the other cycle
		for(int t4=0;t4<32;t4++) begin
			FE_ID_dest_prfreg[0] = FE_ID_dest_prfreg[0] + 1;
			FE_ID_dest_prfreg[1] = FE_ID_dest_prfreg[1] + 1;
		
			ID_valid_inst[0]=~ID_valid_inst[0];
			ID_valid_inst[1]=~ID_valid_inst[1];
			@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.iocnt,ROB_full);
		end
			@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.iocnt,ROB_full);
endtask

task COMMIT1_1;
	  	for(int t5=0;t5<100;t5++) begin
				EX_CMP_ROB[0] = $random;
				EX_CMP_valid[0] = 1;
				@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.iocnt,ROB_full);
			end
endtask

task COMMIT2_1;//commit 1 each cycle, 1 stall after 1st commit
			EX_CMP_valid[0] = 1;
				@(negedge clock);
	  	for(int t7=0;t7<32;t7++) begin
				EX_CMP_ROB[0] = t7;
				EX_CMP_ROB[1] = t7;
				EX_CMP_valid[0] = ~EX_CMP_valid[0];
				EX_CMP_valid[1] = ~EX_CMP_valid[1];
				@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.iocnt,ROB_full);
			end
				@(negedge clock);
				@(negedge clock);
endtask

task COMMIT2_2;
				@(negedge clock);
	  	for(int t8=0;t8<32;t8++) begin
				EX_CMP_ROB[0] = t8;
				EX_CMP_ROB[1] = t8+1;
				EX_CMP_valid[0] = ~EX_CMP_valid[0];
				EX_CMP_valid[1] = ~EX_CMP_valid[1];
				@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.iocnt,ROB_full);
			end
				@(negedge clock);
endtask

task COMMIT_ALLOCATE;		
	  ID_valid_inst[0] = 1;
		ID_valid_inst[1] = 1;
		
		@(negedge clock);
		for(int t9=0;t9<100;t9++) begin
				FE_ID_dest_prfreg[0] = FE_ID_dest_prfreg[0] + 1;
				EX_CMP_ROB[0] = t9 % 32;
				EX_CMP_ROB[1] = t9 % 32 + 1;
				EX_CMP_valid[0] = 1;
				EX_CMP_valid[1] = 1;
			@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.iocnt,ROB_full);
		end
endtask

task BRANCH;
			@(negedge clock);
			ID_valid_inst[0] = 1;
			ID_valid_inst[1] = 1;
			for (int t10=0;t10<30;t10++) begin
					ID_branch[0] =  1;//change from EX_CMP_branch to ID_branch
					branch_pred[0] = 0;
					ID_branch[1] = 1;
					branch_pred[1] = 0;
					@(negedge clock);
					$display("bran_v:%b bran_pred_ornot:%b head:%d tail:%d full:%d full1_left:%d",DUT.bran_v_regfl,DUT.bran_pred_ornot_regfl,ROB_head,ROB_tail,ROB_full,ROB_full_1left);
			end
					@(negedge clock);
					$display("result after allocation");
					$display("bran_v:%b bran_pred_ornot:%b head:%d tail:%d full:%d full1_left:%d",DUT.bran_v_regfl,DUT.bran_pred_ornot_regfl,ROB_head,ROB_tail,ROB_full,ROB_full_1left);
			for (int t10=31;t10>=0;t10-=2) begin
					@(negedge clock);
					EX_CMP_valid[0] = 1;
					EX_CMP_valid[1] = 1;
				  EX_CMP_branch[0] = ~EX_CMP_branch[0];
					EX_CMP_branch[1] = ~EX_CMP_branch[1];
					EX_CMP_ROB[0] = t10;
					EX_CMP_ROB[1] = t10-1;
					@(negedge clock);
			  	$display("bran_ornot_regfl:%b ex_fin_regfl:%b retire1:%b retire2:%b bran1_miss:%b bran2_miss:%b head:%d tail:%d full:%d",DUT.bran_ornot_regfl,DUT.ex_fin_regfl,DUT.retire1,DUT.retire2,RET_branch_miss[0],RET_branch_miss[1],ROB_head,ROB_tail,ROB_full);
			end	
			$display("finish EX_CMP_valid[1] execute");
			for (int t10=0;t10<32;t10++) begin
			@(negedge clock);
			  	$display("bran_ornot_regfl:%b ex_fin_regfl:%b retire1:%b retire2:%b bran1_miss:%b bran2_miss:%b head:%d tail:%d full:%d",DUT.bran_ornot_regfl,DUT.ex_fin_regfl,DUT.retire1,DUT.retire2,RET_branch_miss[0],RET_branch_miss[1],ROB_head,ROB_tail,ROB_full);
			end
endtask


task BRANCH_0;
			@(negedge clock);
			ID_valid_inst[0] = 1;
			ID_valid_inst[1] = 1;
			for (int t10=0;t10<32;t10++) begin
					EX_CMP_branch[0] =  1;
					branch_pred[0] = 1;
					branch_predaddr[0] = 64'h101010101010;
					EX_CMP_branch[1] = 1;
					branch_pred[1] = 1;
					branch_predaddr[1] = 64'h101010101010;
					@(negedge clock);
			end
					@(negedge clock);
					$display("result after allocation");
					$display("bran_v:%b bran_pred_ornot:%b head:%d tail:%d",DUT.bran_v_regfl,DUT.bran_pred_ornot_regfl,ROB_head,ROB_tail);
			for (int t10=31;t10>=0;t10--) begin
					EX_CMP_valid[0] = 1;
					EX_CMP_valid[1] = 1;
				  EX_CMP_branch[0] = 1;
					EX_CMP_branch[1] = 1;
					EX_CMP_result[0] = 64'h101010101010;
					EX_CMP_result[1] = 64'h111111111111;
					EX_CMP_ROB[0] = t10;
					EX_CMP_ROB[1] = t10-1;
					@(negedge clock);
			  	$display("bran_ornot_regfl:%b ex_fin_regfl:%b retire1:%b retire2:%b bran1_miss:%b bran2_miss:%b head:%d tail:%d full:%d",DUT.bran_ornot_regfl,DUT.ex_fin_regfl,DUT.retire1,DUT.retire2,RET_branch_miss[0],RET_branch_miss[1],ROB_head,ROB_tail,ROB_full);
			end	
			for (int t10=0;t10<32;t10++) begin
					@(negedge clock);
			  	$display("bran_ornot_regfl:%b ex_fin_regfl:%b retire1:%b retire2:%b bran1_miss:%b bran2_miss:%b head:%d tail:%d full:%d",DUT.bran_ornot_regfl,DUT.ex_fin_regfl,DUT.retire1,DUT.retire2,RET_branch_miss[0],RET_branch_miss[1],ROB_head,ROB_tail,ROB_full);
			end
endtask


task BRANCH_1;
			@(negedge clock);
			ID_valid_inst[0] = 1;
			ID_valid_inst[1] = 1;
			for (int t10=0;t10<32;t10++) begin
					@(negedge clock);
					EX_CMP_branch[0] =  1;
					branch_pred[0] = 0;
					EX_CMP_branch[1] = 1; 
					branch_pred[1] = 1;
					branch_predaddr[1] = 64'h101010101010;
			end
					@(negedge clock);
					$display("result after allocation");
					$display("bran_v:%b bran_pred_ornot:%b head:%d tail:%d",DUT.bran_v_regfl,DUT.bran_pred_ornot_regfl,ROB_head,ROB_tail);
			for (int t10=0;t10<100;t10++) begin
					@(negedge clock);
					EX_CMP_valid[0] = 1;
					EX_CMP_valid[1] = 1;
				  EX_CMP_branch[0] = ~EX_CMP_branch[0];
					EX_CMP_branch[1] = ~EX_CMP_branch[1];
					EX_CMP_result[0] = 64'h101010101010;
					EX_CMP_result[1] = 64'h101010101010;
					EX_CMP_ROB[0] = t10 % 31;
					EX_CMP_ROB[1] = t10 % 31 +1;
					@(negedge clock);
			  	$display("bran_ornot_regfl:%b ex_fin_regfl:%b retire1:%b retire2:%b bran1_miss:%b bran2_miss:%b head:%d tail:%d full:%d",DUT.bran_ornot_regfl,DUT.ex_fin_regfl,DUT.retire1,DUT.retire2,RET_branch_miss[0],RET_branch_miss[1],ROB_head,ROB_tail,ROB_full);
			end	
endtask


//involve 'BRANCH' in
always
begin
	#`VERILOG_CLOCK_HALF_PERIOD;
			clock  = ~clock; 
end

initial
begin
//		$monitor("Time:%4.0f clock:%b head:%d tail:%d full:%b iocnt:%d",$time,clock,head,tail,full,DUT.iocnt	);
		//$monitor("Time:%4.0f clock:%b reset%b bran_pred ex_branornot%b ex_targetaddr%b ex_fin%b ex_fin_robidx prfdest prfdest_v bran_vbran_predaddr bran_pred_ornot head tail prfn ret_branornot full bran_targetaddr bran_miss"	);
	  clock = 0;
		reset = 1;

		branch_pred[0] = 0;
		EX_CMP_branch[0] = 0;
		EX_CMP_result[0] = 0;
		EX_CMP_valid[0] = 0;
		EX_CMP_ROB[0] = 0;
		FE_ID_dest_prfreg[0] = 0;
		FE_ID_dest_arfreg[0] = 0;
		ID_valid_inst[0] = 0;
		EX_CMP_branch[0] = 0;
		ID_store[0] = 0;
		FE_ID_NPC[0] =0;
		branch_predaddr[0] = 0;

		
		branch_pred[1] = 0;
		EX_CMP_branch[1] = 0;
		EX_CMP_result[1] = 0;
		EX_CMP_valid[1] = 0;
		EX_CMP_ROB[1] = 0;
		FE_ID_dest_prfreg[1] = 0;
		FE_ID_dest_arfreg[1] = 0;
		ID_valid_inst[1] = 0;
		EX_CMP_branch[1] = 0;
		ID_store[1] = 0;
		FE_ID_NPC[1] =0;
		branch_predaddr[1] = 0;
		branch_pred[1] = 0;
//reset
		@(negedge clock);
		@(negedge clock);
		@(negedge clock);
		reset = 0;
		@(negedge clock);
	
//Specify tasks
	//	COMMIT_ALLOCATE; hard to see, cuz instr_v from FE
			BRANCH;	
@(negedge clock);
		$finish;
end
endmodule
