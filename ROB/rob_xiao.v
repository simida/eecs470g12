module rob(
		input								 clk,reset,
		input								 bran_pred,
		input								 ex_branornot,
		input [63:0]				 ex_targetaddr,
		input       				 ex_fin,
		input [`ROB_IDX-1:0] ex_fin_robidx,
		input [`PRF_IDX-1:0] prfdest,
		input 							 prfdest_v,
		input 							 bran_v,
		input [63:0]         bran_predaddr,
		input 			         bran_pred_ornot,
		input								 instr_v,
		output logic [`ROB_IDX-1:0]  head,
		output logic [`ROB_IDX-1:0]	 tail,
		output logic [`PRF_IDX-1:0]  prfn,
		output logic 								 ret_branornot,
		output logic       					 full,
		output logic [63:0]					 bran_targetaddr,
		output logic                 bran_miss
		);

wire [`ROB_IDX-1:0] tail_p1;
wire [`ROB_IDX-1:0] head_p1;
wire [`ROB_IDX:0]	cur_size;
wire								next_full;
wire								next_empty;
logic [`ROB_IDX:0] iocnt;

wire retire;

logic [`ROB_IDX-1:0] next_tail;
logic [`ROB_IDX-1:0] next_head;
logic								 next_bran_miss;
logic								 move_tail;
logic								 empty;

wire [`ROB_IDX:0] next_iocnt;

logic [`ROBSIZE-1:0] [`PRF_IDX-1:0] prfdest_regfl;
logic [`ROBSIZE-1:0] 							  bran_v_regfl;
logic [`ROBSIZE-1:0] [63:0]         bran_predaddr_regfl;
logic [`ROBSIZE-1:0] 			          bran_pred_ornot_regfl;
logic [`ROBSIZE-1:0] [63:0]         bran_targetaddr_regfl;
logic [`ROBSIZE-1:0] 				        bran_ornot_regfl;
logic [`ROBSIZE-1:0] 							  ex_fin_regfl;

//ALLOCATE
assign tail_p1 = tail + 1'b1;
assign head_p1 = head + 1'b1;

assign cur_size = (next_tail >= next_head) ? (next_tail - next_head) : (next_tail + `ROBSIZE - next_head); 
//assign next_iocnt = (move_tail) ? 1'b0 : cur_size ;//F
assign next_iocnt = ((instr_v & iocnt == `ROBSIZE-1) | (next_full & !retire)) ? `ROBSIZE : cur_size ;//F
assign next_full = iocnt ==`ROBSIZE;
assign next_empty = next_iocnt == 0;

//RETIRE
assign retire = !empty && ex_fin_regfl[head];


always_comb begin

	//default
	next_head = head;
	next_tail = tail;
  move_tail = 0;

	//default data

	//HEAD
	if(retire && !empty) begin
			next_head = head_p1;
	end
	//branch miss
	if (bran_ornot_regfl[head]) begin
		if(bran_pred_ornot_regfl[head])
			next_bran_miss = bran_targetaddr_regfl[head]==bran_predaddr_regfl[head];
		else 
		  next_bran_miss = 1;
	end
	else begin
			next_bran_miss = bran_pred_ornot_regfl[head] ? 1 : 0;
	end
	
	if(retire && next_bran_miss) begin
		move_tail = 1;
		
	end

	//TAIL
	if(move_tail) begin
			next_tail = next_head;
	end else begin
			if(prfdest_v && !next_full) begin
				next_tail = tail_p1;
			end
  end
end //always_comb

// ===================================================
// Sequential Block
// ===================================================

always_ff @(posedge clk) begin
	if(reset) begin
		head  <= `SD {`ROB_IDX'b0};
		tail  <= `SD {`ROB_IDX'b0};
		iocnt <= `SD {`ROB_IDX+1{1'b0}}; 
		full  <= `SD 1'b0;
		empty <= `SD 1'b1; 
		bran_miss <= `SD 1'b0;

	end//reset
	else begin

		//ALLOCATE
		if(prfdest_v && !full) begin
				prfdest_regfl[tail]		   	 	<= `SD prfdest;
				bran_v_regfl[tail]          <= `SD bran_v;
				bran_predaddr_regfl[tail]	  <= `SD bran_predaddr; 
				bran_pred_ornot_regfl[tail] <= `SD bran_pred_ornot; 
				ex_fin_regfl[tail]          <= `SD 1'b0; 
		end
		
		//EXECUTION COMPLETE
		if(ex_fin) begin
				ex_fin_regfl[ex_fin_robidx]          <= `SD 1'b1; 
				bran_targetaddr_regfl[ex_fin_robidx] <= `SD ex_targetaddr; 
				bran_ornot_regfl[ex_fin_robidx]			 <= `SD ex_branornot;
		end
	
		//RETIRE
		if(retire) begin
			 prfn            <= `SD prfdest_regfl[head];
			 ret_branornot   <= `SD bran_ornot_regfl[head];
			 bran_targetaddr <= `SD bran_targetaddr_regfl[head];
		end
		head  <= `SD next_head;
		tail  <= `SD next_tail;
		iocnt <= `SD next_iocnt; 
		full  <= `SD next_full;
		empty <= `SD next_empty; 
		bran_miss <= `SD next_bran_miss;
	end

end//always_ff





endmodule
