module testbench();
		logic								 					   clock;
		logic 													 reset;
	
		//Inputs from FE
		logic [1:0] [`PRF_SIZE_LOG-1:0]	 FE_ID_dest_prfreg;
		logic [1:0] [`ARF_SIZE_LOG-1:0]	 FE_ID_dest_arfreg;
		logic	[1:0]							 				 ID_valid_inst;//is inst a valid instruction to be 00 01 10 11
	  
		//Inputs from decoder
		logic [1:0]							 				 ID_branch;//is branch or not (diff)
		logic [1:0] 									 	 ID_store; //is store or not
	  logic [1:0] [63:0] 					  	 FE_ID_NPC;
		logic [1:0]											 FE_ID_halt;	
		
		//Inputs from Execution Complete
		logic	[1:0]					 		 				 EX_CMP_branch;
		logic [1:0] [63:0]				 		 	 EX_CMP_result;//ex_targetaddr
		logic [1:0]  				 		 				 EX_CMP_valid;
		logic [1:0] [`ROB_SIZE_LOG-1:0]  EX_CMP_ROB;
		
		//Inputs from branch predictor
		logic	[1:0]					 		 				 branch_pred;
		logic [1:0] [63:0]   		 				 branch_predaddr;
		
	  //////////
	  //Outputs
	  /////////
		//ROB related output
		wire   [`ROB_SIZE_LOG-1:0]  		 ROB_head;
		wire   [`ROB_SIZE_LOG-1:0]	 		 ROB_tail;
		wire         									   ROB_full;
		wire         								     ROB_full_1left;
		
		//Retire signals
		wire   [1:0]					 						 RET_issue_store;
		wire   [1:0] [`PRF_SIZE_LOG-1:0]   RET_prfn;
		wire   [1:0] [`ARF_SIZE_LOG-1:0]   RET_arfn;
		wire   [1:0] 											 RET_valid;//retire is valid
		wire   [1:0]											 RET_halt;

		//Outputs for early branch resolution
		wire													 BRAT_CMP_miss;
		wire [`BRAT_SIZE_LOG-1:0] 		 BRAT_CMP_num;
		wire [63:0]										 BRAT_CMP_targetaddr;

		wire [1:0]										 BRAT_CMT_v;
		wire [1:0]										 BRAT_CMT_takeornot;
		wire [1:0] [63:0]							 BRAT_CMT_pc;
		wire [1:0] [63:0]						   BRAT_CMT_targetaddr;

		wire [`BRAT_SIZE_LOG-1:0]			 BRAT_tail;
		wire 													 BRAT_full;
		wire 													 BRAT_full_1left;

		wire [`BRAT_SIZE-1:0]					 BRAT_squash;//to RS; 1 should be squashed

Complete_Retire_Stage DUT(

		 .clock,
		 .reset,
		 .FE_ID_dest_prfreg,
		 .FE_ID_dest_arfreg,
		 .ID_valid_inst,//is inst a valid instruction to be 00 01 10 11
		 .ID_branch,//is branch or not (diff)
		 .ID_store, //is store or not
		 .FE_ID_NPC,
		 .FE_ID_halt,
		 .EX_CMP_branch,
		 .EX_CMP_result,//ex_targetaddr
		 .EX_CMP_valid,
		 .EX_CMP_ROB,
		 .branch_pred,
		 .branch_predaddr,
		 .ROB_head,
		 .ROB_tail,
		 .ROB_full,
		 .ROB_full_1left,
		 .RET_issue_store,
		 .RET_prfn,
		 .RET_arfn,
	   .RET_valid,//retire is valid
		 .RET_halt,
		 .BRAT_CMP_miss,
		 .BRAT_CMP_num,
		 .BRAT_CMP_targetaddr,

		 .BRAT_CMT_v,
		 .BRAT_CMT_takeornot,
		 .BRAT_CMT_pc,
		 .BRAT_CMT_targetaddr,

		 .BRAT_tail,
		 .BRAT_full,
		 .BRAT_full_1left,

		 .BRAT_squash//to RS, 1 should be squashed
		);


task COMMIT_ALLOCATE;		
	  ID_valid_inst[0] = 1;
		ID_valid_inst[1] = 1;
		
		@(negedge clock);
		for(int t9=0;t9<100;t9++) begin
				FE_ID_dest_prfreg[0] = FE_ID_dest_prfreg[0] + 1;
				EX_CMP_ROB[0] = t9 % 32;
				EX_CMP_ROB[1] = t9 % 32 + 1;
				EX_CMP_valid[0] = 1;
				EX_CMP_valid[1] = 1;
			@(negedge clock);
				$display("ex_fin_regfl:%b head:%d tail:%d ROB_iocnt:%d full:%d",DUT.ex_fin_regfl,ROB_head,ROB_tail,DUT.ROB_iocnt,ROB_full);
		end
endtask

task BRANCH;
			//test BRAT head, tail, full, brat_entry_regfl
			@(negedge clock);
			ID_valid_inst = 2'b01;
			for (int t10=0;t10<4;t10++) begin
					ID_branch =  2'b01;
					branch_pred[0] = 0;
					@(negedge clock);
					$display("-----------------------------BRAT-ALLOC--------------------------------");
					$display(" head:%d tail:%d full:%d full1_left:%d",DUT.BRAT_head,BRAT_tail,BRAT_full,BRAT_full_1left);
					$display("brat_entry_regfl[3:0]:%d %d %d %d",DUT.brat_entry_regfl[3],DUT.brat_entry_regfl[2],DUT.brat_entry_regfl[1],DUT.brat_entry_regfl[0]);
			end
			ID_valid_inst = 2'b00;
			@(negedge clock);

			for (int t10=3;t10>=0;t10-=2) begin
					@(negedge clock);
					EX_CMP_valid = 2'b11;
					EX_CMP_branch = 2'b11;
					EX_CMP_result[0] = t10;
					EX_CMP_result[1] = t10-1;
				  //EX_CMP_branch[0] = ~EX_CMP_branch[0];
					//EX_CMP_branch[1] = ~EX_CMP_branch[1];
					EX_CMP_ROB[0] = t10;
					EX_CMP_ROB[1] = t10-1;
					@(negedge clock);
					$display("-----------------------------BRAT-EX_CMP--------------------------------");
					$display(" head:%d tail:%d full:%d full1_left:%d",DUT.BRAT_head,BRAT_tail,BRAT_full,BRAT_full_1left);
					$display("brat_entry_regfl[3:0]:%d %d %d %d",DUT.brat_entry_regfl[3],DUT.brat_entry_regfl[2],DUT.brat_entry_regfl[1],DUT.brat_entry_regfl[0]);
				  $display("--");	
					$display("CMP_miss:%b  CMP_num:%d  CMP_targetaddr:%d",BRAT_CMP_miss, BRAT_CMP_num, BRAT_CMP_targetaddr);
					$display("CMT_v:%b, CMT_takeornot: %b", BRAT_CMT_v, BRAT_CMT_takeornot);
			end

			for(int t10=0;t10<3;t10++) begin
					@(negedge clock);
					$display("-----------------------------BRAT-CMT--------------------------------");
					$display(" head:%d tail:%d full:%d full1_left:%d",DUT.BRAT_head,BRAT_tail,BRAT_full,BRAT_full_1left);
					$display("brat_entry_regfl[3:0]:%d %d %d %d",DUT.brat_entry_regfl[3],DUT.brat_entry_regfl[2],DUT.brat_entry_regfl[1],DUT.brat_entry_regfl[0]);
				  $display("--");	
					$display("CMP_miss:%b  CMP_num:%d  CMP_targetaddr:%d",BRAT_CMP_miss, BRAT_CMP_num, BRAT_CMP_targetaddr);
					$display("CMT_v:%b, CMT_takeornot: %b", BRAT_CMT_v, BRAT_CMT_takeornot);
			end	
endtask

always
begin
	#`half_clkperiod;
			clock  = ~clock; 
end

initial
begin
//		$monitor("Time:%4.0f clock:%b head:%d tail:%d full:%b ROB_iocnt:%d",$time,clock,head,tail,full,DUT.ROB_iocnt	);
	  clock = 0;
		reset = 1;

		FE_ID_dest_prfreg = 0;
		FE_ID_dest_arfreg = 0;
		ID_valid_inst = 0;

		ID_branch=0;
		ID_store=0;
		FE_ID_NPC =0;
		FE_ID_halt =0;

		EX_CMP_branch = 0;
		EX_CMP_result = 0;
		EX_CMP_valid = 0;
		EX_CMP_ROB = 0;
		
		branch_pred = 0;
		branch_predaddr = 0;

//reset
		@(negedge clock);
		@(negedge clock);
		@(negedge clock);
		reset = 0;
		@(negedge clock);
	
//Specify tasks
	//	COMMIT_ALLOCATE; hard to see, cuz instr_v from FE
			BRANCH;	
@(negedge clock);
		$finish;
end
endmodule
