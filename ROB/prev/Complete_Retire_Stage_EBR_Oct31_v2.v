module Complete_Retire_Stage(
		
	  //////////
	  // Inputs
	  //////////
		input								 					   clock,
		input 													 reset,
	
		//Inputs from FE
		input [1:0] [`PRF_SIZE_LOG-1:0]	 FE_ID_dest_prfreg,
		input [1:0] [`ARF_SIZE_LOG-1:0]	 FE_ID_dest_arfreg,
		input	[1:0]							 				 ID_valid_inst,//is inst a valid instruction to be 00 01 10 11
	  
		//Inputs from decoder
		input [1:0]							 				 ID_branch,//is branch or not (diff)
		input [1:0] 									 	 ID_store, //is store or not
	  input [1:0] [63:0] 					  	 FE_ID_NPC,
		input [1:0]											 FE_ID_halt,	
		//Inputs from Execution Complete
		input	[1:0]					 		 				 EX_CMP_branch,//branch taken or not
		input [1:0] [63:0]				 		 	 EX_CMP_result,//ex_targetaddr
		input [1:0]  				 		 				 EX_CMP_valid,
		input [1:0] [`ROB_SIZE_LOG-1:0]  EX_CMP_ROB,
		
		//Inputs from branch predictor
		input	[1:0]					 		 				 branch_pred,
		input [1:0] [63:0]   		 				 branch_predaddr,
		
		//Inputs for early branch resolution
		input [1:0]											 FE_BRAT;//if is assigned to a brat
		input [1:0] [`BRAT_SIZE_LOG-1:0] FE_BRAT_entry;//which brat entry is assigned

	  //////////
	  //Outputs
	  /////////
		//ROB related output
		output logic [`ROB_SIZE_LOG-1:0]   ROB_head,
		output logic [`ROB_SIZE_LOG-1:0]	 ROB_tail,
	  output 			       								 ROB_full,
		output 			       								 ROB_full_1left,
			
		//Retire signals
		output reg [1:0]					 						   RET_issue_store,
		output logic [1:0] [`PRF_SIZE_LOG-1:0]   RET_prfn,
		output logic [1:0] [`ARF_SIZE_LOG-1:0]   RET_arfn,
		output logic [1:0] [63:0]								 RET_branch_addr,
		output reg [1:0]                			   RET_branch_miss,
		output reg [1:0] 											   RET_valid,//retire is valid
		output reg [1:0]											   RET_halt
		);

wire [`ROB_SIZE_LOG-1:0] ROB_tail_p1;
wire [`ROB_SIZE_LOG-1:0] ROB_tail_p2;
wire [`ROB_SIZE_LOG-1:0] ROB_head_p1;
wire [`ROB_SIZE_LOG-1:0] ROB_head_p2;
wire [`ROB_SIZE_LOG:0]	 cur_size;
wire										 next_empty;
wire							 			 next_empty_1left;

wire						         retire1;
wire						         retire2;
wire [`ROB_SIZE_LOG:0]   next_iocnt;

logic [`ROB_SIZE_LOG-1:0] next_ROB_tail;
logic [`ROB_SIZE_LOG-1:0] next_ROB_head;
logic [1:0]							  next_RET_branch_miss;
logic			  						  next_issue1_store;
logic							  		  next_issue2_store;
logic [1:0] [63:0]				next_RET_branch_addr;
						
logic										 move_ROB_tail;
logic										 empty;
logic										 empty_1left;
logic [`ROB_SIZE_LOG:0]  iocnt;
logic [1:0]							 incount;
logic [1:0]							 outcount;

logic [`ROB_SIZE-1:0] [`PRF_SIZE_LOG-1:0] prfdest_regfl;
logic [`ROB_SIZE-1:0] [`ARF_SIZE_LOG-1:0] arfdest_regfl;
logic [`ROB_SIZE-1:0] [63:0]						  npc_regfl;
logic [`ROB_SIZE-1:0] 									  bran_v_regfl;
logic [`ROB_SIZE-1:0] 									  store_v_regfl;
logic [`ROB_SIZE-1:0] [63:0]    		      bran_predaddr_regfl;
logic [`ROB_SIZE-1:0] 			    		      bran_pred_ornot_regfl;
logic [`ROB_SIZE-1:0] [63:0]    		      bran_targetaddr_regfl;
logic [`ROB_SIZE-1:0] 				  		      bran_ornot_regfl;
logic [`ROB_SIZE-1:0] 									  ex_fin_regfl;
logic [`ROB_SIZE-1:0] 									  halt_regfl;
logic [`BRAT_SIZE-1:0] 										brat_v_regfl;//need clear this when retired???
logic [`BRAT_SIZE-1:0] [`ROB_SIZE_LOG-1:0] brat_entry_regfl;//need clear this when retired???

//ALLOCATE
assign ROB_tail_p1 = ROB_tail + 1'd1;
assign ROB_tail_p2 = ROB_tail + 2'd2;
assign ROB_head_p1 = ROB_head + 1'd1;
assign ROB_head_p2 = ROB_head + 2'd2;

assign cur_size = (next_ROB_tail >= next_ROB_head) ? (next_ROB_tail - next_ROB_head) : (next_ROB_tail + `ROB_SIZE - next_ROB_head); 
assign next_iocnt = (move_ROB_tail) ? cur_size : (iocnt+incount-outcount);//
assign ROB_full = next_iocnt ==`ROB_SIZE;
assign ROB_full_1left = next_iocnt == {`ROB_SIZE-1};
assign next_empty = next_iocnt == 0;
assign next_empty_1left = next_iocnt == 1;

//RETIRE
assign retire1 = !empty && ex_fin_regfl[ROB_head];
assign retire2 = !empty_1left && retire1 && ex_fin_regfl[ROB_head_p1] && !next_RET_branch_miss[0];  


always_comb begin

	//default
	next_ROB_head = ROB_head;
	next_ROB_tail = ROB_tail;
  move_ROB_tail = 1'b0;
  next_issue1_store = 1'b0;
  next_issue2_store = 1'b0;
	incount = 2'd0;
	outcount = 2'd0;
	
	next_RET_branch_addr[0] = 64'b0;
	next_RET_branch_addr[1] = 64'b0;
	next_RET_branch_miss[0] = 0;
	next_RET_branch_miss[1] = 0;
	//HEAD
	if(retire1) begin
			next_ROB_head = ROB_head_p1;
			outcount = 2'd1;
	end
	
	if(retire2) begin
			next_ROB_head = ROB_head_p2;
			outcount = 2'd2;
	end
	//branch miss 
	if(ex_fin_regfl[ROB_head]) begin
		if (bran_ornot_regfl[ROB_head]) begin
			next_RET_branch_addr[0]= bran_targetaddr_regfl[ROB_head];
			if(bran_pred_ornot_regfl[ROB_head])
				next_RET_branch_miss[0] = bran_targetaddr_regfl[ROB_head]==bran_predaddr_regfl[ROB_head];//??? should be inverse
			else 
			  next_RET_branch_miss[0] = 1;
		end
		else begin
				next_RET_branch_addr[0] = npc_regfl[ROB_head];
				next_RET_branch_miss[0] = bran_pred_ornot_regfl[ROB_head] ? 1 : 0;
		end
	end
	
	if(retire1 && next_RET_branch_miss[0]) begin
		move_ROB_tail = 1;
	end
	if(ex_fin_regfl[ROB_head_p1])begin
		if (bran_ornot_regfl[ROB_head_p1]) begin
			next_RET_branch_addr[1] = bran_targetaddr_regfl[ROB_head_p1];
			if(bran_pred_ornot_regfl[ROB_head_p1])
				next_RET_branch_miss[1] = bran_targetaddr_regfl[ROB_head_p1]==bran_predaddr_regfl[ROB_head_p1];
			else 
		  	next_RET_branch_miss[1] = 1;
		end
		else begin
				next_RET_branch_addr[1] = npc_regfl[ROB_head_p1];
				next_RET_branch_miss[1] = bran_pred_ornot_regfl[ROB_head_p1] ? 1 : 0;
		end
	end
	if(retire2 && next_RET_branch_miss[1] && !next_RET_branch_miss[0]) begin
		move_ROB_tail = 1;
		
	end

	//Early branch resolution
	//default
	next_brat_branch_miss = 1'b0;
	cur_brat = prev_brat + 1'b1;

	if(move_ROB_tail) begin
		brat_v_regfl <= {`BRAT_SIZE'b0};//clear all brat if the commited branch is a miss
	end
	else begin
			//if the completed inst is a branch	
			if(brat_v_regfl[cur_brat]) begin//brat is assigned
					if(ex_fin_regfl[brat_entry_regfl[cur_brat]]) begin//ex_fin
							next_brat_fin =1'b1; //This brat can be freed

							if (bran_ornot_regfl[brat_entry_regfl[cur_brat]]) begin
								next_brat_branch_addr= bran_targetaddr_regfl[brat_entry_regfl[cur_brat]];
								if(bran_pred_ornot_regfl[brat_entry_regfl[cur_brat]])
									next_brat_branch_miss = (bran_targetaddr_regfl[brat_entry_regfl[cur_brat]]==bran_predaddr_regfl[brat_entry_regfl[cur_brat]]) ? 0 : 1;//???
								else 
								  next_brat_branch_miss = 1'b1;
							end
							else begin
									next_brat_branch_addr = npc_regfl[brat_entry_regfl[cur_brat]];
									next_brat_branch_miss = bran_pred_ornot_regfl[brat_entry_regfl[cur_brat]] ? 1 : 0;
							end
					end
			end

			
			//Determine which brat is sent out
			//Both are completed && branch	
			//	Both are miss, free the one closest to prev freed brat -> ONLY Deal
			//	One is miss
			//	None is miss
			//One is completed && branch	
		
			//Can only free one brat each time, & in order ??? -> may not be
			//assigned out, add valid bit
	end//else

	//TAIL
	if(move_ROB_tail) begin
			next_ROB_tail = next_ROB_head;
	end else begin
			if(ID_valid_inst[0]) begin//
				next_ROB_tail = ROB_tail_p1;
				incount = 2'd1;
				
				if(ID_valid_inst[1]) begin
					next_ROB_tail = ROB_tail_p2;
					incount = 2'd2;
				end
			end
  end
 
 //STORE
	next_issue1_store = ex_fin_regfl[next_ROB_head]? store_v_regfl[next_ROB_head]:0;
	next_issue2_store = ex_fin_regfl[next_ROB_head]? store_v_regfl[next_ROB_head+1]:0;//F
end //always_comb

// ===================================================
// Sequential Block
// ===================================================

always_ff @(posedge clock) begin
	if(reset) begin
		ROB_head 	  				<= `SD {`ROB_SIZE_LOG'b0};
		ROB_tail 			  		<= `SD {`ROB_SIZE_LOG'b0};
		iocnt			  				<= `SD {`ROB_SIZE_LOG+1{1'b0}}; 
		empty			  				<= `SD 1'b1; 
		empty_1left 				<= `SD 1'b0; 
		RET_branch_miss[0] 	<= `SD 1'b0;
		RET_branch_miss[1] 	<= `SD 1'b0;
		RET_issue_store[0]  <= `SD 1'b0;
		RET_issue_store[1]  <= `SD 1'b0;
		RET_valid[0]    		<= `SD 1'b0;
		RET_valid[1]    		<= `SD 1'b0;
		RET_branch_addr[0]  <= `SD 64'b0;
		RET_branch_addr[1]  <= `SD 64'b0;
		for(int m = 0; m < `ROB_SIZE; m++) begin
			ex_fin_regfl[m] <= `SD 1'b0;
			halt_regfl[m]   <= `SD 1'b0;
		end
	end//reset
	else begin		

		RET_issue_store[0]	 <= `SD next_issue1_store;
		RET_issue_store[1]	 <= `SD next_issue2_store;
		RET_valid[0]  		   <= `SD retire1;
		RET_valid[1]  		   <= `SD retire2;
		ROB_head 	 					 <= `SD next_ROB_head;
		ROB_tail 			 			 <= `SD next_ROB_tail;
		iocnt			 					 <= `SD next_iocnt; 
		empty 		 					 <= `SD next_empty; 
		empty_1left					 <= `SD next_empty_1left; 
		RET_branch_miss[0]   <= `SD next_RET_branch_miss[0];
		RET_branch_miss[1]   <= `SD next_RET_branch_miss[1];

		//ALLOCATE 
		if(ID_valid_inst[0]) begin
				prfdest_regfl[ROB_tail]		   	 	<= `SD FE_ID_dest_prfreg[0];
				arfdest_regfl[ROB_tail]		   	 	<= `SD FE_ID_dest_arfreg[0];
				npc_regfl[ROB_tail]				   	 	<= `SD FE_ID_NPC[0];
				bran_v_regfl[ROB_tail]          <= `SD ID_branch[0];
				store_v_regfl[ROB_tail]         <= `SD ID_store[0];
				bran_predaddr_regfl[ROB_tail]	  <= `SD branch_predaddr[0]; 
				bran_pred_ornot_regfl[ROB_tail] <= `SD branch_pred[0]; 
				ex_fin_regfl[ROB_tail]          <= `SD FE_ID_halt[0]; 
				halt_regfl[ROB_tail]						<= `SD FE_ID_halt[0];

				if(ID_branch[0]) begin//any branch will be assigned to a brat, otherwise stall
					 brat_entry_regfl[FE_BRAT_entry[0]] <= `SD ROB_tail;	
					 brat_v_regfl[FE_BRAT_entry[0]]			<= `SD 1'b1;
				end

				if(ID_valid_inst[1]) begin
						prfdest_regfl[ROB_tail_p1]		   	 <= `SD FE_ID_dest_prfreg[1];
						arfdest_regfl[ROB_tail_p1]		   	 <= `SD FE_ID_dest_arfreg[1];
						npc_regfl[ROB_tail_p1]				   	 <= `SD FE_ID_NPC[1];
						bran_v_regfl[ROB_tail_p1]          <= `SD ID_branch[1];
						store_v_regfl[ROB_tail_p1]         <= `SD ID_store[1];
						bran_predaddr_regfl[ROB_tail_p1]	 <= `SD branch_predaddr[1]; 
						bran_pred_ornot_regfl[ROB_tail_p1] <= `SD branch_pred[1]; 
						ex_fin_regfl[ROB_tail_p1]          <= `SD FE_ID_halt[1]; 
						halt_regfl[ROB_tail_p1]						 <= `SD FE_ID_halt[1];
					
					if(ID_branch[1]) begin
					 brat_entry_regfl[FE_BRAT_entry[1]] <= `SD ROB_tail_p1;	
					 brat_v_regfl[FE_BRAT_entry[1]]			<= `SD 1'b1;
					end
				end
		end
		
		//EXECUTION COMPLETE 
		if(EX_CMP_valid[0]) begin
				ex_fin_regfl[EX_CMP_ROB[0]]          <= `SD 1'b1; 
				bran_targetaddr_regfl[EX_CMP_ROB[0]] <= `SD EX_CMP_result[0]; 
				bran_ornot_regfl[EX_CMP_ROB[0]]			 <= `SD EX_CMP_branch[0];
		end
	
		if(EX_CMP_valid[1]) begin
				ex_fin_regfl[EX_CMP_ROB[1]]          <= `SD 1'b1; 
				bran_targetaddr_regfl[EX_CMP_ROB[1]] <= `SD EX_CMP_result[1]; 
				bran_ornot_regfl[EX_CMP_ROB[1]]			 <= `SD EX_CMP_branch[1];
		end

		//RETIRE 
		if(retire1) begin
			 RET_prfn[0]        <= `SD prfdest_regfl[ROB_head];
			 RET_arfn[0]        <= `SD arfdest_regfl[ROB_head];
			 RET_branch_addr[0] <= `SD next_RET_branch_addr[0];	
			 RET_halt[0]				<= `SD halt_regfl[ROB_head];			 
		end
		
		if(retire2) begin
			 RET_prfn[1]        <= `SD prfdest_regfl[ROB_head_p1];
			 RET_arfn[1]        <= `SD arfdest_regfl[ROB_head_p1];
			 RET_branch_addr[1] <= `SD next_RET_branch_addr[1];
			 RET_halt[1]				<= `SD halt_regfl[ROB_head_p1] ;
		end

	end
end//always_ff





endmodule
