module rob(
		input								 clk,reset,
		//1st way input
		input								 bran1_pred,
		input								 ex1_branornot,
		input [63:0]				 ex1_targetaddr,
		input       				 ex1_fin,
		input [`ROB_IDX-1:0] ex1_fin_robidx,
		input [`PRF_IDX-1:0] prfdest1,
		input 							 bran1_v,
		input 							 store1_v,
		input [63:0]         bran1_predaddr,
		input 			         bran1_pred_ornot,
		input								 instr1_v,
		//2nd way input
		input								 bran2_pred,
		input								 ex2_branornot,
		input [63:0]				 ex2_targetaddr,
		input       				 ex2_fin,
		input [`ROB_IDX-1:0] ex2_fin_robidx,
		input [`PRF_IDX-1:0] prfdest2,
		input 							 bran2_v,
		input 							 store2_v,
		input [63:0]         bran2_predaddr,
		input 			         bran2_pred_ornot,
		input								 instr2_v,
		
		output logic [`ROB_IDX-1:0]  head,
		output logic [`ROB_IDX-1:0]	 tail,
		output logic       					 full,
		output logic       					 full_1left,
		//1st way output
		output logic					 			 issue1_store,
		output logic [`PRF_IDX-1:0]  prfn1,
		output logic 								 ret1_branornot,
		output logic [63:0]					 bran1_targetaddr,
		output logic                 bran1_miss,
		output logic 								 retire1_v,
		//2nd way output
		output logic					 			 issue2_store,
		output logic [`PRF_IDX-1:0]  prfn2,
		output logic 								 ret2_branornot,
		output logic [63:0]					 bran2_targetaddr,
		output logic                 bran2_miss,
		output logic 								 retire2_v
		);

wire [`ROB_IDX-1:0] tail_p1;
wire [`ROB_IDX-1:0] tail_p2;
wire [`ROB_IDX-1:0] head_p1;
wire [`ROB_IDX-1:0] head_p2;
wire [`ROB_IDX:0]		cur_size;
wire								next_full;
wire								next_full_1left;
wire								next_empty;
wire								next_empty_1left;

wire						  retire1;
wire						  retire2;
wire [`ROB_IDX:0] next_iocnt;

logic [`ROB_IDX-1:0] next_tail;
logic [`ROB_IDX-1:0] next_head;
logic								 next_bran1_miss;
logic								 next_bran2_miss;
logic			  				 next_issue1_store;
logic							   next_issue2_store;

logic								 move_tail;
logic								 empty;
logic								 empty_1left;
logic [`ROB_IDX:0] 	 iocnt;
logic [1:0]					 incount;
logic [1:0]					 outcount;

logic [`ROBSIZE-1:0] [`PRF_IDX-1:0] prfdest_regfl;
logic [`ROBSIZE-1:0] 							  bran_v_regfl;
logic [`ROBSIZE-1:0] 							  store_v_regfl;
logic [`ROBSIZE-1:0] [63:0]         bran_predaddr_regfl;
logic [`ROBSIZE-1:0] 			          bran_pred_ornot_regfl;
logic [`ROBSIZE-1:0] [63:0]         bran_targetaddr_regfl;
logic [`ROBSIZE-1:0] 				        bran_ornot_regfl;
logic [`ROBSIZE-1:0] 							  ex_fin_regfl;

//ALLOCATE
assign tail_p1 = tail + 1'd1;
assign tail_p2 = tail + 2'd2;
assign head_p1 = head + 1'd1;
assign head_p2 = head + 2'd2;

assign cur_size = (next_tail >= next_head) ? (next_tail - next_head) : (next_tail + `ROBSIZE - next_head); 
//assign next_iocnt = (move_tail) ? 1'b0 : cur_size ;//F
//assign next_iocnt = ((instr_v & iocnt == `ROBSIZE-1) | (next_full & !retire)) ? `ROBSIZE : cur_size ;//F
assign next_iocnt = (move_tail) ? cur_size : (iocnt+incount-outcount);//???
assign next_full = next_iocnt ==`ROBSIZE;
assign next_full_1left = next_iocnt == {`ROBSIZE-1};
assign next_empty = next_iocnt == 0;
assign next_empty_1left = next_iocnt == 1;

//RETIRE
assign retire1 = !empty && ex_fin_regfl[head];
assign retire2 = !empty_1left && retire1 && ex_fin_regfl[head_p1] && !bran1_miss;  


always_comb begin

	//default
	next_head = head;
	next_tail = tail;
  move_tail = 1'b0;
  next_issue1_store = 1'b0;
  next_issue2_store = 1'b0;
	incount = 2'd0;
	outcount = 2'd0;
	//default data

	//HEAD
	if(retire1) begin
			next_head = head_p1;
			outcount = 2'd1;
	end
	
	if(retire2) begin
			next_head = head_p2;
			outcount = 2'd2;
	end
	//branch miss ???
	if (bran_ornot_regfl[head]) begin
		if(bran_pred_ornot_regfl[head])
			next_bran1_miss = bran_targetaddr_regfl[head]==bran_predaddr_regfl[head];
		else 
		  next_bran1_miss = 1;
	end
	else begin
			next_bran1_miss = bran_pred_ornot_regfl[head] ? 1 : 0;
	end
	
	if(retire1 && bran1_miss) begin
		move_tail = 1;
	end

	if (bran_ornot_regfl[head_p1]) begin
		if(bran_pred_ornot_regfl[head_p1])
			next_bran2_miss = bran_targetaddr_regfl[head_p1]==bran_predaddr_regfl[head_p1];
		else 
		  next_bran2_miss = 1;
	end
	else begin
			next_bran2_miss = bran_pred_ornot_regfl[head_p1] ? 1 : 0;
	end
	
	if(retire2 && bran2_miss && !bran1_miss) begin
		move_tail = 1;
		
	end
	//TAIL
	if(move_tail) begin
			next_tail = next_head;
	end else begin
			if(instr1_v && !full) begin//???
				next_tail = tail_p1;
				incount = 2'd1;
				
				if(instr2_v && !full_1left) begin
					next_tail = tail_p2;
					incount = 2'd2;
				end
			end
  end
 
 //STORE
	next_issue1_store = store_v_regfl[next_head];
	next_issue2_store = store_v_regfl[next_head+1];//F
end //always_comb

// ===================================================
// Sequential Block
// ===================================================

always_ff @(posedge clk) begin
	if(reset) begin
		head 			  <= `SD {`ROB_IDX'b0};
		tail 			  <= `SD {`ROB_IDX'b0};
		iocnt			  <= `SD {`ROB_IDX+1{1'b0}}; 
		full 			  <= `SD 1'b0;
		full_1left  <= `SD 1'b0;
		empty			  <= `SD 1'b1; 
		empty_1left <= `SD 1'b0; 
		bran1_miss 	<= `SD 1'b0;
		bran2_miss 	<= `SD 1'b0;
		issue1_store <= `SD 1'b0;
		issue2_store <= `SD 1'b0;
		retire1_v    <= `SD 1'b0;
		retire2_v    <= `SD 1'b0;
	end//reset
	else begin
		//ALLOCATE ???
		if(instr1_v && !full) begin
				prfdest_regfl[tail]		   	 	<= `SD prfdest1;
				bran_v_regfl[tail]          <= `SD bran1_v;
				store_v_regfl[tail]         <= `SD store1_v;
				bran_predaddr_regfl[tail]	  <= `SD bran1_predaddr; 
				bran_pred_ornot_regfl[tail] <= `SD bran1_pred_ornot; 
				ex_fin_regfl[tail]          <= `SD 1'b0; 
				
				if(instr2_v && !full_1left) begin
						prfdest_regfl[tail_p1]		   	 	<= `SD prfdest2;
						bran_v_regfl[tail_p1]          <= `SD bran2_v;
						store_v_regfl[tail_p1]         <= `SD store2_v;
						bran_predaddr_regfl[tail_p1]	  <= `SD bran2_predaddr; 
						bran_pred_ornot_regfl[tail_p1] <= `SD bran2_pred_ornot; 
						ex_fin_regfl[tail_p1]          <= `SD 1'b0; 
				end
		end
		
		//EXECUTION COMPLETE ???
		if(ex1_fin) begin
				ex_fin_regfl[ex1_fin_robidx]          <= `SD 1'b1; 
				bran_targetaddr_regfl[ex1_fin_robidx] <= `SD ex1_targetaddr; 
				bran_ornot_regfl[ex1_fin_robidx]			 <= `SD ex1_branornot;
		end
	
		if(ex2_fin) begin
				ex_fin_regfl[ex2_fin_robidx]          <= `SD 1'b1; 
				bran_targetaddr_regfl[ex2_fin_robidx] <= `SD ex2_targetaddr; 
				bran_ornot_regfl[ex2_fin_robidx]			 <= `SD ex2_branornot;
		end

		//RETIRE ???
		if(retire1) begin
			 prfn1            <= `SD prfdest_regfl[head];
			 ret1_branornot   <= `SD bran_ornot_regfl[head];
			 bran1_targetaddr <= `SD bran_targetaddr_regfl[head];		
		end
		
		if(retire2) begin
			 prfn2            <= `SD prfdest_regfl[head_p1];
			 ret2_branornot   <= `SD bran_ornot_regfl[head_p1];
			 bran2_targetaddr <= `SD bran_targetaddr_regfl[head_p1];
		end
		
		head 			 	 <= `SD next_head;
		tail 			 	 <= `SD next_tail;
		iocnt			 	 <= `SD next_iocnt; 
		full 			 	 <= `SD next_full;
		full_1left 	 <= `SD next_full_1left;
		empty 		 	 <= `SD next_empty; 
		empty_1left	 <= `SD next_empty_1left; 
		bran1_miss   <= `SD next_bran1_miss;
		bran2_miss   <= `SD next_bran2_miss;
		issue1_store <= `SD next_issue1_store;
		issue2_store <= `SD next_issue2_store;
		retire1_v    <= `SD retire1;
		retire2_v    <= `SD retire2;
	end

end//always_ff





endmodule
