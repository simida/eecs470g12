module testbench();

	logic								 clk,reset;
  //1st way logic	
	logic								 bran1_pred;
	logic								 ex1_branornot;
	logic [63:0]				 ex1_targetaddr;
	logic       				 ex1_fin;
	logic [`ROB_IDX-1:0] ex1_fin_robidx;
	logic [`PRF_IDX-1:0] prfdest1;
	logic 							 bran1_v;
	logic 							 store1_v;
	logic [63:0]         bran1_predaddr;
	logic 			         bran1_pred_ornot;
	logic								 instr1_v;
	//2nd way logic
	logic								 bran2_pred;
	logic								 ex2_branornot;
	logic [63:0]				 ex2_targetaddr;
	logic       				 ex2_fin;
	logic [`ROB_IDX-1:0] ex2_fin_robidx;
	logic [`PRF_IDX-1:0] prfdest2;
	logic 							 bran2_v;
	logic 							 store2_v;
	logic [63:0]         bran2_predaddr;
	logic 			         bran2_pred_ornot;
	logic								 instr2_v;

	wire [`ROB_IDX-1:0]  head;
	wire [`ROB_IDX-1:0]	 tail;
	wire       					 full;
	wire       					 full_1left;
	//1st way output
	wire					 			 issue1_store;
	wire [`PRF_IDX-1:0]  prfn1;
	wire 								 ret1_branornot;
	wire [63:0]					 bran1_targetaddr;
	wire                 bran1_miss;
	wire 							   retire1_v;
	//2nd way output
	wire					 			 issue2_store;
	wire [`PRF_IDX-1:0]  prfn2;
	wire 								 ret2_branornot;
	wire [63:0]					 bran2_targetaddr;
	wire                 bran2_miss;
	wire  							 retire2_v;

rob trob(
		.clk,
		.reset,

		.bran1_pred,
		.ex1_branornot,
		.ex1_targetaddr,
		.ex1_fin,
		.ex1_fin_robidx,
		.prfdest1,
		.instr1_v,
		.bran1_v,
		.store1_v,
		.bran1_predaddr,
		.bran1_pred_ornot,
		//2nd way input
		.bran2_pred,
		.ex2_branornot,
		.ex2_targetaddr,
		.ex2_fin,
		.ex2_fin_robidx,
		.prfdest2,
		.instr2_v,
		.bran2_v,
		.store2_v,
		.bran2_predaddr,
		.bran2_pred_ornot,
		
		.head,
		.tail,
		.full,
		.full_1left,
		//1st way output
		.issue1_store,
		.prfn1,
		.ret1_branornot,
		.bran1_targetaddr,
		.bran1_miss,
		.retire1_v,
		//2nd way output
		.issue2_store,
		.prfn2,
		.ret2_branornot,
		.bran2_targetaddr,
		.bran2_miss,
	  .retire2_v
		);


task ALLOCATE1_1;
		for(int t1=0;t1<100;t1++) begin
			prfdest1 = prfdest1 + 1;
			instr1_v=~instr1_v;
			@(negedge clk);
		end
endtask

task ALLOCATE1_2;
		for(int t2=0;t2<100;t2++) begin
			prfdest2 = prfdest2 + 1;
			instr2_v=~instr2_v;
			@(negedge clk);
		end
endtask

task ALLOCATE1_R;
		instr1_v = 1;
		instr2_v = 0;
			@(negedge clk);
		for(int t3=0;t3<100;t3++) begin
			prfdest1 = prfdest1 + 1;
			prfdest2 = prfdest2 + 1;
			instr1_v=~instr1_v;
			instr2_v=~instr2_v;
			@(negedge clk);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",trob.ex_fin_regfl,head,tail,trob.iocnt,trob.full);
		end
endtask

task ALLOCATE2;
		for(int t4=0;t4<100;t4++) begin
			prfdest1 = prfdest1 + 1;
			prfdest2 = prfdest2 + 1;
		
			instr1_v=~instr1_v;
			instr2_v=~instr2_v;
			@(negedge clk);
		end
endtask

task COMMIT1_1;
	  	for(int t5=0;t5<100;t5++) begin
				ex1_fin_robidx = $random;
				ex1_fin = 1;
				@(negedge clk);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d",trob.ex_fin_regfl,head,tail,trob.iocnt);
			end
endtask

task COMMIT1_2;
	  	for(int t6=0;t6<100;t6++) begin
				ex2_fin_robidx = $random;
				ex2_fin = 1;
				@(negedge clk);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d",trob.ex_fin_regfl,head,tail,trob.iocnt);
			end
endtask

task COMMIT2_1;
			ex1_fin = 1;
				@(negedge clk);
	  	for(int t7=0;t7<32;t7++) begin
				ex1_fin_robidx = t7;
				ex2_fin_robidx = t7;
				ex1_fin = ~ex1_fin;
				ex2_fin = ~ex2_fin;
				@(negedge clk);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d",trob.ex_fin_regfl,head,tail,trob.iocnt);
				@(negedge clk);
			end
				@(negedge clk);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d",trob.ex_fin_regfl,head,tail,trob.iocnt);
endtask

task COMMIT2_2;
				@(negedge clk);
	  	for(int t8=0;t8<32;t8++) begin
				ex1_fin_robidx = t8;
				ex2_fin_robidx = t8+1;
				ex1_fin = ~ex1_fin;
				ex2_fin = ~ex2_fin;
				@(negedge clk);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d",trob.ex_fin_regfl,head,tail,trob.iocnt);
			end
				@(negedge clk);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d",trob.ex_fin_regfl,head,tail,trob.iocnt);
endtask

task COMMIT_ALLOCATE;		
	  instr1_v = 1;
		instr2_v = 1;
		
		@(negedge clk);
		for(int t9=0;t9<100;t9++) begin
				prfdest1 = prfdest1 + 1;
				ex1_fin_robidx = t9 % 32;
				ex2_fin_robidx = t9 % 32 + 1;
				ex1_fin = 1;
				ex2_fin = 1;
			@(negedge clk);
				$display("ex_fin_regfl:%b head:%d tail:%d iocnt:%d full:%d",trob.ex_fin_regfl,head,tail,trob.iocnt,trob.full);
		end
endtask

task BRANCH;
			@(negedge clk);
			instr1_v = 1;
			instr2_v = 1;
			for (int t10=0;t10<30;t10++) begin
					bran1_v =  1;
					bran1_pred_ornot = 0;
					bran2_v = 1;
					bran2_pred_ornot = 0;
					@(negedge clk);
					//$display("bran_v:%b bran_pred_ornot:%b head:%d tail:%d full:%d full1_left:%d",trob.bran_v_regfl,trob.bran_pred_ornot_regfl,head,tail,full,full_1left);
			end
					@(negedge clk);
					@(negedge clk);
					//$display("result after allocation");
					//$display("bran_v:%b bran_pred_ornot:%b head:%d tail:%d full:%d full1_left:%d",trob.bran_v_regfl,trob.bran_pred_ornot_regfl,head,tail,full,full_1left);
			for (int t10=31;t10>=0;t10-=2) begin
					@(negedge clk);
					ex1_fin = 1;
					ex2_fin = 1;
				  ex1_branornot = ~ex1_branornot;
					ex2_branornot = ~ex2_branornot;
					ex1_fin_robidx = t10;
					ex2_fin_robidx = t10-1;
					@(negedge clk);
			  	//$display("bran_ornot_regfl:%b ex_fin_regfl:%b ret1_branornot:%b retire1:%b retire2:%b bran1_miss:%b ret2_branornot:%b bran2_miss:%b head:%d tail:%d full:%d",trob.bran_ornot_regfl,trob.ex_fin_regfl,ret1_branornot,trob.retire1,trob.retire2,bran1_miss,ret2_branornot,bran2_miss,head,tail,full);
			end	
			//$display("finish execute");
			for (int t10=0;t10<32;t10++) begin
			@(negedge clk);
			  	//$display("bran_ornot_regfl:%b ret1_branornot:%b retire1:%b retire2:%b next_head:%d bran1_miss:%b ret2_branornot:%b bran2_miss:%b head:%d tail:%d",trob.bran_ornot_regfl,ret1_branornot,trob.retire1,trob.retire2,trob.next_head,bran1_miss,ret2_branornot,bran2_miss,head,tail);
			end
endtask


task BRANCH_0;
			@(negedge clk);
			instr1_v = 1;
			instr2_v = 1;
			for (int t10=0;t10<32;t10++) begin
					bran1_v =  1;
					bran1_pred_ornot = 1;
					bran1_predaddr = 64'h101010101010;
					bran2_v = 1;
					bran2_pred_ornot = 1;
					bran2_predaddr = 64'h101010101010;
					@(negedge clk);
			end
					@(negedge clk);
					//$display("result after allocation");
					//$display("bran_v:%b bran_pred_ornot:%b head:%d tail:%d",trob.bran_v_regfl,trob.bran_pred_ornot_regfl,head,tail);
			for (int t10=31;t10>=0;t10--) begin
					ex1_fin = 1;
					ex2_fin = 1;
				  ex1_branornot = 1;
					ex2_branornot = 1;
					ex1_targetaddr = 64'h101010101010;
					ex2_targetaddr = 64'h111111111111;
					ex1_fin_robidx = t10;
					ex2_fin_robidx = t10-1;
					@(negedge clk);
			  	//$display("bran_ornot_regfl:%b ret1_branornot:%b bran1_miss:%b ret2_branornot:%b bran2_miss:%b head:%d tail:%d",trob.bran_ornot_regfl,ret1_branornot,bran1_miss,ret2_branornot,bran2_miss,head,tail);
			end	
			for (int t10=0;t10<32;t10++) begin
					@(negedge clk);
			  	//$display("bran_ornot_regfl:%b ret1_branornot:%b bran1_miss:%b ret2_branornot:%b bran2_miss:%b head:%d tail:%d",trob.bran_ornot_regfl,ret1_branornot,bran1_miss,ret2_branornot,bran2_miss,head,tail);
			end
endtask


task BRANCH_1;
			@(negedge clk);
			instr1_v = 1;
			instr2_v = 1;
			for (int t10=0;t10<32;t10++) begin
					@(negedge clk);
					bran1_v =  1;
					bran1_pred_ornot = 0;
					bran2_v = 1; 
					bran2_pred_ornot = 1;
					bran2_predaddr = 64'h101010101010;
			end
					@(negedge clk);
					//$display("result after allocation");
					//$display("bran_v:%b bran_pred_ornot:%b head:%d tail:%d",trob.bran_v_regfl,trob.bran_pred_ornot_regfl,head,tail);
			for (int t10=0;t10<100;t10++) begin
					@(negedge clk);
					ex1_fin = 1;
					ex2_fin = 1;
				  ex1_branornot = ~ex1_branornot;
					ex2_branornot = ~ex2_branornot;
					ex1_targetaddr = 64'h101010101010;
					ex2_targetaddr = 64'h101010101010;
					ex1_fin_robidx = t10 % 31;
					ex2_fin_robidx = t10 % 31 +1;
					@(negedge clk);
				//$display("bran_ornot_regfl:%b ret1_branornot:%b bran1_miss:%b ret2_branornot:%b bran2_miss:%b head:%d tail:%d",trob.bran_ornot_regfl,ret1_branornot,bran1_miss,ret2_branornot,bran2_miss,head,tail);
			end	
endtask


//involve 'BRANCH' in
always
begin
	#`half_clkperiod;
			clk  = ~clk; 
end

initial
begin
//		$monitor("Time:%4.0f clk:%b head:%d tail:%d full:%b iocnt:%d",$time,clk,head,tail,full,trob.iocnt	);
		//$monitor("Time:%4.0f clk:%b reset%b bran_pred ex_branornot%b ex_targetaddr%b ex_fin%b ex_fin_robidx prfdest prfdest_v bran_vbran_predaddr bran_pred_ornot head tail prfn ret_branornot full bran_targetaddr bran_miss"	);
	  clk = 0;
		reset = 1;

		bran1_pred = 0;
		ex1_branornot = 0;
		ex1_targetaddr = 0;
		ex1_fin = 0;
		ex1_fin_robidx = 0;
		prfdest1 = 0;
		instr1_v = 0;
		bran1_v = 0;
		store1_v = 0;
		bran1_predaddr = 0;
		bran1_pred_ornot = 0;
		
		bran2_pred = 0;
		ex2_branornot = 0;
		ex2_targetaddr = 0;
		ex2_fin = 0;
		ex2_fin_robidx = 0;
		prfdest2 = 0;
		instr2_v = 0;
		bran2_v = 0;
		store2_v = 0;
		bran2_predaddr = 0;
		bran2_pred_ornot = 0;
//reset
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		reset = 0;
		@(negedge clk);
	
//Specify tasks
	  BRANCH_0;
@(negedge clk);
		$finish;
end
endmodule
