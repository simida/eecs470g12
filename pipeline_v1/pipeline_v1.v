////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  File Name: pipeline_v1.v                                                  //
//                                                                            //
//  Description: The combination of all modules. A one-way OoO Processor      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------//

`timescale 1ns/100ps

module pipeline (

		 ////////////////////
		 //     INPUTS     //
		 ////////////////////

		 input        clock,        // System clock
		 input        reset,        // System reset
		 input        [3:0]  mem2proc_response, // Tag from memory about current request
		 input        [63:0] mem2proc_data,     // Data coming back from memory
		 input        [3:0]  mem2proc_tag,      // Tag from memory about current reply\

		 ////////////////////
		 //     OUTPUTS    //
		 ////////////////////
		 
		 output logic [1:0]  proc2mem_command,  // command sent to memory
		 output logic [63:0] proc2mem_addr,     // Address sent to memory
		 output logic [63:0] proc2mem_data,     // Data sent to memory
		 
		 output logic [3:0]  pipeline_completed_insts,
		 output logic [3:0]  pipeline_error_status,
		 output logic [4:0]  pipeline_commit_wr_idx,
		 output logic [63:0] pipeline_commit_wr_data,
		 output logic        pipeline_commit_wr_en,
		 output logic [63:0] pipeline_commit_NPC,
		 
		// testing hooks (these must be exported so we can test
                // the synthesized version) data is tested by looking at
                // the final values in memory
				
			      
		 
                 );

   // Outputs from FrontEnd Stage
   logic [31:0] 		     fe_IR_out;
   logic [31:0] 		     fe_NPC_out;
   logic [`PRF_IDX-1:0] 	     fe_REGA_PRF_out;
   logic [`PRF_IDX-1:0] 	     fe_REGB_PRF_out;
   logic [`PRF_IDX-1:0] 	     fe_Dest_PRF_out;
   logic [1:0] 			     fe_opa_select_out;
   logic [1:0] 			     fe_opb_select_out;
   logic [4:0] 			     fe_alu_func_out;
   logic 			     fe_rd_mem_out;
   logic 			     fe_wr_mem_out;
   logic 			     fe_ldl_mem_out;
   logic 			     fe_stc_mem_out;
   logic 			     fe_cond_branch_out;
   logic 			     fe_uncond_branch_out;
   logic 			     fe_halt_out;
   logic 			     fe_cpuid_out;
   logic 			     fe_illegal_out;
   logic 			     fe_id_valid_inst_out;

   // Outputs from FRONT-END/ISSUE PIPELINE-----RS
       //logics for ALU
   logic 			     RS_ALU_valid_out;
   logic [63:0] 		     RS_ALU_NPC_out;
   logic [31:0] 		     RS_ALU_IR_out;
   logic [`ALU_FUNC_NUM_LOG-1:0]     RS_ALU_alu_func_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_ALU_regA_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_ALU_regB_out;
   logic [1:0] 			     RS_ALU_opa_sel_out;
   logic [1:0] 			     RS_ALU_opb_sel_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_ALU_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     RS_ALU_ROB_out;
   
       //Logics for BR
   logic 			     RS_BR_valid_out;
   logic [63:0] 		     RS_BR_NPC_out;
   logic [31:0] 		     RS_BR_IR_out;
   logic [`ALU_FUNC_NUM_LOG-1:0]     RS_BR_alu_func_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_BR_regA_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_BR_regB_out;
   logic [1:0] 			     RS_BR_opa_sel_out;
   logic [1:0] 			     RS_BR_opb_sel_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_BR_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     RS_BR_ROB_out;
   
       //Logics for MULT
   logic 			     RS_MULT_valid_out;
   logic [63:0] 		     RS_MULT_NPC_out;
   logic [31:0] 		     RS_MULT_IR_out;
   logic [`ALU_FUNC_NUM_LOG-1:0]     RS_MULT_alu_func_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_MULT_regA_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_MULT_regB_out;
   logic [1:0] 			     RS_MULT_opa_sel_out;
   logic [1:0] 			     RS_MULT_opb_sel_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_MULT_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     RS_MULT_ROB_out;
   
       //Logics for MEM
   logic 			     RS_MEM_valid_out;
   logic [63:0] 		     RS_MEM_NPC_out;
   logic [31:0] 		     RS_MEM_IR_out;
   logic [`ALU_FUNC_NUM_LOG-1:0]     RS_MEM_alu_func_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_MEM_regA_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_MEM_regB_out;
   logic [1:0] 			     RS_MEM_opa_sel_out;
   logic [1:0] 			     RS_MEM_opb_sel_out;
   logic [`PRF_SIZE_LOG-1:0] 	     RS_MEM_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     RS_MEM_ROB_out;
   
       // Logics for front End
   logic 			     RS_full_out;
  
   // Outputs from ISSUE/EXE PIPELINE------PRF & Valid Bit
   logic [63:0] 		     PRF_rda_reg_ALU_out;
   logic [63:0] 		     PRF_rda_reg_BR_out;
   logic [63:0] 		     PRF_rda_reg_MULT_out;
   logic [63:0] 		     PRF_rda_reg_MEM_out;
   logic [63:0] 		     PRF_rdb_reg_ALU_out;
   logic [63:0] 		     PRF_rdb_reg_BR_out;
   logic [63:0] 		     PRF_rdb_reg_MULT_out;
   logic [63:0] 		     PRF_rdb_reg_MEM_out;

   logic [`PRF_SIZE - 1:0] 	     VL_valid_list_out;

        // PipeReg for ALU
   logic 			     IS_EX_ALU_valid_out;
   logic [63:0] 		     IS_EX_ALU_NPC_out;
   logic [31:0] 		     IS_EX_ALU_IR_out;
   logic [`ALU_FUNC_NUM_LOG-1:0]     IS_EX_ALU_alu_func_out;
   logic [1:0] 			     IS_EX_ALU_opa_sel_out;
   logic [1:0] 			     IS_EX_ALU_opb_sel_out;
   logic [`PRF_SIZE_LOG-1:0] 	     IS_EX_ALU_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     IS_EX_ALU_ROB_out;
   
       //PipeReg for BR
   logic 			     IS_EX_BR_valid_out;
   logic [63:0] 		     IS_EX_BR_NPC_out;
   logic [31:0] 		     IS_EX_BR_IR_out;
   logic [`ALU_FUNC_NUM_LOG-1:0]     IS_EX_BR_alu_func_out;
   logic [1:0] 			     IS_EX_BR_opa_sel_out;
   logic [1:0] 			     IS_EX_BR_opb_sel_out;
   logic [`PRF_SIZE_LOG-1:0] 	     IS_EX_BR_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     IS_EX_BR_ROB_out;
   
       //PipeReg for MULT
   logic 			     IS_EX_MULT_valid_out;
   logic [63:0] 		     IS_EX_MULT_NPC_out;
   logic [31:0] 		     IS_EX_MULT_IR_out;
   logic [`ALU_FUNC_NUM_LOG-1:0]     IS_EX_MULT_alu_func_out;
   logic [1:0] 			     IS_EX_MULT_opa_sel_out;
   logic [1:0] 			     IS_EX_MULT_opb_sel_out;
   logic [`PRF_SIZE_LOG-1:0] 	     IS_EX_MULT_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     IS_EX_MULT_ROB_out;
   
       //PipeReg for MEM
   logic 			     IS_EX_MEM_valid_out;
   logic [63:0] 		     IS_EX_MEM_NPC_out;
   logic [31:0] 		     IS_EX_MEM_IR_out;
   logic [`ALU_FUNC_NUM_LOG-1:0]     IS_EX_MEM_alu_func_out;
   logic [1:0] 			     IS_EX_MEM_opa_sel_out;
   logic [1:0] 			     IS_EX_MEM_opb_sel_out;
   logic [`PRF_SIZE_LOG-1:0] 	     IS_EX_MEM_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     IS_EX_MEM_ROB_out;
   
   //Outputs for COMPLETE/RETIRE PIPELINE------ROB
   logic [`ROB_IDX-1:0] 	     ROB_head_out;
   logic [`ROB_IDX-1:0] 	     ROB_tail_out;
   logic [`PRF_IDX-1:0] 	     ROB_prfn_out;
   logic 			     ROB_ret_branornot_out;
   logic 			     ROB_full_out;
   logic [63:0] 		     ROB_bran_targetaddr_out;
   logic 			     RON_bran_miss_out;
   
   //Outputs for EXECUTE STAGE
   logic [`PRF_SIZE_LOG-1:0] 	     FU_ALU_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     FU_ALU_ROB_out;
   logic [63:0] 		     FU_ALU_result_out;   // ALU result
   logic 			     FU_ALU_valid_out;
   
   logic [`PRF_SIZE_LOG-1:0] 	     FU_MULT_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     FU_MULT_ROB_out;
   logic [63:0] 		     FU_MULT_result_out;   // MULT result
   logic 			     FU_MULT_valid_out;
   
   logic [`PRF_SIZE_LOG-1:0] 	     FU_BR_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     FU_BR_ROB_out;
   logic [63:0] 		     FU_BR_result_out;   // BRANCH address
   logic 			     FU_BR_branch_out;   // BRANCH OR NOT
   logic 			     FU_BR_valid_out;

   logic [`PRF_SIZE_LOG-1:0] 	     FU_MEM_dest_out;
   logic [`ROB_SIZE_LOG-1:0] 	     FU_MEM_ROB_out;
   logic 			     FU_MEM_stall_out;   
   logic [63:0] 		     FU_MEM_result_out; 
   

   
   // Memory interface/arbiter wires
   logic [63:0] proc2Dmem_addr, proc2Imem_addr;
   logic [1:0] 	     proc2Dmem_command, proc2Imem_command;
   logic [3:0] 	     Imem2proc_response, Dmem2proc_response;


   assign proc2mem_command =
			    (proc2Dmem_command==`BUS_NONE)?proc2Imem_command:proc2Dmem_command;
   assign proc2mem_addr =
			 (proc2Dmem_command==`BUS_NONE)?proc2Imem_addr:proc2Dmem_addr;
   assign Dmem2proc_response = 
			       (proc2Dmem_command==`BUS_NONE) ? 0 : mem2proc_response;
   assign Imem2proc_response =
			      (proc2Dmem_command==`BUS_NONE) ? mem2proc_response : 0;
   
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                   FRONT END STAGE                      //
   //                                                        //
   ////////////////////////////////////////////////////////////

   FrontEnd FrontEnd_0 (
			.clock(clock),
			.reset(reset),
			
			//------From/To Mem------
			.Imem2proc_response(Imem2proc_response),
			.Imem2proc_data(mem2proc_data),
			.Imem2proc_tag(mem2proc_tag),

			.proc2Imem_command(proc2Imem_command),
			.proc2Imem_addr(proc2Imem_addr),

			//------To RS & ROB0------
			.IR(fe_IR_out),
			.NPC(fe_NPC_out),
			.ra_prf_out(fe_RegA_PRF_out),
			.rb_prf_out(fe_RegB_PRF_out),
			.dest_prf_out(fe_Dest_PRF_out),
			.opa_select_out(fe_opa_select_out),
			.opb_select_out(fe_opb_select_out),
			.alu_func_out(fe_alu_func_out),
			.rd_mem_out(fe_rd_mem_out),        // does inst read memory?
			.wr_mem_out(fe_wr_mem_out),        // does inst write memory?
			.ldl_mem_out(fe_ldl_mem_out),       // load-lock inst?
			.stc_mem_out(fe_stc_mem_out),       // store-conditional inst?
			.cond_branch_out(fe_cond_branch_out),   // is inst a conditional branch?
			.uncond_branch_out(fe_uncond_branch_out), // is inst an unconditional branch 
												        // or jump?
			.halt_out(fe_halt_out),
			.cpuid_out(fe_cpuid_out),         // get CPUID inst?
			.illegal_out(fe_illegal_out),
			.id_valid_inst_out(fe_id_valid_inst_out),

			//-----From ROB-----
							input logic ROB_full,
				input logic flush, //Branch Misprediction
				input logic [31:0] Br_target,

				input logic commit1,
				input logic [4:0] arf_dest1,
				input logic [`PRF_IDX-1:0] prf_dest1,
			//------From RS------	
			.RS_full(RS_full_out)
			);
    
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                   FRONT-END/ISSUE PIPELINE             //
   //                            RS                          //
   //                                                        //
   ////////////////////////////////////////////////////////////

   RS RS_0 (//Inputs
	    .reset(reset),
	    .clock(clock),
	    .branch_mis(ROB_bran_miss_out),
	    .valid_list(VL_valid_list_out), 
	    .RAT_regA(fe_REGA_PRF_out),      
	    .RAT_regB(fe_REGB_PRF_out),      
	    .RAT_dest_reg(fe_Dest_PRF_out),  
	    .id_NPC(fe_NPC_out),
	    .id_IR(fe_IR_out),
	    .id_opa_select(fe_opa_select_out),    
	    .id_opb_select(fe_opb_select_out),    
	    .id_alu_func(fe_alu_function_out),      
	    .id_rd_mem(fe_rd_mem_out),       
	    .id_wr_mem(fe_wr_mem_out),        
	    .id_cond_branch(fe_cond_branch_out),   
	    .id_uncond_branch(fe_uncond_branch_out), 
	    .id_halt(fe_halt_out),
	    .id_illegal(fe_illegal_out),
	    .id_valid_inst(fe_id_valid_inst_out),
	    .ROB_head(ROB_head_out),


	    
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                     ISSUE STAGE                        //
   //                     Still    RS                        //
   //                                                        //
   ////////////////////////////////////////////////////////////


	    
	    //Outputs
	    //Outputs for ALU
	    .RS_ALU_valid(RS_ALU_valid_out),
	    .RS_ALU_NPC(RS_ALU_NPC_out),
	    .RS_ALU_IR(RS_ALU_IR_out),
	    .RS_ALU_alu_func(RS_ALU_alu_func_out),
	    .RS_ALU_regA(RS_ALU_regA_out),
	    .RS_ALU_regB(RS_ALU_regB_out),
	    .RS_ALU_opa_sel(RS_ALU_opa_sel_out),
	    .RS_ALU_opb_sel(RS_ALU_opb_sel_out),
	    .RS_ALU_dest(RS_ALU_dest_out),
	    .RS_ALU_ROB(RS_ALU_ROB_out),
	    
	    //Outputs for BR
	    .RS_BR_valid(RS_BR_valid_out),
	    .RS_BR_NPC(RS_BR_NPC_out),
	    .RS_BR_IR(RS_BR_IR_out),
	    .RS_BR_alu_func(RS_BR_alu_func_out),
	    .RS_BR_regA(RS_BR_regA_out),
	    .RS_BR_regB(RS_BR_regB_out),
	    .RS_BR_opa_sel(RS_BR_opa_sel_out),
	    .RS_BR_opb_sel(RS_BR_opb_sel_out),
	    .RS_BR_dest(RS_BR_dest_out),
	    .RS_BR_ROB(RS_BR_ROB_out),

	    //Outputs for MULT
	    .RS_MULT_valid(RS_MULT_valid_out),
	    .RS_MULT_NPC(RS_MULT_NPC_out),
	    .RS_MULT_IR(RS_MULT_IR_out),
	    .RS_MULT_alu_func(RS_MULT_alu_func_out),
	    .RS_MULT_regA(RS_MULT_regA_out),
	    .RS_MULT_regB(RS_MULT_regB_out),
	    .RS_MULT_opa_sel(RS_MULT_opa_sel_out),
	    .RS_MULT_opb_sel(RS_MULT_opb_sel_out),
	    .RS_MULT_dest(RS_MULT_dest_out),
	    .RS_MULT_ROB(RS_MULT_ROB_out),
	    
	    //Outputs for MEM
	    .RS_MEM_valid(RS_MEM_valid_out),
	    .RS_MEM_NPC(RS_MEM_NPC_out),
	    .RS_MEM_IR(RS_MEM_IR_out),
	    .RS_MEM_alu_func(RS_MEM_alu_func_out),
	    .RS_MEM_regA(RS_MEM_regA_out),
	    .RS_MEM_regB(RS_MEM_regB_out),
	    .RS_MEM_opa_sel(RS_MEM_opa_sel_out),
	    .RS_MEM_opb_sel(RS_MEM_opb_sel_out),
	    .RS_MEM_dest(RS_MEM_dest_out),
	    .RS_MEM_ROB(RS_MEM_ROB_out),
	    
	    // Outputs for front End
	    .RS_full(RS_full_out)	
	);


   ////////////////////////////////////////////////////////////
   //                                                        //
   //                   ISSUE/EXECUTE PIPELINE               //
   //                      PRF &  Valid List                 //
   //                                                        //
   ////////////////////////////////////////////////////////////

   PRF PRF_0 (//Inputs
	      .rda_idx({RS_ALU_regA_out, RS_BR_regA_out, RS_MULT_regA_out, RS_MEM_regA_out}),
	      .rdb_idx({RS_ALU_regB_out, RS_BR_regB_out, RS_MULT_regB_out, RS_MEM_regB_out}), 
//	      wr_idx,	// read/write index
//				  input  [3:0] [63:0] wr_data,					// write data
//				  input  [3:0]  wr_en,						// write enable
	      .wr_clk(clock),	                                                                // clock

	      //Outputs
	      .rda_out({PRF_rda_reg_ALU_out, PRF_rda_reg_BR_out, PRF_rda_reg_MULT_out, PRF_rda_reg_MEM_out}),
	      .rdb_out({PRF_rdb_reg_ALU_out, PRF_rdb_reg_BR_out, PRF_rdb_reg_MULT_out, PRF_rdb_reg_MEM_out}),			// read data
	      .write_conflict()				  		// for test use
	      );
  
   valid_list valid_list_0 (//Inputs
//			    input  [3:0] [`PRF_SIZE_LOG-1:0] ready_idx,			// write index
//			    input  [3:0]  ready,						// write enable
			    .clock(clock),							// clock
			    .reset(reset),
			    .branch_mis(ROB_bran_miss_out),
//NEED TO ADD PORT IN FRONT END//////////////////			    input [`PRF_SIZE-1:0] RRAT_valid_list,
			    .valid_list(VL_valid_list_out),			// valid_list
			    .write_conflict()					  	// for test use
			    );

   always_ff @(posedge clock)
     begin
	if (reset)
	  begin
	     // PipeReg for ALU
	     IS_EX_ALU_valid_out     <= `SD 0;
	     IS_EX_ALU_NPC_out       <= `SD 0;
             IS_EX_ALU_IR_out        <= `SD 0;
	     IS_EX_ALU_alu_func_out  <= `SD 0;
	     IS_EX_ALU_opa_sel_out   <= `SD 0;
	     IS_EX_ALU_opb_sel_out   <= `SD 0;
	     IS_EX_ALU_dest_out      <= `SD 0;
	     IS_EX_ALU_ROB_out       <= `SD 0;
	     
	     //PipeReg for BR
	     IS_EX_BR_valid_out     <= `SD 0;
	     IS_EX_BR_NPC_out       <= `SD 0;
             IS_EX_BR_IR_out        <= `SD 0;
	     IS_EX_BR_alu_func_out  <= `SD 0;
	     IS_EX_BR_opa_sel_out   <= `SD 0;
	     IS_EX_BR_opb_sel_out   <= `SD 0;
	     IS_EX_BR_dest_out      <= `SD 0;
	     IS_EX_BR_ROB_out       <= `SD 0;
	     
	     //PipeReg for MULT
	     IS_EX_MULT_valid_out     <= `SD 0;
	     IS_EX_MULT_NPC_out       <= `SD 0;
             IS_EX_MULT_IR_out        <= `SD 0;
	     IS_EX_MULT_alu_func_out  <= `SD 0;
	     IS_EX_MULT_opa_sel_out   <= `SD 0;
	     IS_EX_MULT_opb_sel_out   <= `SD 0;
	     IS_EX_MULT_dest_out      <= `SD 0;
	     IS_EX_MULT_ROB_out       <= `SD 0;
	     
	     //PipeReg for MEM
	     IS_EX_MEM_valid_out     <= `SD 0;
	     IS_EX_MEM_NPC_out       <= `SD 0;
             IS_EX_MEM_IR_out        <= `SD 0;
	     IS_EX_MEM_alu_func_out  <= `SD 0;
	     IS_EX_MEM_opa_sel_out   <= `SD 0;
	     IS_EX_MEM_opb_sel_out   <= `SD 0;
	     IS_EX_MEM_dest_out      <= `SD 0;
	     IS_EX_MEM_ROB_out       <= `SD 0;
	  end // if (reset)
	else 
	  begin
             // PipeReg for ALU
	     IS_EX_ALU_valid_out     <= `SD RS_ALU_valid_out;
	     IS_EX_ALU_NPC_out       <= `SD RS_ALU_NPC_out;
             IS_EX_ALU_IR_out        <= `SD RS_ALU_IR_out;
	     IS_EX_ALU_alu_func_out  <= `SD RS_ALU_alu_func_out;
	     IS_EX_ALU_opa_sel_out   <= `SD RS_ALU_opa_sel_out;
	     IS_EX_ALU_opb_sel_out   <= `SD RS_ALU_opb_sel_out;
	     IS_EX_ALU_dest_out      <= `SD RS_ALU_dest_out;
	     IS_EX_ALU_ROB_out       <= `SD RS_ALU_ROB_out;
	     
	     //PipeReg for BR
	     IS_EX_BR_valid_out     <= `SD RS_BR_valid_out;
	     IS_EX_BR_NPC_out       <= `SD RS_BR_NPC_out;
             IS_EX_BR_IR_out        <= `SD RS_BR_IR_out;
	     IS_EX_BR_alu_func_out  <= `SD RS_BR_alu_func_out;
	     IS_EX_BR_opa_sel_out   <= `SD RS_BR_opa_sel_out;
	     IS_EX_BR_opb_sel_out   <= `SD RS_BR_opb_sel_out;
	     IS_EX_BR_dest_out      <= `SD RS_BR_dest_out;
	     IS_EX_BR_ROB_out       <= `SD RS_BR_ROB_out;
	     
	     //PipeReg for MULT
	     IS_EX_MULT_valid_out     <= `SD RS_MULT_valid_out;
	     IS_EX_MULT_NPC_out       <= `SD RS_MULT_NPC_out;
             IS_EX_MULT_IR_out        <= `SD RS_MULT_IR_out;
	     IS_EX_MULT_alu_func_out  <= `SD RS_MULT_alu_func_out;
	     IS_EX_MULT_opa_sel_out   <= `SD RS_MULT_opa_sel_out;
	     IS_EX_MULT_opb_sel_out   <= `SD RS_MULT_opb_sel_out;
	     IS_EX_MULT_dest_out      <= `SD RS_MULT_dest_out;
	     IS_EX_MULT_ROB_out       <= `SD RS_MULT_ROB_out;
	     
	     //PipeReg for MEM
	     IS_EX_MEM_valid_out     <= `SD RS_MEM_valid_out;
	     IS_EX_MEM_NPC_out       <= `SD RS_MEM_NPC_out;
             IS_EX_MEM_IR_out        <= `SD RS_MEM_IR_out;
	     IS_EX_MEM_alu_func_out  <= `SD RS_MEM_alu_func_out;
	     IS_EX_MEM_opa_sel_out   <= `SD RS_MEM_opa_sel_out;
	     IS_EX_MEM_opb_sel_out   <= `SD RS_MEM_opb_sel_out;
	     IS_EX_MEM_dest_out      <= `SD RS_MEM_dest_out;
	     IS_EX_MEM_ROB_out       <= `SD RS_MEM_ROB_out;
	  end // else: !if(reset)
	
     end // always_ff @ (posedge clock)



   
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                        EXECUTE STAGE                   //
   //                     ALU, MULT, BR, MEM                 //
   //                                                        //
   ////////////////////////////////////////////////////////////
   
   FU_ALU FU_ALU_0 (
		    .clock(clock),               // system clock
		    .reset(reset),         // system reset
		    .ALU_valid(IS_EX_ALU_valid_out),
		    .ALU_NPC(IS_EX_ALU_NPC_out),
		    .ALU_IR(IS_EX_ALU_IR_out),
		    .ALU_alu_func(ES_EX_ALU_alu_func_out),
		    .ALU_regA(PRF_rda_reg_ALU_out),
		    .ALU_regB(PRF_rdb_reg_ALU_out),
		    .ALU_opa_sel(IS_EX_ALU_opa_sel_out),
		    .ALU_opb_sel(IS_EX_ALU_opb_sel_out),
		    .ALU_dest(IS_EX_ALU_dest_out),
		    .ALU_ROB(IS_EX_ALU_ROB_out),

		    .FU_ALU_dest_out(FU_ALU_dest_out),
		    .FU_ALU_ROB_out(FU_ALU_ROB_out),
		    .FU_ALU_result_out(FU_ALU_result_out),   // ALU result
		    .FU_ALU_valid_out(FU_ALU_valid_out)
               );

   FU_MULT FU_MULT_0 (
		      .clock(clock),               // system clock
		      .reset(reset),         // system reset
		      .MULT_valid(IS_EX_MULT_valid_out),
		      .MULT_NPC(IS_EX_MULT_NPC_out),
		      .MULT_IR(IS_EX_MULT_IR_out),
		      .MULT_alu_func(ES_EX_MULT_alu_func_out),
		      .MULT_regA(PRF_rda_reg_MULT_out),
		      .MULT_regB(PRF_rdb_reg_MULT_out),
		      .MULT_opa_sel(IS_EX_MULT_opa_sel_out),
		      .MULT_opb_sel(IS_EX_MULT_opb_sel_out),
		      .MULT_dest(IS_EX_MULT_dest_out),
		      .MULT_ROB(IS_EX_MULT_ROB_out),
		      
		      .FU_MULT_dest_out(FU_MULT_dest_out),
		      .FU_MULT_ROB_out(FU_MULT_ROB_out),
		      .FU_MULT_result_out(FU_MULT_result_out),   // MULT result
		      .FU_MULT_valid_out(FU_MULT_valid_out)
		      );
   
   FU_BR FU_BR_0 (
		  .clock(clock),               // system clock
		  .reset(reset),         // system reset
		  .BR_valid(IS_EX_BR_valid_out),
		  .BR_NPC(IS_EX_BR_NPC_out),
		  .BR_IR(IS_EX_BR_IR_out),
		  .BR_alu_func(ES_EX_BR_alu_func_out),
		  .BR_regA(PRF_rda_reg_BR_out),
		  .BR_regB(PRF_rdb_reg_BR_out),
		  .BR_opa_sel(IS_EX_BR_opa_sel_out),
		  .BR_opb_sel(IS_EX_BR_opb_sel_out),
		  .BR_dest(IS_EX_BR_dest_out),
		  .BR_ROB(IS_EX_BR_ROB_out),
		  
		  .FU_BR_dest_out(FU_BR_dest_out),
		  .FU_BR_ROB_out(FU_BR_ROB_out),
		  .FU_BR_result_out(FU_BR_result_out),
		  .FU_BR_branch_out(FU_BR_branch_out),   // BR result
		  .FU_BR_valid_out(FU_BR_valid_out)
		  );

   FU_MEM FU_MEM_0 (
		    .clock(clock),               // system clock
		    .reset(reset),         // system reset

		    .Dmem2proc_data();
		    .Dmem2proc_tag(), 
		    .Dmem2proc_response(),
		    
		    .MEM_valid(IS_EX_MEM_valid_out),
		    .MEM_NPC(IS_EX_MEM_NPC_out),
		    .MEM_IR(IS_EX_MEM_IR_out),
		    .MEM_alu_func(ES_EX_MEM_alu_func_out),
		    .MEM_regA(PRF_rda_reg_MEM_out),
		    .MEM_regB(PRF_rdb_reg_MEM_out),
		    .MEM_opa_sel(IS_EX_MEM_opa_sel_out),
		    .MEM_opb_sel(IS_EX_MEM_opb_sel_out),
		    .MEM_dest(IS_EX_MEM_dest_out),
		    .MEM_ROB(IS_EX_MEM_ROB_out),
		    
		    .FU_MEM_dest_out(FU_MEM_dest_out),
		    .FU_MEM_ROB_out(FU_MEM_ROB_out),
		    .FU_MEM_result_out(FU_MEM_result_out),
		    
		    .FU_MEM_valid_out(FU_MEM_valid_out)

		    .proc2Dmem_command(proc2Dmem_command),
		    .proc2Dmem_addr(proc2Dmem_addr),    // Address sent to data-memory
		    .proc2Dmem_data(proc2Dmem_data)     // Data sent to data-memory
		    );
   
   
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                 EXECUTE/COMPLETE PIPELINE              //
   //                     ALU, MULT, BR, MEM                 //
   //                                                        //
   ////////////////////////////////////////////////////////////

   ////////////////////////////////////////////////////////////
   //                                                        //
   //                       COMPLETE STAGE                   //
   //                         Nothing...                     //
   //                                                        //
   ////////////////////////////////////////////////////////////

   ////////////////////////////////////////////////////////////
   //                                                        //
   //                  COMPLETE/RETIRE PIPELINE              //
   //                             ROB                        //
   //                                                        //
   ////////////////////////////////////////////////////////////

//ROB only can complete one instruction.
   
   rob rob_0(//Inputs
	     .clk(clock),
	     .reset(reset),
	     .bran_pred(1'b0),
	     .ex_branornot(FU_BR_branch_out),
	     .ex_targetaddr(FI_BR_result_out),
	     .ex_fin(,
//	     input [`ROB_IDX-1:0] ex_fin_robidx,
//	     input [`PRF_IDX-1:0] prfdest,
//	     input 							 prfdest_v,
//	     input 							 bran_v,
	     .bran_predaddr(64'h0000000000000000),
	     .bran_pred_ornot(1'b0),
	     
	     .head(ROB_head_out),
	     .tail(ROB_tail_out),
	     .prfn(ROB_prfn_out),
	     .ret_branornot(ROB_ret_branornot_out),
	     .full(ROB_full_out),
	     .bran_targetaddr(ROB_bran_targeaddr_out),
	     .bran_miss(ROB_bran_miss_out);
	     );