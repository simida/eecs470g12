//Branch direction predictor: Local History Predictor [First version: One way]
//Modified By Jingcheng Wang
//Function: 
//READ(predict): Take Branch Addr(current PC), then output whether it is taken or not taken
//WRITE(modify): Branch commit result is changed in BHT and PHT

`define BHT_LOG2 8
`define BHT_SIZE 256
`define PHT_LOG2 4
`define PHT_SIZE 16

////////////////////////////All places using `define A need to add `A

module Branch_Predictor(
			//From Fetcher
			input [1:0] [63:0] FE_PC,
			
			//To Fetcher
			output logic [1:0] FE_taken, ///////////////////////////////////Need add logic
			
			//From ROB
			input [1:0] [63:0] BRAT_CMT_pc,
			input [1:0] BRAT_CMT_takeornot,
			input [1:0] BRAT_CMT_v,
			
			input clock,reset
			);
   
   //Not it's not possible to have both FE_PC/BRAT_CMT_pc equal 
   //But the BRAT_CMT_pc Pattern MAY ALIAS!!!!
   //FE_PC--BRAT_CMT_pc may Alias
   wire [1:0] [`BHT_LOG2-1:0] 			BHT_index_fe;
   
   assign BHT_index_fe[0] = FE_PC[0][`BHT_LOG2+1:2];
   assign BHT_index_fe[1] = FE_PC[1][`BHT_LOG2+1:2];
   
   
   wire [1:0] [`BHT_LOG2-1:0] 			BHT_index_com; 
   assign BHT_index_com[0] = BRAT_CMT_pc[0][`BHT_LOG2+1:2];
   assign BHT_index_com[1] = BRAT_CMT_pc[1][`BHT_LOG2+1:2];
   
   
   reg [`BHT_SIZE-1:0] [`PHT_LOG2-1:0] 		BHT_registers;
   reg [`PHT_SIZE-1:0] [1:0] 	       		PHT_registers;
   
   logic [`BHT_SIZE-1:0] [`PHT_LOG2-1:0] 	next_BHT_registers;
   logic [`PHT_SIZE-1:0] [1:0] 		 	next_PHT_registers;
   logic [`PHT_SIZE-1:0] [1:0] 		 	next_PHT_registers_mid;
   
   //READ LOGIC: PURELY COMB: SRAM access
   //REMEMBER: 
   //Corner case1: write and read at the same time --> internal forwarding

   always_comb
     begin
	//if(FE_PC!=BRAT_CMT_pc)	fetake[1]=PHT_registers[BHT_registers[BHT_index]][1];
	FE_taken[0]=PHT_registers[BHT_registers[BHT_index_fe[0]]][1];
	FE_taken[1]=PHT_registers[BHT_registers[BHT_index_fe[1]]][1];
     end
   
   
   wire [`PHT_LOG2-1:0] patten_com0=BHT_registers[BHT_index_com[0]];
   wire [`PHT_LOG2-1:0] patten_com1=BHT_registers[BHT_index_com[1]];
   //WRITE LOGIC: UPDATE BOTH BHT AND PHT
   always_comb
     begin
	next_BHT_registers = BHT_registers;
	next_PHT_registers = PHT_registers;
	next_PHT_registers_mid = PHT_registers;
	
	//update BHT
	if(BRAT_CMT_v[0])
	  next_BHT_registers[BHT_index_com[0]] = {patten_com0[`PHT_LOG2-2:0],BRAT_CMT_takeornot[0]};
	if(BRAT_CMT_v[1])
	  next_BHT_registers[BHT_index_com[1]] = {patten_com1[`PHT_LOG2-2:0],BRAT_CMT_takeornot[1]};
	
	
	
	
	/*if(patten_com0==patten_com1 && BRAT_CMT_v[0] && BRAT_CMT_v[1])
	  begin
	     case(BRAT_CMT_takeornot)
	       2'b11: 
		 begin
		    if(PHT_registers[patten_com0][1]==1'b0) next_PHT_registers_mid[patten_com0] =  PHT_registers[patten_com0]+2;
		    else next_PHT_registers_mid[patten_com0] = 2'b11;
		 end
	       2'b00:
		 begin
		    if(PHT_registers[patten_com0][1]==1'b1) next_PHT_registers_mid[patten_com0] =  PHT_registers[patten_com0]-2;
		    else next_PHT_registers_mid[patten_com0] = 2'b00;
		 end
	       2'b01,2'b10:
		 begin
		    //Doing nothing
		 end				
	     endcase
	  end
	else
	  begin*/
	     if(BRAT_CMT_v[0])
	       begin
		  if( (PHT_registers[patten_com0]==2'd0 && ~BRAT_CMT_takeornot[0]) ||
		      (PHT_registers[patten_com0]==2'd3 && BRAT_CMT_takeornot[0]) )
		    begin
		       //if correct, nothing happens
		    end
		  else
		    begin				
		       if(BRAT_CMT_takeornot[0]) next_PHT_registers_mid[patten_com0] =  PHT_registers[patten_com0]+1;
		       else next_PHT_registers_mid[patten_com0] =  PHT_registers[patten_com0]-1;
		    end
	       end//if(BRAT_CMT_v[0])
	     
			next_PHT_registers[patten_com0]=next_PHT_registers_mid[patten_com0];
			next_PHT_registers[patten_com1]=next_PHT_registers_mid[patten_com1];
	     if(BRAT_CMT_v[1])
	       if( (PHT_registers[patten_com1]==2'd0 && ~BRAT_CMT_takeornot[1]) ||
		   (PHT_registers[patten_com1]==2'd3 && BRAT_CMT_takeornot[1]) )
		 begin
		    //if correct, nothing happens
		 end
	       else
		 begin				
		    if(BRAT_CMT_takeornot[1]) next_PHT_registers[patten_com1] =  next_PHT_registers_mid[patten_com1]+1;
		    else next_PHT_registers[patten_com1] =  next_PHT_registers_mid[patten_com1]-1;
		 end
	  //end // else: !if(patten_com0==patten_com1 && BRAT_CMT_v[0] && BRAT_CMT_v[1])
	
     end // always_comb begin
   
   
   
   
   always_ff@(posedge clock)
     begin
	if(reset)
	  begin
	     for(integer i=0; i<`BHT_SIZE; i=i+1) BHT_registers[i] <= `SD 0;
	     for(integer j=0; j<`PHT_SIZE; j=j+1) PHT_registers[j] <= `SD 2'b01;
	  end
	else
	  begin
	     BHT_registers <= `SD next_BHT_registers;
	     PHT_registers <= `SD next_PHT_registers;
	  end
     end
   
   
endmodule
