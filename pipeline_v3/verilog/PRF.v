/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  PRF.v                                               //
//                                                                     //
//  Description :  This module creates the Physical Register used by   // 
//                 the Pipeline.                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

module PRF(
				  input  [3:0] [1:0] [`PRF_SIZE_LOG-1:0] rda_idx, rdb_idx,	// read index
				  input  [1:0] [`PRF_SIZE_LOG-1:0] wr_idx,			// write index
				  input  [1:0] [63:0] wr_data,					// write data
				  input  [1:0]  wr_en,						// write enable
				  input  clock,							// clock
				  output logic [3:0][1:0][63:0] rda_out, rdb_out,		// read data
				  output write_conflict				  		// for test use
				  );
  
  logic  [`PRF_SIZE-1:0] [63:0] registers;   // 32, 64-bit Registers
  logic	 [`PRF_SIZE-1:0] [63:0] next_registers;

  assign write_conflict = wr_en[0] & wr_en[1] & (wr_idx[0]==wr_idx[1]);

  integer i,j;

  always_comb begin
	for (i=0; i<4; i=i+1) begin
	  for (j=0; j<2; j=j+1) begin
		  //
		  // Read port A
		  //
		  if (rda_idx[i][j] == `PRF_ZERO_REG)
		      rda_out[i][j] = 64'b0;
		  else if (wr_en[0] && (wr_idx[0] == rda_idx[i][j]))
		      rda_out[i][j] = wr_data[0];  // internal forwarding 0
		  else if (wr_en[1] && (wr_idx[1] == rda_idx[i][j]))
		      rda_out[i][j] = wr_data[1];  // internal forwarding 1
		  else
		      rda_out[i][j] = registers[rda_idx[i][j]];

		  //
		  // Read port B
		  //
		  if (rdb_idx[i][j] == `PRF_ZERO_REG)
		      rdb_out[i][j] = 64'b0;
		  else if (wr_en[0] && (wr_idx[0] == rdb_idx[i][j]))
		      rdb_out[i][j] = wr_data[0];  // internal forwarding 0
		  else if (wr_en[1] && (wr_idx[1] == rdb_idx[i][j]))
		      rdb_out[i][j] = wr_data[1];  // internal forwarding 1
		  else
		      rdb_out[i][j] = registers[rdb_idx[i][j]];
	  end
	end
  end

  //
  // Write port
  //
  always_comb begin
	next_registers = registers;
	if (wr_en[0])
		next_registers[wr_idx[0]] = wr_data[0];
	if (wr_en[1])
		next_registers[wr_idx[1]] = wr_data[1];
  end

	 
  always_ff @(posedge clock) 
      registers <= `SD next_registers;




endmodule // PRF
