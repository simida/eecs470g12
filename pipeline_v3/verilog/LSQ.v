module mem_stage(
					input         clock,             // system clock
					input         reset,             // system reset
					input  [63:0] ex_mem_rega,       // regA value from reg file (store data)
					input  [63:0] ex_mem_alu_result, // incoming ALU result from EX
					input  	      ex_mem_rd_mem,
					input  	      ex_mem_wr_mem,
					input  [63:0] Dmem2proc_data,
					input   [3:0] Dmem2proc_tag, Dmem2proc_response,

					output [63:0] mem_result_out,    // outgoing instruction result (to MEM/WB)
					output        mem_stall_out,
					output  [1:0] proc2Dmem_command,
					output [63:0] proc2Dmem_addr,    // Address sent to data-memory
					output [63:0] proc2Dmem_data     // Data sent to data-memory
                );

  logic [3:0] mem_waiting_tag;


   // Determine the command that must be sent to mem
  assign proc2Dmem_command =
    (mem_waiting_tag!=0) ? `BUS_NONE
                         : ex_mem_wr_mem ? `BUS_STORE 
                                         : ex_mem_rd_mem ? `BUS_LOAD
                                                         : `BUS_NONE;

   // The memory address is calculated by the ALU
  assign proc2Dmem_data = ex_mem_rega;

  assign proc2Dmem_addr = ex_mem_alu_result;

   // Assign the result-out for next stage
  assign mem_result_out = (ex_mem_rd_mem) ? Dmem2proc_data : ex_mem_alu_result;

  assign mem_stall_out = 
    (ex_mem_rd_mem & ((mem_waiting_tag!=Dmem2proc_tag) | (Dmem2proc_tag==0))) |
    (ex_mem_wr_mem & (Dmem2proc_response==0));

  wire write_enable = ex_mem_rd_mem & 
    ((mem_waiting_tag==0) | (mem_waiting_tag==Dmem2proc_tag));

  always_ff @(posedge clock)
    if(reset)
      mem_waiting_tag <= `SD 0;
    else if(write_enable)
      mem_waiting_tag <= `SD Dmem2proc_response;

endmodule // module mem_stage

module LSQ( 			      
			      //////////
			      // Inputs
			      //////////
			      
			      input 				        reset,            // Syetem Reset
			      input 				        clock,            // System Clock
			      
			      // MEM SIGNALS
			      input  [63:0] 				Dmem2proc_data,
			      input   [3:0] 				Dmem2proc_tag,
			      input   [3:0] 				Dmem2proc_response,

			      // CONTROL SIGNALS
			      
			      input     			        branch_mis,           // Branch Misprediction from ROB.
			      input [`BRAT_SIZE:0]    			branch_mis_brat,      // BRAT number of branch misprediction
			      input [1:0]			        issue_store,          // Read Valid List to see who can be executed
			      input [1:0]			        LSQ_in_valid,         // IF there is one/two Instructions dispatched
 			      input [1:0]				LSQ_in_complete,      // IF there is one/two Instructions dispatched				
			      input                                     LSQ_in_not_erase,     // Signal from LSQ Arbiter. It tells each LSQ how many entries will they actually issue
			      
			      // DISPATCHED INSTRUCTION INFORMATION			      
		      	      input logic [1:0] [63:0]			LSQ_in_NPC,
		      	      input logic [1:0] [31:0] 			LSQ_in_IR,
			      input [1:0] [`PRF_SIZE_LOG-1:0] 	        LSQ_in_dest,          // Destination PRF Number for Dispatched INS
			      input [1:0] [`ROB_SIZE_LOG-1:0] 	        LSQ_in_ROB,           // ROB Number for Dispatched INS
			      input [1:0] [`BRAT_SIZE_LOG:0] 	        LSQ_in_BRAT,           // BRAT Number for Dispatched INS

			      // DISPATCHED INSTRUCTION INFORMATION			      
			      input [1:0] [63:0] 			LSQ_in_address,       // ALU FUNCTION for Dispatched INS   
			      input [1:0] [63:0] 			LSQ_in_store_value,   // reg#/intermediate for REGA of Dispatched INS.
			      input [1:0] [`ROB_SIZE_LOG-1:0] 	        LSQ_in_complete_ROB,  // ROB Number for Dispatched INS
			      input [`ROB_SIZE_LOG-1:0]			ROB_head,

			      // Early Branch Resolution
	       		      input [1:0]					BRAT_CMT_v,	
	       		      input [1:0][`BRAT_SIZE_LOG-1:0]			BRAT_CMT_num,
			      
			      //////////
			      //Outputs
			      //////////
			      
			      // Outputs for FU
			      
			      output logic		   			LSQ_valid_out,
		      	      output logic [63:0]				LSQ_NPC_out,
		      	      output logic [31:0] 				LSQ_IR_out,
		      	      output logic [`PRF_SIZE_LOG-1:0]  		LSQ_dest_out,
		      	      output logic [63:0] 				LSQ_result_out,
		      	      output logic [`ROB_SIZE_LOG-1:0]    		LSQ_ROB_out,
		      	      output logic [`BRAT_SIZE_LOG:0]    		LSQ_BRAT_out,

			      // Outputs for front End			      
			      output [1:0] 			        	LSQ_full,

			      // Outputs for memory
			      output  [1:0] 					proc2Dmem_command,
			      output [63:0] 					proc2Dmem_addr,    // Address sent to data-memory
			      output [63:0] 					proc2Dmem_data     // Data sent to data-memory
			      
			      );
   
   
   parameter log_LSQ_entry_num = 3;
   parameter LSQ_entry_num = 8;
   
   // Head/Tail for determining next dispatch/issue priority
   
   logic  [log_LSQ_entry_num-1:0] 			head, tail;
   logic [log_LSQ_entry_num-1:0] 			next_head, next_tail;
   logic [log_LSQ_entry_num-1:0] 			next_tail_brat;

   // All Values stored in LSQ
   
   logic [LSQ_entry_num-1:0] [63:0] 				NPC;
   logic [LSQ_entry_num-1:0] [31:0] 				IR;
   logic [LSQ_entry_num-1:0] [63:0] 				address;        //contains RegA#/opa_sel
   logic [LSQ_entry_num-1:0] [63:0] 				value;        //contains RegB#/opb_sel
   logic [LSQ_entry_num-1:0] [`PRF_SIZE_LOG-1:0] 		dest;        
   logic [LSQ_entry_num-1:0] [`ROB_SIZE_LOG-1:0] 		ROB;
   logic [LSQ_entry_num-1:0] [`BRAT_SIZE_LOG:0] 		BRAT;
   logic [LSQ_entry_num-1:0] 					in_use;            
   logic [LSQ_entry_num-1:0] 					complete;  

   // Wires for Issue/Dispatch logics

   logic [63:0] 		LSQ_address_out;
   logic [63:0] 		LSQ_value_out;
   logic [`BRAT_SIZE_LOG:0]    	LSQ_BRAT_out_tmp;
   logic [`ROB_SIZE_LOG-1:0] 	ROB_issue;


   logic [63:0] 	mem_result_out;    // outgoing instruction result (to MEM/WB)
   logic        	mem_stall_out;
   logic [1:0] 		proc2Dmem_command_tmp;

  logic [63:0] 			CMP_result;
  logic 			CMP_valid;
  logic 			CMP_stall;
  logic 			LSQ_issue_out;
  logic 			LSQ_rd_mem;
  logic 			LSQ_wr_mem;

   
  assign LSQ_full[0] = in_use[head-2'h2];
  assign LSQ_full[1] = in_use[head-2'h1];

  assign ROB_issue = (issue_store==2'b10) ? ROB_head+1'b1 : ROB_head;


  // First in First out 
  always_comb begin
	if (branch_mis) begin
		if (tail==head && in_use[head]) begin
			if (!branch_mis_brat[BRAT[head]]) begin
				next_tail = head + 1'b1;
				for (int i=0; i<LSQ_entry_num; i++)
					if (!branch_mis_brat[BRAT[i]] && in_use[i]) 
						if ( i==next_tail | i<head)
							next_tail = 1'b1 + i;
//				for (; in_use[next_tail] && !branch_mis_brat[BRAT[next_tail]];next_tail=next_tail+1'b1) ;
			end
			else begin
				next_tail = head;
			end
		end
		else begin
			next_tail = head;
			for (int i=0; i<LSQ_entry_num; i++)
				if (!branch_mis_brat[BRAT[i]] && in_use[i]) 
					if ( i==next_tail | i<head)
						next_tail = 1'b1 + i;
		
//			for (next_tail=head; in_use[next_tail] && !branch_mis_brat[BRAT[next_tail]];next_tail=next_tail+1'b1) ;
		end
	end
	else if (LSQ_in_valid == 2'b11)
		next_tail = tail + 2;
	else if (LSQ_in_valid == 2'b10 || LSQ_in_valid == 2'b01)
		next_tail = tail + 1;
	else
		next_tail = tail;

	if (LSQ_valid_out && !LSQ_in_not_erase)
		next_head = head + 1;
	else 
		next_head = head;
  end

   always_ff @(posedge clock) begin
	if (reset) begin
		head <= `SD 3'b000;
		tail <= `SD 3'b000;
	end
	else begin
			head <= `SD next_head;
			tail <= `SD next_tail; 
	end
   end // always @ (posedge clock


  // ISSUE Load/Store 
  always_ff @(posedge clock) begin
	if (reset) begin
		LSQ_issue_out		<= `SD  0;
		LSQ_address_out 	<= `SD  0;
		LSQ_value_out 		<= `SD  0;
		LSQ_IR_out		<= `SD  `NOOP_INST;
		LSQ_NPC_out		<= `SD  0;
		LSQ_dest_out		<= `SD  0;
		LSQ_ROB_out	        <= `SD  0;
		LSQ_BRAT_out_tmp        <= `SD  0;
		LSQ_rd_mem		<= `SD  0;
		LSQ_wr_mem		<= `SD  0;
	end
	else if (LSQ_in_not_erase) begin
		LSQ_issue_out		<= `SD  0;
		LSQ_address_out 	<= `SD  0;
		LSQ_value_out 		<= `SD  0;
		LSQ_IR_out		<= `SD  IR[next_head];
		LSQ_NPC_out		<= `SD  NPC[next_head];
		LSQ_dest_out		<= `SD  dest[next_head];
		LSQ_ROB_out	        <= `SD  ROB[next_head];
		LSQ_BRAT_out_tmp        <= `SD  (BRAT_CMT_v[0] && BRAT[next_head]==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
					(BRAT_CMT_v[1] &&BRAT[next_head]==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : BRAT[next_head];
		LSQ_rd_mem		<= `SD  0;
		LSQ_wr_mem		<= `SD  0;
	end
	else if (|issue_store && complete[next_head] && ROB[next_head] == ROB_issue) begin
		LSQ_issue_out		<= `SD  1;
		LSQ_address_out 	<= `SD  address[next_head];
		LSQ_value_out 		<= `SD  value[next_head];
		LSQ_IR_out		<= `SD  IR[next_head];
		LSQ_NPC_out		<= `SD  NPC[next_head];
		LSQ_dest_out		<= `SD  dest[next_head];
		LSQ_ROB_out	        <= `SD  ROB[next_head];

		LSQ_BRAT_out_tmp        <= `SD  (BRAT_CMT_v[0] && BRAT[next_head]==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
					(BRAT_CMT_v[1] &&BRAT[next_head]==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : BRAT[next_head];

		LSQ_rd_mem		<= `SD  (IR[next_head][31:26]==`LDQ_INST || IR[next_head][31:26]==`LDQ_L_INST);
		LSQ_wr_mem		<= `SD  (IR[next_head][31:26]==`STQ_INST || IR[next_head][31:26]==`STQ_C_INST);
	end
	else begin 
		LSQ_issue_out		<= `SD  0;
		LSQ_address_out 	<= `SD  0;
		LSQ_value_out 		<= `SD  0;
		LSQ_IR_out		<= `SD  `NOOP_INST;
		LSQ_NPC_out		<= `SD  0;
		LSQ_dest_out		<= `SD  0;
		LSQ_ROB_out	        <= `SD  0;
		LSQ_BRAT_out_tmp        <= `SD  0;
		LSQ_rd_mem		<= `SD  0;
		LSQ_wr_mem		<= `SD  0;

	end
  end


  always_ff @(posedge clock) begin
	if (reset) begin
		CMP_result 	<= `SD 0;
		CMP_valid 	<= `SD 0;
		CMP_stall 	<= `SD 0;
	end
	else begin
		CMP_result 	<= `SD LSQ_result_out;
		CMP_valid 	<= `SD LSQ_valid_out;
		CMP_stall 	<= `SD LSQ_valid_out && LSQ_in_not_erase;
	end
  end

  assign LSQ_BRAT_out		= (BRAT_CMT_v[0] && LSQ_BRAT_out_tmp==BRAT_CMT_num[0]) ? {1'b1,`BRAT_SIZE_LOG'b0} :
					(BRAT_CMT_v[1] && LSQ_BRAT_out_tmp==BRAT_CMT_num[1]) ? {1'b1,`BRAT_SIZE_LOG'b0} : LSQ_BRAT_out_tmp;

  assign LSQ_result_out 	= CMP_stall ? CMP_result : mem_result_out;
  assign LSQ_valid_out 		= CMP_stall ? CMP_valid  : LSQ_issue_out && !mem_stall_out;	
  assign proc2Dmem_command 	= proc2Dmem_command_tmp;

   // ALLOCATE LSQ entries and 
   always_ff @(posedge clock) begin
	if (reset) begin
		for (int i=0; i<LSQ_entry_num; i++) begin
			NPC[i] <= `SD 64'b0;
			IR[i] <= `SD `NOOP_INST;
			value[i] <= `SD 6'b0;
			address[i] <= `SD 6'b0;
			dest[i] <= `SD 6'b0;
			ROB[i] <= `SD 1'b0;
			BRAT[i] <= `SD 1'b0;
			in_use[i] <= `SD 1'b0;	
			complete[i] <= `SD 1'b0;
		end
	end
	else begin
		if (branch_mis) begin
			for (int i=0; i<LSQ_entry_num; i++) 
				if (in_use[i] && branch_mis_brat[BRAT[i]]) begin
					in_use[i] <= `SD 1'b0;	
					complete[i] <= `SD 1'b0;
				end
		end
		else if (LSQ_in_valid == 2'b11) begin
			NPC  [tail] 	<= `SD LSQ_in_NPC[0];
			IR   [tail] 	<= `SD LSQ_in_IR[0];
			dest [tail] 	<= `SD LSQ_in_dest[0];
			ROB  [tail] 	<= `SD LSQ_in_ROB[0];
			BRAT [tail] 	<= `SD LSQ_in_BRAT[0];
			in_use [tail]	<= `SD 1'b1;

			NPC  [tail+1'b1] 	<= `SD LSQ_in_NPC[1];
			IR   [tail+1'b1] 	<= `SD LSQ_in_IR[1];
			dest [tail+1'b1] 	<= `SD LSQ_in_dest[1];
			ROB  [tail+1'b1] 	<= `SD LSQ_in_ROB[1];
			BRAT [tail+1'b1] 	<= `SD LSQ_in_BRAT[1];
			in_use [tail+1'b1]	<= `SD 1'b1;
		end	
		else if (LSQ_in_valid == 2'b01) begin
			NPC  [tail] 	<= `SD LSQ_in_NPC[0];
			IR   [tail] 	<= `SD LSQ_in_IR[0];
			dest [tail] 	<= `SD LSQ_in_dest[0];
			ROB  [tail] 	<= `SD LSQ_in_ROB[0];
			BRAT [tail] 	<= `SD LSQ_in_BRAT[0];
			in_use [tail]	<= `SD 1'b1;
		end
		else if (LSQ_in_valid == 2'b10) begin
			NPC  [tail] 	<= `SD LSQ_in_NPC[1];
			IR   [tail] 	<= `SD LSQ_in_IR[1];
			dest [tail] 	<= `SD LSQ_in_dest[1];
			ROB  [tail] 	<= `SD LSQ_in_ROB[1];
			BRAT [tail] 	<= `SD LSQ_in_BRAT[1];
			in_use [tail]	<= `SD 1'b1;
		end

		if (LSQ_in_complete[0]) begin
			for (int i=0; i<LSQ_entry_num;i++) begin
				if (in_use[i]&& ROB[i] == LSQ_in_complete_ROB[0]) begin
					complete[i] <= `SD 1'b1;
					value[i]    <= `SD LSQ_in_store_value[0];
					address[i]  <= `SD LSQ_in_address[0];
				end				
			end
		end

		if (LSQ_in_complete[1]) begin
			for (int i=0; i<LSQ_entry_num;i++) begin
				if (in_use[i]&& ROB[i] == LSQ_in_complete_ROB[1]) begin
					complete[i] <= `SD 1'b1;
					value[i]    <= `SD LSQ_in_store_value[1];
					address[i]  <= `SD LSQ_in_address[1];
				end				
			end
		end

		if (BRAT_CMT_v[0]) begin
			for (int i=0; i<LSQ_entry_num;i++) begin
				if (in_use[i]&& BRAT[i] == BRAT_CMT_num[0]) begin
					BRAT[i] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};
				end				
			end
		end

		if (BRAT_CMT_v[1]) begin
			for (int i=0; i<LSQ_entry_num;i++) begin
				if (in_use[i]&& BRAT[i] == BRAT_CMT_num[1]) begin
					BRAT[i] <= `SD {1'b1,`BRAT_SIZE_LOG'b0};
				end				
			end
		end

		if (LSQ_valid_out && !LSQ_in_not_erase) begin
			in_use[head] <= `SD 1'b0;	
			complete[head] <= `SD 1'b0;
		end

	end
   end // always @ (posedge clock


   mem_stage mem0 ( 			.clock(clock),             
					.reset(reset),             
					.ex_mem_rega(LSQ_value_out),       
					.ex_mem_alu_result(LSQ_address_out), 
					.ex_mem_wr_mem(LSQ_wr_mem),
					.ex_mem_rd_mem(LSQ_rd_mem),
					.Dmem2proc_data(Dmem2proc_data),
					.Dmem2proc_tag(Dmem2proc_tag),
 					.Dmem2proc_response(Dmem2proc_response),

					.mem_result_out(mem_result_out),    // outgoing instruction result (to MEM/WB)
					.mem_stall_out(mem_stall_out),
					.proc2Dmem_command(proc2Dmem_command_tmp),
					.proc2Dmem_addr(proc2Dmem_addr),    // Address sent to data-memory
					.proc2Dmem_data(proc2Dmem_data)     // Data sent to data-memory
		
	    ); 




endmodule // reservation_station_8

