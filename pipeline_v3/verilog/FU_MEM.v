//////////////////////////////////////////////////////////////////////////
//                                                                      //
//   Modulename :  FU_MEM.v                                             //
//                                                                      //
//  Description :  instruction execute (EX) stage of the pipeline;      //
//                 given the instruction command code CMD, select the   //
//                 proper input A and B for the ALU, compute the result,// 
//                 and compute the condition for branches, and pass all //
//                 the results down the pipeline. MWB                   // 
//                                                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////



//
// The ALU
//
// given the command code CMD and proper operands A and B, compute the
// result of the instruction
//
// This module is purely combinational
//
module MEM_alu(
           input [63:0] opa,
           input [63:0] opb,
           input  [4:0] func,
           
           output logic [63:0] result
          );

    // This function computes a signed less-than operation
  function signed_lt;
    input [63:0] a, b;
    
    if (a[63] == b[63]) 
      signed_lt = (a < b); // signs match: signed compare same as unsigned
    else
      signed_lt = a[63];   // signs differ: a is smaller if neg, larger if pos
  endfunction

  always_comb
  begin
    case (func)
      `ALU_ADDQ:   result = opa + opb;
      `ALU_SUBQ:   result = opa - opb;
      `ALU_AND:    result = opa & opb;
      `ALU_BIC:    result = opa & ~opb;
      `ALU_BIS:    result = opa | opb;
      `ALU_ORNOT:  result = opa | ~opb;
      `ALU_XOR:    result = opa ^ opb;
      `ALU_EQV:    result = opa ^ ~opb;
      `ALU_SRL:    result = opa >> opb[5:0];
      `ALU_SLL:    result = opa << opb[5:0];
      `ALU_SRA:    result = (opa >> opb[5:0]) | ({64{opa[63]}} << (64 -
                             opb[5:0])); // arithmetic from logical shift
      `ALU_CMPULT: result = { 63'd0, (opa < opb) };
      `ALU_CMPEQ:  result = { 63'd0, (opa == opb) };
      `ALU_CMPULE: result = { 63'd0, (opa <= opb) };
      `ALU_CMPLT:  result = { 63'd0, signed_lt(opa, opb) };
      `ALU_CMPLE:  result = { 63'd0, (signed_lt(opa, opb) || (opa == opb)) };
      default:     result = 64'hdeadbeefbaadbeef; // here only to force
                                                  // a combinational solution
                                                  // a casex would be better
    endcase
  end
endmodule // alu

module FU_MEM(
	      input         clock,               // system clock
	      input         reset,               // system reset
	      
	      input 		   			MEM_valid,
	      input					MEM_stall,
	      input [63:0] 	   			MEM_NPC,
	      input [31:0] 	   			MEM_IR,
	      input [`ALU_FUNC_NUM_LOG-1:0] 		MEM_alu_func,
	      input [63:0] 	   			MEM_regA,
	      input [63:0] 	   			MEM_regB,
	      input [1:0]		   		MEM_opa_sel,
	      input [1:0]		   		MEM_opb_sel,
	      input [`PRF_SIZE_LOG-1:0] 	   	MEM_dest,
	      input [`ROB_SIZE_LOG-1:0] 	   	MEM_ROB,
	      input [`BRAT_SIZE_LOG:0] 			MEM_BRAT,
	      
	      output [63:0]				FU_MEM_NPC_out,
	      output [31:0]				FU_MEM_IR_out,
	      output [`PRF_SIZE_LOG-1:0] 		FU_MEM_dest_out,
	      output [`ROB_SIZE_LOG-1:0]		FU_MEM_ROB_out,  
	      output [63:0]      			FU_MEM_result_out, 
	      output [63:0]				FU_MEM_value_out,
	      output [`BRAT_SIZE_LOG:0]	        	FU_MEM_BRAT_out,
	      output  					FU_MEM_valid_out
	      
              );
   

  logic  [63:0] opa_mux_out, opb_mux_out;
  logic         brcond_result;
  logic		mem_stall_out;
   
  wire [63:0] alu_result;


   // set up possible immediates:
   //   mem_disp: sign-extended 16-bit immediate for memory format
   //   br_disp: sign-extended 21-bit immediate * 4 for branch displacement
   //   alu_imm: zero-extended 8-bit immediate for ALU ops
  wire [63:0] mem_disp = { {48{MEM_IR[15]}}, MEM_IR[15:0] };
  wire [63:0] br_disp  = { {41{MEM_IR[20]}}, MEM_IR[20:0], 2'b00 };
  wire [63:0] alu_imm  = { 56'b0, MEM_IR[20:13] };
   
   //
   // ALU opA mux
   //
  always_comb
  begin
    case (MEM_opa_sel)
      `ALU_OPA_IS_REGA:     opa_mux_out = MEM_regA;
      `ALU_OPA_IS_MEM_DISP: opa_mux_out = mem_disp;
      `ALU_OPA_IS_NPC:      opa_mux_out = MEM_NPC;
      `ALU_OPA_IS_NOT3:     opa_mux_out = ~64'h3;
    endcase
  end

   //
   // ALU opB mux
   //
  always_comb
  begin
     // Default value, Set only because the case isnt full.  If you see this
     // value on the output of the mux you have an invalid opb_select
    opb_mux_out = 64'hbaadbeefdeadbeef;
    case (MEM_opb_sel)
      `ALU_OPB_IS_REGB:    opb_mux_out = MEM_regB;
      `ALU_OPB_IS_ALU_IMM: opb_mux_out = alu_imm;
      `ALU_OPB_IS_BR_DISP: opb_mux_out = br_disp;
    endcase 
  end

   //
   // instantiate the ALU
   //
  MEM_alu alu_0 (// Inputs
             .opa(opa_mux_out),
             .opb(opb_mux_out),
             .func(MEM_alu_func),

             // Output
             .result(alu_result)
            );



  wire [63:0] mem_result_out;


  assign FU_MEM_NPC_out 	= MEM_NPC;
  assign FU_MEM_IR_out 		= MEM_IR;
  assign FU_MEM_dest_out 	= MEM_dest;
  assign FU_MEM_ROB_out 	= MEM_ROB;
  assign FU_MEM_result_out 	= alu_result;
  assign FU_MEM_value_out 	= MEM_regA;	
  assign FU_MEM_valid_out 	= MEM_valid;
   assign FU_MEM_BRAT_out       = MEM_BRAT;
   


endmodule // module ex_stage




