`timescale 1ns/100ps

module testbench;
				reg         clock;               // system clock
				reg         reset;               // system reset
				reg 		   			ALU_valid;
				reg [63:0] 	   			ALU_NPC;
				reg [31:0] 	   			ALU_IR;
				reg [`ALU_FUNC_NUM_LOG-1:0] 		ALU_alu_func;
				reg [`PRF_SIZE-1:0] 	   		ALU_regA;
				reg [`PRF_SIZE-1:0] 	   		ALU_regB;
				reg [1:0]		   		ALU_opa_sel;
				reg [1:0]		   		ALU_opb_sel;
				reg [`PRF_SIZE_LOG-1:0] 	   	ALU_dest;
				reg [`ROB_SIZE_LOG-1:0] 	   	ALU_ROB;

				wire [63:0] 				FU_ALU_NPC_out;
				wire [31:0]				FU_ALU_IR_out;
				wire [`PRF_SIZE_LOG-1:0] 		FU_ALU_dest_out;
				wire [`ROB_SIZE_LOG-1:0]		FU_ALU_ROB_out;
				wire [63:0] 				FU_ALU_result_out;   // ALU result
				wire 					FU_ALU_valid_out;

				reg 		   			BR_valid;
				reg [63:0] 	   			BR_NPC;
				reg [31:0] 	   			BR_IR;
				reg [`ALU_FUNC_NUM_LOG-1:0] 		BR_alu_func;
				reg [`PRF_SIZE-1:0] 	   		BR_regA;
				reg [`PRF_SIZE-1:0] 	   		BR_regB;
				reg [1:0]		   		BR_opa_sel;
				reg [1:0]		   		BR_opb_sel;
				reg [`PRF_SIZE_LOG-1:0] 	   	BR_dest;
				reg [`ROB_SIZE_LOG-1:0] 	   	BR_ROB;

				wire [63:0] 				FU_BR_NPC_out;
				wire [31:0]				FU_BR_IR_out;
				wire [`PRF_SIZE_LOG-1:0] 		FU_BR_dest_out;
				wire [`ROB_SIZE_LOG-1:0]		FU_BR_ROB_out;
				wire [63:0] 				FU_BR_result_out;   // ALU result
				wire        				FU_BR_branch_out;  // is this a taken branch?
				wire 					FU_BR_valid_out;

				reg 		   			MULT_valid;
				reg					MULT_stall;
				reg [63:0] 	   			MULT_NPC;
				reg [31:0] 	   			MULT_IR;
				reg [`ALU_FUNC_NUM_LOG-1:0] 		MULT_alu_func;
				reg [`PRF_SIZE-1:0] 	   		MULT_regA;
				reg [`PRF_SIZE-1:0] 	   		MULT_regB;
				reg [1:0]		   		MULT_opa_sel;
				reg [1:0]		   		MULT_opb_sel;
				reg [`PRF_SIZE_LOG-1:0] 	   	MULT_dest;
				reg [`ROB_SIZE_LOG-1:0] 	   	MULT_ROB;

				wire [63:0] 				FU_MULT_NPC_out;
				wire [31:0]				FU_MULT_IR_out;
				wire [`PRF_SIZE_LOG-1:0] 		FU_MULT_dest_out;
				wire [`ROB_SIZE_LOG-1:0]		FU_MULT_ROB_out;
				wire [63:0] 				FU_MULT_result_out;  // is this a taken branch?
				wire 					FU_MULT_valid_out;
				wire 					FU_MULT_stall_out;

				reg  [63:0] 				Dmem2proc_data;
				reg   [3:0] 				Dmem2proc_tag;
				reg   [3:0] 				Dmem2proc_response;
				reg 		   			MEM_valid;
				reg					MEM_stall;
				reg [63:0] 	   			MEM_NPC;
				reg [31:0] 	   			MEM_IR;
				reg [`ALU_FUNC_NUM_LOG-1:0] 		MEM_alu_func;
				reg [`PRF_SIZE-1:0] 	   		MEM_regA;
				reg [`PRF_SIZE-1:0] 	   		MEM_regB;
				reg [1:0]		   		MEM_opa_sel;
				reg [1:0]		   		MEM_opb_sel;
				reg [`PRF_SIZE_LOG-1:0] 	   	MEM_dest;
				reg [`ROB_SIZE_LOG-1:0] 	   	MEM_ROB;

				wire [63:0] 				FU_MEM_NPC_out;
				wire [31:0]				FU_MEM_IR_out;
				wire [`PRF_SIZE_LOG-1:0] 		FU_MEM_dest_out;
				wire [`ROB_SIZE_LOG-1:0]		FU_MEM_ROB_out;
				wire [63:0] 				FU_MEM_result_out;  // is this a taken branch?
				wire 					FU_MEM_valid_out;
				wire 					FU_MEM_stall_out;
				wire  [1:0] proc2Dmem_command;
				wire [63:0] proc2Dmem_addr;    // Address sent to data-memory
				wire [63:0] proc2Dmem_data;     // Data sent to data-memory




	always
	  #5 clock=~clock;

	FU_ALU ALU(
			clock,               // system clock
			reset,               // system reset
			ALU_valid,
			ALU_NPC,
			ALU_IR,
			ALU_alu_func,
			ALU_regA,
			ALU_regB,
			ALU_opa_sel,
			ALU_opb_sel,
			ALU_dest,
			ALU_ROB,

			FU_ALU_NPC_out,
			FU_ALU_IR_out,
			FU_ALU_dest_out,
			FU_ALU_ROB_out,
			FU_ALU_result_out,   // ALU result
			FU_ALU_valid_out	
               );

	 FU_BR BR(
			clock,               // system clock
			reset,               // system reset
			BR_valid,
			BR_NPC,
			BR_IR,
			BR_alu_func,
			BR_regA,
			BR_regB,
			BR_opa_sel,
			BR_opb_sel,
			BR_dest,
			BR_ROB,

			FU_BR_NPC_out,
			FU_BR_IR_out,
			FU_BR_dest_out,
			FU_BR_ROB_out,
			FU_BR_result_out,   // ALU result
			FU_BR_branch_out,  // is this a taken branch?
			FU_BR_valid_out

               );

	FU_MULT MULT(
			clock,               // system clock
			reset,               // system reset
			MULT_valid,
			MULT_stall,
			MULT_NPC,
			MULT_IR,
			MULT_alu_func,
			MULT_regA,
			MULT_regB,
			MULT_opa_sel,
			MULT_opb_sel,
			MULT_dest,
			MULT_ROB,

			FU_MULT_NPC_out,
			FU_MULT_IR_out,
			FU_MULT_dest_out,
			FU_MULT_ROB_out,
			FU_MULT_result_out,  // is this a taken branch?
			FU_MULT_valid_out,
			FU_MULT_stall_out

               );
	
	FU_MEM MEM (
			clock,               // system clock
			reset,               // system reset
			Dmem2proc_data,
			Dmem2proc_tag,
			Dmem2proc_response,
			MEM_valid,
			MEM_stall,
			MEM_NPC,
			MEM_IR,
			MEM_alu_func,
			MEM_regA,
			MEM_regB,
			MEM_opa_sel,
			MEM_opb_sel,
			MEM_dest,
			MEM_ROB,
			FU_MEM_NPC_out,
			FU_MEM_IR_out,
			FU_MEM_dest_out,
			FU_MEM_ROB_out,  
			FU_MEM_result_out, 
			FU_MEM_valid_out,
			FU_MEM_stall_out, 
			proc2Dmem_command,
			proc2Dmem_addr,    // Address sent to data-memory
			proc2Dmem_data     // Data sent to data-memory

               );


	integer i;

	initial 
	begin
		i = 0;
		clock = 0;
		reset = 1;
		ALU_valid = 0;
		BR_valid = 0;
		for (i=0; i< 32; i++) begin
			@(negedge clock)
			reset = 0;
			ALU_NPC = i*4;
			ALU_IR = 0;
			ALU_alu_func = `ALU_ADDQ;
			ALU_regA = i; 
			ALU_regB = i+1;
			ALU_opa_sel = 0;
			ALU_opb_sel= 0;
			ALU_dest = 32 + i;
			ALU_ROB = i;
			BR_NPC = i*4;
			BR_IR = 32'h4;
			BR_alu_func = `ALU_ADDQ;
			BR_regA = 0;
			BR_regB = 0;
			BR_opa_sel = `ALU_OPA_IS_NPC;
			BR_opb_sel = `ALU_OPB_IS_BR_DISP;
			BR_dest = 32 + i;
			BR_ROB = i;
			MULT_valid = (i % 4 != 1);
			MULT_stall = (i % 4 == 2);
			MULT_NPC = i*4;
			MULT_IR = 0;
			MULT_alu_func=`ALU_MULQ;
			MULT_regA = i;
			MULT_regB = i+1;
			MULT_opa_sel = 0;
			MULT_opb_sel = 0;
			MULT_dest = 32 + i;
			MULT_ROB = i;
		end
		$finish;
 	end // initial
endmodule
