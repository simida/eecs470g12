/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  Execute_Stage.v                                     //
//                                                                     //
//  Description :  This module is the Execute Stage (with ppl regs) of // 
//                 the Pipeline.                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

module Execute_Stage (
		      //////////
		      // Inputs
		      //////////  
		      input clock,
		      input reset,
		      input branch_mis,
		      
		      // Inputs from IS		      
		      input [3:0] [1:0] 	                	IS_EX_valid,
		      input [3:0] [1:0] [63:0]				IS_EX_NPC,
		      input [3:0] [1:0] [31:0] 				IS_EX_IR,
		      input [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	IS_EX_alu_func,
		      input [3:0] [1:0] [63:0] 				IS_EX_regA_value,
		      input [3:0] [1:0] [63:0] 				IS_EX_regB_value,
		      input [3:0] [1:0] [1:0] 		    		IS_EX_opa_sel,
		      input [3:0] [1:0] [1:0] 				IS_EX_opb_sel,
		      input [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 		IS_EX_dest,
		      input [3:0] [1:0] [`ROB_SIZE_LOG-1:0]     	IS_EX_ROB,

		      // Inputs from Dmem
		      input  [63:0] Dmem2proc_data,
		      input   [3:0] Dmem2proc_tag,
		      input   [3:0] Dmem2proc_response,

		      ///////////
		      // Outputs
		      ///////////
		      
		      // Outputs for IS
		      output [3:0] [1:0] 				EX_IS_stall,

		      // Outputs for CMP
		      output logic [1:0] 	                	EX_CMP_valid,
		      output logic [1:0] [63:0]				EX_CMP_NPC,
		      output logic [1:0] [31:0] 			EX_CMP_IR,
		      output logic [1:0] [`PRF_SIZE_LOG-1:0]  		EX_CMP_dest,
		      output logic [1:0] [63:0] 			EX_CMP_result,
		      output logic [1:0] [`ROB_SIZE_LOG-1:0]    	EX_CMP_ROB,
		      output logic [1:0] 				EX_CMP_branch,
		      
		      // Outputs for Dmem
		      output  [1:0] 					proc2Dmem_command,
		      output [63:0] 					proc2Dmem_addr,    // Address sent to data-memory
		      output [63:0] 					proc2Dmem_data     // Data sent to data-memory
		      );

   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 		FU_dest_out;
   logic [3:0] [1:0] [63:0] 				FU_NPC_out;
   logic [3:0] [1:0] [31:0] 				FU_IR_out;
   logic [3:0] [1:0] [`ROB_SIZE_LOG-1:0] 		FU_ROB_out;
   logic [1:0] 						FU_branch_out;
   logic [3:0] [1:0][63:0] 				FU_result_out;
   logic [3:0] [1:0] 					FU_valid_out;
   logic [1:0] 						FU_mem_stall_out;
   
   logic [2:0] 						FU_cdb_priority, FU_cdb_priority_next;
   logic [1:0] [3:0] [1:0] 				FU_cdb_choose;
   logic [3:0] [1:0] 					FU_rejected;
   logic [1:0] 						MULT_rejected;
   
   rps_8 cdb_chooser(.req(FU_valid_out),
		     .en(2'b11),
		     .count(FU_cdb_priority),
		     .gnt(FU_cdb_choose)
		     );
   
   assign FU_rejected = FU_valid_out & ~(FU_cdb_choose[0]|FU_cdb_choose[1]);
   assign EX_IS_stall[0] = FU_rejected[0];
   assign EX_IS_stall[1] = FU_rejected[1];
   assign EX_IS_stall[2] = MULT_rejected;
   assign EX_IS_stall[3] = FU_rejected[3] | FU_mem_stall_out;
   assign FU_cdb_priority_next = FU_cdb_priority + 2;
   // Since we only have one MEM now, we disable the second MEM out
   assign FU_mem_stall_out[1] = 1;   
   assign FU_valid_out[3][1] = 0;



   wor [1:0] 						EX_CMP_valid_tmp;
   wor [1:0] [63:0] 					EX_CMP_NPC_tmp;
   wor [1:0] [31:0] 					EX_CMP_IR_tmp;
   wor [1:0] [`PRF_SIZE_LOG-1:0] 			EX_CMP_dest_tmp;
   wor [1:0] [63:0] 					EX_CMP_result_tmp;
   wor [1:0] [`ROB_SIZE_LOG-1:0] 			EX_CMP_ROB_tmp;
   wor [1:0] 						EX_CMP_branch_tmp;

   genvar 						i, j ,k;
   
		      
								    
   generate for(i = 0;i < 2; i ++)
     begin
	for(j = 0; j < 4; j ++)
	  begin
	     for (k = 0; k < 2; k++)
	       begin
		       assign EX_CMP_valid_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_valid_out[j][k] : 0;
		       assign EX_CMP_NPC_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_NPC_out[j][k] : 0;
		       assign EX_CMP_IR_tmp[i] 		= FU_cdb_choose[i][j][k] ?  FU_IR_out[j][k] : 0;
		       assign EX_CMP_dest_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_dest_out[j][k] : 0;
		       assign EX_CMP_result_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_result_out[j][k] : 0;
		       assign EX_CMP_ROB_tmp[i] 	= FU_cdb_choose[i][j][k] ?  FU_ROB_out[j][k] : 0;
		       assign EX_CMP_branch_tmp[i] 	= FU_cdb_choose[i][1][k] ?  FU_branch_out[k] : 0;  
	       end // for (k = 0; k < 2; k++)
	  end // for (j = 0; j < 4; j ++)
     end // for (i = 0;i < 2; i ++)
   endgenerate

   always_ff@(posedge clock)
     begin
	if(reset)
	  begin
	     FU_cdb_priority 	<= `SD 0;
	     EX_CMP_valid 	<= `SD 0;
	     EX_CMP_NPC 	<= `SD 0;
	     EX_CMP_IR 		<= `SD 0;
	     EX_CMP_dest 	<= `SD 0;
	     EX_CMP_result 	<= `SD 0;
	     EX_CMP_ROB		<= `SD 0;
	     EX_CMP_branch	<= `SD 0;
	  end // if (reset)
	else
	  begin
	     FU_cdb_priority 	<= `SD FU_cdb_priority_next;
	     EX_CMP_valid 	<= `SD EX_CMP_valid_tmp;
	     EX_CMP_NPC 	<= `SD EX_CMP_NPC_tmp;
	     EX_CMP_IR 		<= `SD EX_CMP_IR_tmp;
	     EX_CMP_dest 	<= `SD EX_CMP_dest_tmp;
	     EX_CMP_result 	<= `SD EX_CMP_result_tmp;
	     EX_CMP_ROB		<= `SD EX_CMP_ROB_tmp;
	     EX_CMP_branch	<= `SD EX_CMP_branch_tmp;
	  end // if (reset)
     end
   
   FU_ALU ALU [1:0](
		    .clock(clock),               // system clock
		    .reset(reset),               // system reset
		    .ALU_valid(IS_EX_valid[0]),
		    .ALU_NPC(IS_EX_NPC[0]),
		    .ALU_IR(IS_EX_IR[0]),
		    .ALU_alu_func(IS_EX_alu_func[0]),
		    .ALU_regA(IS_EX_regA_value[0]),
		    .ALU_regB(IS_EX_regB_value[0]),
		    .ALU_opa_sel(IS_EX_opa_sel[0]),
		    .ALU_opb_sel(IS_EX_opb_sel[0]),
		    .ALU_dest(IS_EX_dest[0]),
		    .ALU_ROB(IS_EX_ROB[0]),

		    .FU_ALU_NPC_out(FU_NPC_out[0]),
		    .FU_ALU_IR_out(FU_IR_out[0]),
		    .FU_ALU_dest_out(FU_dest_out[0]),
		    .FU_ALU_ROB_out(FU_ROB_out[0]),
		    .FU_ALU_result_out(FU_result_out[0]),  
		    .FU_ALU_valid_out(FU_valid_out[0])
		    );
   
   FU_BR BR [1:0](
		  .clock(clock),               // system clock
		  .reset(reset),               // system reset
		  .BR_valid(IS_EX_valid[1]),
		  .BR_NPC(IS_EX_NPC[1]),
		  .BR_IR(IS_EX_IR[1]),
		  .BR_alu_func(IS_EX_alu_func[1]),
		  .BR_regA(IS_EX_regA_value[1]),
		  .BR_regB(IS_EX_regB_value[1]),
		  .BR_opa_sel(IS_EX_opa_sel[1]),
		  .BR_opb_sel(IS_EX_opb_sel[1]),
		  .BR_dest(IS_EX_dest[1]),
		  .BR_ROB(IS_EX_ROB[1]),

		  .FU_BR_NPC_out(FU_NPC_out[1]),
		  .FU_BR_IR_out(FU_IR_out[1]),
		  .FU_BR_dest_out(FU_dest_out[1]),
		  .FU_BR_ROB_out(FU_ROB_out[1]),
		  .FU_BR_branch_out(FU_branch_out),  // is this a taken branch?
		  .FU_BR_result_out(FU_result_out[1]),   
		  .FU_BR_valid_out(FU_valid_out[1])
		  );
   
   FU_MULT MULT [1:0](
		      .clock(clock),               // system clock
		      .reset(reset),               // system reset
		      .MULT_valid(IS_EX_valid[2]),
		      .MULT_stall(FU_rejected[2]),
		      .MULT_NPC(IS_EX_NPC[2]),
		      .MULT_IR(IS_EX_IR[2]),
		      .MULT_alu_func(IS_EX_alu_func[2]),
		      .MULT_regA(IS_EX_regA_value[2]),
		      .MULT_regB(IS_EX_regB_value[2]),
		      .MULT_opa_sel(IS_EX_opa_sel[2]),
		      .MULT_opb_sel(IS_EX_opb_sel[2]),
		      .MULT_dest(IS_EX_dest[2]),
		      .MULT_ROB(IS_EX_ROB[2]),

		      .FU_MULT_NPC_out(FU_NPC_out[2]),
		      .FU_MULT_IR_out(FU_IR_out[2]),
		      .FU_MULT_dest_out(FU_dest_out[2]),
		      .FU_MULT_ROB_out(FU_ROB_out[2]),
		      .FU_MULT_result_out(FU_result_out[2]),  // is this a taken branch?
		      .FU_MULT_valid_out(FU_valid_out[2])
		      );
   
   FU_MEM MEM(
	      .clock(clock),               // system clock
	      .reset(reset),               // system reset
	      
	      .Dmem2proc_data(Dmem2proc_data),
	      .Dmem2proc_tag(Dmem2proc_tag),
	      .Dmem2proc_response(Dmem2proc_response),
	      
	      .MEM_valid(IS_EX_valid[3][0]),
	      .MEM_stall(FU_rejected[3][0]),
	      .MEM_NPC(IS_EX_NPC[3][0]),
	      .MEM_IR(IS_EX_IR[3][0]),
	      .MEM_alu_func(IS_EX_alu_func[3][0]),
	      .MEM_regA(IS_EX_regA_value[3][0]),
	      .MEM_regB(IS_EX_regB_value[3][0]),
	      .MEM_opa_sel(IS_EX_opa_sel[3][0]),
	      .MEM_opb_sel(IS_EX_opb_sel[3][0]),
	      .MEM_dest(IS_EX_dest[3][0]),
	      .MEM_ROB(IS_EX_ROB[3][0]),


	      .FU_MEM_NPC_out(FU_NPC_out[3][0]),
	      .FU_MEM_IR_out(FU_IR_out[3][0]),
	      .FU_MEM_dest_out(FU_dest_out[3][0]),
	      .FU_MEM_ROB_out(FU_ROB_out[3][0]),  
	      .FU_MEM_result_out(FU_result_out[3][0]), 
	      .FU_MEM_valid_out(FU_valid_out[3][0]),
	      .FU_MEM_stall_out(FU_mem_stall_out[0]),
	      
	      .proc2Dmem_command(proc2Dmem_command),
	      .proc2Dmem_addr(proc2Dmem_addr),    // Address sent to data-memory
	      .proc2Dmem_data(proc2Dmem_data)     // Data sent to data-memory
              );

endmodule

