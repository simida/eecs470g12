//////////////////////////////////////////////////////////////////////////
//                                                                      //
//   Modulename :  FU_ALU.v                                             //
//                                                                      //
//  Description :  instruction execute (EX) stage of the pipeline;      //
//                 given the instruction command code CMD, select the   //
//                 proper input A and B for the ALU, compute the result,// 
//                 and compute the condition for branches, and pass all //
//                 the results down the pipeline. MWB                   // 
//                                                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

//
// The ALU
//
// given the command code CMD and proper operands A and B, compute the
// result of the instruction
//
// This module is purely combinational
//
module ALU_alu(
           input [63:0] opa,
           input [63:0] opb,
           input  [4:0] func,
           
           output logic [63:0] result
          );

    // This function computes a signed less-than operation
  function signed_lt;
    input [63:0] a, b;
    
    if (a[63] == b[63]) 
      signed_lt = (a < b); // signs match: signed compare same as unsigned
    else
      signed_lt = a[63];   // signs differ: a is smaller if neg, larger if pos
  endfunction

  always_comb
  begin
    case (func)
      `ALU_ADDQ:   result = opa + opb;
      `ALU_SUBQ:   result = opa - opb;
      `ALU_AND:    result = opa & opb;
      `ALU_BIC:    result = opa & ~opb;
      `ALU_BIS:    result = opa | opb;
      `ALU_ORNOT:  result = opa | ~opb;
      `ALU_XOR:    result = opa ^ opb;
      `ALU_EQV:    result = opa ^ ~opb;
      `ALU_SRL:    result = opa >> opb[5:0];
      `ALU_SLL:    result = opa << opb[5:0];
      `ALU_SRA:    result = (opa >> opb[5:0]) | ({64{opa[63]}} << (64 -
                             opb[5:0])); // arithmetic from logical shift
      `ALU_CMPULT: result = { 63'd0, (opa < opb) };
      `ALU_CMPEQ:  result = { 63'd0, (opa == opb) };
      `ALU_CMPULE: result = { 63'd0, (opa <= opb) };
      `ALU_CMPLT:  result = { 63'd0, signed_lt(opa, opb) };
      `ALU_CMPLE:  result = { 63'd0, (signed_lt(opa, opb) || (opa == opb)) };
      default:     result = 64'hdeadbeefbaadbeef; // here only to force
                                                  // a combinational solution
                                                  // a casex would be better
    endcase
  end
endmodule // alu

module FU_ALU(
	      input         clock,               // system clock
	      input         reset,               // system reset
	      input 		   			ALU_valid,
	      input [63:0] 	   			ALU_NPC,
	      input [31:0] 	   			ALU_IR,
	      input [`ALU_FUNC_NUM_LOG-1:0] 		ALU_alu_func,
	      input [`PRF_SIZE-1:0] 	   		ALU_regA,
	      input [`PRF_SIZE-1:0] 	   		ALU_regB,
	      input [1:0]		   		ALU_opa_sel,
	      input [1:0]		   		ALU_opb_sel,
	      input [`PRF_SIZE_LOG-1:0] 	   	ALU_dest,
	      input [`ROB_SIZE_LOG-1:0] 	   	ALU_ROB,
	      input [`BRAT_SIZE_LOG-1:0]		ALU_BRAT,
	      
	      output [63:0]				FU_ALU_NPC_out,
	      output [31:0]				FU_ALU_IR_out,
	      output [`PRF_SIZE_LOG-1:0] 		FU_ALU_dest_out,
	      output [`ROB_SIZE_LOG-1:0]		FU_ALU_ROB_out,
	      output [`BRAT_SIZE_LOG-1:0]		FU_ALU_BRAT_out,
	      output [63:0] 				FU_ALU_result_out,   // ALU result
	      output 					FU_ALU_valid_out
               );

  logic  [63:0] opa_mux_out, opb_mux_out;
  wire [63:0]  alu_result;

   
   // set up possible immediates:
   //   mem_disp: sign-extended 16-bit immediate for memory format
   //   br_disp: sign-extended 21-bit immediate * 4 for branch displacement
   //   alu_imm: zero-extended 8-bit immediate for ALU ops
  wire [63:0] mem_disp = { {48{ALU_IR[15]}}, ALU_IR[15:0] };
  wire [63:0] br_disp  = { {41{ALU_IR[20]}}, ALU_IR[20:0], 2'b00 };
  wire [63:0] alu_imm  = { 56'b0, ALU_IR[20:13] };
   
   //
   // ALU opA mux
   //
  always_comb
  begin
    case (ALU_opa_sel)
      `ALU_OPA_IS_REGA:     opa_mux_out = ALU_regA;
      `ALU_OPA_IS_MEM_DISP: opa_mux_out = mem_disp;
      `ALU_OPA_IS_NPC:      opa_mux_out = ALU_NPC;
      `ALU_OPA_IS_NOT3:     opa_mux_out = ~64'h3;
    endcase
  end

   //
   // ALU opB mux
   //
  always_comb
  begin
     // Default value, Set only because the case isnt full.  If you see this
     // value on the output of the mux you have an invalid opb_select
    opb_mux_out = 64'hbaadbeefdeadbeef;
    case (ALU_opb_sel)
      `ALU_OPB_IS_REGB:    opb_mux_out = ALU_regB;
      `ALU_OPB_IS_ALU_IMM: opb_mux_out = alu_imm;
      `ALU_OPB_IS_BR_DISP: opb_mux_out = br_disp;
    endcase 
  end

   //
   // instantiate the ALU
   //
  ALU_alu alu_0 (// Inputs
             .opa(opa_mux_out),
             .opb(opb_mux_out),
             .func(ALU_alu_func),

             // Output
             .result(alu_result)
            );

   assign FU_ALU_NPC_out = ALU_NPC;
   assign FU_ALU_IR_out = ALU_IR;
   assign FU_ALU_dest_out = ALU_dest;
   assign FU_ALU_ROB_out = ALU_ROB;
   assign FU_ALU_result_out = alu_result;
   assign FU_ALU_valid_out = ALU_valid;
   assign FU_ALU_BRAT_out = ALU_BRAT;
   



endmodule // module ex_stage

