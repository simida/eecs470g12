//////////////////////////////////////////////////////////////////////////
//                                                                      //
//   Modulename :  FU_BR.v                                              //
//                                                                      //
//  Description :  instruction execute (EX) stage of the pipeline;      //
//                 given the instruction command code CMD, select the   //
//                 proper input A and B for the ALU, compute the result,// 
//                 and compute the condition for branches, and pass all //
//                 the results down the pipeline. MWB                   // 
//                                                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

//
// The ALU
//
// given the command code CMD and proper operands A and B, compute the
// result of the instruction
//
// This module is purely combinational
//
module BR_alu(
           input [63:0] opa,
           input [63:0] opb,
           input  [4:0] func,
           
           output logic [63:0] result
          );

    // This function computes a signed less-than operation
  function signed_lt;
    input [63:0] a, b;
    
    if (a[63] == b[63]) 
      signed_lt = (a < b); // signs match: signed compare same as unsigned
    else
      signed_lt = a[63];   // signs differ: a is smaller if neg, larger if pos
  endfunction

  always_comb
  begin
    case (func)
      `ALU_ADDQ:   result = opa + opb;
      `ALU_SUBQ:   result = opa - opb;
      `ALU_AND:    result = opa & opb;
      `ALU_BIC:    result = opa & ~opb;
      `ALU_BIS:    result = opa | opb;
      `ALU_ORNOT:  result = opa | ~opb;
      `ALU_XOR:    result = opa ^ opb;
      `ALU_EQV:    result = opa ^ ~opb;
      `ALU_SRL:    result = opa >> opb[5:0];
      `ALU_SLL:    result = opa << opb[5:0];
      `ALU_SRA:    result = (opa >> opb[5:0]) | ({64{opa[63]}} << (64 -
                             opb[5:0])); // arithmetic from logical shift
      `ALU_CMPULT: result = { 63'd0, (opa < opb) };
      `ALU_CMPEQ:  result = { 63'd0, (opa == opb) };
      `ALU_CMPULE: result = { 63'd0, (opa <= opb) };
      `ALU_CMPLT:  result = { 63'd0, signed_lt(opa, opb) };
      `ALU_CMPLE:  result = { 63'd0, (signed_lt(opa, opb) || (opa == opb)) };
      default:     result = 64'hdeadbeefbaadbeef; // here only to force
                                                  // a combinational solution
                                                  // a casex would be better
    endcase
  end
endmodule // alu

//
// BrCond module
//
// Given the instruction code, compute the proper condition for the
// instruction; for branches this condition will indicate whether the
// target is taken.
//
// This module is purely combinational
//
module brcond(// Inputs
              input [63:0] opa,        // Value to check against condition
              input  [2:0] func,       // Specifies which condition to check
			
			  output logic cond        // 0/1 condition result (False/True)
             );

  always_comb
  begin
    case (func[1:0]) // 'full-case'  All cases covered, no need for a default
      2'b00: cond = (opa[0] == 0);  // LBC: (lsb(opa) == 0) ?
      2'b01: cond = (opa == 0);     // EQ: (opa == 0) ?
      2'b10: cond = (opa[63] == 1); // LT: (signed(opa) < 0) : check sign bit
      2'b11: cond = (opa[63] == 1) || (opa == 0); // LE: (signed(opa) <= 0)
    endcase
  
     // negate cond if func[2] is set
    if (func[2])
      cond = ~cond;
  end
endmodule // brcond


module FU_BR(
	     input         clock,               // system clock
	     input         reset,               // system reset
	     input 		   			BR_valid,
	     input [63:0] 	   			BR_NPC,
	     input [31:0] 	   			BR_IR,
	     input [`ALU_FUNC_NUM_LOG-1:0] 		BR_alu_func,
	     input [63:0] 	   			BR_regA,
	     input [63:0] 	   			BR_regB,
	     input [1:0]		   		BR_opa_sel,
	     input [1:0]		   		BR_opb_sel,
	     input [`PRF_SIZE_LOG-1:0] 	   	BR_dest,
	     input [`ROB_SIZE_LOG-1:0] 	   	BR_ROB,
	     input [`BRAT_SIZE_LOG-1:0]			BR_BRAT,
	     input 					BR_uncn_br,
	     
	     output [63:0]				FU_BR_NPC_out,
	     output [31:0]				FU_BR_IR_out,
	     output  [`PRF_SIZE_LOG-1:0] 		FU_BR_dest_out,
	     output  [`ROB_SIZE_LOG-1:0]		FU_BR_ROB_out,
	     output  [63:0] 				FU_BR_result_out,   // ALU result
	     output  [`BRAT_SIZE_LOG-1:0]		FU_BR_BRAT_out,
	     output        				FU_BR_branch_out,  // is this a taken branch?
	     output  [63:0] 				FU_BR_branch_addr_out,  // is this a taken branch?
	     output 					FU_BR_valid_out
	     
             );
   

  logic  [63:0] opa_mux_out, opb_mux_out;
  logic         brcond_result;
  logic [63:0] br_result;
   
   // set up possible immediates:
   //   mem_disp: sign-extended 16-bit immediate for memory format
   //   br_disp: sign-extended 21-bit immediate * 4 for branch displacement
   //   alu_imm: zero-extended 8-bit immediate for ALU ops
  wire [63:0] mem_disp = { {48{BR_IR[15]}}, BR_IR[15:0] };
  wire [63:0] br_disp  = { {41{BR_IR[20]}}, BR_IR[20:0], 2'b00 };
  wire [63:0] alu_imm  = { 56'b0, BR_IR[20:13] };
   
   //
   // ALU opA mux
   //
  always_comb
  begin
    case (BR_opa_sel)
      `ALU_OPA_IS_REGA:     opa_mux_out = BR_regA;
      `ALU_OPA_IS_MEM_DISP: opa_mux_out = mem_disp;
      `ALU_OPA_IS_NPC:      opa_mux_out = BR_NPC;
      `ALU_OPA_IS_NOT3:     opa_mux_out = ~64'h3;
    endcase
  end

   //
   // ALU opB mux
   //
  always_comb
  begin
     // Default value, Set only because the case isnt full.  If you see this
     // value on the output of the mux you have an invalid opb_select
    opb_mux_out = 64'hbaadbeefdeadbeef;
    case (BR_opb_sel)
      `ALU_OPB_IS_REGB:    opb_mux_out = BR_regB;
      `ALU_OPB_IS_ALU_IMM: opb_mux_out = alu_imm;
      `ALU_OPB_IS_BR_DISP: opb_mux_out = br_disp;
    endcase 
  end

   //
   // instantiate the ALU
   //
  BR_alu alu_0 (// Inputs
             .opa(opa_mux_out),
             .opb(opb_mux_out),
             .func(BR_alu_func),

             // Output
             .result(br_result)
            );

   //
   // instantiate the branch condition tester
   //
  brcond brcond (// Inputs
                .opa(BR_regA),       // always check regA value
                .func(BR_IR[28:26]), // inst bits to determine check

                // Output
                .cond(brcond_result)
               );

   assign FU_BR_NPC_out = BR_NPC;
   assign FU_BR_IR_out = BR_IR;
   assign FU_BR_dest_out = BR_dest;
   assign FU_BR_ROB_out = BR_ROB;
   assign FU_BR_result_out = {32'b0,BR_NPC};
   assign FU_BR_branch_out = BR_uncn_br | brcond_result;
   assign FU_BR_branch_addr_out = br_result;
   assign FU_BR_valid_out = BR_valid;
   assign FU_BR_BRAT_out = BR_BRAT;




endmodule // module ex_stage

