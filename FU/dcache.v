
module dcache(
			  input         clock,
			  input         reset,

			  // inputs from memory
			  input   [3:0] Dmem2proc_response,
			  input  [63:0] Dmem2proc_data,
			  input   [3:0] Dmem2proc_tag,

			  // inputs from pipeline
			  input   [1:0] proc2Dcache_command,
			  input  [63:0] proc2Dcache_addr,    // Address sent to data-memory
			  input  [63:0] proc2Dcache_data,     // Data sent to data-memory

			  // inputs from cachemem
			  input  [63:0] cachemem_data,
			  input         cachemem_valid,

			  // outputs to memeory
			  output logic  [1:0] proc2Dmem_command,
			  output logic [63:0] proc2Dmem_addr,		 

			  // outputs to pipeline
			  output logic [63:0] Dcache_data_out,     // value is memory[proc2Dcache_addr]
			  output logic        Dcache_valid_out,    // when this is high

			  // outputs to cachemem
			  output logic        Dcache_wr_en,
			  output logic  [4:0] Dcache_wr_idx,
			  output logic [23:0] Dcache_wr_tag,
			  output logic [63:0] Dcache_wr_data,
			  output logic  [4:0] Dcache_rd_idx,
			  output logic [23:0] Dcache_rd_tag,
	  
             );


  logic [3:0] current_mem_tag;
  logic [3:0] current_index, last_index;
  logic [23:0] current_tag, last_tag;
  logic miss_outstanding;
  logic store;

  assign store = proc2Dcache_command == `BUS_STORE;

  assign {current_tag, current_index} = proc2Dmem_addr[31:3];

  wire changed_addr = (current_index != last_index) || (current_tag != last_tag);
  wire send_request = miss_outstanding && !changed_addr;
  wire update_mem_tag = changed_addr | miss_outstanding | data_write_enable;
  wire unanswered_miss = changed_addr ? !Dcache_valid_out
                  	 : miss_outstanding & (Dmem2proc_response == 0);

  assign proc2Dmem_addr = {proc2Dcache_addr[63:3],3'b0};
  assign proc2Dmem_command = (proc2Dcache_command == `BUS_STORE)? `BUS_STORE : 
				(miss_outstanding && !changed_addr) ? `BUS_LOAD : `BUS_NONE;

  assign Dcache_data_out = cachemem_data;
  assign Dcache_valid_out = cachemem_valid;


  assign Dcache_wr_en = store || ((current_mem_tag == Dmem2proc_tag) && (current_mem_tag != 0));
  assign Dcache_wr_idx = store ? current_index : last_index;
  assign Dcache_wr_tag = store ? current_tag : last_tag;
  assign Dcache_wr_data =  store ? proc2Dcache_data : Dmem2proc_data;
  
  assign Dcache_rd_idx = current_index;
  assign Dcache_rd_tag = current_tag;

  always_ff @(posedge clock)
  begin
    if(reset)
    begin
      last_index       <= `SD -1;   // These are -1 to get ball rolling when
      last_tag         <= `SD -1;   // reset goes low because addr "changes"
      current_mem_tag  <= `SD 0;              
      miss_outstanding <= `SD 0;
    end
    else
    begin
      last_index       <= `SD current_index;
      last_tag         <= `SD current_tag;
      miss_outstanding <= `SD unanswered_miss;
      if(update_mem_tag)
        current_mem_tag <= `SD Dmem2proc_response;
    end
  end

endmodule

