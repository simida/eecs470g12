//////////////////////////////////////////////////////////////////////////
//                                                                      //
//   Modulename :  FU_MEM.v                                             //
//                                                                      //
//  Description :  instruction execute (EX) stage of the pipeline;      //
//                 given the instruction command code CMD, select the   //
//                 proper input A and B for the ALU, compute the result,// 
//                 and compute the condition for branches, and pass all //
//                 the results down the pipeline. MWB                   // 
//                                                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

//
// The ALU
//
// given the command code CMD and proper operands A and B, compute the
// result of the instruction
//
// This module is purely combinational
//
module MEM_alu(
           input [63:0] opa,
           input [63:0] opb,
           input  [4:0] func,
           
           output logic [63:0] result
          );

    // This function computes a signed less-than operation
  function signed_lt;
    input [63:0] a, b;
    
    if (a[63] == b[63]) 
      signed_lt = (a < b); // signs match: signed compare same as unsigned
    else
      signed_lt = a[63];   // signs differ: a is smaller if neg, larger if pos
  endfunction

  always_comb
  begin
    case (func)
      `ALU_ADDQ:   result = opa + opb;
      `ALU_SUBQ:   result = opa - opb;
      `ALU_AND:    result = opa & opb;
      `ALU_BIC:    result = opa & ~opb;
      `ALU_BIS:    result = opa | opb;
      `ALU_ORNOT:  result = opa | ~opb;
      `ALU_XOR:    result = opa ^ opb;
      `ALU_EQV:    result = opa ^ ~opb;
      `ALU_SRL:    result = opa >> opb[5:0];
      `ALU_SLL:    result = opa << opb[5:0];
      `ALU_SRA:    result = (opa >> opb[5:0]) | ({64{opa[63]}} << (64 -
                             opb[5:0])); // arithmetic from logical shift
      `ALU_CMPULT: result = { 63'd0, (opa < opb) };
      `ALU_CMPEQ:  result = { 63'd0, (opa == opb) };
      `ALU_CMPULE: result = { 63'd0, (opa <= opb) };
      `ALU_CMPLT:  result = { 63'd0, signed_lt(opa, opb) };
      `ALU_CMPLE:  result = { 63'd0, (signed_lt(opa, opb) || (opa == opb)) };
      default:     result = 64'hdeadbeefbaadbeef; // here only to force
                                                  // a combinational solution
                                                  // a casex would be better
    endcase
  end
endmodule // alu

module mem_stage(
					input         clock,             // system clock
					input         reset,             // system reset
					input  [63:0] ex_mem_rega,       // regA value from reg file (store data)
					input  [63:0] ex_mem_alu_result, // incoming ALU result from EX
					input  [31:0] ex_mem_IR,
					input  [63:0] Dmem2proc_data,
					input   [3:0] Dmem2proc_tag, Dmem2proc_response,

					output [63:0] mem_result_out,    // outgoing instruction result (to MEM/WB)
					output        mem_stall_out,
					output  [1:0] proc2Dmem_command,
					output [63:0] proc2Dmem_addr,    // Address sent to data-memory
					output [63:0] proc2Dmem_data     // Data sent to data-memory
                );

  logic [3:0] mem_waiting_tag;

  assign ex_mem_rd_mem = (ex_mem_IR[31:26]==`LDQ_INST || ex_mem_IR[31:26]==`LDQ_L_INST);
  assign ex_mem_wr_mem = (ex_mem_IR[31:26]==`STQ_INST || ex_mem_IR[31:26]==`STQ_C_INST);

   // Determine the command that must be sent to mem
  assign proc2Dmem_command =
    (mem_waiting_tag!=0) ? `BUS_NONE
                         : ex_mem_wr_mem ? `BUS_STORE 
                                         : ex_mem_rd_mem ? `BUS_LOAD
                                                         : `BUS_NONE;

   // The memory address is calculated by the ALU
  assign proc2Dmem_data = ex_mem_rega;

  assign proc2Dmem_addr = ex_mem_alu_result;

   // Assign the result-out for next stage
  assign mem_result_out = (ex_mem_rd_mem) ? Dmem2proc_data : ex_mem_alu_result;

  assign mem_stall_out = 
    (ex_mem_rd_mem & ((mem_waiting_tag!=Dmem2proc_tag) | (Dmem2proc_tag==0))) |
    (ex_mem_wr_mem & (Dmem2proc_response==0));

  wire write_enable = ex_mem_rd_mem & 
    ((mem_waiting_tag==0) | (mem_waiting_tag==Dmem2proc_tag));

  always_ff @(posedge clock)
    if(reset)
      mem_waiting_tag <= `SD 0;
    else if(write_enable)
      mem_waiting_tag <= `SD Dmem2proc_response;

endmodule // module mem_stage


module FU_MEM(
				input         clock,               // system clock
				input         reset,               // system reset

				input  [63:0] Dmem2proc_data,
				input   [3:0] Dmem2proc_tag,
				input   [3:0] Dmem2proc_response,

				input 		   			MEM_valid,
				input					MEM_stall,
				input [63:0] 	   			MEM_NPC,
				input [31:0] 	   			MEM_IR,
				input [`ALU_FUNC_NUM_LOG-1:0] 		MEM_alu_func,
				input [`PRF_SIZE-1:0] 	   		MEM_regA,
				input [`PRF_SIZE-1:0] 	   		MEM_regB,
				input [1:0]		   		MEM_opa_sel,
				input [1:0]		   		MEM_opb_sel,
				input [`PRF_SIZE_LOG-1:0] 	   	MEM_dest,
				input [`ROB_SIZE_LOG-1:0] 	   	MEM_ROB,

				output [63:0]				FU_MEM_NPC_out,
				output [31:0]				FU_MEM_IR_out,
				output [`PRF_SIZE_LOG-1:0] 		FU_MEM_dest_out,
				output [`ROB_SIZE_LOG-1:0]		FU_MEM_ROB_out,  
				output [63:0]      			FU_MEM_result_out, 
				output  				FU_MEM_valid_out,
				output  				FU_MEM_stall_out, 

				output  [1:0] proc2Dmem_command,
				output [63:0] proc2Dmem_addr,    // Address sent to data-memory
				output [63:0] proc2Dmem_data     // Data sent to data-memory

               );


  logic  [63:0] opa_mux_out, opb_mux_out;
  logic         brcond_result;
  logic		mem_stall_out;
   
  wire [63:0] alu_result;


   // set up possible immediates:
   //   mem_disp: sign-extended 16-bit immediate for memory format
   //   br_disp: sign-extended 21-bit immediate * 4 for branch displacement
   //   alu_imm: zero-extended 8-bit immediate for ALU ops
  wire [63:0] mem_disp = { {48{MEM_IR[15]}}, MEM_IR[15:0] };
  wire [63:0] br_disp  = { {41{MEM_IR[20]}}, MEM_IR[20:0], 2'b00 };
  wire [63:0] alu_imm  = { 56'b0, MEM_IR[20:13] };
   
   //
   // ALU opA mux
   //
  always_comb
  begin
    case (MEM_opa_sel)
      `ALU_OPA_IS_REGA:     opa_mux_out = MEM_regA;
      `ALU_OPA_IS_MEM_DISP: opa_mux_out = mem_disp;
      `ALU_OPA_IS_NPC:      opa_mux_out = MEM_NPC;
      `ALU_OPA_IS_NOT3:     opa_mux_out = ~64'h3;
    endcase
  end

   //
   // ALU opB mux
   //
  always_comb
  begin
     // Default value, Set only because the case isnt full.  If you see this
     // value on the output of the mux you have an invalid opb_select
    opb_mux_out = 64'hbaadbeefdeadbeef;
    case (MEM_opb_sel)
      `ALU_OPB_IS_REGB:    opb_mux_out = MEM_regB;
      `ALU_OPB_IS_ALU_IMM: opb_mux_out = alu_imm;
      `ALU_OPB_IS_BR_DISP: opb_mux_out = br_disp;
    endcase 
  end

   //
   // instantiate the ALU
   //
  MEM_alu alu_0 (// Inputs
             .opa(opa_mux_out),
             .opb(opb_mux_out),
             .func(MEM_alu_func),

             // Output
             .result(alu_result)
            );



  wire [63:0] mem_result_out;
  wire [1:0] proc2Dmem_command_tmp;

  mem_stage mem0 ( 			.clock(clock),             // system clock
					.reset(reset),             // system reset
					.ex_mem_rega(MEM_regA),       // regA value from reg file (store data)
					.ex_mem_alu_result(alu_result), // incoming ALU result from EX
					.ex_mem_IR(MEM_IR),
					.Dmem2proc_data(Dmem2proc_data),
					.Dmem2proc_tag(Dmem2proc_tag),
 					.Dmem2proc_response(Dmem2proc_response),

					.mem_result_out(mem_result_out),    // outgoing instruction result (to MEM/WB)
					.mem_stall_out(mem_stall_out),
					.proc2Dmem_command(proc2Dmem_command_tmp),
					.proc2Dmem_addr(proc2Dmem_addr),    // Address sent to data-memory
					.proc2Dmem_data(proc2Dmem_data)     // Data sent to data-memory
		
	    ); 

  reg [63:0] NPC;
  reg [31:0] IR;
  reg [`PRF_SIZE_LOG-1:0] dest;
  reg [`ROB_SIZE_LOG-1:0] ROB;
  reg [63:0] result;
  reg valid;
  reg stall;

  always_ff @(posedge clock) begin
	if (reset) begin
		NPC 	<= `SD 0;
		IR 	<= `SD 0;
		dest 	<= `SD 0;
		ROB 	<= `SD 0;
		result 	<= `SD 0;
		valid 	<= `SD 0;
		stall 	<= `SD 0;
	end
	else begin
		NPC 	<= `SD FU_MEM_NPC_out;
		IR 	<= `SD FU_MEM_IR_out;
		dest 	<= `SD FU_MEM_dest_out;
		ROB 	<= `SD FU_MEM_ROB_out;
		result 	<= `SD FU_MEM_result_out;
		valid 	<= `SD FU_MEM_valid_out;
		stall 	<= `SD MEM_stall & FU_MEM_valid_out;
	end
  end




  assign FU_MEM_NPC_out 	= stall ? NPC : MEM_NPC;
  assign FU_MEM_IR_out 		= stall ? IR  : MEM_IR;
  assign FU_MEM_dest_out 	= stall ? dest: MEM_dest;
  assign FU_MEM_ROB_out 	= stall ? ROB : MEM_ROB;
  assign FU_MEM_result_out 	= stall ? result : mem_result_out;
  assign FU_MEM_valid_out 	= stall ? valid : MEM_valid & (~mem_stall_out);	
  assign FU_MEM_stall_out 	= stall ? 0 : mem_stall_out;
  assign proc2Dmem_command 	= stall ? `BUS_NONE : proc2Dmem_command_tmp;

endmodule // module ex_stage




