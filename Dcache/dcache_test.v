
`timescale 1ns/100ps
module testbench();

			  logic         clock;
			  logic         reset;
				//logic from LSQ
				logic						LSQ_DC_rd_mem;
				logic	 [63:0]		LSQ_DC_addr;
				logic  					LSQ_DC_wr_mem;
				logic	 [63:0]		LSQ_DC_wr_value;
				logic	 [`BRAT_SIZE_LOG-1:0]	LSQ_DC_BRAT_num;
				logic	 [`ROB_SIZE_LOG-1:0]	LSQ_DC_ROB_num;
				//logic from Dcache
			  wire  [63:0] Dcache_data;
			  wire         Dcache_valid;
				//logic from Dmemory
				wire  [63:0]	Dmem_DC_data;
				wire	 [3:0]	Dmem_DC_response;
				wire	 [3:0]	Dmem_DC_tag;
				//logic from ROB
				logic  [`BRAT_SIZE-1:0]	BRAT_squash;

				//output to LSQ
				wire 					MSHR_full;
				wire  				LD_BUF_full;
				wire	 [63:0]	Dcache_LSQ_data;
				wire	 [63:0]	Dcache_LSQ_rd_addr;
				wire						Dcache_LSQ_rd_valid;
				wire	[`ROB_SIZE_LOG-1:0]	Dcache_LSQ_ROB_num;
			  //output to Dcache	
				wire	 [`SET_IDX_SIZE_LOG-1:0]	 DC_Dcache_wr1_idx;
				wire	 [`SET_IDX_SIZE_LOG-1:0]	 DC_Dcache_wr2_idx;
				wire	 [`DCACHE_TAG_SIZE_LOG-1:0]DC_Dcache_wr1_tag;
				wire	 [`DCACHE_TAG_SIZE_LOG-1:0]DC_Dcache_wr2_tag;
				wire	 [63:0]									   DC_Dcache_wr1_data;
				wire	 [63:0]									   DC_Dcache_wr2_data;
				wire														 DC_Dcache_wr1_enable;
				wire														 DC_Dcache_wr1_enable;
				wire	 [`SET_IDX_SIZE_LOG-1:0]	DC_Dcache_rd_idx;
				wire	 [`DCACHE_TAG_SIZE_LOG-1:0]			DC_Dcache_rd_tag;
				//output to Dmem
				wire	 [63:0]				DC_Dmem_addr;
				wire	 [1:0]				DC_Dmem_command;
				wire	 [63:0]				DC_Dmem_data;

	dcache DUT_dcache(
			  .clock,
			  .reset,
				//input from LSQ
				.LSQ_DC_rd_mem,
				.LSQ_DC_addr,
				.LSQ_DC_wr_mem,
				.LSQ_DC_wr_value,
				.LSQ_DC_BRAT_num,
				.LSQ_DC_ROB_num,
				//input from Dcache
			  .Dcache_data,
			  .Dcache_valid,
				//input from Dmemory
				.Dmem_DC_data,
				.Dmem_DC_response,
				.Dmem_DC_tag,
				//input from ROB
				.BRAT_squash,

				//output to LSQ
				.MSHR_full,//???
				.LD_BUF_full,
				.Dcache_LSQ_data,
				.Dcache_LSQ_rd_addr,
				.Dcache_LSQ_rd_valid,
				.Dcache_LSQ_ROB_num,
			  //output to Dcache	
				.DC_Dcache_wr1_idx,
				.DC_Dcache_wr2_idx,
				.DC_Dcache_wr1_tag,
				.DC_Dcache_wr2_tag,
				.DC_Dcache_wr1_data,
				.DC_Dcache_wr2_data,
				.DC_Dcache_wr1_enable,
				.DC_Dcache_wr2_enable,
				.DC_Dcache_rd_idx,
				.DC_Dcache_rd_tag,
				//output to Dmem
				.DC_Dmem_addr,
				.DC_Dmem_command,
				.DC_Dmem_data
			);

  dcachemem DUT_dcachemem(
				.clock, 
				.reset, 
				.wr1_en(DC_Dcache_wr1_enable),
				.wr1_idx(DC_Dcache_wr1_idx), 
				.wr1_tag(DC_Dcache_wr1_tag), 
				.wr1_data(DC_Dcache_wr1_data), 
				.wr2_en(DC_Dcache_wr2_enable),
				.wr2_idx(DC_Dcache_wr2_idx), 
				.wr2_tag(DC_Dcache_wr2_tag), 
				.wr2_data(DC_Dcache_wr2_data), 

				.rd1_idx(DC_Dcache_rd_idx),
				.rd1_tag(DC_Dcache_rd_tag),

				.rd1_data(Dcache_data),
				.rd1_valid(Dcache_valid)
				
       );
	
   mem DUT_mem(
			.clk(clock),              // Memory clock
			.proc2mem_addr(DC_Dmem_addr),    // address for current command
			.proc2mem_data(DC_Dmem_data),    // address for current command
			.proc2mem_command(DC_Dmem_command), // `BUS_NONE `BUS_LOAD or `BUS_STORE

			.mem2proc_response(Dmem_DC_response), // 0 = can't accept, other=tag of transaction
			.mem2proc_data(Dmem_DC_data),     // data resulting from a load
			.mem2proc_tag(Dmem_DC_tag)       // 0 = no value, other=tag of transaction
           );

	always
	begin
		#`half_clkperiod;
				clock  = ~clock; 
	end


  // Show contents of a range of Unified Memory, in both hex and decimal
  task show_mem_with_decimal;
   input [31:0] start_addr;
   input [31:0] end_addr;
   int showing_data;
   begin
    $display("@@@");
    showing_data=0;
    for(int k=start_addr;k<=end_addr; k=k+1)
      if (DUT_mem.unified_memory[k] != 0)
      begin
        $display("@@@ mem[%5d] = %x : %0d", k*8, DUT_mem.unified_memory[k], 
                                                 DUT_mem.unified_memory[k]);
        showing_data=1;
      end
      else if(showing_data!=0)
      begin
        $display("@@@");
        showing_data=0;
      end
    $display("@@@");
   end
  endtask  // task show_mem_with_decimal

task Dcache_rd_miss;//MSHR_full ???		
		@(negedge clock);
		for(int i=0;i<21;i++)begin
				$display("LSQ_DC_rd_mem:%b DC_Dmem_command:%b Dmem_DC_response:%d Dmem_DC_tag:%d MSHR_count:%d MSHR_full:%b",LSQ_DC_rd_mem,DC_Dmem_command,Dmem_DC_response,Dmem_DC_tag,DUT_dcache.MSHR_count,MSHR_full);
			@(negedge clock);
				LSQ_DC_rd_mem=1;	
				LSQ_DC_addr=LSQ_DC_addr+8;
		end
		LSQ_DC_rd_mem=0;	
endtask

task Dcache_wr_miss;		
		@(negedge clock);
		for(int i=0;i<21;i++)begin
				$display("LSQ_DC_wr_mem:%b DC_Dmem_command:%b Dmem_DC_response:%d Dmem_DC_tag:%d MSHR_count:%d MSHR_full:%b",LSQ_DC_wr_mem,DC_Dmem_command,Dmem_DC_response,Dmem_DC_tag,DUT_dcache.MSHR_count,MSHR_full);
			@(negedge clock);
				LSQ_DC_wr_mem=1;	
				LSQ_DC_addr=LSQ_DC_addr+8;
				LSQ_DC_wr_value=LSQ_DC_wr_value+8;
		end
		LSQ_DC_wr_mem=0;	
endtask



task read_conflict;
		reg [63:0] stack_top;
		stack_top = 16'h0000;
		for(int i=0;i<21;i++)begin
				LSQ_DC_rd_mem=1;
				LSQ_DC_addr=stack_top;
				LSQ_DC_wr_value=0;
				LSQ_DC_BRAT_num=4;
				LSQ_DC_wr_mem=0;	
				stack_top = stack_top + 8;
				@(posedge clock);
		end
		stack_top=16'h0040;
		for(int i=0;i<8;i++)begin
				LSQ_DC_rd_mem=1;
				LSQ_DC_addr=stack_top;
				LSQ_DC_wr_value=0;
				LSQ_DC_BRAT_num=4;
				LSQ_DC_wr_mem=0;	
				@(posedge clock);
		end
		LSQ_DC_wr_mem=0;	
		LSQ_DC_rd_mem=0;
endtask

task write_conflict;
		reg [63:0] stack_top;
		stack_top = 16'h0000;
		for(int i=0;i<21;i++)begin
				LSQ_DC_rd_mem=1;
				LSQ_DC_addr=stack_top;
				LSQ_DC_wr_value=0;
				LSQ_DC_BRAT_num=4;
				LSQ_DC_wr_mem=0;	
				stack_top = stack_top + 8;
				@(posedge clock);
		end
		stack_top=16'h0030;
		for(int i=0;i<8;i++)begin
				LSQ_DC_rd_mem=1;
				LSQ_DC_addr=stack_top;
				LSQ_DC_wr_value=0;
				LSQ_DC_BRAT_num=4;
				LSQ_DC_wr_mem=0;	
				@(posedge clock);
		end
endtask

task read_after_write;		
		reg [63:0] stack_top;
		stack_top = 16'h1000;
		@(negedge clock);
		for(int i=0;i<21;i++)begin
				LSQ_DC_rd_mem=0;
				LSQ_DC_addr=stack_top;
				LSQ_DC_wr_value=i;
				LSQ_DC_BRAT_num=4;
				LSQ_DC_wr_mem=1;	
			@(negedge clock);
				LSQ_DC_rd_mem=1;
				LSQ_DC_addr=stack_top;
				LSQ_DC_wr_value=i;
				LSQ_DC_BRAT_num=4;
				LSQ_DC_wr_mem=0;	
				stack_top = stack_top + 8;
			@(negedge clock);
		end
		LSQ_DC_wr_mem=0;	
		LSQ_DC_rd_mem=0;
endtask

  initial begin

		clock=0;
		// from LSQ
		LSQ_DC_rd_mem=0;
		LSQ_DC_addr=0;
		LSQ_DC_wr_mem=0;
		LSQ_DC_wr_value=8;
		LSQ_DC_BRAT_num=0;
		// from LD_BUF
		BRAT_squash=0;


    // Pulse the reset signal
    $display("@@\n@@\n@@  %t  Asserting System reset......", $realtime);
    reset = 1'b1;
    @(posedge clock);

    $readmemh("program.mem", DUT_mem.unified_memory);

    @(posedge clock);
    `SD;
    // This reset is at an odd time to avoid the pos & neg clock edges

    reset = 1'b0;
    $display("@@  %t  Deasserting System reset......\n@@\n@@", $realtime);


		Dcache_rd_miss;
		read_after_write;
		read_conflict;
		show_mem_with_decimal(0,`MEM_64BIT_LINES - 1);
		$finish;
	end
endmodule
