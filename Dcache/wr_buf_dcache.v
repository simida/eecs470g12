//bug2: if rd dcache & wr dcache mem of the same address, do forwarding
//bug3: DC_Dcache_wr1_enable in seq or comb?
`timescale 1ns/100ps
module dcache(
			  input         clock,
			  input         reset,
				//input from LSQ
				input						LSQ_DC_rd_mem,
				input	 [63:0]		LSQ_DC_addr,
				input  					LSQ_DC_wr_mem,
				input	 [63:0]		LSQ_DC_wr_value,
				input	 [`BRAT_SIZE_LOG-1:0]	LSQ_DC_BRAT_num,
				//input from Dcache
			  input  [63:0] Dcache_data,
			  input         Dcache_valid,
				//input from Dmemory
				input  [63:0]	Dmem_DC_data,
				input	 [3:0]	Dmem_DC_response,
				input	 [3:0]	Dmem_DC_tag,
				//input from ROB
				input  [`BRAT_SIZE-1:0]	BRAT_squash,

				//output to LSQ
				output 	logic					MSHR_full,//??? not use head/tail, but just count???
				output  logic					LD_BUF_full,
				output  logic					WR_BUF_full,
				output	logic [63:0]	Dcache_LSQ_data,
				output	logic [63:0]	Dcache_LSQ_rd_addr,
				output	logic					Dcache_LSQ_rd_valid,
			  //output to Dcache	
				output	logic [`SET_IDX_SIZE_LOG-1:0]					DC_Dcache_wr1_idx, DC_Dcache_wr2_idx,
				output	logic [`DCACHE_TAG_SIZE_LOG-1:0]			DC_Dcache_wr1_tag, DC_Dcache_wr2_tag,
				output	logic [63:0]													DC_Dcache_wr1_data, DC_Dcache_wr2_data,
				output	logic																	DC_Dcache_wr1_enable, DC_Dcache_wr2_enable,
				output	logic [`SET_IDX_SIZE_LOG-1:0]					DC_Dcache_rd_idx,
				output	logic [`DCACHE_TAG_SIZE_LOG-1:0]			DC_Dcache_rd_tag,
				//output to Dmem
				output	logic	[63:0]				DC_Dmem_addr,
				output	logic [1:0]					DC_Dmem_command,
				output	logic [63:0]				DC_Dmem_data
        );

  logic												D1_LSQ_DC_rd_mem;
	logic	 [63:0]								D1_LSQ_DC_addr;
	logic	 [`BRAT_SIZE_LOG-1:0]	D1_LSQ_DC_BRAT_num;
	logic	 [1:0]								D1_LSQ_DC_command;
	logic												D1_DC_Dcache_wr1_enable;

	logic	[63:0]			 next_Dcache_LSQ_data;
	logic	[63:0]			 next_Dcache_LSQ_rd_addr; 
	logic							 next_Dcache_LSQ_rd_valid	;

  logic [15:0][63:0]								MSHR_regfl_addr;//need to store ???			
	logic [15:0]											MSHR_regfl_valid;
	logic [15:0][1:0]									MSHR_regfl_valid_command;//??? to resolve squash case
	logic [15:0][`BRAT_SIZE_LOG-1:0]	MSHR_regfl_BRAT_num;
	logic [15:0]											MSHR_regfl_wr_enable;

  logic	[63:0]				next_DC_Dmem_addr;
  logic [1:0]					next_DC_Dmem_command;
	logic [63:0]				next_DC_Dmem_data;
  //LD_BUF: for structural hazard between mem ld output and cache ld output 
	logic [`LD_BUF_SIZE-1:0][63:0]	LD_BUF_data;
	logic [`LD_BUF_SIZE-1:0][63:0]	LD_BUF_addr;
	logic [`LD_BUF_SIZE-1:0]      	LD_BUF_valid;

  logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_head;
  wire [`LD_BUF_SIZE_LOG-1:0]		  LD_BUF_head_p1;
  logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_tail;
  wire [`LD_BUF_SIZE_LOG-1:0]		  LD_BUF_tail_p1;

  //WR_BUF: for structural hazard: mem ld output and cache st, write into cache 
	logic [`WR_BUF_SIZE-1:0][63:0]	WR_BUF_data;
	logic [`WR_BUF_SIZE-1:0][63:0]	WR_BUF_addr;
	logic [`WR_BUF_SIZE-1:0]      	WR_BUF_valid;

  logic [`WR_BUF_SIZE_LOG-1:0]		WR_BUF_head;
  wire [`WR_BUF_SIZE_LOG-1:0]		  WR_BUF_head_p1;
  logic [`WR_BUF_SIZE_LOG-1:0]		WR_BUF_tail;
  wire [`WR_BUF_SIZE_LOG-1:0]		  WR_BUF_tail_p1;

  logic [`MSHR_SIZE_LOG-1:0]		MSHR_count;

	assign LD_BUF_head_p1 = LD_BUF_head + 1'd1;
  assign LD_BUF_tail_p1 =LD_BUF_tail + 1;
	assign WR_BUF_head_p1 = WR_BUF_head + 1'd1;
  assign WR_BUF_tail_p1 =WR_BUF_tail + 1;
	
	always_comb begin
		//default
		DC_Dmem_command = `BUS_NONE;
		next_Dcache_LSQ_rd_valid	= 1'b0;

		if(!Dcache_valid) begin//read cache miss
			if(LSQ_DC_rd_mem)begin
				DC_Dmem_addr 		= LSQ_DC_addr;
				DC_Dmem_command = `BUS_LOAD;
			end
		end
		else begin
			if(LSQ_DC_rd_mem)begin
				 next_Dcache_LSQ_data 			= Dcache_data;
				 next_Dcache_LSQ_rd_addr		= LSQ_DC_addr; 
				 next_Dcache_LSQ_rd_valid	= 1'b1;
	  	end
		end

		if(LSQ_DC_wr_mem)begin
				DC_Dmem_addr    = LSQ_DC_addr;
				DC_Dmem_command = `BUS_STORE;
				DC_Dmem_data		= LSQ_DC_wr_value;
		end

		if(MSHR_count == 4'hf) //assume 15 entry
			MSHR_full = 1'b1;
		else
			MSHR_full = 1'b0;
	end

  always_ff @(posedge clock)
  begin
    if(reset)
    begin
				DC_Dcache_wr1_idx       <= `SD -1;
				DC_Dcache_wr1_tag       <= `SD -1;
				DC_Dcache_wr1_data      <= `SD -1;
				DC_Dcache_wr1_enable	<= `SD 1'b0;
				for(int m = 0; m<16;m++)begin
					MSHR_regfl_valid[m] <= `SD 1'b0;
					MSHR_regfl_valid_command[m] <= `SD 2'b0;
					MSHR_regfl_wr_enable[m] <= `SD 1'b0;
				end

				LD_BUF_head 	  		<= `SD {`LD_BUF_SIZE_LOG'b0};
				LD_BUF_tail 				<= `SD {`LD_BUF_SIZE_LOG'b0};
				LD_BUF_full 				<= `SD 1'b0;
				LD_BUF_valid        <= `SD {`LD_BUF_SIZE'b0};
				WR_BUF_head 	  		<= `SD {`WR_BUF_SIZE_LOG'b0};
				WR_BUF_tail 				<= `SD {`WR_BUF_SIZE_LOG'b0};
				WR_BUF_full 				<= `SD 1'b0;
				WR_BUF_valid        <= `SD {`WR_BUF_SIZE'b0};

				MSHR_count 				<= `SD {`MSHR_SIZE_LOG'b0};
    end
    else
    begin
				D1_LSQ_DC_rd_mem 		<= `SD LSQ_DC_rd_mem;
				D1_LSQ_DC_addr   		<= `SD LSQ_DC_addr;
				D1_LSQ_DC_addr   		<= `SD LSQ_DC_addr;
				D1_LSQ_DC_command 	<= `SD {LSQ_DC_wr_mem,LSQ_DC_rd_mem};
			  DC_Dcache_wr1_enable <= `SD 1'b0;

			  Dcache_LSQ_rd_valid	<= `SD 1'b0;

			//clear MSHR
			for(int i = 0;i<`MSHR_SIZE;i++)begin
				if(MSHR_regfl_valid_command[i]) begin
					if(BRAT_squash[MSHR_regfl_BRAT_num[i]])
							MSHR_regfl_valid_command[i] <= 2'b0;
							MSHR_regfl_wr_enable[i] <= 1'b0;
				 end
			end
		  
			if(Dmem_DC_response && Dmem_DC_tag) MSHR_count <= MSHR_count;//1 in, 1 out
			else if(Dmem_DC_response)	MSHR_count <= MSHR_count+{`MSHR_SIZE_LOG'b1};//1 in
			else if(Dmem_DC_tag)	MSHR_count <= MSHR_count-{`MSHR_SIZE_LOG'b1};//1 out

			//Send to access memory
		  if(Dmem_DC_response)begin
				MSHR_regfl_addr[Dmem_DC_response] 	<= `SD D1_LSQ_DC_addr;
				MSHR_regfl_BRAT_num[Dmem_DC_response] 	<= `SD D1_LSQ_DC_BRAT_num;
				MSHR_regfl_valid[Dmem_DC_response] 	<= `SD 1'b1;
				MSHR_regfl_valid_command[Dmem_DC_response] 	<= `SD D1_LSQ_DC_command;
				if(D1_LSQ_DC_command==`BUS_LOAD) 
						MSHR_regfl_wr_enable[Dmem_DC_response] <= `SD 1'b1;

			//STORE goes into MSHR, it clears 'DC_Dcache_wr1_enable' of prev LOAD
				if(D1_LSQ_DC_command==`BUS_STORE) begin
						for(int t=0;t<`MSHR_SIZE;t++) begin
							if(MSHR_regfl_valid_command[t]==`BUS_LOAD) begin
									if(MSHR_regfl_addr[t]==D1_LSQ_DC_addr)
											MSHR_regfl_wr_enable[t] <= `SD 1'b0;
							end
						end
				end
			end
			
			//Handle data from memory
			if(Dmem_DC_tag)begin//no matter squash or not, move head
				MSHR_regfl_valid[Dmem_DC_tag]  <= `SD 1'b0;
			end

			//output to LSQ
			if(MSHR_regfl_valid_command[Dmem_DC_tag]==2'b1)begin
					 Dcache_LSQ_data 								<= `SD Dmem_DC_data;
					 Dcache_LSQ_rd_addr							<= `SD MSHR_regfl_addr[Dmem_DC_tag];
					 Dcache_LSQ_rd_valid						<= `SD 1'b1;

					 //cache hit not able to send to LSQ
					 if(next_Dcache_LSQ_rd_valid) begin
					   LD_BUF_data[LD_BUF_tail]  <= `SD next_Dcache_LSQ_data;
					   LD_BUF_addr[LD_BUF_tail]  <= `SD next_Dcache_LSQ_rd_addr;
					   LD_BUF_valid[LD_BUF_tail] <= `SD 1'b1;
						 LD_BUF_tail               <= LD_BUF_tail_p1;//???
						 if (LD_BUF_valid[LD_BUF_tail_p1])
						 		LD_BUF_full <= `SD 1'b1;
						 
					 end                        	
			end
			else if(!LD_BUF_valid[LD_BUF_head])begin
					 Dcache_LSQ_data 								<= `SD LD_BUF_data[LD_BUF_head];
					 Dcache_LSQ_rd_addr							<= `SD LD_BUF_addr[LD_BUF_head];
					 Dcache_LSQ_rd_valid						<= `SD 1'b1;
				  //previous version this is both in comb and seq
					 LD_BUF_valid[LD_BUF_head] <= `SD 1'b0; 
					 if(LD_BUF_valid[LD_BUF_head_p1])
					 	LD_BUF_head <= LD_BUF_head + 1'd1;

					 LD_BUF_full <= `SD 1'b0;//able to relase full

					 //cache hit not able to send to LSQ
					 if(next_Dcache_LSQ_rd_valid) begin
					   LD_BUF_data[LD_BUF_tail]  <= `SD next_Dcache_LSQ_data;
					   LD_BUF_addr[LD_BUF_tail]  <= `SD next_Dcache_LSQ_rd_addr;
					   LD_BUF_valid[LD_BUF_tail] <= `SD 1'b1;
						 LD_BUF_tail               <= LD_BUF_tail_p1;//???
						 if (LD_BUF_valid[LD_BUF_tail_p1])
						 		LD_BUF_full <= `SD 1'b1;
						 
					 end                        	
				end	
			else if(next_Dcache_LSQ_rd_valid) begin
					 Dcache_LSQ_data 								<= `SD next_Dcache_LSQ_data;
					 Dcache_LSQ_rd_addr							<= `SD next_Dcache_LSQ_rd_addr;
					 Dcache_LSQ_rd_valid						<= `SD 1'b1;
			end

			if(LSQ_DC_rd_mem) begin
				{DC_Dcache_rd_tag,DC_Dcache_rd_idx} <= `SD LSQ_DC_addr[63:`BLOCK_SIZE_LOG];
			end
			
			//mem ld output write back to cache
			if(MSHR_regfl_valid_command[Dmem_DC_tag]==2'b1 && MSHR_regfl_wr_enable[Dmem_DC_tag])begin

					 DC_Dcache_wr1_data 										<= `SD Dmem_DC_data;
					 {DC_Dcache_wr1_tag,DC_Dcache_wr1_idx} 	<= `SD MSHR_regfl_addr[Dmem_DC_tag][63:`BLOCK_SIZE_LOG];
					 DC_Dcache_wr1_enable 									<= `SD 1'b1;///
					 MSHR_regfl_valid_command[Dmem_DC_tag]<= `SD 2'b0;
					 MSHR_regfl_wr_enable[Dmem_DC_tag]		<= `SD 1'b0;
			   if(LSQ_DC_wr_mem) begin
					   WR_BUF_data[WR_BUF_tail]  <= `SD LSQ_DC_wr_value;
					   WR_BUF_addr[WR_BUF_tail]  <= `SD LSQ_DC_addr;
					   WR_BUF_valid[WR_BUF_tail] <= `SD 1'b1;
						 WR_BUF_tail               <= WR_BUF_tail_p1;//???
						 if (WR_BUF_valid[WR_BUF_tail_p1])
						 		WR_BUF_full <= `SD 1'b1;

				 end
			end
			else if(!WR_BUF_valid[WR_BUF_head])begin
					 DC_Dcache_wr1_data 								  <= `SD WR_BUF_data[WR_BUF_head];
	 				{DC_Dcache_wr1_tag,DC_Dcache_wr1_idx} <= `SD WR_BUF_addr[WR_BUF_head][63:`BLOCK_SIZE_LOG];
					 DC_Dcache_wr1_enable						    <= `SD 1'b1;
					 WR_BUF_valid[WR_BUF_head]          <= `SD 1'b0; 
					 if(WR_BUF_valid[WR_BUF_head_p1])
					 	WR_BUF_head <= WR_BUF_head + 1'd1;

					 WR_BUF_full <= `SD 1'b0;//able to relase full

			   if(LSQ_DC_wr_mem) begin
					   WR_BUF_data[WR_BUF_tail]  <= `SD LSQ_DC_wr_value;
					   WR_BUF_addr[WR_BUF_tail]  <= `SD LSQ_DC_addr;
					   WR_BUF_valid[WR_BUF_tail] <= `SD 1'b1;
						 WR_BUF_tail               <= WR_BUF_tail_p1;//???
						 if (WR_BUF_valid[WR_BUF_tail_p1])
						 		WR_BUF_full <= `SD 1'b1;

				 end
			end	
			else if(LSQ_DC_wr_mem) begin
				{DC_Dcache_wr1_tag,DC_Dcache_wr1_idx} <= `SD LSQ_DC_addr[63:`BLOCK_SIZE_LOG];
				DC_Dcache_wr1_data   							<= `SD LSQ_DC_wr_value;
				DC_Dcache_wr1_enable						<= `SD 1'b1;
			end

    end
  end

endmodule

