
module dcache(
			  input         clock,
			  input         reset,
				//input from LSQ
				input						LSQ_DC_rd_mem,
				input	 [63:0]		LSQ_DC_addr,
				input  					LSQ_DC_wr_mem,
				input	 [63:0]		LSQ_DC_wr_value,
				input	 [`BRAT_SIZE_LOG-1:0]	LSQ_DC_BRAT_num,
				//input from Dcache
			  input  [63:0] Dcache_data,
			  input         Dcache_valid,
				//input from Dmemory
				input  [63:0]	Dmem_DC_data,
				input	 [3:0]	Dmem_DC_response,
				input	 [3:0]	Dmem_DC_tag,
				//input from LD_BUF
				input  [`BRAT_SIZE-1:0]	BRAT_squash,

				//output to LSQ
				output 								MSHR_full,
				output								MSHR_full_1left,
				output	logic [63:0]	Dcache_LSQ_data,
				output	logic [63:0]	Dcache_LSQ_rd_addr,
				output	logic					Dcache_LSQ_rd_valid,
			  //output to Dcache	
				output	logic [`SET_IDX_SIZE_LOG-1:0]	DC_Dcache_wr_idx,
				output	logic [`TAG_SIZE_LOG-1:0]			DC_Dcache_wr_tag,
				output	logic [63:0]									DC_Dcache_wr_data,
				output	logic													DC_Dcache_wr_enable,
				output	logic [`SET_IDX_SIZE_LOG-1:0]	DC_Dcache_rd_idx,
				output	logic [`TAG_SIZE_LOG-1:0]			DC_Dcache_rd_tag,
				//output to Dmem
				output	logic	[63:0]				DC_Dmem_addr,
				output	logic [1:0]					DC_Dmem_command,
				output	logic [63:0]				DC_Dmem_data,
        );


	logic [`MSHR_SIZE_LOG-1:0]	MSHR_free_slot;

  logic [15:0][63:0]								MSHR_regfl_addr;			
	logic [15:0]											MSHR_regfl_valid;
	logic [15:0][`BRAT_SIZE_LOG-1:0]	MSHR_regfl_BRAT_num;

  logic	[63:0]				next_DC_Dmem_addr;
  logic [1:0]					next_DC_Dmem_command;
	logic [63:0]				next_DC_Dmem_data;
  //LD_BUF: for structural hazard between mem ld output and cache ld output 
	logic [`LD_BUF_SIZE-1:0][63:0]	LD_BUF_data;
	logic [`LD_BUF_SIZE-1:0][63:0]	LD_BUF_addr;

  logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_head;
  //logic [`LD_BUF_SIZE_LOG-1:0]		next_LD_BUF_head; //??? head is synch
  //wire [`LD_BUF_SIZE_LOG-1:0]		  LD_BUF_head_p1;
	wire [`LD_BUF_SIZE_LOG:0]	      LD_BUF_cur_size;
  wire [`LD_BUF_SIZE_LOG:0]       next_LD_BUF_iocnt;
  logic [`LD_BUF_SIZE_LOG:0]      LD_BUF_iocnt;
  logic 													LD_BUF_incount;
  logic 													LD_BUF_outcount;
	wire														next_LD_BUF_empty;
  logic														LD_BUF_empty;

	//assign LD_BUF_head_p1 = LD_BUF_head + 1'd1;
  assign LD_BUF_tail_p1 =LD_BUF_tail + 1;
 
	//assign LD_BUF_cur_size = (next_LD_BUF_tail >= next_LD_BUF_head) ? (next_LD_BUF_tail - next_LD_BUF_head) : (next_LD_BUF_tail + `LD_BUF_SIZE - next_LD_BUF_head);
	assign LD_BUF_cur_size = (next_LD_BUF_tail >= next_LD_BUF_head) ? (next_LD_BUF_tail - next_LD_BUF_head) : (next_LD_BUF_tail + `LD_BUF_SIZE - next_LD_BUF_head);
  assign next_LD_BUF_iocnt = LD_BUF_iocnt+LD_BUF_incount-LD_BUF_outcount;//
assign LD_BUF_full = LD_BUF_iocnt ==`LD_BUF_SIZE;
assign next_LD_BUF_empty = next_LD_BUF_iocnt == 0;
	
	always_comb begin
	//default
		next_LD_BUF_tail = LD_BUF_tail;
	  LD_BUF_incount = 2'd0;
		

		if(!Dcache_valid) begin
			if(LSQ_DC_rd_mem)begin
				DC_Dmem_addr 		= LSQ_DC_addr;
				DC_Dmem_command = `BUS_LOAD;
			end
			else if(LSQ_DC_wr_mem)begin
				DC_Dmem_addr    = LSQ_DC_addr;
				DC_Dmem_command = `BUS_STORE;
				DC_Dmem_data		= LSQ_DC_wr_value;
			end
		end
		else begin
			if(LSQ_DC_rd_mem)begin
				LD_BUF_data[LD_BUF_tail] = Dcache_data;
				LD_BUF_addr[LD_BUF_tail] = LSQ_DC_addr; 
				LD_BUF_valid[LD_BUF_tail] = 1'b1;
	      LD_BUF_incount = 1'd1;
		end
	end

  always_ff @(posedge clock)
  begin
    if(reset)
    begin
				DC_Dcache_idx       <= `SD -1;
				DC_Dcache_tag       <= `SD -1;
				DC_Dcache_data      <= `SD -1;
				DC_Dcache_wr_enable	<= `SD 1'b0;
				for(int m = 0; m<16;m++)begin
					MSHR_regfl_valid[m] <= `SD 1'b0;
				end

				LD_BUF_head 	  				<= `SD {`LD_BUF_SIZE_LOG'b0};
				LD_BUF_tail 			  		<= `SD {`LD_BUF_SIZE_LOG'b0};
				LD_BUF_iocnt			  		<= `SD {`LD_BUF_SIZE_LOG+1{1'b0}}; 
				LD_BUF_empty			  		<= `SD 1'b1; 
    end
    else
    begin
				D1_LSQ_DC_rd_mem 		<= `SD LSQ_DC_rd_mem;
				D1_LSQ_DC_addr   		<= `SD LSQ_DC_addr;
				D1_LSQ_DC_BRAT_num 	<= `SD LSQ_DC_BRAT_num;
			  DC_Dcache_wr_enable <= `SD 1'b0;

				LD_BUF_head 	 			<= `SD next_LD_BUF_head;
				LD_BUF_tail 				<= `SD next_LD_BUF_tail;
				LD_BUF_iocnt				<= `SD next_LD_BUF_iocnt; 
				LD_BUF_empty 		 		<= `SD next_LD_BUF_empty; 

			//clear MSHR

			for(int i = 0;i<`MSHR_SIZE;i++)begin
				if(MSHR_regfl_valid[i]) begin
					if(BRAT_squash[MSHR_regfl_BRAT_num[i]])
							MSHR_regfl_valid[i] <= 1'b0;
				 end
			end
		  
			//Send to access memory
		  if(Dmem_DC_response)begin
				MSHR_regfl_addr[Dmem_DC_response] 	<= `SD D1_LSQ_DC_addr;
				MSHR_regfl_BRAT_num[Dmem_DC_response] 	<= `SD D1_LSQ_DC_BRAT_num;
				MSHR_regfl_valid[Dmem_DC_response] 	<= `SD 1'b1;
			end
			
			//Handle data from memory
			if(Dmem_DC_tag & MSHR_regfl_valid[Dmem_DC_tag])begin
					 DC_Dcache_wr_data 										<= `SD Dmem_DC_data;
					 {DC_Dcache_wr_tag,DC_Dcache_wr_idx} 	<= `SD MSHR_regfl_addr[Dmem_DC_tag][31:`BLOCK_SIZE_LOG];
					 DC_Dcache_wr_enable 						<= `SD 1'b1;
					 MSHR_regfl_valid[Dmem_DC_tag]  <= `SD 1'b0;
					 //output to LSQ
					 Dcache_LSQ_data 								<= `SD Dmem_DC_data;
					 Dcache_LSQ_rd_addr							<= `SD MSHR_regfl_addr[Dmem_DC_tag];
					 Dcache_LSQ_rd_valid						<= `SD 1'b1;
			end
			else begin
				if(!LD_BUF_empty)begin
					 Dcache_LSQ_data 								<= `SD LD_BUF_data[LD_BUF_head];
					 Dcache_LSQ_rd_addr							<= `SD LD_BUF_addr[LD_BUF_head];
					 Dcache_LSQ_rd_valid						<= `SD 1'b1;
				   
					 LD_BUF_head <= LD_BUF_head + 1'd1;
				end	
			end

			if(LSQ_DC_rd_mem) begin
				{DC_Dcache_rd_tag,DC_Dcache_rd_idx} <= `SD LSQ_DC_rd_addr[31:`BLOCK_SIZE_LOG];
				DC_Dcache_wr_enable						<= `SD 1'b0;
			end
			else if(LSQ_DC_wr_mem && !MSHR_regfl_valid[Dmem_DC_tag])begin
				{DC_Dcache_wr_tag,DC_Dcache_wr_idx} <= `SD LSQ_DC_wr_addr[31:`BLOCK_SIZE_LOG];
				DC_Dcache_wr_data   							<= `SD LSQ_DC_wr_value;
				DC_Dcache_wr_enable						<= `SD 1'b1;
      end
    end
  end

endmodule

