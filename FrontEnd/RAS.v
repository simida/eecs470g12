//Return Address Stack:
//	speculatively store the register value of $r26 
//	(cause it take a while for the unconditional br to store the $r26 value)
//	So what it does is just a Jump to return forwarding
//	Before jump complete in RS and return address
//However, this value should be correct when the early branch resolution is implemented
//
//	allocate entry when jump occur and  

//Request: Front END must make sure in one cycle only on JUMP or RET will be executed

`define RAS_SIZE 64
`define RAS_LOG2 6

module RAS(
	input logic [63:0] PC,
	input logic pop,
	output logic empty,
	output logic [63:0] predicted_PC,

  input logic push,
	input logic [63:0] push_PC,

	input logic flush,
	input logic [RAS_SIZE:0] [63:0] BRAS,

	input logic clock, reset	
);

reg [RAS_SIZE-1:0] [63:0] registers;
reg [RAS_LOG2:0] stack_head; //stack_head are all 1s -- it is empty: -1

logic [RAS_SIZE-1:0] [63:0] next_registers;
logic [RAS_LOG2:0] next_stack_head;


assign empty=(stack_head==(`RAS_LOG2+1)'hFFFF_FFFF);

always_comb
begin
	next_registers = registers;
	next_stack_head = stack_head;
	predicted_PC = 0;

	if(pop & ~empty)//if stack is not empty
		begin
			next_stack_head = stack_head - 1; 
			predicted_PC = registers[stack_head];
		end

	if(push && (stack_head[RAS_LOG2] != 0) && (stack_head[RAS_LOG2-1:0] != (`RAS_LOG2)'hFFFF_FFFF) )//if the stack is not full
		begin
			next_stack_head = stack_head + 1; 
			next_registers[next_stack_head] = push_PC;
		end

	if(push && (stack_head[RAS_LOG2] == 1) && (stack_head[RAS_LOG2-1:0] == 0) )//if the stack is full
		begin
			next_registers[RAS_SIZE-1] = push_PC;
			next_registers[RAS_SIZE-2:0] =  registers[RAS_SIZE-1:1];
		end
end



always_ff
begin
	if(reset)
		begin	
			for(integer i=0;i<RAS_SIZE;i=i+1) begin
				registers[i] <= `SD 0;
			end
			stack_head <= `SD (`RAS_LOG2+1)'hFFFF_FFFF;
		end
	else
		begin
			registers <= `SD next_registers;
			stack_head <= `SD next_stack_head;
		end
end


endmodule
