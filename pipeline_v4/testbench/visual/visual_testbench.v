/////////////////////////////////////////////////////////////////////////
//                                                                     //
//                                                                     //
//   Modulename :  visual_testbench.v                                  //
//                                                                     //
//  Description :  Testbench module for the verisimple pipeline        //
//                   for the visual debugger                           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////


extern void initcurses(int);
extern void flushpipe();
extern void waitforresponse();
extern void initmem();
extern int get_instr_at_pc(int);
extern int not_valid_pc(int);

module testbench();

  // Registers and wires used in the testbench
  reg        clock;
  reg        reset;
  reg [31:0] clock_count;
  reg [31:0] instr_count;

  wire [1:0]  proc2mem_command;
  wire [63:0] proc2mem_addr;
  wire [63:0] proc2mem_data;
  wire [3:0]  mem2proc_response;
  wire [63:0] mem2proc_data;
  wire [3:0]  mem2proc_tag;

  wire [3:0]  pipeline_completed_insts;
  wire [3:0]  pipeline_error_status;
  wire [1:0][4:0]  pipeline_commit_wr_idx;
  wire [1:0][63:0] pipeline_commit_wr_data;
  wire [1:0]       pipeline_commit_wr_en;
  wire [1:0][63:0] pipeline_commit_NPC;

  integer i;

  // Instantiate the Pipeline
  pipeline pipeline_0 (// Inputs
                       .clock             (clock),
                       .reset             (reset),
                       .mem2proc_response (mem2proc_response),
                       .mem2proc_data     (mem2proc_data),
                       .mem2proc_tag      (mem2proc_tag),
                       
                        // Outputs
                       .proc2mem_command  (proc2mem_command),
                       .proc2mem_addr     (proc2mem_addr),
                       .proc2mem_data     (proc2mem_data),

                       .pipeline_completed_insts(pipeline_completed_insts),
                       .pipeline_error_status(pipeline_error_status),
                       .pipeline_commit_wr_data(pipeline_commit_wr_data),
                       .pipeline_commit_wr_idx(pipeline_commit_wr_idx),
                       .pipeline_commit_wr_en(pipeline_commit_wr_en),
                       .pipeline_commit_NPC(pipeline_commit_NPC)
                      );

  // Instantiate the Data Memory
  mem memory (// Inputs
            .clk               (clock),
            .proc2mem_command  (proc2mem_command),
            .proc2mem_addr     (proc2mem_addr),
            .proc2mem_data     (proc2mem_data),

             // Outputs

            .mem2proc_response (mem2proc_response),
            .mem2proc_data     (mem2proc_data),
            .mem2proc_tag      (mem2proc_tag)
           );

  // Generate System Clock
  always
  begin
    #(`VERILOG_CLOCK_PERIOD/2.0);
    clock = ~clock;
  end

  // Count the number of posedges and number of instructions completed
  // till simulation ends
  always @(posedge clock)
  begin
    if(reset)
    begin
      clock_count <= `SD 0;
      instr_count <= `SD 0;
    end
    else
    begin
      clock_count <= `SD (clock_count + 1);
      instr_count <= `SD (instr_count + pipeline_completed_insts);
    end
  end  

  initial
  begin
    clock = 0;
    reset = 0;

    // Call to initialize visual debugger
    // *Note that after this, all stdout output goes to visual debugger*
    // each argument is number of registers/signals for the group
    // (IF, IF/ID, ID, ID/EX, EX, EX/MEM, MEM, MEM/WB, WB, Misc)
    initcurses(7);

    // Pulse the reset signal
    reset = 1'b1;
    @(posedge clock);
    @(posedge clock);

    // Read program contents into memory array
    $readmemh("program.mem", memory.unified_memory);

    @(posedge clock);
    @(posedge clock);
    `SD;
    // This reset is at an odd time to avoid the pos & neg clock edges
    reset = 1'b0;
  end

  always @(negedge clock)
  begin
    if(!reset)
    begin
      `SD;
      `SD;

      // deal with any halting conditions
      if(pipeline_error_status!=`NO_ERROR)
      begin
        #100
        $display("\nDONE\n");
        waitforresponse();
        flushpipe();
        $finish;
      end

    end
  end 

  // This block is where we dump all of the signals that we care about to
  // the visual debugger.  Notice this happens at *every* clock edge.
  always @(clock) begin
    #2;

    // Dump clock and time onto stdout
    $display("c%h%7.0d",clock,clock_count);
    $display("t%8.0f",$time);
    $display("z%h",reset);

    // Dump interesting register/signal contents onto stdout
    // format is "<reg group prefix><name> <width in hex chars>:<data>"
    // Current register groups (and prefixes) are:
    // f: IF   d: ID   e: EX   m: MEM    w: WB  v: misc. reg
    // g: IF/ID   h: ID/EX  i: EX/MEM  j: MEM/WB

    // RAT signals (32) - prefix 'a'
    for (i=0; i<32; i++) begin
	$display("a 2:%02d",  pipeline_0.FrontEnd_0.RAT1.registers[i]);
    end

    // RRAT signals (32) - prefix 'r'
    for (i=0; i<32; i++) begin
	$display("r 2:%02d", pipeline_0.FrontEnd_0.RRAT1.registers[i]);
    end

    // PRF signals (64) - prefix 'p'
    for (i=0; i<64; i++) begin
	$write("p 21:%014d", pipeline_0.Issue_Stage.PRF.registers[i]);
	if (pipeline_0.IS_ID_valid_list[i]) $display("-V");
	else $display("-N");
    end


    $display("o 20:%02d", pipeline_0.Complete_Retire_Stage.ROB_head);
    $display("o 20:%02d", pipeline_0.Complete_Retire_Stage.ROB_tail);
    // ROB signals (32) - prefix 'o'
    for (i=0; i<32; i++) begin
	if ((pipeline_0.Complete_Retire_Stage.ROB_head<=i && i<pipeline_0.Complete_Retire_Stage.ROB_tail ) || 
		((Complete_Retire_Stage.ROB_head>pipeline_0.Complete_Retire_Stage.ROB_tail) && (i<pipeline_0.Complete_Retire_Stage.ROB_tail | i >=pipeline_0.Complete_Retire_Stage.ROB_head))) begin
		if (pipeline_0.Complete_Retire_Stage.ex_fin_regfl[i])
			$display("o 20:%s,a%02d,p%02d,%010h","C", pipeline_0.Complete_Retire_Stage.arfdest_regfl[i],pipeline_0.Complete_Retire_Stage.prfdest_regfl[i],pipeline_0.Complete_Retire_Stage.npc_regfl[i][39:0]);
		else 
			$display("o 20:%s,a%02d,p%02d,%010h","N", pipeline_0.Complete_Retire_Stage.arfdest_regfl[i],pipeline_0.Complete_Retire_Stage.prfdest_regfl[i],pipeline_0.Complete_Retire_Stage.npc_regfl[i][39:0]);
	end
	else $display("o 20:--------------------");
    end

    $display("q 20:%02d", pipeline_0.FrontEnd_0.FRL.head);
    $display("q 20:%02d", pipeline_0.FrontEnd_0.FRL.tail);
    // Freelist signals (32) - prefix 'q'
    for (i=0; i<32; i++) begin
	if ((pipeline_0.FrontEnd_0.FRL.head<=i && i<pipeline_0.FrontEnd_0.FRL.tail ) || 
		((pipeline_0.FrontEnd_0.FRL.head>=pipeline_0.FrontEnd_0.FRL.tail) && (i<pipeline_0.FrontEnd_0.FRL.tail | i >=pipeline_0.FrontEnd_0.FRL.head))) begin
			$display("q 20:%02d", pipeline_0.FrontEnd_0.FRL.FRL_registers[i]);
	end
	else $display("q 20:--");
    end

    // dispatch signals (4) - prefix 'w'
    $display("w95:---PC=%016h---Icache_data:%016h---Icache_valid:%0b-----------------",
    pipeline_0.FrontEnd_0.PC,pipeline_0.FrontEnd_0.cachemem_data,pipeline_0.FrontEnd_0.cachemem_valid);


    // dispatch signals (4) - prefix 'd'
    for (i=0; i<2; i++) begin
	if (pipeline_0.FE_ID_valid_inst[i])  begin
		$display("d95:---NPC=%010h---PRF_RegA:p%02d---PRF_RegB:p%02d---ARF_destR:r%02d---PRF_destR:p%02d",
		pipeline_0.FE_ID_NPC[i],pipeline_0.FE_ID_regA[i],pipeline_0.FE_ID_regB[i],pipeline_0.FE_ID_dest_arfreg[i],pipeline_0.FE_ID_dest_reg[i]);
		$display("d95:---IR=%s---ROB#=%02d---opa_select=%d---opb_select=%d",get_instr_string(pipeline_0.FE_ID_IR[i], pipeline_0.FE_ID_valid_inst[i]),pipeline_0.ROB_tail+i,pipeline_0.FE_ID_opa_select[i],pipeline_0.FE_ID_opb_select[i]);
	end
	else begin
		$display("d95:--------------------------------------------------------------------------------------------");
		$display("d95:--------------------------------------------------------------------------------------------");
	end
    end

    // RS signals (8) - prefix 's'
    for (i=0; i<8; i++) begin
	if (pipeline_0.Dispatch_Stage.ALU.in_use[i])
		$write("s30:ROB%02d--rA:%02d-%s--rB:%02d-%c|", pipeline_0.Dispatch_Stage.ALU.ROB[i],pipeline_0.Dispatch_Stage.ALU.opa[i],Dispatch_Stage.ALU.opa_valid[i]?"V":"N",
		pipeline_0.Dispatch_Stage.ALU.opb[i],Dispatch_Stage.ALU.opb_valid[i]?"V":"N");
	else 
		$write("s30:-----------------------|");
	$display("");
    end

    for (i=0; i<4; i++) begin
	if (pipeline_0.Dispatch_Stage.BR.in_use[i])
		$write("s30:ROB%02d--rA:%02d-%s--rB:%02d-%c|", pipeline_0.Dispatch_Stage.BR.ROB[i],pipeline_0.Dispatch_Stage.BR.opa[i],Dispatch_Stage.BR.opa_valid[i]?"V":"N",
		pipeline_0.Dispatch_Stage.BR.opb[i],Dispatch_Stage.BR.opb_valid[i]?"V":"N");
	else 
		$write("s30:-----------------------|");
	$display("");
    end
    for (i=0; i<4; i++) begin
	if (pipeline_0.Dispatch_Stage.MULT.in_use[i])
		$write("s30:ROB%02d--rA:%02d-%s--rB:%02d-%c|", pipeline_0.Dispatch_Stage.MULT.ROB[i],pipeline_0.Dispatch_Stage.MULT.opa[i],Dispatch_Stage.MULT.opa_valid[i]?"V":"N",
		pipeline_0.Dispatch_Stage.MULT.opb[i],Dispatch_Stage.MULT.opb_valid[i]?"V":"N");
	else 
		$write("s30:-----------------------|");
	$display("");
    end
    for (i=0; i<8; i++) begin
	if (pipeline_0.Dispatch_Stage.MEM.in_use[i])
		$write("s30:ROB%02d--regA:%02d--rB:%02d-%c", pipeline_0.Dispatch_Stage.MEM.ROB[i],pipeline_0.Dispatch_Stage.MEM.opa[i],
		pipeline_0.Dispatch_Stage.MEM.opb[i],Dispatch_Stage.MEM.opb_valid[i]?"V":"N");
	else 
		$write("s30:------------------------");
	$display("");
    end


    // SQ signals (8) - prefix 'k'
    for (i=0; i<8; i++) begin
	if (pipeline_0.LSQ.sq1.sq_valid[i])
		$write("k30:ROB%02d-rA:%02d-%s-addr:%013h-%s", pipeline_0.LSQ.sq1.sq_ROB[i], pipeline_0.LSQ.sq1.sq_regB[i],pipeline_0.IS_ID_valid_list[pipeline_0.LSQ.sq1.sq_regB[i]]?"V":"N",
		pipeline_0.LSQ.sq1.sq_MEM_addr[i][51:0],pipeline_0.LSQ.sq1.sq_addr_valid[i]?"V":"N");
	else 
		$write("k30:-----------------------------------");
	$display("");
    end


    // LQ signals (8) - prefix 'u'
    for (i=0; i<8; i++) begin
	if (pipeline_0.LSQ.lq1.lq_valid[i])
		$write("u30:ROB%02d-rA:%02d-addr:%016h", pipeline_0.LSQ.lq1.lq_ROB[i],pipeline_0.LSQ.lq1.lq_regA[i],pipeline_0.LSQ.lq1.lq_MEM_addr[i]);
	else 
		$write("u30:-----------------------------------");
	$display("");
    end

    // Dcache signals (8) - prefix 'j'
    for (i=0; i<15; i++) begin
	if (pipeline_0.Dcache.MSHR_regfl_valid_command[i])
		$write("j30:ROB%02d-rA:%02d-addr:%016h", pipeline_0.Dcache.MSHR_regfl_ROB_num[i],pipeline_0.Dcache.MSHR_regfl_wr_enable[i],pipeline_0.Dcache.MSHR_regfl_addr[i]);
	else 
		$write("j30:-----------------------------------");
	$display("");
    end
    if (pipeline_0.proc2Dmem_command==`BUS_STORE)
	$display("j30:proc2Dmem_command:BUS_STORE");
    else if ( pipeline_0.proc2Dmem_command==`BUS_LOAD)
	$display("j30:proc2Dmem_command:BUS_LOAD!"); 
    else 
	$display("j30:proc2Dmem_command:BUS_NONE!");
    $display("j30:proc2Dmem_addr:%016h", pipeline_0.proc2Dmem_addr);
    $display("j30:proc2Dmem_data:%016h", pipeline_0.proc2mem_data);
    $display("j30:Dmem2proc_response:%02d", pipeline_0.Dmem2proc_response);
    $display("j30:Dmem2proc_tag:%02d", pipeline_0.mem2proc_tag);
    $display("j30:Dmem2proc_data:%016h", pipeline_0.mem2proc_data);

    // issue signals (8) - prefix 'i'
    for (i=0; i<4; i++) begin
	for (int j=0; j<2; j++) begin
		if (pipeline_0.ID_IS_rs_valid[i][j])  begin
			$display("i95:---NPC=%010h---IR=%s---ROB#=%02d---PRF_RegA:p%02d---PRF_RegB:p%02d---PRF_destR:p%02d",
			pipeline_0.ID_IS_NPC[i][j],get_instr_string(pipeline_0.ID_IS_IR[i][j], pipeline_0.ID_IS_rs_valid[i][j]), pipeline_0.ID_IS_ROB[i][j],
			pipeline_0.ID_IS_regA[i][j],pipeline_0.ID_IS_regB[i][j],pipeline_0.ID_IS_dest[i][j]);
		end
		else begin
			$display("i95:---------------------------------------------------------------------------------------------");
		end
	end
    end

    // FU signals (8) - prefix 'f'
    for (i=0; i<4; i++) begin
	for (int j=0; j<2; j++) begin
		if (pipeline_0.Execute_Stage.FU_valid_out[i][j])  begin
			$display("f95:---NPC=%010h---IR=%s---ROB#=%02d---Result=%016h---PRF_destR:p%02d",
			pipeline_0.Execute_Stage.FU_NPC_out[i][j],get_instr_string(pipeline_0.Execute_Stage.FU_IR_out[i][j], pipeline_0.Execute_Stage.FU_valid_out[i][j]), pipeline_0.Execute_Stage.FU_ROB_out[i][j],
			pipeline_0.Execute_Stage.FU_result_out[i][j],pipeline_0.Execute_Stage.FU_dest_out[i][j]);
		end
		else begin
			$display("f95:---------------------------------------------------------------------------------------------");
		end
	end
    end

    // complete signals (4) - prefix 'l'
    for (i=0; i<2; i++) begin
	if (pipeline_0.EX_CMP_valid[i])  begin
		$display("l50:---NPC=%010h---IR=%s---ROB#=%02d",pipeline_0.EX_CMP_NPC[i],get_instr_string(pipeline_0.EX_CMP_IR[i],1),pipeline_0.EX_CMP_ROB[i]);
		$display("l50:---PRF%02d---Result=%010h---Branch_taken=%d",pipeline_0.EX_CMP_dest[i],pipeline_0.EX_CMP_result[i][39:0],pipeline_0.EX_CMP_branch[i]);
	end
	else begin
		$display("l50:--------------------------------------------------");
		$display("l50:--------------------------------------------------");
	end
    end

    // retire signals (4) - prefix 'e'
    for (i=0; i<2; i++) begin
	if (pipeline_0.RET_valid[i])  begin
		$display("e60:---NPC=%010h---ARF%02d---PRF%02d---inst:%04d",pipeline_0.Complete_Retire_Stage.npc_regfl[pipeline_0.ROB_head-2+i][39:0],pipeline_0.RET_arfn[i],pipeline_0.RET_prfn[i],instr_count+i);
		$display("e60:--------------------------------------------------");
	end
	else begin
		$display("e60:--------------------------------------------------");
		$display("e60:--------------------------------------------------");
	end
    end

    // misc signals (3) - prefix 'v'
    if (pipeline_0.BRAT_CMP_miss) $display ("v 64:BRANCH_MISS!!!");
    else $display("v 64:BRANCH_DOES_NOT_MISS!!!");
    $display("v 64:BRAT_Squash:%04b", pipeline_0.BRAT_squash);
    $display("v 64:RS_full:%b", pipeline_0.RS_full);
    $display("v 64:ROB_full:%b%b", pipeline_0.ROB_full, pipeline_0.ROB_full_1left| pipeline_0.ROB_full);
    $display("v 64:BRAT_full:%b%b", pipeline_0.BRAT_full, pipeline_0.BRAT_full_1left | pipeline_0.BRAT_full);
    $display("v 64:EX_IS_stall:%b", pipeline_0.EX_IS_stall);
    $display("v 64:IS_ID_Not_Erase:%b", pipeline_0.IS_ID_Not_Erase);
    // must come last
    $display("break");

    // This is a blocking call to allow the debugger to control when we
    // advance the simulation
    waitforresponse();
  end

  function [8*7-1:0] get_instr_string;
    input [31:0] IR;
    input        instr_valid;
    begin
      if (!instr_valid)
        get_instr_string = "--------";
      else if (IR==`NOOP_INST)
        get_instr_string = "_____nop";
      else
        case (IR[31:26])
          6'h00: get_instr_string = (IR == 32'h555) ? "____halt" : "call_pal";
          6'h08: get_instr_string = "_____lda";
          6'h09: get_instr_string = "____ldah";
          6'h0a: get_instr_string = "____ldbu";
          6'h0b: get_instr_string = "____ldqu";
          6'h0c: get_instr_string = "____ldwu";
          6'h0d: get_instr_string = "_____stw";
          6'h0e: get_instr_string = "_____stb";
          6'h0f: get_instr_string = "____stqu";
          6'h10: // INTA_GRP
            begin
              case (IR[11:5])
                7'h00: get_instr_string = "____addl";
                7'h02: get_instr_string = "__s4addl";
                7'h09: get_instr_string = "____subl";
                7'h0b: get_instr_string = "__s4subl";
                7'h0f: get_instr_string = "__cmpbge";
                7'h12: get_instr_string = "__s8addl";
                7'h1b: get_instr_string = "__s8subl";
                7'h1d: get_instr_string = "__cmpult";
                7'h20: get_instr_string = "____addq";
                7'h22: get_instr_string = "__s4addq";
                7'h29: get_instr_string = "____subq";
                7'h2b: get_instr_string = "__s4subq";
                7'h2d: get_instr_string = "___cmpeq";
                7'h32: get_instr_string = "__s8addq";
                7'h3b: get_instr_string = "__s8subq";
                7'h3d: get_instr_string = "__cmpule";
                7'h40: get_instr_string = "___addlv";
                7'h49: get_instr_string = "___sublv";
                7'h4d: get_instr_string = "___cmplt";
                7'h60: get_instr_string = "___addqv";
                7'h69: get_instr_string = "___subqv";
                7'h6d: get_instr_string = "___cmple";
                default: get_instr_string = "_invalid";
              endcase
            end
          6'h11: // INTL_GRP
            begin
              case (IR[11:5])
                7'h00: get_instr_string = "_____and";
                7'h08: get_instr_string = "_____bic";
                7'h14: get_instr_string = "_cmovlbs";
                7'h16: get_instr_string = "_cmovlbc";
                7'h20: get_instr_string = "_____bis";
                7'h24: get_instr_string = "__cmoveq";
                7'h26: get_instr_string = "__cmovne";
                7'h28: get_instr_string = "___ornot";
                7'h40: get_instr_string = "_____xor";
                7'h44: get_instr_string = "__cmovlt";
                7'h46: get_instr_string = "__cmovge";
                7'h48: get_instr_string = "_____eqv";
                7'h61: get_instr_string = "___amask";
                7'h64: get_instr_string = "__cmovle";
                7'h66: get_instr_string = "__cmovgt";
                7'h6c: get_instr_string = "_implver";
                default: get_instr_string = "_invalid";
              endcase
            end
          6'h12: // INTS_GRP
            begin
              case(IR[11:5])
                7'h02: get_instr_string = "___mskbl";
                7'h06: get_instr_string = "___extbl";
                7'h0b: get_instr_string = "___insbl";
                7'h12: get_instr_string = "___mskwl";
                7'h16: get_instr_string = "___extwl";
                7'h1b: get_instr_string = "___inswl";
                7'h22: get_instr_string = "___mskll";
                7'h26: get_instr_string = "___extll";
                7'h2b: get_instr_string = "___insll";
                7'h30: get_instr_string = "_____zap";
                7'h31: get_instr_string = "__zapnot";
                7'h32: get_instr_string = "___mskql";
                7'h34: get_instr_string = "_____srl";
                7'h36: get_instr_string = "___extql";
                7'h39: get_instr_string = "_____sll";
                7'h3b: get_instr_string = "___insql";
                7'h3c: get_instr_string = "_____sra";
                7'h52: get_instr_string = "___mskwh";
                7'h57: get_instr_string = "___inswh";
                7'h5a: get_instr_string = "___extwh";
                7'h62: get_instr_string = "___msklh";
                7'h67: get_instr_string = "___inslh";
                7'h6a: get_instr_string = "___extlh";
                7'h72: get_instr_string = "___mskqh";
                7'h77: get_instr_string = "___insqh";
                7'h7a: get_instr_string = "___extqh";
                default: get_instr_string = "_invalid";
              endcase
            end
          6'h13: // INTM_GRP
            begin
              case (IR[11:5])
                7'h01: get_instr_string = "____mull";
                7'h20: get_instr_string = "____mulq";
                7'h30: get_instr_string = "____umulh";
                7'h40: get_instr_string = "____mullv";
                7'h60: get_instr_string = "____mulqv";
                default: get_instr_string = "_invalid";
              endcase
            end
          6'h14: get_instr_string = "____itfp"; // unimplemented
          6'h15: get_instr_string = "____fltv"; // unimplemented
          6'h16: get_instr_string = "____flti"; // unimplemented
          6'h17: get_instr_string = "____fltl"; // unimplemented
          6'h1a: get_instr_string = "_____jsr";
          6'h1c: get_instr_string = "____ftpi";
          6'h20: get_instr_string = "_____ldf";
          6'h21: get_instr_string = "_____ldg";
          6'h22: get_instr_string = "_____lds";
          6'h23: get_instr_string = "_____ldt";
          6'h24: get_instr_string = "_____stf";
          6'h25: get_instr_string = "_____stg";
          6'h26: get_instr_string = "_____sts";
          6'h27: get_instr_string = "_____stt";
          6'h28: get_instr_string = "_____ldl";
          6'h29: get_instr_string = "_____ldq";
          6'h2a: get_instr_string = "____ldll";
          6'h2b: get_instr_string = "____ldql";
          6'h2c: get_instr_string = "_____stl";
          6'h2d: get_instr_string = "_____stq";
          6'h2e: get_instr_string = "____stlc";
          6'h2f: get_instr_string = "____stqc";
          6'h30: get_instr_string = "______br";
          6'h31: get_instr_string = "____fbeq";
          6'h32: get_instr_string = "____fblt";
          6'h33: get_instr_string = "____fble";
          6'h34: get_instr_string = "_____bsr";
          6'h35: get_instr_string = "____fbne";
          6'h36: get_instr_string = "____fbge";
          6'h37: get_instr_string = "____fbgt";
          6'h38: get_instr_string = "____blbc";
          6'h39: get_instr_string = "_____beq";
          6'h3a: get_instr_string = "_____blt";
          6'h3b: get_instr_string = "_____ble";
          6'h3c: get_instr_string = "____blbs";
          6'h3d: get_instr_string = "_____bne";
          6'h3e: get_instr_string = "_____bge";
          6'h3f: get_instr_string = "_____bgt";
          default: get_instr_string = "_invalid";
        endcase
    end
  endfunction

endmodule
