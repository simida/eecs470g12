# Begin_DVE_Session_Save_Info
# DVE full session
# Saved on Wed Nov 27 05:54:03 2013
# Designs open: 1
#   Sim: /afs/umich.edu/user/p/a/paulcao/eecs470/Project/eecs470g12/pipeline_v4/dve_simv
# Toplevel windows open: 1
# 	TopLevel.2
#   Wave.1: 107 signals
#   Group count = 1
#   Group Dcache signal count = 107
# End_DVE_Session_Save_Info

# DVE version: E-2011.03_Full64
# DVE build date: Feb 23 2011 21:10:05


#<Session mode="Full" path="/afs/umich.edu/user/p/a/paulcao/eecs470/Project/eecs470g12/pipeline_v4/DVEfiles/session.tcl" type="Debug">

gui_set_loading_session_type Post
gui_continuetime_set -value 90000

# Close design
if { [gui_sim_state -check active] } {
    gui_sim_terminate
}
gui_close_db -all
gui_expr_clear_all

# Close all windows
gui_close_window -type Console
gui_close_window -type Wave
gui_close_window -type Source
gui_close_window -type Schematic
gui_close_window -type Data
gui_close_window -type DriverLoad
gui_close_window -type List
gui_close_window -type Memory
gui_close_window -type HSPane
gui_close_window -type DLPane
gui_close_window -type Assertion
gui_close_window -type CovHier
gui_close_window -type CoverageTable
gui_close_window -type CoverageMap
gui_close_window -type CovDensity
gui_close_window -type CovDetail
gui_close_window -type Local
gui_close_window -type Stack
gui_close_window -type Watch
gui_close_window -type Grading
gui_close_window -type Group
gui_close_window -type Transaction



# Application preferences
gui_set_pref_value -key app_default_font -value {Helvetica,10,-1,5,50,0,0,0,0,0}
gui_src_preferences -tabstop 8 -maxbits 24 -windownumber 1
#<WindowLayout>

# DVE Topleve session: 


# Create and position top-level windows :TopLevel.2

if {![gui_exist_window -window TopLevel.2]} {
    set TopLevel.2 [ gui_create_window -type TopLevel \
       -icon $::env(DVE)/auxx/gui/images/toolbars/dvewin.xpm] 
} else { 
    set TopLevel.2 TopLevel.2
}
gui_show_window -window ${TopLevel.2} -show_state normal -rect {{64 60} {2555 1405}}

# ToolBar settings
gui_set_toolbar_attributes -toolbar {TimeOperations} -dock_state top
gui_set_toolbar_attributes -toolbar {TimeOperations} -offset 0
gui_show_toolbar -toolbar {TimeOperations}
gui_set_toolbar_attributes -toolbar {&File} -dock_state top
gui_set_toolbar_attributes -toolbar {&File} -offset 0
gui_show_toolbar -toolbar {&File}
gui_set_toolbar_attributes -toolbar {&Edit} -dock_state top
gui_set_toolbar_attributes -toolbar {&Edit} -offset 0
gui_show_toolbar -toolbar {&Edit}
gui_set_toolbar_attributes -toolbar {Simulator} -dock_state top
gui_set_toolbar_attributes -toolbar {Simulator} -offset 0
gui_show_toolbar -toolbar {Simulator}
gui_set_toolbar_attributes -toolbar {Interactive Rewind} -dock_state top
gui_set_toolbar_attributes -toolbar {Interactive Rewind} -offset 0
gui_show_toolbar -toolbar {Interactive Rewind}
gui_set_toolbar_attributes -toolbar {Signal} -dock_state top
gui_set_toolbar_attributes -toolbar {Signal} -offset 0
gui_show_toolbar -toolbar {Signal}
gui_set_toolbar_attributes -toolbar {&Scope} -dock_state top
gui_set_toolbar_attributes -toolbar {&Scope} -offset 0
gui_show_toolbar -toolbar {&Scope}
gui_set_toolbar_attributes -toolbar {&Trace} -dock_state top
gui_set_toolbar_attributes -toolbar {&Trace} -offset 0
gui_show_toolbar -toolbar {&Trace}
gui_set_toolbar_attributes -toolbar {BackTrace} -dock_state top
gui_set_toolbar_attributes -toolbar {BackTrace} -offset 0
gui_show_toolbar -toolbar {BackTrace}
gui_set_toolbar_attributes -toolbar {&Window} -dock_state top
gui_set_toolbar_attributes -toolbar {&Window} -offset 0
gui_show_toolbar -toolbar {&Window}
gui_set_toolbar_attributes -toolbar {Zoom} -dock_state top
gui_set_toolbar_attributes -toolbar {Zoom} -offset 0
gui_show_toolbar -toolbar {Zoom}
gui_set_toolbar_attributes -toolbar {Zoom And Pan History} -dock_state top
gui_set_toolbar_attributes -toolbar {Zoom And Pan History} -offset 0
gui_show_toolbar -toolbar {Zoom And Pan History}
gui_set_toolbar_attributes -toolbar {Grid} -dock_state top
gui_set_toolbar_attributes -toolbar {Grid} -offset 0
gui_show_toolbar -toolbar {Grid}

# End ToolBar settings

# Docked window settings
gui_sync_global -id ${TopLevel.2} -option true

# MDI window settings
set Wave.1 [gui_create_window -type {Wave}  -parent ${TopLevel.2}]
gui_show_window -window ${Wave.1} -show_state maximized
gui_update_layout -id ${Wave.1} {{show_state maximized} {dock_state undocked} {dock_on_new_line false} {child_wave_left 723} {child_wave_right 1763} {child_wave_colname 345} {child_wave_colvalue 374} {child_wave_col1 0} {child_wave_col2 1}}

# End MDI window settings

gui_set_env TOPLEVELS::TARGET_FRAME(Source) none
gui_set_env TOPLEVELS::TARGET_FRAME(Schematic) none
gui_set_env TOPLEVELS::TARGET_FRAME(PathSchematic) none
gui_set_env TOPLEVELS::TARGET_FRAME(Wave) none
gui_set_env TOPLEVELS::TARGET_FRAME(List) none
gui_set_env TOPLEVELS::TARGET_FRAME(Memory) none
gui_set_env TOPLEVELS::TARGET_FRAME(DriverLoad) none
gui_update_statusbar_target_frame ${TopLevel.2}

#</WindowLayout>

#<Database>

# DVE Open design session: 

if { [llength [lindex [gui_get_db -design Sim] 0]] == 0 } {
gui_set_env SIMSETUP::SIMARGS {{}}
gui_set_env SIMSETUP::SIMEXE {./dve_simv}
gui_set_env SIMSETUP::ALLOW_POLL {0}
if { ![gui_is_db_opened -db {/afs/umich.edu/user/p/a/paulcao/eecs470/Project/eecs470g12/pipeline_v4/dve_simv}] } {
gui_sim_run Ucli -exe dve_simv -args {-ucligui } -dir /afs/umich.edu/user/p/a/paulcao/eecs470/Project/eecs470g12/pipeline_v4 -nosource
}
}
if { ![gui_sim_state -check active] } {error "Simulator did not start correctly" error}
gui_set_precision 1s
gui_set_time_units 1s
#</Database>

# DVE Global setting session: 


# Global: Breakpoints

# Global: Bus

# Global: Expressions

# Global: Signal Time Shift

# Global: Signal Compare

# Global: Signal Groups
gui_load_child_values {testbench.pipeline.Dcache}

set Dcache Dcache
gui_sg_create ${Dcache}
gui_sg_addsignal -group ${Dcache} { testbench.pipeline.Dcache.BRAT_CMT_num testbench.pipeline.Dcache.BRAT_CMT_v testbench.pipeline.Dcache.BRAT_squash testbench.pipeline.Dcache.D1_DC_Dcache_wr1_enable testbench.pipeline.Dcache.D1_LSQ_DC_BRAT_num testbench.pipeline.Dcache.D1_LSQ_DC_BRAT_numreg testbench.pipeline.Dcache.D1_LSQ_DC_IR testbench.pipeline.Dcache.D1_LSQ_DC_NPC testbench.pipeline.Dcache.D1_LSQ_DC_PRF_num testbench.pipeline.Dcache.D1_LSQ_DC_ROB_num testbench.pipeline.Dcache.D1_LSQ_DC_addr testbench.pipeline.Dcache.D1_LSQ_DC_rd_mem testbench.pipeline.Dcache.D1_LSQ_DC_rd_memreg testbench.pipeline.Dcache.D1_LSQ_DC_wr_mem testbench.pipeline.Dcache.D1_LSQ_DC_wr_value testbench.pipeline.Dcache.DC_Dcache_rd_idx testbench.pipeline.Dcache.DC_Dcache_rd_tag testbench.pipeline.Dcache.DC_Dcache_wr1_data testbench.pipeline.Dcache.DC_Dcache_wr1_enable testbench.pipeline.Dcache.DC_Dcache_wr1_idx testbench.pipeline.Dcache.DC_Dcache_wr1_tag testbench.pipeline.Dcache.DC_Dcache_wr2_data testbench.pipeline.Dcache.DC_Dcache_wr2_enable testbench.pipeline.Dcache.DC_Dcache_wr2_idx testbench.pipeline.Dcache.DC_Dcache_wr2_tag testbench.pipeline.Dcache.DC_Dmem_addr testbench.pipeline.Dcache.DC_Dmem_command testbench.pipeline.Dcache.DC_Dmem_data testbench.pipeline.Dcache.Dcache_LSQ_BRAT_num testbench.pipeline.Dcache.Dcache_LSQ_BRAT_numreg testbench.pipeline.Dcache.Dcache_LSQ_PRF_num testbench.pipeline.Dcache.Dcache_LSQ_ROB_num testbench.pipeline.Dcache.Dcache_LSQ_data testbench.pipeline.Dcache.Dcache_LSQ_rd_IR testbench.pipeline.Dcache.Dcache_LSQ_rd_NPC testbench.pipeline.Dcache.Dcache_LSQ_rd_addr testbench.pipeline.Dcache.Dcache_LSQ_rd_valid testbench.pipeline.Dcache.Dcache_LSQ_rd_validreg testbench.pipeline.Dcache.Dcache_data testbench.pipeline.Dcache.Dcache_out testbench.pipeline.Dcache.Dcache_valid testbench.pipeline.Dcache.Dmem_DC_data testbench.pipeline.Dcache.Dmem_DC_response testbench.pipeline.Dcache.Dmem_DC_tag testbench.pipeline.Dcache.Dmem_out testbench.pipeline.Dcache.LD_BUF_BRAT_num testbench.pipeline.Dcache.LD_BUF_BRAT_numreg testbench.pipeline.Dcache.LD_BUF_IR testbench.pipeline.Dcache.LD_BUF_NPC testbench.pipeline.Dcache.LD_BUF_PRF_num testbench.pipeline.Dcache.LD_BUF_ROB_num testbench.pipeline.Dcache.LD_BUF_addr testbench.pipeline.Dcache.LD_BUF_data testbench.pipeline.Dcache.LD_BUF_empty testbench.pipeline.Dcache.LD_BUF_full testbench.pipeline.Dcache.LD_BUF_in testbench.pipeline.Dcache.LD_BUF_in_entry testbench.pipeline.Dcache.LD_BUF_invalid testbench.pipeline.Dcache.LD_BUF_out testbench.pipeline.Dcache.LD_BUF_out_entry {testbench.pipeline.Dcache.LD_BUF_valid[1]} testbench.pipeline.Dcache.LD_BUF_valid testbench.pipeline.Dcache.clock testbench.pipeline.Dcache.LD_BUF_validreg testbench.pipeline.Dcache.LSQ_DC_BRAT_num testbench.pipeline.Dcache.LSQ_DC_IR testbench.pipeline.Dcache.LSQ_DC_NPC testbench.pipeline.Dcache.LSQ_DC_PRF_num testbench.pipeline.Dcache.LSQ_DC_ROB_num testbench.pipeline.Dcache.LSQ_DC_addr testbench.pipeline.Dcache.LSQ_DC_rd_mem testbench.pipeline.Dcache.LSQ_DC_wr_mem testbench.pipeline.Dcache.LSQ_DC_wr_value testbench.pipeline.Dcache.MSHR_count testbench.pipeline.Dcache.MSHR_full testbench.pipeline.Dcache.MSHR_regfl_BRAT_num testbench.pipeline.Dcache.MSHR_regfl_BRAT_numreg testbench.pipeline.Dcache.MSHR_regfl_IR testbench.pipeline.Dcache.MSHR_regfl_NPC testbench.pipeline.Dcache.MSHR_regfl_PRF_num testbench.pipeline.Dcache.MSHR_regfl_ROB_num testbench.pipeline.Dcache.MSHR_regfl_addr testbench.pipeline.Dcache.MSHR_regfl_shifter testbench.pipeline.Dcache.MSHR_regfl_valid_command testbench.pipeline.Dcache.MSHR_regfl_valid_commandreg testbench.pipeline.Dcache.MSHR_regfl_wr_enable testbench.pipeline.Dcache.MSHR_regfl_wr_enablereg testbench.pipeline.Dcache.mem2proc_response testbench.pipeline.Dcache.mem2proc_tag testbench.pipeline.Dcache.next_D1_LSQ_DC_BRAT_num testbench.pipeline.Dcache.next_D1_LSQ_DC_rd_mem testbench.pipeline.Dcache.next_D1_LSQ_DC_wr_mem testbench.pipeline.Dcache.next_DC_Dmem_addr testbench.pipeline.Dcache.next_DC_Dmem_command testbench.pipeline.Dcache.next_DC_Dmem_data }
gui_sg_addsignal -group ${Dcache} { testbench.pipeline.Dcache.next_Dcache_LSQ_data testbench.pipeline.Dcache.next_Dcache_LSQ_rd_IR testbench.pipeline.Dcache.next_Dcache_LSQ_rd_NPC testbench.pipeline.Dcache.next_Dcache_LSQ_rd_addr testbench.pipeline.Dcache.next_Dcache_LSQ_rd_valid testbench.pipeline.Dcache.next_LD_BUF_empty testbench.pipeline.Dcache.next_LSQ_DC_BRAT_num testbench.pipeline.Dcache.next_LSQ_DC_PRF_num testbench.pipeline.Dcache.next_LSQ_DC_ROB_num testbench.pipeline.Dcache.next_MSHR_count testbench.pipeline.Dcache.next_MSHR_regfl_shifter testbench.pipeline.Dcache.reset }
gui_set_radix -radix {decimal} -signals {Sim:testbench.pipeline.Dcache.Dcache_LSQ_ROB_num}
gui_set_radix -radix {unsigned} -signals {Sim:testbench.pipeline.Dcache.Dcache_LSQ_ROB_num}

# Global: Highlighting

# Post database loading setting...

# Restore C1 time
gui_set_time -C1_only 84353



# Save global setting...

# Wave/List view global setting
gui_cov_show_value -switch false

# Close all empty TopLevel windows
foreach __top [gui_ekki_get_window_ids -type TopLevel] {
    if { [llength [gui_ekki_get_window_ids -parent $__top]] == 0} {
        gui_close_window -window $__top
    }
}
gui_set_loading_session_type noSession
# DVE View/pane content session: 


# View 'Wave.1'
gui_wv_sync -id ${Wave.1} -switch false
set groupExD [gui_get_pref_value -category Wave -key exclusiveSG]
gui_set_pref_value -category Wave -key exclusiveSG -value {false}
set origWaveHeight [gui_get_pref_value -category Wave -key waveRowHeight]
gui_list_set_height -id Wave -height 25
set origGroupCreationState [gui_list_create_group_when_add -wave]
gui_list_create_group_when_add -wave -disable
gui_marker_set_ref -id ${Wave.1}  C1
gui_wv_zoom_timerange -id ${Wave.1} 84317 84428
gui_list_add_group -id ${Wave.1} -after {New Group} {Dcache}
gui_list_expand -id ${Wave.1} testbench.pipeline.Dcache.LD_BUF_valid
gui_list_expand -id ${Wave.1} testbench.pipeline.Dcache.LD_BUF_validreg
gui_list_select -id ${Wave.1} {testbench.pipeline.Dcache.LD_BUF_out }
gui_seek_criteria -id ${Wave.1} {Any Edge}



gui_set_env TOGGLE::DEFAULT_WAVE_WINDOW ${Wave.1}
gui_set_pref_value -category Wave -key exclusiveSG -value $groupExD
gui_list_set_height -id Wave -height $origWaveHeight
if {$origGroupCreationState} {
	gui_list_create_group_when_add -wave -enable
}
if { $groupExD } {
 gui_msg_report -code DVWW028
}
gui_list_set_filter -id ${Wave.1} -list { {Buffer 1} {Input 1} {Others 1} {Linkage 1} {Output 1} {Parameter 1} {All 1} {Aggregate 1} {Event 1} {Assertion 1} {Constant 1} {Interface 1} {Signal 1} {$unit 1} {Inout 1} {Variable 1} }
gui_list_set_filter -id ${Wave.1} -text {*}
gui_list_set_insertion_bar  -id ${Wave.1} -group Dcache  -item testbench.pipeline.Dcache.clock -position below

gui_marker_move -id ${Wave.1} {C1} 84353
gui_view_scroll -id ${Wave.1} -vertical -set 1093
gui_show_grid -id ${Wave.1} -enable false
# Restore toplevel window zorder
# The toplevel window could be closed if it has no view/pane
if {[gui_exist_window -window ${TopLevel.2}]} {
	gui_set_active_window -window ${TopLevel.2}
	gui_set_active_window -window ${Wave.1}
}
#</Session>

