/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  valid_list.v                                        //
//                                                                     //
//  Description :  This module creates the valid list of the           // 
//                  Physical Register used by the Pipeline.            //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

module valid_list(		  input  clock,							// clock
				  input  reset,
				  input  branch_mis,
				  input  [1:0] [`PRF_SIZE_LOG-1:0] ready_idx,			// ready  index
				  input  [1:0] [`PRF_SIZE_LOG-1:0] retire_idx,			// retire index
				  input  [1:0]  ready,						// ready enable
				  input  [1:0]  retire,						// retire enable
			          input [`PRF_SIZE-1:0] BRAT_valid_list,
				  output logic [`PRF_SIZE-1:0] valid_list,			// valid_list
				  output write_conflict
				  );
  
  logic    [`PRF_SIZE-1:0] registers;   	
  logic	   [`PRF_SIZE-1:0] next_registers;

  assign write_conflict = ready[0] & ready[1] & (ready_idx[0] == ready_idx[1]);


  //
  // Write port
  //
  always_comb begin
	next_registers = registers;
	if (reset) begin
		next_registers[31:0] = 32'h7fff_ffff;
		next_registers[63:32] = 32'h0;
		next_registers[`PRF_ZERO_REG] = 1'b1;
	end
	else begin
		if (branch_mis)
			next_registers = registers & BRAT_valid_list;
		if (ready[0])
			next_registers[ready_idx[0]] = 1'b1;
		if (ready[1])
			next_registers[ready_idx[1]] = 1'b1;
		if (retire[0])
			next_registers[retire_idx[0]] = 1'b0;
		if (retire[1])
			next_registers[retire_idx[1]] = 1'b0;
		next_registers[`PRF_ZERO_REG] = 1'b1;
	end
  end

 
  always_ff @(posedge clock) 
      registers <= `SD next_registers;

  //
  // valid_list
  //	
  assign valid_list = next_registers;




endmodule // valid_list
