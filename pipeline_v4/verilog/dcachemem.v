// cachemem32x64

module dcachemem(
				input clock, reset, 
				input wr1_en,wr2_en,
				input  [4:0] wr1_idx, wr2_idx,
				input  [4:0] rd1_idx,
				input [`DCACHE_TAG_SIZE_LOG-1:0] wr1_tag, wr2_tag,
				input [`DCACHE_TAG_SIZE_LOG-1:0] rd1_tag,
				input [63:0] wr1_data,wr2_data, 
				
				output logic [63:0] rd1_data,
				output logic rd1_valid
                );



logic [31:0] [63:0] data ;
logic [31:0] [21:0] tags; 
logic [31:0]        valids;

always_comb begin
   /*
	if(wr1_en && wr1_idx==rd1_idx && wr1_tag==rd1_tag) begin //forwarding
		rd1_data = wr1_data;
		rd1_valid = wr1_en;
	end
	else if(wr2_en && wr2_idx==rd1_idx && wr2_tag==rd1_tag) begin //forwarding
		rd1_data = wr2_data;
		rd1_valid = wr2_en;
	end
    else  begin
    */
		rd1_data = data[rd1_idx];
		rd1_valid = valids[rd1_idx]&&(tags[rd1_idx] == rd1_tag);
	//end
end

always_ff @(posedge clock)
begin
  if(reset) valids <= `SD 31'b0;
  else begin
	 if(wr1_en) 
    valids[wr1_idx] <= `SD 1;
	 if (wr2_en)
		valids[wr2_idx] <= `SD 1;
	end
end

always_ff @(posedge clock)
begin
  if(wr1_en)
  begin
    data[wr1_idx] <= `SD wr1_data;
    tags[wr1_idx] <= `SD wr1_tag;
  end
	if(wr2_en)
	begin
    data[wr2_idx] <= `SD wr2_data;
    tags[wr2_idx] <= `SD wr2_tag;
	end
end

endmodule
