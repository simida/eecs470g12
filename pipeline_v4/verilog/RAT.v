//Function: Map table: Save all the ARF to PRF mapping info
//Modifed by: Jingcheng Wang

//When modify RAT:
//		reset: all 1, 2, 3, ...
//		rename: instr has dest reg, then set wr_en=1, prf_dest write into RAT @ next posedge
//		flush: ROB flush everything, RAT gets the copy of RRAT
`define BRAT_NUM_LOG 2
`define BRAT_NUM 4

module RAT(
//READ Src Reg
	input  logic [4:0] 									arf_regA,
  input  logic [4:0] 									arf_regB, 
	output logic [`PRF_IDX-1:0] 				prf_regA,
	output logic [`PRF_IDX-1:0] 				prf_regB,

	input  logic [4:0] 									arf_regA1,
  input  logic [4:0] 									arf_regB1, 
	output logic [`PRF_IDX-1:0] 				prf_regA1,
	output logic [`PRF_IDX-1:0] 				prf_regB1,

//Write(rename) Dest Reg
  input  logic 									wr_en,wr_en1,
  input  logic [4:0] 									arf_dest,
	input  logic [`PRF_IDX-1:0] 				prf_dest,

  input  logic [4:0] 									arf_dest1,
	input  logic [`PRF_IDX-1:0] 				prf_dest1,


//ON a flush of ROB, Copy RRAT
//	input  logic												flush,
//	input  logic [31:0] [`PRF_IDX-1:0] 	RRAT_registers,

////////////////////////////
//Early Branch Resolution///
////////////////////////////
	//on dispatch a branch, allocate a BRAT
	input logic [`BRAT_NUM_LOG-1:0]			BRAT_tail,
	input logic branch1, branch2, //if first instr want to copy BRAT: branch1 will be 1

	//on find a mispredict, Overwrite RAT with BRAT
	input logic [`BRAT_NUM_LOG-1:0] BRAT_CMP_num, //which BRAT used to overwrite RAT
	input  logic										BRAT_CMP_miss,

	//From RAS
	input logic [1:0] [`RAS_SIZE-1:0] [63:0] 	BRAS_cp_reg,
  input logic [1:0] [`RAS_LOG2:0]   	BRAS_cp_head,
	//To RAS
	output logic [`RAS_SIZE-1:0] [63:0] 	BRAS_ld_reg,
  output logic [`RAS_LOG2:0]   	BRAS_ld_head,

	input  logic 									clock,
	input  logic 									reset

);

	//Current RAT
  reg    [31:0] [`PRF_IDX-1:0] registers;   // 32, `PRF_IDX-bit Registers

	////////////////////////////////
	//[For Early Branch Resolution]/
	////////////////////////////////
	//on a branch if BRAT available, then send a copy of RAT(after rename) to BRAT (if two branch, send twice)
	//Data Structure 
	reg [`BRAT_NUM-1:0] [`RAS_SIZE:0] [63:0] BRAS_registers;
	reg [`BRAT_NUM-1:0] [`RAS_LOG2:0] BRAS_head;
 
  wire [`BRAT_NUM_LOG-1:0] BRAT_tail_plus1 = BRAT_tail + 1'b1;

	always_comb
		begin
			BRAS_ld_reg = BRAS_registers[BRAT_CMP_num];
			BRAS_ld_head = BRAS_head[BRAT_CMP_num];
		end//	always_comb

	always_ff @(posedge clock)
	begin
		if(reset)
			begin
				for(integer j=0; j<`BRAT_NUM; j=j+1)
					begin
						for(integer i=0; i<`RAS_SIZE; i=i+1)
							begin
								BRAS_registers[j][i] 	<= `SD 0;
							end//for(i)
						BRAS_head <= `SD 0;
					end//for(j)
			end//if(reset)

		else
			case({branch1,branch2})			
			 2'b11://if(branch1 && branch2)
				begin
					BRAS_registers[BRAT_tail] 	<= `SD BRAS_cp_reg[0];
					BRAS_registers[BRAT_tail_plus1] 	<= `SD BRAS_cp_reg[1];
					BRAS_head[BRAT_tail] 	<= `SD BRAS_cp_head[0];
					BRAS_head[BRAT_tail_plus1] <= `SD BRAS_cp_head[1];
				end
			2'b10://if(branch1) 
				begin
					BRAS_registers[BRAT_tail] 	<= `SD BRAS_cp_reg[1];
					BRAS_head[BRAT_tail] 	<= `SD BRAS_cp_head[1];
				end
			2'b01://if(branch2) 
				begin
					BRAS_registers[BRAT_tail] 	<= `SD BRAS_cp_reg[1];
					BRAS_head[BRAT_tail] 	<= `SD BRAS_cp_head[1];
				end				
			endcase
	end


	reg [`BRAT_NUM-1:0] [31:0] [`PRF_IDX-1:0] BRAT_registers;
	logic [`BRAT_NUM-1:0] [31:0] [`PRF_IDX-1:0] next_BRAT_registers;



//UPON a Branch Dispatch, Allocate BRAT
/*
	always_comb
		begin
			next_BRAT_registers = BRAT_registers;
			if(branch1 && branch2)
				begin
					next_BRAT_registers[BRAT_tail] = registers;
					if(wr_en && (arf_dest != `ZERO_REG)) next_BRAT_registers[BRAT_tail][arf_dest] = prf_dest; 

					next_BRAT_registers[BRAT_tail_plus1] = registers;
					if(wr_en && (arf_dest != `ZERO_REG)) next_BRAT_registers[BRAT_tail_plus1][arf_dest] = prf_dest; 
					if(wr_en1 && (arf_dest1 != `ZERO_REG)) next_BRAT_registers[BRAT_tail_plus1][arf_dest1] = prf_dest1;
				end//if(branch1 && branch2)

			else
				begin
					if(branch1) 					
						begin
							next_BRAT_registers[BRAT_tail] = registers;
							if(wr_en && (arf_dest != `ZERO_REG)) next_BRAT_registers[BRAT_tail][arf_dest] = prf_dest; 
						end
			
					if(branch2) 					
						begin
							next_BRAT_registers[BRAT_tail] = registers;
							if(wr_en && (arf_dest != `ZERO_REG)) next_BRAT_registers[BRAT_tail][arf_dest] = prf_dest; 	
							if(wr_en1 && (arf_dest1 != `ZERO_REG)) next_BRAT_registers[BRAT_tail][arf_dest1] = prf_dest1;
						end
				end
		end
*/

	always_comb
		begin
			next_BRAT_registers = BRAT_registers;

			next_BRAT_registers[BRAT_tail] = registers;
			if(wr_en && (arf_dest != `ZERO_REG)) next_BRAT_registers[BRAT_tail][arf_dest] = prf_dest; 

			next_BRAT_registers[BRAT_tail_plus1] = registers;
			if(wr_en && (arf_dest != `ZERO_REG)) next_BRAT_registers[BRAT_tail_plus1][arf_dest] = prf_dest; 
			if(wr_en1 && (arf_dest1 != `ZERO_REG)) next_BRAT_registers[BRAT_tail_plus1][arf_dest1] = prf_dest1;

		end//	always_comb

	always_ff @(posedge clock)
	begin
		if(reset)
			begin
				for(integer BRAT_j=0; BRAT_j<`BRAT_NUM; BRAT_j=BRAT_j+1)
					begin
						for(integer BRAT_i=0; BRAT_i<31; BRAT_i=BRAT_i+1)
							begin
								BRAT_registers[BRAT_j][BRAT_i] 	<= `SD BRAT_i;
							end//for(BRAT_i)
						BRAT_registers[BRAT_j][`ZERO_REG] 	<= `SD `PRF_ZERO_REG;
					end//for(BRAT_j)
			end//if(reset)

		else if(branch1 && branch2)
			begin
				BRAT_registers[BRAT_tail] 	<= `SD next_BRAT_registers[BRAT_tail];
				BRAT_registers[BRAT_tail_plus1] 	<= `SD next_BRAT_registers[BRAT_tail_plus1];
			end

		else 
			begin
					if(branch1) BRAT_registers[BRAT_tail] 	<= `SD next_BRAT_registers[BRAT_tail];
					if(branch2) BRAT_registers[BRAT_tail] 	<= `SD next_BRAT_registers[BRAT_tail_plus1];
			end
	end
	////////////////////////////////
	//[END]/////////////////////////
	////////////////////////////////





	assign prf_regA = registers[arf_regA];
  assign prf_regB = registers[arf_regB];
	//assign prf_regA1 = registers[arf_regA1];
  //assign prf_regB1 = registers[arf_regB1];


	//---------Internal Forwarding---------------
  always_comb
    if (arf_regA1 == `ZERO_REG)
      prf_regA1 = `PRF_ZERO_REG;
    else if (wr_en && (arf_dest == arf_regA1))
      prf_regA1 = prf_dest;  // internal forwarding
    else
      prf_regA1 = registers[arf_regA1];

  always_comb
    if (arf_regB1 == `ZERO_REG)
      prf_regB1 = `PRF_ZERO_REG;
    else if (wr_en && (arf_dest == arf_regB1))
      prf_regB1 = prf_dest;  // internal forwarding
    else
      prf_regB1 = registers[arf_regB1];



	integer i;
	always_ff @(posedge clock)
	begin
		if(reset)
			begin
				/*registers[0] 		<= `SD 0;
				registers[1] 		<= `SD 1;
				registers[2] 		<= `SD 2;
				registers[3] 		<= `SD 3;
				registers[4] 		<= `SD 4;
				registers[5] 		<= `SD 5;
				registers[6] 		<= `SD 6;
				registers[7] 		<= `SD 7;
				registers[8] 		<= `SD 8;
				registers[9] 		<= `SD 9;
				registers[10] 	<= `SD 10;
				registers[11] 	<= `SD 11;
				registers[12] 	<= `SD 12;
				registers[13] 	<= `SD 13;
				registers[14] 	<= `SD 14;
				registers[15] 	<= `SD 15;
				registers[16] 	<= `SD 16;
				registers[17] 	<= `SD 17;
				registers[18] 	<= `SD 18;
				registers[19] 	<= `SD 19;
				registers[20] 	<= `SD 20;
				registers[21] 	<= `SD 21;
				registers[22] 	<= `SD 22;
				registers[23] 	<= `SD 23;
				registers[24] 	<= `SD 24;
				registers[25] 	<= `SD 25;
				registers[26] 	<= `SD 26;
				registers[27] 	<= `SD 27;
				registers[28] 	<= `SD 28;
				registers[29] 	<= `SD 29;
				registers[30] 	<= `SD 30;*/
				for(i=0; i<31; i=i+1)
				begin
					registers[i] 	<= `SD i;
				end
				registers[`ZERO_REG] 	<= `SD `PRF_ZERO_REG;
			end

		else if(BRAT_CMP_miss)
			begin
				//EBR if mispredict: copy corresponding BRAT To RAT
				registers       <= `SD BRAT_registers[BRAT_CMP_num];
			end

		else
			begin
				//if dispatch, then rename the corresponding entry
				if(wr_en && (arf_dest != `ZERO_REG)) registers[arf_dest] <= `SD prf_dest; 
				if(wr_en1 && (arf_dest1 != `ZERO_REG)) registers[arf_dest1] <= `SD prf_dest1;  
			end
	end//	always_ff @(posedge clock)



endmodule
