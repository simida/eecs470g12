//Function: A Ring buffer: Save all the free PRF in RAT
//Modifed by: Jingcheng Wang

//When modify Free list:
//		the case only change the head and tail
//		rename: if RAT assert rename request and give the number of reg needed, 
//						Free list will pop out the number of free PRF (max num 2 support 2-way)
//		flush: ROB flush everything, RAT free list gets the copy of RRAT free list--Not the case
//						Upon a flush, the only thing changed is the head of the FRL. The reg content is the same.
//		No need to have RRAT Free List At All!!!		
//
//    The only case that the reg content will be changed:
//		reset: all 31, 32, 33, 34, ... ,62
//		commit: RRAT send the freed PRF, Free list insert it to the buffer. (max num 2)


//Corner case: 2-way commit: free prf and flush can be asserted at the same time!!!
//Corner case: rename request and flush request happen at the same cycle: Nothing need to be done!!
//							just remember to set the two fetched instr to invalid is Enough

`define FREE_LIST_IDX 5 //32 free list entry now. so 2^FREE_LIST_IDX=32

module FreeRegList(
	//Control signal
	input logic clock, reset, flush,
	output logic [1:0] FRL_empty, // FRL_structure hazard: 1x: empty; 01: has 1 avail; 00: over 2 avail;

	//Data signal
	//****Don't need this information!!!
	//input logic [31:0] [`PRF_IDX-1:0] RRAT_FRL,

	//Data input: insert freed prf from commit
	input logic free_v1, free_v2,
	input logic [`PRF_IDX-1:0] freed_prf1,freed_prf2,

	//Data output: give free prf
	input logic rename1, rename2,
	output logic rename_v1, rename_v2,
	output logic [`PRF_IDX-1:0] prf1,prf2
);


	
	//Registers
	logic [31:0] [`PRF_IDX-1:0] FRL;//a ring buffer
	logic [`FREE_LIST_IDX:0] free_n;//record the number of free PRF in FRL;
	logic [`FREE_LIST_IDX-1:0] head, tail;//record the head and tail of the ring buffer


	//Next state combinational signal	
	logic [31:0] [`PRF_IDX-1:0] next_FRL;
	logic [`FREE_LIST_IDX:0] next_free_n, next_free_n2;//record the number of free PRF in FRL;
	logic [`FREE_LIST_IDX-1:0] next_head, next_tail;//record the head and tail of the ring buffer

	


	assign FRL_empty[1] = (free_n==0);
	assign FRL_empty[0] = (free_n==1);




	always_comb
		begin
			rename_v1=0;
			rename_v2=0;
			prf1=`PRF_ZERO_REG;
			prf2=`PRF_ZERO_REG;

			next_free_n=free_n;
			next_free_n2=free_n;
			next_head=head;
			next_tail=tail;
			next_FRL=FRL;



			// Free Registers
			// Completely independent of branch misprediction

			if(free_v1 && free_v2)
				begin
					next_FRL[tail]=freed_prf1;
					next_FRL[tail+1'h1]=freed_prf2;
					next_tail = tail + 2'h2;
					next_free_n2 = free_n + 2'h2;
				end//if(free_v1 && free_v2)
			else if(free_v1)
						begin
							next_FRL[tail]=freed_prf1;
							next_tail = tail + 1'h1;
							next_free_n2 = free_n + 1'h1;
							//Never: next_free_n = next_free_n + 1; Infinite Loop
						end
			else if(free_v2)
						begin
							next_FRL[tail]=freed_prf2;
							next_tail = tail + 1'h1;
							next_free_n2 = free_n + 1'h1;
						end

			//Highest priority: flush
			if(flush)
				begin
					//next_FRL = RRAT_FRL; //if flush copy the RRAT free list
															//RRAT FRL only has tail and it is the same as that in RAT
					next_free_n = 32;
					next_head = next_tail;

				end//if(flush)

			//No Flush happen in this cycle: rename and free reg as usual
			else
				begin
					if(rename1 || rename2)
						begin
							if(rename1 && rename2)
								begin
									rename_v1=1;
									rename_v2=1;
									prf1=FRL[head];
									prf2=FRL[head+5'h1];
									next_head = head + 5'h2;
									next_free_n2 = next_free_n2 - 5'h2;									
								end
							else if(rename1)
								begin
									rename_v1=1;
									rename_v2=0;
									prf1=FRL[head];
									prf2=`PRF_ZERO_REG;
									next_head = head + 5'h1;
									next_free_n2 = next_free_n2 - 5'h1;
								end
							else if(rename2)
								begin
									rename_v1=0;
									rename_v2=1;
									prf1=`PRF_ZERO_REG;
									prf2=FRL[head];
									next_head = head + 5'h1;
									next_free_n2 = next_free_n2 - 5'h1;
								end		
						end//if(rename1 || rename2)

				end//else


		end//always_comb







	always_ff @(posedge clock)
		begin
			if(reset)
				begin
					FRL[0] 		<= `SD 31;
					FRL[1] 		<= `SD 32;
					FRL[2] 		<= `SD 33;
					FRL[3] 		<= `SD 34;
					FRL[4] 		<= `SD 35;
					FRL[5] 		<= `SD 36;
					FRL[6] 		<= `SD 37;
					FRL[7] 		<= `SD 38;
					FRL[8] 		<= `SD 39;
					FRL[9] 		<= `SD 40;
					FRL[10] 	<= `SD 41;
					FRL[11] 	<= `SD 42;
					FRL[12] 	<= `SD 43;
					FRL[13] 	<= `SD 44;
					FRL[14] 	<= `SD 45;
					FRL[15] 	<= `SD 46;
					FRL[16] 	<= `SD 47;
					FRL[17] 	<= `SD 48;
					FRL[18] 	<= `SD 49;
					FRL[19] 	<= `SD 50;
					FRL[20] 	<= `SD 51;
					FRL[21] 	<= `SD 52;
					FRL[22] 	<= `SD 53;
					FRL[23] 	<= `SD 54;
					FRL[24] 	<= `SD 55;
					FRL[25] 	<= `SD 56;
					FRL[26] 	<= `SD 57;
					FRL[27] 	<= `SD 58;
					FRL[28] 	<= `SD 59;
					FRL[29] 	<= `SD 60;
					FRL[30] 	<= `SD 61;
					FRL[31] 	<= `SD 62;
					
					//Warning: All reg type need to be reset in always_ff--those are drives of combiniational signals
					free_n		<= `SD 32;//reset 32 free registers
					head			<= `SD 0;
					tail			<= `SD 0;//tail is always pointed to the first used unfreed slot
														////32 is another name of 0
				end

			//Assume in ROB instr that freed prf is ahead of mispredicted Branch
			//So give the Second priority to free prf request
			else
				begin
					//FRL				<= `SD RRAT_FRL;
					FRL				<= `SD next_FRL; //So that I can use sequential(blocking) assignment to next_FRL
					head			<= `SD next_head;
					tail			<= `SD next_tail;
					free_n		<= `SD next_free_n;
				end
		end//always_ff @(posedge clock)


endmodule







