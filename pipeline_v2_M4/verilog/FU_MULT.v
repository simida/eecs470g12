module mult_stage(clock, reset, stall, 
                  product_in,  mplier_in,  mcand_in,  start, NPC_in, IR_in, ROB_in, dest_in, 
                  product_out, mplier_out, mcand_out, done, NPC_out, IR_out, ROB_out, dest_out);

  input clock, reset, start, stall;
  input [63:0] product_in, mplier_in, mcand_in;
  input [63:0] NPC_in;
  input [31:0] IR_in;
  input [`ROB_SIZE_LOG-1:0] ROB_in;
  input [`PRF_SIZE_LOG-1:0] dest_in;



  output done;
  output [63:0] product_out, mplier_out, mcand_out;
  output reg [63:0] NPC_out;
  output reg [31:0] IR_out;
  output reg [`ROB_SIZE_LOG-1:0] ROB_out;
  output reg [`PRF_SIZE_LOG-1:0] dest_out;

  reg  [63:0] prod_in_reg, partial_prod_reg;
  wire [63:0] partial_product, next_mplier, next_mcand;


  reg [63:0] mplier_out, mcand_out;
  reg done;
  
  assign product_out = prod_in_reg + partial_prod_reg;

  assign partial_product = mplier_in[15:0] * mcand_in;

  assign next_mplier = {16'b0, mplier_in[63:16]};
  assign next_mcand = {mcand_in[47:0],16'b0};

  always_ff @(posedge clock)
  begin
    if (reset) begin
            prod_in_reg      <= `SD 0;
	    partial_prod_reg <= `SD 0;
	    mplier_out       <= `SD 0;
	    mcand_out        <= `SD 0;
            NPC_out          <= `SD 0;
	    IR_out           <= `SD 0;
	    ROB_out	     <= `SD 0;
	    dest_out	     <= `SD 0;
    end
    else if (~stall | ~done) begin
	    prod_in_reg      <= `SD product_in;
	    partial_prod_reg <= `SD partial_product;
	    mplier_out       <= `SD next_mplier;
	    mcand_out        <= `SD next_mcand;
            NPC_out          <= `SD NPC_in;
	    IR_out           <= `SD IR_in;
	    ROB_out	     <= `SD ROB_in;
	    dest_out	     <= `SD dest_in;
    end
  end

  always_ff @(posedge clock)
  begin
    if(reset)
      done <= `SD 1'b0;
    else if (~stall | ~done)
      done <= `SD start;
  end

endmodule

module mult(clock, reset, stall, mplier, mcand, start, NPC_in, IR_in, ROB_in, dest_in, product, done, NPC_out, IR_out, ROB_out, dest_out, mult_stall);

  input clock, reset, start, stall;
  input [63:0] mcand, mplier;
  input [63:0] NPC_in;
  input [31:0] IR_in;
  input [`ROB_SIZE_LOG-1:0] ROB_in;
  input [`PRF_SIZE_LOG-1:0] dest_in;

  output [63:0] product;
  output done;
  output [63:0] NPC_out;
  output [31:0] IR_out;
  output [`ROB_SIZE_LOG-1:0] ROB_out;
  output [`PRF_SIZE_LOG-1:0] dest_out;
  output mult_stall;
  


  wire [63:0] mcand_out, mplier_out;
  wire [191:0] internal_products, internal_mcands, internal_mpliers;
  wire [2:0] internal_dones;
  wire [3*`ROB_SIZE_LOG-1:0] internal_ROB;
  wire [3*`PRF_SIZE_LOG-1:0] internal_dest;
  wire [191:0] internal_NPC;
  wire [95:0] internal_IR;

  assign mult_stall = stall & internal_dones[0];
  
  mult_stage mstage [3:0] 
    (.clock(clock),
     .reset(reset),
     .stall(stall),
     .product_in({internal_products,64'h0}),
     .mplier_in({internal_mpliers,mplier}),
     .mcand_in({internal_mcands,mcand}),
     .start({internal_dones,start}),
     .NPC_in({internal_NPC, NPC_in}),
     .IR_in({internal_IR,IR_in}),
     .ROB_in({internal_ROB, ROB_in}),
     .dest_in({internal_dest,dest_in}),
     .product_out({product,internal_products}),
     .mplier_out({mplier_out,internal_mpliers}),
     .mcand_out({mcand_out,internal_mcands}),
     .done({done,internal_dones}),
     .NPC_out({NPC_out,internal_NPC}),
     .IR_out({IR_out, internal_IR}),
     .ROB_out({ROB_out,internal_ROB}),
     .dest_out({dest_out, internal_dest})
    );

endmodule

module FU_MULT(
				input         clock,               // system clock
				input         reset,               // system reset
				input 		   			MULT_valid,
				input					MULT_stall,
				input [63:0] 	   			MULT_NPC,
				input [31:0] 	   			MULT_IR,
				input [`ALU_FUNC_NUM_LOG-1:0] 		MULT_alu_func,
				input [63:0] 	   			MULT_regA,
				input [63:0] 	   			MULT_regB,
				input [1:0]		   		MULT_opa_sel,
				input [1:0]		   		MULT_opb_sel,
				input [`PRF_SIZE_LOG-1:0] 	   	MULT_dest,
				input [`ROB_SIZE_LOG-1:0] 	   	MULT_ROB,

				output [63:0]				FU_MULT_NPC_out,
				output [31:0]				FU_MULT_IR_out,
				output [`PRF_SIZE_LOG-1:0] 		FU_MULT_dest_out,
				output [`ROB_SIZE_LOG-1:0]		FU_MULT_ROB_out,
				output [63:0] 				FU_MULT_result_out,  // is this a taken branch?
				output     				FU_MULT_valid_out,
				output  				FU_MULT_stall_out

               );


  logic  [63:0] opa_mux_out, opb_mux_out;

  wire [63:0]  mult_result;
  wire done;
  wire [63:0] NPC_out;
  wire [31:0] IR_out;
  wire [`ROB_SIZE_LOG-1:0] ROB_out;
  wire [`PRF_SIZE_LOG-1:0] dest_out;
  wire stall_out;
   


   // set up possible immediates:
   //   mem_disp: sign-extended 16-bit immediate for memory format
   //   br_disp: sign-extended 21-bit immediate * 4 for branch displacement
   //   alu_imm: zero-extended 8-bit immediate for ALU ops
  wire [63:0] mem_disp = { {48{MULT_IR[15]}}, MULT_IR[15:0] };
  wire [63:0] br_disp  = { {41{MULT_IR[20]}}, MULT_IR[20:0], 2'b00 };
  wire [63:0] alu_imm  = { 56'b0, MULT_IR[20:13] };
   
   //
   // ALU opA mux
   //
  always_comb
  begin
    case (MULT_opa_sel)
      `ALU_OPA_IS_REGA:     opa_mux_out = MULT_regA;
      `ALU_OPA_IS_MEM_DISP: opa_mux_out = mem_disp;
      `ALU_OPA_IS_NPC:      opa_mux_out = MULT_NPC;
      `ALU_OPA_IS_NOT3:     opa_mux_out = ~64'h3;
    endcase
  end

   //
   // ALU opB mux
   //
  always_comb
  begin
     // Default value, Set only because the case isnt full.  If you see this
     // value on the output of the mux you have an invalid opb_select
    opb_mux_out = 64'hbaadbeefdeadbeef;
    case (MULT_opb_sel)
      `ALU_OPB_IS_REGB:    opb_mux_out = MULT_regB;
      `ALU_OPB_IS_ALU_IMM: opb_mux_out = alu_imm;
      `ALU_OPB_IS_BR_DISP: opb_mux_out = br_disp;
    endcase 
  end

   //
   // instantiate the MULT
   //
  mult mult0(
	      // input
              .clock(clock),
	      .reset(reset),
	      .stall(MULT_stall),
              .mplier(opb_mux_out),
              .mcand(opa_mux_out),
              .start(MULT_valid),
	      .NPC_in(MULT_NPC),
	      .IR_in(MULT_IR),
	      .ROB_in(MULT_ROB),
	      .dest_in(MULT_dest),
	      // output
              .product(mult_result),
	      .done(done),
	      .NPC_out(NPC_out),
	      .IR_out(IR_out),
	      .ROB_out(ROB_out),
	      .dest_out(dest_out),
              .mult_stall(stall_out)
	     );


   assign FU_MULT_NPC_out = NPC_out;
   assign FU_MULT_IR_out= IR_out;
   assign FU_MULT_result_out = mult_result;
   assign FU_MULT_valid_out = done;
   assign FU_MULT_ROB_out = ROB_out;
   assign FU_MULT_dest_out = dest_out;
   assign FU_MULT_stall_out = stall_out;


endmodule // module ex_stage
