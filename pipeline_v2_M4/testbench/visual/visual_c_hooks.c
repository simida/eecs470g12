#include "DirectC.h"
#include <curses.h>
#include <stdio.h>
#include <signal.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <time.h>
#include <string.h>

#define PARENT_READ     readpipe[0]
#define CHILD_WRITE     readpipe[1]
#define CHILD_READ      writepipe[0]
#define PARENT_WRITE    writepipe[1]
#define NUM_HISTORY     2048
#define NUM_ARF         32
#define NUM_STAGES      5
#define NOOP_INST       0x47ff041f
#define NUM_REG_GROUPS  8

// random variables/stuff
int fd[2], writepipe[2], readpipe[2];
int stdout_save;
int stdout_open;
void signal_handler_IO (int status);
int wait_flag=0;
char done_state;
char echo_data;
FILE *fp;
FILE *fp2;
int setup_registers = 0;
int stop_time;
int done_time = -1;
char time_wrapped = 0;

// Structs to hold information about each register/signal group
typedef struct win_info {
  int height;
  int width;
  int starty;
  int startx;
  int color;
} win_info_t;

typedef struct reg_group {
  WINDOW *reg_win;
  char ***reg_contents;
  char **reg_names;
  int num_regs;
  win_info_t reg_win_info;
} reg_group_t;

// Window pointers for ncurses windows
WINDOW *title_win;
WINDOW *comment_win;
WINDOW *time_win;
WINDOW *sim_time_win;
WINDOW *instr_win;
WINDOW *clock_win;
WINDOW *RAT_win;
WINDOW *RRAT_win;
WINDOW *PRF_win;
WINDOW *ROB_win;
WINDOW *RS_win;
WINDOW *FU_win;
WINDOW *dispatch_win;
WINDOW *issue_win;
WINDOW *complete_win;
WINDOW *retire_win;
WINDOW *misc_win;

// arrays for register contents and names
int history_num=0;
int num_misc_regs;
char readbuffer[1024];
char **timebuffer;
char **cycles;
char *clocks;
char *resets;
char ***RAT_contents;
char ***RRAT_contents;
char ***PRF_contents;
char ***ROB_contents;
char ***dispatch_contents;
char ***RS_contents;
char ***issue_contents;
char ***FU_contents;
char ***complete_contents;
char ***retire_contents;
char ***misc_contents;

char *get_opcode_str(int inst, int valid_inst);
void parse_register(char* readbuf, int reg_num, char*** content);
int get_time();


// Helper function for ncurses gui setup
WINDOW *create_newwin(int height, int width, int starty, int startx, int color){
  WINDOW *local_win;
  local_win = newwin(height, width, starty, startx);
  wbkgd(local_win,COLOR_PAIR(color));
  wattron(local_win,COLOR_PAIR(color));
  box(local_win,0,0);
  wrefresh(local_win);
  return local_win;
}

// Function to draw positive edge or negative edge in clock window
void update_clock(char clock_val){
  static char cur_clock_val = 0;
  // Adding extra check on cycles because:
  //  - if the user, right at the beginning of the simulation, jumps to a new
  //    time right after a negative clock edge, the clock won't be drawn
  if((clock_val != cur_clock_val) || strncmp(cycles[history_num],"      0",7) == 1){
    mvwaddch(clock_win,3,7,ACS_VLINE | A_BOLD);
    if(clock_val == 1){

      //we have a posedge
      mvwaddch(clock_win,2,1,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,ACS_ULCORNER | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      mvwaddch(clock_win,4,1,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_LRCORNER | A_BOLD);
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
    } else {

      //we have a negedge
      mvwaddch(clock_win,4,1,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,ACS_LLCORNER | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      mvwaddch(clock_win,2,1,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_HLINE | A_BOLD);
      waddch(clock_win,ACS_URCORNER | A_BOLD);
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
      waddch(clock_win,' ');
    }
  }
  cur_clock_val = clock_val;
  wrefresh(clock_win);
}

// Function to create and initialize the gui
// Color pairs are (foreground color, background color)
// If you don't like the dark backgrounds, a safe bet is to have
//   COLOR_BLUE/BLACK foreground and COLOR_WHITE background
void setup_gui(FILE* fp){

  initscr();
  if(has_colors()){
    start_color();
    init_pair(1,COLOR_CYAN,COLOR_BLACK);    // shell background
    init_pair(2,COLOR_YELLOW,COLOR_RED);
    init_pair(3,COLOR_RED,COLOR_BLACK);
    init_pair(4,COLOR_YELLOW,COLOR_BLUE);   // title window
    init_pair(5,COLOR_YELLOW,COLOR_BLACK);  // register/signal windows
    init_pair(6,COLOR_RED,COLOR_BLACK);
    init_pair(7,COLOR_MAGENTA,COLOR_BLACK); // pipeline window
  }
  curs_set(0);
  noecho();
  cbreak();
  keypad(stdscr,TRUE);
  wbkgd(stdscr,COLOR_PAIR(1));
  wrefresh(stdscr);
  int pipe_width=0;


  //instantiate the title window at top of screen
  title_win = create_newwin(3,COLS,0,0,4);
  mvwprintw(title_win,1,1,"SIMULATION INTERFACE");
  mvwprintw(title_win,1,COLS-9,"GROUP 12");
  wrefresh(title_win);

  //instantiate time window at right hand side of screen
  time_win = create_newwin(3,10,8,COLS-10,5);
  mvwprintw(time_win,0,3,"TIME");
  wrefresh(time_win);

  //instantiate a sim time window which states the actual simlator time
  sim_time_win = create_newwin(3,10,11,COLS-10,5);
  mvwprintw(sim_time_win,0,1,"SIM TIME");
  wrefresh(sim_time_win);

  //instantiate a window to show which clock edge this is
  clock_win = create_newwin(6,15,8,COLS-25,5);
  mvwprintw(clock_win,0,5,"CLOCK");
  mvwprintw(clock_win,1,1,"cycle:");
  update_clock(0);
  wrefresh(clock_win);

  // instantiate a window for the RAT on the right side
  RAT_win = create_newwin(34,10,14,COLS-70,5);
  mvwprintw(RAT_win,0,4,"RAT");
  int i=0;
  char tmp_buf[32];

  for (i=0; i < 32; i++) {
    sprintf(tmp_buf, "r%02d: ", i);
    mvwprintw(RAT_win,i+1,1,tmp_buf);
  }

  wrefresh(RAT_win);

  // instantiate a window for the RRAT on the right side
  RRAT_win = create_newwin(34,10,14,COLS-60,5);
  mvwprintw(RRAT_win,0,3,"RRAT");

  for (i=0; i < 32; i++) {
    sprintf(tmp_buf, "r%02d: ", i);
    mvwprintw(RRAT_win,i+1,1,tmp_buf);
  }

  wrefresh(RRAT_win);

  // instantiate a window for the PRF on the right side
  PRF_win = create_newwin(34,50,14,COLS-50,5);
  mvwprintw(PRF_win,0,24,"PRF");

  for (i=0; i < 32; i++) {
    sprintf(tmp_buf, "p%02d: ", i);
    mvwprintw(PRF_win,i+1,1,tmp_buf);
  }

  for (i=0; i < 32; i++) {
    sprintf(tmp_buf, "p%02d: ", i+32);
    mvwprintw(PRF_win,i+1,26,tmp_buf);
  }
  wrefresh(PRF_win);

  //instantiate window to visualize ROB
  ROB_win = create_newwin(36,30,12,COLS-100,5);
  mvwprintw(ROB_win,0,14,"ROB");
  mvwprintw(ROB_win,1,1,"Head:");
  mvwprintw(ROB_win,2,1,"Tail:");
  for (i=0; i < 32; i++) {
    sprintf(tmp_buf, "R%02d: ", i);
    mvwprintw(ROB_win,i+3,1,tmp_buf);
  }
  wrefresh(ROB_win);

  //instantiate window to visualize dispatch stage
  dispatch_win = create_newwin(6,100,6,0,5);
  mvwprintw(dispatch_win,0,44,"dispatch stage");
  wrefresh(dispatch_win);

  //instantiate window to visualize RS
  RS_win = create_newwin(11,100,12,0,5);
  mvwprintw(RS_win,0,50,"RS");
  mvwprintw(RS_win,1,2,"      @@RS_ALU         |      @@RS_BR          |      @@RS_MULT        |      @@RS_MEM          ");
  wrefresh(RS_win);

  //instantiate window to visualize issue stage
  issue_win = create_newwin(10,100,23,0,5);
  mvwprintw(issue_win,0,45,"issue stage");
  wrefresh(issue_win);

  //instantiate window to FU 
  FU_win = create_newwin(10,100,LINES-29,0,5);
  mvwprintw(FU_win,0,49,"FU");
  wrefresh(FU_win);

  //instantiate window to complete stage
  complete_win = create_newwin(6,100,LINES-19,0,5);
  mvwprintw(complete_win,0,43,"complete stage");
  wrefresh(complete_win);

  //instantiate window to retire stage
  retire_win = create_newwin(6,100,LINES-13,0,5);
  mvwprintw(retire_win,0,44,"retire stage");
  wrefresh(retire_win);
  
  // instantiate window to visualize misc regs/wires
  misc_win = create_newwin(7,COLS-25-30,LINES-7,30,5);
  mvwprintw(misc_win,0,(COLS-30-30)/2-6,"MISC SIGNALS");
  wrefresh(misc_win);

  //instantiate an instructional window to help out the user some
  instr_win = create_newwin(7,30,LINES-7,0,5);
  mvwprintw(instr_win,0,9,"INSTRUCTIONS");
  wattron(instr_win,COLOR_PAIR(5));
  mvwaddstr(instr_win,1,1,"'n'   -> Next clock edge");
  mvwaddstr(instr_win,2,1,"'b'   -> Previous clock edge");
  mvwaddstr(instr_win,3,1,"'c/g' -> Goto specified time");
  mvwaddstr(instr_win,4,1,"'r'   -> Run to end of sim");
  mvwaddstr(instr_win,5,1,"'q'   -> Quit Simulator");
  wrefresh(instr_win);

  refresh();
}

// This function updates all of the signals being displayed with the values
// from time history_num_in (this is the index into all of the data arrays).
// If the value changed from what was previously display, the signal has its
// display color inverted to make it pop out.
void parsedata(int history_num_in){
  static int old_history_num_in=0;
  static int old_head_position=0;
  static int old_tail_position=0;
  int i=0;
  int tmp=0;
  int tmp_val=0;
  char tmp_buf[32];

  // Handle updating resets
  if (resets[history_num_in]) {
    wattron(title_win,A_REVERSE);
    mvwprintw(title_win,1,(COLS/2)-3,"RESET");
    wattroff(title_win,A_REVERSE);
  }
  else if (done_time != 0 && (history_num_in == done_time)) {
    wattron(title_win,A_REVERSE);
    mvwprintw(title_win,1,(COLS/2)-3,"DONE ");
    wattroff(title_win,A_REVERSE);
  }
  else
    mvwprintw(title_win,1,(COLS/2)-3,"     ");
  wrefresh(title_win);


  // Handle updating the RAT window
  for (i=0; i < 32; i++) {
    if (strncmp(RAT_contents[history_num_in][i],
                RAT_contents[old_history_num_in][i],16))
      wattron(RAT_win, A_REVERSE);
    else
      wattroff(RAT_win, A_REVERSE);
    mvwaddnstr(RAT_win,i+1,6,RAT_contents[history_num_in][i],16);
  }
  wrefresh(RAT_win);

  // Handle updating the RRAT window
  for (i=0; i < 32; i++) {
    if (strncmp(RRAT_contents[history_num_in][i],
                RRAT_contents[old_history_num_in][i],16))
      wattron(RRAT_win, A_REVERSE);
    else
      wattroff(RRAT_win, A_REVERSE);
    mvwaddnstr(RRAT_win,i+1,6,RRAT_contents[history_num_in][i],16);
  }
  wrefresh(RRAT_win);

  // Handle updating the PRF window
  for (i=0; i < 32; i++) {
    if (strncmp(PRF_contents[history_num_in][i],
                PRF_contents[old_history_num_in][i],16))
      wattron(PRF_win, A_REVERSE);
    else
      wattroff(PRF_win, A_REVERSE);
    mvwaddnstr(PRF_win,i+1,6,PRF_contents[history_num_in][i],16);
  }

  for (i=0; i < 32; i++) {
    if (strncmp(PRF_contents[history_num_in][i+32],
                PRF_contents[old_history_num_in][i+32],16))
      wattron(PRF_win, A_REVERSE);
    else
      wattroff(PRF_win, A_REVERSE);
    mvwaddnstr(PRF_win,i+1,31,PRF_contents[history_num_in][i+32],16);
  }
  wrefresh(PRF_win);


  //instantiate window to visualize ROB
  for(i=0;i<34;i++){
    if (strcmp(ROB_contents[history_num_in][i],
                ROB_contents[old_history_num_in][i]))
      wattron(ROB_win, A_REVERSE);
    else
      wattroff(ROB_win, A_REVERSE);
    mvwaddstr(ROB_win,i+1, 6, ROB_contents[history_num_in][i]);
  }
  wrefresh(ROB_win);


  //instantiate window to visualize dispatch stage
  for(i=0;i<4;i++){
    if (strcmp(dispatch_contents[history_num_in][i],
                dispatch_contents[old_history_num_in][i]))
      wattron(dispatch_win, A_REVERSE);
    else
      wattroff(dispatch_win, A_REVERSE);
    mvwaddstr(dispatch_win,i+1, 3, dispatch_contents[history_num_in][i]);
  }
  wrefresh(dispatch_win);


  //instantiate window to visualize RS
  for(i=0;i<8;i++){
    if (strcmp(RS_contents[history_num_in][i],
                RS_contents[old_history_num_in][i]))
      wattron(RS_win, A_REVERSE);
    else
      wattroff(RS_win, A_REVERSE);
    mvwaddstr(RS_win,i+2, 2, RS_contents[history_num_in][i]);
  }
  wrefresh(RS_win);

  //instantiate window to visualize issue stage
  for(i=0;i<8;i++){
    if (strcmp(issue_contents[history_num_in][i],
                issue_contents[old_history_num_in][i]))
      wattron(issue_win, A_REVERSE);
    else
      wattroff(issue_win, A_REVERSE);
    mvwaddstr(issue_win,i+1, 3, issue_contents[history_num_in][i]);
  }
  wrefresh(issue_win);

  //instantiate window to visualize FU
  for(i=0;i<8;i++){
    if (strcmp(FU_contents[history_num_in][i],
                FU_contents[old_history_num_in][i]))
      wattron(FU_win, A_REVERSE);
    else
      wattroff(FU_win, A_REVERSE);
    mvwaddstr(FU_win,i+1, 3, FU_contents[history_num_in][i]);
  }
  wrefresh(FU_win);

  //instantiate window to visualize complete stage
  for(i=0;i<4;i++){
    if (strcmp(complete_contents[history_num_in][i],
                complete_contents[old_history_num_in][i]))
      wattron(complete_win, A_REVERSE);
    else
      wattroff(complete_win, A_REVERSE);
    mvwaddstr(complete_win,i+1, 3, complete_contents[history_num_in][i]);
  }
  wrefresh(complete_win);

  //instantiate window to visualize retire stage
  for(i=0;i<4;i++){
    if (strcmp(retire_contents[history_num_in][i],
                retire_contents[old_history_num_in][i]))
      wattron(retire_win, A_REVERSE);
    else
      wattroff(retire_win, A_REVERSE);
    mvwaddstr(retire_win,i+1, 3, retire_contents[history_num_in][i]);
  }
  wrefresh(retire_win);

  // Handle updating the misc. window
  int row=1,col=1;
  for (i=0;i<num_misc_regs;i++){
    if (strcmp(misc_contents[history_num_in][i],
                misc_contents[old_history_num_in][i]))
      wattron(misc_win, A_REVERSE);
    else
      wattroff(misc_win, A_REVERSE);    

    mvwaddstr(misc_win,(i%5)+1,((i/5)*30)+3,misc_contents[history_num_in][i]);
    if ((++row)>6) {
      row=1;
      col+=30;
    }
  }
  wrefresh(misc_win);

  //update the time window
  mvwaddstr(time_win,1,1,timebuffer[history_num_in]);
  wrefresh(time_win);

  //update to the correct clock edge for this history
  mvwaddstr(clock_win,1,7,cycles[history_num_in]);
  update_clock(clocks[history_num_in]);

  //save the old history index to check for changes later
  old_history_num_in = history_num_in; 
}

// Parse a line of data output from the testbench
int processinput(){
  static int RAT_num = 0;
  static int RRAT_num = 0;
  static int PRF_num = 0;
  static int ROB_num = 0;
  static int dispatch_num = 0;
  static int RS_num = 0;
  static int issue_num = 0;
  static int FU_num = 0;
  static int complete_num = 0;
  static int retire_num = 0;
  static int misc_reg_num = 0;
  int tmp_len;
  char val_buf[256];

  //get rid of newline character
  readbuffer[strlen(readbuffer)-1] = 0;

  if(strncmp(readbuffer,"t",1) == 0){

    //We are getting the timestamp
    strcpy(timebuffer[history_num],readbuffer+1);
  }else if(strncmp(readbuffer,"c",1) == 0){

    //We have a clock edge/cycle count signal
    if(strncmp(readbuffer+1,"0",1) == 0)
      clocks[history_num] = 0;
    else
      clocks[history_num] = 1;

    // grab clock count (for some reason, first clock count sent is
    // too many digits, so check for this)
    strncpy(cycles[history_num],readbuffer+2,7);
    if (strncmp(cycles[history_num],"       ",7) == 0)
      cycles[history_num][6] = '0';
    
  }else if(strncmp(readbuffer,"z",1) == 0){
    
    // we have a reset signal
    if(strncmp(readbuffer+1,"0",1) == 0)
      resets[history_num] = 0;
    else
      resets[history_num] = 1;

  }else if(strncmp(readbuffer,"a",1) == 0){
    // We are getting an RAT register

    // If this is the first time we've seen the register,
    // add data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, RAT_num, RAT_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(RAT_contents[history_num][RAT_num],val_buf);
    }
    RAT_num++;	

  }else if(strncmp(readbuffer,"r",1) == 0){
    // We are getting an RRAT register

    // If this is the first time we've seen the register,
    // add data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, RRAT_num, RRAT_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(RRAT_contents[history_num][RRAT_num],val_buf);
    }
    RRAT_num++;	

  }else if(strncmp(readbuffer,"p",1) == 0){
    // We are getting an PRF register

    // If this is the first time we've seen the register,
    // add data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, PRF_num, PRF_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(PRF_contents[history_num][PRF_num],val_buf);
    }
    PRF_num++;	

  }else if(strncmp(readbuffer,"o",1) == 0){
    // We are getting an ROB register

    // If this is the first time we've seen the register,
    // add data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, ROB_num, ROB_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(ROB_contents[history_num][ROB_num],val_buf);
    }
    ROB_num++;	

  }else if(strncmp(readbuffer,"d",1) == 0){
    // We are getting an dispatch register

    // If this is the first time we've seen the register,
    // add data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, dispatch_num, dispatch_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(dispatch_contents[history_num][dispatch_num],val_buf);
    }
    dispatch_num++;	

  }else if(strncmp(readbuffer,"s",1) == 0){
    // We are getting an RS register

    // If this is the first time we've seen the register,
    // add data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, RS_num, RS_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(RS_contents[history_num][RS_num],val_buf);
    }
    RS_num++;	

  }else if(strncmp(readbuffer,"i",1) == 0){
    // We are getting an issue register

    // If this is the first time we've seen the register,
    // add data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, issue_num, issue_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(issue_contents[history_num][issue_num],val_buf);
    }
    issue_num++;	

  }else if(strncmp(readbuffer,"f",1) == 0){
    // We are getting an FU register

    // If this is the first time we've seen the register,
    // add data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, FU_num, FU_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(FU_contents[history_num][FU_num],val_buf);
    }
    FU_num++;	

  }else if(strncmp(readbuffer,"l",1) == 0){
    // We are getting an complete register

    // If this is the first time we've seen the register,
    // add data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, complete_num, complete_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(complete_contents[history_num][complete_num],val_buf);
    }
    complete_num++;	

  }else if(strncmp(readbuffer,"e",1) == 0){
    // We are getting an retire register

    // If this is the first time we've seen the register,
    // add data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, retire_num, retire_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(retire_contents[history_num][retire_num],val_buf);
    }
    retire_num++;	

  }else if(strncmp(readbuffer,"v",1) == 0){

    //we are processing misc register/wire data
    // If this is the first time we've seen the register,
    // add name and data to arrays
    if (!setup_registers) {
      parse_register(readbuffer, misc_reg_num, misc_contents);
    } else {
      sscanf(readbuffer,"%*c%d:%s",&tmp_len,val_buf);
      strcpy(misc_contents[history_num][misc_reg_num],val_buf);
    }

    misc_reg_num++;
  }else if (strncmp(readbuffer,"break",4) == 0) {
    // If this is the first time through, indicate that we've setup all of
    // the register arrays.
    setup_registers = 1;

    //we've received our last data segment, now go process it
    RAT_num = 0;
    RRAT_num = 0;
    PRF_num = 0;
    ROB_num = 0;
    dispatch_num = 0;
    RS_num = 0;
    issue_num = 0;
    FU_num = 0;
    complete_num = 0;
    retire_num = 0;
    misc_reg_num = 0;

    //update the simulator time, this won't change with 'b's
    mvwaddstr(sim_time_win,1,1,timebuffer[history_num]);
    wrefresh(sim_time_win);

    //tell the parent application we're ready to move on
    return(1); 
  }

   return(0);
}

//this initializes a ncurses window and sets up the arrays for exchanging reg information
void initcurses(int misc_regs){
  int nbytes;
  int ready_val;
  int i=0;
  done_state = 0;
  echo_data = 1;
  num_misc_regs = misc_regs;
  pid_t childpid;
  pipe(readpipe);
  pipe(writepipe);
  stdout_save = dup(1);
  childpid = fork();
  if(childpid == 0){
    close(PARENT_WRITE);
    close(PARENT_READ);
    fp = fdopen(CHILD_READ, "r");
    fp2 = fopen("program.out","w");
    int i=0;

    //allocate room on the heap for the reg data
    RAT_contents      	= (char**) malloc(NUM_HISTORY*sizeof(char*));
    RRAT_contents     	= (char***) malloc(NUM_HISTORY*sizeof(char**));
    PRF_contents      	= (char***) malloc(NUM_HISTORY*sizeof(char**));
    ROB_contents      	= (char***) malloc(NUM_HISTORY*sizeof(char**));
    dispatch_contents 	= (char***) malloc(NUM_HISTORY*sizeof(char**));
    RS_contents       	= (char***) malloc(NUM_HISTORY*sizeof(char**));
    issue_contents    	= (char***) malloc(NUM_HISTORY*sizeof(char**));
    FU_contents       	= (char***) malloc(NUM_HISTORY*sizeof(char**));
    complete_contents 	= (char***) malloc(NUM_HISTORY*sizeof(char**));
    retire_contents   	= (char***) malloc(NUM_HISTORY*sizeof(char**));
    misc_contents     	= (char***) malloc(NUM_HISTORY*sizeof(char**));
    timebuffer        	= (char**) malloc(NUM_HISTORY*sizeof(char*));
    cycles            	= (char**) malloc(NUM_HISTORY*sizeof(char*));
    clocks            	= (char*) malloc(NUM_HISTORY*sizeof(char));
    resets            	= (char*) malloc(NUM_HISTORY*sizeof(char));

    for(;i<NUM_HISTORY;i++){
      timebuffer[i]       	= (char*) malloc(8);
      cycles[i]           	= (char*) malloc(7);
      RAT_contents[i]     	= (char**) malloc(32*sizeof(char*));
      RRAT_contents[i]    	= (char**) malloc(32*sizeof(char*));
      PRF_contents[i]     	= (char**) malloc(64*sizeof(char*));
      ROB_contents[i]     	= (char**) malloc(34*sizeof(char*));
      dispatch_contents[i]    	= (char**) malloc(4*sizeof(char*));
      RS_contents[i]    	= (char**) malloc(8*sizeof(char*));
      issue_contents[i]    	= (char**) malloc(8*sizeof(char*));
      FU_contents[i]    	= (char**) malloc(8*sizeof(char*));
      complete_contents[i]    	= (char**) malloc(4*sizeof(char*));
      retire_contents[i]    	= (char**) malloc(4*sizeof(char*));
      misc_contents[i]    	= (char**) malloc(num_misc_regs*sizeof(char*));
    }

    setup_gui(fp);

    // Main loop for retrieving data and taking commands from user
    char quit_flag = 0;
    char resp=0;
    char running=0;
    int mem_addr=0;
    char goto_flag = 0;
    char cycle_flag = 0;
    char done_received = 0;
    memset(readbuffer,'\0',sizeof(readbuffer));
    while(!quit_flag){
      if (!done_received) {
        fgets(readbuffer, sizeof(readbuffer), fp);
        ready_val = processinput();
      }
      if(strcmp(readbuffer,"DONE") == 0) {
        done_received = 1;
        done_time = history_num - 1;
      }
      if(ready_val == 1 || done_received == 1){
        if(echo_data == 0 && done_received == 1) {
          running = 0;
          timeout(-1);
          echo_data = 1;
          history_num--;
          history_num%=NUM_HISTORY;
        }
        if(echo_data != 0){
          parsedata(history_num);
        }
        history_num++;
        // keep track of whether time wrapped around yet
        if (history_num == NUM_HISTORY)
          time_wrapped = 1;
        history_num%=NUM_HISTORY;

        //we're done reading the reg values for this iteration
        if (done_received != 1) {
          write(CHILD_WRITE, "n", 1);
          write(CHILD_WRITE, &mem_addr, 2);
        }
        char continue_flag = 0;
        int hist_num_temp = (history_num-1)%NUM_HISTORY;
        if (history_num==0) hist_num_temp = NUM_HISTORY-1;
        char echo_data_tmp,continue_flag_tmp;

        while(continue_flag == 0){
          resp=getch();
          if(running == 1){
            continue_flag = 1;
          }
          if(running == 0 || resp == 'p'){ 
            if(resp == 'n' && hist_num_temp == (history_num-1)%NUM_HISTORY){
              if (!done_received)
                continue_flag = 1;
            }else if(resp == 'n'){
              //forward in time, but not up to present yet
              hist_num_temp++;
              hist_num_temp%=NUM_HISTORY;
              parsedata(hist_num_temp);
            }else if(resp == 'r'){
              echo_data = 0;
              running = 1;
              timeout(0);
              continue_flag = 1;
            }else if(resp == 'p'){
              echo_data = 1;
              timeout(-1);
              running = 0;
              parsedata(hist_num_temp);
            }else if(resp == 'q'){
              //quit
              continue_flag = 1;
              quit_flag = 1;
            }else if(resp == 'b'){
              //We're goin BACK IN TIME, woohoo!
              // Make sure not to wrap around to NUM_HISTORY-1 if we don't have valid
              // data there (time_wrapped set to 1 when we wrap around to history 0)
              if (hist_num_temp > 0) {
                hist_num_temp--;
                parsedata(hist_num_temp);
              } else if (time_wrapped == 1) {
                hist_num_temp = NUM_HISTORY-1;
                parsedata(hist_num_temp);
              }
            }else if(resp == 'g' || resp == 'c'){
              // See if user wants to jump to clock cycle instead of sim time
              cycle_flag = (resp == 'c');

              // go to specified simulation time (either in history or
              // forward in simulation time).
              stop_time = get_time();
              
              // see if we already have that time in history
              int tmp_time;
              int cur_time;
              int delta;
              if (cycle_flag)
                sscanf(cycles[hist_num_temp], "%u", &cur_time);
              else
                sscanf(timebuffer[hist_num_temp], "%u", &cur_time);
              delta = (stop_time > cur_time) ? 1 : -1;
              if ((hist_num_temp+delta)%NUM_HISTORY != history_num) {
                tmp_time=hist_num_temp;
                i= (hist_num_temp+delta >= 0) ? (hist_num_temp+delta)%NUM_HISTORY : NUM_HISTORY-1;
                while (i!=history_num) {
                  if (cycle_flag)
                    sscanf(cycles[i], "%u", &cur_time);
                  else
                    sscanf(timebuffer[i], "%u", &cur_time);
                  if ((delta == 1 && cur_time >= stop_time) ||
                      (delta == -1 && cur_time <= stop_time)) {
                    hist_num_temp = i;
                    parsedata(hist_num_temp);
                    stop_time = 0;
                    break;
                  }

                  if ((i+delta) >=0)
                    i = (i+delta)%NUM_HISTORY;
                  else {
                    if (time_wrapped == 1)
                      i = NUM_HISTORY - 1;
                    else {
                      parsedata(hist_num_temp);
                      stop_time = 0;
                      break;
                    }
                  }
                }
              }

              // If we looked backwards in history and didn't find stop_time
              // then give up
              if (i==history_num && (delta == -1 || done_received == 1))
                stop_time = 0;

              // Set flags so that we run forward in the simulation until
              // it either ends, or we hit the desired time
              if (stop_time > 0) {
                // grab current values
                echo_data = 0;
                running = 1;
                timeout(0);
                continue_flag = 1;
                goto_flag = 1;
              }
            }
          }
        }
        // if we're instructed to goto specific time, see if we're there
        int cur_time=0;
        if (goto_flag==1) {
          if (cycle_flag)
            sscanf(cycles[hist_num_temp], "%u", &cur_time);
          else
            sscanf(timebuffer[hist_num_temp], "%u", &cur_time);
          if ((cur_time >= stop_time) ||
              (strcmp(readbuffer,"DONE")==0) ) {
            goto_flag = 0;
            echo_data = 1;
            running = 0;
            timeout(-1);
            continue_flag = 0;
            //parsedata(hist_num_temp);
          }
        }
      }
    }
    refresh();
    delwin(title_win);
    endwin();
    fflush(stdout);
    if(resp == 'q'){
      fclose(fp2);
      write(CHILD_WRITE, "Z", 1);
      exit(0);
    }
    readbuffer[0] = 0;
    while(strncmp(readbuffer,"DONE",4) != 0){
      if(fgets(readbuffer, sizeof(readbuffer), fp) != NULL)
        fputs(readbuffer, fp2);
    }
    fclose(fp2);
    fflush(stdout);
    write(CHILD_WRITE, "Z", 1);
    printf("Child Done Execution\n");
    exit(0);
  } else {
    close(CHILD_READ);
    close(CHILD_WRITE);
    dup2(PARENT_WRITE, 1);
    close(PARENT_WRITE);
    
  }
}


// Function to make testbench block until debugger is ready to proceed
int waitforresponse(){
  static int mem_start = 0;
  char c=0;
  while(c!='n' && c!='Z') read(PARENT_READ,&c,1);
  if(c=='Z') exit(0);
  mem_start = read(PARENT_READ,&c,1);
  mem_start = mem_start << 8 + read(PARENT_READ,&c,1);
  return(mem_start);
}

void flushpipe(){
  char c=0;
  read(PARENT_READ, &c, 1);
}

// Function to return string representation of opcode given inst encoding
char *get_opcode_str(int inst, int valid_inst)
{
  int opcode, check;
  char *str;
  
  if (valid_inst == ((int)'x' - (int)'0'))
    str = "-";
  else if(!valid_inst)
    str = "-";
  else if(inst==NOOP_INST)
    str = "nop";
  else {
    opcode = (inst >> 26) & 0x0000003f;
    check = (inst>>5) & 0x0000007f;
    switch(opcode)
    {
      case 0x00: str = (inst == 0x555) ? "halt" : "call_pal"; break;
      case 0x08: str = "lda"; break;
      case 0x09: str = "ldah"; break;
      case 0x0a: str = "ldbu"; break;
      case 0x0b: str = "ldqu"; break;
      case 0x0c: str = "ldwu"; break;
      case 0x0d: str = "stw"; break;
      case 0x0e: str = "stb"; break;
      case 0x0f: str = "stqu"; break;
      case 0x10: // INTA_GRP
         switch(check)
         {
           case 0x00: str = "addl"; break;
           case 0x02: str = "s4addl"; break;
           case 0x09: str = "subl"; break;
           case 0x0b: str = "s4subl"; break;
           case 0x0f: str = "cmpbge"; break;
           case 0x12: str = "s8addl"; break;
           case 0x1b: str = "s8subl"; break;
           case 0x1d: str = "cmpult"; break;
           case 0x20: str = "addq"; break;
           case 0x22: str = "s4addq"; break;
           case 0x29: str = "subq"; break;
           case 0x2b: str = "s4subq"; break;
           case 0x2d: str = "cmpeq"; break;
           case 0x32: str = "s8addq"; break;
           case 0x3b: str = "s8subq"; break;
           case 0x3d: str = "cmpule"; break;
           case 0x40: str = "addlv"; break;
           case 0x49: str = "sublv"; break;
           case 0x4d: str = "cmplt"; break;
           case 0x60: str = "addqv"; break;
           case 0x69: str = "subqv"; break;
           case 0x6d: str = "cmple"; break;
           default: str = "invalid"; break;
         }
         break;
      case 0x11: // INTL_GRP
         switch(check)
         {
           case 0x00: str = "and"; break;
           case 0x08: str = "bic"; break;
           case 0x14: str = "cmovlbs"; break;
           case 0x16: str = "cmovlbc"; break;
           case 0x20: str = "bis"; break;
           case 0x24: str = "cmoveq"; break;
           case 0x26: str = "cmovne"; break;
           case 0x28: str = "ornot"; break;
           case 0x40: str = "xor"; break;
           case 0x44: str = "cmovlt"; break;
           case 0x46: str = "cmovge"; break;
           case 0x48: str = "eqv"; break;
           case 0x61: str = "amask"; break;
           case 0x64: str = "cmovle"; break;
           case 0x66: str = "cmovgt"; break;
           case 0x6c: str = "implver"; break;
           default: str = "invalid"; break;
         }
         break;
      case 0x12: // INTS_GRP
         switch(check)
         {
           case 0x02: str = "mskbl"; break;
           case 0x06: str = "extbl"; break;
           case 0x0b: str = "insbl"; break;
           case 0x12: str = "mskwl"; break;
           case 0x16: str = "extwl"; break;
           case 0x1b: str = "inswl"; break;
           case 0x22: str = "mskll"; break;
           case 0x26: str = "extll"; break;
           case 0x2b: str = "insll"; break;
           case 0x30: str = "zap"; break;
           case 0x31: str = "zapnot"; break;
           case 0x32: str = "mskql"; break;
           case 0x34: str = "srl"; break;
           case 0x36: str = "extql"; break;
           case 0x39: str = "sll"; break;
           case 0x3b: str = "insql"; break;
           case 0x3c: str = "sra"; break;
           case 0x52: str = "mskwh"; break;
           case 0x57: str = "inswh"; break;
           case 0x5a: str = "extwh"; break;
           case 0x62: str = "msklh"; break;
           case 0x67: str = "inslh"; break;
           case 0x6a: str = "extlh"; break;
           case 0x72: str = "mskqh"; break;
           case 0x77: str = "insqh"; break;
           case 0x7a: str = "extqh"; break;
           default: str = "invalid"; break;
         }
         break;
      case 0x13: // INTM_GRP
         switch(check)
         {
           case 0x00: str = "mull"; break;
           case 0x20: str = "mulq"; break;
           case 0x30: str = "umulh"; break;
           case 0x40: str = "mullv"; break;
           case 0x60: str = "mulqv"; break;
           default: str = "invalid"; break;
         }
         break;
      case 0x14: str = "itfp"; break; // unimplemented
      case 0x15: str = "fltv"; break; // unimplemented
      case 0x16: str = "flti"; break; // unimplemented
      case 0x17: str = "fltl"; break; // unimplemented
      case 0x1a: str = "jsr"; break;
      case 0x1c: str = "ftpi"; break;
      case 0x20: str = "ldf"; break;
      case 0x21: str = "ldg"; break;
      case 0x22: str = "lds"; break;
      case 0x23: str = "ldt"; break;
      case 0x24: str = "stf"; break;
      case 0x25: str = "stg"; break;
      case 0x26: str = "sts"; break;
      case 0x27: str = "stt"; break;
      case 0x28: str = "ldl"; break;
      case 0x29: str = "ldq"; break;
      case 0x2a: str = "ldll"; break;
      case 0x2b: str = "ldql"; break;
      case 0x2c: str = "stl"; break;
      case 0x2d: str = "stq"; break;
      case 0x2e: str = "stlc"; break;
      case 0x2f: str = "stqc"; break;
      case 0x30: str = "br"; break;
      case 0x31: str = "fbeq"; break;
      case 0x32: str = "fblt"; break;
      case 0x33: str = "fble"; break;
      case 0x34: str = "bsr"; break;
      case 0x35: str = "fbne"; break;
      case 0x36: str = "fbge"; break;
      case 0x37: str = "fbgt"; break;
      case 0x38: str = "blbc"; break;
      case 0x39: str = "beq"; break;
      case 0x3a: str = "blt"; break;
      case 0x3b: str = "ble"; break;
      case 0x3c: str = "blbs"; break;
      case 0x3d: str = "bne"; break;
      case 0x3e: str = "bge"; break;
      case 0x3f: str = "bgt"; break;
      default: str = "invalid"; break;
    }
  }

  return str;
}

// Function to parse register $display() from testbench and add to
// names/contents arrays
void parse_register(char *readbuf, int reg_num, char*** contents) {
  char val_buf[256];
  int tmp_len;

  sscanf(readbuf,"%*c%d:%s",&tmp_len,val_buf);
  int i=0;
  for (;i<NUM_HISTORY;i++){
    contents[i][reg_num] = (char*) malloc((tmp_len+1)*sizeof(char));
  }
  strcpy(contents[history_num][reg_num],val_buf);
}


// Ask user for simulation time to stop at
// Since the enter key isn't detected, user must press 'g' key
//  when finished entering a number.
int get_time() {
  int col = COLS/2-6;
  wattron(title_win,A_REVERSE);
  mvwprintw(title_win,1,col,"goto time: ");
  wrefresh(title_win);
  int resp=0;
  int ptr = 0;
  char buf[32];
  int i;
  
  resp=wgetch(title_win);
  while(resp != 'g' && resp != KEY_ENTER && resp != ERR && ptr < 6) {
    if (isdigit((char)resp)) {
      waddch(title_win,(char)resp);
      wrefresh(title_win);
      buf[ptr++] = (char)resp;
    }
    resp=wgetch(title_win);
  }

  // Clean up title window
  wattroff(title_win,A_REVERSE);
  mvwprintw(title_win,1,col,"           ");
  for(i=0;i<ptr;i++)
    waddch(title_win,' ');

  wrefresh(title_win);

  buf[ptr] = '\0';
  return atoi(buf);
}
