/////////////////////////////////////////////////////////////////////////
//                                                                     //
//                                                                     //
//   Modulename :  testbench.v                                         //
//                                                                     //
//  Description :  Testbench module for the verisimple pipeline;       //
//                                                                     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

module testbench;

  // Registers and wires used in the testbench
  logic        clock;
  logic        reset;

  logic  [1:0] proc2mem_command;
  logic [63:0] proc2mem_addr;
  logic [63:0] proc2mem_data;
  logic  [3:0] mem2proc_response;
  logic [63:0] mem2proc_data;
  logic  [3:0] mem2proc_tag;


	logic [1:0] [31:0] IR;
	logic [1:0] [63:0] NPC;
				
	logic [1:0] [`PRF_IDX-1:0] ra_prf_out;// reg A prf
  logic [1:0] [`PRF_IDX-1:0] rb_prf_out;// reg B prf
  logic [1:0] [`PRF_IDX-1:0] dest_prf_out;// destination (writeback) register index
									        // (ZERO_REG if no writeback)

  logic  [1:0] [1:0] opa_select_out;    // ALU opa mux select (ALU_OPA_xxx *)
  logic  [1:0] [1:0] opb_select_out;    // ALU opb mux select (ALU_OPB_xxx *)
  logic  [1:0] [4:0] alu_func_out;      // ALU function select (ALU_xxx *)
  logic  [1:0]      rd_mem_out;        // does inst read memory?
  logic  [1:0]      wr_mem_out;        // does inst write memory?
  logic  [1:0]      ldl_mem_out;       // load-lock inst?
  logic  [1:0]      stc_mem_out;       // store-conditional inst?
  logic  [1:0]      cond_branch_out;   // is inst a conditional branch?
  logic  [1:0]      uncond_branch_out; // is inst an unconditional branch 
									        // or jump?
  logic  [1:0]      halt_out;
  logic  [1:0]      cpuid_out;         // get CPUID inst?
  logic  [1:0]      illegal_out;
	logic [1:0] id_valid_inst_out;

	assign proc2mem_data=0;

  // Strings to hold instruction opcode
  logic  [8*7:0] if_instr_str;




//-------------Input that Can be changed
	//------From ROB------	
	logic ROB_full;
	logic flush; //Branch Misprediction
	logic [63:0] Br_target;

	logic commit1;
	logic [4:0] arf_dest1;
	logic [`PRF_IDX-1:0] prf_dest1;

	//------From RS------	
	logic RS_full;


	FrontEnd FrontEnd1(
				//------From/To Mem------					
				.Imem2proc_response(mem2proc_response),
				.mem2proc_data(mem2proc_data),    
				.mem2proc_tag(mem2proc_tag),      

				.proc2Imem_command(proc2mem_command),  
				.proc2Imem_addr(proc2mem_addr),     


				//------To RS & ROB------	
				.IR,
				.NPC,
							
				.ra_prf_out,// reg A prf
			  .rb_prf_out,// reg B prf
			  .dest_prf_out,// destination (writeback) register index
												        // (ZERO_REG if no writeback)

			  .opa_select_out,    // ALU opa mux select (ALU_OPA_xxx *)
			  .opb_select_out,    // ALU opb mux select (ALU_OPB_xxx *)
			  .alu_func_out,      // ALU function select (ALU_xxx *)
			  .rd_mem_out,        // does inst read memory?
			  .wr_mem_out,        // does inst write memory?
			  .ldl_mem_out,       // load-lock inst?
			  .stc_mem_out,       // store-conditional inst?
			  .cond_branch_out,   // is inst a conditional branch?
			  .uncond_branch_out, // is inst an unconditional branch 
												        // or jump?
			  .halt_out,
			  .cpuid_out,         // get CPUID inst?
			  .illegal_out,
				.id_valid_inst_out,

				//------From ROB------	
				.ROB_full,
				.flush, //Branch Misprediction
				.Br_target,

				.commit1,
				.arf_dest1,
				.prf_dest1,

							//Supporting 2-way committing 2 instr
				.commit2(1'b0),
				.arf_dest2(5'b0),
				.prf_dest2(6'b0),

				//------From RS------	
				.RS_full,

				.clock,
				.reset
);



  // Instantiate the Data Memory
  mem memory (// Inputs
            .clk               (clock),
            .proc2mem_command  (proc2mem_command),
            .proc2mem_addr     (proc2mem_addr),
            .proc2mem_data     (proc2mem_data),

             // Outputs
            .mem2proc_response (mem2proc_response),
            .mem2proc_data     (mem2proc_data),
            .mem2proc_tag      (mem2proc_tag)
           );


  // Generate System Clock
  always
  begin
    #(`VERILOG_CLOCK_PERIOD/2.0);
    clock = ~clock;
  end

	initial
		begin
		#(`VERILOG_CLOCK_PERIOD/2.0*500);
		$finish;
		end

  initial
  begin
      
    clock = 1'b0;
    reset = 1'b0;

		ROB_full=0;
		flush=0;
		Br_target=0;
		commit1=0;
		arf_dest1=0;
		prf_dest1=0;
		RS_full=0;



    reset = 1'b1; 
    @(posedge clock);
    @(posedge clock);
    `SD;
    reset = 1'b0;

			while(1)
			begin
		    @(negedge clock);
				$display("@@@ instruction fetched %s", if_instr_str);
			end
  end




  // Translate IRs into strings for opcodes (for waveform viewer)
  always_comb begin
    if_instr_str  = get_instr_string(IR, id_valid_inst_out);
  end

  function [8*7:0] get_instr_string;
    input [31:0] IR;
    input        instr_valid;
    begin
      if (!instr_valid)
        get_instr_string = "-";
      else if (IR==`NOOP_INST)
        get_instr_string = "nop";
      else
        case (IR[31:26])
          6'h00: get_instr_string = (IR == 32'h555) ? "halt" : "call_pal";
          6'h08: get_instr_string = "lda";
          6'h09: get_instr_string = "ldah";
          6'h0a: get_instr_string = "ldbu";
          6'h0b: get_instr_string = "ldqu";
          6'h0c: get_instr_string = "ldwu";
          6'h0d: get_instr_string = "stw";
          6'h0e: get_instr_string = "stb";
          6'h0f: get_instr_string = "stqu";
          6'h10: // INTA_GRP
            begin
              case (IR[11:5])
                7'h00: get_instr_string = "addl";
                7'h02: get_instr_string = "s4addl";
                7'h09: get_instr_string = "subl";
                7'h0b: get_instr_string = "s4subl";
                7'h0f: get_instr_string = "cmpbge";
                7'h12: get_instr_string = "s8addl";
                7'h1b: get_instr_string = "s8subl";
                7'h1d: get_instr_string = "cmpult";
                7'h20: get_instr_string = "addq";
                7'h22: get_instr_string = "s4addq";
                7'h29: get_instr_string = "subq";
                7'h2b: get_instr_string = "s4subq";
                7'h2d: get_instr_string = "cmpeq";
                7'h32: get_instr_string = "s8addq";
                7'h3b: get_instr_string = "s8subq";
                7'h3d: get_instr_string = "cmpule";
                7'h40: get_instr_string = "addlv";
                7'h49: get_instr_string = "sublv";
                7'h4d: get_instr_string = "cmplt";
                7'h60: get_instr_string = "addqv";
                7'h69: get_instr_string = "subqv";
                7'h6d: get_instr_string = "cmple";
                default: get_instr_string = "invalid";
              endcase
            end
          6'h11: // INTL_GRP
            begin
              case (IR[11:5])
                7'h00: get_instr_string = "and";
                7'h08: get_instr_string = "bic";
                7'h14: get_instr_string = "cmovlbs";
                7'h16: get_instr_string = "cmovlbc";
                7'h20: get_instr_string = "bis";
                7'h24: get_instr_string = "cmoveq";
                7'h26: get_instr_string = "cmovne";
                7'h28: get_instr_string = "ornot";
                7'h40: get_instr_string = "xor";
                7'h44: get_instr_string = "cmovlt";
                7'h46: get_instr_string = "cmovge";
                7'h48: get_instr_string = "eqv";
                7'h61: get_instr_string = "amask";
                7'h64: get_instr_string = "cmovle";
                7'h66: get_instr_string = "cmovgt";
                7'h6c: get_instr_string = "implver";
                default: get_instr_string = "invalid";
              endcase
            end
          6'h12: // INTS_GRP
            begin
              case(IR[11:5])
                7'h02: get_instr_string = "mskbl";
                7'h06: get_instr_string = "extbl";
                7'h0b: get_instr_string = "insbl";
                7'h12: get_instr_string = "mskwl";
                7'h16: get_instr_string = "extwl";
                7'h1b: get_instr_string = "inswl";
                7'h22: get_instr_string = "mskll";
                7'h26: get_instr_string = "extll";
                7'h2b: get_instr_string = "insll";
                7'h30: get_instr_string = "zap";
                7'h31: get_instr_string = "zapnot";
                7'h32: get_instr_string = "mskql";
                7'h34: get_instr_string = "srl";
                7'h36: get_instr_string = "extql";
                7'h39: get_instr_string = "sll";
                7'h3b: get_instr_string = "insql";
                7'h3c: get_instr_string = "sra";
                7'h52: get_instr_string = "mskwh";
                7'h57: get_instr_string = "inswh";
                7'h5a: get_instr_string = "extwh";
                7'h62: get_instr_string = "msklh";
                7'h67: get_instr_string = "inslh";
                7'h6a: get_instr_string = "extlh";
                7'h72: get_instr_string = "mskqh";
                7'h77: get_instr_string = "insqh";
                7'h7a: get_instr_string = "extqh";
                default: get_instr_string = "invalid";
              endcase
            end
          6'h13: // INTM_GRP
            begin
              case (IR[11:5])
                7'h01: get_instr_string = "mull";
                7'h20: get_instr_string = "mulq";
                7'h30: get_instr_string = "umulh";
                7'h40: get_instr_string = "mullv";
                7'h60: get_instr_string = "mulqv";
                default: get_instr_string = "invalid";
              endcase
            end
          6'h14: get_instr_string = "itfp"; // unimplemented
          6'h15: get_instr_string = "fltv"; // unimplemented
          6'h16: get_instr_string = "flti"; // unimplemented
          6'h17: get_instr_string = "fltl"; // unimplemented
          6'h1a: get_instr_string = "jsr";
          6'h1c: get_instr_string = "ftpi";
          6'h20: get_instr_string = "ldf";
          6'h21: get_instr_string = "ldg";
          6'h22: get_instr_string = "lds";
          6'h23: get_instr_string = "ldt";
          6'h24: get_instr_string = "stf";
          6'h25: get_instr_string = "stg";
          6'h26: get_instr_string = "sts";
          6'h27: get_instr_string = "stt";
          6'h28: get_instr_string = "ldl";
          6'h29: get_instr_string = "ldq";
          6'h2a: get_instr_string = "ldll";
          6'h2b: get_instr_string = "ldql";
          6'h2c: get_instr_string = "stl";
          6'h2d: get_instr_string = "stq";
          6'h2e: get_instr_string = "stlc";
          6'h2f: get_instr_string = "stqc";
          6'h30: get_instr_string = "br";
          6'h31: get_instr_string = "fbeq";
          6'h32: get_instr_string = "fblt";
          6'h33: get_instr_string = "fble";
          6'h34: get_instr_string = "bsr";
          6'h35: get_instr_string = "fbne";
          6'h36: get_instr_string = "fbge";
          6'h37: get_instr_string = "fbgt";
          6'h38: get_instr_string = "blbc";
          6'h39: get_instr_string = "beq";
          6'h3a: get_instr_string = "blt";
          6'h3b: get_instr_string = "ble";
          6'h3c: get_instr_string = "blbs";
          6'h3d: get_instr_string = "bne";
          6'h3e: get_instr_string = "bge";
          6'h3f: get_instr_string = "bgt";
          default: get_instr_string = "invalid";
        endcase
    end
  endfunction

endmodule  // module testbench

