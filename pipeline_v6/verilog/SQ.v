
module sq(
	  // From Front End: Dispatch
   	  //input logic  [1:0]	 				instr_valid,
   	  input logic  [1:0]	 				instr_is_st, //make sure it's valid and store
	  input logic  [1:0] [`PRF_SIZE_LOG-1:0] 		regB,
   	  input logic  [1:0] [`ROB_SIZE_LOG-1:0] 		ROB,
   	  input logic  [1:0] [`BRAT_SIZE_LOG:0] 		BRAT,
   	  input logic  [1:0] [63:0] 				NPC,
   	  input logic  [1:0] [31:0] 				IR,

	  //Early Branch Resolution
	  input logic  [`SQ_SIZE_LOG-1:0]			Bsq_tail, //From Front End, on a misprediction, replace current tail

	  // From BOB BRAT: Mispredict + dispatch + BRAT_retire
	  input logic  					branch_mis,
	  input logic  [`BRAT_SIZE:0]				branch_mis_brat,
	  input logic  [1:0]                                   BRAT_CMT_v,
	  input logic  [1:0] [`BRAT_SIZE_LOG-1:0]		BRAT_CMT_num,
	  
	  //Inputs from MEM ALU: ALU
	  //input logic  [1:0] 					ALU_LSQ_valid,
	  input logic  [1:0] 					ALU_LSQ_ld_or_st,
	  input logic  [1:0] [`SQ_SIZE_LOG-1:0]			ALU_LSQ_SQ_addr,
	  input logic  [1:0] [63:0]				ALU_LSQ_MEM_addr,

	  // From Dcache: decache accept the store issue or not: Issue
	  input logic 						LSQ_not_erase_Dcache,
	  // Inputs from ROB: Issue
	  input logic 						ROB_head_is_store,
	  input logic 	[`ROB_SIZE_LOG-1:0]			ROB_head,
	  // Output to ROB: Issue
	  output logic 						SQ_CMP,

	  // Outputs to LQ
	  output logic  [`SQ_SIZE-1:0]				sq_valid,
	  output logic  [`SQ_SIZE-1:0]				sq_addr_valid,
	  output logic  [`SQ_SIZE-1:0] [63:0]			sq_MEM_addr,
	  output logic  [`SQ_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 	sq_regB,
	  output logic  [`SQ_SIZE-1:0] [`ROB_SIZE_LOG-1:0]	sq_ROB,
	  output logic  [`SQ_SIZE_LOG-1:0]			sq_head,
	  output logic  [`SQ_SIZE_LOG-1:0]			sq_tail,

	  // Outputs to Dcache
	  output logic  					SQ_Dcache_valid,
	  output logic  [63:0]					SQ_Dcache_NPC,
	  output logic  [31:0]					SQ_Dcache_IR,
	  output logic  [`ROB_SIZE_LOG-1:0]			SQ_Dcache_ROB,
	  output logic  [`BRAT_SIZE_LOG:0]			SQ_Dcache_BRAT,
	  output logic  [63:0]					SQ_Dcache_MEM_addr,
	  output logic  [`PRF_SIZE_LOG-1:0]			SQ_Dcache_regB,

	  // Outputs to FrontEnd
	  output logic  [1:0]					SQ_full,

	  input logic  clock,
	  input logic  reset
	  );

	// Registers for SQ
	//logic [`SQ_SIZE-1:0] 				sq_valid;
	//logic [`SQ_SIZE-1:0] 				sq_addr_valid;
	//logic [`SQ_SIZE-1:0] [63:0] 				sq_MEM_addr;
	//logic [`SQ_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		sq_regB;
	//logic [`SQ_SIZE-1:0] [`ROB_SIZE_LOG-1:0] 		sq_ROB;
	logic [`SQ_SIZE-1:0] [`BRAT_SIZE_LOG:0] 		sq_BRAT;
	logic [`SQ_SIZE-1:0] [63:0] 				sq_NPC;
	logic [`SQ_SIZE-1:0] [31:0] 				sq_IR;
	//logic [`SQ_SIZE_LOG-1:0]				sq_head;
	//logic [`SQ_SIZE_LOG-1:0]				sq_tail;

	// Wires for next LQ regs
	logic [`SQ_SIZE-1:0] 				next_sq_valid;
	logic [`SQ_SIZE-1:0] 				next_sq_addr_valid;
	logic [`SQ_SIZE-1:0] [63:0] 				next_sq_MEM_addr;
	logic [`SQ_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 		next_sq_regB;
	logic [`SQ_SIZE-1:0] [`ROB_SIZE_LOG-1:0] 		next_sq_ROB;
	logic [`SQ_SIZE-1:0] [`BRAT_SIZE_LOG:0] 		next_sq_BRAT;
	logic [`SQ_SIZE-1:0] [63:0] 				next_sq_NPC;
	logic [`SQ_SIZE-1:0] [31:0] 				next_sq_IR;
	logic [`SQ_SIZE_LOG-1:0]				next_sq_head;
	logic [`SQ_SIZE_LOG-1:0]				next_sq_tail;
   

	wire [`SQ_SIZE_LOG-1:0]	sq_tail_plus_1 = sq_tail + 1'b1;
	wire [`SQ_SIZE_LOG-1:0]	sq_tail_plus_2 = sq_tail + 2'b10;

   assign SQ_full[0]= sq_valid[sq_tail+1'b1];
   assign SQ_full[1]= sq_valid[sq_tail];

	always_comb
	  begin
		next_sq_valid = sq_valid; //branch mis | dispatch + issue 
		next_sq_addr_valid = sq_addr_valid; //branch mis | alu + issue 
		next_sq_MEM_addr = sq_MEM_addr; // alu
		next_sq_regB = sq_regB; //dispatch
		next_sq_ROB = sq_ROB; //dispatch 
		next_sq_BRAT = sq_BRAT; //dispatch | BRAT retire
		next_sq_NPC = sq_NPC; //dispatch
		next_sq_IR = sq_IR; //dispatch
		next_sq_head = sq_head; //issue
		next_sq_tail = sq_tail; //branch mis | dispatch

		SQ_Dcache_valid = 0;
		SQ_Dcache_NPC = 0;
		SQ_Dcache_IR = 0;
		SQ_Dcache_ROB = 0;
		SQ_Dcache_BRAT = {1'b1,`BRAT_SIZE_LOG'b0};
		SQ_Dcache_MEM_addr = 0;
		SQ_Dcache_regB = 0;
		SQ_CMP = 1'b0;

		if(branch_mis)
		  begin
			for(integer i=0; i<`SQ_SIZE; i=i+1)
			  begin
				if(branch_mis_brat[sq_BRAT[i]]) 
				  begin
					next_sq_valid[i] = 1'b0;
					next_sq_addr_valid[i] = 1'b0;
					next_sq_tail = Bsq_tail;
				  end
			  end
		  end//if(branch_miss)
		else
		  begin
			//*on a BRAT retire
			for(integer i=0; i<`SQ_SIZE; i=i+1)
			  begin
				if (BRAT_CMT_v[0] && sq_BRAT[i]==BRAT_CMT_num[0])
				  next_sq_BRAT[i] = {1'b1,`BRAT_SIZE_LOG'b0};
			     	if (BRAT_CMT_v[1] && sq_BRAT[i]==BRAT_CMT_num[1])
			       	  next_sq_BRAT[i] = {1'b1,`BRAT_SIZE_LOG'b0};
			  end

			//on a dispatch
			case(instr_is_st)
			  2'b01, 2'b10:
			    begin
				next_sq_valid[sq_tail] = 1'b1;
				next_sq_regB[sq_tail] = instr_is_st[0]? regB[0]:regB[1];
				next_sq_ROB[sq_tail] = instr_is_st[0]? ROB[0]:ROB[1];
				next_sq_BRAT[sq_tail] = instr_is_st[0]? BRAT[0]:BRAT[1];
				next_sq_NPC[sq_tail] = instr_is_st[0]? NPC[0]:NPC[1];
				next_sq_IR[sq_tail] = instr_is_st[0]? IR[0]:IR[1];
				next_sq_tail = sq_tail_plus_1;
			    end
			  2'b11:
			    begin
				next_sq_valid[sq_tail] = 1'b1;
				next_sq_regB[sq_tail] = regB[0];
				next_sq_ROB[sq_tail] = ROB[0];
				next_sq_BRAT[sq_tail] = BRAT[0];
				next_sq_NPC[sq_tail] = NPC[0];
				next_sq_IR[sq_tail] = IR[0];
				next_sq_valid[sq_tail_plus_1] = 1'b1;
				next_sq_regB[sq_tail_plus_1] = regB[1];
				next_sq_ROB[sq_tail_plus_1] = ROB[1];
				next_sq_BRAT[sq_tail_plus_1] = BRAT[1];
				next_sq_NPC[sq_tail_plus_1] = NPC[1];
				next_sq_IR[sq_tail_plus_1] = IR[1];
				next_sq_tail = sq_tail_plus_2;
			    end
			endcase

			//on a alu dispatch
			if(ALU_LSQ_ld_or_st[0])
			  begin
				next_sq_addr_valid[ALU_LSQ_SQ_addr[0]] = 1'b1;
				next_sq_MEM_addr[ALU_LSQ_SQ_addr[0]] = ALU_LSQ_MEM_addr[0];
			  end
			if(ALU_LSQ_ld_or_st[1])
			  begin
				next_sq_addr_valid[ALU_LSQ_SQ_addr[1]] = 1'b1;
				next_sq_MEM_addr[ALU_LSQ_SQ_addr[1]] = ALU_LSQ_MEM_addr[1];
			  end

			//on a issue
			if(ROB_head_is_store && ROB_head == sq_ROB[sq_head] && ~LSQ_not_erase_Dcache && sq_addr_valid[sq_head])
			  begin
				SQ_Dcache_valid = 1'b1;
				SQ_Dcache_NPC = sq_NPC[sq_head];
				SQ_Dcache_IR = sq_IR[sq_head];
				SQ_Dcache_ROB = sq_ROB[sq_head];
				SQ_Dcache_MEM_addr = sq_MEM_addr[sq_head];
				SQ_Dcache_regB = sq_regB[sq_head];
				SQ_CMP = 1'b1;

				next_sq_valid[sq_head] = 1'b0;
				next_sq_addr_valid[sq_head] = 1'b0;
				next_sq_head = sq_head + 1'b1;
			  end
		  end//else
	  end//always_comb


   genvar i;
   generate
      for (i=0;i<`SQ_SIZE;i++)
	begin
	   always_ff@(posedge clock)
	     begin
		if(reset)
		  begin
		     sq_NPC[i] 	 	<= `SD 0;
		     sq_IR[i]  		<= `SD 0;
		     sq_valid[i] 	<= `SD 0;
		     sq_addr_valid[i] 	<= `SD 0;
		     sq_MEM_addr[i] 	<= `SD 0;
		     sq_regB[i] 	<= `SD 0;
		     sq_ROB[i] 	 	<= `SD 0;
		     sq_BRAT[i]  	<= `SD 0;
		  end
		else
		  begin
		     sq_NPC[i] 	 	<= `SD next_sq_NPC[i];
		     sq_IR[i]  		<= `SD next_sq_IR[i];
		     sq_valid[i] 	<= `SD next_sq_valid[i];
		     sq_addr_valid[i] 	<= `SD next_sq_addr_valid[i];
		     sq_MEM_addr[i] 	<= `SD next_sq_MEM_addr[i];
		     sq_regB[i] 	<= `SD next_sq_regB[i];
		     sq_ROB[i] 	 	<= `SD next_sq_ROB[i];
		     sq_BRAT[i]  	<= `SD next_sq_BRAT[i];
		  end
	     end // always_ff@ (posedge clock)
	end // for (i=0;i<`SQ_SIZE;i++)
   endgenerate

	always_ff@(posedge clock)
	  begin
		if(reset)
		  begin
			sq_head  	<= `SD 0;
			sq_tail  	<= `SD 0;
		  end
		else
		  begin
			sq_head  	<= `SD next_sq_head;
			sq_tail  	<= `SD next_sq_tail;
		  end
	  end
endmodule // sq
