`define IB_SIZE 8 //PrefetchBuffer Size
`define IB_SIZE_LOG 3

module icache(
				//MEM
			  output logic  [1:0] proc2Imem_command,
			  output logic [63:0] proc2Imem_addr,

			  input   [3:0] Imem2proc_response,
			  input  [63:0] Imem2proc_data,
			  input   [3:0] Imem2proc_tag,

				//FE
			  input  [63:0] proc2Icache_addr,
			  output logic [63:0] Icache_data_out,     // value is memory[proc2Icache_addr]
			  output logic        Icache_valid_out,    // when this is high

				//read cache
			  output logic  [4:0] current_index,
			  output logic [23:0] current_tag,
			  input  [63:0] cachemem_data,
			  input         cachemem_valid,
				//write cache
			  output logic  [4:0] data_write_index,
			  output logic [23:0] data_write_tag,
			  output logic        data_write_enable,

			  input         clock,
			  input         reset
			  
             );


	logic  [4:0] last_index;
	logic [23:0] last_tag;
	
	
  logic miss_outstanding;
	logic IB_miss; //prefetch buffer miss: no valid entry in prefetch buffer has the current PC

	logic [63:0] start_addr;
	logic start, stop;


	logic inprocess, next_inprocess;
	logic [`IB_SIZE_LOG:0] cnt, next_cnt;

	logic [`IB_SIZE-1:0] FIFO_valid;
	logic [`IB_SIZE-1:0] [63:0] FIFO_addr;
	logic [`IB_SIZE-1:0] [3:0] FIFO_tag;
	//logic [`IB_SIZE-1:0] [63:0] FIFO_IR;

	logic [`IB_SIZE-1:0] next_FIFO_valid;
	logic [`IB_SIZE-1:0] [63:0] next_FIFO_addr;
	logic [`IB_SIZE-1:0] [3:0] next_FIFO_tag;
	//logic [`IB_SIZE-1:0] [63:0] next_FIFO_IR;


	//if change addr: I need to first access cache and judge whether cache miss or not
  assign {current_tag, current_index} = proc2Icache_addr[31:3];

  assign Icache_data_out = cachemem_data;
  assign Icache_valid_out = cachemem_valid; 
	assign start_addr = proc2Icache_addr[31:3];

  wire changed_addr = (current_index!=last_index) || (current_tag!=last_tag);

	always_comb
		begin
			IB_miss = 1;
			for(integer i=0; i<`IB_SIZE; i=i+1)
				begin
					if(FIFO_valid[i] && FIFO_addr[i]==proc2Icache_addr[31:3]) IB_miss = 0;
				end
		end

	assign start = changed_addr & ~cachemem_valid & IB_miss;
	assign stop = 1'b0;
	//assign stop =({current_tag, current_index}!={last_tag, last_index}) && (({current_tag, current_index}-{last_tag, last_index})!=1);
	//assign stop =IB_miss && !cache_valid;
	//assign stop =({current_tag, current_index}<FIFO_addr[0]) || ({current_tag, current_index}>(FIFO_addr[0]+`IB_SIZE-1));

     
	always_comb
		begin
			next_cnt = cnt;
			next_FIFO_valid = FIFO_valid;
			next_FIFO_addr = FIFO_addr;
			next_FIFO_tag = FIFO_tag;
			//next_FIFO_IR = FIFO_IR;
			next_inprocess = inprocess;
			data_write_enable = 1'b0;
			{data_write_tag,data_write_index} = 0;
			proc2Imem_command = 0;
			proc2Imem_addr = 0;

			if(start) begin
				for(integer i=1; i<`IB_SIZE ;i=i+1)begin
					next_FIFO_valid[i] = 0;
					next_FIFO_tag[i] = 0;
					next_FIFO_addr[i] = 0;
				end//for

				next_inprocess = 1'b1;
				next_cnt = 0;
				next_FIFO_valid[0] = 1'b1;
				next_FIFO_addr[0] = start_addr;
				next_FIFO_tag[0] = 0;
			end//if(start)

			else if(stop) begin
				next_inprocess = 0;
				for(integer i=0; i<`IB_SIZE ;i=i+1)begin
					next_FIFO_valid[i] = 0;
					next_FIFO_tag[i] = 0;
					next_FIFO_addr[i] = 0;
				end//for
			end//if(stop)

			else if(inprocess) begin				
				proc2Imem_addr = {FIFO_addr[cnt],3'b0};
				proc2Imem_command = `BUS_LOAD;

				if(Imem2proc_response!=0) begin
					if(cnt == `IB_SIZE-1) begin
							next_inprocess = 1'b0;
							next_FIFO_tag[cnt] = Imem2proc_response;
						end
					else begin
						next_cnt = cnt+1'b1;
						next_FIFO_tag[cnt] = Imem2proc_response;

						next_FIFO_valid[next_cnt] = 1'b1;
						next_FIFO_addr[next_cnt] = FIFO_addr[0] + next_cnt;
					end
				end//if(Imem2proc_response!=0)
			end//if(inprocess)

			if(Imem2proc_tag!=0 && !start && !stop)
				begin
					for(integer i=0; i<`IB_SIZE ;i=i+1)
						begin
							if(FIFO_valid[i] && FIFO_tag[i]==Imem2proc_tag)
							begin
								next_FIFO_valid[i]=1'b0;
								data_write_enable = 1'b1;
								{data_write_tag,data_write_index} = FIFO_addr[i];
							end
						end
				end//if(Imem2proc_tag!=0)
		end


  always_ff @(posedge clock)
		begin
			if(reset)
				begin
					for(integer i=0; i<`IB_SIZE ;i=i+1)begin
						FIFO_valid[i] <= `SD 0;
						FIFO_addr[i] <= `SD 0;
						FIFO_tag[i] <= `SD 0;
						//FIFO_IR[i] <= `SD 0;
						cnt <= `SD 0;
						inprocess <= `SD 0;
					end//for
				end//if(reset)
			else
				begin
						FIFO_valid <= `SD next_FIFO_valid;
						FIFO_addr <= `SD next_FIFO_addr;
						FIFO_tag <= `SD next_FIFO_tag;
						//FIFO_IR <= `SD next_FIFO_IR;
						cnt <= `SD next_cnt;
						inprocess <= `SD next_inprocess;
				end//else
		end//always_ff


  always_ff @(posedge clock)
  begin
    if(reset)
    begin
      last_index       <= `SD -1;   // These are -1 to get ball rolling when
      last_tag         <= `SD -1;   // reset goes low because addr "changes"
    end
    else
    begin
      last_index       <= `SD current_index;
      last_tag         <= `SD current_tag;
		end
  end

endmodule

