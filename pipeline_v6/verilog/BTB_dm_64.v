/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  BTB_dm_64.v                                         //
//                                                                     //
//  Description :  This module is the 64 bit direct map BTB of         // 
//                 the Pipeline.                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
// Notice: what I want is current PC, not NPC
//////////////////////////////////////////////////////////////

module BTB_dm_64 (
		  //////////////
		  // Inputs
		  //////////////
		  //input [`BTB_LINE_SIZE_LOG-1:0] 		rd_branch_addr,
		  //input [31-`BTB_LINE_SIZE_LOG-`BTB_SET_NUM:0]	rd_branch_index,
		  input 							clock,
		  input 							reset,
		  input [1:0][63:0] 			        		rd_branch_pc,
		  
		  input [1:0][63:0] 		        			BRAT_CMT_pc,
		  input [1:0]							BRAT_CMT_v,
		  input [1:0][63:0]						BRAT_CMT_targetaddr,

		  //////////////
		  // Outputs
		  //////////////
		  output [1:0][63:0]						rd_branch_target_out,
		  output [1:0]							rd_branch_valid_out
		  );

   logic [`BTB_LINE_SIZE-1:0] 							BTB_valid;
   logic [`BTB_LINE_SIZE-1:0][63-`BTB_LINE_SIZE_LOG-`BTB_SET_NUM_LOG-2:0] 	BTB_tag;
   logic [`BTB_LINE_SIZE-1:0][63:0] 						BTB_target;

   wire [1:0] [`BTB_LINE_SIZE-1:0] 							BTB_valid_next;
   wire [1:0] [`BTB_LINE_SIZE-1:0][63-`BTB_LINE_SIZE_LOG-`BTB_SET_NUM_LOG-2:0]  	BTB_tag_next;
   wire [1:0] [`BTB_LINE_SIZE-1:0][63:0] 						BTB_target_next;
   wire [1:0] [`BTB_LINE_SIZE-1:0]                                                      helper;

   
   
   logic   [1:0] [63:0] 								rd_branch_target_out_tmp;
   wor   [1:0] 									rd_branch_valid_out_tmp;

   genvar 									i,j;

   generate for(j=0;j<2;j++) begin
      for(i=0;i<`BTB_LINE_SIZE;i++)
          begin
	     assign helper[j][i] = (rd_branch_pc[j][`BTB_LINE_SIZE_LOG+1:2] == i) && 
					       (rd_branch_pc[j][63:`BTB_SET_NUM_LOG+`BTB_LINE_SIZE_LOG+2] == BTB_tag[i]) &&
					       BTB_valid[i] ;
	  end
   end
   endgenerate

   always_comb begin
	rd_branch_target_out_tmp = 128'b0;
	for (int k=0; k<`BTB_LINE_SIZE; k++) begin
		if (helper[0][k])
			rd_branch_target_out_tmp[0] = BTB_target[k];
		if (helper[1][k])
			rd_branch_target_out_tmp[1] = BTB_target[k];	
	end
   end

   assign rd_branch_target_out = rd_branch_target_out_tmp;
   assign rd_branch_valid_out = rd_branch_valid_out_tmp;
   generate for(j=0;j<2;j++)
     begin
	for(i=0;i<`BTB_LINE_SIZE;i++)
	  begin
	     assign rd_branch_valid_out_tmp[j] = helper[j][i] ? 1 : 0;
	     
	     assign BTB_valid_next[j][i] = (BRAT_CMT_v[j] &&
					 (BRAT_CMT_pc[j][`BTB_LINE_SIZE_LOG+1:2] == i)) ?
					1 :
					BTB_valid[i];
	     
	     assign BTB_tag_next[j][i] = (BRAT_CMT_v[j] &&
				       (BRAT_CMT_pc[j][`BTB_LINE_SIZE_LOG+1:2] == i)) ?
				      BRAT_CMT_pc[j][63:`BTB_SET_NUM_LOG+`BTB_LINE_SIZE_LOG+2] :
				      BTB_tag[i];
	     
	     assign BTB_target_next[j][i] = (BRAT_CMT_v[j] &&
					  (BRAT_CMT_pc[j][`BTB_LINE_SIZE_LOG+1:2] == i)) ?
					 BRAT_CMT_targetaddr[j] :
					 BTB_target[i];
	  end // for (i=0;i<`BTB_LINE_SIZE;i++)
     end // for (j=0;j<2;j++) 
   endgenerate
   
   generate for(i = 0;i < `BTB_LINE_SIZE;i++)
     begin
	always_ff@(posedge clock)
	  begin
	     if(reset)
	       begin
		  BTB_valid[i]  <= `SD 0;
		  BTB_tag[i]    <= `SD 0;
		  BTB_target[i] <= `SD 0;
	       end
	     else
	       begin
		  BTB_valid[i]  <= `SD  BTB_valid_next[0][i] | BTB_valid_next[1][i];
		  BTB_tag[i]    <= `SD  BTB_tag_next[0][i] | BTB_tag_next[1][i];
		  BTB_target[i] <= `SD  BTB_target_next[0][i] | BTB_target_next[1][i];
	       end // else: !if(reset)
	  end // always_ff@ (posedge clock)
     end // for (i = 0;i < `BTB_LINE_SIZE;i++)
   endgenerate
   
endmodule
