module lsq(
	  // Control Signals
	  input 					branch_mis,
	  input [`BRAT_SIZE:0]				branch_mis_brat,
	  input logic [`BRAT_SIZE_LOG-1:0] BRAT_CMP_num, //which BRAT used to overwrite RAT

	  input [`PRF_SIZE-1:0]				valid_list,
	  input 					LSQ_not_erase_PRF,
	  input [1:0]                                   BRAT_CMT_v,
	  input [1:0] [`BRAT_SIZE_LOG-1:0]		BRAT_CMT_num,
	  
	  // Inputs from MEM ALU
	  input [1:0] 					ALU_LSQ_valid,
	  input [1:0] [63:0]				ALU_LSQ_NPC,
	  input [1:0] [31:0]				ALU_LSQ_IR,
	  input [1:0] 					ALU_LSQ_ld_or_st,  // 0 for ld, 1 for st
	  input [1:0] [63:0]				ALU_LSQ_MEM_addr,
	  input [1:0] [`PRF_SIZE_LOG-1:0]		ALU_LSQ_regA,
	  input [1:0] [`ROB_SIZE_LOG-1:0]		ALU_LSQ_ROB,
	  input [1:0] [`BRAT_SIZE_LOG:0]		ALU_LSQ_BRAT,
	  input logic  [1:0] [`SQ_SIZE_LOG-1:0]		ALU_LSQ_SQ_addr,
	  
	  // Inputs from ROB
	  input [`ROB_SIZE_LOG-1:0]			ROB_head,
	  input 					ROB_head_is_store,


	  // From Front End: Dispatch
	   input logic [1:0] 					instr_is_ld,
   	   input logic  [1:0]	 				instr_is_st, //make sure it's valid and store
	   input logic  [1:0] [`PRF_SIZE_LOG-1:0] 		regA,
   	   input logic  [1:0] [`BRAT_SIZE_LOG:0] 		BRAT,
   	   input logic  [1:0] [63:0] 				NPC,
   	   input logic  [1:0] [31:0] 				IR,
   	   input logic  [`ROB_SIZE_LOG-1:0] 			ROB_tail,


	input logic [1:0]      	FE_ID_valid_inst,    // is inst a valid instruction to be  00 01 11
	input [1:0]        			FE_ID_wr_mem,        // does inst write memory?
	input logic branch1, branch2,
	input logic [`BRAT_NUM_LOG-1:0]			BRAT_tail,



	  // Output to ROB: Issue
	  output logic 						SQ_CMP,

	//input to LSQ
	   input 	logic					MSHR_full,//??? not use head/tail, but just count???
	   input  logic					LD_BUF_full,
	   
	//output to Dcache Controller
	   output logic 			LSQ_DC_rd_mem,
	   output logic   			LSQ_DC_wr_mem,
	   output logic 	 [63:0]		LSQ_DC_addr,
	   output logic [`PRF_SIZE_LOG-1:0]	LSQ_DC_PRF_num,
	   output logic 	 [`BRAT_SIZE_LOG:0]	LSQ_DC_BRAT_num,
	   output logic 	 [`ROB_SIZE_LOG-1:0]	LSQ_DC_ROB_num,
	   output logic [`PRF_SIZE_LOG-1:0] 	LSQ_DC_regA,
	   output logic [63:0]			LSQ_DC_NPC,
	   output logic [31:0]			LSQ_DC_IR,


	  // Outputs for PRF_after
	  output 					LQ_PRF_valid,
	  output [63:0]					LQ_PRF_NPC,
	  output [31:0]					LQ_PRF_IR,
	  output [`ROB_SIZE_LOG-1:0]			LQ_PRF_ROB,
	  output [`BRAT_SIZE_LOG:0]			LQ_PRF_BRAT,
	  output [`PRF_SIZE_LOG-1:0]			LQ_PRF_forward_PRF,
	  output [`PRF_SIZE_LOG-1:0]			LQ_PRF_regA,

	  // Outputs for PRF_pipeline_before
	  output [1:0]					LQ_full,
	  // Outputs to FrontEnd
	  output logic  [1:0]				SQ_full,
	  output logic  [`SQ_SIZE_LOG-1:0]	        sq_tail,

	   input clock,
	   input reset
	   );

	// Inputs from SQ
	  wire [`SQ_SIZE-1:0]				sq_valid;
	  wire [`SQ_SIZE-1:0]				sq_addr_valid;
	  wire [`SQ_SIZE-1:0] [63:0]			sq_MEM_addr;
	  wire [`SQ_SIZE-1:0] [`PRF_SIZE_LOG-1:0] 	sq_regA;
	  wire [`SQ_SIZE-1:0] [`ROB_SIZE_LOG-1:0]	sq_ROB;
	  wire [`SQ_SIZE_LOG-1:0]			sq_head;
	
	  wire						LQ_not_erase_Dcache;
	  wire						SQ_not_erase_Dcache;

  	  wire [`BRAT_NUM_LOG-1:0] BRAT_tail_plus1 = BRAT_tail + 1'b1;
	  //Early Branch Resolution
   logic [`BRAT_NUM-1:0] [`SQ_SIZE_LOG-1:0] Bsq_tail; //From Front End, on a misprediction, replace current tail

	  // Outputs for Dcache
	  logic 					LQ_Dcache_valid;
	  logic [`ROB_SIZE_LOG-1:0]			LQ_Dcache_ROB;
	  logic [`BRAT_SIZE_LOG:0]			LQ_Dcache_BRAT;
	  logic [63:0]					LQ_Dcache_MEM_addr;
	  logic [`PRF_SIZE_LOG-1:0]			LQ_Dcache_regA;

	  // Outputs to Dcache
	  logic  					SQ_Dcache_valid;
	  logic		  [`ROB_SIZE_LOG-1:0]		SQ_Dcache_ROB;
	  logic		  [`BRAT_SIZE_LOG:0]		SQ_Dcache_BRAT;
	  logic   	  [63:0]			SQ_Dcache_MEM_addr;
	  logic 	  [`PRF_SIZE_LOG-1:0]		SQ_Dcache_regA;

	  //Useless wire
	  logic [63:0]					LQ_Dcache_NPC;
	  logic [31:0]					LQ_Dcache_IR;
	  logic [31:0]					SQ_Dcache_IR;
	  logic [63:0]					SQ_Dcache_NPC;

   assign LSQ_DC_rd_mem = LQ_Dcache_valid;
   assign LSQ_DC_wr_mem = SQ_Dcache_valid;
   assign LSQ_DC_NPC = SQ_Dcache_valid? SQ_Dcache_NPC : LQ_Dcache_NPC;
   assign LSQ_DC_IR = SQ_Dcache_valid? SQ_Dcache_IR : LQ_Dcache_IR;
   
   assign LSQ_DC_PRF_num = LQ_Dcache_regA;
   assign LSQ_DC_addr = SQ_Dcache_valid? SQ_Dcache_MEM_addr : LQ_Dcache_MEM_addr;
   assign LSQ_DC_BRAT_num = SQ_Dcache_valid?  SQ_Dcache_BRAT : LQ_Dcache_BRAT;
   assign LSQ_DC_ROB_num = SQ_Dcache_valid? SQ_Dcache_ROB : LQ_Dcache_ROB;
   assign LSQ_DC_regA = SQ_Dcache_regA;
   


   assign LQ_not_erase_Dcache = MSHR_full&LD_BUF_full;
   assign SQ_not_erase_Dcache = MSHR_full;

   wire [1:0] [`ROB_SIZE_LOG-1:0] ROB;
   assign ROB[0] = FE_ID_valid_inst[0] ? ROB_tail : 0;
   assign ROB[1] = FE_ID_valid_inst[1] ? ROB_tail + 1'b1 : 0;


lq lq1(
	// Control Signals
	.branch_mis(branch_mis),
	.branch_mis_brat(branch_mis_brat),
	.valid_list(valid_list),
	.LSQ_not_erase_PRF(LSQ_not_erase_PRF),
	.LSQ_not_erase_Dcache(LQ_not_erase_Dcache),
	.BRAT_CMT_v(BRAT_CMT_v),
	.BRAT_CMT_num(BRAT_CMT_num),

       // Inputs from FE
       .instr_is_ld(instr_is_ld), //make sure it's valid and store
       .NPC(NPC),
       .IR(IR),
       .regA(regA),
       .ROB(ROB),
       .BRAT(BRAT),

	// Inputs from MEM ALU
	.ALU_LSQ_valid(ALU_LSQ_valid),
	.ALU_LSQ_NPC(ALU_LSQ_NPC),
	.ALU_LSQ_IR(ALU_LSQ_IR),
	.ALU_LSQ_ld_or_st(ALU_LSQ_ld_or_st),  // 0 for ld, 1 for st
	.ALU_LSQ_MEM_addr(ALU_LSQ_MEM_addr),
	.ALU_LSQ_regA(ALU_LSQ_regA),
	.ALU_LSQ_ROB(ALU_LSQ_ROB),
	.ALU_LSQ_BRAT(ALU_LSQ_BRAT),

	// Inputs from SQ
	.sq_valid(sq_valid),
	.sq_addr_valid(sq_addr_valid),
	.sq_MEM_addr(sq_MEM_addr),
	.sq_regA(sq_regA),
	.sq_ROB(sq_ROB),
	.sq_head(sq_head),
	.sq_tail(sq_tail),

	// Inputs from ROB
	.ROB_head(ROB_head),
	.ROB_head_is_store({ROB_head_is_store, 1'b0}),

	////////////////
	// Outputs
	////////////////
	// Outputs for PRF_after
	.LQ_PRF_valid(LQ_PRF_valid),
	.LQ_PRF_NPC(LQ_PRF_NPC),
	.LQ_PRF_IR(LQ_PRF_IR),
	.LQ_PRF_ROB(LQ_PRF_ROB),
	.LQ_PRF_BRAT(LQ_PRF_BRAT),
	.LQ_PRF_forward_PRF(LQ_PRF_forward_PRF),
	.LQ_PRF_regA(LQ_PRF_regA),

	// Outputs for Dcache
	.LQ_Dcache_valid(LQ_Dcache_valid),
	.LQ_Dcache_NPC(LQ_Dcache_NPC),
	.LQ_Dcache_IR(LQ_Dcache_IR),
	.LQ_Dcache_ROB(LQ_Dcache_ROB),
	.LQ_Dcache_BRAT(LQ_Dcache_BRAT),
	.LQ_Dcache_MEM_addr(LQ_Dcache_MEM_addr),
	.LQ_Dcache_regA(LQ_Dcache_regA),

	// Outputs for PRF_pipeline_before
	.LQ_full(LQ_full),

	.clock(clock),
	.reset(reset)
);


sq sq1(
	// From Front End: Dispatch
	.instr_is_st(instr_is_st), //make sure it's valid and store
	.regB(regA),
	.ROB(ROB),
	.BRAT(BRAT),
	.NPC(NPC),
	.IR(IR),

	//Early Branch Resolution
	.Bsq_tail(Bsq_tail[BRAT_CMP_num]), //From Front End, on a misprediction, replace current tail


	// From BOB BRAT: Mispredict + dispatch + BRAT_retire
	.branch_mis(branch_mis),
	.branch_mis_brat(branch_mis_brat),
	.BRAT_CMT_v(BRAT_CMT_v),
	.BRAT_CMT_num(BRAT_CMT_num),
		  

	//Inputs from MEM ALU: ALU
	.ALU_LSQ_ld_or_st({(ALU_LSQ_ld_or_st[1]&ALU_LSQ_valid[1]),(ALU_LSQ_ld_or_st[0]&ALU_LSQ_valid[0])}),  // 0 for ld, 1 for st
	.ALU_LSQ_SQ_addr(ALU_LSQ_SQ_addr),
	.ALU_LSQ_MEM_addr(ALU_LSQ_MEM_addr),


	// From Dcache: decache accept the store issue or not: Issue
	.LSQ_not_erase_Dcache(SQ_not_erase_Dcache),
	// Inputs from ROB: Issue
	.ROB_head_is_store(ROB_head_is_store),
	.ROB_head(ROB_head),
	// Output to ROB: Issue
	.SQ_CMP(SQ_CMP),

	// Outputs to LQ
	.sq_valid(sq_valid),
	.sq_addr_valid(sq_addr_valid),
	.sq_MEM_addr(sq_MEM_addr),
	.sq_regB(sq_regA), // REGA ? REGB
	.sq_ROB(sq_ROB),
	.sq_head(sq_head),
	.sq_tail(sq_tail),


	// Outputs to Dcache
	.SQ_Dcache_valid(SQ_Dcache_valid),
	.SQ_Dcache_NPC(SQ_Dcache_NPC),
	.SQ_Dcache_IR(SQ_Dcache_IR),
	.SQ_Dcache_ROB(SQ_Dcache_ROB),
	.SQ_Dcache_BRAT(SQ_Dcache_BRAT),
	.SQ_Dcache_MEM_addr(SQ_Dcache_MEM_addr),
	.SQ_Dcache_regB(SQ_Dcache_regA),

	// Outputs to FrontEnd
	.SQ_full(SQ_full),

	.clock(clock),
	.reset(reset)
);


	always_ff @(posedge clock)
	begin
		if(reset)
			begin
				for(integer BRAT_j=0; BRAT_j<`BRAT_NUM; BRAT_j=BRAT_j+1)
					begin
						Bsq_tail[BRAT_j] <= `SD 0;
					end//for(BRAT_j)
			end//if(reset)

		else
			case({branch2,branch1})
			2'b11:
				begin
					Bsq_tail[BRAT_tail] 	<= `SD sq_tail;
					Bsq_tail[BRAT_tail_plus1] 	<= `SD sq_tail;
				end

			2'b01:
				begin
					Bsq_tail[BRAT_tail] 	<= `SD sq_tail;
				end

			2'b10:
				begin
					if(FE_ID_wr_mem[0]&FE_ID_valid_inst[0])
						Bsq_tail[BRAT_tail] 	<= `SD sq_tail+1'b1;
					else
						Bsq_tail[BRAT_tail] 	<= `SD sq_tail;
				end
			endcase
	end


endmodule
