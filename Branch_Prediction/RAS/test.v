`define RAS_SIZE 4
`define RAS_LOG2 2
`define RAS_LOG2_PLUS_ONE `RAS_LOG2+1

module test;

   reg 				clock;
   reg 				reset;
   reg 	      			pop;
   reg 	      			push;
   reg [63:0] 				push_PC;
   reg [1:0]	      				BRAS_cp;
   reg 	      				flush;
   reg [1:0][`RAS_SIZE:0][63:0] 		BRAS;
   reg [1:0] [`RAS_LOG2:0] 			BRAS_head;
   wire 				empty;
   wire [63:0]				predicted_PC;
   wire [1:0] [`RAS_SIZE:0][63:0] 	BRAS_in;
   wire [1:0] [`RAS_LOG2:0] 		BRAS_head_in;
   
   always
     begin
	#5 clock = ~clock;
     end

   RAS RAS(clock,reset,pop,push,push_PC,flush,BRAS,BRAS_head,empty,predicted_PC,BRAS_in[1],BRAS_head_in[1]);
   
   genvar i;
   generate for(i=0;i<2;i++)
     begin
	always@(posedge clock)
	  begin
	     if(reset)
	       begin
		  BRAS[i] <= `SD 0;
		  BRAS_head[i] <= `SD `RAS_LOG2_PLUS_ONE'hFFFF_FFFF;
	       end
	     else if(BRAS_cp[i])
	       begin
		  BRAS[i] <= `SD BRAS_in;
		  BRAS_head[i] <= `SD BRAS_head_in;
	       end
	  end
     end // for (i=0;i<2;i++)
   endgenerate
	
   initial
     begin
	clock=0;
	reset=1;
	pop=0;
	push=0;
	push_PC=0;
	BRAS_cp=0;
	flush=0;

	@(negedge clock);
	reset = 0;
	@(negedge clock);
	push = 1;
	push_PC = 64'h4;
	@(negedge clock);
	push_PC = 64'h8;
	@(negedge clock);
	push_PC = 64'hC;	
	@(negedge clock);
	push_PC = 64'h10;
	@(negedge clock);
	push_PC = 64'h14;
	@(negedge clock);
	push_PC = 64'h18;
	BRAS_cp = 1;
	@(negedge clock);
	pop=1;
	push_PC = 64'h1C;
	BRAS_cp = 0;
	@(negedge clock);
	push=0;
	@(negedge clock);
	@(negedge clock);
	@(negedge clock);
	@(negedge clock);
	BRAS_cp = 3;
	@(negedge clock);
	push=1;
	BRAS_cp = 0;
	push_PC = 64'h20;
	@(negedge clock);
	push_PC = 64'h24;
	@(negedge clock);
	push_PC = 64'h28;
	@(negedge clock);
	flush = 1;
	pop=0;
	push=0;
	@(negedge clock);
	pop=1;
	$finish;
	
	
     end // initial begin
endmodule