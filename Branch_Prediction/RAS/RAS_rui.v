//Return Address Stack:
//	speculatively store the register value of $r26 
//	(cause it take a while for the unconditional br to store the $r26 value)
//	So what it does is just a Jump to return forwarding
//	Before jump complete in RS and return address
//However, this value should be correct when the early branch resolution is implemented
//
//	allocate entry when jump occur and  

//Request: Front END must make sure in one cycle only on JUMP or RET will be executed

`define RAS_SIZE 4
`define RAS_LOG2 2
`define RAS_LOG2_PLUS_ONE 3

module RAS(
	   ////////////
	   // Inputs
	   ////////////
	   input  				clock,
	   input 				reset,
	   
	   input  				pop,

	   
	   input  				push,
	   input [63:0] 			push_PC,

	   input  				flush, 
	   input [`RAS_SIZE:0] [63:0] 		BRAS_ld_reg,
	   input [`RAS_LOG2:0]                  BRAS_ld_head,
	   
	   
	   output logic 			empty,
	   output logic [63:0] 			predicted_PC,

	   output [1:0] [`RAS_SIZE:0] [63:0] 	BRAS_cp_reg,
	   output [1:0] [`RAS_LOG2:0]   	BRAS_cp_head
	   );

   reg [`RAS_SIZE-1:0] [63:0] 			registers;
   reg [`RAS_LOG2:0] 				stack_head; //stack_head are all 1s -- it is empty: -1
   
   logic [`RAS_SIZE-1:0] [63:0] 		next_registers;
   logic [`RAS_LOG2:0] 				next_stack_head;
   
   
   assign empty=(stack_head==~(`RAS_LOG2_PLUS_ONE'h0));
   assign BRAS_cp_head = {stack_head,next_stack_head};
   assign BRAS_cp_reg = {registers,next_registers};
   always_comb
     begin
	next_registers = registers;
	next_stack_head = stack_head;
	predicted_PC = 0;
	
	if(pop & ~empty)//if stack is not empty
	  begin
	     next_stack_head = stack_head - 1; 
	     predicted_PC = registers[stack_head];
	     next_registers[stack_head] = 0;
	  end
	
	if(push && ((stack_head[`RAS_LOG2] != 0) || (stack_head[`RAS_LOG2-1:0] != ~(`RAS_LOG2'h0))) )//if the stack is not full
	  begin
	     next_stack_head = stack_head + 1; 
	     next_registers[next_stack_head] = push_PC;
	  end
	
	if(push && (stack_head[`RAS_LOG2] == 0) && (stack_head[`RAS_LOG2-1:0] ==  ~(`RAS_LOG2'h0)) )//if the stack is full
	  begin
	     next_registers[`RAS_SIZE-2:0] =  registers[`RAS_SIZE-1:1];
	     next_registers[`RAS_SIZE-1] = push_PC;
	  end
	
	if(flush)
	  begin
	     next_registers = BRAS_ld_reg;
	     next_stack_head = BRAS_ld_head;
	  end
     end
   
   
   
   always_ff@(posedge clock)
     begin
	if(reset)
	  begin	
	     registers <= `SD 0;
	     stack_head <= `SD ~(`RAS_LOG2_PLUS_ONE'b000000);
	  end
	else
	  begin
	     registers <= `SD next_registers;
	     stack_head <= `SD next_stack_head;
	  end
     end
   
   
endmodule
