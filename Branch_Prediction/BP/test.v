module test;

   reg clock;
   reg reset;
   reg [1:0][63:0] FE_PC;
		  
   reg [1:0][63:0] comPC;
   reg [1:0] 	   comtaken;
   reg [1:0] 	   FE_taken_old;
   reg [1:0] 	   FE_taken_old_old;

   reg [1:0]       comwr_en;

   wire [1:0]      FE_taken;


   
   genvar 	    i;
   
   Branch_Predictor Branch_Predictor(.*);

   initial
     begin
	clock=0;
	reset=1;
	FE_PC=0;
	comPC=0;
	comtaken=0;
	comwr_en=0;
	@(negedge clock);
	reset=0;
	@(negedge clock);
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000004, 64'h0000000000000000};
	FE_taken_old <= #5 FE_taken;
	@(posedge clock);
	comwr_en <= #3 2'b11;
	comtaken <= #3 2'b01;
	comPC <= #3 {64'h0000000000000004, 64'h0000000000000000};
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old <= #5 FE_taken;
	@(posedge clock);
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old <= #5 FE_taken;
	@(posedge clock);
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old <= #5 FE_taken;
	@(posedge clock);
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old <= #5 FE_taken;
	@(posedge clock);
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old <= #5 FE_taken;
	@(posedge clock);
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old <= #5 FE_taken;
	@(posedge clock);
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old <= #5 FE_taken;
	@(posedge clock);
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old <= #5 FE_taken;
	@(posedge clock);
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old <= #5 FE_taken;
	@(posedge clock);
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old <= #5 FE_taken;

	$display("--------------------------");
	
	
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old[0]  <= #5 FE_taken[0];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comtaken <= #3 comtaken;
	comwr_en <= #3 2'b00;
	FE_taken_old[1]  <= #5 FE_taken[1];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comwr_en <= #3 2'b11;
	comPC <= #3 {64'h0000000000000008, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old[0]  <= #5 FE_taken[0];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	comwr_en <= #3 2'b00;
	FE_taken_old[1]  <= #5 FE_taken[1];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comwr_en <= #3 2'b11;
	comPC <= #3 {64'h0000000000000008, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old[0]  <= #5 FE_taken[0];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	comwr_en <= #3 2'b00;
	FE_taken_old[1]  <= #5 FE_taken[1];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comwr_en <= #3 2'b11;
	comPC <= #3 {64'h0000000000000008, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old[0]  <= #5 FE_taken[0];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	comwr_en <= #3 2'b00;
	FE_taken_old[1]  <= #5 FE_taken[1];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comwr_en <= #3 2'b11;
	comPC <= #3 {64'h0000000000000008, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old[0]  <= #5 FE_taken[0];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	comwr_en <= #3 2'b00;
	FE_taken_old[1]  <= #5 FE_taken[1];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comwr_en <= #3 2'b11;
	comPC <= #3 {64'h0000000000000008, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old[0]  <= #5 FE_taken[0];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	comwr_en <= #3 2'b00;
	FE_taken_old[1]  <= #5 FE_taken[1];
	@(posedge clock);
	FE_PC <= #3 {64'h0000000000000000, 64'h0000000000000008};
	comwr_en <= #3 2'b11;
	comPC <= #3 {64'h0000000000000008, 64'h0000000000000008};
	comtaken <= #3 ~comtaken;
	#4 $display ("%b   %b", FE_taken_old, comtaken);
	FE_taken_old[0]  <= #5 FE_taken[0];

	
	$finish;
     end

   always
     begin
	#20 clock = ~clock;
     end

endmodule