`define testing 32

module test;

   reg clock;
   reg reset;
   reg [1:0][63:0] rd_branch_pc;
		  
   reg [1:0][63:0] wr_branch_pc;
   reg [1:0] 	   wr_branch_en;
   reg [1:0][63:0] wr_branch_target;

   wire [1:0][31:0] rd_branch_target_out;
   wire [1:0] 	    rd_branch_valid_out;
   reg 	[30:0]	    aaa;
   wor 		    test;
   
   genvar 	    i;
   
   BTB_dm_64 BTB(clock,reset,rd_branch_pc,wr_branch_pc,wr_branch_en,wr_branch_target,rd_branch_target_out,rd_branch_valid_out);

   initial
     begin
	aaa = 6;
	
	clock=0;
	reset=1;
	rd_branch_pc=0;
	wr_branch_pc=0;
	wr_branch_en=0;
	wr_branch_target=0;
	
	@(negedge clock);
	reset=0;
	@(negedge clock);
	wr_branch_en={1'b1,1'b1};
	wr_branch_pc={64'h1234567890ABCDEF,64'hFEDCBA0987654321};
	wr_branch_target={64'h4,64'h8};
	@(negedge clock);
	rd_branch_pc={64'h1234567890ABCDED,64'hFEDCBA0987654322};
	wr_branch_en={1'b1,1'b1};
	wr_branch_pc={64'h22222222222222EF,64'h4444444444444421};
	wr_branch_target={64'hB,64'hF};
	@(negedge clock);
	rd_branch_pc={64'h1234567890ABCDED,64'h22222222222222EF};
	@(negedge clock);
	$finish;
     end

   always
     begin
	#5 clock = ~clock;
     end

   
   
   generate for(i=3;i<4;i++)
     begin
	assign test = (aaa[3:1] == i)? 1:0;
	always@(posedge clock)
	  begin
	    $display("%d",test);
	  end
     end
   endgenerate
endmodule