//Function: A Ring buffer: Save all the free PRF in RAT
//Modifed by: Jingcheng Wang

//When modify Free list:
//		the case only change the head and tail
//		rename: if RAT assert rename request and give the number of reg needed, 
//						Free list will pop out the number of free PRF (max num 2 support 2-way)
//		flush: ROB flush everything, RAT free list gets the copy of RRAT free list--Not the case
//						Upon a flush, the only thing changed is the head of the FRL. The reg content is the same.
//		No need to have RRAT Free List At All!!!		
//
//    The only case that the reg content will be changed:
//		reset: all 31, 32, 33, 34, ... ,62
//		commit: RRAT send the freed PRF, Free list insert it to the buffer. (max num 2)


//Corner case: 2-way commit: free prf and flush can be asserted at the same time!!!
//Corner case: rename request and flush request happen at the same cycle: Nothing need to be done!!
//							just remember to set the two fetched instr to invalid is Enough

`define FREE_LIST_IDX 5 //32 free list entry now. so 2^FREE_LIST_IDX=32
`define BRAT_NUM_LOG 2
`define BRAT_NUM 4

module FreeRegList(
	//Control signal
	input flush,
	output logic [1:0] FRL_empty, // FRL_structure hazard: 1x: empty; 01: has 1 avail; 00: over 2 avail;

	//Data signal
	//****Don't need this information!!!
	//input logic [31:0] [`PRF_IDX-1:0] RRAT_FRL,

	//Data input: insert freed prf from commit
	input logic free_v1, free_v2,
	input logic [`PRF_IDX-1:0] freed_prf1,freed_prf2,

	//Data output: give free prf
	input logic rename1, rename2,
	output logic rename_v1, rename_v2,
	output logic [`PRF_IDX-1:0] prf1,prf2,


////////////////////////////
//Early Branch Resolution///
////////////////////////////
	//on dispatch a branch, allocate a FRL
	input logic [`BRAT_NUM_LOG-1:0]			BRAT_tail,
	input logic branch1, branch2, //if first instr want to copy BRAT: branch1 will be 1

	//on find a mispredict, Overwrite FRL with BFRL
	input logic [`BRAT_NUM_LOG-1:0] BRAT_CMP_num, //which BRAT used to overwrite RAT
	input  logic										BRAT_CMP_miss,

	//To Valid Register List of PRF
	output [63:0]  valid_list;

	input logic clock, reset
);

	





	//Registers
	logic [31:0] [`PRF_IDX-1:0] FRL_registers;//a ring buffer
	logic [`FREE_LIST_IDX:0] free_n;//record the number of free PRF in FRL;
	logic [`FREE_LIST_IDX-1:0] head, tail;//record the head and tail of the ring buffer
	//Next state combinational signal	
	logic [31:0] [`PRF_IDX-1:0] next_FRL_registers;
	logic [`FREE_LIST_IDX:0] next_free_n, next_free_n2;//record the number of free PRF in FRL;
	logic [`FREE_LIST_IDX-1:0] next_head, next_tail;//record the head and tail of the ring buffer





/////////////	//////////////////////////////// /////////////
/////////////	//[For Early Branch Resolution]/ /////////////
/////////////	//////////////////////////////// /////////////
	//on a branch if BRAT available, then send a copy of RAT(after rename) to BRAT (if two branch, send twice)
	//Data Structure 
	//For Early Branch Resolution
	//ONLY NEED to have BHEAD and BFREE_N!!!!
	//reg [`BRAT_NUM_LOG-1:0] [31:0] [`PRF_IDX-1:0] BFRL_registers;//a ring buffer
	reg [`BRAT_NUM_LOG-1:0] [`FREE_LIST_IDX-1:0] Bhead;//, Btail;//record the head and tail of the ring buffer
	reg [`BRAT_NUM_LOG-1:0] [`FREE_LIST_IDX:0] Bfree_n;//record the number of free PRF in FRL;

	//logic [`BRAT_NUM_LOG-1:0] [31:0] [`PRF_IDX-1:0] next_BFRL_registers;//a ring buffer
	logic [`BRAT_NUM_LOG-1:0] [`FREE_LIST_IDX-1:0] next_Bhead;//, next_Btail;//record the head and tail of the ring buffer
	logic [`BRAT_NUM_LOG-1:0] [`FREE_LIST_IDX:0] next_Bfree_n, next_Bfree_n2;//record the number of free PRF in FRL;

	wire [`BRAT_NUM_LOG-1:0] BRAT_tail_plus1 = BRAT_tail+1;



//UPON a Branch Dispatch, Allocate BFRL
//However, if the Branch will change register, the change must be also copied

//First Must let correct retirement (freed register) appear on all the BFRL
//Then only allocate the register that when meet a branch
	always_comb
		begin
			//next_BFRL_registers = BFRL_registers;
			//next_Btail=Btail;
			next_Bhead=Bhead;
			next_Bfree_n=Bfree_n;
			next_Bfree_n2=Bfree_n;

//First Must let correct retirement (freed register) appear on all the BFRL
			if(free_v1 && free_v2)
				begin
					for(integer i=0; i<`BRAT_NUM; i=i+1)
						begin
							//next_BFRL_registers[i][Btail[i]]=freed_prf1;
							//next_FRL_registers[i][Btail[i]+1'b1]=freed_prf2;
							//next_Btail[i] = Btail[i] + 2'h2;
							next_Bfree_n2[i] = Bfree[i] + 2'h2;
						end
				end//if(free_v1 && free_v2)
			else if(free_v1 || free_v2)
						begin
							for(integer i=0; i<`BRAT_NUM; i=i+1)
								begin
									//next_BFRL_registers[i][Btail[i]]=freed_prf1;
									//next_Btail[i] = Btail[i] + 1'b1;
									next_Bfree_n2[i] = Bfree[i] + 1'b1;
									//Never: next_free_n = next_free_n + 1; Infinite Loop
								end
						end
							
			/*else if(free_v2)
						begin
							//next_BFRL_registers[i][Btail[i]]=freed_prf2;
							//next_Btail[i] = Btail[i] + 1'b1;
							next_Bfree_n[i] = Bfree[i] + 1'b1;
						end*/

//Then only allocate the register that when meet a branch, 
//but must first free and allocate
			next_Bfree_n[i] = next_Bfree_n2[i];
//USE CASE is very good for Case covery!!!
			if(branch1 && branch2)
					begin
						case({rename1,rename2})
							begin
								2'b11://if(rename1&&rename2)
									begin
										next_Bhead[BRAT_tail] = head + 1'b1;
										next_Bfree_n[BRAT_tail] = next_free_n2 - 1'b1;
										next_Bhead[BRAT_tail_plus1] = head + 2'h2;
										next_Bfree_n[BRAT_tail_plus1] = next_free_n2 - 2'h2;
									end
								2'b10://if(rename1)
									begin
										next_Bhead[BRAT_tail] = head + 1'b1;
										next_Bfree_n[BRAT_tail] = next_free_n2 - 1'b1;
										next_Bhead[BRAT_tail_plus1] = head + 1'b1;
										next_Bfree_n[BRAT_tail_plus1] = next_free_n2 - 1'b1;
									end
								2'b01://if(rename2)
									begin
										next_Bhead[BRAT_tail] = head;
										next_Bfree_n[BRAT_tail] = next_free_n2;
										next_Bhead[BRAT_tail_plus1] = head + 1'b1;
										next_Bfree_n[BRAT_tail_plus1] = next_free_n2 - 1'b1;
									end		
								2'b00:
									begin
										next_Bhead[BRAT_tail] = head;
										next_Bfree_n[BRAT_tail] = next_free_n2;
										next_Bhead[BRAT_tail_plus1] = head;
										next_Bfree_n[BRAT_tail_plus1] = next_free_n2;
									end
						endcase
					end//if(branch1 && branch2)
			else if(branch1)
					begin
						if(rename1)
							begin
								next_Bhead[BRAT_tail] = head + 1'b1;
								next_Bfree_n[BRAT_tail] = next_free_n2 - 1'b1;
							end
					end//if(branch1)
			else if(branch2)
					begin
						case({rename1,rename2})
							begin
								2'b11://if(rename1&&rename2)
									begin
										next_Bhead[BRAT_tail] = head + 2'h2;
										next_Bfree_n[BRAT_tail] = next_free_n2 - 2'h2;
								end
								2'b10,2'b01://if(rename1)
									begin
										next_Bhead[BRAT_tail] = head + 1'b1;
										next_Bfree_n[BRAT_tail] = next_free_n2 - 1'b1;
									end
								2'b00:
									begin
										next_Bhead[BRAT_tail] = head;
										next_Bfree_n[BRAT_tail] = next_free_n2;
									end
						endcase
					end//if(branch2)


	always_ff
	begin
		if(reset)
			begin
				for(integer BRAT_j=0; BRAT_j<BRAT_NUM; BRAT_j=BRAT_j+1)
					begin
						Bfree_n[BRAT_j]		<= `SD 32;//reset 32 free registers
						Bhead[BRAT_j]			<= `SD 0;
					end//for(BRAT_j)
			end//if(reset)

//Cause all the BFRL needed to be update per cycle, no matter whether we copy allocate the new BFRL or not
//So would better to have BFRL different from BRAT
		else 
			begin
				Bfree_n		<= `SD next_Bfree_n;//reset 32 free registers
				Bhead			<= `SD next_Bhead;
			end
	end
/////////////	//////////////////////////////// /////////////
/////////////	//[For Early Branch Resolution]/ /////////////
/////////////	//////////////////////////////// /////////////
//END




	





	assign FRL_empty[1] = (free_n==0);
	assign FRL_empty[0] = (free_n==1);



	always_comb
		begin
			rename_v1=0;
			rename_v2=0;
			prf1=0;
			prf2=0;

			next_free_n=free_n;
			next_head=head;
			next_tail=tail;
			next_FRL_registers=FRL_registers;


			if(free_v1 && free_v2)
				begin
					next_FRL_registers[tail]=freed_prf1;
					next_FRL_registers[tail+1]=freed_prf2;
					next_tail = tail + 2;
					next_free_n2 = next_free + 2;
				end//if(free_v1 && free_v2)
			else if(free_v1)
						begin
							next_FRL_registers[tail]=freed_prf1;
							next_tail = tail + 1;
							next_free_n2 = next_free + 1;
							//Never: next_free_n = next_free_n + 1; Infinite Loop
						end
			else if(free_v2)
						begin
							next_FRL_registers[tail]=freed_prf2;
							next_tail = tail + 1;
							next_free_n2 = next_free + 1;
						end

			//Highest priority: flush
			if(flush)
				begin
					//next_FRL = RRAT_FRL; //if flush copy the RRAT free list
															//RRAT FRL only has tail and it is the same as that in RAT
					next_free_n = 32;
					//if @ the same time a prf is freed					
					//if(free && (free_n==2'b1))
					next_head = next_tail;
				end//if(flush)

			//No Flush happen in this cycle: rename and free reg as usual
			else
				begin
					if(rename1 || rename2)
						begin
							if(rename1 && rename2)
								begin
									rename_v1=1;
									rename_v2=1;
									prf1=FRL_registers[head];
									prf2=FRL_registers[head+1];
									next_head = head + 2;
									next_free_n = next_free_n2 - 2;									
								end
							else if(rename1)
								begin
									rename_v1=1;
									rename_v2=0;
									prf1=FRL_registers[head];
									prf2=0;
									next_head = head + 1;
									next_free_n = next_free_n2 - 1;
								end
							else if(rename2)
								begin
									rename_v1=0;
									rename_v2=1;
									prf1=0;
									prf2=FRL_registers[head];
									next_head = head + 1;
									next_free_n = next_free_n2 - 1;
								end		
						end//if(rename1 || rename2)
				end//else
		end//always_comb







	always_ff @(posedge clock)
		begin
			if(reset)
				begin
					FRL_registers[0] 		<= `SD 31;
					FRL_registers[1] 		<= `SD 32;
					FRL_registers[2] 		<= `SD 33;FFFF_FFFF
					FRL_registers[3] 		<= `SD 34;
					FRL_registers[4] 		<= `SD 35;
					FRL_registers[5] 		<= `SD 36;
					FRL_registers[6] 		<= `SD 37;
					FRL_registers[7] 		<= `SD 38;
					FRL_registers[8] 		<= `SD 39;
					FRL_registers[9] 		<= `SD 40;
					FRL_registers[10] 	<= `SD 41;
					FRL_registers[11] 	<= `SD 42;
					FRL_registers[12] 	<= `SD 43;
					FRL_registers[13] 	<= `SD 44;
					FRL_registers[14] 	<= `SD 45;
					FRL_registers[15] 	<= `SD 46;
					FRL_registers[16] 	<= `SD 47;
					FRL_registers[17] 	<= `SD 48;
					FRL_registers[18] 	<= `SD 49;
					FRL_registers[19] 	<= `SD 50;
					FRL_registers[20] 	<= `SD 51;
					FRL_registers[21] 	<= `SD 52;
					FRL_registers[22] 	<= `SD 53;
					FRL_registers[23] 	<= `SD 54;
					FRL_registers[24] 	<= `SD 55;
					FRL_registers[25] 	<= `SD 56;
					FRL_registers[26] 	<= `SD 57;
					FRL_registers[27] 	<= `SD 58;
					FRL_registers[28] 	<= `SD 59;
					FRL_registers[29] 	<= `SD 60;
					FRL_registers[30] 	<= `SD 61;
					FRL_registers[31] 	<= `SD 62;
					
					//Warning: All reg type need to be reset in always_ff--those are drives of combiniational signals
					free_n		<= `SD 32;//reset 32 free registers
					head			<= `SD 0;
					tail			<= `SD 0;//tail is always pointed to the first used unfreed slot
														////32 is another name of 0
				end

			else if(BRAT_CMP_miss)
			begin
				//EBR if mispredict: copy corresponding BRAT To RAT
				head			<= `SD next_Bhead[BRAT_CMP_num];
				free_n		<= `SD next_Bfree_n[BRAT_CMP_num];
			end

			//Assume in ROB instr that freed prf is ahead of mispredicted Branch
			//So give the Second priority to free prf request
			else
				begin
					//FRL				<= `SD RRAT_FRL;
					FRL_registers				<= `SD next_FRL_registers; //So that I can use sequential(blocking) assignment to next_FRL
					head			<= `SD next_head;
					tail			<= `SD next_tail;
					free_n		<= `SD next_free_n;
				end
		end//always_ff @(posedge clock)


		//1 not free, 0 free
		always_comb
		begin
			valid_list=64'hFFFF_FFFF_FFFF_FFFF;
			if(next_free_n==0)
			
			else if(next_head==next_tail)
			begin
				for(integer i=0; i<32; i=i+1)
				begin
					valid_list[next_FRL_registers[i]]=1'b0;
				end
			end

			else if(next_head<next_tail)
			begin
				for(integer i=0; i<32; i=i+1)
				begin
					if(i>=next_head && i<next_tail) valid_list[next_FRL_registers[i]]=1'b0;
				end
			end

			else if(next_head>next_tail)
			begin
				for(integer i=0; i<32; i=i+1)
				begin
					if((i>=next_head && i<=31) || (i>=0 && i<next_tail-1'b1)) valid_list[next_FRL_registers[i]]=1'b0;
				end
			end
		end

endmodule







