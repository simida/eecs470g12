module testbench;


				  reg  [1:0] [`PRF_SIZE_LOG-1:0] ready_idx;			// read/write index
				  reg  [1:0]  ready;						// write enable
				  reg  clock;							// clock
				  reg  reset;
				  reg  branch_mis;
			          reg  [`PRF_SIZE-1:0] RRAT_valid_list;
				  wire [`PRF_SIZE-1:0] valid_list;			// valid_list
				  wire write_conflict;					  	// for test use
        valid_list top(
				  ready_idx,			// read/write index
				  ready,						// write enable
				  clock,							// clock
				  reset,
				  branch_mis,
			          RRAT_valid_list,
				  valid_list,			// valid_list
				  write_conflict					  	// for test use
				  );
	


	initial 
	begin
		$finish;
 	end // initial
endmodule
