module testbench;


				  reg  [3:0][1:0] [`PRF_SIZE_LOG-1:0] rda_idx, rdb_idx;
				  reg  [1:0] [`PRF_SIZE_LOG-1:0] wr_idx;
				  reg  [1:0] [63:0] wr_data;					// write data
				  reg  [1:0]  wr_en;						// write enable
				  reg  clock;						// clock
				  wire [3:0][1:0] [63:0] rda_out, rdb_out;			// read data
				  wire write_conflict;				  	// for test use
	PRF top(
				  rda_idx, rdb_idx, wr_idx,	// read/write index
				  wr_data,					// write data
				  wr_en,						// write enable
				  clock,						// clock
				  rda_out, rdb_out,			// read data
				  write_conflict				  	// for test use
				  );
	


	initial 
	begin
		$finish;
 	end // initial
endmodule
