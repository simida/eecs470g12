/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  valid_list.v                                        //
//                                                                     //
//  Description :  This module creates the valid list of the           // 
//                  Physical Register used by the Pipeline.            //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

module valid_list(		  input  clock,							// clock
				  input  reset,
				  input  branch_mis,
				  input  [1:0] [`PRF_SIZE_LOG-1:0] ready_idx,			// read/write index
				  input  [1:0]  ready,						// write enable
			          input [`PRF_SIZE-1:0] RRAT_valid_list,
				  output logic [`PRF_SIZE-1:0] valid_list,			// valid_list
				  output write_conflict					  	// for test use
				  );
  
  logic    [`PRF_SIZE-1:0] registers;   	
  logic	   [`PRF_SIZE-1:0] next_registers;

  assign write_conflict = ready[0] & ready[1] & (ready_idx[0] == ready_idx[1]);


  //
  // Write port
  //
  always_comb begin
	next_registers = registers;
	if (reset)
		next_registers[31:0] = 32'hffff_ffff;
	else if (branch_mis)
		next_registers = RRAT_valid_list;
	else begin
		if (ready[0])
			next_registers[ready_idx[0]] = 1'b1;
		if (ready[1])
			next_registers[ready_idx[1]] = 1'b1;
	end
  end

 
  always_ff @(posedge clock) 
      registers <= `SD next_registers;

  //
  // valid_list
  //	
  always_comb begin
	valid_list = registers;
	valid_list[`PRF_ZERO_REG] = 1'b1;
	if (reset)
		valid_list[31:0] = 32'hffff_ffff;
	if (branch_mis)
		valid_list = RRAT_valid_list;
	else begin
		if (ready[0])
			valid_list[ready_idx[0]] = 1'b1;
		if (ready[1])
			valid_list[ready_idx[1]] = 1'b1;
	end
  end



endmodule // valid_list
