/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  Issue_Stage.v                                       //
//                                                                     //
//  Description :  This module is the Issue Stage (with ppl regs) of   // 
//                 the Pipeline.                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`define FU_TYPE_NUM_LOG 2

module Issue_Stage(
		   ///////////
		   // Inputs
		   ///////////
		   input 					reset,		// System Reset
		   input 					clock,		// System Clock
		   input					branch_mis,	// Branch Misprediction Signal

		   //Inputs from FE
		   input [`PRF_SIZE-1:0] 			FE_rrat_valid_list,
		   input [1:0] [`PRF_SIZE_LOG-1:0]      	FE_prf_freed,       // PRF Index of Freeing valid_list entries
		   
		   //Inputs from ID
		   input [3:0] [1:0]				ID_IS_rs_valid,
		   input [3:0] [1:0] [63:0]			ID_IS_NPC,
		   input [3:0] [1:0] [31:0] 			ID_IS_IR,
		   input [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	ID_IS_alu_func,
		   input [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 	ID_IS_regA,
		   input [3:0] [1:0] [`PRF_SIZE_LOG-1:0]      	ID_IS_regB,
		   input [3:0] [1:0] [1:0]	       		ID_IS_opa_sel,
		   input [3:0] [1:0] [1:0]     	   		ID_IS_opb_sel,
		   input [3:0] [1:0] [`PRF_SIZE_LOG-1:0]      	ID_IS_dest,
		   input [3:0] [1:0] [`ROB_SIZE_LOG-1:0]      	ID_IS_ROB,
		   input [3:0] [1:0] [`BRAT_SIZE_LOG-1:0]       ID_IS_BRAT,
		   input [1:0] 				      	ID_IS_uncn_br,

		   //Inputs from EX
		   input [3:0] [1:0]				EX_IS_stall,	// Whether the Execution Stage has a stall.
		   								// [3:0] for ALU, MULT, BR, MEM
		   								// [1:0] for 00, 01, 10, 11
		   //Inputs from CMP(Complete Stage)
		   input [1:0] [`PRF_SIZE_LOG-1:0]      	CMP_wr_idx,     // The Index of writeback value
		   input [1:0] [63:0] 				CMP_wr_data,	// The Data of writeback value
		   input [1:0] 					CMP_wr_en,	// Write Enable of Execution(Complete) Stage


		   //Inputs from Retire Stage
		   input [1:0]                                  RET_valid,      // Write Enable of Freeing valid_list entries
		   input [`BRAT_SIZE-1:0] 			BRAT_squash,

		   ///////////
		   // Outputs
		   ///////////

		   //Outputs for EX
		   output logic [3:0] [1:0] 	                	IS_EX_valid,
		   output logic [3:0] [1:0] [63:0]			IS_EX_NPC,
		   output logic [3:0] [1:0] [31:0] 			IS_EX_IR,
		   output logic [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	IS_EX_alu_func,
		   output logic [3:0] [1:0] [63:0] 			IS_EX_regA_value,
		   output logic [3:0] [1:0] [63:0] 			IS_EX_regB_value,
		   output logic [3:0] [1:0] [1:0] 		    	IS_EX_opa_sel,
		   output logic [3:0] [1:0] [1:0] 			IS_EX_opb_sel,
		   output logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 	IS_EX_dest,
		   output logic [3:0] [1:0] [`ROB_SIZE_LOG-1:0]     	IS_EX_ROB,
		   output logic [3:0] [1:0] [`BRAT_SIZE_LOG-1:0]        IS_EX_BRAT,
		   output logic [1:0] 				      	IS_EX_uncn_br,

		   //Outputs for ID
		   output [3:0] [1:0]					IS_ID_Not_Erase,
		   output [`PRF_SIZE-1:0] 				IS_ID_valid_list,

		   //Outputs for Debugging
		   output [1:0] 					write_conflict
		   );
    
   logic [3:0] [1:0] [63:0] 					IS_regA_value_out;
   logic [3:0] [1:0] [63:0] 					IS_regB_value_out;
   // Since we only have one MEM now, we disable the second MEM RS out
   logic [3:0] [1:0] 						IS_EX_validreg;
   
   
   genvar 							i,j;

   generate for(i=0;i<4;i++)
     begin
	for(j=0;j<2;j++)
	  begin
	     assign IS_EX_valid = (branch_mis & (BRAT_squash[IS_EX_BRAT[i][j]] == 1)) ? 0 : IS_EX_validreg;
	  end
     end
   endgenerate

   PRF PRF(
	   .rda_idx(ID_IS_regA),
	   .rdb_idx(ID_IS_regB),
	   .wr_idx(CMP_wr_idx), 					// write index
	   .wr_data(CMP_wr_data),					// write data
	   .wr_en(CMP_wr_en),						// write enable
	   .clock(clock),	
	   .reset(reset),					
	   .rda_out(IS_regA_value_out),
	   .rdb_out(IS_regB_value_out),			
	   .write_conflict(write_conflict[0])				// for test use
	   );
		   
   valid_list valid_list (//Inputs
			  .clock(clock),				// clock
			  .reset(reset),
			  .branch_mis(branch_mis),
			  .ready_idx(CMP_wr_idx),			// write index
			  .ready(CMP_wr_en),				// write enable
			  .retire_idx(FE_prf_freed),
			  .retire(RET_valid),
			  .RRAT_valid_list(FE_rrat_valid_list),
			  .valid_list(IS_ID_valid_list),	        // valid_list
			  .write_conflict(write_conflict[1])		// for test use
			  );

   generate for (i = 0;i < 4; i = i +1 ) 
     begin
	for (j = 0; j < 2; j = j + 1) 
	  begin
	     
	     assign IS_ID_Not_Erase[i][j] 		= EX_IS_stall[i][j] & IS_EX_valid[i][j]; 
	     
	     always_ff@(posedge clock)
	       begin
		  if(reset)
		    begin
		       IS_EX_validreg[i][j] 		<= `SD 0;
		       IS_EX_NPC[i][j]    		<= `SD 0;
		       IS_EX_IR[i][j]			<= `SD 0;
		       IS_EX_alu_func[i][j]		<= `SD 0;
		       IS_EX_regA_value[i][j] 		<= `SD 0;
		       IS_EX_regB_value[i][j] 		<= `SD 0;
		       IS_EX_opa_sel[i][j]		<= `SD 0;
		       IS_EX_opb_sel[i][j] 		<= `SD 0;
		       IS_EX_dest[i][j]			<= `SD 0;
		       IS_EX_ROB[i][j] 			<= `SD 0;
		       IS_EX_BRAT[i][j] 		<= `SD 0;
		       
		    end // if (reset)
		 else if(~EX_IS_stall[i][j])
		    begin
		       IS_EX_validreg[i][j] 		<= `SD ID_IS_rs_valid[i][j];
		       IS_EX_NPC[i][j]    		<= `SD ID_IS_NPC[i][j];
		       IS_EX_IR[i][j]			<= `SD ID_IS_IR[i][j];
		       IS_EX_alu_func[i][j]		<= `SD ID_IS_alu_func[i][j];
		       IS_EX_regA_value[i][j] 		<= `SD IS_regA_value_out[i][j];
		       IS_EX_regB_value[i][j] 		<= `SD IS_regB_value_out[i][j];
		       IS_EX_opa_sel[i][j]		<= `SD ID_IS_opa_sel[i][j];
		       IS_EX_opb_sel[i][j] 		<= `SD ID_IS_opb_sel[i][j];
		       IS_EX_dest[i][j]			<= `SD ID_IS_dest[i][j];
		       IS_EX_ROB[i][j] 			<= `SD ID_IS_ROB[i][j];
		       IS_EX_BRAT[i][j]			<= `SD ID_IS_BRAT[i][j];
		    end	 
		 else
		   IS_EX_validreg[i][j]			<= `SD IS_EX_valid[i][j];
	       end // always_ff@ (posedge clock)
	  end // for (j = 0; j < 2; j = j + 1)
     end // for (i = 0;i < 4; i = i +1 )
      
   endgenerate


   always_ff @(posedge clock) begin
	if(reset|branch_mis)
		IS_EX_uncn_br <= `SD 2'b0;
	else begin
		if (~EX_IS_stall[1][0])
			IS_EX_uncn_br[0] <= `SD ID_IS_uncn_br[0];
		if (~EX_IS_stall[1][1])
			IS_EX_uncn_br[1] <= `SD ID_IS_uncn_br[1];
	end

   end

endmodule // Issue_Stage
