/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  Issue_Stage.v                                       //
//                                                                     //
//  Description :  This module is the Issue Stage (with ppl regs) of   // 
//                 the Pipeline.                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`define FU_TYPE_NUM_LOG 2

module Issue_Stage(
		   ///////////
		   // Inputs
		   ///////////
		   input 					reset,		// System Reset
		   input 					clock,		// System Clock
		   input					branch_mis,	// Branch Misprediction Signal

		   //Inputs from FE
		   input [`PRF_SIZE-1:0] 			FE_valid_list,
		   input [1:0] [`PRF_SIZE_LOG-1:0]      	FE_prf_freed,       // PRF Index of Freeing valid_list entries
		   
		   //Inputs from ID
		   input [3:0] [1:0]				ID_IS_rs_valid,
		   input [3:0] [1:0] [63:0]			ID_IS_NPC,
		   input [3:0] [1:0] [31:0] 			ID_IS_IR,
		   input [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	ID_IS_alu_func,
		   input [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 	ID_IS_regA,
		   input [3:0] [1:0] [`PRF_SIZE_LOG-1:0]      	ID_IS_regB,
		   input [3:0] [1:0] [1:0]	       		ID_IS_opa_sel,
		   input [3:0] [1:0] [1:0]     	   		ID_IS_opb_sel,
		   input [3:0] [1:0] [`PRF_SIZE_LOG-1:0]      	ID_IS_dest,
		   input [3:0] [1:0] [`ROB_SIZE_LOG-1:0]      	ID_IS_ROB,
		   input [3:0] [1:0] [`BRAT_SIZE_LOG:0]         ID_IS_BRAT,
		   input [1:0] 				      	ID_IS_uncn_br,
		   input [1:0]					ID_IS_ld_or_st,
		   input [1:0] [`SQ_SIZE_LOG-1:0]	        ID_IS_out_sq_num,

		   //Inputs from LQ
		   input 					LQ_PRF_valid,
		   input [63:0]					LQ_PRF_NPC,
		   input [31:0]					LQ_PRF_IR,
		   input [`ROB_SIZE_LOG-1:0]			LQ_PRF_ROB,
		   input [`BRAT_SIZE_LOG:0]			LQ_PRF_BRAT,
		   input [`PRF_SIZE_LOG-1:0]			LQ_PRF_forward_PRF,
		   input [`PRF_SIZE_LOG-1:0]			LQ_PRF_regA,

		   //Inputs from SQ
		   input  [`PRF_SIZE_LOG-1:0]			LSQ_DC_regA,

		   //Inputs from EX
		   input [3:0] [1:0]				EX_IS_stall,	// Whether the Execution Stage has a stall.
		   								// [3:0] for ALU, MULT, BR, MEM
		   								// [1:0] for 00, 01, 10, 11
		   
		   //Inputs from CMP(Complete Stage)
		   input [1:0] [`PRF_SIZE_LOG-1:0]      	CMP_wr_idx,     // The Index of writeback value
		   input [1:0] [63:0] 				CMP_wr_data,	// The Data of writeback value
		   input [1:0] 					CMP_wr_en,	// Write Enable of Execution(Complete) Stage


		   //Inputs from Retire Stage
		   input [1:0]                                  RET_valid,      // Write Enable of Freeing valid_list entries
		   input [`BRAT_SIZE:0] 			BRAT_squash,

		   //Inputs from BRAT CMT Stage
		   input [1:0]					BRAT_CMT_v,
		   input [1:0] [`BRAT_SIZE_LOG-1:0]		BRAT_CMT_num,

		   ///////////
		   // Outputs
		   ///////////

		   //Outputs for EX
		   output 	[3:0] [1:0] 	                	IS_EX_valid,
		   output logic [3:0] [1:0] [63:0]			IS_EX_NPC,
		   output logic [3:0] [1:0] [31:0] 			IS_EX_IR,
		   output logic [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	IS_EX_alu_func,
		   output logic [2:0] [1:0] [63:0] 			IS_EX_regA_value,
		   output logic [3:0] [1:0] [63:0] 			IS_EX_regB_value,
		   output logic [1:0] [`PRF_SIZE_LOG-1:0] 		IS_EX_ls_regA,		
		   output logic [3:0] [1:0] [1:0] 		    	IS_EX_opa_sel,
		   output logic [3:0] [1:0] [1:0] 			IS_EX_opb_sel,
		   output logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 	IS_EX_dest,
		   output logic [3:0] [1:0] [`ROB_SIZE_LOG-1:0]     	IS_EX_ROB,
		   output 	[3:0] [1:0] [`BRAT_SIZE_LOG:0]        	IS_EX_BRAT,
		   output logic [1:0] 				      	IS_EX_uncn_br,
		   output logic [1:0]					IS_EX_ld_or_st,
		   output logic [1:0] [`SQ_SIZE_LOG-1:0]	       	IS_EX_out_sq_num,

		   //Outputs for CDB
		   output 					       	PRF_CDB_valid,
		   output logic [63:0]			       		PRF_CDB_NPC,
		   output logic [31:0]			       		PRF_CDB_IR,
		   output logic [`ROB_SIZE_LOG-1:0]	       		PRF_CDB_ROB,
		   output	[`BRAT_SIZE_LOG:0]	       		PRF_CDB_BRAT,
		   output logic [63:0]	       				PRF_CDB_value,
		   output logic [`PRF_SIZE_LOG-1:0]	       		PRF_CDB_regA,

		   //Outputs for Dcache
		   output       [63:0]					LSQ_DC_wr_value,
		   
		   //Outputs for ID
		   output [3:0] [1:0]					IS_ID_Not_Erase,
		   output [`PRF_SIZE-1:0] 				IS_ID_valid_list,

		   //Outputs for Debugging
		   output [`PRF_SIZE-1:0] [63:0]			PRF_value,
		   output [1:0] 					write_conflict
		   );
    
   logic [2:0] [1:0] [63:0] 						IS_regA_value_out;
   logic [3:0] [1:0] [63:0] 						IS_regB_value_out;
   logic [63:0] 							PRF_ld_forward_value_out;
   logic [63:0] 							PRF_st_value_out;
							
   // Since we only have one MEM now, we disable the second MEM RS out
   logic [3:0] [1:0] 							IS_EX_validreg;
   logic [3:0] [1:0] [`BRAT_SIZE_LOG:0] 				IS_EX_BRATreg;
   logic 								PRF_CDB_validreg;
   logic [`BRAT_SIZE_LOG:0] 						PRF_CDB_BRATreg;
   logic 								PRF_Dcache_validreg;
   logic [`BRAT_SIZE_LOG:0] 						PRF_Dcache_BRATreg;
   
   
   
   genvar 								i,j;




   // PRF
   PRF PRF(
	   .rda_idx(ID_IS_regA[2:0]),
	   .rdb_idx(ID_IS_regB),
	   .LQ_regA(LQ_PRF_forward_PRF),
	   .SQ_regA(LSQ_DC_regA),
	   .wr_idx(CMP_wr_idx), 					// write index
	   .wr_data(CMP_wr_data),					// write data
	   .wr_en(CMP_wr_en),						// write enable
	   .clock(clock),				
	   .rda_out(IS_regA_value_out),
	   .rdb_out(IS_regB_value_out),
	   .LQ_value(PRF_ld_forward_value_out),
	   .SQ_value(LSQ_DC_wr_value),
	   .next_registers(PRF_value),
	   .write_conflict(write_conflict[0])				// for test use
	   );

   // valid_list
   valid_list valid_list (//Inputs
			  .clock(clock),				// clock
			  .reset(reset),
			  .branch_mis(branch_mis),
			  .ready_idx(CMP_wr_idx),			// write index
			  .ready(CMP_wr_en),				// write enable
			  .retire_idx(FE_prf_freed),
			  .retire(RET_valid),
			  .BRAT_valid_list(FE_valid_list),
			  .valid_list(IS_ID_valid_list),	        // valid_list
			  .write_conflict(write_conflict[1])		// for test use
			  );
   
   generate for(i=0;i<4;i++)
     begin
	for(j=0;j<2;j++)
	  begin
	     assign IS_EX_valid[i][j] = (branch_mis & (BRAT_squash[IS_EX_BRAT[i][j]] == 1)) ? 0 : IS_EX_validreg[i][j];
	     assign IS_EX_BRAT[i][j] = (BRAT_CMT_v[1] && (IS_EX_BRATreg[i][j] == {1'b0,BRAT_CMT_num[1]})) || (BRAT_CMT_v[0] && (IS_EX_BRATreg[i][j] == {1'b0,BRAT_CMT_num[0]})) ? {1'b1,`BRAT_SIZE_LOG'b0} : IS_EX_BRATreg[i][j];
	  end
     end
   endgenerate

   assign PRF_CDB_valid = (branch_mis & (BRAT_squash[PRF_CDB_BRAT] == 1)) ? 0 : PRF_CDB_validreg;
   assign PRF_CDB_BRAT = ((BRAT_CMT_v[1] && (PRF_CDB_BRATreg == {1'b0,BRAT_CMT_num[1]})) || (BRAT_CMT_v[0] && (PRF_CDB_BRATreg == {1'b0,BRAT_CMT_num[0]}))) ? {1'b1,`BRAT_SIZE_LOG'b0} : PRF_CDB_BRATreg;
   
   always_ff@(posedge clock)
     begin
	if(reset)
	  begin
	     PRF_CDB_validreg <= `SD 0;
	     PRF_CDB_NPC <= `SD 0;
	     PRF_CDB_IR <= `SD 0;
	     PRF_CDB_ROB <= `SD 0;
	     PRF_CDB_BRATreg <= `SD 0;
	     PRF_CDB_value <= `SD 0;
	     PRF_CDB_regA <= `SD 0;
	  end
	else
	  begin
	     PRF_CDB_validreg <= `SD LQ_PRF_valid;
	     PRF_CDB_NPC <= `SD LQ_PRF_NPC;
	     PRF_CDB_IR <= `SD LQ_PRF_IR;
	     PRF_CDB_ROB <= `SD LQ_PRF_ROB;
	     PRF_CDB_BRATreg <= `SD LQ_PRF_BRAT;
	     PRF_CDB_value <= `SD PRF_ld_forward_value_out;
	     PRF_CDB_regA <= `SD LQ_PRF_regA;
	  end // else: !if(reset)
     end // always_ff@ (posedge clock


   generate for (i = 0;i < 3; i = i +1 ) 
     begin
	for (j = 0; j < 2; j = j + 1) 
	  begin
	     
	     assign IS_ID_Not_Erase[i][j] 		= EX_IS_stall[i][j] & IS_EX_valid[i][j]; 
	     
	     always_ff@(posedge clock)
	       begin
		  if(reset)
		    begin
		       IS_EX_validreg[i][j] 		<= `SD 0;
		       IS_EX_NPC[i][j]    		<= `SD 0;
		       IS_EX_IR[i][j]			<= `SD 0;
		       IS_EX_alu_func[i][j]		<= `SD 0;
		       IS_EX_regA_value[i][j] 		<= `SD 0;
		       IS_EX_regB_value[i][j] 		<= `SD 0;
		       IS_EX_opa_sel[i][j]		<= `SD 0;
		       IS_EX_opb_sel[i][j] 		<= `SD 0;
		       IS_EX_dest[i][j]			<= `SD 0;
		       IS_EX_ROB[i][j] 			<= `SD 0;
		       IS_EX_BRATreg[i][j] 		<= `SD 0;
		       
		    end // if (reset)
		 else if(~EX_IS_stall[i][j])
		    begin
		       IS_EX_validreg[i][j] 		<= `SD ID_IS_rs_valid[i][j];
		       IS_EX_NPC[i][j]    		<= `SD ID_IS_NPC[i][j];
		       IS_EX_IR[i][j]			<= `SD ID_IS_IR[i][j];
		       IS_EX_alu_func[i][j]		<= `SD ID_IS_alu_func[i][j];
		       IS_EX_regA_value[i][j] <= `SD IS_regA_value_out[i][j];
		       IS_EX_regB_value[i][j] 		<= `SD IS_regB_value_out[i][j];
		       IS_EX_opa_sel[i][j]		<= `SD ID_IS_opa_sel[i][j];
		       IS_EX_opb_sel[i][j] 		<= `SD ID_IS_opb_sel[i][j];
		       IS_EX_dest[i][j]			<= `SD ID_IS_dest[i][j];
		       IS_EX_ROB[i][j] 			<= `SD ID_IS_ROB[i][j];
		       IS_EX_BRATreg[i][j]		<= `SD ID_IS_BRAT[i][j];
		    end	 
		 else
		   begin
		      IS_EX_validreg[i][j]			<= `SD IS_EX_valid[i][j];
		      IS_EX_BRATreg[i][j]			<= `SD IS_EX_BRAT[i][j];
		   end // else: !if(~EX_IS_stall[i][j])
	       end // always_ff@ (posedge clock)
	  end // for (j = 0; j < 2; j = j + 1)
     end // for (i = 0;i < 4; i = i +1 )    
   endgenerate


   generate  for (j = 0; j < 2; j = j + 1) 
     begin	
	assign IS_ID_Not_Erase[3][j] 		= EX_IS_stall[3][j] & IS_EX_valid[3][j]; 
	
	always_ff@(posedge clock)
	  begin
	     if(reset)
	       begin
		  IS_EX_validreg[3][j] 		<= `SD 0;
		  IS_EX_NPC[3][j]    		<= `SD 0;
		  IS_EX_IR[3][j]			<= `SD 0;
		  IS_EX_alu_func[3][j]		<= `SD 0;
		  IS_EX_ls_regA[j] 		<= `SD 0;
		  IS_EX_regB_value[3][j] 		<= `SD 0;
		  IS_EX_opa_sel[3][j]		<= `SD 0;
		  IS_EX_opb_sel[3][j] 		<= `SD 0;
		  IS_EX_dest[3][j]			<= `SD 0;
		  IS_EX_ROB[3][j] 			<= `SD 0;
		  IS_EX_BRATreg[3][j] 		<= `SD 0;
		  
	       end // if (reset)
	     else if(~EX_IS_stall[3][j])
	       begin
		  IS_EX_validreg[3][j] 		<= `SD ID_IS_rs_valid[3][j];
		  IS_EX_NPC[3][j]    		<= `SD ID_IS_NPC[3][j];
		  IS_EX_IR[3][j]			<= `SD ID_IS_IR[3][j];
		  IS_EX_alu_func[3][j]		<= `SD ID_IS_alu_func[3][j];
		  IS_EX_ls_regA[j] 		<= `SD ID_IS_regA[3][j];
		  IS_EX_regB_value[3][j] 		<= `SD IS_regB_value_out[3][j];
		  IS_EX_opa_sel[3][j]		<= `SD ID_IS_opa_sel[3][j];
		  IS_EX_opb_sel[3][j] 		<= `SD ID_IS_opb_sel[3][j];
		  IS_EX_dest[3][j]			<= `SD ID_IS_dest[3][j];
		  IS_EX_ROB[3][j] 			<= `SD ID_IS_ROB[3][j];
		  IS_EX_BRATreg[3][j]		<= `SD ID_IS_BRAT[3][j];
	       end	 
	     else
	       begin
		  IS_EX_validreg[3][j]			<= `SD IS_EX_valid[3][j];
		  IS_EX_BRATreg[3][j]			<= `SD IS_EX_BRAT[3][j];
	       end // else: !if(~EX_IS_stall[3][j])
	  end // always_ff@ (posedge clock)
     end // for (j = 0; j < 2; j = j + 1)
   endgenerate

   
   generate for(i=0;i<2;i++)
     begin
	always_ff @(posedge clock) begin
	   if(reset)
	     begin
		IS_EX_uncn_br[i] <= `SD 0;
		IS_EX_ld_or_st[i] <= `SD 0;
		IS_EX_out_sq_num[i] <= `SD 0;
	     end
	   else
	     begin
		if (~EX_IS_stall[1][i])
		  begin
		     IS_EX_uncn_br[i] 		<= `SD ID_IS_uncn_br[i];
		  end
		if(~EX_IS_stall[3][i])
		  begin
		     IS_EX_ld_or_st[i] <= `SD ID_IS_ld_or_st[i];
		     IS_EX_out_sq_num[i] <= `SD ID_IS_out_sq_num[i];
		  end
	     end // else: !if(reset|branch_mis)
	end // always_ff @ (posedge clock)
     end // for (i=0;i<2;i++)
   endgenerate
   
endmodule // Issue_Stage
