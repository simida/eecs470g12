/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  Dispatch_Stage.v                                    //
//                                                                     //
//  Description :  This module is the Whole Dispatch_Stage of          // 
//                 the Pipeline.                                       //
//                                                                     //
/////////////////////////////////////////////////////////////////////////


module Dispatch_Stage (
		       //////////
		       // Inputs
		       //////////
		       input        				reset,
		       input        				clock,
		       
		       input        				BRAT_CMP_miss,
		       input [`BRAT_SIZE:0]			BRAT_squash,
		       input [`PRF_SIZE-1:0] 			valid_list, 

		       /*
		       //Inputs from Dmem
		       input  [63:0] 				Dmem2proc_data,
		       input   [3:0] 				Dmem2proc_tag,
		       input   [3:0] 				Dmem2proc_response,
		       */
		       /*
		       //Inputs from EX stage
		       input      				EX_LSQ_not_erase,
		       input [1:0]				EX_LSQ_complete,
		       input [1:0] [63:0] 			EX_LSQ_address,       
		       input [1:0] [63:0] 			EX_LSQ_value,   
		       input [1:0] [`ROB_SIZE_LOG-1:0] 	        EX_LSQ_ROB,
		       */
		       
		       //Inputs from FE_RAT
		       input [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_regA,         // reg A value
		       input [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_regB,         // reg B value
		       input [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_dest_reg,     // destination (writeback) register index
		       input [1:0] [`BRAT_SIZE_LOG:0] 		FE_ID_BRAT,         // destination (writeback) register index
		       
		       //Inputs from Decoder
		       input [1:0] [63:0] 			FE_ID_NPC,
		       input [1:0] [31:0] 			FE_ID_IR,
		       input [1:0] [1:0] 			FE_ID_opa_select,    // ALU opa mux select (ALU_OPA_xxx *)
		       input [1:0] [1:0] 			FE_ID_opb_select,    // ALU opb mux select (ALU_OPB_xxx *)
   	               // (ZERO_REG if no writeback)
		       input [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	FE_ID_alu_func,      // ALU function select (ALU_xxx *)
		       input [1:0]        			FE_ID_rd_mem,        // does inst read memory?
		       input [1:0]        			FE_ID_wr_mem,        // does inst write memory?
		       input [1:0]  			        FE_ID_cond_branch,   // is inst a conditional branch?
		       input [1:0]        			FE_ID_uncond_branch, // is inst an unconditional branch 
	               // or jump?
		       input [1:0]       			FE_ID_halt,          // is inst a halt 00 01 10 11
		       input [1:0]       			FE_ID_illegal,       // is inst an illegal inst 00 01 10 11
		       input [1:0]       			FE_ID_valid_inst,    // is inst a valid instruction to be  00 01 11
		       //  counted for CPI calculations?
		       
		       //Inputs from PRF
		       input [7:0]                              IS_ID_Not_Erase,
		       
		       //Inputs from ROB
		       input [`ROB_SIZE_LOG-1:0] 		ROB_head,	
		       input [`ROB_SIZE_LOG-1:0]		ROB_tail,
		       
		       // Early Branch Resolution
		       input [1:0]				BRAT_CMT_v,	
		       input [1:0][`BRAT_SIZE_LOG-1:0]		BRAT_CMT_num,

		       //Inputs from SQ
		       input [1:0]				SQ_full,
		       input [`SQ_SIZE_LOG-1:0]			SQ_tail,
		       
		       
		       //////////
		       //Outputs
		       //////////
		       //Outputs for FU
		       //0-ALU 1-MULT 2-BR 3-MEM
		       output [3:0] [1:0]		   		ID_IS_rs_valid,
		       output [3:0] [1:0] [63:0] 	   		ID_IS_NPC,
		       output [3:0] [1:0] [31:0] 	   		ID_IS_IR,
		       output [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	ID_IS_alu_func,
		       output [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 		ID_IS_regA,
		       output [3:0] [1:0] [`PRF_SIZE_LOG-1:0]        	ID_IS_regB,
		       output [3:0] [1:0] [1:0]	       			ID_IS_opa_sel,
		       output [3:0] [1:0] [1:0]     	   		ID_IS_opb_sel,
		       output [3:0] [1:0] [`PRF_SIZE_LOG-1:0]        	ID_IS_dest,
		       output [3:0] [1:0] [`ROB_SIZE_LOG-1:0]        	ID_IS_ROB,
		       output [3:0] [1:0] [`BRAT_SIZE_LOG:0]        	ID_IS_BRAT,
		       output [1:0] 					ID_IS_uncn_br,
		       output [1:0]			       		ID_IS_ld_or_st,    // whether it is a ld or st
		       output [1:0] [`SQ_SIZE_LOG-1:0]			ID_IS_out_sq_num,
		       
		       /*
		       //Outputs for CMP
		       output logic		   			LSQ_CMP_valid,
      		       output logic [63:0]				LSQ_CMP_NPC,
      		       output logic [31:0] 				LSQ_CMP_IR,
      		       output logic [`PRF_SIZE_LOG-1:0]  		LSQ_CMP_dest,
      		       output logic [63:0] 				LSQ_CMP_result,
      		       output logic [`ROB_SIZE_LOG-1:0]    		LSQ_CMP_ROB,
      		       output logic [`BRAT_SIZE_LOG:0]    		LSQ_CMP_BRAT,
			*/
		       
		       // Outputs for Front End
		       output [3:0] [1:0]          		      	RS_full

		       /*
		       // Outputs for memory
    		       output  [1:0] 					proc2Dmem_command,
		       output [63:0] 					proc2Dmem_addr,    // Address sent to data-memory
		       output [63:0] 					proc2Dmem_data     // Data sent to data-memory
		       */
		       );
   
   logic [1:0] [`PRF_SIZE_LOG-1:0] 				FE_ID_ROB;
   logic [3:0] [1:0] 						RS_in_valid;
   logic [3:0] [1:0] [63:0] 					RS_in_NPC;
   logic [3:0] [1:0] [31:0] 					RS_in_IR;
   logic [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 			RS_in_alu_func;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 			RS_in_regA;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 			RS_in_regB;
   logic [3:0] [1:0] [1:0] 					RS_in_opa_sel;
   logic [3:0] [1:0] [1:0] 					RS_in_opb_sel;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 			RS_in_dest;
   logic [3:0] [1:0] [`ROB_SIZE_LOG-1:0] 			RS_in_ROB;
   logic [3:0] [1:0] [`BRAT_SIZE_LOG:0] 			RS_in_BRAT;
   
   
   wire [1:0] inst_valid = FE_ID_valid_inst & (~FE_ID_illegal) & (~FE_ID_halt);

   wire [3:0] [1:0] RS_in_num;
   wire [1:0] MEM_full;

   genvar i,j;

   assign FE_ID_ROB[0] = FE_ID_valid_inst[0] ? ROB_tail : 0;
   assign FE_ID_ROB[1] = FE_ID_valid_inst[1] ? ROB_tail + 1 : 0;
   assign RS_in_num[0] = inst_valid & (~RS_in_num[1]) & (~RS_in_num[2]) & (~RS_in_num[3]);
   assign RS_in_num[1] = inst_valid & (FE_ID_cond_branch | FE_ID_uncond_branch);
   assign RS_in_num[2] = inst_valid & {FE_ID_alu_func[1] == `ALU_MULQ, FE_ID_alu_func[0] == `ALU_MULQ};
   assign RS_in_num[3] = inst_valid & (FE_ID_rd_mem | FE_ID_wr_mem);

   assign RS_full[3] = MEM_full | SQ_full;

   generate for (i=0;i<4;i = i+1)
     begin
	always_comb
	  begin
	     if(RS_full[i] == 2'b01 && RS_in_num[i] == 2'b01)
	       begin
		  RS_in_valid[i] = 2'b01;
	       end
	     else RS_in_valid[i] = RS_in_num[i] & ~RS_full[i];
	  end
     end
   endgenerate

   generate for (i=0; i<4; i=i+1) begin
      for (j=0; j < 2; j = j+1) begin
	 assign RS_in_NPC[i][j] 		= FE_ID_NPC[j];
	 assign RS_in_IR[i][j] 			= RS_in_valid[i][j] ? FE_ID_IR[j] : `NOOP_INST;
	 assign RS_in_alu_func[i][j] 		= RS_in_valid[i][j] ? FE_ID_alu_func[j] : 0;
	 assign RS_in_regA[i][j] 		= RS_in_valid[i][j] ? FE_ID_regA[j] : 0;
	 assign RS_in_regB[i][j] 		= RS_in_valid[i][j] ? FE_ID_regB[j] : 0;
	 assign RS_in_opa_sel[i][j] 		= RS_in_valid[i][j] ? FE_ID_opa_select[j] : 0;
	 assign RS_in_opb_sel[i][j]		= RS_in_valid[i][j] ? FE_ID_opb_select[j] : 0;
	 assign RS_in_dest[i][j] 		= RS_in_valid[i][j] ? FE_ID_dest_reg[j] : 0;
	 assign RS_in_ROB[i][j] 		= RS_in_valid[i][j] ? FE_ID_ROB[j] : 0;
	 assign RS_in_BRAT[i][j] 		= RS_in_valid[i][j] ? FE_ID_BRAT[j] : 0;
      end // for (j=0; j < 2; j = j+1)
   end // for (i=0; i<4; i=i+1)
      
   endgenerate

//   assign RS_reject = inst_valid ^ (RS_in_valid[0] | RS_in_valid[1] | RS_in_valid[2] | RS_in_valid[3]);

   RS_ALU ALU( 
	     .reset(reset), 
	     .clock(clock), 
	     .branch_mis(BRAT_CMP_miss),
	     .branch_mis_brat(BRAT_squash),
	     .valid_list(valid_list),
	     .BRAT_CMT_v(BRAT_CMT_v),
	     .BRAT_CMT_num(BRAT_CMT_num),
	     .RS_in_valid(RS_in_valid[0]),
             .RS_in_not_erase(IS_ID_Not_Erase[1:0]),
	     .RS_in_NPC(RS_in_NPC[0]),
	     .RS_in_IR(RS_in_IR[0]),				
	     .RS_in_alu_func(RS_in_alu_func[0]),
	     .RS_in_regA(RS_in_regA[0]),
	     .RS_in_regB(RS_in_regB[0]),
	     .RS_in_opa_sel(RS_in_opa_sel[0]),
	     .RS_in_opb_sel(RS_in_opb_sel[0]),
	     .RS_in_dest(RS_in_dest[0]),
	     .RS_in_ROB(RS_in_ROB[0]),
             .RS_in_BRAT(RS_in_BRAT[0]),
	     .RS_out_valid(ID_IS_rs_valid[0]),
	     .RS_out_NPC(ID_IS_NPC[0]),
	     .RS_out_IR(ID_IS_IR[0]),
	     .RS_out_alu_func(ID_IS_alu_func[0]),
	     .RS_out_regA(ID_IS_regA[0]),
	     .RS_out_regB(ID_IS_regB[0]),
	     .RS_out_opa_sel(ID_IS_opa_sel[0]),
	     .RS_out_opb_sel(ID_IS_opb_sel[0]),
	     .RS_out_dest(ID_IS_dest[0]),
	     .RS_out_ROB(ID_IS_ROB[0]),
	     .RS_out_BRAT(ID_IS_BRAT[0]),
	     .RS_full(RS_full[0])
   	     );
   
   RS_BR BR( 
	    .reset(reset), 
	    .clock(clock), 
	    .branch_mis(BRAT_CMP_miss),
	    .branch_mis_brat(BRAT_squash),
	    .valid_list(valid_list),
	    .BRAT_CMT_v(BRAT_CMT_v),
	    .BRAT_CMT_num(BRAT_CMT_num),
	    .RS_in_valid(RS_in_valid[1]),
            .RS_in_not_erase(IS_ID_Not_Erase[3:2]),
	    .RS_in_NPC(RS_in_NPC[1]),
	    .RS_in_IR(RS_in_IR[1]),				
	    .RS_in_alu_func(RS_in_alu_func[1]),
	    .RS_in_regA(RS_in_regA[1]),
	    .RS_in_regB(RS_in_regB[1]),
	    .RS_in_opa_sel(RS_in_opa_sel[1]),
	    .RS_in_opb_sel(RS_in_opb_sel[1]),
	    .RS_in_dest(RS_in_dest[1]),
	    .RS_in_ROB(RS_in_ROB[1]),
            .RS_in_BRAT(RS_in_BRAT[1]),
	    .RS_in_uncn_br(FE_ID_uncond_branch),
	    .RS_out_valid(ID_IS_rs_valid[1]),
	    .RS_out_NPC(ID_IS_NPC[1]),
	    .RS_out_IR(ID_IS_IR[1]),
	    .RS_out_alu_func(ID_IS_alu_func[1]),
	    .RS_out_regA(ID_IS_regA[1]),
	    .RS_out_regB(ID_IS_regB[1]),
	    .RS_out_opa_sel(ID_IS_opa_sel[1]),
	    .RS_out_opb_sel(ID_IS_opb_sel[1]),
	    .RS_out_dest(ID_IS_dest[1]),
	    .RS_out_ROB(ID_IS_ROB[1]),
	    .RS_out_BRAT(ID_IS_BRAT[1]),
	    .RS_out_uncn_br(ID_IS_uncn_br),
	    .RS_full(RS_full[1])
   	    );
   
   RS_MULT MULT( 
	      .reset(reset), 
	      .clock(clock), 
	      .branch_mis(BRAT_CMP_miss),
	      .branch_mis_brat(BRAT_squash),
	      .valid_list(valid_list),
	      .BRAT_CMT_v(BRAT_CMT_v),
	      .BRAT_CMT_num(BRAT_CMT_num),
	      .RS_in_valid(RS_in_valid[2]),
              .RS_in_not_erase(IS_ID_Not_Erase[5:4]),
	      .RS_in_NPC(RS_in_NPC[2]),
	      .RS_in_IR(RS_in_IR[2]),				
	      .RS_in_alu_func(RS_in_alu_func[2]),
	      .RS_in_regA(RS_in_regA[2]),
	      .RS_in_regB(RS_in_regB[2]),
	      .RS_in_opa_sel(RS_in_opa_sel[2]),
	      .RS_in_opb_sel(RS_in_opb_sel[2]),
	      .RS_in_dest(RS_in_dest[2]),
	      .RS_in_ROB(RS_in_ROB[2]),
              .RS_in_BRAT(RS_in_BRAT[2]),
	      .RS_out_valid(ID_IS_rs_valid[2]),
	      .RS_out_NPC(ID_IS_NPC[2]),
	      .RS_out_IR(ID_IS_IR[2]),
	      .RS_out_alu_func(ID_IS_alu_func[2]),
	      .RS_out_regA(ID_IS_regA[2]),
	      .RS_out_regB(ID_IS_regB[2]),
	      .RS_out_opa_sel(ID_IS_opa_sel[2]),
	      .RS_out_opb_sel(ID_IS_opb_sel[2]),
	      .RS_out_dest(ID_IS_dest[2]),
	      .RS_out_ROB(ID_IS_ROB[2]),
	      .RS_out_BRAT(ID_IS_BRAT[2]),
	      .RS_full(RS_full[2])
  	      );
   
   RS_MEM MEM( 
	       .reset(reset), 
	       .clock(clock), 
	       .branch_mis(BRAT_CMP_miss),
	       .branch_mis_brat(BRAT_squash),
	       .valid_list(valid_list),
	       .BRAT_CMT_v(BRAT_CMT_v),
	       .BRAT_CMT_num(BRAT_CMT_num),
	       .RS_in_valid(RS_in_valid[3]),
               .RS_in_not_erase(IS_ID_Not_Erase[7:6]),
	       .RS_in_NPC(RS_in_NPC[3]),
	       .RS_in_IR(RS_in_IR[3]),				
	       .RS_in_alu_func(RS_in_alu_func[3]),
	       .RS_in_regA(RS_in_dest[3]),
	       .RS_in_regB(RS_in_regB[3]),
	       .RS_in_opa_sel(RS_in_opa_sel[3]),
	       .RS_in_opb_sel(RS_in_opb_sel[3]),
	       .RS_in_dest(RS_in_regA[3]),
	       .RS_in_ROB(RS_in_ROB[3]),
               .RS_in_BRAT(RS_in_BRAT[3]),
	       .RS_in_sq_tail(SQ_tail),
	       .RS_out_valid(ID_IS_rs_valid[3]),
	       .RS_out_NPC(ID_IS_NPC[3]),
	       .RS_out_IR(ID_IS_IR[3]),
	       .RS_out_alu_func(ID_IS_alu_func[3]),
	       .RS_out_regA(ID_IS_regA[3]),
	       .RS_out_regB(ID_IS_regB[3]),
	       .RS_out_opa_sel(ID_IS_opa_sel[3]),
	       .RS_out_opb_sel(ID_IS_opb_sel[3]),
	       .RS_out_dest(ID_IS_dest[3]),
	       .RS_out_ROB(ID_IS_ROB[3]),
	       .RS_out_BRAT(ID_IS_BRAT[3]),
	       .RS_out_ld_or_st(ID_IS_ld_or_st),
	       .RS_out_sq_num(ID_IS_out_sq_num),
	       .RS_full(MEM_full)
   	       );

   /*
   LSQ  LSQ( .reset(reset),
	     .clock(clock),
	     .Dmem2proc_data(Dmem2proc_data),
	     .Dmem2proc_tag(Dmem2proc_tag),
	     .Dmem2proc_response(Dmem2proc_response),
	     .branch_mis(BRAT_CMP_miss),
	     .branch_mis_brat(BRAT_squash),
	     .issue_store(RET_issue_store),
	     .BRAT_CMT_v(BRAT_CMT_v),
	     .BRAT_CMT_num(BRAT_CMT_num),
	     .LSQ_in_valid(RS_in_valid[3]),
	     .LSQ_in_complete(EX_LSQ_complete),
	     .LSQ_in_not_erase(EX_LSQ_not_erase),
	     .LSQ_in_NPC(RS_in_NPC[3]),
	     .LSQ_in_IR(RS_in_IR[3]),
	     .LSQ_in_dest(RS_in_dest[3]),
	     .LSQ_in_ROB(RS_in_ROB[3]),
             .LSQ_in_BRAT(RS_in_BRAT[3]),
	     .LSQ_in_address(EX_LSQ_address),
	     .LSQ_in_store_value(EX_LSQ_value),
	     .LSQ_in_complete_ROB(EX_LSQ_ROB),
	     .ROB_head(ROB_head),
	     .LSQ_valid_out(LSQ_CMP_valid),
	     .LSQ_NPC_out(LSQ_CMP_NPC),
	     .LSQ_IR_out(LSQ_CMP_IR),
	     .LSQ_dest_out(LSQ_CMP_dest),
	     .LSQ_result_out(LSQ_CMP_result),
	     .LSQ_ROB_out(LSQ_CMP_ROB),
             .LSQ_BRAT_out(LSQ_CMP_BRAT),
	     .LSQ_full(LSQ_full),
	     .proc2Dmem_command(proc2Dmem_command),
	     .proc2Dmem_addr(proc2Dmem_addr),
	     .proc2Dmem_data(proc2Dmem_data)
         );
    */


endmodule // Top_RS
