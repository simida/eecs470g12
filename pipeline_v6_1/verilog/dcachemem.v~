// cachemem32x64
module dcachemem(
				input clock, reset, 
				input wr1_en,wr2_en,
				input  [4:0] wr1_idx, wr2_idx,
				input  [4:0] rd1_idx,
				input [`DCACHE_TAG_SIZE_LOG-1:0] wr1_tag, wr2_tag,
				input [`DCACHE_TAG_SIZE_LOG-1:0] rd1_tag,
				input [63:0] wr1_data,wr2_data, 
				
				output logic [63:0] rd_data_t,
				output logic rd_valid_t
                );


logic [63:0] rd1_data;
logic rd1_valid;

logic [31:0] [63:0] data ;
logic [31:0] [21:0] tags; 
logic [31:0]        valids;
//for victim cache
logic wr1_vic_en;
logic [63:0] wr1_vic_data;
logic [`DCACHE_TAG_SIZE_LOG-1:0] wr1_vic_tag;
logic wr2_vic_en;
logic [63:0] wr2_vic_data;
logic [`DCACHE_TAG_SIZE_LOG-1:0] wr2_vic_tag;
//victim cache
	//write
logic [`VIC_DCACHE_SIZE-1:0]				vic_cache_valids;
logic [`VIC_DCACHE_SIZE-1:0][28:0]	vic_cache_tagidx;
logic [`VIC_DCACHE_SIZE-1:0][63:0]	vic_cache_values;
logic [`VIC_DCACHE_SIZE_LOG-1:0]	vic_cache_head;
wire  [`VIC_DCACHE_SIZE_LOG-1:0]	vic_cache_head_p1;
	//erase
logic																vic_cache_erase1_valid;
logic [`VIC_DCACHE_SIZE_LOG-1:0]			vic_cache_erase1_addr;
logic																vic_cache_erase2_valid;
logic [`VIC_DCACHE_SIZE_LOG-1:0]			vic_cache_erase2_addr;
	//read
logic					vic_cache_valid;
logic [63:0]  vic_cache_data;

assign vic_cache_head_p1 = vic_cache_head + `VIC_DCACHE_SIZE_LOG'd1;
assign rd_valid_t = rd1_valid ? 1 : vic_cache_valid ? 1:0;
assign rd_data_t = rd1_valid ? rd1_data : vic_cache_valid ? vic_cache_data : 0;

//for victim cache
always_comb begin
	vic_cache_valid = 1'b0;
	vic_cache_erase1_valid=1'b0;
	vic_cache_erase2_valid=1'b0;
	vic_cache_data = 64'b0;
	vic_cache_erase1_addr = 0;
	vic_cache_erase2_addr = 0;

 for(int i=0;i<`VIC_DCACHE_SIZE;i++) begin
	if(vic_cache_valids[i] && (vic_cache_tagidx[i]=={rd1_tag,rd1_idx}))begin
		vic_cache_valid=1'b1;
		vic_cache_data=vic_cache_values[i];
	end
 end//for

	if(wr1_en) begin
	 for(int i=0;i<`VIC_DCACHE_SIZE;i++) begin
		if(vic_cache_valids[i] && (vic_cache_tagidx[i]=={wr1_tag,wr1_idx}))begin
			vic_cache_erase1_valid=1'b1;
			vic_cache_erase1_addr=i;
		end
	 end//for
	end//if

	if(wr2_en) begin
	 for(int i=0;i<`VIC_DCACHE_SIZE;i++) begin
		if(vic_cache_valids[i] && (vic_cache_tagidx[i]=={wr2_tag,wr2_idx}))begin
			vic_cache_erase2_valid=1'b1;
			vic_cache_erase2_addr=i;
		end
	 end//for
	end//if

end//always_comb

always_comb begin
		rd1_data = data[rd1_idx];
		rd1_valid = valids[rd1_idx]&&(tags[rd1_idx] == rd1_tag);
	
		//victim cache
		wr1_vic_en = 1'b0;
		wr2_vic_en = 1'b0;

		if(wr1_en && valids[wr1_idx])	begin
			wr1_vic_en = 1'b1;
			wr1_vic_data=data[wr1_idx];
			wr1_vic_tag=tags[wr1_idx];
		end
		if(wr2_en && valids[wr2_idx])	begin
			wr2_vic_en = 1'b1;
			wr2_vic_data=data[wr2_idx];
			wr2_vic_tag=tags[wr2_idx];
		end
end

always_ff @(posedge clock)
begin
  if(reset) valids <= `SD 31'b0;
  else begin
	 if(wr1_en) 
    valids[wr1_idx] <= `SD 1;
	 if (wr2_en)
		valids[wr2_idx] <= `SD 1;
	end
end

always_ff @(posedge clock)
begin
  if(wr1_en)
  begin
    data[wr1_idx] <= `SD wr1_data;
    tags[wr1_idx] <= `SD wr1_tag;
  end
	if(wr2_en)
	begin
    data[wr2_idx] <= `SD wr2_data;
    tags[wr2_idx] <= `SD wr2_tag;
	end
end

	//victim cache
	always_ff @(posedge clock) begin
		if(reset) begin
			for(int i=0;i<`VIC_DCACHE_SIZE;i++) begin
				vic_cache_valids[i] <= `SD 1'b0;
			end
			vic_cache_head <= `SD {`VIC_DCACHE_SIZE_LOG'b0};  
		end
		else begin
			//erase victim cache
			if(vic_cache_erase1_valid)begin
				vic_cache_valids[vic_cache_erase1_addr] <= `SD 1'b0;
			end
			if(vic_cache_erase2_valid)begin
				vic_cache_valids[vic_cache_erase2_addr] <= `SD 1'b0;
			end
			//data evicted from dcachemem
			if(wr1_vic_en) begin
				vic_cache_tagidx[vic_cache_head] <= `SD {wr1_vic_tag,wr1_idx};	
				vic_cache_values[vic_cache_head]	 <= `SD wr1_vic_data;
				vic_cache_valids[vic_cache_head]	 <= `SD 1'b1;

				if(wr2_vic_en) begin
					vic_cache_tagidx[vic_cache_head_p1] <= `SD {wr2_vic_tag,wr2_idx};	
					vic_cache_values[vic_cache_head_p1]	 <= `SD wr2_vic_data;
					vic_cache_valids[vic_cache_head_p1]	 <= `SD 1'b1;
					vic_cache_head <= `SD vic_cache_head + `VIC_DCACHE_SIZE_LOG'd2;
				end
				else begin
					vic_cache_head <= `SD vic_cache_head + `VIC_DCACHE_SIZE_LOG'd1;
				end

			end
			else if(wr2_vic_en) begin
					vic_cache_tagidx[vic_cache_head] <= `SD {wr2_vic_tag,wr2_idx};	
					vic_cache_values[vic_cache_head]	 <= `SD wr2_vic_data;
					vic_cache_valids[vic_cache_head]	 <= `SD 1'b1;
					vic_cache_head <= `SD vic_cache_head + `VIC_DCACHE_SIZE_LOG'd1;
			end


		end // else: !if(reset)
  end // always_ff @ (posedge clock)

endmodule
