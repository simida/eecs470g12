//NOTE: cache wr_enable is asyn, rd_valid is syn
module dcache(
			  input         clock,
			  input         reset,
				//input from LSQ
				input						LSQ_DC_rd_mem,
				input	 [63:0]		LSQ_DC_addr,
				input  					LSQ_DC_wr_mem,
				input	 [63:0]		LSQ_DC_wr_value,
				input	 [`BRAT_SIZE_LOG:0]	LSQ_DC_BRAT_num,
				input	 [`ROB_SIZE_LOG-1:0]	LSQ_DC_ROB_num,
				input  [`PRF_SIZE_LOG-1:0]	LSQ_DC_PRF_num,
				input  [63:0]								LSQ_DC_NPC,
				input  [31:0]								LSQ_DC_IR,
				//input from Dcache
			  input  [63:0] Dcache_data,
			  input         Dcache_valid,
				//input from Dmemory
				input  [63:0]	Dmem_DC_data,
				input	 [3:0]	Dmem_DC_response,
				input	 [3:0]	Dmem_DC_tag,
				//input from ROB
				input  [`BRAT_SIZE:0]	BRAT_squash,
				//input from BRAT_CMT Stage
				input [1:0]											BRAT_CMT_v,
				input [1:0][`BRAT_SIZE_LOG-1:0]	BRAT_CMT_num,
				//input from mem.v
				input [3:0]		mem2proc_response,
				input [3:0]		mem2proc_tag,

				//output to LSQ
				output 	logic					MSHR_full,//not use head/tail, but just count
				output  logic					LD_BUF_full,
				output	logic [63:0]	Dcache_LSQ_data,
				output	logic [63:0]	Dcache_LSQ_rd_addr,
				output	logic					Dcache_LSQ_rd_valid,
				output  logic	[`ROB_SIZE_LOG-1:0]	Dcache_LSQ_ROB_num,
				output  logic [`PRF_SIZE_LOG-1:0] Dcache_LSQ_PRF_num,
				output  logic [`BRAT_SIZE_LOG:0] Dcache_LSQ_BRAT_num,//1 bit larger
				output  logic [63:0] 	Dcache_LSQ_rd_NPC,
				output  logic [31:0] 	Dcache_LSQ_rd_IR,
			  //output to Dcache	
				output	logic [`SET_IDX_SIZE_LOG-1:0]					DC_Dcache_wr1_idx, DC_Dcache_wr2_idx,
				output	logic [`DCACHE_TAG_SIZE_LOG-1:0]			DC_Dcache_wr1_tag, DC_Dcache_wr2_tag,
				output	logic [63:0]													DC_Dcache_wr1_data, DC_Dcache_wr2_data,
				output	logic																	DC_Dcache_wr1_enable, DC_Dcache_wr2_enable,
				output	logic [`SET_IDX_SIZE_LOG-1:0]					DC_Dcache_rd_idx,
				output	logic [`DCACHE_TAG_SIZE_LOG-1:0]			DC_Dcache_rd_tag,
				//output to Dmem
				output	logic	[63:0]				DC_Dmem_addr,
				output	logic [1:0]					DC_Dmem_command,
				output	logic [63:0]				DC_Dmem_data,


output  logic												D1_LSQ_DC_rd_mem,
output  logic												D1_LSQ_DC_rd_memreg,
output  logic												D1_LSQ_DC_wr_mem,
output	logic	 [63:0]								D1_LSQ_DC_addr,
output	logic	 [63:0]								D1_LSQ_DC_NPC,
output	logic	 [31:0]								D1_LSQ_DC_IR,
output	logic	 [`BRAT_SIZE_LOG:0]		D1_LSQ_DC_BRAT_num,
output	logic	 [`BRAT_SIZE_LOG:0]		D1_LSQ_DC_BRAT_numreg,
output	logic	 [`ROB_SIZE_LOG-1:0]	D1_LSQ_DC_ROB_num,
output	logic	 [`PRF_SIZE_LOG-1:0]	D1_LSQ_DC_PRF_num,
//output	logic												D1_DC_Dcache_wr1_enable,
output	logic  [63:0]								D1_LSQ_DC_wr_value,

output	logic	[63:0]			 next_Dcache_LSQ_data,
output	logic	[63:0]			 next_Dcache_LSQ_rd_addr, 
output	logic							 next_Dcache_LSQ_rd_valid	,
output	logic	[`BRAT_SIZE_LOG:0]	next_LSQ_DC_BRAT_num,
output	logic [`BRAT_SIZE_LOG:0]	next_D1_LSQ_DC_BRAT_num,
output	logic											next_D1_LSQ_DC_rd_mem,
output	logic 										next_D1_LSQ_DC_wr_mem,
output	logic	[`ROB_SIZE_LOG-1:0]	  next_LSQ_DC_ROB_num,
output	logic	[`PRF_SIZE_LOG-1:0]	  next_LSQ_DC_PRF_num,

				//prefetch memory
output	logic [`MEM_LATENCY_IN_CYCLES_P1+1:0]	MSHR_regfl_shifter,
output	logic [`MEM_LATENCY_IN_CYCLES_P1+1:0]	next_MSHR_regfl_shifter,

//output  logic	[63:0]				next_DC_Dmem_addr,
//output  logic [1:0]					next_DC_Dmem_command,
//output	logic [63:0]				next_DC_Dmem_data,

output  logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_out_entry,
output  logic [`LD_BUF_SIZE_LOG-1:0]		LD_BUF_in_entry,

output  logic [`MSHR_SIZE_LOG-1:0]		MSHR_count,
output  logic [`MSHR_SIZE_LOG-1:0]		next_MSHR_count,
output  logic  				Dcache_LSQ_rd_validreg,
output  logic [`BRAT_SIZE_LOG:0] 		Dcache_LSQ_BRAT_numreg,


output	logic  Dmem_out,
output	logic  Dcache_out,
output	logic  LD_BUF_in,
output	logic  LD_BUF_out,
output	logic[63:0]			 next_Dcache_LSQ_rd_NPC, 
output	logic[31:0]			 next_Dcache_LSQ_rd_IR, 


output	logic										     LD_BUF_empty
        );

  logic [15:0][63:0]								MSHR_regfl_NPC;//no need to store			
  logic [15:0][31:0]								MSHR_regfl_IR;//no need to store		
  logic [15:0][63:0]								MSHR_regfl_addr;//no need to store			
	//logic [15:0]											MSHR_regfl_valid;
	logic [15:0]											MSHR_regfl_valid_command;//??? to resolve squash case
	logic [15:0]											MSHR_regfl_valid_commandreg;//??? to resolve squash case
	logic [15:0][`BRAT_SIZE_LOG:0]		MSHR_regfl_BRAT_num;
	logic [15:0][`BRAT_SIZE_LOG:0]		MSHR_regfl_BRAT_numreg;
	logic [15:0][`ROB_SIZE_LOG-1:0]		MSHR_regfl_ROB_num;
	logic [15:0]						MSHR_regfl_wr_enable;
	logic [15:0]						MSHR_regfl_wr_enablereg;
	logic [15:0][`PRF_SIZE_LOG-1:0]		MSHR_regfl_PRF_num;
  //LD_BUF: for structural hazard between mem ld output and cache ld output 
	logic [`LD_BUF_SIZE-1:0][63:0]						   LD_BUF_data;
	logic [`LD_BUF_SIZE-1:0][63:0]						   LD_BUF_addr;
	logic [`LD_BUF_SIZE-1:0][63:0]						   LD_BUF_NPC;
	logic [`LD_BUF_SIZE-1:0][31:0]						   LD_BUF_IR;
	logic [`LD_BUF_SIZE-1:0]      						   LD_BUF_valid;
	logic [`LD_BUF_SIZE-1:0]      						   LD_BUF_validreg;
	logic [`LD_BUF_SIZE-1:0][`BRAT_SIZE_LOG:0] 	 LD_BUF_BRAT_num;//when squash
	logic [`LD_BUF_SIZE-1:0][`BRAT_SIZE_LOG:0] 	 LD_BUF_BRAT_numreg;
	logic [`LD_BUF_SIZE-1:0][`ROB_SIZE_LOG-1:0]  LD_BUF_ROB_num;
	logic [`LD_BUF_SIZE-1:0][`PRF_SIZE_LOG-1:0]  LD_BUF_PRF_num;
	logic [`LD_BUF_SIZE-1:0]										 LD_BUF_invalid;
   
assign LD_BUF_full = LD_BUF_validreg == 2'b11;//assume 2 entry for LD_BUF
assign next_LD_BUF_empty = LD_BUF_validreg == 2'b0;
assign Dcache_LSQ_rd_valid = (BRAT_squash[Dcache_LSQ_BRAT_num] == 1) ? 0 : Dcache_LSQ_rd_validreg;
   assign Dcache_LSQ_BRAT_num = ((BRAT_CMT_v[1] && (Dcache_LSQ_BRAT_numreg == {1'b0,BRAT_CMT_num[1]})) || (BRAT_CMT_v[0] && (Dcache_LSQ_BRAT_numreg == {1'b0,BRAT_CMT_num[0]}))) ? {1'b1,`BRAT_SIZE_LOG'b0} : Dcache_LSQ_BRAT_numreg;
	

	always_comb begin
		//default
		DC_Dmem_command = `BUS_NONE;
		DC_Dmem_addr 		= 64'b0;
    DC_Dmem_data    = 64'b0;

		next_Dcache_LSQ_rd_valid	= 1'b0;
		next_Dcache_LSQ_data 		= 64'b0;
		next_Dcache_LSQ_rd_addr	= 64'b0; 
		next_Dcache_LSQ_rd_NPC		= 64'b0; 
		next_Dcache_LSQ_rd_IR		= 32'b0; 
		next_LSQ_DC_BRAT_num = 0;
		next_LSQ_DC_ROB_num = 0;
		next_LSQ_DC_PRF_num = 0;

	  DC_Dcache_wr1_enable = 1'b0;
	  DC_Dcache_wr2_enable = 1'b0;
	  DC_Dcache_wr1_data  =  64'b0;
		DC_Dcache_wr1_tag = 0;
	  DC_Dcache_wr1_idx = 0;
	  DC_Dcache_wr2_data  =  64'b0;
		DC_Dcache_wr2_tag = 0;
	  DC_Dcache_wr2_idx = 0;

		Dmem_out = 1'b0;
		Dcache_out = 1'b0;
		next_MSHR_count=MSHR_count;

		LD_BUF_in = 1'b0;
		LD_BUF_in_entry = 1'b0;
		LD_BUF_out = 1'b0;
		LD_BUF_out_entry = 1'b0;
		LD_BUF_invalid={`LD_BUF_SIZE'b0};

		next_D1_LSQ_DC_BRAT_num={1'b0,LSQ_DC_BRAT_num};
		next_D1_LSQ_DC_rd_mem=LSQ_DC_rd_mem;
		next_D1_LSQ_DC_wr_mem=LSQ_DC_wr_mem;
		D1_LSQ_DC_rd_mem=D1_LSQ_DC_rd_memreg;
		D1_LSQ_DC_BRAT_num = D1_LSQ_DC_BRAT_numreg;
		
		//rd mem squash
		if(BRAT_squash[D1_LSQ_DC_BRAT_numreg])begin
			D1_LSQ_DC_rd_mem=1'b0;
		end
		//BRAT CMT
		if(BRAT_CMT_v[1]&&(D1_LSQ_DC_BRAT_numreg=={1'b0,BRAT_CMT_num[1]} ))
				D1_LSQ_DC_BRAT_num ={1'b1,`BRAT_SIZE_LOG'b0};
		else if(BRAT_CMT_v[0]&&(D1_LSQ_DC_BRAT_numreg=={1'b0,BRAT_CMT_num[0]} ))
				D1_LSQ_DC_BRAT_num ={1'b1,`BRAT_SIZE_LOG'b0};
		else
				D1_LSQ_DC_BRAT_num = D1_LSQ_DC_BRAT_numreg;

		if(mem2proc_response)
				next_MSHR_count= next_MSHR_count+1;
		if(mem2proc_tag)
				next_MSHR_count=next_MSHR_count-1;

		next_MSHR_regfl_shifter={MSHR_regfl_shifter[`MEM_LATENCY_IN_CYCLES_P1:0],1'b0}; 
		if(D1_LSQ_DC_wr_mem) begin
			next_MSHR_regfl_shifter[0]=1;		
		end
		
		if(MSHR_regfl_shifter[`MEM_LATENCY_IN_CYCLES_P1+1]) begin
			next_MSHR_count=next_MSHR_count-1;
		end

	
		if(D1_LSQ_DC_rd_mem)begin
			if(!Dcache_valid) begin//read cache miss
				DC_Dmem_addr 		= D1_LSQ_DC_addr;
				DC_Dmem_command = `BUS_LOAD;
			end
			else begin
		      next_Dcache_LSQ_data 		= Dcache_data;
		      next_Dcache_LSQ_rd_addr	= D1_LSQ_DC_addr; 
		      next_Dcache_LSQ_rd_NPC		= D1_LSQ_DC_NPC; 
		      next_Dcache_LSQ_rd_IR		= D1_LSQ_DC_IR; 
		      next_Dcache_LSQ_rd_valid	= 1'b1;
		      next_LSQ_DC_BRAT_num = {1'b0,D1_LSQ_DC_BRAT_num};
		      next_LSQ_DC_ROB_num = D1_LSQ_DC_ROB_num;
			   	next_LSQ_DC_PRF_num = D1_LSQ_DC_PRF_num;
			end
		end
	   //LSQ store to memory
	  else if(D1_LSQ_DC_wr_mem) begin
	      DC_Dmem_addr    = D1_LSQ_DC_addr;
	      DC_Dmem_command = `BUS_STORE;
	      DC_Dmem_data		= D1_LSQ_DC_wr_value;
	  end
	   
	   //LSQ store to cache
	   if(LSQ_DC_wr_mem) begin
	      {DC_Dcache_wr2_tag,DC_Dcache_wr2_idx} = LSQ_DC_addr[63:`BLOCK_SIZE_LOG];
	      DC_Dcache_wr2_data   							=  LSQ_DC_wr_value;
	      DC_Dcache_wr2_enable						= 1'b1;
	   end
	   
		//squash MSHR(move from seq to comb)
		MSHR_regfl_valid_command = MSHR_regfl_valid_commandreg;
		MSHR_regfl_wr_enable = MSHR_regfl_wr_enablereg;
		for(int i = 0;i<`MSHR_SIZE;i++)begin
			if(MSHR_regfl_valid_commandreg[i]) begin
				if(BRAT_squash[MSHR_regfl_BRAT_numreg[i]])begin
						MSHR_regfl_valid_command[i] = 1'b0;
						MSHR_regfl_wr_enable[i] = 1'b0;
				end
			 end
		end

		//squash LD_BUF
		LD_BUF_valid=LD_BUF_validreg;
		for(int j=0;j<`LD_BUF_SIZE;j++) begin
			if(BRAT_squash[LD_BUF_BRAT_numreg[j]]) begin
					LD_BUF_valid[j] = 1'b0;
			end
		end
		//BRAT_CMT
		MSHR_regfl_BRAT_num = MSHR_regfl_BRAT_numreg;
		for(int k=0;k<`MSHR_SIZE;k++)begin
				if(BRAT_CMT_v[1]&&(MSHR_regfl_BRAT_numreg[k]=={1'b0,BRAT_CMT_num[1]} ))
						MSHR_regfl_BRAT_num[k] ={1'b1,`BRAT_SIZE_LOG'b0};
				else if(BRAT_CMT_v[0]&&(MSHR_regfl_BRAT_numreg[k]=={1'b0,BRAT_CMT_num[0]} ))
						MSHR_regfl_BRAT_num[k] ={1'b1,`BRAT_SIZE_LOG'b0};
				else
						MSHR_regfl_BRAT_num[k] = MSHR_regfl_BRAT_numreg[k];
		end

		LD_BUF_BRAT_num= LD_BUF_BRAT_numreg;
		for(int l=0;l<`LD_BUF_SIZE;l++) begin
				if(BRAT_CMT_v[1]&&(LD_BUF_BRAT_numreg[l]=={1'b0,BRAT_CMT_num[1]} ))
						LD_BUF_BRAT_num[l]={1'b1,`BRAT_SIZE_LOG'b0};
				else if(BRAT_CMT_v[0]&&(LD_BUF_BRAT_numreg[l]=={1'b0,BRAT_CMT_num[0]} ))
						LD_BUF_BRAT_num[l]={1'b1,`BRAT_SIZE_LOG'b0};
				else 
						LD_BUF_BRAT_num[l]= LD_BUF_BRAT_numreg[l];
		end

		
		//Output to LSQ/CDB from where
			//if this tag is in MSHR
		if(Dmem_DC_tag) begin
    		   if(MSHR_regfl_valid_command[Dmem_DC_tag])
		     		Dmem_out = 1'b1;
		end
		
		if(Dmem_out) begin
			if(next_Dcache_LSQ_rd_valid) begin
					LD_BUF_in = 1'b1;
					if(~LD_BUF_valid[0])
						LD_BUF_in_entry = 1'b0;
					else 
						LD_BUF_in_entry = 1'b1;//assume 2entry, must be one of them
			end
	
		end
		else begin
				if(!next_LD_BUF_empty) begin//next ???
					LD_BUF_out=1'b1;
					if(LD_BUF_valid[0])
						LD_BUF_out_entry = 1'b0;
					else
						LD_BUF_out_entry = 1'b1;//assume 2entry, must be one of them

					if(next_Dcache_LSQ_rd_valid) begin
							LD_BUF_in = 1'b1;
							if(~LD_BUF_valid[0])
								LD_BUF_in_entry = 1'b0;
							else 
								LD_BUF_in_entry = 1'b1;//assume 2entry, must be one of them
					end
				end
				else begin
					if(next_Dcache_LSQ_rd_valid) begin
							Dcache_out = 1'b1;
					end
				end
		end

		//mem ld output write back to cache
		if(MSHR_regfl_valid_command[Dmem_DC_tag] && Dmem_out && MSHR_regfl_wr_enable[Dmem_DC_tag])begin//timing???
				 DC_Dcache_wr1_data 										= Dmem_DC_data;
				 {DC_Dcache_wr1_tag,DC_Dcache_wr1_idx} 	= MSHR_regfl_addr[Dmem_DC_tag][63:`BLOCK_SIZE_LOG];
				 DC_Dcache_wr1_enable 									= 1'b1;///
		end

		//if want to write to same cache addr
		if(DC_Dcache_wr1_enable && DC_Dcache_wr2_enable) begin
				if(DC_Dcache_wr1_tag == DC_Dcache_wr2_tag) begin
					if(DC_Dcache_wr1_idx == DC_Dcache_wr2_idx) begin
							DC_Dcache_wr1_enable = 1'b0;//LSQ comes later
					end
				end
		end
		//MSHR_full
		if(MSHR_count == 4'hf) //assume 15 entry
			MSHR_full = 1'b1;
		else
			MSHR_full = 1'b0;
	end//always_comb

			


  always_ff @(posedge clock)
  begin
    if(reset)
    begin
			DC_Dcache_rd_tag <= `SD 0;
			DC_Dcache_rd_idx <= `SD 0;
			D1_LSQ_DC_wr_mem    <= `SD 0;
			D1_LSQ_DC_rd_memreg    <= `SD 0;

      Dcache_LSQ_rd_validreg	<= `SD 1'b0;
       
       for(int m = 0; m<16;m++)begin
	 	 //MSHR_regfl_valid[m] <= `SD 1'b0;
	 	 MSHR_regfl_valid_commandreg[m] <= `SD 1'b0;
	 	 MSHR_regfl_wr_enablereg[m] <= `SD 1'b0;
       end
       
       LD_BUF_validreg    <= `SD {`LD_BUF_SIZE'b0};
       LD_BUF_empty		  <= `SD 1'b1; 
       
       MSHR_count 			<= `SD {`MSHR_SIZE_LOG'b0};
	   MSHR_regfl_shifter  <= `SD {`MEM_LATENCY_IN_CYCLES_P1+2{1'b0}};
    end // if (reset)
     
    else begin
       D1_LSQ_DC_rd_memreg 		<= `SD next_D1_LSQ_DC_rd_mem;
       D1_LSQ_DC_addr   		<= `SD LSQ_DC_addr;
       D1_LSQ_DC_NPC   	   <= `SD LSQ_DC_NPC;
       D1_LSQ_DC_IR    	   <= `SD LSQ_DC_IR;
       D1_LSQ_DC_BRAT_numreg  <= `SD next_D1_LSQ_DC_BRAT_num;//???
       D1_LSQ_DC_ROB_num   <= `SD LSQ_DC_ROB_num;
       D1_LSQ_DC_PRF_num   <= `SD LSQ_DC_PRF_num;
       D1_LSQ_DC_wr_mem    <= `SD next_D1_LSQ_DC_wr_mem;
       D1_LSQ_DC_wr_value  <= `SD LSQ_DC_wr_value;
       
       Dcache_LSQ_rd_validreg	<= `SD 1'b0;
       LD_BUF_empty			  	<= `SD next_LD_BUF_empty; 
       MSHR_count 				<= `SD next_MSHR_count;
	   MSHR_regfl_shifter  		<= `SD next_MSHR_regfl_shifter;
       
       //BRAT_squash
		MSHR_regfl_valid_commandreg	<= `SD MSHR_regfl_valid_command;
		MSHR_regfl_wr_enablereg <= `SD MSHR_regfl_wr_enable;
		LD_BUF_validreg	  <= `SD LD_BUF_valid;
       //change by BRAT_CMT
       LD_BUF_BRAT_numreg <= `SD LD_BUF_BRAT_num;
       MSHR_regfl_BRAT_numreg <= `SD MSHR_regfl_BRAT_num;

       //Send to access memory
       if((Dmem_DC_response != 4'b0) && D1_LSQ_DC_rd_mem)
		 begin//only load in MSHR
		    MSHR_regfl_NPC[Dmem_DC_response] 	<= `SD D1_LSQ_DC_NPC;
		    MSHR_regfl_IR[Dmem_DC_response] 	<= `SD D1_LSQ_DC_IR;
		    MSHR_regfl_addr[Dmem_DC_response] 	<= `SD D1_LSQ_DC_addr;
		    MSHR_regfl_BRAT_numreg[Dmem_DC_response] <= `SD D1_LSQ_DC_BRAT_num;
		    MSHR_regfl_ROB_num[Dmem_DC_response] 	<= `SD D1_LSQ_DC_ROB_num;
		    MSHR_regfl_PRF_num[Dmem_DC_response] 	<= `SD D1_LSQ_DC_PRF_num;
		    //MSHR_regfl_valid[Dmem_DC_response] 	<= `SD 1'b1;
		    MSHR_regfl_valid_commandreg[Dmem_DC_response] 	<= `SD D1_LSQ_DC_rd_mem;
		    MSHR_regfl_wr_enablereg[Dmem_DC_response] <= `SD 1'b1;
		 end // if (Dmem_DC_response && D1_LSQ_DC_rd_mem)
       
       //STORE goes into MSHR, it clears 'DC_Dcache_wr1_enable' of prev LOAD
       if((Dmem_DC_response != 4'b0) && D1_LSQ_DC_wr_mem) 
		 begin
		    for(int t=0;t<`MSHR_SIZE;t++) 
		      begin
			 if(MSHR_regfl_valid_commandreg[t]) 
			   begin
			      if(MSHR_regfl_addr[t]==D1_LSQ_DC_addr)
				MSHR_regfl_wr_enablereg[t] <= `SD 1'b0;
			   end
		      end
		 end // if (Dmem_DC_response && D1_LSQ_DC_wr_mem)
    
       //output to LSQ/CDB
       //cache hit not able to send to LSQ
       if(LD_BUF_in) begin
		  LD_BUF_data[LD_BUF_in_entry] 	  <= `SD next_Dcache_LSQ_data;
		  LD_BUF_addr[LD_BUF_in_entry] 	  <= `SD next_Dcache_LSQ_rd_addr;
		  LD_BUF_NPC[LD_BUF_in_entry] 	  	<= `SD next_Dcache_LSQ_rd_NPC;
		  LD_BUF_IR[LD_BUF_in_entry] 	  	<= `SD next_Dcache_LSQ_rd_IR;
		  LD_BUF_validreg[LD_BUF_in_entry]	  <= `SD 1'b1;
		  LD_BUF_BRAT_numreg[LD_BUF_in_entry] <= `SD next_LSQ_DC_BRAT_num;
		  LD_BUF_ROB_num[LD_BUF_in_entry] <= `SD next_LSQ_DC_ROB_num;
		  LD_BUF_PRF_num[LD_BUF_in_entry] <= `SD next_LSQ_DC_PRF_num;
       end
       
       if(Dmem_out)begin
	 	 Dcache_LSQ_data 			<= `SD Dmem_DC_data;
	 	 Dcache_LSQ_rd_addr		<= `SD MSHR_regfl_addr[Dmem_DC_tag];
	 	 Dcache_LSQ_rd_NPC		<= `SD MSHR_regfl_NPC[Dmem_DC_tag];
	 	 Dcache_LSQ_rd_IR 		<= `SD MSHR_regfl_IR[Dmem_DC_tag];
	 	 Dcache_LSQ_rd_validreg	<= `SD 1'b1;
	 	 Dcache_LSQ_ROB_num		<= `SD MSHR_regfl_ROB_num[Dmem_DC_tag];
	 	 Dcache_LSQ_BRAT_numreg	<= `SD MSHR_regfl_BRAT_num[Dmem_DC_tag];
	 	 Dcache_LSQ_PRF_num		<= `SD MSHR_regfl_PRF_num[Dmem_DC_tag];
	 	 //clear MSHR
	 	 MSHR_regfl_valid_commandreg[Dmem_DC_tag] 					<= `SD 1'b0;
	 	 MSHR_regfl_wr_enablereg[Dmem_DC_tag] 	<= `SD 1'b0;
       end
       
       if(LD_BUF_out)begin
	 	 Dcache_LSQ_data 		<= `SD LD_BUF_data[LD_BUF_out_entry];
	 	 Dcache_LSQ_rd_addr		<= `SD LD_BUF_addr[LD_BUF_out_entry];
	 	 Dcache_LSQ_rd_NPC		<= `SD LD_BUF_NPC[LD_BUF_out_entry];
	 	 Dcache_LSQ_rd_IR 		<= `SD LD_BUF_IR[LD_BUF_out_entry];
	 	 Dcache_LSQ_rd_validreg	<= `SD 1'b1;
	 	 Dcache_LSQ_ROB_num		<= `SD LD_BUF_ROB_num[LD_BUF_out_entry];
	 	 Dcache_LSQ_BRAT_numreg	<= `SD LD_BUF_BRAT_num[LD_BUF_out_entry];
	 	 Dcache_LSQ_PRF_num		<= `SD LD_BUF_PRF_num[LD_BUF_out_entry];
	 	 LD_BUF_validreg[LD_BUF_out_entry] <= `SD 1'b0; 
       end	
       
       if(Dcache_out) begin
		  Dcache_LSQ_data 			<= `SD next_Dcache_LSQ_data;
		  Dcache_LSQ_rd_addr		<= `SD next_Dcache_LSQ_rd_addr;
		  Dcache_LSQ_rd_NPC			<= `SD next_Dcache_LSQ_rd_NPC;
		  Dcache_LSQ_rd_IR			<= `SD next_Dcache_LSQ_rd_IR;
		  Dcache_LSQ_rd_validreg	<= `SD 1'b1;
		  Dcache_LSQ_ROB_num		<= `SD next_LSQ_DC_ROB_num;
		  Dcache_LSQ_BRAT_numreg	<= `SD next_LSQ_DC_BRAT_num;
		  Dcache_LSQ_PRF_num		<= `SD next_LSQ_DC_PRF_num;
       end
       
       if(LSQ_DC_rd_mem) begin
	  {DC_Dcache_rd_tag,DC_Dcache_rd_idx} <= `SD LSQ_DC_addr[63:`BLOCK_SIZE_LOG];
       end
       
       
    end // else: !if(reset)
     
  end // always_ff @ (posedge clock)
   

endmodule

