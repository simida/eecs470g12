////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  File Name: pipeline_v1.v                                                  //
//                                                                            //
//  Description: The combination of all modules. A one-way OoO Processor      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------//

`timescale 1ns/100ps

module pipeline (

		 ////////////////////
		 //     INPUTS     //
		 ////////////////////

		 input        clock,        // System clock
		 input        reset,        // System reset
		 input        [3:0]  mem2proc_response, // Tag from memory about current request
		 input        [63:0] mem2proc_data,     // Data coming back from memory
		 input        [3:0]  mem2proc_tag,      // Tag from memory about current reply\

		 ////////////////////
		 //     OUTPUTS    //
		 ////////////////////
		 
		 output logic [1:0]  proc2mem_command,  // command sent to memory
		 output logic [63:0] proc2mem_addr,     // Address sent to memory
		 output logic [63:0] proc2mem_data,     // Data sent to memory
		 
		 output logic [3:0]  		pipeline_completed_insts,
		 output logic [3:0]  		pipeline_error_status,
		 output logic [1:0][4:0]  	pipeline_commit_wr_idx,
		 output logic [1:0][63:0] 	pipeline_commit_wr_data,
		 output logic [1:0]	        pipeline_commit_wr_en,
		 output logic [1:0][63:0] 	pipeline_commit_NPC
		 
                 );


   //synopsys sync_set_reset "reset"

   // Memory interface/arbiter wires
   logic [63:0] 	       proc2Dmem_addr, proc2Imem_addr;
   logic [1:0] 		       proc2Dmem_command, proc2Imem_command;
   logic [3:0] 		       Imem2proc_response, Dmem2proc_response;

   //////////////////////////
   // Outputs From FE
   //////////////////////////
   //------To RS & ROB------	
   logic [1:0] [31:0] 		     FE_ID_IR;
   logic [1:0] [63:0] 		     FE_ID_NPC;
   
   logic [1:0] [`PRF_SIZE_LOG-1:0]   FE_ID_regA;         // reg A value
   logic [1:0] [`PRF_SIZE_LOG-1:0]   FE_ID_regB;         // reg B value
   logic [1:0] [`PRF_SIZE_LOG-1:0]   FE_ID_dest_reg;     // destination (writeback) register index
   logic [1:0] [`ARF_SIZE_LOG-1:0]   FE_ID_dest_arfreg;     // destination (writeback) register index
   
   //logics from Decoder
   logic [1:0] [1:0] 		     FE_ID_opa_select;    // ALU opa mux select (ALU_OPA_xxx *)
   logic [1:0] [1:0] 		     FE_ID_opb_select;    // ALU opb mux select (ALU_OPB_xxx *)
   // (ZERO_REG if no writeback)
   logic [1:0] [`ALU_FUNC_NUM_LOG-1:0] FE_ID_alu_func;      // ALU function select (ALU_xxx *)
   logic [1:0] 			       FE_ID_rd_mem;        // does inst read memory?
   logic [1:0] 			       FE_ID_wr_mem;        // does inst write memory?
   logic [1:0] 			       FE_ID_cond_branch;   // is inst a conditional branch?
   logic [1:0] 			       FE_ID_uncond_branch; // is inst an unconditional branch 
   // or jump?
   logic [1:0] 			       FE_ID_halt;          // is inst a halt 00 01 10 11
   logic [1:0] 			       FE_ID_illegal;       // is inst an illegal inst 00 01 10 11
   logic [1:0] 			       FE_ID_valid_inst;    // is inst a valid instruction to be  00 01 10 11
   
   logic [`PRF_SIZE-1:0] 	       FE_rrat_valid_list;	       
   logic [1:0] [`PRF_IDX-1:0]	       FE_prf_freed;


   
   //////////////////////////
   // Outputs From ID
   //////////////////////////
   
   //Logics for FU
   //0-ALU 1-MULT 2-BR 3-MEM
   logic [3:0] [1:0] 		                ID_IS_rs_valid;
   logic [3:0] [1:0] [63:0] 	       		ID_IS_NPC;
   logic [3:0] [1:0] [31:0] 	       		ID_IS_IR;
   logic [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	ID_IS_alu_func;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0]     	ID_IS_regA;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0]     	ID_IS_regB;
   logic [3:0] [1:0] [1:0] 		     	ID_IS_opa_sel;
   logic [3:0] [1:0] [1:0] 		     	ID_IS_opb_sel;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0]     	ID_IS_dest;
   logic [3:0] [1:0] [`ROB_SIZE_LOG-1:0]     	ID_IS_ROB;
   logic [1:0] 					ID_IS_uncn_br;

   //Logics for CMP
   logic		   			LSQ_CMP_valid;
   logic [63:0]					LSQ_CMP_NPC;
   logic [31:0] 				LSQ_CMP_IR;
   logic [`PRF_SIZE_LOG-1:0]  			LSQ_CMP_dest;
   logic [63:0] 				LSQ_CMP_result;
   logic [`ROB_SIZE_LOG-1:0]    		LSQ_CMP_ROB;
   
   // Logics for Front End
   logic [3:0] [1:0] 				RS_full;
   
   //////////////////////////
   // Outputs From IS
   //////////////////////////

   //Logics for EX
   logic [3:0] [1:0] 				IS_EX_valid;
   logic [3:0] [1:0] [63:0] 			IS_EX_NPC;
   logic [3:0] [1:0] [31:0] 			IS_EX_IR;
   logic [3:0] [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	IS_EX_alu_func;
   logic [3:0] [1:0] [63:0] 			IS_EX_regA_value;
   logic [3:0] [1:0] [63:0] 			IS_EX_regB_value;
   logic [3:0] [1:0] [1:0] 			IS_EX_opa_sel;
   logic [3:0] [1:0] [1:0] 			IS_EX_opb_sel;
   logic [3:0] [1:0] [`PRF_SIZE_LOG-1:0] 	IS_EX_dest;
   logic [3:0] [1:0] [`ROB_SIZE_LOG-1:0] 	IS_EX_ROB;
   logic [1:0] 					IS_EX_uncn_br;
   
   //Logics for ID
   logic [3:0] [1:0] 				IS_ID_Not_Erase;
   logic [`PRF_SIZE-1:0] 			IS_ID_valid_list;
   
   //Logics for Debugging
   logic [`PRF_SIZE-1:0] [63:0]		    	IS_PRF;
   logic [1:0] 				    	write_conflict;
    
   //////////////////////////
   // Outputs From EX
   //////////////////////////
   
   // Logics for IS
   logic [3:0] [1:0] 			     EX_IS_stall;

   // Logics for LSQ
   logic      				     EX_LSQ_not_erase;
   logic [1:0]				     EX_LSQ_complete;
   logic [1:0] [63:0] 			     EX_LSQ_address;      
   logic [1:0] [63:0] 			     EX_LSQ_value;   
   logic [1:0] [`ROB_SIZE_LOG-1:0] 	     EX_LSQ_ROB;
   
   // Logics for CMP
   logic [1:0] 				     EX_CMP_valid;
   logic [1:0] [63:0] 			     EX_CMP_NPC;
   logic [1:0] [31:0] 			     EX_CMP_IR;
   logic [1:0] [`PRF_SIZE_LOG-1:0] 	     EX_CMP_dest;
   logic [1:0] [63:0] 			     EX_CMP_result;
   logic [1:0] [`ROB_SIZE_LOG-1:0] 	     EX_CMP_ROB;
   logic [1:0] 				     EX_CMP_branch;
   logic [1:0] [63:0]			     EX_CMP_branch_addr;
   
   //////////////////////////
   // Outputs From CMP/RET
   //////////////////////////
   
   //ROB related output
   logic [`ROB_SIZE_LOG-1:0] 		     ROB_head;
   logic [`ROB_SIZE_LOG-1:0] 		     ROB_tail;
   logic 				     ROB_full;
   logic 				     ROB_full_1left;
   
   //Retire signals
   logic [1:0] 				     RET_issue_store;
   logic [1:0] [`PRF_SIZE_LOG-1:0] 	     RET_prfn;
   logic [1:0] [`ARF_SIZE_LOG-1:0] 	     RET_arfn;
   logic [1:0] [63:0] 	     		     RET_NPC;
   //output logic [1:0]											 RET_branch;
   logic [1:0] [63:0] 			     RET_branch_addr;
   logic [1:0] 				     RET_branch_miss;
   logic [1:0] 				     RET_valid;//retire is valid
   logic [1:0]				     RET_halt;



   // Helping logics

   wire  [63:0] 			     RET_branch_addr_221 = RET_branch_miss[0] ? RET_branch_addr[0] : RET_branch_addr[1];
   wire 				     RET_branch_miss_221 = RET_branch_miss[0] | RET_branch_miss[1];

   wire	 [1:0]				     RET_free_enable;
		
   assign RET_free_enable[0] = RET_valid[0] & (RET_prfn[0] != `PRF_ZERO_REG);
   assign RET_free_enable[1] = RET_valid[1] & (RET_prfn[1] != `PRF_ZERO_REG);			


   assign proc2mem_command = (proc2Dmem_command==`BUS_NONE) ? proc2Imem_command : proc2Dmem_command;
   assign proc2mem_addr = (proc2Dmem_command==`BUS_NONE) ? proc2Imem_addr : proc2Dmem_addr;
   assign Dmem2proc_response = (proc2Dmem_command==`BUS_NONE) ? 0:  mem2proc_response;
   assign Imem2proc_response = (proc2Dmem_command==`BUS_NONE) ? mem2proc_response : 0;
   
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                   FRONT END STAGE                      //
   //                                                        //
   ////////////////////////////////////////////////////////////
   
   FrontEnd FrontEnd_0 (
			.clock(clock),
			.reset(reset),
			
			//------From/To Mem------
			.Imem2proc_response(Imem2proc_response),
			.mem2proc_data(mem2proc_data),
			.mem2proc_tag(mem2proc_tag),

			.proc2Imem_command(proc2Imem_command),
			.proc2Imem_addr(proc2Imem_addr),
			
			//------To RS & ROB------	
			.FE_ID_IR(FE_ID_IR),
			.FE_ID_NPC(FE_ID_NPC),
			
			.FE_ID_regA(FE_ID_regA),         // reg A value
			.FE_ID_regB(FE_ID_regB),         // reg B value
			.FE_ID_dest_prfreg(FE_ID_dest_reg),     // destination (writeback) register index
			.FE_ID_dest_arfreg(FE_ID_dest_arfreg),     // destination (writeback) register index
			
			//outputs from Decoder
			.FE_ID_opa_select(FE_ID_opa_select),    // ALU opa mux select (ALU_OPA_xxx *)
			.FE_ID_opb_select(FE_ID_opb_select),    // ALU opb mux select (ALU_OPB_xxx *)
   	                // (ZERO_REG if no writeback)
			.FE_ID_alu_func(FE_ID_alu_func),      // ALU function select (ALU_xxx *)
			.FE_ID_rd_mem(FE_ID_rd_mem),        // does inst read memory?
			.FE_ID_wr_mem(FE_ID_wr_mem),        // does inst write memory?
			.FE_ID_cond_branch(FE_ID_cond_branch),   // is inst a conditional branch?
			.FE_ID_uncond_branch(FE_ID_uncond_branch), // is inst an unconditional branch 
	                // or jump?
			.FE_ID_halt(FE_ID_halt),          // is inst a halt 00 01 10 11
			.FE_ID_illegal(FE_ID_illegal),       // is inst an illegal inst 00 01 10 11
			.FE_ID_valid_inst(FE_ID_valid_inst),    // is inst a valid instruction to be  00 01 10 11
			
			
			//------From ROB------	
			.ROB_full(ROB_full),
			.ROB_full_1left(ROB_full_1left),
			.RET_prfn(RET_prfn),
			.RET_arfn(RET_arfn),
			.RET_valid(RET_free_enable),
			.RET_branch_miss(RET_branch_miss_221),
			.RET_branch_addr(RET_branch_addr_221),
			
			
			//------From RS------	
			.RS_full(RS_full),

			//------To RS------
			.FE_rrat_valid_list(FE_rrat_valid_list),
			.FE_prf_freed(FE_prf_freed)
			
			);
    
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                  	   Dispatch Stage		     //
   //                            RS                          //
   //                                                        //
   ////////////////////////////////////////////////////////////

  Dispatch_Stage Dispatch_Stage (
				 //////////
				 // Inputs
				 //////////
				 .reset(reset),
	       			 .clock(clock),
	       			 .branch_mis(RET_branch_miss_221),
				 .valid_list(IS_ID_valid_list), 

		  	         // Inputs from Dmem
		  	         .Dmem2proc_data(mem2proc_data),
		                 .Dmem2proc_tag(mem2proc_tag),
		                 .Dmem2proc_response(Dmem2proc_response),
	       
				 //Inputs from FE_RAT
				 .FE_ID_regA(FE_ID_regA),         // reg A value
				 .FE_ID_regB(FE_ID_regB),         // reg B value
				 .FE_ID_dest_reg(FE_ID_dest_reg),     // destination (writeback) register index
	       
				 //Inputs from Decoder
				 .FE_ID_NPC(FE_ID_NPC),
	       			 .FE_ID_IR(FE_ID_IR),
	       			 .FE_ID_opa_select(FE_ID_opa_select),    // ALU opa mux select (ALU_OPA_xxx *)
				 .FE_ID_opb_select(FE_ID_opb_select),    // ALU opb mux select (ALU_OPB_xxx *)
   	                                                                  // (ZERO_REG if no writeback)
				 .FE_ID_alu_func(FE_ID_alu_func),      // ALU function select (ALU_xxx *)
				 .FE_ID_rd_mem(FE_ID_rd_mem),        // does inst read memory?
	       			 .FE_ID_wr_mem(FE_ID_wr_mem),        // does inst write memory?
				 .FE_ID_cond_branch(FE_ID_cond_branch),   // is inst a conditional branch?
				 .FE_ID_uncond_branch(FE_ID_uncond_branch), // is inst an unconditional branch 
	                                                                  // or jump?
				 .FE_ID_halt(FE_ID_halt),          // is inst a halt 00 01 10 11
				 .FE_ID_illegal(FE_ID_illegal),       // is inst an illegal inst 00 01 10 11
				 .FE_ID_valid_inst(FE_ID_valid_inst),    // is inst a valid instruction to be  00 01 11
				 //  counted for CPI calculations?
				 
				 //Inputs from PRF
				 .IS_ID_Not_Erase(IS_ID_Not_Erase),

				 //Inputs from EX to LSQ
	       			 .EX_LSQ_not_erase(EX_LSQ_not_erase),
	       			 .EX_LSQ_complete(EX_LSQ_complete),
	       			 .EX_LSQ_address(EX_LSQ_address),       
	       			 .EX_LSQ_value(EX_LSQ_value),   
	       			 .EX_LSQ_ROB(EX_LSQ_ROB),
				 
				 //Inputs from ROB
				 .ROB_head(ROB_head),	
				 .ROB_tail(ROB_tail),	
				 .RET_issue_store(RET_issue_store),
				 
				 //////////
				 //Outputs
				 //////////
				 //Outputs for FU
				 //0-ALU 1-MULT 2-BR 3-MEM
				 .ID_IS_rs_valid(ID_IS_rs_valid),
				 .ID_IS_NPC(ID_IS_NPC),
	       			 .ID_IS_IR(ID_IS_IR),
				 .ID_IS_alu_func(ID_IS_alu_func),
				 .ID_IS_regA(ID_IS_regA),
	            		 .ID_IS_regB(ID_IS_regB),
	       			 .ID_IS_opa_sel(ID_IS_opa_sel),
	       			 .ID_IS_opb_sel(ID_IS_opb_sel),
	       			 .ID_IS_dest(ID_IS_dest),
	            		 .ID_IS_ROB(ID_IS_ROB),
				 .ID_IS_uncn_br(ID_IS_uncn_br),
	       
	                         //Outputs for CMP
	            		 .LSQ_CMP_valid(LSQ_CMP_valid),
 	            		 .LSQ_CMP_NPC(LSQ_CMP_NPC),
	            		 .LSQ_CMP_IR(LSQ_CMP_IR),
	            		 .LSQ_CMP_dest(LSQ_CMP_dest),
	            		 .LSQ_CMP_result(LSQ_CMP_result),
	            		 .LSQ_CMP_ROB(LSQ_CMP_ROB),

				 // Outputs for Front End
				 .RS_full(RS_full),

				 // Outputs for Dmem
				 .proc2Dmem_command(proc2Dmem_command),
				 .proc2Dmem_addr(proc2Dmem_addr),    // Address sent to data-memory
				 .proc2Dmem_data(proc2mem_data)     // Data sent to data-memory
	       
	       );
   


   ////////////////////////////////////////////////////////////
   //                                                        //
   //                         ISSUE Stage                    //
   //                      PRF &  Valid List                 //
   //                                                        //
   ////////////////////////////////////////////////////////////
    Issue_Stage Issue_Stage(
			    ///////////
			    // Inputs
			    ///////////
			    .reset(reset),		// System Reset
			    .clock(clock),		// System Clock
			    .branch_mis(RET_branch_miss_221),	// Branch Misprediction Signal
			    
			    //Inputs from FE
			    .FE_rrat_valid_list(FE_rrat_valid_list),
			    .FE_prf_freed(FE_prf_freed),       // PRF Index of Freeing valid_list entries
			    
			    //Inputs from ID
			    .ID_IS_rs_valid(ID_IS_rs_valid),
			    .ID_IS_NPC(ID_IS_NPC),
			    .ID_IS_IR(ID_IS_IR),
			    .ID_IS_alu_func(ID_IS_alu_func),
			    .ID_IS_regA(ID_IS_regA),
			    .ID_IS_regB(ID_IS_regB),
			    .ID_IS_opa_sel(ID_IS_opa_sel),
			    .ID_IS_opb_sel(ID_IS_opb_sel),
			    .ID_IS_dest(ID_IS_dest),
			    .ID_IS_ROB(ID_IS_ROB),
		            .ID_IS_uncn_br(ID_IS_uncn_br),
			    
			    
			    //Inputs from EX
			    .EX_IS_stall(EX_IS_stall),	// Whether the Execution Stage has a stall.
		   	                                // [3:0] for ALU, MULT, BR, MEM
		   	                                // [1:0] for 00, 01, 10, 11
                    	    //Inputs from CMP(Complete Stage)
			    .CMP_wr_idx(EX_CMP_dest),     // The Index of writeback value
			    .CMP_wr_data(EX_CMP_result),	// The Data of writeback value
			    .CMP_wr_en(EX_CMP_valid),	// Write Enable of Execution(Complete) Stage


			    //Inputs from Retire Stage
			    .RET_valid(RET_valid),      // Write Enable of Freeing valid_list entries
			    
			    ///////////
			    // Outputs
			    ///////////
			    
			    //Outputs for EX
			    .IS_EX_valid(IS_EX_valid),
			    .IS_EX_NPC(IS_EX_NPC),
			    .IS_EX_IR(IS_EX_IR),
			    .IS_EX_alu_func(IS_EX_alu_func),
			    .IS_EX_regA_value(IS_EX_regA_value),
			    .IS_EX_regB_value(IS_EX_regB_value),
			    .IS_EX_opa_sel(IS_EX_opa_sel),
			    .IS_EX_opb_sel(IS_EX_opb_sel),
			    .IS_EX_dest(IS_EX_dest),
			    .IS_EX_ROB(IS_EX_ROB),
		            .IS_EX_uncn_br(IS_EX_uncn_br),
			    
			    //Outputs for ID
			    .IS_ID_Not_Erase(IS_ID_Not_Erase),
			    .IS_ID_valid_list(IS_ID_valid_list),
			    
			    //Outputs for Debugging
			    .IS_PRF(IS_PRF),
			    .write_conflict()
			    );
  
   ////////////////////////////////////////////////////////////
   //                                                        //
   //                        EXECUTE STAGE                   //
   //                     ALU, MULT, BR, MEM                 //
   //                                                        //
   ////////////////////////////////////////////////////////////
   Execute_Stage Execute_Stage(
		  //////////
		  // Inputs
		  //////////  
		  .clock(clock),
		  .reset(reset),
		  .branch_mis(|RET_branch_miss),
		  
		  // Inputs from IS		      
		  .IS_EX_valid(IS_EX_valid),
		  .IS_EX_NPC(IS_EX_NPC),
		  .IS_EX_IR(IS_EX_IR),
		  .IS_EX_alu_func(IS_EX_alu_func),
		  .IS_EX_regA_value(IS_EX_regA_value),
		  .IS_EX_regB_value(IS_EX_regB_value),
		  .IS_EX_opa_sel(IS_EX_opa_sel),
		  .IS_EX_opb_sel(IS_EX_opb_sel),
		  .IS_EX_dest(IS_EX_dest),
		  .IS_EX_ROB(IS_EX_ROB),
		  .IS_EX_uncn_br(IS_EX_uncn_br),

	          //Inputs from LSQ
	          .LSQ_CMP_valid(LSQ_CMP_valid),
 	          .LSQ_CMP_NPC(LSQ_CMP_NPC),
	          .LSQ_CMP_IR(LSQ_CMP_IR),
	          .LSQ_CMP_dest(LSQ_CMP_dest),
	          .LSQ_CMP_result(LSQ_CMP_result),
	          .LSQ_CMP_ROB(LSQ_CMP_ROB),


		  ///////////
		  // Outputs
		  ///////////
		  
		  // Outputs for IS
		  .EX_IS_stall(EX_IS_stall),

		  // Outputs for LSQ
	       	  .EX_LSQ_not_erase(EX_LSQ_not_erase),
	       	  .EX_LSQ_complete(EX_LSQ_complete),
	       	  .EX_LSQ_address(EX_LSQ_address),       
	       	  .EX_LSQ_value(EX_LSQ_value),   
	       	  .EX_LSQ_ROB(EX_LSQ_ROB),
		  
		  // Outputs for CMP
		  .EX_CMP_valid(EX_CMP_valid),
		  .EX_CMP_NPC(EX_CMP_NPC),
		  .EX_CMP_IR(EX_CMP_IR),
		  .EX_CMP_dest(EX_CMP_dest),
		  .EX_CMP_result(EX_CMP_result),
		  .EX_CMP_ROB(EX_CMP_ROB),
		  .EX_CMP_branch(EX_CMP_branch),
		  .EX_CMP_branch_addr(EX_CMP_branch_addr)

		  );
   

   ////////////////////////////////////////////////////////////
   //                                                        //
   //                     COMPLETE/RETIRE Stage              //
   //                             ROB                        //
   //                                                        //
   ////////////////////////////////////////////////////////////

   Complete_Retire_Stage Complete_Retire_Stage(
			  
					       //////////
					       // Inputs
					       //////////
			  		       .clock(clock),
			  		       .reset(reset),
					       
					       //Inputs from FE
			  		       .FE_ID_dest_prfreg(FE_ID_dest_reg),
					       .FE_ID_dest_arfreg(FE_ID_dest_arfreg),
			  	 	       .ID_valid_inst(FE_ID_valid_inst),//is inst a valid instruction to be 00 01 10 11
					       
					       //Inputs from decoder
					       .ID_branch(FE_ID_cond_branch | FE_ID_uncond_branch),//is branch or not (diff)
//NEED MODIFY
			  		       .ID_store(FE_ID_rd_mem|FE_ID_wr_mem), //is store or not
					       .FE_ID_NPC(FE_ID_NPC),
		                               .FE_ID_halt(FE_ID_halt),	
			  
					       //Inputs from Execution Complete
					       .EX_CMP_branch(EX_CMP_branch),
			  		       .EX_CMP_result(EX_CMP_branch_addr),//ex_targetaddr
			  	 	       .EX_CMP_valid(EX_CMP_valid),
					       .EX_CMP_ROB(EX_CMP_ROB),
			  
					       //Inputs from branch predictor
			  		       .branch_pred(2'b00),
			  		       .branch_predaddr(FE_ID_NPC),
			  
					       //////////
					       //Outputs
					       /////////
					       //ROB related output
					       .ROB_head(ROB_head),
					       .ROB_tail(ROB_tail),
			  		       .ROB_full(ROB_full),
			  		       .ROB_full_1left(ROB_full_1left),
			  
					       //Retire signals
			  		       .RET_issue_store(RET_issue_store),
					       .RET_prfn(RET_prfn),
					       .RET_arfn(RET_arfn),
					       .RET_NPC(RET_NPC),
					       //output logic [1:0]											 RET_branch,
					       .RET_branch_addr(RET_branch_addr),
					       .RET_branch_miss(RET_branch_miss),
			  		       .RET_valid(RET_valid),								          //retire is valid
					       .RET_halt(RET_halt)									  //Program halt
			  ); 


   assign pipeline_completed_insts = RET_valid[0]+RET_valid[1];
   assign pipeline_error_status = (RET_halt) ? `HALTED_ON_HALT : `NO_ERROR;

   assign pipeline_commit_wr_idx = RET_arfn;
   assign pipeline_commit_wr_data[0] = IS_PRF[RET_prfn[0]];
   assign pipeline_commit_wr_data[1] = IS_PRF[RET_prfn[1]];
   assign pipeline_commit_wr_en = RET_valid;
   assign pipeline_commit_NPC = RET_NPC;


endmodule
