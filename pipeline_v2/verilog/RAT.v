//Function: Map table: Save all the ARF to PRF mapping info
//Modifed by: Jingcheng Wang

//When modify RAT:
//		reset: all 1, 2, 3, ...
//		rename: instr has dest reg, then set wr_en=1, prf_dest write into RAT @ next posedge
//		flush: ROB flush everything, RAT gets the copy of RRAT

module RAT(
//READ Src Reg
	input  logic [4:0] 									arf_regA,
  input  logic [4:0] 									arf_regB, 
	output logic [`PRF_IDX-1:0] 				prf_regA,
	output logic [`PRF_IDX-1:0] 				prf_regB,

	input  logic [4:0] 									arf_regA1,
  input  logic [4:0] 									arf_regB1, 
	output logic [`PRF_IDX-1:0] 				prf_regA1,
	output logic [`PRF_IDX-1:0] 				prf_regB1,

//Write(rename) Dest Reg
  input  logic [4:0] 									arf_dest,
	input  logic [`PRF_IDX-1:0] 				prf_dest,

  input  logic [4:0] 									arf_dest1,
	input  logic [`PRF_IDX-1:0] 				prf_dest1,

//ON a flush of ROB, Copy RRAT
	input  logic												flush,
	input  logic [31:0] [`PRF_IDX-1:0] 	RRAT_registers,

  input  logic 									wr_en,wr_en1,
	input  logic 									clock,
	input  logic 									reset
);

  logic    [31:0] [`PRF_IDX-1:0] registers;   // 32, `PRF_IDX-bit Registers

	assign prf_regA = registers[arf_regA];
  assign prf_regB = registers[arf_regB];
	//assign prf_regA1 = registers[arf_regA1];
  //assign prf_regB1 = registers[arf_regB1];

	//---------Internal Forwarding---------------
  always_comb
    if (arf_regA1 == `ZERO_REG)
      prf_regA1 = `PRF_ZERO_REG;
    else if (wr_en && (arf_dest == arf_regA1))
      prf_regA1 = prf_dest;  // internal forwarding
    else
      prf_regA1 = registers[arf_regA1];
  always_comb
    if (arf_regB1 == `ZERO_REG)
      prf_regB1 = `PRF_ZERO_REG;
    else if (wr_en && (arf_dest == arf_regB1))
      prf_regB1 = prf_dest;  // internal forwarding
    else
      prf_regB1 = registers[arf_regB1];



	integer i;
	always_ff @(posedge clock)
	begin
		if(reset)
			begin
				/*registers[0] 		<= `SD 0;
				registers[1] 		<= `SD 1;
				registers[2] 		<= `SD 2;
				registers[3] 		<= `SD 3;
				registers[4] 		<= `SD 4;
				registers[5] 		<= `SD 5;
				registers[6] 		<= `SD 6;
				registers[7] 		<= `SD 7;
				registers[8] 		<= `SD 8;
				registers[9] 		<= `SD 9;
				registers[10] 	<= `SD 10;
				registers[11] 	<= `SD 11;
				registers[12] 	<= `SD 12;
				registers[13] 	<= `SD 13;
				registers[14] 	<= `SD 14;
				registers[15] 	<= `SD 15;
				registers[16] 	<= `SD 16;
				registers[17] 	<= `SD 17;
				registers[18] 	<= `SD 18;
				registers[19] 	<= `SD 19;
				registers[20] 	<= `SD 20;
				registers[21] 	<= `SD 21;
				registers[22] 	<= `SD 22;
				registers[23] 	<= `SD 23;
				registers[24] 	<= `SD 24;
				registers[25] 	<= `SD 25;
				registers[26] 	<= `SD 26;
				registers[27] 	<= `SD 27;
				registers[28] 	<= `SD 28;
				registers[29] 	<= `SD 29;
				registers[30] 	<= `SD 30;*/
				for(i=0; i<31; i=i+1)
				begin
					registers[i] 	<= `SD i;
				end
				registers[`ZERO_REG] 	<= `SD `PRF_ZERO_REG;
			end

		else if(flush)
			begin
				registers       <= `SD RRAT_registers;
			end

		else if(wr_en||wr_en1)
			begin
				if(wr_en && (arf_dest != `ZERO_REG)) registers[arf_dest] <= `SD prf_dest; 
				if(wr_en1 && (arf_dest1 != `ZERO_REG)) registers[arf_dest1] <= `SD prf_dest1; 
			end
	end//	always_ff @(posedge clock)



endmodule


