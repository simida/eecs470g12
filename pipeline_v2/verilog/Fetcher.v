//Function: 
//				1. Send address to ICache, get the instruction: IR --> Decoder
//				2. Calculate what to fetch in the next cycle: which PC? --Can add Predictor
//Modifed by: Jingcheng Wang

module Fetcher(
				  input         clock,               
				  input         reset,               

					//-----------To ICache------------------
				  output logic [63:0] proc2Icache_addr,
					//-----------From ICache------------------
				  input  [63:0] Icache2proc_data,		 
				  input         Icache_valid,

					//-----------To Decoder/RS/ROB------------------
				  output logic [63:0] NPC1,
				  output logic [31:0] IR1,	
				  output logic if_valid_inst1,
				  output logic [63:0] NPC2,
				  output logic [31:0] IR2,	
				  output logic if_valid_inst2,

					//-----------From ROB------------------
				  input         flush,              // taken-branch signal
				  input  [63:0] target_pc,           
					//------------From ROB/RS/FRL--Structure Hazard------
					input Struct_Stall,
					input Two_Accept
        );

  logic [63:0] PC_reg;               // PC we are currently fetching
  logic [63:0] next_PC;

  logic PC_enable;



  logic   [63:0] PC_plus_8;
  logic   [63:0] PC_plus_4;
  assign PC_plus_8 = PC_reg + 8;     // default next PC value
  assign PC_plus_4 = PC_reg + 4;     // default next PC value

	
	//if fetch successfully then it is valid!!!
	wire valid_inst;
  assign valid_inst = Icache_valid && (!flush);


//---------Interact with ICache to get IR-------------------------------

  assign proc2Icache_addr = {PC_reg[63:3], 3'b0};

	always_comb
		begin
		NPC1 = PC_plus_4;
		NPC2 = PC_plus_8;
		IR1 =	32'b0;
		IR2 =	32'b0;
		if_valid_inst1 = 1'b0;
		if_valid_inst2 = 1'b0;
			if(PC_reg[2])
				begin
				IR1 =	Icache2proc_data[63:32];
				if_valid_inst1 = valid_inst;
				end
			else
				begin
					IR1 =	Icache2proc_data[31:0];
					IR2 =	Icache2proc_data[63:32];
					if_valid_inst1 = valid_inst;
					if_valid_inst2 = valid_inst;
				end			
		end	
    // this mux is because the Imem gives us 64 bits not 32 bits
  	//assign IR = PC_reg[2] ? Icache2proc_data[63:32] : Icache2proc_data[31:0];


//---------Interact with ROB/RS to calculate next PC-------------------------------
//Normal case: if cache_valid => send to Backend
//abnormal case: flush/stall
//							stall: ROB/RS/FRL instr_invalid + PC stall
//							flush: instr_invalid (PC don't stall)  
//
//Think: whether to stall PC or instr_invalid or next_PC


    // The take-branch signal must override stalling (otherwise it may be lost)
  assign PC_enable= (Icache_valid && ~Struct_Stall) || flush;

    // next PC is target_pc if there is a taken branch or the next sequential PC (PC+4) if no branch
    // (halting is handled with the enable PC_enable;
  assign next_PC = flush ? target_pc : ((if_valid_inst2 && Two_Accept)? PC_plus_8 : PC_plus_4);


  // This register holds the PC value
  always_ff @(posedge clock)
  begin
    if(reset)
      PC_reg <= `SD 0;       // initial PC value is 0
    else if(PC_enable)
      PC_reg <= `SD next_PC; // transition to next PC
  end  // always
endmodule  // module if_stage
