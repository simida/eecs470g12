`timescale 1ns/100ps

module FrontEnd(
				//------From/To Mem------					
				input [3:0]   Imem2proc_response, // Tag from memory about current request
				input [63:0]  mem2proc_data,     // Data coming back from memory
				input [3:0]   mem2proc_tag,      // Tag from memory about current reply

				output logic [1:0]  proc2Imem_command,  // command sent to memory
				output logic [63:0] proc2Imem_addr,     // Address sent to memory


				//------To RS & ROB------	
	       output [1:0] [31:0] 			FE_ID_IR,
	       output [1:0] [63:0] 			FE_ID_NPC,

	       output [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_regA,         // reg A value
	       output [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_regB,         // reg B value
	       output [1:0] [`PRF_SIZE_LOG-1:0] 		FE_ID_dest_prfreg,     // destination (writeback) register index
	       output logic [1:0] [`ARF_SIZE_LOG-1:0] 		FE_ID_dest_arfreg,     // destination (writeback) register index
	       
	       //outputs from Decoder
	       output [1:0] [1:0] 			FE_ID_opa_select,    // ALU opa mux select (ALU_OPA_xxx *)
	       output [1:0] [1:0] 			FE_ID_opb_select,    // ALU opb mux select (ALU_OPB_xxx *)
   	                                                                  // (ZERO_REG if no writeback)
	       output [1:0] [`ALU_FUNC_NUM_LOG-1:0] 	FE_ID_alu_func,      // ALU function select (ALU_xxx *)
	       output [1:0]        			FE_ID_rd_mem,        // does inst read memory?
	       output [1:0]        			FE_ID_wr_mem,        // does inst write memory?
	       output [1:0]  			      FE_ID_cond_branch,   // is inst a conditional branch?
	       output [1:0]        			FE_ID_uncond_branch, // is inst an unconditional branch 
	                                                                  // or jump?
	       output [1:0]       			FE_ID_halt,          // is inst a halt 00 01 10 11
	       output [1:0]       			FE_ID_illegal,       // is inst an illegal inst 00 01 10 11
	       output [1:0]       			FE_ID_valid_inst,    // is inst a valid instruction to be  00 01 10 11


		//------To RS------
		output logic [63:0] FE_rrat_valid_list,
		output logic [1:0] [`PRF_IDX-1:0]	FE_prf_freed,

		//------From ROB------	
		input logic       								 ROB_full,
		input logic       								 ROB_full_1left,
		input logic [1:0] [`PRF_SIZE_LOG-1:0]   RET_prfn,
		input logic [1:0] [`ARF_SIZE_LOG-1:0]   RET_arfn,
		input logic [1:0] 							     	RET_valid,
		input logic 								       RET_branch_miss,
		input logic [63:0]								RET_branch_addr,
		
		
		//------From RS------	
				input logic [3:0] [1:0] RS_full,

				input clock,
				input reset
);


					//------------FRL--Fetcher--------
					logic	[1:0] FRL_empty; 				

					//------------Fetcher--Decoder------
					logic	[1:0] if_valid_inst, id_valid_inst; 

					//------------Decoder--Rat------
   logic [1:0] [4:0] 				      ra_idx_out,rb_idx_out;








	wire [1:0] ROB_full_inside ={ROB_full,ROB_full_1left};
	wire Struct_Stall;//if None of the Instr fetched/decoded will be accepted
	wire Two_Accept;//if Both instr will be accepted
	reg instr0_accept, instr1_accept;
	
	assign FE_ID_valid_inst[0]=instr0_accept&id_valid_inst[0];
	assign FE_ID_valid_inst[1]=instr1_accept&id_valid_inst[0];

	assign Struct_Stall = (~instr0_accept) && (~instr1_accept);
	assign Two_Accept = (instr0_accept && instr1_accept);

	wire ALU0 = (FE_ID_alu_func[0] != `ALU_MULQ) && !(FE_ID_cond_branch[0] | FE_ID_uncond_branch[0]) && !(FE_ID_rd_mem[0] | FE_ID_wr_mem[0]);
	wire ALU1 = (FE_ID_alu_func[1] != `ALU_MULQ) && !(FE_ID_cond_branch[1] | FE_ID_uncond_branch[1]) && !(FE_ID_rd_mem[1] | FE_ID_wr_mem[1]);
	always_comb
   		begin
		   instr0_accept=1;
		   instr1_accept=1;
		   if(~id_valid_inst[0]) instr0_accept=0;
		   if(~id_valid_inst[1]) instr1_accept=0;
		   if(ROB_full_inside[1] | FRL_empty[1]) begin instr0_accept=0; instr1_accept=0; end
		   if(ROB_full_inside[0] | FRL_empty[0]) instr1_accept=0;
		   
		   if((FE_ID_cond_branch[0] | FE_ID_uncond_branch[0]) && RS_full[1][1]) instr0_accept=0;
		   if((FE_ID_cond_branch[1] | FE_ID_uncond_branch[1]) && RS_full[1][1]) instr1_accept=0;
		   if((FE_ID_cond_branch[0] | FE_ID_uncond_branch[0]) && RS_full[1][0] && (FE_ID_cond_branch[1] | FE_ID_uncond_branch[1])) instr1_accept=0;
		   
		   if((FE_ID_alu_func[0] == `ALU_MULQ) && RS_full[2][1]) instr0_accept=0;
		   if((FE_ID_alu_func[1] == `ALU_MULQ) && RS_full[2][1]) instr1_accept=0;
		   if((FE_ID_alu_func[0] == `ALU_MULQ) && RS_full[2][0] && (FE_ID_alu_func[1] == `ALU_MULQ)) instr1_accept=0;
		   
		   if((FE_ID_rd_mem[0] | FE_ID_wr_mem[0]) && RS_full[3][1]) instr0_accept=0;
		   if((FE_ID_rd_mem[1] | FE_ID_wr_mem[1]) && RS_full[3][1]) instr1_accept=0;
		   if((FE_ID_rd_mem[1] | FE_ID_wr_mem[1]) && RS_full[3][0] && (FE_ID_rd_mem[0] | FE_ID_wr_mem[0])) instr1_accept=0;
		   
		   if(ALU0 && RS_full[0][1]) instr0_accept=0;
		   if(ALU1 && RS_full[0][1]) instr1_accept=0;
		   if(ALU0 && RS_full[0][0] && ALU1) instr1_accept=0;
		   if(~instr0_accept) instr1_accept = 0;   
		end








//----------------------FETCH-------------------------------

				// Icache wires
				logic [63:0] cachemem_data;
				logic        cachemem_valid;
				logic  [4:0] Icache_rd_idx;
				logic [23:0] Icache_rd_tag;
				logic  [4:0] Icache_wr_idx;
				logic [23:0] Icache_wr_tag;
				logic        Icache_wr_en;
				logic [63:0] Icache_data_out, proc2Icache_addr;
				logic        Icache_valid_out;



  // Actual cache (data and tag RAMs)
  cache cachememory (// inputs
            .clock(clock),
            .reset(reset),

						//Interface From Mem
            .wr1_data(mem2proc_data),

						//Interface From Cache controler
            .wr1_en(Icache_wr_en),
            .wr1_idx(Icache_wr_idx),
            .wr1_tag(Icache_wr_tag),            
            .rd1_idx(Icache_rd_idx),
            .rd1_tag(Icache_rd_tag),

            // outputs
						//Interface To Cache controler
            .rd1_data(cachemem_data),
            .rd1_valid(cachemem_valid)
           );

  // Cache controller
  icache icache_0(
					// inputs 
          .clock(clock),
          .reset(reset),

					//Interface From Fetcher
          .proc2Icache_addr(proc2Icache_addr),
					//Interface to Fetcher
          .Icache_data_out(Icache_data_out),
          .Icache_valid_out(Icache_valid_out),


					//Interface to MEM
          .proc2Imem_command(proc2Imem_command),
          .proc2Imem_addr(proc2Imem_addr),
					//Interface From Mem
          .Imem2proc_response(Imem2proc_response),
          .Imem2proc_data(mem2proc_data),
          .Imem2proc_tag(mem2proc_tag),


					//Interface to ICache
          .current_index(Icache_rd_idx),
          .current_tag(Icache_rd_tag),
          .last_index(Icache_wr_idx),
          .last_tag(Icache_wr_tag),
          .data_write_enable(Icache_wr_en),
					//Interface From ICache
          .cachemem_data(cachemem_data),
          .cachemem_valid(cachemem_valid)

         );



	Fetcher Fetcher1(
					//-----------To Cache controler------------------
					.proc2Icache_addr(proc2Icache_addr),
					//-----------From Cache controler----------------
				  .Icache2proc_data(Icache_data_out),		   
				  .Icache_valid(Icache_valid_out),

					//-----------To Decoder/RS/ROB------------------
					//First instr
					.NPC1(FE_ID_NPC[0]),			     
				  .IR1(FE_ID_IR[0]),			        
				  .if_valid_inst1(if_valid_inst[0]),
					//Second instr
					.NPC2(FE_ID_NPC[1]),			     
				  .IR2(FE_ID_IR[1]),			        
				  .if_valid_inst2(if_valid_inst[1]),


					//-----------From ROB------------------
				  .flush(RET_branch_miss),              
					.target_pc(RET_branch_addr),           

					//------------From ROB/RS/FRL--Structure Hazard------
					.Two_Accept(Two_Accept),
					.Struct_Stall(Struct_Stall),

				  .clock(clock),
				  .reset(reset)
        ); 




//----------------------DECODE-------------------------------

	wire [1:0] cpuid_out;
	wire [1:0] ldl_mem_out;
	wire [1:0] stc_mem_out;
	Decoder Decoder0(
          //-------From Fetcher------
					.IR(FE_ID_IR[0]),
				  .if_valid_inst(if_valid_inst[0]),

					//-------To Renamer------
					.ra_idx_out(ra_idx_out[0]),
			  	.rb_idx_out(rb_idx_out[0]),
			  	.dest_idx_out(FE_ID_dest_arfreg[0]),


					//-------To RS/ROB------
					.opa_select_out(FE_ID_opa_select[0]),
					.opb_select_out(FE_ID_opb_select[0]),
					.alu_func_out(FE_ID_alu_func[0]),
					.rd_mem_out(FE_ID_rd_mem[0]),
					.wr_mem_out(FE_ID_wr_mem[0]),
					.ldl_mem_out(ldl_mem_out[0]),
					.stc_mem_out(stc_mem_out[0]),
					.cond_branch_out(FE_ID_cond_branch[0]),
					.uncond_branch_out(FE_ID_uncond_branch[0]),
					.halt_out(FE_ID_halt[0]),
					.cpuid_out(cpuid_out[0]),
					.illegal_out(FE_ID_illegal[0]),
					.id_valid_inst_out(id_valid_inst[0])
        );

	Decoder Decoder1(
          //-------From Fetcher------
					.IR(FE_ID_IR[1]),
				  .if_valid_inst(if_valid_inst[1]),

					//-------To Renamer------
					.ra_idx_out(ra_idx_out[1]),
			  	.rb_idx_out(rb_idx_out[1]),
			  	.dest_idx_out(FE_ID_dest_arfreg[1]),

					//-------To RS/ROB------
					.opa_select_out(FE_ID_opa_select[1]),
					.opb_select_out(FE_ID_opb_select[1]),
					.alu_func_out(FE_ID_alu_func[1]),
					.rd_mem_out(FE_ID_rd_mem[1]),
					.wr_mem_out(FE_ID_wr_mem[1]),
					.ldl_mem_out(ldl_mem_out[1]),
					.stc_mem_out(stc_mem_out[1]),
					.cond_branch_out(FE_ID_cond_branch[1]),
					.uncond_branch_out(FE_ID_uncond_branch[1]),
					.halt_out(FE_ID_halt[1]),
					.cpuid_out(cpuid_out[1]),
					.illegal_out(FE_ID_illegal[1]),
					.id_valid_inst_out(id_valid_inst[1])
        );





//----------------------RENAME-------------------------------


	//---------RRAT--RAT-------------------
	wire [31:0] [`PRF_IDX-1:0] RRAT_registers;

	//rename happens only when instr is valid and dest_reg is not ZERO_REG
	wire instr0_rename=(FE_ID_valid_inst[0] && (FE_ID_dest_arfreg[0] != `ZERO_REG));
	wire instr1_rename=(FE_ID_valid_inst[1] && (FE_ID_dest_arfreg[1] != `ZERO_REG));


	//write to RAT only when need rename and rename succeeds
	wire rename_v1,rename_v2;
	wire RAT_wr1 = instr0_rename && rename_v1;
	wire RAT_wr2 = instr1_rename && rename_v2;
	
	RAT RAT1(
				//------From Decoder------
					.arf_regA(ra_idx_out[0]),
					.arf_regB(rb_idx_out[0]), 
					.arf_dest(FE_ID_dest_arfreg[0]),
					.wr_en(RAT_wr1),

					.arf_regA1(ra_idx_out[1]),
					.arf_regB1(rb_idx_out[1]), 
					.arf_dest1(FE_ID_dest_arfreg[1]),
					.wr_en1(RAT_wr2),

				//------To RS----------
					.prf_regA(FE_ID_regA[0]),
					.prf_regB(FE_ID_regB[0]),
					.prf_regA1(FE_ID_regA[1]),
					.prf_regB1(FE_ID_regB[1]),

				//------From ROB
					.flush(RET_branch_miss),

				//------From FRL------
					.prf_dest(FE_ID_dest_prfreg[0]),
					.prf_dest1(FE_ID_dest_prfreg[1]),

				//------From RRAT
					.RRAT_registers(RRAT_registers),

					.clock(clock),
					.reset(reset)
				);


	logic [5:0] freed_prf1,freed_prf2;
	assign FE_prf_freed[0]=freed_prf1;
	assign FE_prf_freed[1]=freed_prf2;

	FreeRegList FRL(
					//-------From Decoder----------//rename controler
					.rename1(instr0_rename), .rename2(instr1_rename),

					//-------To RAT----------------//rename result
					.rename_v1(rename_v1), .rename_v2(rename_v2),
					.prf1(FE_ID_dest_prfreg[0]), .prf2(FE_ID_dest_prfreg[1]),

					.FRL_empty(FRL_empty), //not necessary to stall if instr don't write reg


					//-------From RRAT----------
					.free_v1(RET_valid[0]), .free_v2(RET_valid[1]), //used 2-way
					.freed_prf1(freed_prf1), .freed_prf2(freed_prf2), //used 2-way

					//------From ROB----------
					.flush(RET_branch_miss),

					.clock(clock),
					.reset(reset)			
				);


	RRAT RRAT1(
				//------From ROB----------
				.wr_en(RET_valid[0]), .wr_en2(RET_valid[1]),
				.arf_dest1(RET_arfn[0]),
				.prf_dest1(RET_prfn[0]),
				.arf_dest2(RET_arfn[1]),
				.prf_dest2(RET_prfn[1]),

				//------To FRL----------
				.freed_prf1(freed_prf1), .freed_prf2(freed_prf2),

				//------To RAT----------
				.RRAT_registers(RRAT_registers),
				.FE_rrat_valid_list(FE_rrat_valid_list),

				.clock(clock),
				.reset(reset)
			);





endmodule
