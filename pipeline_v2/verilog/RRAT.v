//Function: Map table: Save all the ARF to PRF mapping info (non-spectulative)
//Modifed by: Jingcheng Wang

//Caution!!! Convention: Please let the commit order follow the arf_dest1 and arf_dest2
//Corner case: arf_dest1 == arf_dest2 WAW

//When modify RRAT:
//		reset: all 1, 2, 3, ...
//		commit: ROB give ARF and PRF

module RRAT(
	//From ROB
  input logic [4:0]	arf_dest1,
	input logic [`PRF_IDX-1:0]	prf_dest1,
  input logic wr_en, wr_en2,
  input logic [4:0]	arf_dest2,
	input logic [`PRF_IDX-1:0]	prf_dest2,

	//To FRL
	output logic [`PRF_IDX-1:0]	freed_prf1, freed_prf2,

	//To RAT
	output [31:0] [`PRF_IDX-1:0]	RRAT_registers,

	output logic [63:0] FE_rrat_valid_list,

	input  logic	clock,
	input  logic	reset
);

  logic    [31:0] [`PRF_IDX-1:0] registers;   // 32, `PRF_IDX-bit Registers

  logic    [31:0] [`PRF_IDX-1:0] next_registers;   // 32, `PRF_IDX-bit Registers

	assign RRAT_registers = next_registers;

	always_comb
		begin
			freed_prf1 = 0;
			freed_prf2 = 0;
			if(wr_en && wr_en2 && (arf_dest1 == arf_dest2))
				begin
					freed_prf1 = registers[arf_dest1];
					freed_prf2 = prf_dest1;
				end
			else 
				begin
					if(wr_en) 
						freed_prf1 = registers[arf_dest1];
					if(wr_en2) 
						freed_prf2 = registers[arf_dest2];
				end
		end

								
	always_comb
		begin
			next_registers = registers;
			if(wr_en)		next_registers[arf_dest1] = prf_dest1;		
			if(wr_en2)	next_registers[arf_dest2] = prf_dest2;
		end
	

	
	integer i;
	always_ff @(posedge clock)
	begin
		if(reset)
			begin
				for(i=0; i<31; i=i+1)
				begin
					registers[i] 	<= `SD i;
				end
				registers[`ZERO_REG] 	<= `SD `PRF_ZERO_REG;
			end

		else
			begin
				registers[30:0] <= `SD next_registers[30:0]; 
			end
	end//	always_ff @(posedge clock)


	always_comb
	begin
		FE_rrat_valid_list = 64'h0;
		for(integer i=0;i<32;i=i+1)
		 FE_rrat_valid_list[next_registers[i]] = 1;
	end

endmodule


