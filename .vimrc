"[ .vimrc ]
	version 6.0
	set nocompatible

"[ editing ]
	set shiftwidth=4
	set tabstop=2
	set autoindent
	"set bs=2
	set backspace=indent,eol,start
	"set smartindent
	set t_kb=
	:fixdel "added on 7th
	set number
	set encoding=utf-8
"[ misc ]
	set showmatch
	set showmode
	set history=50

"[ syntax ]
	"set syntax=c
	syntax on

"[ ruler ]
	set ruler
	"set rulerformat=%30(%=%5*moonbear%4*%m\ %3*%l%4*,\ %3*%c%4*\ \<\ %2*%P%4*\ \>%)
	set showcmd

"[ status ]
	highlight User1 ctermfg=Red 
	highlight User2 term=underline cterm=underline ctermfg=Green
	highlight User3 term=underline cterm=underline ctermfg=Yellow
	highlight User4 term=underline cterm=underline ctermfg=white
	highlight User5 ctermfg=cyan
	highlight User6 ctermfg=white
	set laststatus=2
	set statusline=%4*%<\ %1*[%F]%4*\ %5*%y%4*%=%6*%m%4*\ %3*%l%4*,\ %3*%c%4*\ \<\ %2*%P%4*\ \>


"[ mouse ]
"	set mouse=a



set hlsearch
highlight comment ctermfg=cyan
highlight search ctermbg=yellow
highlight search ctermfg=black


