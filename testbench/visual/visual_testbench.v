/////////////////////////////////////////////////////////////////////////
//                                                                     //
//                                                                     //
//   Modulename :  visual_testbench.v                                  //
//                                                                     //
//  Description :  Testbench module for the verisimple pipeline        //
//                   for the visual debugger                           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

extern void initcurses(int);
extern void flushpipe();
extern void waitforresponse();
extern void initmem();
extern int get_instr_at_pc(int);
extern int not_valid_pc(int);

`define SD #1

module testbench;

  // Registers and wires used in the testbench
  logic        clock;
  logic        reset;
  logic [31:0] clock_count;
  logic [31:0] instr_count;
  int          wb_fileno;

  logic  [1:0] proc2mem_command;
  logic [63:0] proc2mem_addr;
  logic [63:0] proc2mem_data;
  logic  [3:0] mem2proc_response;
  logic [63:0] mem2proc_data;
  logic  [3:0] mem2proc_tag;

  logic  [3:0] pipeline_completed_insts;
  logic  [3:0] pipeline_error_status;
  logic  [4:0] pipeline_commit_wr_idx;
  logic [63:0] pipeline_commit_wr_data;
  logic        pipeline_commit_wr_en;
  logic [63:0] pipeline_commit_NPC;


  logic [63:0] if_NPC_out;
  logic [31:0] if_IR_out;
  logic        if_valid_inst_out;
  logic [63:0] if_id_NPC;
  logic [31:0] if_id_IR;
  logic        if_id_valid_inst;
  logic [63:0] id_ex_NPC;
  logic [31:0] id_ex_IR;
  logic        id_ex_valid_inst;
  logic [63:0] ex_mem_NPC;
  logic [31:0] ex_mem_IR;
  logic        ex_mem_valid_inst;
  logic [63:0] mem_wb_NPC;
  logic [31:0] mem_wb_IR;
  logic        mem_wb_valid_inst;

  // Strings to hold instruction opcode
  logic  [8*7:0] if_instr_str;
  logic  [8*7:0] id_instr_str;
  logic  [8*7:0] ex_instr_str;
  logic  [8*7:0] mem_instr_str;
  logic  [8*7:0] wb_instr_str;


  // Instantiate the Pipeline
  pipeline pipeline (// Inputs
                       .clock             (clock),
                       .reset             (reset),
                       .mem2proc_response (mem2proc_response),
                       .mem2proc_data     (mem2proc_data),
                       .mem2proc_tag      (mem2proc_tag),

                        // Outputs
                       .proc2mem_command  (proc2mem_command),
                       .proc2mem_addr     (proc2mem_addr),
                       .proc2mem_data     (proc2mem_data),

                       .pipeline_completed_insts(pipeline_completed_insts),
                       .pipeline_error_status(pipeline_error_status),
                       .pipeline_commit_wr_data(pipeline_commit_wr_data),
                       .pipeline_commit_wr_idx(pipeline_commit_wr_idx),
                       .pipeline_commit_wr_en(pipeline_commit_wr_en),
                       .pipeline_commit_NPC(pipeline_commit_NPC),

                       .if_NPC_out(if_NPC_out),
                       .if_IR_out(if_IR_out),
                       .if_valid_inst_out(if_valid_inst_out),
                       .if_id_NPC(if_id_NPC),
                       .if_id_IR(if_id_IR),
                       .if_id_valid_inst(if_id_valid_inst),
                       .id_ex_NPC(id_ex_NPC),
                       .id_ex_IR(id_ex_IR),
                       .id_ex_valid_inst(id_ex_valid_inst),
                       .ex_mem_NPC(ex_mem_NPC),
                       .ex_mem_IR(ex_mem_IR),
                       .ex_mem_valid_inst(ex_mem_valid_inst),
                       .mem_wb_NPC(mem_wb_NPC),
                       .mem_wb_IR(mem_wb_IR),
                       .mem_wb_valid_inst(mem_wb_valid_inst)
                      );


  // Instantiate the Data Memory
  mem memory (// Inputs
            .clk               (clock),
            .proc2mem_command  (proc2mem_command),
            .proc2mem_addr     (proc2mem_addr),
            .proc2mem_data     (proc2mem_data),

             // Outputs

            .mem2proc_response (mem2proc_response),
            .mem2proc_data     (mem2proc_data),
            .mem2proc_tag      (mem2proc_tag)
           );

  integer i;

  // Generate System Clock
  always
  begin
    #5 clock = ~clock;
  end

  // Count the number of posedges and number of instructions completed
  // till simulation ends
  always @(posedge clock)
  begin
    if(reset)
    begin
      clock_count <= `SD 0;
    end
    else
    begin
      clock_count <= `SD (clock_count + 1);
    end
  end  

  initial
  begin
    clock = 0;
    reset = 0;

    // Call to initialize visual debugger
    // *Note that after this, all stdout output goes to visual debugger*
    // each argument is number of registers/signals for the group
    // (IF, IF/ID, ID, ID/EX, EX, EX/MEM, MEM, MEM/WB, WB, Misc)
    initcurses(3);
    // Pulse the reset signal
    reset = 1'b1;
    @(posedge clock);
    @(posedge clock);

    // Read program contents into memory array
    //$readmemh("program.mem", memory.unified_memory);

    @(posedge clock);
    @(posedge clock);
    `SD;
    // This reset is at an odd time to avoid the pos & neg clock edges
    reset = 1'b0;
    #1000
        $display("\nDONE\n");
	$finish;
  end

  // This block is where we dump all of the signals that we care about to
  // the visual debugger.  Notice this happens at *every* clock edge.
  always @(clock) begin
    #2;

    // Dump clock and time onto stdout
    $display("c%h%7.0d",clock,clock_count);
    $display("t%8.0f",$time);
    $display("z%h",reset);

    // Dump interesting register/signal contents onto stdout
    // format is "<reg group prefix><name> <width in hex chars>:<data>"
    // Current register groups (and prefixes) are:
    // f: IF   d: ID   e: EX   m: MEM    w: WB  v: misc. reg
    // g: IF/ID   h: ID/EX  i: EX/MEM  j: MEM/WB

    // RAT signals (32) - prefix 'a'
    for (i=0; i<32; i++) begin
	$display("a6:%6d", i);
    end

    // RRAT signals (32) - prefix 'r'
    for (i=0; i<32; i++) begin
	$display("r6:%6d", i);
    end

    // PRF signals (64) - prefix 'p'
    for (i=0; i<64; i++) begin
	$display("p6:%6d", i);
    end

    // ROB signals (32) - prefix 'o'
    for (i=0; i<32; i++) begin
	$display("o6:%6d", i);
    end

    // dispatch signals (2) - prefix 'd'
    for (i=0; i<2; i++) begin
	$display("d6:%6d", $random);
    end

    // RS signals (8) - prefix 's'
    for (i=0; i<8; i++) begin
	$display("s6:%6d", $random);
    end

    // issue signals (8) - prefix 'i'
    for (i=0; i<8; i++) begin
	$display("i6:%6d", $random);
    end

    // FU signals (8) - prefix 'f'
    for (i=0; i<8; i++) begin
	$display("f6:%6d", $random);
    end

    // complete signals (4) - prefix 'l'
    for (i=0; i<4; i++) begin
	$display("l6:%6d", $random);
    end

    // retire signals (4) - prefix 'e'
    for (i=0; i<4; i++) begin
	$display("e6:%6d", $random);
    end

    // misc signals (3) - prefix 'v'
    for (i=0; i<3; i++) begin
	$display("v6:%6d", 32);
    end

    // must come last
    $display("break");

    // This is a blocking call to allow the debugger to control when we
    // advance the simulation
    waitforresponse();
  end

endmodule
